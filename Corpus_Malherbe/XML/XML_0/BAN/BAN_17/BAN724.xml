<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN724">
				<head type="main">Bakkhos</head>
				<head type="sub_1">Prologue récité à l’Opéra par C. Coquelin <lb></lb>dans la représentation consacrée a l’histoire du théatre <lb></lb>le 27 janvier 1886</head>
				<lg n="1">
					<l n="1" num="1.1">Hommes, je suis Bakkhos aux lèvres purpurines,</l>
					<l n="2" num="1.2">Qui reçoit le soleil embrasé sur son flanc,</l>
					<l n="3" num="1.3">Et qui meurt et renaît dans vos fortes poitrines,</l>
					<l n="4" num="1.4">Et le sang généreux de la vigne est mon sang.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Je suis l’enchantement des soirs et des journées.</l>
					<l n="6" num="2.2">Je suis le Vin, qui met dans vos cœurs un éclair,</l>
					<l n="7" num="2.3">Et, prodige inouï, c’est de moi que sont nées</l>
					<l n="8" num="2.4">La fière Tragédie et sa sœur à l’œil clair,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">La Comédie, aimable et folle entre vos gloires,</l>
					<l n="10" num="3.2">Dont la sagesse humaine est le riant trésor,</l>
					<l n="11" num="3.3">Et qui sur son beau front tresse des grappes noires</l>
					<l n="12" num="3.4">Et frappe l’air du bruit de ses cymbales d’or.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">C’est le soir, dans un bourg glorieux de l’Attique,</l>
					<l n="14" num="4.2">Où le soleil couchant s’empourpre de rougeurs</l>
					<l n="15" num="4.3">Et, poussant vers les cieux un grand cri frénétique,</l>
					<l n="16" num="4.4">Sur leurs lourds chariots montent les vendangeurs.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Et près d’eux, esquissant leurs danses orageuses,</l>
					<l n="18" num="5.2">Pour répandre la joie en passant dans les bourgs,</l>
					<l n="19" num="5.3">Les yeux rouges, le front taché, les vendangeuses</l>
					<l n="20" num="5.4">Avec des gestes fous tapent sur leurs tambours.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Des porteurs de paniers marchent en longues lignes,</l>
					<l n="22" num="6.2">Flamboyants et vermeils dans les roses du soir,</l>
					<l n="23" num="6.3">Et tout fumant, le sang mystérieux des vignes</l>
					<l n="24" num="6.4">Sur un long rhythme clair s’écoule du pressoir.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">L’âne pensif et doux, tout orné de guirlandes,</l>
					<l n="26" num="7.2">De la fête en délire est l’hôte essentiel ;</l>
					<l n="27" num="7.3">Ses oreilles sans fin se lèvent toutes grandes,</l>
					<l n="28" num="7.4">Comme pour déchirer le voile bleu du ciel.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Sur son dos ingénu ballotte une outre pleine,</l>
					<l n="30" num="8.2">A moins que ce ne soit, turbulent et divin,</l>
					<l n="31" num="8.3">Le beau ventre gonflé de mon père Silène,</l>
					<l n="32" num="8.4">Glorieux et ravi d’avoir bu trop de vin.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">D’un char à l’autre, on parle, on s’injurie, on joue.</l>
					<l n="34" num="9.2">Holà ! Doris, tu bois à flots inépuisés !</l>
					<l n="35" num="9.3">Hé ! Céléno, qui t’a si bien rougi la joue ?</l>
					<l n="36" num="9.4">Sont-ce les noirs raisins, bacchante, ou les baisers ?</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Puis, sur un autre char, selon l’antique mode,</l>
					<l n="38" num="10.2">On chante ma louange, ou celle d’un héros,</l>
					<l n="39" num="10.3">Ou quelque dieu célèbre, et la strophe de l’Ode</l>
					<l n="40" num="10.4">Voltige en palpitant sur les lourds tombereaux.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Puis, comme dans la mer, dont les chevaux hennissent</l>
					<l n="42" num="11.2">Quand la vague tressaille avec un bruit vainqueur,</l>
					<l n="43" num="11.3">A la voix du chanteur les autres voix s’unissent,</l>
					<l n="44" num="11.4">Pareilles aux rumeurs des flots, — et c’est le Chœur !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Et c’est la Tragédie et c’est la Comédie,</l>
					<l n="46" num="12.2">Avec les longs sanglots et les rires vengeurs,</l>
					<l n="47" num="12.3">Qui ravissent les cieux de leur chanson hardie,</l>
					<l n="48" num="12.4">Et qui naissent ainsi parmi les vendangeurs.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">O mes filles, gardez vos fronts tachés de lie,</l>
					<l n="50" num="13.2">Sous la pourpre héroïque et sous le péplos blanc !</l>
					<l n="51" num="13.3">O Melpomène, et toi, vendangeuse Thalie,</l>
					<l n="52" num="13.4">Buvez toujours le flot généreux de mon sang.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">O chanteuses, gardez toujours l’antique ivresse,</l>
					<l n="54" num="14.2">Et n’oubliez jamais votre berceau natal.</l>
					<l n="55" num="14.3">Toi, la dominatrice, et toi, la charmeresse,</l>
					<l n="56" num="14.4">Soûlez-vous de ce vin qu’on nomme l’Idéal !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Et, vos fronts couronnés de fleurs que rien ne fane,</l>
					<l n="58" num="15.2">Laissant la platitude au menteur exécré,</l>
					<l n="59" num="15.3">Muse d’Eschyle, et toi, muse d’Aristophane,</l>
					<l n="60" num="15.4">Souvenez-vous de mordre à mon raisin sacré.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Toi, redis-nous les Rois et leur destin funeste,</l>
					<l n="62" num="16.2">La pâle Phèdre en proie à ses tristes aveux,</l>
					<l n="63" num="16.3">Argos et le festin horrible de Thyeste</l>
					<l n="64" num="16.4">Et les malheurs venus d’Hélène aux beaux cheveux.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Fais revivre pour nous la grande Histoire amère,</l>
					<l n="66" num="17.2">Dis-nous Hector, pareil au fougueux aquilon,</l>
					<l n="67" num="17.3">Oreste en pleurs, fuyant les Chiennes de sa mère,</l>
					<l n="68" num="17.4">Et Cassandre criant : Apollon ! Apollon !</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Et toi, ma préférée, ô folle Comédie,</l>
					<l n="70" num="18.2">Montre ton rire en fleur, pareil au lys éclos !</l>
					<l n="71" num="18.3">Que ton regard s’allume, ainsi qu’un incendie :</l>
					<l n="72" num="18.4">Fais tintinnabuler tes grappes de grelots !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Que le doux vent d’été baise ta gorge nue !</l>
					<l n="74" num="19.2">La lèvre humide encor du nectar que tu bois,</l>
					<l n="75" num="19.3">Montre l’Humanité, cette race ingénue,</l>
					<l n="76" num="19.4">Pareille, en sa démence, aux animaux des bois !</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Montre ces insensés, et l’homme, et l’homme, et l’homme,</l>
					<l n="78" num="20.2">Penché vers l’ombre, au lieu de regarder le jour,</l>
					<l n="79" num="20.3">Que devant lui, pareils à des bêtes de somme,</l>
					<l n="80" num="20.4">Chasse, à grands coups de fouet, l’inévitable Amour !</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Ris avec Plaute, avec l’ingénieux Térence !</l>
					<l n="82" num="21.2">Mais en donnant la vie à leurs acteurs bouffons,</l>
					<l n="83" num="21.3">Enivre-toi déjà, Muse, de l’espérance</l>
					<l n="84" num="21.4">Qui tombe jusqu’à toi du haut des cieux profonds.</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Car pour mêler sa flamme avec la fange humaine,</l>
					<l n="86" num="22.2">Pour livrer l’Imposteur à l’éternel tourment,</l>
					<l n="87" num="22.3">Et montrer le roi Zeus rêvant aux pieds d’Alcmène,</l>
					<l n="88" num="22.4">Un homme un jour viendra, qui sera ton amant.</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Oui, celui-là sur qui tout mon espoir se fonde,</l>
					<l n="90" num="23.2">C’est le penseur sublime et le grand ouvrier,</l>
					<l n="91" num="23.3">C’est le Contemplateur à la tête féconde,</l>
					<l n="92" num="23.4">Qui sera, comme un roi, couronné de laurier.</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Tu baiseras son front de ta bouche ravie,</l>
					<l n="94" num="24.2">Et tu le serviras avec fidélité.</l>
					<l n="95" num="24.3">Mais lorsque ce génie aura quitté la vie</l>
					<l n="96" num="24.4">Pour grandir, triomphant, dans l’immortalité,</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Reste après lui, pensive, auguste et familière,</l>
					<l n="98" num="25.2">Et comme aux premiers jours de ton matin vermeil,</l>
					<l n="99" num="25.3">O fille de Bakkhos, amante de Molière,</l>
					<l n="100" num="25.4">Nymphe, bois notre vin de pourpre et de soleil.</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Garde pieusement la joie et le délire</l>
					<l n="102" num="26.2">Que ce poëte a mis dans ton œil radieux,</l>
					<l n="103" num="26.3">Et souviens-toi toujours, déesse, que le rire</l>
					<l n="104" num="26.4">Est le plus beau présent qui nous vienne des Dieux !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1886">Mardi, 26 janvier 1886.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>