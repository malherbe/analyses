<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN696">
				<head type="main">L’Enfant</head>
				<lg n="1">
					<l n="1" num="1.1">C’était au Luxembourg, par un matin brûlant</l>
					<l n="2" num="1.2">De Juillet, où le clair soleil étincelant</l>
					<l n="3" num="1.3">Versait partout les feux de ses apothéoses,</l>
					<l n="4" num="1.4">Jetait des taches d’or parmi les lauriers-roses</l>
					<l n="5" num="1.5">Et baignant de rayons leurs cœurs incendiés,</l>
					<l n="6" num="1.6">Embrasait, furieux, les fleurs des grenadiers.</l>
					<l n="7" num="1.7">De beaux enfants jouaient, montrant leurs jambes nues,</l>
					<l n="8" num="1.8">Gais, sérieux, ouvrant leurs bouches ingénues,</l>
					<l n="9" num="1.9">Et la course faisait voler dans l’air vermeil</l>
					<l n="10" num="1.10">Leurs cheveux frémissants, blonds comme le soleil.</l>
					<l n="11" num="1.11">Les beaux petits garçons et les petites filles</l>
					<l n="12" num="1.12">Jouaient à la madame, à la toupie, aux billes.</l>
					<l n="13" num="1.13">Ceux-ci, vite, emplissaient à la pelle des seaux</l>
					<l n="14" num="1.14">De sable, ou bien faisaient voltiger les cerceaux,</l>
					<l n="15" num="1.15">Ou se disputaient, fous et prompts à la riposte.</l>
					<l n="16" num="1.16">D’autres couraient ensemble et jouaient à la poste,</l>
					<l n="17" num="1.17">Faisant voler au vent leur petit cotillon.</l>
					<l n="18" num="1.18">L’un était le cheval, l’autre le postillon,</l>
					<l n="19" num="1.19">Et leurs petits amis avaient grand’peine à suivre</l>
					<l n="20" num="1.20">Les claquements du fouet et les grelots de cuivre.</l>
					<l n="21" num="1.21">Tous, douces fleurs, charmante aurore du présent,</l>
					<l n="22" num="1.22">Allaient se bousculant, se battant, se baisant,</l>
					<l n="23" num="1.23">Et leurs grands yeux emplis d’espoir et de chimères</l>
					<l n="24" num="1.24">Faisaient s’épanouir les sourires des mères,</l>
					<l n="25" num="1.25">Et tout n’était que joie infinie à l’entour.</l>
					<l n="26" num="1.26">Mais, ô rêve ! ô sinistre enchantement du jour !</l>
					<l n="27" num="1.27">Comme s’il eût caché d’invisibles désastres,</l>
					<l n="28" num="1.28">Il sembla que l’azur, où sommeillent les astres,</l>
					<l n="29" num="1.29">S’allumait, et dans l’air fluide et paresseux,</l>
					<l n="30" num="1.30">Les spectres de midi, plus effrayants que ceux</l>
					<l n="31" num="1.31">De la nuit, au milieu des rayons apparurent,</l>
					<l n="32" num="1.32">Foules qui lentement s’enflèrent et s’accrurent,</l>
					<l n="33" num="1.33">Flottant dans la lumière et l’éblouissement ;</l>
					<l n="34" num="1.34">Et dans le lointain clair s’ébauchaient vaguement</l>
					<l n="35" num="1.35">Ces fantômes gardant leur sinistre posture,</l>
					<l n="36" num="1.36">Teints des couleurs du prisme et de la pourriture.</l>
					<l n="37" num="1.37">C’était le Meurtre ayant dans la main son couteau,</l>
					<l n="38" num="1.38">Le Vol, cachant des sacs pleins d’or sous un manteau,</l>
					<l n="39" num="1.39">L’Usure avec des mains faites comme des serres,</l>
					<l n="40" num="1.40">La Débauche riante au sein rongé d’ulcères,</l>
					<l n="41" num="1.41">L’Avarice veillant auprès d’un coffre ouvert,</l>
					<l n="42" num="1.42">L’Ivresse avec son verre empli du poison vert,</l>
					<l n="43" num="1.43">La Colère acharnée à de hideux sévices,</l>
					<l n="44" num="1.44">Et toute la cohorte innombrable des Vices</l>
					<l n="45" num="1.45">Et des vils Appétits repus et triomphants.</l>
					<l n="46" num="1.46">Et tous, en regardant les beaux petits enfants,</l>
					<l n="47" num="1.47">Disaient : Vous serez les acteurs des sombres drames,</l>
					<l n="48" num="1.48">Les vivants. Vous serez des hommes et des femmes,</l>
					<l n="49" num="1.49">Nés de la fange, par le désir entraînés,</l>
					<l n="50" num="1.50">Abjects, vains ; c’est pourquoi vous nous appartenez.</l>
					<l n="51" num="1.51">Ivres et furieux, vous chercherez vos joies</l>
					<l n="52" num="1.52">Dans la chair pantelante, et vous êtes nos proies.</l>
					<l n="53" num="1.53">Mais un frisson d’horreur dans leur foule courut</l>
					<l n="54" num="1.54">Et tranquille, parmi les enfants apparut,</l>
					<l n="55" num="1.55">Avec une douceur amie et reposée,</l>
					<l n="56" num="1.56">Pareil au chaste lys que baigne la rosée,</l>
					<l n="57" num="1.57">Un enfant couronné d’épines, que ceignait</l>
					<l n="58" num="1.58">Une blanche auréole, et dont le front saignait.</l>
					<l n="59" num="1.59">Devant son clair regard, aussi doux que les baumes,</l>
					<l n="60" num="1.60">S’enfuirent, éperdus, les livides fantômes,</l>
					<l n="61" num="1.61">Les Vices, les Fureurs, les sanglants Appétits,</l>
					<l n="62" num="1.62">Et lui, le chaste Enfant, tandis que les petits</l>
					<l n="63" num="1.63">Le regardaient sans peur de leurs yeux téméraires,</l>
					<l n="64" num="1.64">Il leur disait : Jouez en paix, mes petits frères.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1887">Mercredi, 5 janvier 1887.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>