<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN358">
				<head type="main">Le Livre d’Heures de la Châtelaine</head>
				<lg n="1">
					<l n="1" num="1.1">Or la comtesse Yseult avait un livre d’Heures,</l>
					<l n="2" num="1.2">Si beau que ses enfants en étaient orgueilleux,</l>
					<l n="3" num="1.3">Et que la Reine même, en ses nobles demeures,</l>
					<l n="4" num="1.4">N’avait rien de si riche et de si merveilleux.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Un feuillage d’argent couvrait de frêles branches</l>
					<l n="6" num="2.2">Le dos clair du missel, et, sans plus d’ornements,</l>
					<l n="7" num="2.3">Sur son velours, couleur des premières pervenches,</l>
					<l n="8" num="2.4">On voyait resplendir un chiffre en diamants.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Le vélin des feuillets, où des images pures</l>
					<l n="10" num="3.2">Se détachaient aussi par un art surhumain,</l>
					<l n="11" num="3.3">Prêtait ses fonds de neige à des miniatures</l>
					<l n="12" num="3.4">Toutes brillantes d’or, d’azur et de carmin.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ici veillait Marie, et sur la paille fraîche,</l>
					<l n="14" num="4.2">Le bonhomme Joseph admirait en priant</l>
					<l n="15" num="4.3">Le Roi de l’univers couché dans une crèche,</l>
					<l n="16" num="4.4">Adoré pauvre et nu par les rois d’Orient.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Là, parmi les parfums qui ruisselaient en ondes,</l>
					<l n="18" num="5.2">Magdeleine, ravie et pleine de ferveur,</l>
					<l n="19" num="5.3">Dénouait ses cheveux, et de leurs nappes blondes</l>
					<l n="20" num="5.4">Elle essuyait les pieds de son divin Sauveur.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Ailleurs, sous le berceau d’une treille fleurie,</l>
					<l n="22" num="6.2">Où se mêlaient la vigne et le pampre vermeil,</l>
					<l n="23" num="6.3">L’enfant Jésus, porté par la Vierge Marie,</l>
					<l n="24" num="6.4">Souriait aux raisins inondés de soleil.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Puis, de tendres couleurs toutes enluminées,</l>
					<l n="26" num="7.2">Parmi les fonds d’argent par le rose adoucis,</l>
					<l n="27" num="7.3">Les légendes des saints dans les lettres ornées</l>
					<l n="28" num="7.4">Déroulaient tout au long de merveilleux récits.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Mais le peintre surtout, dans de riches losanges</l>
					<l n="30" num="8.2">Encadrés de rubis par son art précieux,</l>
					<l n="31" num="8.3">Avait représenté les extases des Anges</l>
					<l n="32" num="8.4">Transportés et ravis dans les sphères des cieux.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Les uns, dans le lapis couvert de sombres voiles</l>
					<l n="34" num="9.2">De leurs profonds regards teignant l’horizon bleu,</l>
					<l n="35" num="9.3">Conduisaient en rêvant des chariots d’étoiles</l>
					<l n="36" num="9.4">Et des astres épars aux crinières de feu.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Les autres, murmurant d’harmonieux distiques</l>
					<l n="38" num="10.2">Nés de l’embrassement de deux rhythmes charmés,</l>
					<l n="39" num="10.3">Tressaient les lys sans tache et les roses mystiques,</l>
					<l n="40" num="10.4">Pour ceindre de parfums leurs cheveux enflammés.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Comme sur les étangs les vertes demoiselles,</l>
					<l n="42" num="11.2">Ceux-là, rassérénant le splendide outremer,</l>
					<l n="43" num="11.3">Faisaient parmi l’éther frissonner leurs six ailes</l>
					<l n="44" num="11.4">Et baignaient de rayons les effluves de l’air.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Puis, d’autres s’enchantaient au délire des harpes.</l>
					<l n="46" num="12.2">Au bord du firmament penchés sur leurs genoux,</l>
					<l n="47" num="12.3">D’autres venaient tisser les suaves écharpes</l>
					<l n="48" num="12.4">Qui sont l’arc d’alliance entre le ciel et nous.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Et, parmi les lueurs les plus épanouies,</l>
					<l n="50" num="13.2">Humblement prosternés dans la pourpre des soirs,</l>
					<l n="51" num="13.3">D’autres, baignés enfin de clartés éblouies,</l>
					<l n="52" num="13.4">Jusqu’au Trône élevaient leurs fumants encensoirs.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Or souvent, l’âme prise à toutes ces féeries,</l>
					<l n="54" num="14.2">La belle Yseult suivait, les yeux remplis de pleurs,</l>
					<l n="55" num="14.3">Les tableaux plus vermeils que mille pierreries</l>
					<l n="56" num="14.4">Et le ruissellement de leurs vives couleurs.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Ensuite, regardant la fenêtre où le givre</l>
					<l n="58" num="15.2">Fleurit ses tendres lys faits d’un pâle duvet,</l>
					<l n="59" num="15.3">Debout et tout émue, elle fermait le livre,</l>
					<l n="60" num="15.4">Et pendant bien longtemps alors elle rêvait.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Ses cheveux qu’un bandeau de saphirs illumine,</l>
					<l n="62" num="16.2">S’échappant comme un fleuve en flots purs et dorés</l>
					<l n="63" num="16.3">Sur son corsage rose orné de blanche hermine,</l>
					<l n="64" num="16.4">Faisaient une auréole à ses yeux azurés.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Pensive, elle tenait toujours le livre d’Heures ;</l>
					<l n="66" num="17.2">Mais alors s’enfuyant sur des ailes de feu,</l>
					<l n="67" num="17.3">Toute à ses visions, flammes intérieures,</l>
					<l n="68" num="17.4">Son âme enamourée errait dans le ciel bleu.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Alors il lui semblait, sur le pavé des salles</l>
					<l n="70" num="18.2">S’échappant des feuillets de son missel fermé,</l>
					<l n="71" num="18.3">Voir fleurir en berceaux les roses idéales</l>
					<l n="72" num="18.4">Peintes sur les blancheurs du vélin parfumé.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Près des pâles bleuets, sur qui l’insecte rôde,</l>
					<l n="74" num="19.2">Le muguet odorant croissait au pied des lys,</l>
					<l n="75" num="19.3">Et sous les gazons verts aux reflets d’émeraude</l>
					<l n="76" num="19.4">Se mêlaient la pervenche et le myosotis.</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Penchés sur ses cheveux frissonnants comme un saule,</l>
					<l n="78" num="20.2">Le vol des Chérubins et les Anges aussi</l>
					<l n="79" num="20.3">Touchaient en se jouant son front et son épaule</l>
					<l n="80" num="20.4">De leur aile de neige, et lui parlaient ainsi :</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">O belle et douce Yseult, toi dont la vie est sainte,</l>
					<l n="82" num="21.2">Et, toute dévouée à des actes pieux,</l>
					<l n="83" num="21.3">Comme un calme ruisseau, s’écoule dans l’enceinte</l>
					<l n="84" num="21.4">De la maison bénie où dorment tes aïeux !</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Va, cesse d’envier les sereines extases</l>
					<l n="86" num="22.2">Et les félicités que nous goûtons sans fin</l>
					<l n="87" num="22.3">Dans les cieux de saphir, d’opale et de topazes</l>
					<l n="88" num="22.4">Où l’Archange sommeille aux bras du Séraphin.</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Car, aux yeux du Seigneur, tes yeux remplis d’étoiles,</l>
					<l n="90" num="23.2">Que sur le crucifix tu baisses en priant,</l>
					<l n="91" num="23.3">Valent tous les soleils et les astres sans voiles</l>
					<l n="92" num="23.4">Que nous guidons en chœur dans l’azur flamboyant.</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Tes lèvres sans souillure, et qu’une larme arrose</l>
					<l n="94" num="24.2">Lorsqu’on t’implore au nom de son bien aimé Fils,</l>
					<l n="95" num="24.3">Valent mieux devant lui que la mystique rose</l>
					<l n="96" num="24.4">Rougissante et fleurie entre les divins lys.</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Et l’encens de ton cœur, feu que Marie admire</l>
					<l n="98" num="25.2">Comme son plus suave et son plus cher trésor,</l>
					<l n="99" num="25.3">Monte aussi bien vers Dieu que l’encens ou la myrrhe</l>
					<l n="100" num="25.4">Qui fume à ses genoux dans nos encensoirs d’or !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1849">Août 1849.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>