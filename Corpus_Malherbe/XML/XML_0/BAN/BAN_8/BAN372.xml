<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN372">
				<lg n="1">
					<l n="1" num="1.1">Fille de la clarté, Muse aux regards vermeils,</l>
					<l n="2" num="1.2">Ouvre les yeux. Que font dans l’éther les soleils ?</l>
					<l n="3" num="1.3">Ils gravitent. Que fait l’Océan vaste ? Il broie</l>
					<l n="4" num="1.4">Les navires de l’homme en rugissant de joie.</l>
					<l n="5" num="1.5">Et le tonnerre ? Il gronde. Et l’aigle immense ? Il fond</l>
					<l n="6" num="1.6">Sur la brebis, du haut du ciel clair et profond,</l>
					<l n="7" num="1.7">Et l’emporte à son aire. Et le lion ? Il plante</l>
					<l n="8" num="1.8">Ses fortes dents parmi la chair vive et sanglante.</l>
					<l n="9" num="1.9">Et le doux rossignol ? Blessé cruellement</l>
					<l n="10" num="1.10">Par sa fleur, il la chante avec ravissement</l>
					<l n="11" num="1.11">Et retourne au buisson d’épines. Et la rose,</l>
					<l n="12" num="1.12">Que fait-elle du flot d’ambroisie ? Elle arrose</l>
					<l n="13" num="1.13">La terre de parfums et les grands cœurs d’amour.</l>
					<l n="14" num="1.14">Et le penseur ? Il vient à la clarté du jour</l>
					<l n="15" num="1.15">Pour secouer devant la foule intimidée</l>
					<l n="16" num="1.16">Ton glaive de lumière, inexorable Idée !</l>
					<l n="17" num="1.17">Et le poëte auguste ? Il tourne son flambeau</l>
					<l n="18" num="1.18">Vers la Beauté, sa foi, qu’on a mise au tombeau,</l>
					<l n="19" num="1.19">Et se penchant sur elle avec mélancolie,</l>
					<l n="20" num="1.20">Il relève en pleurant cette image avilie.</l>
					<l n="21" num="1.21">Et l’impuissant, ô Muse ? Il vit, fier de railler</l>
					<l n="22" num="1.22">Et de mentir. C’est bien, Muse, allons travailler.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1856">Février 1856.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>