<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN360">
				<head type="main">A Mesdemoiselles Aménaïde, Lyzie et Eugénie de Friberg</head>
				<lg n="1">
					<l n="1" num="1.1">O vous, mes jeunes sœurs que je ne connais pas !</l>
					<l n="2" num="1.2">Sur l’éternel gazon que caressent vos pas</l>
					<l n="3" num="1.3"><space quantity="8" unit="char"></space>Je vous vois passer souriantes.</l>
					<l n="4" num="1.4">C’est en vain que Thétis, reine du gouffre amer,</l>
					<l n="5" num="1.5">Vous cache à mes regards, ô perles de la mer,</l>
					<l n="6" num="1.6"><space quantity="8" unit="char"></space>Dans ses Antilles verdoyantes.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Poëte extasié que ravissent leurs jeux,</l>
					<l n="8" num="2.2">Ce n’est plus dans les bois du Parnasse neigeux</l>
					<l n="9" num="2.3"><space quantity="8" unit="char"></space>Que mon cœur rêve les trois Grâces ;</l>
					<l n="10" num="2.4">Ce n’est plus, Olmios, vers tes flots argentés</l>
					<l n="11" num="2.5">Que j’égare mes yeux et mes vers enchantés,</l>
					<l n="12" num="2.6"><space quantity="8" unit="char"></space>Dans le sable d’or où tu passes !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">C’est vers ce paradis désiré des marins,</l>
					<l n="14" num="3.2">Où sous les bananiers et dans les tamarins,</l>
					<l n="15" num="3.3"><space quantity="8" unit="char"></space>Les sylphes de l’air font la sieste,</l>
					<l n="16" num="3.4">Où cent îles en fleur, filles des Océans,</l>
					<l n="17" num="3.5">Sous les magnolias lavent leurs pieds géants</l>
					<l n="18" num="3.6"><space quantity="8" unit="char"></space>Dans une mer d’un bleu céleste.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">C’est parmi les saphirs où ces riants îlots</l>
					<l n="20" num="4.2">Sortent comme Cypris de l’écume des flots,</l>
					<l n="21" num="4.3"><space quantity="8" unit="char"></space>Peuplés de soudaines féeries,</l>
					<l n="22" num="4.4">Où, près de l’ananas et du pâle oranger,</l>
					<l n="23" num="4.5">Le hamac, suspendu comme un oiseau léger,</l>
					<l n="24" num="4.6"><space quantity="8" unit="char"></space>Berce les molles rêveries.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Je vous vois dans l’air pur de ces jardins si doux,</l>
					<l n="26" num="5.2">Causant et souriant, tandis qu’une de vous,</l>
					<l n="27" num="5.3"><space quantity="8" unit="char"></space>Ainsi qu’une amazone ailée,</l>
					<l n="28" num="5.4">Devance les éclairs et s’avance en rêvant</l>
					<l n="29" num="5.5">Sur un cheval fougueux, qui fustige le vent</l>
					<l n="30" num="5.6"><space quantity="8" unit="char"></space>De sa crinière échevelée.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Je vous vois, et mes vers fendent le ciel brumeux.</l>
					<l n="32" num="6.2">Puissent un jour me prendre et m’emporter comme eux</l>
					<l n="33" num="6.3"><space quantity="8" unit="char"></space>Sur le dos de la vague blonde,</l>
					<l n="34" num="6.4">Avec leurs mille pieds, pour mes désirs trop lents,</l>
					<l n="35" num="6.5">Ces navires de feu dont les baisers brûlants</l>
					<l n="36" num="6.6"><space quantity="8" unit="char"></space>Laissent une ride sur l’onde !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1850">Juillet 1850.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>