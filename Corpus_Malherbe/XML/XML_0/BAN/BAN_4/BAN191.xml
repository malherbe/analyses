<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">AMÉTHYSTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>319 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">AMÉTHYSTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleamethystes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1860-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN191">
				<head type="main">Vers sapphiques</head>
				<lg n="1">
					<l n="1" num="1.1">Ma foi, mon espoir, mes chants fiers et doux,</l>
					<l n="2" num="1.2">Je t’ai tout donné, jusqu’à mon courroux.</l>
					<l n="3" num="1.3">Ce n’est pas assez, dit ton cœur jaloux.</l>
					<l n="4" num="1.4"><space quantity="12" unit="char"></space>Il a bien raison !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Il me faut bénir ta blonde toison,</l>
					<l n="6" num="2.2">Tes beaux yeux armés pour la trahison,</l>
					<l n="7" num="2.3">Et ton sein de neige, et le noir poison</l>
					<l n="8" num="2.4"><space quantity="12" unit="char"></space>Qu’a versé ta main !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Je les bénirai ! cher ange inhumain,</l>
					<l n="10" num="3.2">Fleurisse ta bouche au riant carmin !</l>
					<l n="11" num="3.3">Et toi, si ton pied le trouve en chemin,</l>
					<l n="12" num="3.4"><space quantity="12" unit="char"></space>Foule aux pieds mon cœur.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Oui, sers de complice au passant moqueur,</l>
					<l n="14" num="4.2">Et du noir oubli rhapsode vainqueur,</l>
					<l n="15" num="4.3">Mes vers frémissants chanteront en chœur</l>
					<l n="16" num="4.4"><space quantity="12" unit="char"></space>Ton nom adoré.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Jusqu’aux astres clairs je l’emporterai,</l>
					<l n="18" num="5.2">Et mon luth, peut-être un jour admiré,</l>
					<l n="19" num="5.3">Fera que l’éclat de ton front doré</l>
					<l n="20" num="5.4"><space quantity="12" unit="char"></space>Demeure immortel.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Puisse-t-il, flambeau de mon cher autel,</l>
					<l n="22" num="6.2">Éblouir de feu les divins sommets,</l>
					<l n="23" num="6.3">Et sur les piliers de saphir du ciel</l>
					<l n="24" num="6.4"><space quantity="12" unit="char"></space>Briller à jamais.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1861">Février 1861.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>