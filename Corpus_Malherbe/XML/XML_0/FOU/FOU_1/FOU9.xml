<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SINGESSE</head><div type="poem" key="FOU9">
					<head type="main">LA SINGESSE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									I cannot conceive you to be human creatures <lb></lb>
									but a sort of species hardly a degree above a <lb></lb>
									monkey ; who has more diverting tricks than <lb></lb>
									any of you, is an animal less mischievous and <lb></lb>
									expensive.
								</quote>
								<bibl>
									<name>SWIFT</name> (Letter to a very young lady).
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Donc voici ! Moi, Poète, en ma haute sagesse</l>
						<l n="2" num="1.2">Respuant l’Ève à qui le Père succomba</l>
						<l n="3" num="1.3">J’ai choisi pour l’aimer une jeune singesse</l>
						<l n="4" num="1.4">Au pays noir dans la forêt de Mayummba.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Fille des mandrills verts, ô guenuche d’Afrique,</l>
						<l n="6" num="2.2">Je te proclame ici la reine et la Vénus</l>
						<l n="7" num="2.3">Quadrumane, et je bous d’une ardeur hystérique</l>
						<l n="8" num="2.4">Pour les callosités qui bordent ton anus.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">J’aime ton cul pelé, tes rides, tes bajoues</l>
						<l n="10" num="3.2">Et je proclamerai devant maintes et maints,</l>
						<l n="11" num="3.3">Devant monsieur Reyer, mordieu ! que tu ne joues</l>
						<l n="12" num="3.4">Oncques du piano malgré tes quatre mains ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Et comme Salomon pour l’enfant sémitique,</l>
						<l n="14" num="4.2">La perle d’Issachar offerte au bien-aimé,</l>
						<l n="15" num="4.3">J’entonnerai pour toi l’énamouré cantique,</l>
						<l n="16" num="4.4">Ô ma tour de David, ô mon jardin fermé…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">C’était dans la forêt vierge sous les tropiques</l>
						<l n="18" num="5.2">Où s’ouvre en éventail le palmier chamœrops ;</l>
						<l n="19" num="5.3">Dans le soir alangui d’effluves priapiques</l>
						<l n="20" num="5.4">Stridait, rauque, le cri des nyctalomerops ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">L’heure glissait, nocturne, où gazelles, girafes,</l>
						<l n="22" num="6.2">Couaggas, éléphants, zèbres, zébus, springbocks <ref target="1" type="noteAnchor">1</ref></l>
						<l n="23" num="6.3">Vont boire aux zihouas sans verres ni carafes</l>
						<l n="24" num="6.4">Laissant l’homme pervers s’intoxiquer de bocks ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Sous les cactus en feu tout droits comme des cierges</l>
						<l n="26" num="7.2">Des lianes rampaient (nullement de Pougy) ;</l>
						<l n="27" num="7.3">Autant que la forêt ma Singesse était vierge ;</l>
						<l n="28" num="7.4">De son sang virginal l’humus était rougi.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Le premier, j’écartai ses lèvres de pucelle</l>
						<l n="30" num="8.2">En un rut triomphal, oublieux de Malthus,</l>
						<l n="31" num="8.3">Et des parfums salés montaient de son aisselle</l>
						<l n="32" num="8.4">Et des parfums pleuvaient des larysacanthus ;</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Elle se redressa, fière de sa blessure,</l>
						<l n="34" num="9.2">À demi souriante et confuse à demi ;</l>
						<l n="35" num="9.3">Le rugissement fou de notre jouissure</l>
						<l n="36" num="9.4">Arrachait au repos le chacal endormi.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Sept fois je la repris, lascive : son œil jaune</l>
						<l n="38" num="10.2">Clignottait, langoureux, tour à tour, et mutin ;</l>
						<l n="39" num="10.3">La Dryade amoureuse aux bras du jeune Faune</l>
						<l n="40" num="10.4">A moins d’amour en fleurs et d’esprit libertin !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Toi, Fille des humains, triste poupée humaine</l>
						<l n="42" num="11.2">Au ventre plein de son, tondeuse de Samson,</l>
						<l n="43" num="11.3">Dalila, Bovary, Marneffe ou Celimène,</l>
						<l n="44" num="11.4">Contemple mon épouse et retiens sa leçon :</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Mon épouse est loyale et très chaste et soumise</l>
						<l n="46" num="12.2">Et j’adore la voir, aux matins ingénus,</l>
						<l n="47" num="12.3">Le cœur sans artifice et le corps sans chemise,</l>
						<l n="48" num="12.4">Au soleil tropical, montrer ses charmes nus ;</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Elle sait me choisir ignames et goyaves ;</l>
						<l n="50" num="13.2">Lorsque nous cheminons par les sentiers étroits,</l>
						<l n="51" num="13.3">Ses mains aux doigts velus écartent les agaves,</l>
						<l n="52" num="13.4">Tel un page attentif marchant devant les rois,</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Puis dans ma chevelure oublieuse du peigne</l>
						<l n="54" num="14.2">Avec précaution elle cherche les poux</l>
						<l n="55" num="14.3">Satisfaite pourvu que d’un sourire daigne</l>
						<l n="56" num="14.4">La payer, une fois, le Seigneur et l’Époux.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Si quelque souvenir de souleur morte amasse</l>
						<l n="58" num="15.2">Des rides sur mon front que l’ennui foudroya,</l>
						<l n="59" num="15.3">Pour divertir son maître elle fait la grimace,</l>
						<l n="60" num="15.4">Grotesque et fantastique à délecter Goya !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Un étrange rictus tord sa narine bleue,</l>
						<l n="62" num="16.2">Elle se gratte d’un geste obscène et joli</l>
						<l n="63" num="16.3">La fesse puis s’accroche aux branches par la queue</l>
						<l n="64" num="16.4">En bondissant, Footit, Littl-Tich. Hanlon-Lee !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">Mais soudain la voilà très grave ! Sa mimique</l>
						<l n="66" num="17.2">Me dicte et je sais lire en ses regards profonds</l>
						<l n="67" num="17.3">Des vocables muets au sens métaphysique</l>
						<l n="68" num="17.4">Je comprends son langage et nous philosophons :</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">Elle croit en un Dieu par qui le soleil brille</l>
						<l n="70" num="18.2">Qui créa l’univers pour le bon chimpanzé</l>
						<l n="71" num="18.3">Puis dont le Fils-Unique, un jour s’est fait gorille</l>
						<l n="72" num="18.4">Pour ravir le pécheur à l’enfer embrasé !</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">Simiesque Iaveh de la forêt immense</l>
						<l n="74" num="19.2">Ô Zeus omnipotent de l’Animalité,</l>
						<l n="75" num="19.3">Fais germer en ses flancs et croître ma semence,</l>
						<l n="76" num="19.4">Ouvre son utérus à la maternité</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1">Car je veux voir issus de sa vulve féconde</l>
						<l n="78" num="20.2">Nos enfants libérés d’atavismes humains</l>
						<l n="79" num="20.3">Aux obroontchoas que la serpe n’émonde</l>
						<l n="80" num="20.4">Jamais en grimaçant grimper à quatre mains !…</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1">Et dans l’espoir sacré d’une progéniture</l>
						<l n="82" num="21.2">Sans lois, sans préjugés, sans rêves décevants</l>
						<l n="83" num="21.3">Nous offrons notre amour à la grande Nature,</l>
						<l n="84" num="21.4">Fiers comme les palmiers, libres comme les vents ! ! !</l>
					</lg>
					<closer>
						<note type="footnote" id="1">
							Etc., etc. <lb></lb>
							(Note de l’Auteur.)
						</note>
					</closer>
				</div></body></text></TEI>