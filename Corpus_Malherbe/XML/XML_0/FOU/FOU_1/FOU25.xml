<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CARNAVAL DE CHEFS-D’ŒUVRE</head><div type="poem" key="FOU25">
					<head type="main">À LA VÉNUS DE MILO</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
								Aux quinze-vingt le vieil Homère <lb></lb>
								et toi cascade, Hortense, ma fille !
								</quote>
								<bibl>
									<name>J. VALLÈS</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Idéal manchot des constipés architectes</l>
						<l n="2" num="1.2">sortis « <hi rend="ital">premier</hi> » de l’<hi rend="ital">École-des-Vilains-Arts</hi> ,</l>
						<l n="3" num="1.3">Paros mal retrouvé par les benêts hasards,</l>
						<l n="4" num="1.4">réduction colas pour mâcheurs de Pandectes ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">plâtre durci sur la tronche pleine d’insectes</l>
						<l n="6" num="2.2">de petits Italos ; rossignol des bazars ;</l>
						<l n="7" num="2.3">nulle en bizarre et bon nanan des vieux busards</l>
						<l n="8" num="2.4">chez Balandard sur le pendule où tu t’objectes ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Paganisme des quincailliers ! Bronze en toc ! zinc !</l>
						<l n="10" num="3.2">sache que les adorateurs de Lao-Tzeng,</l>
						<l n="11" num="3.3">ceux qu’un magot, poussah falot, séduit et botte,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1">ô mijaurée, ont renversé ton pied d’estal</l>
						<l n="13" num="4.2">et qu’ils ont mis dans un Panthéon de cristal</l>
						<l n="14" num="4.3">ta sœur négresse aux longs tétons, la Hottentote ! ! !</l>
					</lg>
				</div></body></text></TEI>