<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉPITRE FALOTE ET BALNÉAIRE</head><div type="poem" key="FOU19">
					<head type="main">ÉPITRE <lb></lb>FALOTE ET BALNÉAIRE</head>
					<opener>
						<salute>À JOSEPH SAVARY <lb></lb>dilettante bourguignon.</salute>
						<epigraph>
							<cit>
								<quote>
									Eau bienfaisante ! <lb></lb>
									Puissant secours <lb></lb>
									Qui nous exempte <lb></lb>
									De maux si lourds.
								</quote>
								<bibl>
									<name>A. POMMIER</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Savary, joyeux compagnon</l>
						<l n="2" num="1.2">Africain, Gascon, Bourguignon</l>
						<l n="3" num="1.3">Qui vis joyeux loin des Quarante</l>
						<l n="4" num="1.4">Au pays de ces nobles ducs</l>
						<l n="5" num="1.5">Qu’en ses bouquins un peu… caducs</l>
						<l n="6" num="1.6">Célébra Môssieur de Barante</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Bourguignon mais fils de Paris</l>
						<l n="8" num="2.2">Prince du rire et des houris</l>
						<l n="9" num="2.3">Contemnant le singe et le pître</l>
						<l n="10" num="2.4">Mon bon vieux, il me plaît, ce soir,</l>
						<l n="11" num="2.5">De t’envoyer, sans plus surseoir,</l>
						<l n="12" num="2.6">Une ode habillée en épître !…</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Donc, chaque jour plus avachi</l>
						<l n="14" num="3.2">Je me trimbale dans Vichy</l>
						<l n="15" num="3.3">Où des Messieurs jaunes d’ictère</l>
						<l n="16" num="3.4">Aux dames de même couleur</l>
						<l n="17" num="3.5">Exposent les phases de leur</l>
						<l n="18" num="3.6">Goutte (civile ou militaire !)</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">De Guérêt, de Poulocondor,</l>
						<l n="20" num="4.2">Du Brésil où vit le condor,</l>
						<l n="21" num="4.3">Ducs, fabricants de margarines,</l>
						<l n="22" num="4.4">Cabotins, bourgeois saugrenus,</l>
						<l n="23" num="4.5">Comme une trombe, ils sont venus</l>
						<l n="24" num="4.6">Faire analyser leur urines.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Il en vient de Costarica,</l>
						<l n="26" num="5.2">Des bords du lac Titicaca,</l>
						<l n="27" num="5.3">De Pontoise et de Pampelune</l>
						<l n="28" num="5.4">Et de Bucharest et de Brest</l>
						<l n="29" num="5.5">Et je veux n’être plus Fourest</l>
						<l n="30" num="5.6">S’il n’en tombe aussi de la Lune !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Barons juifs entasseurs d’écus,</l>
						<l n="32" num="6.2">Épiciers chauves et cocus</l>
						<l n="33" num="6.3">Et généraux de Bolivie</l>
						<l n="34" num="6.4">Ostentent d’un air convaincu</l>
						<l n="35" num="6.5">Leur bedaine et leur trou du cul</l>
						<l n="36" num="6.6">Aux doucheurs dont l’âme est ravie.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Les uns, dolents du pancréas</l>
						<l n="38" num="7.2">Rimeraient à « Jean Moréas »</l>
						<l n="39" num="7.3">D’autres (Larbaud leur soit propice) !</l>
						<l n="40" num="7.4">Ayant du sucre en leur pipi</l>
						<l n="41" num="7.5">Semblent moins des pommes d’api</l>
						<l n="42" num="7.6">Que des morceaux de pain d’épice.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Le soir, au casino, des tas</l>
						<l n="44" num="8.2">De Mercadets et de rastas</l>
						<l n="45" num="8.3">Ouvrent la banque ou l’on trébuche</l>
						<l n="46" num="8.4">Rubis aux doigts, gilet trop neuf,</l>
						<l n="47" num="8.5">Ils savent l’art d’abattre <hi rend="ital">Neuf</hi></l>
						<l n="48" num="8.6">En donnant au ponte une bûche !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Cependant que des avocats</l>
						<l n="50" num="9.2">Croassant comme des choucas</l>
						<l n="51" num="9.3">Mènent au concert leurs femelles</l>
						<l n="52" num="9.4">Dont le… bas-fond saigne encor du</l>
						<l n="53" num="9.5">Terrible effort d’avoir pondu</l>
						<l n="54" num="9.6">Quinze mômes affreux comme elles !</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">Or ce que peut œuvrer, parmi</l>
						<l n="56" num="10.2">Tous ces Pécuchets, ton ami</l>
						<l n="57" num="10.3">Dis-moi, vieux frangin, que t’en semble ?</l>
						<l n="58" num="10.4">Sinon rêver aux jours (lointains</l>
						<l n="59" num="10.5">Hélas !) où les doux Philistins</l>
						<l n="60" num="10.6">Dans Paris nous verront ensemble ?</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">Ah ! ces beaux jours quand luiront-ils</l>
						<l n="62" num="11.2">Où, tenant des propos subtils,</l>
						<l n="63" num="11.3">Aux bourgeois taillant des croupières,</l>
						<l n="64" num="11.4">Nous jetterons au nez d’Homais</l>
						<l n="65" num="11.5">Nos rimes d’or sans que jamais</l>
						<l n="66" num="11.6">S’appesantissent nos paupières !</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">Car il sied ne parler qu’en vers :</l>
						<l n="68" num="12.2">Comme un digne bourgeois d’Anvers</l>
						<l n="69" num="12.3">Soigne une tulipe et l’arrose,</l>
						<l n="70" num="12.4">Nobles jardiniers, cultivons</l>
						<l n="71" num="12.5">La fleur mystique et réservons</l>
						<l n="72" num="12.6">Aux maraîchers la vile prose ?</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1">Des vers ! des vers ! et c’est pourquoi</l>
						<l n="74" num="13.2">Si tu veux qu’on te laisse coi</l>
						<l n="75" num="13.3">Siroter près d’une crédence</l>
						<l n="76" num="13.4">Ton vieux Beaune sache qu’il faut,</l>
						<l n="77" num="13.5">Sans rémission ni défaut</l>
						<l n="78" num="13.6">Épistoler et d’abondance !…</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1">Et puis, t’ayant serré la main,</l>
						<l n="80" num="14.2">Je vais ronfler jusqu’à demain :</l>
						<l n="81" num="14.3">Le ciel, en son omnipotence,</l>
						<l n="82" num="14.4">Nous inspirant maint beau sonnet</l>
						<l n="83" num="14.5">Toujours nous préserve d’Ohnet,</l>
						<l n="84" num="14.6">De la grippe et de la potence !</l>
					</lg>
				</div></body></text></TEI>