<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’ÉTERNELLE CHANSON</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>192 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/henribeauclairleshorizontales.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>L’Éternelle chanson</title>
						<author>Henri Beauclair</author>
						<imprint>
							<publisher>Léon Vanier,Paris</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA1">
				<head type="number">I</head>
				<head type="main">Billet doux</head>
				<lg n="1">
					<l n="1" num="1.1">Blondine, j’ai perdu mon cœur,</l>
					<l n="2" num="1.2">En vous reconduisant, Dimanche.</l>
					<l n="3" num="1.3">Connaissez-vous pas l’escroqueur ?</l>
					<l n="4" num="1.4">Blondine, j’ai perdu mon cœur.</l>
					<l n="5" num="1.5">Je vous vois d’ici, l’air moqueur,</l>
					<l n="6" num="1.6">Me dire : Il n’est pas dans ma manche.</l>
					<l n="7" num="1.7">Blondine, j’ai perdu mon cœur,</l>
					<l n="8" num="1.8">En vous reconduisant, Dimanche.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Serait-il pas entré chez vous ?</l>
					<l n="10" num="2.2">Très fortement je l’en soupçonne.</l>
					<l n="11" num="2.3">Il a parfois des pensers fous ;</l>
					<l n="12" num="2.4">Serait-il pas entré chez vous ?</l>
					<l n="13" num="2.5">Ne vous mettez pas en courroux</l>
					<l n="14" num="2.6">Je n’accuse encore personne.</l>
					<l n="15" num="2.7">Serait-il pas entré chez vous ?</l>
					<l n="16" num="2.8">Très fortement je l’en soupçonne.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="17" num="3.1">Il est comme les papillons</l>
					<l n="18" num="3.2">Qui vont toujours vers les lumières,</l>
					<l n="19" num="3.3">Quitte à brûler tous leurs paillons.</l>
					<l n="20" num="3.4">Il est comme les papillons ;</l>
					<l n="21" num="3.5">Or, il a dû voir les rayons</l>
					<l n="22" num="3.6">Qui s’échappent de vos paupières.</l>
					<l n="23" num="3.7">Il est comme les papillons</l>
					<l n="24" num="3.8">Qui vont toujours vers les lumières.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">A-t-il eu tort ou bien raison</l>
					<l n="26" num="4.2">De risquer un tel coup d’audace ?</l>
					<l n="27" num="4.3">Dites le moi, Lise, Lison,</l>
					<l n="28" num="4.4">A-t-il eu tort ou bien raison.</l>
					<l n="29" num="4.5">Gardez le dans votre maison,</l>
					<l n="30" num="4.6">Baste ! il tiendra si peu de place.</l>
					<l n="31" num="4.7">A-t-il eu tort ou bien raison</l>
					<l n="32" num="4.8">De risquer un tel coup d’audace ?</l>
				</lg>
			</div></body></text></TEI>