<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HORIZONTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 — 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/horizont.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Horizontales</title>
								<author>Henri Beauclair</author>
								<edition>2e édition en partie originale</edition>
								<imprint>
									<publisher>Léon Vanier, Paris</publisher>
									<date when="1886">1886</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA16">
				<head type="main">Violées et Inviolables</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Canaris ! Canaris ! Pleure !</quote>
							<bibl>
								<name>V. HUGO</name>, <hi rend="ital">Orientales</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Doux pays où l’on sait la valeur du billon,</l>
					<l n="2" num="1.2">O Toi qui te nommas : la pudique Albion,</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space>Angleterre, chaste Angleterre,</l>
					<l n="4" num="1.4">Qui l’eût cru ? Du palais jusques à l’atelier,</l>
					<l n="5" num="1.5">Chacun de tes toits cèle une «maison Tellier ?»</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space>Cette nouvelle étrangle, atterre !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">O Shocking ! qui l’a dit ? qui donc a révélé</l>
					<l n="8" num="2.2">Tes dessous ? Nous croyions, naïfs, au rêve ailé</l>
					<l n="9" num="2.3"><space unit="char" quantity="8"></space>De la miss à longues chaussures !</l>
					<l n="10" num="2.4">De même à la candeur du Gentleman rider</l>
					<l n="11" num="2.5">Et nous ne pensions pas suspecte sa raideur ;</l>
					<l n="12" num="2.6"><space unit="char" quantity="8"></space>A-t-on bien dit des choses sûres ?</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Le doute, hélas ! n’est pas possible, le Pall-Mall</l>
					<l n="14" num="3.2">Gazette nous apprend que l’on a mis à mal</l>
					<l n="15" num="3.3"><space unit="char" quantity="8"></space>Des très mineures innombrables !</l>
					<l n="16" num="3.4">«C’est du joli, dirait Gavroche ! Zut alors !</l>
					<l n="17" num="3.5">«Et ta sœur ?» Et sa sœur fait le bonheur des lords</l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space>Et d’un groupe d’inviolables !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">O pays des John Brown, des royaux Philémons,</l>
					<l n="20" num="4.2">Salut ! Et nous qui, loin d’en faire fi, l’aimons</l>
					<l n="21" num="4.3"><space unit="char" quantity="8"></space>L’impérissable bagatelle,</l>
					<l n="22" num="4.4">Nous sommes enfoncés, nous le reconnaissons</l>
					<l n="23" num="4.5">L’Anglais, toujours boxeur, s’est écrié : boxons !</l>
					<l n="24" num="4.6"><space unit="char" quantity="8"></space>Il n’a pas dit : «Quelle âge a-t-elle ?»</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Non ! la vierge est à tous ! les vieux, les laids, les beaux,</l>
					<l n="26" num="5.2">O veinards ! vous avez dans votre île : Lesbos</l>
					<l n="27" num="5.3"><space unit="char" quantity="8"></space>Sodome, Gomorrhe et Cythère !</l>
					<l n="28" num="5.4">Pleure ! Pleure ! ô Paris ! la pudique Albion</l>
					<l n="29" num="5.5">T’enlève la couronne et te dame le pion !</l>
					<l n="30" num="5.6"><space unit="char" quantity="8"></space>Hip ! hip ! hourrah pour l’Angleterre !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1885">Juillet 1885.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>