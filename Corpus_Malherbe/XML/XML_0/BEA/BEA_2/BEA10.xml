<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HORIZONTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 — 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/horizont.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Horizontales</title>
								<author>Henri Beauclair</author>
								<edition>2e édition en partie originale</edition>
								<imprint>
									<publisher>Léon Vanier, Paris</publisher>
									<date when="1886">1886</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA10">
				<head type="number">IV</head>
				<head type="main">La Fortune perdue</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Allah ! qui me rendra ma redoutable armée !</quote>
							<bibl>
								<name>V. HUGO</name>, <hi rend="ital">Orientales</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Vénus ! qui me rendra ma grande renommée,</l>
					<l n="2" num="1.2">Ma chevelure d’or et ma taille d’almée ?</l>
					<l n="3" num="1.3">Mon hôtel et ma chambre éblouissante à voir,</l>
					<l n="4" num="1.4">Où, la nuit, s’allumaient des feux au fond de l’ombre,</l>
					<l n="5" num="1.5">Où, de ducs et de rois vint défiler un nombre</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space>Que moi-même ne puis savoir ?</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Qui me rendra mes grooms aux splendides livrées ?</l>
					<l n="8" num="2.2">Et mes laquais, couverts de pelisses fourrées,</l>
					<l n="9" num="2.3">Mes cochers, galonnés comme des généraux ?</l>
					<l n="10" num="2.4">Mes marmitons, sortis des fameuses cuisines,</l>
					<l n="11" num="2.5">Dont les bisques et les salmis de bécassines</l>
					<l n="12" num="2.6">Relevaient le courage abattu des héros ?</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Tous ces vaillants, à l’œil de flamme, à l’âme forte,</l>
					<l n="14" num="3.2">Qui, chacun à son tour, avaient franchi ma porte,</l>
					<l n="15" num="3.3">Quoi ? je ne verrai plus en persillant au Bois</l>
					<l n="16" num="3.4">Leurs troupes, par le temps, hélas ! diminuées,</l>
					<l n="17" num="3.5">Derrière mon landau s’ébattre par nuées,</l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space>A l’épatement des bourgeois !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Les voilà tous partis, leurs cœurs brûlent pour d’autres.</l>
					<l n="20" num="4.2">Tous, pendant quarante ans, firent les bons apôtres,</l>
					<l n="21" num="4.3">Achetant par de l’or le droit de m’approcher !</l>
					<l n="22" num="4.4">Tous partis ! Les bijoux ont pris la même route,</l>
					<l n="23" num="4.5">Ma beauté, mes appas ! Hélas ! quelle déroute !</l>
					<l n="24" num="4.6">Vénus ! je n’ai plus même un lit où me coucher !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Vénus ! qui me rendra ma grande renommée ?</l>
					<l n="26" num="5.2">Ma chevelure d’or est blanche et clairsemée ;</l>
					<l n="27" num="5.3">Je n’ai plus de logis et suis sur le pavé !</l>
					<l n="28" num="5.4">Quoi ? soupirants, amants, des quatre coins du monde.</l>
					<l n="29" num="5.5">Leurs présents, leurs amours, ô misère profonde !</l>
					<l n="30" num="5.6"><space unit="char" quantity="8"></space>C’est comme si j’avais rêvé !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Ainsi parlait Cora le soir de sa défaite.</l>
					<l n="32" num="6.2">Elle n’était vraiment pas du tout à la fête,</l>
					<l n="33" num="6.3">Pearl et des pleurs perlaient dans ses yeux meurtriers. ( ? ?)</l>
					<l n="34" num="6.4">Rêveuse, elle songeait au retour de Cythère.</l>
					<l n="35" num="6.5">Près d’elle, son bidet du pied frappait la terre,</l>
					<l n="36" num="6.6">Un bidet maigre et nu, dépourvu d’étriers !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">Août 1883.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>