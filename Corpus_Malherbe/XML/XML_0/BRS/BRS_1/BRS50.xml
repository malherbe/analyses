<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE SILENCE DES PAUVRES</head><div type="poem" key="BRS50">
					<head type="main">SAINTE MADELEINE INUTILE</head>
					<opener>
						<salute>à une petite statue</salute>
						<epigraph>
							<cit>
								<quote>
									Dans la niche en pierre tranquille…
								</quote>
							</cit>
						</epigraph>
					</opener>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Dans le coin où le soir t’oublie,</l>
							<l n="2" num="1.2">Tu ne sens pas le vieil amour</l>
							<l n="3" num="1.3">Qu’a mis sur ta tête pâlie</l>
							<l n="4" num="1.4">La fête ignorante du jour.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Dans le repos où tu reposes,</l>
							<l n="6" num="2.2">Tu ne sens pas, ô grave sœur,</l>
							<l n="7" num="2.3">Cette clarté des vagues choses</l>
							<l n="8" num="2.4">Qui l’illumine avec douceur…</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Voici le soir. Toujours semblable</l>
							<l n="10" num="3.2">Sur le seuil d’or des jours d’été,</l>
							<l n="11" num="3.3">En vain le soleil adorable</l>
							<l n="12" num="3.4">T’a fait un peu de charité.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Tu n’es pas plus douce — plus sombre,</l>
							<l n="14" num="4.2">Pourtant, beaucoup étaient venus</l>
							<l n="15" num="4.3">Pour t’écouter pleurer de l’ombre</l>
							<l n="16" num="4.4">Et se troubler de tes doigts nus.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Tu n’as même pas sur la pierre,</l>
							<l n="18" num="5.2">Pendant un vague et triste instant,</l>
							<l n="19" num="5.3">Souri comme — un peu de lumière</l>
							<l n="20" num="5.4">À ces pauvres voix qu’on entend.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="21" num="1.1">Tu lèves sans douleur, sans joie</l>
							<l n="22" num="1.2">Tes yeux où le soleil se perd,</l>
							<l n="23" num="1.3">Tes mains où notre amour se noie</l>
							<l n="24" num="1.4">Comme un bon frisson dans la mer.</l>
						</lg>
						<lg n="2">
							<l n="25" num="2.1">Oh ! pendant que, morne débâcle,</l>
							<l n="26" num="2.2">Nous passons dans un morne bruit,</l>
							<l n="27" num="2.3">Si tu faisais le doux miracle</l>
							<l n="28" num="2.4">D’avoir un peu froid dans la nuit…</l>
						</lg>
						<lg n="3">
							<l n="29" num="3.1">Si trop calme et belle sans trêve</l>
							<l n="30" num="3.2">Aux beaux silences étoilés,</l>
							<l n="31" num="3.3">Tes yeux s’appauvrissaient du rêve</l>
							<l n="32" num="3.4">Dont nos regards sont désolés…</l>
						</lg>
						<lg n="4">
							<l n="33" num="4.1">On viendrait te voir, simple et chère,</l>
							<l n="34" num="4.2">Indécise comme un secret,</l>
							<l n="35" num="4.3">Avec la robe de prière</l>
							<l n="36" num="4.4">Qu’un humble cierge te ferait.</l>
						</lg>
						<lg n="5">
							<l n="37" num="5.1">Tu serais ce qui fait renaître</l>
							<l n="38" num="5.2">L’âme heureuse, le songe éteint…</l>
							<l n="39" num="5.3">Et ton chemin serait peut-être</l>
							<l n="40" num="5.4">La caresse de mon destin.</l>
						</lg>
						<lg n="6">
							<l n="41" num="6.1">Tu serais l’amour, et l’enfance…</l>
							<l n="42" num="6.2">Et pourtant, toi qui ne dis rien,</l>
							<l n="43" num="6.3">Et moi qui souris de souffrance,</l>
							<l n="44" num="6.4">Je sens que ton silence est bien.</l>
						</lg>
						<lg n="7">
							<l n="45" num="7.1">Je t’adore dans ton grand règne,</l>
							<l n="46" num="7.2">Et dans l’espace sans amours.</l>
							<l n="47" num="7.3">Tu ne dis rien comme l’on saigne,</l>
							<l n="48" num="7.4">Et te voir, c’est pleurer toujours.</l>
						</lg>
						<lg n="8">
							<l n="49" num="8.1">Oh ! sans raison, sans mal, sans crimes,</l>
							<l n="50" num="8.2">Et sans remords au fond de moi,</l>
							<l n="51" num="8.3">Je voudrais à tes pieds sublimes</l>
							<l n="52" num="8.4">Pleurer que la lumière soit !</l>
						</lg>
						<lg n="9">
							<l n="53" num="9.1">Pleurer que le jour s’irradie</l>
							<l n="54" num="9.2">Que la nuit brûle dans les cieux,</l>
							<l n="55" num="9.3">Pleurer tout ce pauvre incendie</l>
							<l n="56" num="9.4">Qui monte humblement dans nos yeux…</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="57" num="1.1">Tandis que les vieilles aux « simples »</l>
							<l n="58" num="1.2">Marmonnent un air enchanté,</l>
							<l n="59" num="1.3">Tu regardes de tes yeux simples</l>
							<l n="60" num="1.4">Le monde de simplicité.</l>
						</lg>
						<lg n="2">
							<l n="61" num="2.1">Et tandis que sans espérance</l>
							<l n="62" num="2.2">Nous passons et ne parlons pas,</l>
							<l n="63" num="2.3">Tu comprends avec ton silence</l>
							<l n="64" num="2.4">La prière que font nos pas.</l>
						</lg>
						<lg n="3">
							<l n="65" num="3.1">Tu regardes, toujours la même,</l>
							<l n="66" num="3.2">Tu souris, comme ton ciel bleu,</l>
							<l n="67" num="3.3">Et quand on crie ou qu’on blasphème,</l>
							<l n="68" num="3.4">Tu laisses dire, comme Dieu.</l>
						</lg>
					</div>
				</div></body></text></TEI>