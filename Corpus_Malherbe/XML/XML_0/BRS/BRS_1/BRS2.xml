<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MESSE DU PASSÉ</head><div type="poem" key="BRS2">
					<head type="main">TABLEAUX</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Chaque parole est un sourire.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Au fond du vieux salon où le bal se précise,</l>
							<l n="2" num="1.2">Les traînes de satin, couleur de demi-jour,</l>
							<l n="3" num="1.3">Suivent avec lenteur la musique indécise.</l>
							<l n="4" num="1.4">Au fond du vieux salon, au fond de tant de jours,</l>
							<l n="5" num="1.5">Sur les danseurs errants et les formes assises,</l>
							<l n="6" num="1.6">Tous les reflets du ciel habillent les atours.</l>
							<l n="7" num="1.7">L’aile des éventails est prise, et tremble, lasse.</l>
							<l n="8" num="1.8">Un doux soleil fleurit, captif jusqu’au matin.</l>
							<l n="9" num="1.9">La danse éparpillée affronte en vain l’espace,</l>
							<l n="10" num="1.10">Elle obéit sans cesse, et retombe sans fin.</l>
							<l n="11" num="1.11">Toute la vie enclose entre les feux des glaces</l>
							<l n="12" num="1.12">Voudrait s’enfuir, et reste là, comme un jardin.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="13" num="1.1">J’ouvre les yeux, lassé par la très longue veille ;</l>
							<l n="14" num="1.2">C’est la chambre dolente et l’ombre dans le coin,</l>
							<l n="15" num="1.3">Et la voix de l’horloge à voix toujours pareille.</l>
							<l n="16" num="1.4">La fenêtre confuse éclaire par un joint</l>
							<l n="17" num="1.5">D’une mince lueur le plafond qui sommeille ;</l>
							<l n="18" num="1.6">Dans la rue, une voix se lamente très loin.</l>
							<l n="19" num="1.7">La paix des grands rideaux où l’âme tiède est prise</l>
							<l n="20" num="1.8">Garde ses longs plis morts sur mon repos très lourd,</l>
							<l n="21" num="1.9">Et mon demi-sommeil rêve dans l’heure grise…</l>
							<l n="22" num="1.10">J’entends des bruits craintifs dans la maison, autour</l>
							<l n="23" num="1.11">Elle approche à pas doux pour n’être pas surprise,</l>
							<l n="24" num="1.12">Et par la porte blanche elle entre avec le jour.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="25" num="1.1">Aux sentiers où je vais mon pas triste résonne.</l>
							<l n="26" num="1.2">Nous nous sommes quittés ; il fait froid, il a plu ;</l>
							<l n="27" num="1.3">Je viens dans le grand parc où ne vient plus personne…</l>
							<l n="28" num="1.4">Nous nous sommes quittés, puisque tu l’as voulu.</l>
							<l n="29" num="1.5">Ô pauvre cœur désert où trop de vent frissonne,</l>
							<l n="30" num="1.6">Ô pauvre cœur creusé de l’automne, salut !</l>
							<l n="31" num="1.7">Le silence et l’absence ouvrent la forêt nue,</l>
							<l n="32" num="1.8">La feuilles gît, légère et lourde, en désarroi,</l>
							<l n="33" num="1.9">Je pense aux chemins clairs où ta grâce est venue !</l>
							<l n="34" num="1.10">Et le ciel s’assombrit lentement, il fait froid,</l>
							<l n="35" num="1.11">Mon âme douloureuse erre dans l’avenue</l>
							<l n="36" num="1.12">Et la grande nature est plus triste que moi.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<lg n="1">
							<l n="37" num="1.1">Au bord de la fontaine où je vais à pas lents,</l>
							<l n="38" num="1.2">La statue, au milieu de la pénombre, écoute</l>
							<l n="39" num="1.3">Le murmure de l’eau qui baigne ses pieds blancs</l>
							<l n="40" num="1.4">Et l’on perçoit au loin sous l’ombre de la voûte</l>
							<l n="41" num="1.5">Et le deuil transpercé des grands rameaux dolents</l>
							<l n="42" num="1.6">La fontaine qui tremble et pleure goutte à goutte.</l>
							<l n="43" num="1.7">Oh ! tout est plein ici des pudeurs de l’adieu.</l>
							<l n="44" num="1.8">Un frisson morne court dans la forêt pâlie…</l>
							<l n="45" num="1.9">On croit voir en la nuit comme en un jour plus bleu,</l>
							<l n="46" num="1.10">La sainte qui venait, si triste et si jolie</l>
							<l n="47" num="1.11">Vers la clairière astrale où tout veillait un peu,</l>
							<l n="48" num="1.12">Avec son luxe d’ombre et de mélancolie…</l>
						</lg>
					</div>
				</div></body></text></TEI>