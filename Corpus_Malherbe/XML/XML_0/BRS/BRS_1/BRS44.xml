<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA HAINE</head><div type="poem" key="BRS44">
					<head type="main">APPARITION</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Hélas, si nous savions la fin de la journée.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Quand la chute du soir, grande tempête nue,</l>
						<l n="2" num="1.2">Dépouille les maisons au bord de l’avenue,</l>
						<l n="3" num="1.3">Quand dans la chambre faible, à l’heure sans abri,</l>
						<l n="4" num="1.4">Le fond du cœur est vague et désert comme un cri.</l>
						<l n="5" num="1.5">Quand la rumeur se tait laissant dormir tranquilles</l>
						<l n="6" num="1.6">Les grands rêves lassés comme les grandes villes</l>
						<l n="7" num="1.7">Et que tout front en deuil s’incline dans un coin,</l>
						<l n="8" num="1.8">Je te vois t’ébaucher, rêve qui viens de loin.</l>
						<l n="9" num="1.9">Et le blême décor, lorsque sur tes vieux charmes,</l>
						<l n="10" num="1.10">Tes voiles, on dirait, tombent comme des larmes,</l>
						<l n="11" num="1.11">C’est le jour malheureux, c’est le jour de longueur,</l>
						<l n="12" num="1.12">C’est le jour et le soir, pauvres frères sans cœur !</l>
						<l n="13" num="1.13">Ton front lent et brouillé n’est plus qu’un blanc vestige.</l>
						<l n="14" num="1.14">Ton œil n’est plus que triste ainsi qu’un vieux vertige,</l>
						<l n="15" num="1.15">Et sur ta lèvre pâle à l’ancien pli moqueur</l>
						<l n="16" num="1.16">S’entr’ouvre doucement le sanglot de ton cœur…</l>
						<l n="17" num="1.17">Et je vois la douleur qui vit sous ta paupière</l>
						<l n="18" num="1.18">Tomber de tes grands yeux comme un peu de lumière</l>
						<l n="19" num="1.19">Tu viens, très malheureuse, au foyer qui fut tien,</l>
						<l n="20" num="1.20">Tu me tends vaguement ta main qui ne peut rien</l>
						<l n="21" num="1.21">Et dans les yeux ternis à peine l’on devine</l>
						<l n="22" num="1.22">Le fragile rayon dont ma lampe est divine.</l>
						<l n="23" num="1.23">Puis tu t’en vas toujours, souffrance du dehors.</l>
					</lg>
					<lg n="2">
						<l n="24" num="2.1">Douleur pâle du ciel dont, tous les jours sont morts,</l>
						<l n="25" num="2.2">Angoisse du passé toujours inassouvie,</l>
						<l n="26" num="2.3">Reste, douce et paisible, au grand seuil de ma vie</l>
						<l n="27" num="2.4">En remuant ton voile avec tes doigts tremblants :</l>
						<l n="28" num="2.5">Reste douce et paisible avec tes cheveux blancs.</l>
					</lg>
				</div></body></text></TEI>