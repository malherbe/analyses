<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA HAINE</head><div type="poem" key="BRS45">
					<head type="main">LE SOMMEIL</head>
					<lg n="1">
						<l n="1" num="1.1">Tandis qu’au milieu du silence</l>
						<l n="2" num="1.2">Tu t’endors sous le rideau noir,</l>
						<l n="3" num="1.3">Je sens éclore ton absence</l>
						<l n="4" num="1.4">Dans la grande chambre du soir.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Quelle immense pitié se lève,</l>
						<l n="6" num="2.2">Pauvre ange aux yeux clos, à te voir</l>
						<l n="7" num="2.3">Lorsque tu dors, blanche de rêve,</l>
						<l n="8" num="2.4">Auprès de moi triste du soir.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Tu ne me connais plus, ma reine,</l>
						<l n="10" num="3.2">Tout entière à l’espoir tremblant</l>
						<l n="11" num="3.3">Ta petite main tient à peine</l>
						<l n="12" num="3.4">La douce vie et le drap blanc.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Et je reste seul, et je pense</l>
						<l n="14" num="4.2">Que tu rêves bien loin du jour,</l>
						<l n="15" num="4.3">Et que ton repos est immense</l>
						<l n="16" num="4.4">Et divin comme notre amour !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Nous en avons comme un présage</l>
						<l n="18" num="5.2">Au fond des soirs mystérieux,</l>
						<l n="19" num="5.3">Lorsque notre âme fait naufrage</l>
						<l n="20" num="5.4">Dans la fatigue de nos yeux.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Je suis seul parmi toutes choses,</l>
						<l n="22" num="6.2">Hélas, tout se tait devant moi,</l>
						<l n="23" num="6.3">Et ta figure aux lèvres closes</l>
						<l n="24" num="6.4">Est comme un souvenir de toi.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Je te vois tranquille et sans geste,</l>
						<l n="26" num="7.2">Ton sourire s’est effacé…</l>
						<l n="27" num="7.3">On dirait l’adieu qui vous reste</l>
						<l n="28" num="7.4">Quand on est seul dans le passé.</l>
					</lg>
				</div></body></text></TEI>