<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRS1">
				<head type="main">LA PLEUREUSE</head>
				<lg n="1">
					<l n="1" num="1.1">Oh bien des fois, au gré du rêve où tu te penches,</l>
					<l n="2" num="1.2">Tu vis le hameau calme avec ses maisons blanches,</l>
					<l n="3" num="1.3">Et la paix de l’azur a fait pleurer ta paix.</l>
					<l n="4" num="1.4">Et bien des fois, la nuit, lorsque tu regardais,</l>
					<l n="5" num="1.5">J’ai senti ta douleur monter jusqu’aux étoiles,</l>
					<l n="6" num="1.6">Et te vis épier dans l’ombre aux ombres pâles</l>
					<l n="7" num="1.7">Cet immense malheur qu’on ne peut pas savoir…</l>
					<l n="8" num="1.8">Lorsque nous regardons monter la mer du soir,</l>
					<l n="9" num="1.9">Ainsi que deux faux dieux sur les mornes rivages,</l>
					<l n="10" num="1.10">Nous voyons devant nous passer de grands veuvages</l>
					<l n="11" num="1.11">Et c’est ton désespoir qui souffre avec douceur.</l>
					<l n="12" num="1.12">Désert de ton frisson, pauvreté de ton cœur !</l>
				</lg>
				<lg n="2">
					<l n="13" num="2.1">Et tu vas inquiète, et très calme et très seule,</l>
					<l n="14" num="2.2">Ô si jeune âme avec des mains comme une aïeule,</l>
					<l n="15" num="2.3">Toi qui, pauvre rêveuse, avais aux temps lointains</l>
					<l n="16" num="2.4">Dans les nuits de bonheur des songes enfantins,</l>
					<l n="17" num="2.5">Qui, bercée à la voix d’aurore qui se lève</l>
					<l n="18" num="2.6">Et souriante encor d’une écharpe de rêve,</l>
					<l n="19" num="2.7">Dans le ciel du matin n’as trouvé que l’azur !</l>
					<l n="20" num="2.8">Si le dieu de cœur simple est le seul dieu très pur,</l>
					<l n="21" num="2.9">Pleure la grande vie et tout ce que vous faites,</l>
					<l n="22" num="2.10">Ô vous qui souriez, ô ceux que tu rachètes</l>
					<l n="23" num="2.11">Quand lasse, dans les champs d’étés et de sommeil,</l>
					<l n="24" num="2.12">Tu sens se dévaster la pitié du soleil !</l>
				</lg>
				<lg n="3">
					<l n="25" num="3.1">Et je te dis souvent que nous sommes sublimes</l>
					<l n="26" num="3.2">Et qu’il est un mystère, et que nous l’entendîmes ;</l>
					<l n="27" num="3.3">Et je te dis cela quand nous nous effleurons,</l>
					<l n="28" num="3.4">Quand le demi-sommeil laisse errer nos deux fronts</l>
					<l n="29" num="3.5">Et que la lampe est douce au fond de l’âme close…</l>
					<l n="30" num="3.6">Et sans me regarder, tu pleures d’autre chose.</l>
				</lg>
			</div></body></text></TEI>