<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MESSE DU PASSÉ</head><div type="poem" key="BRS8">
					<head type="main">NOUS NOUS SOMMES REVUS</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Le silence est un pardon <lb></lb>
									Plus triste.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Nous avons eu le jour et le matin livide</l>
						<l n="2" num="1.2">Et le rêve éternel que nous rêvions en vain…</l>
						<l n="3" num="1.3">Nous avons eu la vie avec sa place vide</l>
						<l n="4" num="1.4">Et le large soleil sans parole et sans pain !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Nous avons eu la paix de toutes les journées ;</l>
						<l n="6" num="2.2">Les rêves de voix basse et les repos trop lourds.</l>
						<l n="7" num="2.3">Et nous nous en allons avec nos destinées</l>
						<l n="8" num="2.4">Et nos yeux désolés se chercheront toujours.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Oh ! que tu dois souffrir tandis que l’ombre rampe,</l>
						<l n="10" num="3.2">Que la chambre s’emplit de la pâleur des cieux,</l>
						<l n="11" num="3.3">Que le soir indolent en attendant la lampe</l>
						<l n="12" num="3.4">Fais toute attente grise auprès des rideaux vieux.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Que t’importe à présent l’espoir crépusculaire,</l>
						<l n="14" num="4.2">Assise avec le soir, douce sainte d’amour.</l>
						<l n="15" num="4.3">Oh ! tu ne songeais plus à lever ta paupière</l>
						<l n="16" num="4.4">Vers le côté divin d’où tombe un peu de jour.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Passons, passons toujours, errons où nous errâmes</l>
						<l n="18" num="5.2">Et regardons l’espace à nos yeux étendus,</l>
						<l n="19" num="5.3">Pauvres gens isolés dans le parc, pauvres âmes</l>
						<l n="20" num="5.4">Qui voulions retrouver le paradis perdu !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Tout est mort, tout est mort, l’azur et l’innocence,</l>
						<l n="22" num="6.2">Et ce que veille l’ombre et ce qui nous attend,</l>
						<l n="23" num="6.3">Et tout ce qu’on bénit quand on passe en silence</l>
						<l n="24" num="6.4">Et tout ce qu’on écoute et tout ce qu’on entend.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Parcourons le vieux parc qui jadis fut le nôtre,</l>
						<l n="26" num="7.2">Le parc de vieux étangs, de feuilles et d’amours,</l>
						<l n="27" num="7.3">Marchons désespérés et très doux l’un à l’autre…</l>
						<l n="28" num="7.4">Oh ! la vie, oh ! le mal de s’en aller toujours !…</l>
					</lg>
				</div></body></text></TEI>