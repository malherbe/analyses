<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES CHOSES</head><div type="poem" key="BRS27">
					<head type="main">TERCETS</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									La fenêtre m’attriste, ce soir.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Au pays des champs bleus et des choses heureuses,</l>
							<l n="2" num="1.2">Allez, ô mes oiseaux, dans le soir qui s’endort</l>
							<l n="3" num="1.3">Sur vos cœurs désolés et vos ailes peureuses.</l>
						</lg>
						<lg n="2">
							<l n="4" num="2.1">Le chagrin de la vie est doux comme la mort…</l>
							<l n="5" num="2.2">J’entends pleurer les chants et les rumeurs fiévreuses</l>
							<l n="6" num="2.3">De la vieille cité debout dans le ciel d’or !</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1">J’ai regardé longtemps dans la même attitude</l>
							<l n="8" num="3.2">La chambre sans couleur où mon cœur est resté,</l>
							<l n="9" num="3.3">Lourd de son long silence et de sa solitude.</l>
						</lg>
						<lg n="4">
							<l n="10" num="4.1">Puis, large rayon d’or à la pâle clarté,</l>
							<l n="11" num="4.2">Sur le mur de repos que le soir gris dénude,</l>
							<l n="12" num="4.3">La fenêtre vermeille où je vois la cité.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="13" num="1.1">C’est la nuit. Tout s’est tu dans les mornes enceintes ;</l>
							<l n="14" num="1.2">Dans l’azur du silence où sont morts tant d’adieux,</l>
							<l n="15" num="1.3">J’entends errer longtemps toutes les voix éteintes.</l>
						</lg>
						<lg n="2">
							<l n="16" num="2.1">Et je regarde loin des implacables cieux,</l>
							<l n="17" num="2.2">Plus loin que tous les chants et que toutes les plaintes</l>
							<l n="18" num="2.3">La blancheur du matin où se parlent vos yeux.</l>
						</lg>
						<lg n="3">
							<l n="19" num="3.1">Ô porteurs incertains des armes et des lyres,</l>
							<l n="20" num="3.2">Cherchons l’apothéose et le souverain bien</l>
							<l n="21" num="3.3">Sur le chemin de gloire où sont les vrais martyres.</l>
						</lg>
						<lg n="4">
							<l n="22" num="4.1">Je vois qu’il va mourir ce passé qui fut mien,</l>
							<l n="23" num="4.2">Je vois que mon grand soir ternirait vos sourires,</l>
							<l n="24" num="4.3">Que je suis malheureux, et que je ne veux rien.</l>
						</lg>
					</div>
				</div></body></text></TEI>