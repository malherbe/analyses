<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA29">
					<head type="main">LES DEUX FANTÔMES</head>
					<opener>
						<salute>A Madame Amable Tastu.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">O nuit ! quel œil humain peut lire dans ton ombre ?</l>
						<l n="2" num="1.2">Quelle voix nous dira ce qui s’agite aux cieux,</l>
						<l n="3" num="1.3">Quand la terre est tranquille et que, sur l’azur sombre,</l>
						<l n="4" num="1.4">Les astres, dont Dieu seul sait l’éclat et le nombre,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>Roulent froids et silencieux ?</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">O nuit ! J’ai vu passer deux fantômes célèbres ;</l>
						<l n="7" num="2.2">Ils rasaient dans leur vol les dômes de Paris ;</l>
						<l n="8" num="2.3">La ville se berçait dans la paix des ténèbres ;</l>
						<l n="9" num="2.4">Seuls, au sommet des tours, quelques oiseaux funèbres</l>
						<l n="10" num="2.5"><space unit="char" quantity="8"></space>Tournoyaient en poussant des cris.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">Tous les deux ils quittaient la tombe inexorable ;</l>
						<l n="12" num="3.2">Tous les deux ils venaient du tropique enflammé ;</l>
						<l n="13" num="3.3">L’un des bords où mugit un océan de sable,</l>
						<l n="14" num="3.4">L’autre d’un roc désert, où le flot implacable</l>
						<l n="15" num="3.5"><space unit="char" quantity="8"></space>Garde son sépulcre enfermé.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">Chacun d’eux à son tour fut puissant par la guerre ;</l>
						<l n="17" num="4.2">Vivants, le monde à peine a pu les contenir ;</l>
						<l n="18" num="4.3">Morts, ils n’ont rencontré qu’une insensible pierre,</l>
						<l n="19" num="4.4">Où le temps ronge en paix leurs noms et leur poussière,</l>
						<l n="20" num="4.5"><space unit="char" quantity="8"></space>Où les vents seuls viennent gémir.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">Leurs fantômes souvent de leurs urnes s’élancent,</l>
						<l n="22" num="5.2">Sur ce monde oublieux qui ne les connaît plus,</l>
						<l n="23" num="5.3">Par la foudre escortés, dans la nuit ils s’avancent,</l>
						<l n="24" num="5.4">S’inclinent tristement sur l’univers et pensent</l>
						<l n="25" num="5.5"><space unit="char" quantity="8"></space>A leurs empires disparus.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1">Je les ai vus tous deux : l’un, comme les rois mages,</l>
						<l n="27" num="6.2">Ceignait son front hautain de la tiare d’or ;</l>
						<l n="28" num="6.3">Sur sa barbe flottante avaient neigé les âges ;</l>
						<l n="29" num="6.4">Son œil fier, qu’autrefois entouraient tant d’hommages,</l>
						<l n="30" num="6.5"><space unit="char" quantity="8"></space>Semblait les commander encor.</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1">Il descendit aux bords où l’Obélisque antique</l>
						<l n="32" num="7.2">De son dard anguleux semble percer le ciel ;</l>
						<l n="33" num="7.3">Sur son flanc il croisa son manteau fantastique,</l>
						<l n="34" num="7.4">Et longtemps mesura le géant granitique</l>
						<l n="35" num="7.5"><space unit="char" quantity="8"></space>D’un regard sombre et solennel.</l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1">Ses yeux étincelaient d’une flamme éthérée,</l>
						<l n="37" num="8.2">Tandis qu’il parcourait du regard lentement</l>
						<l n="38" num="8.3">Cet étrange alphabet d’une langue ignorée,</l>
						<l n="39" num="8.4">Gravé pour l’avenir, par une main sacrée,</l>
						<l n="40" num="8.5"><space unit="char" quantity="8"></space>Sur les faces du monument.</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1">C’est qu’il y retrouvait sa puissance hautaine,</l>
						<l n="42" num="9.2">Ses combats retracés en récits glorieux,</l>
						<l n="43" num="9.3">Et, sous son nom vainqueur, dévoués à la haine,</l>
						<l n="44" num="9.4">Les noms des rois vaincus, qu’il traînait à la chaîne,</l>
						<l n="45" num="9.5"><space unit="char" quantity="8"></space>Ou qu’il immolait à ses Dieux,</l>
					</lg>
					<lg n="10">
						<l n="46" num="10.1">L’autre ombre n’avait pas cet appareil superbe,</l>
						<l n="47" num="10.2">Quoique son pied jadis eût foulé comme l’herbe</l>
						<l n="48" num="10.3"><space unit="char" quantity="12"></space>Les rois de l’univers.</l>
						<l n="49" num="10.4">Les tortures avaient brisé cette grande âme,</l>
						<l n="50" num="10.5">Et son fantôme encor portait la trace infâme</l>
						<l n="51" num="10.6"><space unit="char" quantity="12"></space>De l’exil et des fers.</l>
					</lg>
					<lg n="11">
						<l n="52" num="11.1">Mais qu’il était sublime et beau sans diadème,</l>
						<l n="53" num="11.2">Ce héros retrempé dans le fatal baptême</l>
						<l n="54" num="11.3"><space unit="char" quantity="12"></space>De son adversité !</l>
						<l n="55" num="11.4">C’était bien lui ! c’était sa tète souveraine,</l>
						<l n="56" num="11.5">Son regard foudroyant, qui tenait en haleine</l>
						<l n="57" num="11.6"><space unit="char" quantity="12"></space>Le monde épouvanté !</l>
					</lg>
					<lg n="12">
						<l n="58" num="12.1">C’était cet uniforme usé par la mitraille ;</l>
						<l n="59" num="12.2">C’était ce manteau bleu, sur les champs de bataille</l>
						<l n="60" num="12.3"><space unit="char" quantity="12"></space>Tant de fois déployé,</l>
						<l n="61" num="12.4">Et ce petit chapeau, couronne populaire,</l>
						<l n="62" num="12.5">Que trente rois n’ont pu ravir, dans leur colère,</l>
						<l n="63" num="12.6"><space unit="char" quantity="12"></space>A son front foudroyé.</l>
					</lg>
					<lg n="13">
						<l n="64" num="13.1">C’est ainsi que, dans l’ombre, au sein de la tempête</l>
						<l n="65" num="13.2">Qui sur ses pas grondait, lui faisant une fête</l>
						<l n="66" num="13.3"><space unit="char" quantity="12"></space>Comme un bruit de combats,</l>
						<l n="67" num="13.4">Je l’ai vu de son vol embrasser la Colonne,</l>
						<l n="68" num="13.5">Et, sur ce bronze saint que sa gloire environne,</l>
						<l n="69" num="13.6"><space unit="char" quantity="12"></space>Contempler ses soldats.</l>
					</lg>
					<lg n="14">
						<l n="70" num="14.1">Qu’étaient-ils devenus ces vieux vainqueurs du monde ?</l>
						<l n="71" num="14.2">La mort les dévorait dans leur tombe profonde</l>
						<l n="72" num="14.3"><space unit="char" quantity="12"></space>De Wagram ou d’Eylau ;</l>
						<l n="73" num="14.4">Et leur triste Empereur, pleurant sur son trophée,</l>
						<l n="74" num="14.5">Murmurait lentement d’une voix étouffée :</l>
						<l n="75" num="14.6"><space unit="char" quantity="12"></space>« O France ! ô Waterloo ! »</l>
					</lg>
					<lg n="15">
						<l n="76" num="15.1">Il s’inclinait pensif au-dessus de la ville,</l>
						<l n="77" num="15.2">Et dans la nuit, longtemps contemplait, immobile,</l>
						<l n="78" num="15.3"><space unit="char" quantity="12"></space>Le sol que nous foulons,</l>
						<l n="79" num="15.4">Comme un aigle qui plane aux voûtes éternelles,</l>
						<l n="80" num="15.5">Se penche sur son aire et couve de ses ailes</l>
						<l n="81" num="15.6"><space unit="char" quantity="12"></space>Le sommeil des aiglons.</l>
					</lg>
					<lg n="16">
						<l n="82" num="16.1">Mais quand il vit briller, ainsi qu’un météore,</l>
						<l n="83" num="16.2">Le fantôme éclatant du vieux roi de l’Aurore,</l>
						<l n="84" num="16.3">Il sembla retrouver son pouvoir d’autrefois</l>
						<l n="85" num="16.4"><space unit="char" quantity="8"></space>Et s.i majesté pour lui dire :</l>
						<l n="86" num="16.5">« Salut, fils de Memnon ! Salut, vainqueur des rois !</l>
						<l n="87" num="16.6"><space unit="char" quantity="8"></space>Sois bienvenu dans mon empire !</l>
					</lg>
					<lg n="17">
						<l n="88" num="17.1">« Souviens-toi, Sésostris, qu’au temps de tes splendeurs,</l>
						<l n="89" num="17.2">Il fut un peuple grand de toutes tes grandeurs,</l>
						<l n="90" num="17.3">Pour lui tes bataillons ravageaient les contrées ;</l>
						<l n="91" num="17.4"><space unit="char" quantity="8"></space>Pour lui, du Niger à l’Indus,</l>
						<l n="92" num="17.5">De l’océan arabe aux mers hyperborées,</l>
						<l n="93" num="17.6"><space unit="char" quantity="8"></space>Tombaient cent peuples confondus.</l>
					</lg>
					<lg n="18">
						<l n="94" num="18.1">« Cette Égypte, pour qui tu gagnais des batailles,</l>
						<l n="95" num="18.2">Ton peuple, était pour toi le sang de tes entrailles ;</l>
						<l n="96" num="18.3">Et, quand tu revenais d’affronter le trépas,</l>
						<l n="97" num="18.4"><space unit="char" quantity="8"></space>S’il applaudissait tes merveilles,</l>
						<l n="98" num="18.5">Il n’était aucun bruit, dans les bruits d’ici-bas,</l>
						<l n="99" num="18.6"><space unit="char" quantity="8"></space>Qui fût plus doux à tes oreilles.</l>
					</lg>
					<lg n="19">
						<l n="100" num="19.1">« La France fut ainsi le peuple de mon cœur.</l>
						<l n="101" num="19.2">Pour elle, ô Pharaon ! mon bras, cent fois vainqueur,</l>
						<l n="102" num="19.3">Courba le front des rois réduits au vasselage ;</l>
						<l n="103" num="19.4"><space unit="char" quantity="8"></space>Et, quand j’avais bien combattu,</l>
						<l n="104" num="19.5">Ses acclamations me payaient mon courage.</l>
						<l n="105" num="19.6"><space unit="char" quantity="8"></space>Sésostris, me reconnais-tu ? »</l>
					</lg>
					<lg n="20">
						<l n="106" num="20.1">« — Oui ! dit l’antique aïeul des monarques Numides,</l>
						<l n="107" num="20.2">Oui ! je te reconnais. Du haut des Pyramides</l>
						<l n="108" num="20.3">J’accompagnai, témoin de tes hardis travaux,</l>
						<l n="109" num="20.4"><space unit="char" quantity="8"></space>Ces quarante siècles de gloire</l>
						<l n="110" num="20.5">Que ta voix évoquait du fond de leurs tombeaux</l>
						<l n="111" num="20.6"><space unit="char" quantity="8"></space>Pour assister à ta victoire.</l>
					</lg>
					<lg n="21">
						<l n="112" num="21.1">« Salut, ô conquérant ! je suis digne de toi.</l>
						<l n="113" num="21.2">Moi-même j’ai rangé l’univers sous ma loi.</l>
						<l n="114" num="21.3">Mes cohortes étaient sœurs des soldats d’Arcole,</l>
						<l n="115" num="21.4"><space unit="char" quantity="8"></space>Mon nom frère aîné de ton nom.</l>
						<l n="116" num="21.5">Le temps couronnera d’une même auréole</l>
						<l n="117" num="21.6"><space unit="char" quantity="8"></space>Sésostris et Napoléon.</l>
					</lg>
					<lg n="22">
						<l n="118" num="22.1">« Que ta France adorée, où tant d’éclat rayonne,</l>
						<l n="119" num="22.2">Garde mon Obélisque auprès de ta Colonne,</l>
						<l n="120" num="22.3">Pour qu’à leur base un jour les siècles à venir,</l>
						<l n="121" num="22.4"><space unit="char" quantity="8"></space>Épris de nos vastes pensées,</l>
						<l n="122" num="22.5">Avec un saint respect viennent s’entretenir</l>
						<l n="123" num="22.6"><space unit="char" quantity="8"></space>De nos étoiles éclipsées. »</l>
					</lg>
					<lg n="23">
						<l n="124" num="23.1">C’est ainsi qu’ils pleuraient sur leurs deux monuments.</l>
						<l n="125" num="23.2">Le ciel s’illuminait de moments en moments,</l>
						<l n="126" num="23.3">Et je crus entrevoir, à ces lueurs étranges,</l>
						<l n="127" num="23.4"><space unit="char" quantity="8"></space>Dans les nuages de la nuit,</l>
						<l n="128" num="23.5">Des armes, des drapeaux, et d’immenses phalanges</l>
						<l n="129" num="23.6"><space unit="char" quantity="8"></space>Autour d’eux se ranger sans bruit.</l>
					</lg>
					<lg n="24">
						<l n="130" num="24.1">Puis l’orage emporta ces visions funèbres,</l>
						<l n="131" num="24.2">Et je me trouvai seul perdu dans les ténèbres.</l>
						<l n="132" num="24.3">Les astres éternels, rayonnant de clartés,</l>
						<l n="133" num="24.4"><space unit="char" quantity="8"></space>Traçaient leur sillon dans l’espace,</l>
						<l n="134" num="24.5">Impassibles témoins de nos fragilités</l>
						<l n="135" num="24.6"><space unit="char" quantity="8"></space>Et du néant de ce qui <choice hand="RR" type="false_verse" reason="analysis"><sic>se</sic><corr source="edition_1866"></corr></choice> passe.</l>
					</lg>
				</div></body></text></TEI>