<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA93">
					<head type="main">LA FILLE DU TINTORET</head>
					<opener>
						<salute>A Léon Cogniet.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Venise ! oh ! que de fois, un désir fantastique</l>
						<l n="2" num="1.2">A transporté mon cœur sur ton Adriatique !</l>
						<l n="3" num="1.3">De l’espace et du temps déchirant le rideau,</l>
						<l n="4" num="1.4">J’ai rêvé tes canaux sillonnés de gondoles,</l>
						<l n="5" num="1.5">Et tes palais de marbre et tes blanches coupoles,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Et ton Saint-Marc et ton Lido !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Là, parmi les splendeurs de ton architecture,</l>
						<l n="8" num="2.2">J’aime à ressusciter les rois de la peinture</l>
						<l n="9" num="2.3">Qui prenaient leurs couleurs au ciel vénitien ;</l>
						<l n="10" num="2.4">Je vois les deux Palma, dont le génie éclate,</l>
						<l n="11" num="2.5">Véronèse drapé de pourpre et d’écarlate,</l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space>Et le grand maître Titien ;</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Les uns glissent, bercés par les ondes limpides,</l>
						<l n="14" num="3.2">Souriant aux chansons de ces beautés splendides,</l>
						<l n="15" num="3.3">De ces reines d’un jour, qui vivront sous leur main ;</l>
						<l n="16" num="3.4">D’autres, le front pensif, sur la sombre lagune</l>
						<l n="17" num="3.5">Vont rêver, isolés de la foule importune,</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space>A leur chef-d’œuvre de demain.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Toi surtout, Robusti, vieillard au front austère,</l>
						<l n="20" num="4.2">Aussi fier que ton nom, j’aime ton caractère.</l>
						<l n="21" num="4.3">Où tout autre eût cédé, tu luttes et grandis.</l>
						<l n="22" num="4.4">Tu veux le premier rang dans la noble phalange :</l>
						<l n="23" num="4.5">La terre à Titien, l’enfer à Michel-Ange, </l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space>A Tintoret le paradis !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><space unit="char" quantity="8"></space>Maître ! quand Venise en ivresse</l>
						<l n="26" num="5.2"><space unit="char" quantity="8"></space>S’égaye et rit de toute part,</l>
						<l n="27" num="5.3"><space unit="char" quantity="8"></space>Pourquoi rester, dans ta vieillesse,</l>
						<l n="28" num="5.4"><space unit="char" quantity="8"></space>Dédaigneux et fier à l’écart ?</l>
						<l n="29" num="5.5"><space unit="char" quantity="8"></space>C’est que ton âme est orgueilleuse.</l>
						<l n="30" num="5.6"><space unit="char" quantity="8"></space>De ta fille, enfant merveilleuse,</l>
						<l n="31" num="5.7"><space unit="char" quantity="8"></space>Tu soutiens le sublime essor.</l>
						<l n="32" num="5.8"><space unit="char" quantity="8"></space>Ainsi le chêne, dans sa force,</l>
						<l n="33" num="5.9"><space unit="char" quantity="8"></space>Sur ses bras à la rude écorce</l>
						<l n="34" num="5.10"><space unit="char" quantity="8"></space>Suspend la vigne aux grappes d’or.</l>
					</lg>
					<lg n="6">
						<l n="35" num="6.1"><space unit="char" quantity="8"></space>Belle et sainte ! On dirait un ange !</l>
						<l n="36" num="6.2"><space unit="char" quantity="8"></space>Les cieux doivent la regretter.</l>
						<l n="37" num="6.3"><space unit="char" quantity="8"></space>Ses regards ont un charme étrange,</l>
						<l n="38" num="6.4"><space unit="char" quantity="8"></space>Sa voix semble toujours chanter.</l>
						<l n="39" num="6.5"><space unit="char" quantity="8"></space>La harpe, entre ses mains bénies,</l>
						<l n="40" num="6.6"><space unit="char" quantity="8"></space>A d’indicibles harmonies</l>
						<l n="41" num="6.7"><space unit="char" quantity="8"></space>Qui font du plaisir et du mal ;</l>
						<l n="42" num="6.8"><space unit="char" quantity="8"></space>Ses pinceaux animent la toile ;</l>
						<l n="43" num="6.9"><space unit="char" quantity="8"></space>Elle fait pâlir ton étoile,</l>
						<l n="44" num="6.10"><space unit="char" quantity="8"></space>Et son génie est ton rival.</l>
					</lg>
					<lg n="7">
						<l n="45" num="7.1"><space unit="char" quantity="8"></space>Autant que toi Venise est folle</l>
						<l n="46" num="7.2"><space unit="char" quantity="8"></space>De Maria Tintorelia.</l>
						<l n="47" num="7.3"><space unit="char" quantity="8"></space>C’est la merveille, c’est l’idole !</l>
						<l n="48" num="7.4"><space unit="char" quantity="8"></space>Paraît-elle ? on dit : — La voilà !</l>
						<l n="49" num="7.5"><space unit="char" quantity="8"></space>Le doge lui sert de modèle ;</l>
						<l n="50" num="7.6"><space unit="char" quantity="8"></space>Les rois, pour être peints par elle,</l>
						<l n="51" num="7.7"><space unit="char" quantity="8"></space>Lui dépêchent leurs envoyés.</l>
						<l n="52" num="7.8"><space unit="char" quantity="8"></space>Oh ! dans ta solitude austère,</l>
						<l n="53" num="7.9"><space unit="char" quantity="8"></space>Que tu dois être un heureux père !…</l>
						<l n="54" num="7.10"><space unit="char" quantity="8"></space>Il est heureux.’ Oh ! oui… Voyez !…</l>
					</lg>
					<lg n="8">
						<l n="55" num="8.1">Voyez sous ces rideaux la blonde Tintorelle,</l>
						<l n="56" num="8.2">Pâle, froide, immobile et douloureuse à voir.</l>
						<l n="57" num="8.3"><space unit="char" quantity="12"></space>Son père au désespoir</l>
						<l n="58" num="8.4">Se penche vers son lit, encor plus pâle qu’elle.</l>
					</lg>
					<lg n="9">
						<l n="59" num="9.1">Il contemple, d’un œil terne et stupéfié,</l>
						<l n="60" num="9.2">Son bonheur, un cadavre, et son espoir, une ombre !</l>
						<l n="61" num="9.3"><space unit="char" quantity="12"></space>Il est là, morne, sombre.</l>
						<l n="62" num="9.4">Comme si la douleur l’avait pétrifié.</l>
					</lg>
					<lg n="10">
						<l n="63" num="10.1">Sa fille souriait, ce matin, fraîche et forte ;</l>
						<l n="64" num="10.2">Sa toile, ses pinceaux, ses couleurs… ô destin !…</l>
						<l n="65" num="10.3"><space unit="char" quantity="12"></space>Préparés ce matin,</l>
						<l n="66" num="10.4">Semblent l’attendre encore… et ce soir elle est morte !</l>
					</lg>
					<lg n="11">
						<l n="67" num="11.1">Morte !… Il ne le croit pas. Pauvre cœur paternel,</l>
						<l n="68" num="11.2">Qui nageait, ce matin, dans des torrents de joie,</l>
						<l n="69" num="11.3"><space unit="char" quantity="12"></space>Et que le ciel foudroie,</l>
						<l n="70" num="11.4">Comment pourrait-il croire à ce deuil éternel ?</l>
					</lg>
					<lg n="12">
						<l n="71" num="12.1">Plus d’enfant ! ne plus voir sa tète enchanteresse !</l>
						<l n="72" num="12.2">Ses yeux qui, du vieillard illuminant le soir.</l>
						<l n="73" num="12.3"><space unit="char" quantity="12"></space>Étoiles de l’espoir,</l>
						<l n="74" num="12.4">Donnaient à son déclin l’éclat de la jeunesse !</l>
					</lg>
					<lg n="13">
						<l n="75" num="13.1">Plus d’enfant ! Et qui donc ramassera demain</l>
						<l n="76" num="13.2">Ces pinceaux enviés, fameux par tant d’ouvrages,</l>
						<l n="77" num="13.3"><space unit="char" quantity="12"></space>Glorieux héritages</l>
						<l n="78" num="13.4">Qui s’échappent déjà de sa tremblante main ?</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1">Plus d’enfant ! Avec toi, fugitive colombe,</l>
						<l n="80" num="14.2">Le rire, la gaîté, les chants harmonieux</l>
						<l n="81" num="14.3"><space unit="char" quantity="12"></space>Sont remontés aux cieux,</l>
						<l n="82" num="14.4">Et la harpe aux doux sons dormira sur ta tombe.</l>
					</lg>
					<lg n="15">
						<l n="83" num="15.1">Ce père, qui marchait dans son joyeux orgueil,</l>
						<l n="84" num="15.2">Radieux de sa fille à son bras attachée,</l>
						<l n="85" num="15.3"><space unit="char" quantity="12"></space>Ira, tète penchée,</l>
						<l n="86" num="15.4">Aussi blême qu’un mort évoqué du cercueil.</l>
					</lg>
					<lg n="16">
						<l n="87" num="16.1">Au géant des forêts la vigne qui s’enchaîne</l>
						<l n="88" num="16.2">Tombe avec les rameaux qui lui servaient d’appui :</l>
						<l n="89" num="16.3"><space unit="char" quantity="12"></space>C’est la vigne aujourd’hui</l>
						<l n="90" num="16.4">Qui meurt, et dont la mort fait succomber le chêne.</l>
					</lg>
					<lg n="17">
						<l n="91" num="17.1">La douleur du vieillard éclate, et prosterné :</l>
						<l n="92" num="17.2">« Dans ma fille, ô mon Dieu, vous m’aviez couronné ;</l>
						<l n="93" num="17.3"><space unit="char" quantity="12"></space>C’était mon bien, ma vie.</l>
						<l n="94" num="17.4">Pourquoi sans le vieux père avoir frappé l’enfant ?</l>
						<l n="95" num="17.5">Ah ! j’étais trop heureux, j’étais trop triomphant</l>
						<l n="96" num="17.6"><space unit="char" quantity="12"></space>Et trop digne d’envie !</l>
					</lg>
					<lg n="18">
						<l n="97" num="18.1">« Ayez pitié, Seigneur, et faites-moi mourir !</l>
						<l n="98" num="18.2">J’ai souffert aujourd’hui plus qu’on ne peut souffrir</l>
						<l n="99" num="18.3"><space unit="char" quantity="12"></space>Dans toute une existence.</l>
						<l n="100" num="18.4">Puisque vous m’avez pris le trésor que j’aimais,</l>
						<l n="101" num="18.5">Prenez-moi donc aussi. Je suis mort désormais ;</l>
						<l n="102" num="18.6"><space unit="char" quantity="12"></space>Je n’ai plus d’espérance !</l>
					</lg>
					<lg n="19">
						<l n="103" num="19.1">« Beauté, grâce, génie et vertu, tout est là !</l>
						<l n="104" num="19.2">Je ne te verrai plus, ô ma Tintorella !</l>
						<l n="105" num="19.3"><space unit="char" quantity="12"></space>Le tombeau qui dévore,</l>
						<l n="106" num="19.4">De toi, sang de mon sang, de toi, chair de ma chair,</l>
						<l n="107" num="19.5">Fait un reste insensible !… O mon bien le plus cher,</l>
						<l n="108" num="19.6"><space unit="char" quantity="12"></space>Je veux te voir encore !</l>
					</lg>
					<lg n="20">
						<l n="109" num="20.1">« Vous qu’elle a préparés, ses pinceaux, ses couleurs,</l>
						<l n="110" num="20.2">Venez à mon secours ; soulagez mes douleurs,</l>
						<l n="111" num="20.3"><space unit="char" quantity="12"></space>Rendez-moi son visage.</l>
						<l n="112" num="20.4">Quand Dieu de mon exil voudra me retirer,</l>
						<l n="113" num="20.5">O ma Tintorella ! que je puisse expirer</l>
						<l n="114" num="20.6"><space unit="char" quantity="12"></space>Les yeux sur ton image ! »</l>
					</lg>
					<lg n="21">
						<l n="115" num="21.1">Sa main tient la palette, et, dévorant son deuil,</l>
						<l n="116" num="21.2">Il fixe sur sa fille un pénétrant coup d’œil.</l>
						<l n="117" num="21.3">Il sature longtemps son âme paternelle</l>
						<l n="118" num="21.4">De ta pâleur de marbre, ô douloureux modèle !</l>
						<l n="119" num="21.5">Une lampe funèbre, à travers un rideau,</l>
						<l n="120" num="21.6">De sa morne lumière éclaire le tableau,</l>
						<l n="121" num="21.7">Et glisse sur la morte étendue en sa couche.</l>
						<l n="122" num="21.8">Ses beaux yeux sont fermés languissamment ; sa bouche</l>
						<l n="123" num="21.9">Est entr’ouverte encor par le dernier soupir ;</l>
						<l n="124" num="21.10">Et le doigt de la mort, qui vient de l’assoupir,</l>
						<l n="125" num="21.11">A laissé sur son front le divin caractère</l>
						<l n="126" num="21.12">D’un ange que le ciel vient de prendre à la terre</l>
						<l n="127" num="21.13">Toi, vieillard, pâle, sombre, et cependant vainqueur</l>
						<l n="128" num="21.14">Du sanglant désespoir qui te ronge le cœur,</l>
						<l n="129" num="21.15">Tu concentres ton âme en ce suprême ouvrage.</l>
						<l n="130" num="21.16">Par un sublime effort d’amour et de courage,</l>
						<l n="131" num="21.17">Tu veux, et ton pinceau n’a pas même hésité.</l>
						<l n="132" num="21.18">Si ta lèvre est aride et ton front contracté,</l>
						<l n="133" num="21.19">Si ton œil est brûlant, aucun pleur ne le voile,</l>
						<l n="134" num="21.20">Et l’image adorée a passé sur la toile.</l>
						<l n="135" num="21.21">Rongez, vers du tombeau ! faites votre devoir ;</l>
						<l n="136" num="21.22">Sur la Tintorella vous êtes sans pouvoir.</l>
						<l n="137" num="21.23">Par deux fois au néant le Tintoret l’a prise :</l>
						<l n="138" num="21.24">Père, il lui donna l’être ; artiste, il l’éternise !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>L’Étang</placeName>,
							<date when="1852">16 septembre 1852.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>