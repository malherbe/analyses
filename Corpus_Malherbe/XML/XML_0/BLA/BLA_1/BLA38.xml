<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA38">
					<head type="main">AMOUR</head>
					<opener>
						<salute>A Marie Désirée.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Lorsque je vois tes yeux, ma plus vive souffrance</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Sous ton regard s’évanouit ;</l>
						<l n="3" num="1.3">Lorsque tu me souris, la fleur de l’espérance</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>En moi brille et s épanouit !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Quand tu me dis : « Je t’aime ! » ô ma seule adorée ;</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Les cieux alors me sont ouverts,</l>
						<l n="7" num="2.3">La terre est trop étroite à mon âme enivrée,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Je suis le roi de l’univers !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Je suis poète alors ; car les feux du génie</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Rayonnent sur moi de tes yeux ;</l>
						<l n="11" num="3.3">Car de ton cœur au mien un fleuve d’harmonie</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Descend à flots mélodieux.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Mais viens-tu par hasard à détourner la tète,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>Je m’égare en tristes accords ;</l>
						<l n="15" num="4.3">Ma poésie en deuil perd ses habits de fête,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Ma voix et mon bonheur sont morts.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Et si je cherche encore au dedans de moi-même,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>Comme on cherche un songe au réveil,</l>
						<l n="19" num="5.3">Ce que chantait mon cœur quand tu disais : « Je t’aime !</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>Quand brillait ton regard vermeil,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">La joie et le sourire, avec ton doux visage,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>Abandonnent mon front penché :</l>
						<l n="23" num="6.3">L’oiseau mélodieux se tait sous le feuillage,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>Lorsque le soleil est caché.</l>
					</lg>
				</div></body></text></TEI>