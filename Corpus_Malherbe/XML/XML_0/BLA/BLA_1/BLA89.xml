<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA89">
					<head type="main">LE PAUVRE MOINE</head>
					<lg n="1">
						<l n="1" num="1.1">Le pauvre moine, au fond du cloître austère.</l>
						<l n="2" num="1.2">Pleure sa vie, et quand Dieu lui dirait :</l>
						<l n="3" num="1.3">« Demain, mon fils, tu seras sous la terre ! »</l>
						<l n="4" num="1.4">Prêt à sonder le terrible mystère,</l>
						<l n="5" num="1.5">Il verrait fuir le soleil sans regret.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">Parfois pourtant, quand le jour étincelle,</l>
						<l n="7" num="2.2">L’espoir remonte à son front soucieux.</l>
						<l n="8" num="2.3">L’oiseau chanteur, la source qui ruisselle,</l>
						<l n="9" num="2.4">Les champs, l’air pur où son Dieu se décèle,</l>
						<l n="10" num="2.5">Charment encor son oreille et ses yeux.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">Mais vient la nuit. A ses maux il succombe ;</l>
						<l n="12" num="3.2">Pour lui le cloître est plus qu’une prison.</l>
						<l n="13" num="3.3">Il croit, vivant étendu dans sa tombe,</l>
						<l n="14" num="3.4">Frapper du front la pierre qui retombe…</l>
						<l n="15" num="3.5">Son désespoir lutte avec sa raison.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">« Navré d’amour ; en ma douleur profonde,</l>
						<l n="17" num="4.2">Cherchant l’oubli comme un divin bienfait,</l>
						<l n="18" num="4.3">J’avais cru fuir et mon cœur et le monde.</l>
						<l n="19" num="4.4">Et cet habit cache un volcan qui gronde :</l>
						<l n="20" num="4.5">Malheur à moi ! Qu’ai-je dit ? Qu’ai-je fait )</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">« Quand le matin je vais à la chapelle,</l>
						<l n="22" num="5.2">Quand je suis seul à prier dans le chœur,</l>
						<l n="23" num="5.3">Mon chant s’éteint dans ma gorge rebelle.</l>
						<l n="24" num="5.4">Je crois entendre une voix qui m’appelle,</l>
						<l n="25" num="5.5">Timide voix qui me brise le cœur.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1">« Votre portrait, sainte Vierge Marie,</l>
						<l n="27" num="6.2">Dans les vapeurs qu’exhale l’encensoir,</l>
						<l n="28" num="6.3">Prend à mes yeux une forme chérie ;</l>
						<l n="29" num="6.4">Ce n’est plus vous, c’est elle que je prie :</l>
						<l n="30" num="6.5">Je resterais à genoux jusqu’au soir.</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1">« Un frère alors me tire par ma robe ;</l>
						<l n="32" num="7.2">Je me relève et vais sans savoir où.</l>
						<l n="33" num="7.3">Mon pied tremblant sous mon cœur se dérobe ;</l>
						<l n="34" num="7.4">Sans m’éveiller, Dieu briserait le globe.</l>
						<l n="35" num="7.5">Je vais mourir ou j’en deviendrai fou !</l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1">« Mon cœur palpite à rompre ma poitrine,</l>
						<l n="37" num="8.2">Ma tète brûle et j’ai froid ! Si j’osais</l>
						<l n="38" num="8.3">M’offrir en face à la fureur divine,</l>
						<l n="39" num="8.4">Si je frappais ma tète que j’incline</l>
						<l n="40" num="8.5">Contre le marbre et si je l’écrasais ?…</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1">« Non ! loin de moi cette lâche pensée !</l>
						<l n="42" num="9.2">Pitié, Seigneur, ou je serai vaincu.</l>
						<l n="43" num="9.3">Mais quoi ! toujours, d’une bouche lassée,</l>
						<l n="44" num="9.4">Boire à longs traits cette coupe glacée,</l>
						<l n="45" num="9.5">Et mourir vieux et n’avoir pas vécu !</l>
					</lg>
					<lg n="10">
						<l n="46" num="10.1">« Oh ! ne plus voir cette étroite demeure,</l>
						<l n="47" num="10.2">Franchir ces murs, briser ce joug de fer !</l>
						<l n="48" num="10.3">Du temps passé, rien qu’un jour, rien qu’une heure.</l>
						<l n="49" num="10.4">Rien qu’un baiser de celle que je pleure,</l>
						<l n="50" num="10.5">Rien qu’un sourire, un regard… et l’enfer ! »</l>
					</lg>
					<lg n="11">
						<l n="51" num="11.1">Le pauvre moine ainsi courbé dans l’ombre,</l>
						<l n="52" num="11.2">De deuil en deuil au désespoir conduit,</l>
						<l n="53" num="11.3">S’en va pleurant dans sa cellule sombre,</l>
						<l n="54" num="11.4">Et de son cœur les battements sans nombre</l>
						<l n="55" num="11.5">Lui comptent seuls les heures de la nuit.</l>
					</lg>
				</div></body></text></TEI>