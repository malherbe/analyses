<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA91">
					<head type="main">LA FÉE DE ROMEFORT</head>
					<opener>
						<salute>A madame la comtesse de Bondy,</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Madame, il me souvient de ce jour trop rapide</l>
						<l n="2" num="1.2">Où. m ’ayant accepté pour votre chevalier,</l>
						<l n="3" num="1.3">Dans votre Romefort vous me serviez de guide :</l>
						<l n="4" num="1.4">Il est de ces bonheurs qu’on ne peut oublier.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Au sommet du donjon qui domine la plaine,</l>
						<l n="6" num="2.2">Je vous suivais, passant où vous aviez passé ;</l>
						<l n="7" num="2.3">Et du sombre manoir, aimable châtelaine,</l>
						<l n="8" num="2.4">Vous me ressuscitiez le fantôme glacé.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Vous évoquiez ces preux dont l’âme fut si grande</l>
						<l n="10" num="3.2">Sous le pourpoint de soie ou l’armure en métal ;</l>
						<l n="11" num="3.3">Mais auprès de l’histoire il manquait la légende :</l>
						<l n="12" num="3.4">Il fallait une fée au donjon féodal.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">On m’a, dans le pays, fait le récit étrange</l>
						<l n="14" num="4.2">D’une charmante Fée errante aux alentours ;</l>
						<l n="15" num="4.3">Aux grâces d’une femme elle unit un cœur d’ange.</l>
						<l n="16" num="4.4">Et d’un castel voisin elle habite les tours.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Souvent à Romefort on la voit apparaître.</l>
						<l n="18" num="5.2">A sa voix le deuil cesse et le malheur finit ;</l>
						<l n="19" num="5.3">Le pauvre qui l’invoque aussitôt sent renaître</l>
						<l n="20" num="5.4">En son cœur l’espérance, et tout bas la bénit.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Les enfants du hameau qui s’en vont à l’école,</l>
						<l n="22" num="6.2">Pour complaire à la Fée apprennent leurs leçons ;</l>
						<l n="23" num="6.3">Par elle, ils savent l’art de fixer la parole,</l>
						<l n="24" num="6.4">Et vont, joyeux oiseaux, lui chanter leurs chansons.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Des lettres, des beaux-arts aimant l’essor sublime,</l>
						<l n="26" num="7.2">Elle dérobe au temps ce qu’il allait flétrir ;</l>
						<l n="27" num="7.3">Elle a l’âme qui crée et l’esprit qui ranime :</l>
						<l n="28" num="7.4">Ce qu’elle a préféré ne saurait plus mourir.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">La voir est un plaisir, la connaître une joie ;</l>
						<l n="30" num="8.2">Heureux ceux qu’elle enchante, et plus heureux encor</l>
						<l n="31" num="8.3">Ceux qui sont aimés d’elle et marchent dans sa voie.</l>
						<l n="32" num="8.4">Que n’offrirait-on pas pour un pareil trésor ?</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Oh ! que longtemps elle aille, adorable et discrète,</l>
						<l n="34" num="9.2">Répandant ses bienfaits sans laisser voir sa main,</l>
						<l n="35" num="9.3">Réchauffant tous les cœurs touchés par sa baguette ;</l>
						<l n="36" num="9.4">Que les fleurs qu’elle sème embaument son chemin ! </l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Les pauvres dont la peine est par elle étouffée,</l>
						<l n="38" num="10.2">Les enfants, les vieillards la nomment à genoux.</l>
						<l n="39" num="10.3">Je ne vous dirai pas le nom de cette Fée ;</l>
						<l n="40" num="10.4">Mais chacun la connaît, madame… excepté vous.</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Longefont</placeName>,
							<date when="1857">novembre 1857.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>