<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA109">
					<head type="main">LE COMTE ADICK</head>
					<head type="form">BALLADE</head>
					<opener>
						<salute>A mon cher onde Paul Richer.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">La trompette des alarmes</l>
						<l n="2" num="1.2">A sonné dans les châteaux.</l>
						<l n="3" num="1.3">Le comte Adick prend ses armes</l>
						<l n="4" num="1.4">Et rassemble ses vassaux.</l>
						<l n="5" num="1.5">A l’appel de la patrie,</l>
						<l n="6" num="1.6">Jamais magnat de Hongrie</l>
						<l n="7" num="1.7">N’a tardé, même d’un jour.</l>
						<l n="8" num="1.8">Il met sa cotte de maille ;</l>
						<l n="9" num="1.9">Son bon cheval de bataille</l>
						<l n="10" num="1.10">Hennit au pied de la tour.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">Une belle et noble Dame</l>
						<l n="12" num="2.2">Regarde tous ces apprêts,</l>
						<l n="13" num="2.3">Et sourit, la mort dans l’âme,</l>
						<l n="14" num="2.4">D’un sourire pur et frais :</l>
						<l n="15" num="2.5">C’est la jeune fiancée,</l>
						<l n="16" num="2.6">Qui concentre sa pensée</l>
						<l n="17" num="2.7">Sur le Comte, ses amours,</l>
						<l n="18" num="2.8">Qu’elle voit, de sa fenêtre,</l>
						<l n="19" num="2.9">Partir, pour longtemps peut-être,</l>
						<l n="20" num="2.10">Et peut-être pour toujours.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Mais de gémir elle a honte,</l>
						<l n="22" num="3.2">Car la Hongrie en danger</l>
						<l n="23" num="3.3">Appelle le noble Comte</l>
						<l n="24" num="3.4">Pour combattre et la venger.</l>
						<l n="25" num="3.5">Aux brillants éclairs du sabre,</l>
						<l n="26" num="3.6">Au destrier qui se cabre,</l>
						<l n="27" num="3.7">Elle rit avec effort :</l>
						<l n="28" num="3.8">Elle parle de victoire,</l>
						<l n="29" num="3.9">De prochain retour, de gloire,</l>
						<l n="30" num="3.10">Et rêve blessure et mort.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">« — Je pars, ma blonde Gisèle ;</l>
						<l n="32" num="4.2">Mais je te rapporterai</l>
						<l n="33" num="4.3">Et mon cœur aussi fidèle,</l>
						<l n="34" num="4.4">Et mon nom plus honoré.</l>
						<l n="35" num="4.5">Cet anneau de fiancée</l>
						<l n="36" num="4.6">Tient mon âme à toi fixée</l>
						<l n="37" num="4.7">D’un nœud qu’on ne peut briser !</l>
						<l n="38" num="4.8">Puis il prend sa main petite,</l>
						<l n="39" num="4.9">Et sur la bague bénite</l>
						<l n="40" num="4.10">Il dépose un doux baiser.</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1">Soudain la trompette sonne ;</l>
						<l n="42" num="5.2">L’adieu se perd dans le bruit.</l>
						<l n="43" num="5.3">Sur le coursier qui frissonne</l>
						<l n="44" num="5.4">Le Comte part : tout le suit.</l>
						<l n="45" num="5.5">Au soleil, dans la poussière,</l>
						<l n="46" num="5.6">Flotte la rouge bannière</l>
						<l n="47" num="5.7">Et luit mainte armure en feu ;</l>
						<l n="48" num="5.8">Gisèle en pleurs suit leur trace,</l>
						<l n="49" num="5.9">Et le dernier bruit qui passe</l>
						<l n="50" num="5.10">Lui porte un dernier adieu.</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1">Adick, aux champs du carnage,</l>
						<l n="52" num="6.2">Fait briller son noble cœur.</l>
						<l n="53" num="6.3">La gloire aime le courage ;</l>
						<l n="54" num="6.4">Adick est partout vainqueur,</l>
						<l n="55" num="6.5">Cependant sa fiancée,</l>
						<l n="56" num="6.6">D’un mal dévorant blessée,</l>
						<l n="57" num="6.7">Voit de bien près le tombeau,</l>
						<l n="58" num="6.8">Et le venin qui ravage</l>
						<l n="59" num="6.9">Marque à jamais son passage</l>
						<l n="60" num="6.10">Sur ce front hier si beau.</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1">La beauté n’est rien pour elle ;</l>
						<l n="62" num="7.2">Cependant à son miroir</l>
						<l n="63" num="7.3">Elle court, pauvre Gisèle,</l>
						<l n="64" num="7.4">Et frémit de s’y revoir !</l>
						<l n="65" num="7.5">Un deuil affreux la dévore ;</l>
						<l n="66" num="7.6">Comment plaira-t-elle encore</l>
						<l n="67" num="7.7">A ce héros des combats,</l>
						<l n="68" num="7.8">Qui déjà revient peut-être,</l>
						<l n="69" num="7.9">Et, la voyant apparaître.</l>
						<l n="70" num="7.10">Ne la reconnaîtra pas !</l>
					</lg>
					<lg n="8">
						<l n="71" num="8.1">« — Oh ! je voudrais être morte !</l>
						<l n="72" num="8.2">Pourquoi voir encor le jour,</l>
						<l n="73" num="8.3">Si le mal qui fuit m’emporte</l>
						<l n="74" num="8.4">Mon bonheur et mon amour ?</l>
						<l n="75" num="8.5">Tandis qu’à son apanage</l>
						<l n="76" num="8.6">Adik joint, par son courage,</l>
						<l n="77" num="8.7">La gloire d’un nom vanté,</l>
						<l n="78" num="8.8">Je perds ma seule richesse,</l>
						<l n="79" num="8.9">Mon seul titre à sa tendresse,</l>
						<l n="80" num="8.10">Ma couronne de beauté ! »</l>
					</lg>
					<lg n="9">
						<l n="81" num="9.1">Tandis qu’elle fond en larmes,</l>
						<l n="82" num="9.2">Partout résonne à la fois</l>
						<l n="83" num="9.3">Le bruit des pas et des armes.</l>
						<l n="84" num="9.4">Du comte Adick c’est la. voix :</l>
						<l n="85" num="9.5">« — Où donc es-tu, ma Gisèle ?</l>
						<l n="86" num="9.6">Viens ! viens ! celui qui t’appelle,</l>
						<l n="87" num="9.7">C’est ton bien-aimé ; c’est moi ! »</l>
						<l n="88" num="9.8">Elle frémit de l’entendre.</l>
						<l n="89" num="9.9">Ce cri d’une voix si tendre</l>
						<l n="90" num="9.10">Lui remplit Je cœur d’effroi.</l>
					</lg>
					<lg n="10">
						<l n="91" num="10.1">« — Ne m’approche pas, dit-elle</l>
						<l n="92" num="10.2">Dans son douloureux émoi.</l>
						<l n="93" num="10.3">Fuis ! j’ai cessé d’être belle ;</l>
						<l n="94" num="10.4">Je suis indigne de toi ! »</l>
						<l n="95" num="10.5">Et ses deux mains, avec crainte.</l>
						<l n="96" num="10.6">D’une convulsive étreinte</l>
						<l n="97" num="10.7">Voilaient son front agité.</l>
						<l n="98" num="10.8">Mais lui : « — Viens à moi ! je t’aime</l>
						<l n="99" num="10.9">Si ton amour est le même,</l>
						<l n="100" num="10.10">Que m’importe ta beauté ! »</l>
					</lg>
					<lg n="11">
						<l n="101" num="11.1">— Non ! à mon âme éperdue</l>
						<l n="102" num="11.2">Épargne ce désespoir ;</l>
						<l n="103" num="11.3">Tu frémiras à ma vue ! »</l>
						<l n="104" num="11.4">« — Mes yeux ne peuvent plus voir !…</l>
						<l n="105" num="11.5">Elle regarde… A la guerre,</l>
						<l n="106" num="11.6">D’une atteinte meurtrière,</l>
						<l n="107" num="11.7">Le comte a perdu les yeux.</l>
						<l n="108" num="11.8">« — Adick, ô toi que j’adore,</l>
						<l n="109" num="11.9">Tu peux donc m’aimer encore !</l>
						<l n="110" num="11.10">Soyez bénis, justes cieux ! »</l>
					</lg>
					<lg n="12">
						<l n="111" num="12.1">Partout la jeune comtesse</l>
						<l n="112" num="12.2">Conduit l’aveugle adoré ;</l>
						<l n="113" num="12.3">Et si d’une gaze épaisse</l>
						<l n="114" num="12.4">Elle a le front entouré,</l>
						<l n="115" num="12.5">Ce n’est pas qu’elle regrette</l>
						<l n="116" num="12.6">Sa forme autrefois parfaite :</l>
						<l n="117" num="12.7">Elle craint, d’un cœur jaloux,</l>
						<l n="118" num="12.8">Que sur sa beauté perdue</l>
						<l n="119" num="12.9">Quelque parole entendue</l>
						<l n="120" num="12.10">N’attriste son noble époux.</l>
					</lg>
				</div></body></text></TEI>