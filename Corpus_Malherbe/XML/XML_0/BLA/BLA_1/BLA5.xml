<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA5">
					<head type="main">SOUS UN TOIT DE CHAUME</head>
					<head type="form">ÉLÉGIE</head>
					<head type="sub_2">COURONNÉE PAR L’ACADÉMIE DES JEUX FLORAUX</head>
					<lg n="1">
						<l n="1" num="1.1">Sur le bord de la route il est une chaumine</l>
						<l n="2" num="1.2">Qu’entoure un enclos vert, qu’un cerisier domine,</l>
						<l n="3" num="1.3"><space unit="char" quantity="12"></space>Couvert de fruits rougis ;</l>
						<l n="4" num="1.4">Son faîte est couronné de ces fleurs, de ces lierres</l>
						<l n="5" num="1.5">Dont le printemps se plaît à parer les chaumières</l>
						<l n="6" num="1.6"><space unit="char" quantity="12"></space>Et les pauvres logis.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Lors du dernier avril, au temps des pâquerettes,</l>
						<l n="8" num="2.2">Quand les mouches dans l’herbe aux mobiles aigrettes</l>
						<l n="9" num="2.3"><space unit="char" quantity="12"></space>S’ébattent par milliers,</l>
						<l n="10" num="2.4">Sous ce toit demeurait une enfant du village,</l>
						<l n="11" num="2.5">Plus fraîche que les fleurs, plus vive et plus volage</l>
						<l n="12" num="2.6"><space unit="char" quantity="12"></space>Que l’oiseau des halliers.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Comme elle était alors séduisante et jolie !</l>
						<l n="14" num="3.2">Que de grâce, d’amour et de mélancolie</l>
						<l n="15" num="3.3"><space unit="char" quantity="12"></space>Dans ses deux grands yeux bleus !</l>
						<l n="16" num="3.4">Moins douce est la lueur des lampes solitaires</l>
						<l n="17" num="3.5">Qui répandent dans l’ombre au fond des sanctuaires</l>
						<l n="18" num="3.6"><space unit="char" quantity="12"></space>Un rayon nébuleux.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Avec ses dents de nacre, avec son teint de pèche,</l>
						<l n="20" num="4.2">Comme elle souriait, dans sa toilette fraîche</l>
						<l n="21" num="4.3"><space unit="char" quantity="12"></space>Négligée à dessein !</l>
						<l n="22" num="4.4">Combien la regarder était charmante chose,</l>
						<l n="23" num="4.5">Et combien elle était plus rose que la rose</l>
						<l n="24" num="4.6"><space unit="char" quantity="12"></space>Attachée à son sein !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">On la voyait joyeuse à la fenêtre ouverte,</l>
						<l n="26" num="5.2">Sur le banc de la porte, ou sous la treille verte</l>
						<l n="27" num="5.3"><space unit="char" quantity="12"></space>Travailler et chanter.</l>
						<l n="28" num="5.4">Quand, par un beau matin, on côtoyait la haie,</l>
						<l n="29" num="5.5">Devant tant de candeur et tant de gaîté vraie,</l>
						<l n="30" num="5.6"><space unit="char" quantity="12"></space>Il fallait s’arrêter !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Et l’écho redisait son chant souple et facile,</l>
						<l n="32" num="6.2">Et le passant restait sur la route, immobile,</l>
						<l n="33" num="6.3"><space unit="char" quantity="12"></space>Son bâton sur le sol.</l>
						<l n="34" num="6.4">Ne sachant si la voix qu’il écoutait de l’âme</l>
						<l n="35" num="6.5">Était en vérité la chanson d’une femme</l>
						<l n="36" num="6.6"><space unit="char" quantity="12"></space>Ou bien d’un rossignol.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Tandis qu’il demeurait arrêté, la folâtre</l>
						<l n="38" num="7.2">Dans le feuillage épais, à son œil idolâtre</l>
						<l n="39" num="7.3"><space unit="char" quantity="12"></space>Se cachait avec soin,</l>
						<l n="40" num="7.4">Puis se taisait et puis, tout à coup, sous la vigne,</l>
						<l n="41" num="7.5">Capricieusement montrait son cou de cygne</l>
						<l n="42" num="7.6"><space unit="char" quantity="12"></space>En souriant de loin.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Mais quand le mendiant, chancelant et sans guide,</l>
						<l n="44" num="8.2">Passait vers le midi sur le chemin aride,</l>
						<l n="45" num="8.3"><space unit="char" quantity="12"></space>Sous le soleil en feu,</l>
						<l n="46" num="8.4">Elle accueillait du cœur sa plainte abandonnée,</l>
						<l n="47" num="8.5">Et rompait avec lui ce pain de la journée</l>
						<l n="48" num="8.6"><space unit="char" quantity="12"></space>Que l’on demande à Dieu.</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Le pauvre s’arrêtait avec un long sourire ;</l>
						<l n="50" num="9.2">Délassant ses pieds nus que la ronce déchire</l>
						<l n="51" num="9.3"><space unit="char" quantity="12"></space>Et ses membres perclus ;</l>
						<l n="52" num="9.4">Puis, lorsqu’il reprenait sa pesante besace,</l>
						<l n="53" num="9.5">Longtemps encor des yeux elle suivait sa trace,</l>
						<l n="54" num="9.6"><space unit="char" quantity="12"></space>Triste et ne chantant plus !</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">Juillet finit à peine : eh bien ! devant sa porte,</l>
						<l n="56" num="10.2">Voyez la jeune fille assise, demi-morte,</l>
						<l n="57" num="10.3"><space unit="char" quantity="12"></space>Au soleil sur le seuil,</l>
						<l n="58" num="10.4">Laissant errer ses yeux qu’ici-bas rien n’arrête,</l>
						<l n="59" num="10.5">Faible, pâle, immobile, et déjà comme prête</l>
						<l n="60" num="10.6"><space unit="char" quantity="12"></space>A descendre au cercueil.</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">Son teint ne rougit plus que des feux de la fièvre,</l>
						<l n="62" num="11.2">La brûlante insomnie a séché sur sa lèvre</l>
						<l n="63" num="11.3"><space unit="char" quantity="12"></space>Le rire et la chanson :</l>
						<l n="64" num="11.4">Elle meurt, pauvre épi rongé dans sa racine,</l>
						<l n="65" num="11.5">Qui jaunit sans mûrir, se dessèche et s’incline</l>
						<l n="66" num="11.6"><space unit="char" quantity="12"></space>Bien avant la moisson !</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">Le pauvre et le passant sur le chemin écoutent,</l>
						<l n="68" num="12.2">Cherchent des yeux l’enfant belle et rieuse, et doutent,</l>
						<l n="69" num="12.3"><space unit="char" quantity="12"></space>Et retardent leurs pas.</l>
						<l n="70" num="12.4">Elle est là devant eux l’enfant belle et rieuse,</l>
						<l n="71" num="12.5">Et l’indigence même, à son tour oublieuse,</l>
						<l n="72" num="12.6"><space unit="char" quantity="12"></space>Ne la reconnaît pas.</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1">Voilà donc ce que sont la jeunesse et la joie !</l>
						<l n="74" num="13.2">Qui pourrait aujourd’hui passer par cette voie</l>
						<l n="75" num="13.3"><space unit="char" quantity="12"></space>Sans fléchir les genoux ?’</l>
						<l n="76" num="13.4">La mort reprend sitôt ce que la vie accorde !</l>
						<l n="77" num="13.5">Seigneur, Dieu de clémence et de miséricorde,</l>
						<l n="78" num="13.6"><space unit="char" quantity="12"></space>Ayez pitié de nous !</l>
					</lg>
				</div></body></text></TEI>