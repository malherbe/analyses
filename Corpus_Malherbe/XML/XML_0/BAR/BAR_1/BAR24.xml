<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR24">
				<head type="main">Les Spectres</head>
				<opener>
					<salute>À M. B.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Vous les connaissez bien ces amants des clairières,</l>
					<l n="2" num="1.2">Ces spectres, revenant, de la tombe transis,</l>
					<l n="3" num="1.3">Sous la lune bleuâtre et ses pâles lumières…</l>
					<l n="4" num="1.4"><space quantity="12" unit="char"></space>Ils dansent dans les cimetières,</l>
					<l n="5" num="1.5"><space quantity="12" unit="char"></space>Mais dans mon cœur, ils sont assis.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Ils sont là, tous, assis avec mélancolie,</l>
					<l n="7" num="2.2">Dans l’immobilité des morts, sous leurs tombeaux :</l>
					<l n="8" num="2.3">Et pâles et navrés, croyant qu’on les oublie,</l>
					<l n="9" num="2.4">Ils ne se doutent pas qu’ils sont pour nous la Vie,</l>
					<l n="10" num="2.5"><space quantity="12" unit="char"></space>Plus puissants qu’elle et bien plus beaux !</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">O spectres des amours finis, — spectres de femmes,</l>
					<l n="12" num="3.2">Qui faites nos regrets pires que des remords…</l>
					<l n="13" num="3.3">Vous ne revenez pas que la nuit dans nos âmes…</l>
					<l n="14" num="3.4">Mais des jours les plus clairs vous noircissez les flammes</l>
					<l n="15" num="3.5"><space quantity="12" unit="char"></space>Et, morts, faites de nous des morts !</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1">Et toi, toi qui me crois vivant, — vivant encore,</l>
					<l n="17" num="4.2">Car je le redeviens sous tes regards si doux, —</l>
					<l n="18" num="4.3">Crains les sentiments fous des cœurs à leur aurore,</l>
					<l n="19" num="4.4">Et n’apprends pas qu’il est dans ce cœur qui t’adore</l>
					<l n="20" num="4.5"><space quantity="12" unit="char"></space>Un mur de mortes entre nous !</l>
				</lg>
			</div></body></text></TEI>