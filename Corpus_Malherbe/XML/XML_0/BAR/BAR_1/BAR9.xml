<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR9">
				<head type="main">Les Nénuphars</head>
				<opener>
					<salute>À la baronne de B…</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Nénuphars blancs, ô lys des eaux limpides,</l>
					<l n="2" num="1.2">Neige montant du fond de leur azur,</l>
					<l n="3" num="1.3">Qui, sommeillant sur vos tiges humides,</l>
					<l n="4" num="1.4">Avez besoin, pour dormir, d’un lit pur ;</l>
					<l n="5" num="1.5">Fleurs de pudeur, oui ! vous êtes trop fières</l>
					<l n="6" num="1.6">Pour vous laisser cueillir… et vivre après.</l>
					<l n="7" num="1.7">Nénuphars blanc, dormez sur vos rivières,</l>
					<l n="8" num="1.8"><space quantity="12" unit="char"></space>Je ne vous cueillerai jamais !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Nénuphars blancs, ô fleurs des eaux rêveuses,</l>
					<l n="10" num="2.2">Si vous rêvez, à quoi donc rêvez-vous ?…</l>
					<l n="11" num="2.3">Car pour rêver il faut être amoureuses,</l>
					<l n="12" num="2.4">Il faut avoir le cœur pris… ou jaloux ;</l>
					<l n="13" num="2.5">Mais vous, ô fleurs que l’eau baigne et protège,</l>
					<l n="14" num="2.6">Pour vous, rêver… c’est aspirer le frais !</l>
					<l n="15" num="2.7">Nénuphars blancs, dormez dans votre neige !</l>
					<l n="16" num="2.8"><space quantity="12" unit="char"></space>Je ne vous cueillerai jamais !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Nénuphars blancs, fleurs des eaux engourdies</l>
					<l n="18" num="3.2">Dont la blancheur fait froid aux cœurs ardents,</l>
					<l n="19" num="3.3">Qui vous plongez dans vos eaux détiédies</l>
					<l n="20" num="3.4">Quand le soleil y luit, Nénuphars blancs !</l>
					<l n="21" num="3.5">Restez cachés aux anses des rivières,</l>
					<l n="22" num="3.6">Dans les brouillards, sous les saules épais…</l>
					<l n="23" num="3.7">Des fleurs de Dieu vous êtes les dernières !</l>
					<l n="24" num="3.8"><space quantity="12" unit="char"></space>Je ne vous cueillerai jamais !</l>
				</lg>
			</div></body></text></TEI>