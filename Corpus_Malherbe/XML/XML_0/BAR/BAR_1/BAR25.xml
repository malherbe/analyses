<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR25">
				<head type="main">Chanson</head>
				<lg n="1">
					<l n="1" num="1.1">Oui ! restons masqués pour le monde !</l>
					<l n="2" num="1.2">Il ne vaut pas ce qu’il verrait</l>
					<l n="3" num="1.3">Dans notre intimité profonde,</l>
					<l n="4" num="1.4">S’il en surprenait le secret !</l>
					<l n="5" num="1.5">Il en abuserait, sans doute ;</l>
					<l n="6" num="1.6">Il est si cruel et si bas !</l>
					<l n="7" num="1.7">Ma Clara, pour toi je redoute</l>
					<l n="8" num="1.8">Ce que, toi, tu ne connais pas !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Toi, tu ne connais de la vie</l>
					<l n="10" num="2.2">Que ce qu’en a rêvé ton coeur…</l>
					<l n="11" num="2.3">Mais moi, Clara, je m’en défie…</l>
					<l n="12" num="2.4">Je sais ce qu’elle a de menteur.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Je sais combien font de blessures</l>
					<l n="14" num="3.2">Les cœurs jaloux aux cœurs heureux…</l>
					<l n="15" num="3.3">Nos masques seront nos armures !</l>
					<l n="16" num="3.4">Masquons-nous, Clara, tu le veux !</l>
				</lg>
				<lg n="4">
					<l n="17" num="4.1">Glace tes yeux charmants que j’aime ;</l>
					<l n="18" num="4.2">Fais mieux, ma Clara ! — remplis-les</l>
					<l n="19" num="4.3">De dédain, de cruauté même…</l>
					<l n="20" num="4.4">Ris de moi, je te le permets !</l>
					<l n="21" num="4.5">Que jamais on ne puisse dire :</l>
					<l n="22" num="4.6">« Voyez ! ils se font les yeux doux !</l>
					<l n="23" num="4.7">« Ils ont l’un sur l’autre un empire… »</l>
					<l n="24" num="4.8">Masquons-nous, Clara, masquons-nous !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Tu n’en seras pas moins charmante,</l>
					<l n="26" num="5.2">Et peut-être que tu seras,</l>
					<l n="27" num="5.3">Fausse, encore plus enivrante,</l>
					<l n="28" num="5.4">Et que mieux tu m’enivreras !</l>
					<l n="29" num="5.5">Le charme est si grand, du mystère !</l>
					<l n="30" num="5.6">Aux fronts blancs sied le masque noir…</l>
					<l n="31" num="5.7">Mentir, c’est mieux que de se taire ;</l>
					<l n="32" num="5.8">Se savoir, c’est plus que se voir !</l>
				</lg>
			</div></body></text></TEI>