<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR11">
				<lg n="1">
					<l n="1" num="1.1">Oh ! pourquoi voyager ? as-tu dit. C’est que l’âme</l>
					<l n="2" num="1.2">Se prend de longs ennuis et partout et toujours ;</l>
					<l n="3" num="1.3">C’est qu’il est un désir, ardent comme une flamme,</l>
					<l n="4" num="1.4">Qui, nos amours éteints, survit à nos amours !</l>
					<l n="5" num="1.5">C’est qu’on est mal ici ! — Comme les hirondelles,</l>
					<l n="6" num="1.6">Un vague instinct d’aller nous dévore à mourir ;</l>
					<l n="7" num="1.7">C’est qu’à nos cœurs, mon Dieu ! vous avez mis des ailes.</l>
					<l n="8" num="1.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">C’est que le cœur hennit en pensant aux voyages,</l>
					<l n="10" num="2.2">Plus fort que le coursier qui sellé nous attend ;</l>
					<l n="11" num="2.3">C’est qu’il est dans le nom des plus lointains rivages</l>
					<l n="12" num="2.4">Des charmes sans pareils pour celui qui l’entend ;</l>
					<l n="13" num="2.5">Irrésistible appel, ranz des vaches pour l’âme</l>
					<l n="14" num="2.6">Qui cherche son pays perdu — dans l’avenir ;</l>
					<l n="15" num="2.7">C’est fier comme un clairon, doux comme un chant de femme.</l>
					<l n="16" num="2.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">C’est que toi, pauvre enfant, et si jeune et si belle,</l>
					<l n="18" num="3.2">Qui vivais près de nous et couchais sur nos cœurs,</l>
					<l n="19" num="3.3">Tu n’as pas su dompter cette force rebelle</l>
					<l n="20" num="3.4">Qui nous jeta vers toi pour nous pousser ailleurs !</l>
					<l n="21" num="3.5">Tu n’as plus de mystère au fond de ton sourire,</l>
					<l n="22" num="3.6">Nous le connaissons trop pour jamais revenir ;</l>
					<l n="23" num="3.7">La chaîne des baisers se rompt, — l’amour expire…</l>
					<l n="24" num="3.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">En vain, tout en pleurant, la femme qui nous aime</l>
					<l n="26" num="4.2">Viendrait à notre épaule agrafer nos manteaux,</l>
					<l n="27" num="4.3">Nous resterions glacés à cet instant suprême ;</l>
					<l n="28" num="4.4">A trop couler pour nous des pleurs ne sont plus beaux.</l>
					<l n="29" num="4.5">Nous n’entendrions plus cette voix qui répète :</l>
					<l n="30" num="4.6">« Oh ! pourquoi voyager ? » dans un tendre soupir,</l>
					<l n="31" num="4.7">Et nous dirions adieu sans retourner la tête.</l>
					<l n="32" num="4.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Oh ! ne m’accuse pas ; accuse la nature,</l>
					<l n="34" num="5.2">Accuse Dieu plutôt, — mais ne m’accuse pas !</l>
					<l n="35" num="5.3">Est-ce ma faute, à moi, si dans la vie obscure</l>
					<l n="36" num="5.4">Mes yeux ont soif de jour, mes pieds ont soif de pas ?</l>
					<l n="37" num="5.5">Si je n’ai pu rester à languir sur ta couche,</l>
					<l n="38" num="5.6">Si tes bras m’étouffaient sans me faire mourir,</l>
					<l n="39" num="5.7">S’il me fallait plus d’air qu’il n’en peut dans ta bouche</l>
					<l n="40" num="5.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">Pourquoi ne pouvais-tu suffire à ma pensée</l>
					<l n="42" num="6.2">Et tes yeux n’être plus que mes seuls horizons ?</l>
					<l n="43" num="6.3">Pourquoi ne pas cacher ma tête reposée</l>
					<l n="44" num="6.4">Sous les abris d’or pur de tes longs cheveux blonds ?</l>
					<l n="45" num="6.5">Comme la jeune épouse endormie à l’aurore,</l>
					<l n="46" num="6.6">La fleur d’amour, comme elle, au soir va se rouvrir…</l>
					<l n="47" num="6.7">Mais si l’amour n’est plus, pourquoi de l’âme encore ?</l>
					<l n="48" num="6.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1">Tu ne la connais pas, cette vie ennuyée,</l>
					<l n="50" num="7.2">Lasse de pendre au mât, avide d’ouragan.</l>
					<l n="51" num="7.3">Toi, tu restes toujours, sur ton coude appuyée,</l>
					<l n="52" num="7.4">A voir stagner la tienne ainsi qu’un bel étang.</l>
					<l n="53" num="7.5">Restes-y ! Mon amour fut l’ombre d’un nuage</l>
					<l n="54" num="7.6">Sur l’étang ; — le soleil y reviendra frémir !</l>
					<l n="55" num="7.7">Tu ne garderas pas trace de mon passage…</l>
					<l n="56" num="7.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1">Ô coupe de vermeil où j’ai puisé la vie,</l>
					<l n="58" num="8.2">Je ne t’emporte pas dans mon sein tout glacé !</l>
					<l n="59" num="8.3">Reste derrière moi, reste à demi remplie,</l>
					<l n="60" num="8.4">Offrande à l’avenir et débris du passé.</l>
					<l n="61" num="8.5">Je peux boire à présent, sans que trop il m’en coûte,</l>
					<l n="62" num="8.6">Un breuvage moins doux et moins prompt à tarir,</l>
					<l n="63" num="8.7">Dans le creux de mes mains, aux fossés de la route…</l>
					<l n="64" num="8.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="9">
					<l n="65" num="9.1">Mais, si c’est t’offenser que partir, oh ! pardonne ;</l>
					<l n="66" num="9.2">Quoique de ces douleurs dont tu n’eus point ta part,</l>
					<l n="67" num="9.3">Rien, hélas ! (et pourtant autrefois tu fus bonne !)</l>
					<l n="68" num="9.4">Ne saurait racheter le crime du départ.</l>
					<l n="69" num="9.5">Pourquoi t’associerais-je à mon triste voyage ?</l>
					<l n="70" num="9.6">Lorsque tu le pourrais, oserais-tu venir ?</l>
					<l n="71" num="9.7">Plus sombre que Lara, je n’aurai point de page…</l>
					<l n="72" num="9.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="10">
					<l n="73" num="10.1">Et qu’importe un pardon ! — Innocent ou coupable,</l>
					<l n="74" num="10.2">On n’est jamais fidèle ou parjure à moitié ;</l>
					<l n="75" num="10.3">Le cœur, sans être dur, demeure inébranlable,</l>
					<l n="76" num="10.4">Et l’oubli lui vaut mieux qu’une vaine pitié.</l>
					<l n="77" num="10.5">Ah ! l’oubli ! quel repos quand notre âme est lassée !</l>
					<l n="78" num="10.6">Endors-toi dans ses bras, sans rêver ni souffrir…</l>
					<l n="79" num="10.7">Je ne veux rien de toi… pas même une pensée !</l>
					<l n="80" num="10.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="11">
					<l n="81" num="11.1">Car il est, tu le sais, ô femme abandonnée,</l>
					<l n="82" num="11.2">Un voyageur plus vieux, plus sans pitié que moi,</l>
					<l n="83" num="11.3">Et ce n’est pas un jour, quelques mois, une année,</l>
					<l n="84" num="11.4">Mais c’est tout qu’il doit prendre, aux autres comme à toi !</l>
					<l n="85" num="11.5">Tel que des épis d’or sciés d’un bras avide,</l>
					<l n="86" num="11.6">Il prend beauté, bonheur, et jusqu’au souvenir,</l>
					<l n="87" num="11.7">Fait sa gerbe et s’en va du champ qu’il laisse aride…</l>
					<l n="88" num="11.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
				<lg n="12">
					<l n="89" num="12.1">Oui ! partir avant lui, partir avant qu’il vienne !</l>
					<l n="90" num="12.2">Te laisser belle encor sous tes pleurs répandus,</l>
					<l n="91" num="12.3">Ne pas chercher ta main qui froidit dans la mienne,</l>
					<l n="92" num="12.4">Et, sous un front terni, tes yeux, astres perdus !</l>
					<l n="93" num="12.5">N’eût-on que le respect de celle qui fut belle</l>
					<l n="94" num="12.6">Il faudrait s’épargner de la voir se flétrir,</l>
					<l n="95" num="12.7">Puisque Dieu ne veut pas qu’elle soit immortelle !</l>
					<l n="96" num="12.8"><space quantity="12" unit="char"></space>Voilà pourquoi je veux partir !</l>
				</lg>
			</div></body></text></TEI>