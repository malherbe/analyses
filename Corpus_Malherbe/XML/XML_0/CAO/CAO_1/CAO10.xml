<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO10">
					<head type="main">LA SAINT-JEAN-BAPTISTE</head>
					<opener>
						<salute>A M. AMÉDÉE ROBITAILLE <lb></lb>Président général de la société St-Jean-Baptiste.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Quand brille à l’horizon le jour de la patrie,</l>
						<l n="2" num="1.2">Les Canadiens-Français, l’âme toute attendrie,</l>
						<l n="3" num="1.3">Célèbrent des aïeux les vertus, les exploits ;</l>
						<l n="4" num="1.4">Et, léguant à l’oubli tout ce qui les divise,</l>
						<l n="5" num="1.5">Ils suivent l’étendard qui porte leur devise :</l>
						<l n="6" num="1.6">« Nos institutions, notre langue et nos lois ! »</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Ils marchent, le front haut, sur ce sol où leurs pères</l>
						<l n="8" num="2.2">Ont posé les jalons de ces villes prospères</l>
						<l n="9" num="2.3">Que le touriste admire aux bords du Saint-Laurent.</l>
						<l n="10" num="2.4">Ils s’arrêtent parfois dans leur pèlerinage</l>
						<l n="11" num="2.5">Pour saluer le nom d’un noble personnage</l>
						<l n="12" num="2.6">Buriné sur l’airain d’un humble monument.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Ils vont se recueillir un instant dans le temple</l>
						<l n="14" num="3.2">Sous le tendre regard de Dieu qui les contemple</l>
						<l n="15" num="3.3">Et les fait triompher d’ennemis dangereux ;</l>
						<l n="16" num="3.4">Ils retrempent leur foi ‒ la foi des leurs ancêtres ‒</l>
						<l n="17" num="3.5">Que savent leur transmettre une foule de prêtres</l>
						<l n="18" num="3.6">Aussi braves et saints que Brébeuf et Buteux.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Et lorsqu’ils ont offert au ciel un pur hommage,</l>
						<l n="20" num="4.2">Ils retournent chacun festoyer sous l’ombrage</l>
						<l n="21" num="4.3">Des érables plantés en l’honneur de saint Jean.</l>
						<l n="22" num="4.4">O les joyeux refrains que chantent les poitrines</l>
						<l n="23" num="4.5">Que de mots répétés par des voix argentines</l>
						<l n="24" num="4.6">Et qui mettent la joie au cœur de l’indigent…</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Puis, le soir, ils s’en vont sur la place publique</l>
						<l n="26" num="5.2">Où d’éloquents tribuns, à la voix sympathique,</l>
						<l n="27" num="5.3">Redisent la valeur de ceux qui ne sont plus ;</l>
						<l n="28" num="5.4">Il sont heureux d’entendre exalter la mémoire</l>
						<l n="29" num="5.5">De ces fameux héros dont nous parle l’histoire,</l>
						<l n="30" num="5.6">Et jurent d’imiter leurs brillantes vertus !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">O Canadiens-Français d’une même croyance,</l>
						<l n="32" num="6.2">Vous dont le fier esprit égale la vaillance,</l>
						<l n="33" num="6.3"><space unit="char" quantity="8"></space>Fêtez avec éclat ce jour !</l>
						<l n="34" num="6.4">Portant de Carillon l’immortelle bannière</l>
						<l n="35" num="6.5">Allez au champ d’honneur vénérer la poussière</l>
						<l n="36" num="6.6"><space unit="char" quantity="8"></space>Des guerriers morts pour votre amour !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1889">Juin 1889</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>