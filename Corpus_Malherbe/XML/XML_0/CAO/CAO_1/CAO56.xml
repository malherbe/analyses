<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HYMNES, ROMANCES ET CHANSONNETTES</head><div type="poem" key="CAO56">
					<head type="main">AUX RAQUETTEURS DE SHERBROOKE</head>
					<head type="tune">Air : « Hiouppe ! Hiouppe ! sur la rivière, etc. »</head>
					<div type="section" n="1">
						<head type="main">I</head>
						<lg n="1">
							<l n="1" num="1.1"><space unit="char" quantity="4"></space>Sherbrooke, c’est la ville</l>
							<l n="2" num="1.2"><space unit="char" quantity="4"></space>Où la franche gaîté</l>
							<l n="3" num="1.3"><space unit="char" quantity="4"></space>Sur tous les fronts scintille,</l>
							<l n="4" num="1.4"><space unit="char" quantity="4"></space>L’hiver comme l’été.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="5" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="6" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="7" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="8" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">II</head>
						<lg n="1">
							<l n="9" num="1.1"><space unit="char" quantity="4"></space>L’on vante sa largesse,</l>
							<l n="10" num="1.2"><space unit="char" quantity="4"></space>Son hospitalité,</l>
							<l n="11" num="1.3"><space unit="char" quantity="4"></space>Sa grande politesse</l>
							<l n="12" num="1.4"><space unit="char" quantity="4"></space>Et son urbanité.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="13" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="14" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="15" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="16" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">III</head>
						<lg n="1">
							<l n="17" num="1.1"><space unit="char" quantity="4"></space>Ses habitants s’amusent</l>
							<l n="18" num="1.2"><space unit="char" quantity="4"></space>Avec moralité,</l>
							<l n="19" num="1.3"><space unit="char" quantity="4"></space>Mais jamais ne refusent</l>
							<l n="20" num="1.4"><space unit="char" quantity="4"></space>De boire une santé !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="21" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="22" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="23" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="24" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="main">IV</head>
						<lg n="1">
							<l n="25" num="1.1"><space unit="char" quantity="4"></space>Ils aiment la raquette</l>
							<l n="26" num="1.2"><space unit="char" quantity="4"></space>Puis savent la porter ;</l>
							<l n="27" num="1.3"><space unit="char" quantity="4"></space>Leur gentille toilette</l>
							<l n="28" num="1.4"><space unit="char" quantity="4"></space>Fait plus d’un cœur sauter.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="29" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="30" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="31" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="32" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="main">V</head>
						<lg n="1">
							<l n="33" num="1.1"><space unit="char" quantity="4"></space>Ils sont déjà quarante,</l>
							<l n="34" num="1.2"><space unit="char" quantity="4"></space>A part le comité,</l>
							<l n="35" num="1.3"><space unit="char" quantity="4"></space>Et compteront soixante</l>
							<l n="36" num="1.4"><space unit="char" quantity="4"></space>Avant la Trinité !</l>
						</lg>
					</div>
					<div type="section" n="6">
						<head type="main">REFRAIN :</head>
						<lg n="1">
							<l rhyme="none" n="37" num="1.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="38" num="1.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="39" num="1.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="40" num="1.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="7">
						<head type="main">VI</head>
						<lg n="1">
							<l n="41" num="1.1"><space unit="char" quantity="4"></space>Car toute la jeunesse</l>
							<l n="42" num="1.2"><space unit="char" quantity="4"></space>Désire <hi rend="ital">raquetter</hi> ;</l>
							<l n="43" num="1.3"><space unit="char" quantity="4"></space>Elle comprend l’ivresse</l>
							<l n="44" num="1.4"><space unit="char" quantity="4"></space>Qu’on éprouve à trotter.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="45" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="46" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="47" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="48" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="8">
						<head type="main">VII</head>
						<lg n="1">
							<l n="49" num="1.1"><space unit="char" quantity="4"></space>Et, bravant la tempête,</l>
							<l n="50" num="1.2"><space unit="char" quantity="4"></space>Le froid, l’humidité,</l>
							<l n="51" num="1.3"><space unit="char" quantity="4"></space>Elle dit et répète :</l>
							<l n="52" num="1.4"><space unit="char" quantity="4"></space>Courir, c’est la santé !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="53" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="54" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="55" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="56" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="9">
						<head type="main">VIII</head>
						<lg n="1">
							<l n="57" num="1.1"><space unit="char" quantity="4"></space>Honneur à la raquette</l>
							<l n="58" num="1.2"><space unit="char" quantity="4"></space>A son ancienneté,</l>
							<l n="59" num="1.3"><space unit="char" quantity="4"></space>A sa forme coquette,</l>
							<l n="60" num="1.4"><space unit="char" quantity="4"></space>A son utilité.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="61" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="62" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="63" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="64" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="10">
						<head type="main">IX</head>
						<lg n="1">
							<l n="65" num="1.1"><space unit="char" quantity="4"></space>Ce soulier poétique</l>
							<l n="66" num="1.2"><space unit="char" quantity="4"></space>Fut jadis inventé,</l>
							<l n="67" num="1.3"><space unit="char" quantity="4"></space>Sur le sol d’Amérique</l>
							<l n="68" num="1.4"><space unit="char" quantity="4"></space>Par un homme futé !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="69" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="70" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="71" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="72" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="11">
						<head type="main">X</head>
						<lg n="1">
							<l n="73" num="1.1"><space unit="char" quantity="4"></space>Il légua son ouvrage</l>
							<l n="74" num="1.2"><space unit="char" quantity="4"></space>A la postérité,</l>
							<l n="75" num="1.3"><space unit="char" quantity="4"></space>Qui, depuis d’âge en âge,</l>
							<l n="76" num="1.4"><space unit="char" quantity="4"></space>L’a toujours imité.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="77" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="78" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="79" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="80" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="12">
						<head type="main">XI</head>
						<lg n="1">
							<l n="81" num="1.1"><space unit="char" quantity="4"></space>O raquette, nos pères</l>
							<l n="82" num="1.2"><space unit="char" quantity="4"></space>Aiment à te porter ;</l>
							<l n="83" num="1.3"><space unit="char" quantity="4"></space>Ils ne te laissent guères</l>
							<l n="84" num="1.4"><space unit="char" quantity="4"></space>Qu’un instant pour lutter !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="85" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="86" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="87" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="88" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="13">
						<head type="main">XII</head>
						<lg n="1">
							<l n="89" num="1.1"><space unit="char" quantity="4"></space>Et nos bons missionnaires,</l>
							<l n="90" num="1.2"><space unit="char" quantity="4"></space>Prêchant la vérité,</l>
							<l n="91" num="1.3"><space unit="char" quantity="4"></space>Sur raquettes légères</l>
							<l n="92" num="1.4"><space unit="char" quantity="4"></space>Ont mainte fois monté.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="93" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="94" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="95" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="96" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="14">
						<head type="main">XIII</head>
						<lg n="1">
							<l n="97" num="1.1"><space unit="char" quantity="4"></space>Nous sommes de leur race :</l>
							<l n="98" num="1.2"><space unit="char" quantity="4"></space>C’est là notre fierté !</l>
							<l n="99" num="1.3"><space unit="char" quantity="4"></space>Comme eux, fendons l’espace</l>
							<l n="100" num="1.4"><space unit="char" quantity="4"></space>Avec agilité !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="101" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="102" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="103" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="104" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="15">
						<head type="main">XIV</head>
						<lg n="1">
							<l n="105" num="1.1"><space unit="char" quantity="4"></space>Que le vieux et le jeune</l>
							<l n="106" num="1.2"><space unit="char" quantity="4"></space>Exempts d’infirmité,</l>
							<l n="107" num="1.3"><space unit="char" quantity="4"></space>Se présentent sans gêne</l>
							<l n="108" num="1.4"><space unit="char" quantity="4"></space>Devant le comité.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="109" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="110" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="111" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="112" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
					<div type="section" n="16">
						<head type="main">XV</head>
						<lg n="1">
							<l n="113" num="1.1"><space unit="char" quantity="4"></space>Nous leur disons d’avance :</l>
							<l n="114" num="1.2"><space unit="char" quantity="4"></space>Vous serez acceptés.</l>
							<l n="115" num="1.3"><space unit="char" quantity="4"></space>Car les fils de la France</l>
							<l n="116" num="1.4"><space unit="char" quantity="4"></space>Par nous sont bien traités !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="117" num="2.1">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="118" num="2.2"><space unit="char" quantity="4"></space>Chantant la chansonnette,</l>
							<l rhyme="none" n="119" num="2.3">Hiouppe ! Hiouppe ! sur la raquette</l>
							<l rhyme="none" n="120" num="2.4"><space unit="char" quantity="4"></space>Nous ne fatiguons pas !</l>
						</lg>
					</div>
				</div></body></text></TEI>