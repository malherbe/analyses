<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO11">
					<head type="main">IL SERA PRÊTRE !</head>
					<opener>
						<salute>A MADAME L. G. V…</salute>
						<epigraph>
							<cit>
								<quote>
									Le prêtre est un pont jeté entre le ciel et <lb></lb>
									la terre. Le jour où il n’y aurait plus <lb></lb>
									de prêtres, le monde s’abîmerait dans une <lb></lb>
									immense ruine.
								</quote>
								<bibl>
									<name></name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">C’était un beau matin. Les cloches de l’église</l>
						<l n="2" num="1.2">Mêlaient joyeusement aux accords de la brise</l>
						<l n="3" num="1.3"><space unit="char" quantity="12"></space>Leurs sons harmonieux ;</l>
						<l n="4" num="1.4">Le peuple agenouillé dans notre basilique,</l>
						<l n="5" num="1.5">Adressait en son cœur une douce supplique</l>
						<l n="6" num="1.6"><space unit="char" quantity="12"></space>Au Monarque des cieux.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">A l’autel se tenaient douze jeunes lévites</l>
						<l n="8" num="2.2">Venus pour dire au monde, aux plaisirs illicites</l>
						<l n="9" num="2.3"><space unit="char" quantity="12"></space>Un éternel adieu ;</l>
						<l n="10" num="2.4">Leurs lèvres murmuraient d’ineffables prières</l>
						<l n="11" num="2.5">Et des larmes d’amour nageaient sous leurs paupières</l>
						<l n="12" num="2.6"><space unit="char" quantity="12"></space>Quand ils firent le vœu.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Que c’est donc merveilleux cette cérémonie !</l>
						<l n="14" num="3.2">Quel cachet de grandeur, de sainte poésie</l>
						<l n="15" num="3.3"><space unit="char" quantity="12"></space>Ne contient-elle pas ?</l>
						<l n="16" num="3.4">Et ces fils d’Adam, nés comme nous dans les larmes,</l>
						<l n="17" num="3.5">Livreront à satan et ses compagnons d’armes</l>
						<l n="18" num="3.6"><space unit="char" quantity="12"></space>Des valeureux combats !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Quelle langue pourrait, ô noble et digne femme !</l>
						<l n="20" num="4.2">Exprimer le bonheur dont fut pleine votre âme</l>
						<l n="21" num="4.3"><space unit="char" quantity="12"></space>Au « vœu » de votre enfant ?</l>
						<l n="22" num="4.4">Ah ! vous étiez heureuse au delà de tout rêve,</l>
						<l n="23" num="4.5">Car l’évêque sacrait, ô pauvre fille d’Ève,</l>
						<l n="24" num="4.6"><space unit="char" quantity="12"></space>Le sang de votre sang !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Oui, vous étiez heureuse, ô bonne et tendre mère,</l>
						<l n="26" num="5.2">Plus que si des honneurs la couronne éphémère</l>
						<l n="27" num="5.3"><space unit="char" quantity="12"></space>Eût ceint ce front aimé ;</l>
						<l n="28" num="5.4">Heureuse jusqu’au point de croire que Dieu même</l>
						<l n="29" num="5.5">N’avait jamais offert de plus beau diadème</l>
						<l n="30" num="5.6"><space unit="char" quantity="12"></space>En son ciel embaumé.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Réjouissez-vous bien, naïve et sainte femme !</l>
						<l n="32" num="6.2">Exaltez cet enfant que l’Église proclame</l>
						<l n="33" num="6.3"><space unit="char" quantity="12"></space>Un dévoué pasteur ;</l>
						<l n="34" num="6.4">Contemplez son regard où la pureté brille,</l>
						<l n="35" num="6.5">Son front calme et serein où la grâce scintille,</l>
						<l n="36" num="6.6"><space unit="char" quantity="12"></space>Ses traits pleins de douceur !</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Vous l’aimiez !… Cependant lorsqu’il vous fit connaître</l>
						<l n="38" num="7.2">Que le ciel l’appelait à devenir un prêtre,</l>
						<l n="39" num="7.3"><space unit="char" quantity="12"></space>L’ami des malheureux,</l>
						<l n="40" num="7.4">Alors vous avez dit, avec le saint prophète ;</l>
						<l n="41" num="7.5">« Que votre volonté, verbe divin soit faite</l>
						<l n="42" num="7.6"><space unit="char" quantity="12"></space>Ici-bas comme aux cieux ! »</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Il sera prêtre ! Ainsi, joyeux, il abandonne</l>
						<l n="44" num="8.2">Les passagers plaisirs auxquels l’homme s’adonne,</l>
						<l n="45" num="8.3"><space unit="char" quantity="12"></space>Et qui font son malheur ;</l>
						<l n="46" num="8.4">Il quitte sans regret amis, parents richesses ;</l>
						<l n="47" num="8.5">Son cœur ‒ brûlant foyer des pures allégresses ‒</l>
						<l n="48" num="8.6"><space unit="char" quantity="12"></space>Palpite avec ardeur !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Ses mains que vous pressiez jadis avec tendresse,</l>
						<l n="50" num="9.2">Toucheront désormais, durant la sainte messe,</l>
						<l n="51" num="9.3"><space unit="char" quantity="12"></space>Le corps, le sang de Dieu ;</l>
						<l n="52" num="9.4">Ses pieds qu’avec amour vous baisiez dans les langes</l>
						<l n="53" num="9.5">Serviront à porter l’auguste pains des anges</l>
						<l n="54" num="9.6"><space unit="char" quantity="12"></space>Aux mortels, en tout lieu !</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">Femme, vous n’aurez pas l’orgueil d’être grand’mère,</l>
						<l n="56" num="10.2">Mais votre fils unique aura, sur cette terre,</l>
						<l n="57" num="10.3"><space unit="char" quantity="12"></space>Une postérité :</l>
						<l n="58" num="10.4">Elle renfermera le grand, le prolétaire ;</l>
						<l n="59" num="10.5">Le vieillard et l’enfant le nommeront « mon père »,</l>
						<l n="60" num="10.6"><space unit="char" quantity="12"></space>L’œil brillant de fierté.</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">Il sera prêtre ! Aussi que de brebis errantes</l>
						<l n="62" num="11.2">Reprendront sous ses soins, heureuses, repentantes,</l>
						<l n="63" num="11.3"><space unit="char" quantity="12"></space>La route du bercail ;</l>
						<l n="64" num="11.4">Et que de malheureux, guidés par sa parole,</l>
						<l n="65" num="11.5">A son exemple, iront, de l’Équateur au Pôle,</l>
						<l n="66" num="11.6"><space unit="char" quantity="12"></space>Achever son travail !</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">Nouveau Vincent de Paul, cet homme charitable</l>
						<l n="68" num="12.2">Pressera sur son sein le pauvre misérable,</l>
						<l n="69" num="12.3"><space unit="char" quantity="12"></space>Abandonné de tous ;</l>
						<l n="70" num="12.4">Il lui prodiguera les plus grandes tendresses,</l>
						<l n="71" num="12.5">Et ce pauvre, touché, contera ses faiblesses</l>
						<l n="72" num="12.6"><space unit="char" quantity="12"></space>En tombant à genoux !</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1">Puis, lorsque les méchants, le cœur rempli de rage</l>
						<l n="74" num="13.2">Maudiront, saliront de leur ignoble outrage</l>
						<l n="75" num="13.3"><space unit="char" quantity="12"></space>L’apôtre du Seigneur,</l>
						<l n="76" num="13.4">Alors cet homme saint sentira dans son âme</l>
						<l n="77" num="13.5">Un amour plus ardent, une plus vive flamme</l>
						<l n="78" num="13.6"><space unit="char" quantity="12"></space>Pour le faible pécheur ?</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1">Il est consacré prêtre ! Et vous, sa bonne mère,</l>
						<l n="80" num="14.2">Vous goûtez ardemment sa parole sincère,</l>
						<l n="81" num="14.3"><space unit="char" quantity="12"></space>Pleine d’émotion.</l>
						<l n="82" num="14.4">Vous assistez tremblante, à la première messe</l>
						<l n="83" num="14.5">De ce fils qui vous donne ‒ ô sublime caresse ! ‒</l>
						<l n="84" num="14.6"><space unit="char" quantity="12"></space>Sa bénédiction…</l>
					</lg>
					<lg n="15">
						<l n="85" num="15.1">Femme, allez maintenant à vos œuvres pieuses,</l>
						<l n="86" num="15.2">Et lorsque sonneront les heures douloureuses,</l>
						<l n="87" num="15.3"><space unit="char" quantity="12"></space>Pensez à votre enfant ;</l>
						<l n="88" num="15.4">Pensez aux doux bienfaits qu’il sème sur la terre :</l>
						<l n="89" num="15.5">Ce souvenir sera le baume salutaire</l>
						<l n="90" num="15.6"><space unit="char" quantity="12"></space>De votre cœur souffrant</l>
					</lg>
					<closer>
						<dateline>
							<date when="1879">Juin 1879</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>