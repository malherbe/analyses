<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO29">
					<head type="main">ÉLÉGIE</head>
					 <opener>
						<salute>A MONSIEUR E. G…. qui vient de perdre sa femme.</salute>
					 </opener>
					<lg n="1">
						<l n="1" num="1.1">Tout est fini ! La tombe</l>
						<l n="2" num="1.2">Te couvre pour toujours…</l>
						<l n="3" num="1.3">Mon pauvre cœur succombe</l>
						<l n="4" num="1.4">Sous le fardeau des jours…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Dieu m’a ravi la joie</l>
						<l n="6" num="2.2">En t’appelant aux cieux,</l>
						<l n="7" num="2.3">Et la douleur déploie</l>
						<l n="8" num="2.4">Son voile sur mes yeux !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Du haut du ciel, ô femme</l>
						<l n="10" num="3.2">Veille sur nos enfants,</l>
						<l n="11" num="3.3">Afin que leur jeune âme</l>
						<l n="12" num="3.4">Ressemble au pur encens.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Obtiens-leur l’avantage</l>
						<l n="14" num="4.2">D’aimer le doux Jésus,</l>
						<l n="15" num="4.3">De suivre sa loi sage,</l>
						<l n="16" num="4.4">D’imiter ses vertus</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Et lorsque la souffrance</l>
						<l n="18" num="5.2">Viendra les visiter,</l>
						<l n="19" num="5.3">Donne-leur la vaillance</l>
						<l n="20" num="5.4">De bien la supporter.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Oui, fais qu’à ton exemple,</l>
						<l n="22" num="6.2">Au jour de la douleur,</l>
						<l n="23" num="6.3">Ils aillent dans le temple</l>
						<l n="24" num="6.4">Implorer le Seigneur.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Et moi qui suis le père</l>
						<l n="26" num="7.2">De ces trois malheureux,</l>
						<l n="27" num="7.3">Je serai, je l’espère,</l>
						<l n="28" num="7.4">Un modèle pour eux.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Adieu, femme adorée !</l>
						<l n="30" num="8.2">Dors sous ce tertre en fleurs</l>
						<l n="31" num="8.3">Que mon âme navrée</l>
						<l n="32" num="8.4">Féconde de ses pleurs !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1886">15 septembre 1886</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>