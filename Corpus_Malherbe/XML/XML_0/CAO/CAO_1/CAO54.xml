<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HYMNES, ROMANCES ET CHANSONNETTES</head><div type="poem" key="CAO54">
					<head type="main">LA CRÈCHE DE NOËL <ref target="7" type="noteAnchor">7</ref></head>
					<head type="main">Musique de M. N. Crépault</head>
					<div type="section" n="1">
					<head type="main">I</head>
						<lg n="1">
							<l n="1" num="1.1">L’âpre saison déroule sur la terre</l>
							<l n="2" num="1.2">Son lourd manteau de neige et de frimas ;</l>
							<l n="3" num="1.3">Le vent du soir soupire avec mystère</l>
							<l n="4" num="1.4">Dans la ramure où brille le verglas.</l>
							<l n="5" num="1.5">Il est minuit. Le carillon du temple</l>
							<l n="6" num="1.6">Jette aux échos un hymne triomphant,</l>
							<l n="7" num="1.7">Et le chrétien, à deux genoux, contemple <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="8" num="1.8">Avec amour un adorable enfant <del reason="analysis" hand="RR" type="repetition">(bis)</del>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">II</head>
						<lg n="1">
							<l n="9" num="1.1">Il est plus grand que tous les rois du monde,</l>
							<l n="10" num="1.2">Plus radieux que l’astre universel,</l>
							<l n="11" num="1.3">Plus éloquent que la foudre qui gronde,</l>
							<l n="12" num="1.4">Plus pur et saint que les anges du ciel !</l>
							<l n="13" num="1.5">Et cependant, il est né sur la paille ;</l>
							<l n="14" num="1.6">Son divin corps éprouve des douleurs…</l>
							<l n="15" num="1.7">Que l’univers d’allégresse tressaille, <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="16" num="1.8">Car cet enfant rachète nos malheurs ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">III</head>
						<lg n="1">
							<l n="17" num="1.1">Au front du ciel une étoile rayonne,</l>
							<l n="18" num="1.2">Guidant les pas des rois les plus puissants</l>
							<l n="19" num="1.3">Qui vont offrir ‒ en guise de couronne ‒</l>
							<l n="20" num="1.4">Au nouveau-né l’or, la myrrhe et l’encens !</l>
							<l n="21" num="1.5">Allons chrétiens, à l’exemple des Mages,</l>
							<l n="22" num="1.6">Nous prosterner devant le Rédempteur !</l>
							<l n="23" num="1.7">Adressons-lui nos vertueux hommages <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="24" num="1.8">Et redisons : Gloire au Libérateur ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<closer>
						<dateline>
							<date when="1887">Décembre 1887</date>.
						</dateline>
						<note type="footnote" id="7">[Dédié au révérend M.F.-H. Bélanger, curé de St-Roch, Québec.]</note>
					</closer>
				</div></body></text></TEI>