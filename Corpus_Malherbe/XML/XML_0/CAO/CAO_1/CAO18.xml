<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO18">
					<head type="main">QU’EST-CE QUE LA VIE ?</head>
					<head type="form">Pièce traduite de « <hi rend="ital">What is Life ?</hi> » de Samuel Moore</head>
					<lg n="1">
						<l n="1" num="1.1">Je demandais un jour à l’un de ces vieillards,</l>
						<l n="2" num="1.2">Dont la pâle figure et les sombres regards</l>
						<l n="3" num="1.3">Accusent la souffrance et l’amère ironie,</l>
						<l n="4" num="1.4">S’il pouvait m’expliquer ce simple mot : la vie ?</l>
						<l n="5" num="1.5">Courbant sa tête blanche, il dit en soupirant :</l>
						<l n="6" num="1.6">« La vie est une scène où le pauvre et le grand</l>
						<l n="7" num="1.7">Luttent pour obtenir l’honneur et la richesse ;</l>
						<l n="8" num="1.8">Quelques rayons d’amour, de joie et de tristesse ;</l>
						<l n="9" num="1.9">Des efforts pour saisir un brillant lendemain ;</l>
						<l n="10" num="1.10">Une flamme qui luit et disparaît soudain ;</l>
						<l n="11" num="1.11">Un flot que le torrent caresse, agite, emporte ;</l>
						<l n="12" num="1.12">Une rose qui naît et bientôt sera morte ;</l>
						<l n="13" num="1.13">La vie est ce chemin qui commence au berceau,</l>
						<l n="14" num="1.14">Et qu’on a parcouru lorsqu’on touche au tombeau !</l>
						<l n="15" num="1.15">L’homme croit au bonheur, et depuis son enfance,</l>
						<l n="16" num="1.16">Pour l’atteindre, il travaille, use son existence ;</l>
						<l n="17" num="1.17">Mais au lieu du bonheur il trouve le trépas,</l>
						<l n="18" num="1.18">Et devient ce limon qu’on foule sous nos pas… »</l>
					</lg>
					<lg n="2">
						<l n="19" num="2.1">Si le néant était le terme de la vie,</l>
						<l n="20" num="2.2">Dieu, lui, dis-je, serait un infâme génie.</l>
						<l n="21" num="2.3">Comment ! nous serions tous destinés à souffrir,</l>
						<l n="22" num="2.4">A vivre sans espoir et sans espoir mourir ?…</l>
						<l n="23" num="2.5">Votre vie est affreuse : elle est la mort de l’âme ;</l>
						<l n="24" num="2.6">Car l’âme juste espère en Dieu qui la réclame.</l>
					</lg>
					<lg n="3">
						<l n="25" num="3.1">Plus ému que content des paroles du vieux ‒</l>
						<l n="26" num="3.2">Paroles qui blessaient mes sentiments pieux ‒</l>
						<l n="27" num="3.3">J’abordai sur la route un homme au doux visage,</l>
						<l n="28" num="3.4">Un homme dont l’esprit me parut droit et sage,</l>
						<l n="29" num="3.5">Et je lui demandai, d’un ton respectueux,</l>
						<l n="30" num="3.6">De résoudre pour moi le problème épineux.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">Une lueur d’espoir éclaira sa figure,</l>
						<l n="32" num="4.2">Et, s’inclinant, il dit d’une voix mâle et pure :</l>
						<l n="33" num="4.3">« La vie est pour connaître et servir le Seigneur,</l>
						<l n="34" num="4.4">Recevoir sa doctrine avec joie et douceur,</l>
						<l n="35" num="4.5">Imiter les vertus du Christ ‒ divin modèle ‒</l>
						<l n="36" num="4.6">Afin de vivre un jour de sa vie immortelle.</l>
					</lg>
					<lg n="5">
						<l n="37" num="5.1">« La vie est un foyer qu’alimente la foi ;</l>
						<l n="38" num="5.2">Un livre où le Seigneur a buriné sa loi ;</l>
						<l n="39" num="5.3">Un creuset où notre âme, au feu de la souffrance,</l>
						<l n="40" num="5.4">S’épure et sent grandir en elle l’espérance.</l>
						<l n="41" num="5.5">Il vit, l’homme qui sait ses crimes pardonnés,</l>
						<l n="42" num="5.6">Il entrevoit du ciel les justes couronnés ;</l>
						<l n="43" num="5.7">En mourant au péché, son âme se délie</l>
						<l n="44" num="5.8">Et recouvre aussitôt la véritable vie.</l>
						<l n="45" num="5.9">Vivre enfin, ici-bas, c’est souffrir et lutter ;</l>
						<l n="46" num="5.10">Vivre aussi, c’est le Christ ! mourir, c’est triompher !</l>
						<l n="47" num="5.11">Notre corps, je le sais, est tiré de la terre,</l>
						<l n="48" num="5.12">Et doit, après la mort, redevenir poussière ;</l>
						<l n="49" num="5.13">Mais l’âme ‒ souffle pur sorti du cœur de Dieu ‒</l>
						<l n="50" num="5.14">Quittera pour toujours ce misérable lieu ! »</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1">Ah ! s’il faut vivre ainsi, lui dis-je, je veux vivre !</l>
						<l n="52" num="6.2">Vivre sous les regards de Celui qui délivre</l>
						<l n="53" num="6.3">L’âme de sa prison pour la conduire au port ;</l>
						<l n="54" num="6.4">Oui, je veux triompher du vice et de la mort !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1888">Juillet 1888</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>