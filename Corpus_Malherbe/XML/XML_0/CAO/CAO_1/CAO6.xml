<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO6">
					<head type="main">LA NUIT DE NOËL</head>
					<opener>
						<salute>A M. J-C TACHÉ, OTTAWA</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Au pied de sa couche grossière</l>
						<l n="2" num="1.2">Le petit pauvre a mis son bas,</l>
						<l n="3" num="1.3">En murmurant cette prière :</l>
						<l n="4" num="1.4">Bon Jésus, ne m’oubliez pas !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Il ne sait point que la misère</l>
						<l n="6" num="2.2">Plane au-dessus de son réduit,</l>
						<l n="7" num="2.3">Et que sa malheureuse mère</l>
						<l n="8" num="2.4">N’a fait qu’un repas aujourd’hui !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Il ignore donc, à son âge,</l>
						<l n="10" num="3.2">Que l’on peut souffrir de la faim,</l>
						<l n="11" num="3.3">Et qu’un firmament sans nuage</l>
						<l n="12" num="3.4">Peut devenir sombre demain.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Il ne sait qu’une seule chose :</l>
						<l n="14" num="4.2">C’est la grande nuit de Noël,</l>
						<l n="15" num="4.3">La nuit où l’enfant Jésus rose</l>
						<l n="16" num="4.4">Apporte des présents du ciel.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Il s’endort sous des draps de laine,</l>
						<l n="18" num="5.2">L’un sur l’autre assez mal cousus ;</l>
						<l n="19" num="5.3">Mais ces draps valent bien l’haleine</l>
						<l n="20" num="5.4">Du bœuf qui soufflait sur Jésus !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Des songes d’or bercent son âme ;</l>
						<l n="22" num="6.2">Il voit, dans l’ombre qui grandit,</l>
						<l n="23" num="6.3">Un esprit aux ailes de flamme,</l>
						<l n="24" num="6.4">Voltiger autour de son lit,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Et dans son bas mette un mélange</l>
						<l n="26" num="7.2">De fruits vermeils et de bonbons ;</l>
						<l n="27" num="7.3">Puis le rêveur, d’un geste étrange,</l>
						<l n="28" num="7.4">Tends les menottes vers ces dons…</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Debout, la mère est là qui pleure,</l>
						<l n="30" num="8.2">Le cœur brisé par le chagrin,</l>
						<l n="31" num="8.3">Car pas d’argent dans la demeure,</l>
						<l n="32" num="8.4">Et pas un seul morceau de pain.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Un douloureux transport l’agite ;</l>
						<l n="34" num="9.2">Son regard se voile un instant ;</l>
						<l n="35" num="9.3">Son cœur à se rompre palpite,</l>
						<l n="36" num="9.4">Et son esprit va délirant :</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">« Dieu donne au riche l’opulence</l>
						<l n="38" num="10.2">Avec la joie et le bonheur ;</l>
						<l n="39" num="10.3">Au pauvre, il donne l’indigence</l>
						<l n="40" num="10.4">Avec l’envie et la douleur !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">« Le riche emplit de friandises</l>
						<l n="42" num="11.2">Le bas soyeux de son bambin</l>
						<l n="43" num="11.3">Et moi je n’ai que des reprises</l>
						<l n="44" num="11.4">A faire au bas de l’orphelin…</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">« Mais je blasphème, ô Dieu ! pardonne,</l>
						<l n="46" num="12.2">Dit-elle, en tombant à genoux !</l>
						<l n="47" num="12.3">Ma pauvre langue déraisonne,</l>
						<l n="48" num="12.4">Car c’est toi qui veilles sur nous.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">« Sombre ou rose est notre existence :</l>
						<l n="50" num="13.2">De ton amour c’est le secret ;</l>
						<l n="51" num="13.3">A notre âme il faut la souffrance,</l>
						<l n="52" num="13.4">Comme à l’or il faut le creuset. »</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Minuit sonne. La cloche appelle</l>
						<l n="54" num="14.2">Le peuple auprès du saint berceau ;</l>
						<l n="55" num="14.3">La veuve, à cette voix si belle,</l>
						<l n="56" num="14.4">Éprouve un sentiment nouveau.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">« Pendant que mon ange sommeille,</l>
						<l n="58" num="15.2">Fait-elle, en essuyant ses yeux,</l>
						<l n="59" num="15.3">Allons à la crèche vermeille</l>
						<l n="60" num="15.4">Adorer l’envoyé des cieux. »</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Dans le temple de la prière</l>
						<l n="62" num="16.2">Elle pénètre en chancelant,</l>
						<l n="63" num="16.3">Car la douleur et la misère</l>
						<l n="64" num="16.4">Ont rendu son corps défaillant.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">Près d’elle, un homme charitable</l>
						<l n="66" num="17.2">Qui compte déjà de longs jours,</l>
						<l n="67" num="17.3">Devine, à son air lamentable,</l>
						<l n="68" num="17.4">Qu’elle végète sans secours.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">Il la connaît et la vénère,</l>
						<l n="70" num="18.2">Et désirant l’aider un peu</l>
						<l n="71" num="18.3">Il sort et vole à la chaumière</l>
						<l n="72" num="18.4">De celle qui prie au saint lieu.</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">Sans effort il ouvre la porte,</l>
						<l n="74" num="19.2">La porte fermée au loquet,</l>
						<l n="75" num="19.3">Dépose le falot qu’il porte</l>
						<l n="76" num="19.4">Et met sur la table un paquet.</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1">Il va sortir, quant la voix fraîche</l>
						<l n="78" num="20.2">De l’enfant bredouille tout bas :</l>
						<l n="79" num="20.3">« Le bon Jésus sort de la crèche</l>
						<l n="80" num="20.4">Pour emplir tous les petits bas ! »</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1">L’homme, ému par ce songe étrange,</l>
						<l n="82" num="21.2">Fuit et revient en quelques bonds</l>
						<l n="83" num="21.3">Glisser dans le bas du bel ange</l>
						<l n="84" num="21.4">Des pièces d’or et des bonbons…</l>
					</lg>
					<lg n="22">
						<l n="85" num="22.1">Il est jour. Le soleil inonde</l>
						<l n="86" num="22.2">La chaumière de mille feux.</l>
						<l n="87" num="22.3">Soudain, levant sa tête blonde,</l>
						<l n="88" num="22.4">L’enfant pousse des cris joyeux.</l>
					</lg>
					<lg n="23">
						<l n="89" num="23.1">La mère, à ces tons d’allégresse,</l>
						<l n="90" num="23.2">Se lève et croit rêver encor !</l>
						<l n="91" num="23.3">L’enfant l’embrasse et la caresse</l>
						<l n="92" num="23.4">En lui montrant les pièces d’or.</l>
					</lg>
					<lg n="24">
						<l n="93" num="24.1">Sauvés ! Sauvés exclame-t-elle !</l>
						<l n="94" num="24.2">‒ Enfant, d’où vient ce trésor-là ?</l>
						<l n="95" num="24.3">‒ Mère, la chose est naturelle :</l>
						<l n="96" num="24.4">Il vient du bon Jésus, voilà !</l>
					</lg>
					<lg n="25">
						<l n="97" num="25.1">Intelligente autant que sage,</l>
						<l n="98" num="25.2">La mère devine à l’instant ;</l>
						<l n="99" num="25.3">Et, décrochant une humble image,</l>
						<l n="100" num="25.4">Elle dit en s’agenouillant :</l>
					</lg>
					<lg n="26">
						<l n="101" num="26.1">« Enfant, devant cette madone,</l>
						<l n="102" num="26.2">Disons, en ce jour solennel :</l>
						<l n="103" num="26.3">Oh ! bénissez celui qui donne</l>
						<l n="104" num="26.4">L’or et les bonbons de Noël ! »</l>
					</lg>
					<closer>
						<dateline>
							<date when="1890">27 décembre 1890</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>