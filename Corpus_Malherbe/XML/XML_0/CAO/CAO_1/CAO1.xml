<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO1">
					<head type="main">LE BONHEUR</head>
					<opener>
						<salute>A MA FEMME</salute>
						<epigraph>
							<cit>
								<quote>
									Où donc est le bonheur ? disais-je. ‒ Infortuné !<lb></lb>
									Le bonheur, ô mon Dieu, vous me l’avez donné.
								</quote>
								<bibl>
									<name>VICTOR HUGO</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">J’ai cherché vainement dans les bruyantes fêtes,</l>
						<l n="2" num="1.2">Où l’éclat des plaisirs éblouit tant de têtes,</l>
						<l n="3" num="1.3">Ce trésor précieux qu’on nomme le bonheur ;</l>
						<l n="4" num="1.4">Je l’ai cherché d’abord sur le sol que je foule</l>
						<l n="5" num="1.5">En voulant soulever les bravos de la foule,</l>
						<l n="6" num="1.6">Et je n’ai recueilli qu’un éphémère honneur !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Pour le trouver, j’ai fait de pénibles voyages,</l>
						<l n="8" num="2.2">Franchi les flots amers, parcouru maints villages</l>
						<l n="9" num="2.3">Où la vive gaîté faisait battre les cœurs ;</l>
						<l n="10" num="2.4">Mais, ô fatalité ! la sombre nostalgie,</l>
						<l n="11" num="2.5">Ce désir violent de revoir la patrie,</l>
						<l n="12" num="2.6">Aggravait chaque jour le poids de mes malheurs !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Après avoir vécu sur la plage étrangère,</l>
						<l n="14" num="3.2">Sans ressource et craignant la main de la misère,</l>
						<l n="15" num="3.3">Je revins au pays avec le fol espoir</l>
						<l n="16" num="3.4">De trouver le bonheur en l’amitié sincère</l>
						<l n="17" num="3.5">D’hommes que mainte fois j’avais aidés naguère.</l>
						<l n="18" num="3.6">Mais les cruels ingrats rougirent de me voir !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Le bonheur !… pour l’avoir j’ai gravi le Parnasse</l>
						<l n="20" num="4.2">Sur la cime duquel les disciples d’Horace</l>
						<l n="21" num="4.3">Buvaient le doux nectar que leur versaient les dieux ;</l>
						<l n="22" num="4.4">J’allais toucher au but, quand mon lâche Pégase,</l>
						<l n="23" num="4.5">Prenant un ton railleur, me lança cette phrase :</l>
						<l n="24" num="4.6">« Halte-là ! car tu n’es qu’un intrus en ces lieux… »</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Alors je m’écriai, dans ma douleur amère.</l>
						<l n="26" num="5.2"><hi rend="ital">Où donc est le bonheur ?</hi> Serait-ce une chimère</l>
						<l n="27" num="5.3">Qui redonne l’espoir à tout être souffrant ?</l>
						<l n="28" num="5.4">Hélas ! je le croyais… Mais dès le jour, ô femme,</l>
						<l n="29" num="5.5">Où les sons de ta voix firent vibrer mon âme,</l>
						<l n="30" num="5.6">Je goûtai du bonheur le délice enivrant !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Et depuis qu’à nos yeux ‒ aurore fortunée ‒</l>
						<l n="32" num="6.2">S’alluma le divin flambeau de l’hyménée,</l>
						<l n="33" num="6.3">Le bonheur, tu le sais, nous souria toujours.</l>
						<l n="34" num="6.4">Il nous sourira même au sein de la souffrance,</l>
						<l n="35" num="6.5">Parce que nous plaçons toute notre espérance</l>
						<l n="36" num="6.6">Dans le Dieu qui bénit et féconde les jours !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1886">Septembre 1886</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>