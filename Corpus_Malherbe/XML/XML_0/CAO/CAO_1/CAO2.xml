<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO2">
					<head type="main">RENOUVEAU</head>
					<opener>
						<salute>A M. BENJAMIN SULTE</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Le doux printemps vient de paraître</l>
						<l n="2" num="1.2">Sous son manteau de velours vert,</l>
						<l n="3" num="1.3">Et déjà l’on voit disparaître</l>
						<l n="4" num="1.4">Tous les vestiges de l’hiver.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Son œil à l’éclat de la braise :</l>
						<l n="6" num="2.2">A la chaleur de ses rayons</l>
						<l n="7" num="2.3">Naissent lilas, fleur, rose et fraise.</l>
						<l n="8" num="2.4">Abeilles d’or et papillons.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Les arbres engourdis naguère</l>
						<l n="10" num="3.2">Semblent dresser plus haut le front,</l>
						<l n="11" num="3.3">Car la nature, en bonne mère,</l>
						<l n="12" num="3.4">Verse la sève dans leur tronc.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Au plus épais de la ramure</l>
						<l n="14" num="4.2">Les oiseaux préparent leurs nids,</l>
						<l n="15" num="4.3">Sans s’occuper si la pâture</l>
						<l n="16" num="4.4">Ou le lin leur seront fournis.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Du sol jaillit plus d’une source</l>
						<l n="18" num="5.2">Que la froidure emprisonnait ;</l>
						<l n="19" num="5.3">Et le ruisseau reprend sa course</l>
						<l n="20" num="5.4">A travers clos et jardinet.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Sur le bord de maintes rivières</l>
						<l n="22" num="6.2">L’on voit le castor vigilant</l>
						<l n="23" num="6.3">Transporter le bois et les pierres</l>
						<l n="24" num="6.4">Pour bâtir son gîte étonnant.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">La brise, sylphide légère,</l>
						<l n="26" num="7.2">Fait la cour à toutes les fleurs,</l>
						<l n="27" num="7.3">Puis vole embaumer l’atmosphère</l>
						<l n="28" num="7.4">Des plus enivrantes senteurs.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">De la cime de nos montagnes</l>
						<l n="30" num="8.2">Se précipite le torrent</l>
						<l n="31" num="8.3">Qui fertilise nos campagnes</l>
						<l n="32" num="8.4">Avec les eaux du Saint-Laurent.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">A nos fenêtres, l’hirondelle</l>
						<l n="34" num="9.2">S’annonce par des cris joyeux ;</l>
						<l n="35" num="9.3">Elle revient à tire-d’aile</l>
						<l n="36" num="9.4">Charmer les jeunes et les vieux.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Au palais comme à la chaumière,</l>
						<l n="38" num="10.2">La porte s’ouvre à deux battants :</l>
						<l n="39" num="10.3">Riche et pauvre ont soif de lumière</l>
						<l n="40" num="10.4">D’air pur, de parfums odorants.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Parfois l’on quitte sa demeure</l>
						<l n="42" num="11.2">Pour aller prendre un gai repas</l>
						<l n="43" num="11.3">Sur la pelouse où toute à l’heure,</l>
						<l n="44" num="11.4">Bébé fera ses premiers pas.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Plus loin les colons sur leur terre</l>
						<l n="46" num="12.2">Travaillent courageusement</l>
						<l n="47" num="12.3">A l’œuvre utile et salutaire</l>
						<l n="48" num="12.4">Qu’on nomme le défrichement.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Les uns creusent, les autres sèment</l>
						<l n="50" num="13.2">Ou bien coupent les arbres morts ;</l>
						<l n="51" num="13.3">Ces braves bûchent, chantent, s’aiment</l>
						<l n="52" num="13.4">Et dorment la nuit sans remords !</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">La fillette en robe de bure</l>
						<l n="54" num="14.2">Chante et cultive tout le jour ;</l>
						<l n="55" num="14.3">Le soir venu, sa lèvre pure</l>
						<l n="56" num="14.4">Dira peut-être un mot d’amour !…</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Oui, l’homme, les oiseaux, les plantes</l>
						<l n="58" num="15.2">Et l’onde aux bruits mystérieux</l>
						<l n="59" num="15.3">Mêlent leurs voix reconnaissantes</l>
						<l n="60" num="15.4">Pour célébrer le Roi des cieux.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Car tout ce qui vit et respire,</l>
						<l n="62" num="16.2">Tout ce qui chante, pleure ou croit,</l>
						<l n="63" num="16.3">Reconnaît qu’il est sous l’empire</l>
						<l n="64" num="16.4">D’un esprit souverain et droit !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">Printemps, réveil de la nature,</l>
						<l n="66" num="17.2">Oh ! sois le bienvenu toujours !</l>
						<l n="67" num="17.3">Quand tu parais, la créature</l>
						<l n="68" num="17.4">Espère encore des beaux jours !</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">C’est toi qui donnes à la plaine</l>
						<l n="70" num="18.2">Son riche et moelleux vêtement ;</l>
						<l n="71" num="18.3">C’est toi qui fais germer la graine</l>
						<l n="72" num="18.4">D’où sortira notre aliment !</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">C’est toi qui rends au pulmonaire</l>
						<l n="74" num="19.2">La force et souvent la santé ;</l>
						<l n="75" num="19.3">C’est toi que l’Indien vénère</l>
						<l n="76" num="19.4">En recouvrant la liberté !</l>
					</lg>
					<ab type="dot">⁂</ab>
					<lg n="20">
						<l n="77" num="20.1">O printemps, messager Céleste,</l>
						<l n="78" num="20.2">Admirable consolateur</l>
						<l n="79" num="20.3">Ton éclat seul nous manifeste</l>
						<l n="80" num="20.4">La puissance du Créateur !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1887">4 juin 1887</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>