<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉTOILES FILANTES</head><div type="poem" key="GIL25">
					<head type="main">Georges-Étienne Cartier</head>
					<lg n="1">
						<l n="1" num="1.1">Cartier ! tu combattis toujours franc et sans dol ;</l>
						<l n="2" num="1.2">La majesté du temps sur ton rêve est passée ;</l>
						<l n="3" num="1.3">L’avenir connaîtra ta profonde pensée,</l>
						<l n="4" num="1.4">Car dans l’azur des cieux ta gloire a pris son vol !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Maintenant que l’Histoire a flagellé l’Envie</l>
						<l n="6" num="2.2">Dont la lèvre hideuse affligea ta fierté,</l>
						<l n="7" num="2.3">Élève sur l’autel de la postérité,</l>
						<l n="8" num="2.4">En leçon pour nos fils, l’exemple de ta vie.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Grand cœur que l’idéal a fait seul palpiter</l>
						<l n="10" num="3.2">Plus haut que l’intérêt matériel de l’heure,</l>
						<l n="11" num="3.3">Dans le temps écoulé ton œuvre qui demeure.</l>
						<l n="12" num="3.4">Nargue les fronts étroits qu’il te fallut dompter.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Sur nos frères lointains quand l’injustice tombe,</l>
						<l n="14" num="4.2">Puisse ton souvenir nous mener au combat,</l>
						<l n="15" num="4.3">En ces jours de bassesse, où plus d’un renégat</l>
						<l n="16" num="4.4">Ose se réclamer de ton cœur sur ta tombe !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"> Sous tes traits, ô grand homme, à la face du ciel,</l>
						<l n="18" num="5.2">C’est l’antique droiture et la chevalerie,</l>
						<l n="19" num="5.3">L’honneur, le dévouement, c’est toute la patrie</l>
						<l n="20" num="5.4">Qu’un sculpteur fixera dans le bronze éternel !…</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Muse, clame son nom dans tes apothéoses !</l>
						<l n="22" num="6.2">Que tes rayons soient doux à sa pierre, ô soleil !</l>
						<l n="23" num="6.3">Enfants, par vos chansons, allégez son sommeil !</l>
						<l n="24" num="6.4">Hommes, brûlez l’encens ! Femmes, jetez des roses !</l>
					</lg>
				</div></body></text></TEI>