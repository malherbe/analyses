<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CAP ÉTERNITÉ</head><div type="poem" key="GIL10">
					<head type="number">Chant IX</head>
					<head type="main">Le Cap Éternité</head>
					<lg n="1">
						<l n="1" num="1.1">Fronton vertigineux dont un monde est le temple,</l>
						<l n="2" num="1.2">C’est à l’éternité que ce cap fait songer :</l>
						<l n="3" num="1.3">Laisse en face de lui l’heure se prolonger</l>
						<l n="4" num="1.4">Silencieusement, ô mon âme, et contemple !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Défiant le calcul, au sein du fleuve obscur</l>
						<l n="6" num="2.2">Il plonge ; le miroir est digne de l’image.</l>
						<l n="7" num="2.3">Et quand le vent s’endort au large, le nuage</l>
						<l n="8" num="2.4">Couronne son front libre au pays de l’azur.</l>
						<l n="9" num="2.5">Le plomb du nautonier à sa base s’égare,</l>
						<l n="10" num="2.6">Et d’en bas, bien souvent, notre regard se perd</l>
						<l n="11" num="2.7">En cherchant son sommet familier de l’éclair</l>
						<l n="12" num="2.8">C’est pourquoi le passant étonné le compare</l>
						<l n="13" num="2.9">À la mystérieuse et noire Éternité.</l>
						<l n="14" num="2.10">Témoin pétrifié des premiers jours du monde,</l>
						<l n="15" num="2.11">Il était sous le ciel avant l’humanité,</l>
						<l n="16" num="2.12">Car plus mystérieux que dans la nuit de l’onde</l>
						<l n="17" num="2.13">Où sa base s’enfonce, il plonge dans le temps ;</l>
						<l n="18" num="2.14">Et le savant pensif qui marque nos instants,</l>
						<l n="19" num="2.15">N’a pu compter son âge à l’aune des années.</l>
					</lg>
					<lg n="3">
						<l n="20" num="3.1">Il a vu s’accomplir de sombres destinées.</l>
						<l n="21" num="3.2">Rien n’a modifié son redoutable aspect.</l>
						<l n="22" num="3.3">Il a vu tout changer, pendant qu’il échappait</l>
						<l n="23" num="3.4">À la terrestre loi des choses périssables,</l>
						<l n="24" num="3.5">Il a vu tout changer, tout naître et tout mourir,</l>
						<l n="25" num="3.6">Et tout renaître encore, et vivre, et se flétrir :</l>
						<l n="26" num="3.7">Les grands pins et le lierre à ses flancs formidables,</l>
						<l n="27" num="3.8">Et, dans le tourbillon des siècles emportés,</l>
						<l n="28" num="3.9">Les générations, leurs sanglots et leurs rires,</l>
						<l n="29" num="3.10">Les faibles et les forts, les bourgs et les cités,</l>
						<l n="30" num="3.11">Les royaumes obscurs et les puissants empires !</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">Des reptiles ailés parcouraient ses versants</l>
						<l n="32" num="4.2">Longtemps avant que l’homme eût paru sur la terre ;</l>
						<l n="33" num="4.3">Longtemps avant sa voix, leurs cris retentissants</l>
						<l n="34" num="4.4">Troublaient le vierge écho des bois pleins de mystère.</l>
						<l n="35" num="4.5">Enfin, dans la forêt où régnait l’animal,</l>
						<l n="36" num="4.6">Il a vu dominer l’être à l’âme immortelle,</l>
						<l n="37" num="4.7">Celui que ses instincts entraînent vers le mal,</l>
						<l n="38" num="4.8">Et qui conserve en lui la divine étincelle.</l>
						<l n="39" num="4.9">Sur le globe, bientôt, cette race nouvelle</l>
						<l n="40" num="4.10">Domina tout, devint innombrable et grandit ;</l>
						<l n="41" num="4.11">Mais ses iniquités grandirent avec elle,</l>
						<l n="42" num="4.12">Et Dieu qu’elle affligea dans son cœur, la maudit.</l>
						<l n="43" num="4.13">Alors les océans de l’abîme jaillirent ;</l>
						<l n="44" num="4.14">Les écluses du ciel toutes grandes s’ouvrirent,</l>
						<l n="45" num="4.15">Et la pluie en torrents effroyables tomba.</l>
						<l n="46" num="4.16">Pendant quarante jours, l’onde diluvienne</l>
						<l n="47" num="4.17">Tomba, submergeant tout, montagne comme plaine ;</l>
						<l n="48" num="4.18">Et tout être qui vit sur terre, succomba.</l>
						<l n="49" num="4.19">Le Cap fut submergé : sa cime souveraine,</l>
						<l n="50" num="4.20">Sa cime habituée aux rayons fulgurants,</l>
						<l n="51" num="4.21">Vit tout un monde mort passer dans la pénombre :</l>
						<l n="52" num="4.22">Mammouth géant qui lutte et trouble au loin l’eau sombre,</l>
						<l n="53" num="4.23">Hommes qu’entre deux eaux emportent les courants,</l>
						<l n="54" num="4.24">Aigles dont l’aile lasse en sombrant bat encore…</l>
						<l n="55" num="4.25">La cime d’où montaient des chansons dans l’aurore,</l>
						<l n="56" num="4.26">La cime humiliée a vu, sous ses grands pins,</l>
						<l n="57" num="4.27">Se fermer la mâchoire affreuse des requins.</l>
						<l n="58" num="4.28">Mais les eaux du déluge enfin se retirèrent.</l>
						<l n="59" num="4.29">Les fleuves peu à peu reprirent leur niveau ;</l>
						<l n="60" num="4.30">Aux âges envolés les âges succédèrent,</l>
						<l n="61" num="4.31">Et les graves humains parurent de nouveau.</l>
					</lg>
					<lg n="5">
						<l n="62" num="5.1">Longtemps il les a vus, dans l’écorce légère</l>
						<l n="63" num="5.2">Sillonner au loin l’onde en plongeant l’aviron :</l>
						<l n="64" num="5.3">Puis vinrent les héros dont notre race est fière :</l>
						<l n="65" num="5.4">Le chevalier sans peur et le missionnaire,</l>
						<l n="66" num="5.5">En passant dans son ombre ont découvert leur front ;</l>
						<l n="67" num="5.6">Puis survint le radeau du rude bûcheron</l>
						<l n="68" num="5.7">Devant qui s’inclinait la forêt séculaire :</l>
						<l n="69" num="5.8">Et naguères enfin parurent les voiliers</l>
						<l n="70" num="5.9">Qui flottaient sur la vague, emportés par les brises</l>
						<l n="71" num="5.10">Comme des oiseaux noirs aux grandes ailes grises.</l>
					</lg>
					<lg n="6">
						<l n="72" num="6.1">Et tout est disparu ! navires, chevaliers,</l>
						<l n="73" num="6.2">Et bûcherons joyeux, et martyrs, et sauvages,</l>
						<l n="74" num="6.3">Mammouths géants, poissons ailés, hommes pervers</l>
						<l n="75" num="6.4">Dont les iniquités perdirent l’univers.</l>
						<l n="76" num="6.5">Ont passé tour à tour, emportés par les âges,</l>
						<l n="77" num="6.6">Comme passent les flots à l’heure du reflux !</l>
						<l n="78" num="6.7">Et le terrain de pierre a vu toutes ces choses,</l>
						<l n="79" num="6.8">Et bien d’autres encor qui ne reviendront plus ;</l>
						<l n="80" num="6.9">Et rien n’a transformé ses lignes grandioses :</l>
						<l n="81" num="6.10">Depuis les premiers jours, fixe dans son granit,</l>
						<l n="82" num="6.11">L’immuable géant dressé sur l’Infini,</l>
						<l n="83" num="6.12">Sous le même soleil est demeuré le même !</l>
					</lg>
					<lg n="7">
						<l n="84" num="7.1">À peine si, de siècle en siècle, la forêt</l>
						<l n="85" num="7.2">Qui remplace à son front celle qui disparaît,</l>
						<l n="86" num="7.3">Donne au vieil empereur un nouveau diadème.</l>
						<l n="87" num="7.4">Lorsque d’un roi puissant la Mort sonne l’appel,</l>
						<l n="88" num="7.5">Sa couronne anoblit le roi qui le remplace ;</l>
						<l n="89" num="7.6">Mais quand la mort se heurte au granit éternel,</l>
						<l n="90" num="7.7">Le monarque demeure et la couronne passe !</l>
					</lg>
					<lg n="8">
						<l n="91" num="8.1">S’il tressaille parfois, de mille ans en mille ans,</l>
						<l n="92" num="8.2">Quand un fragment de roc s’éboule sur ses flancs,</l>
						<l n="93" num="8.3">Avec un grand fracas que l’écho répercute</l>
						<l n="94" num="8.4">Aux lointains horizons, c’est pour marquer la chute</l>
						<l n="95" num="8.5">D’un royaume fameux parmi les nations,</l>
						<l n="96" num="8.6">Ou pour sonner le glas des générations.</l>
						<l n="97" num="8.7">Et lorsque le fragment détaché de la cime</l>
						<l n="98" num="8.8">Frôle le flanc sonore et tombe dans l’abîme</l>
						<l n="99" num="8.9">Qui l’englobe en grondant et se ferme sur lui,</l>
						<l n="100" num="8.10">L’eau noire et frissonnante emporte dans sa nuit</l>
						<l n="101" num="8.11">Cette vibration jusqu’à la mer lointaine :</l>
						<l n="102" num="8.12">Le Cap Éternité fait dire à l’Océan</l>
						<l n="103" num="8.13">Qu’un empire effacé de la mémoire humaine</l>
						<l n="104" num="8.14">A rendu sa grandeur éphémère au néant.</l>
					</lg>
					<lg n="9">
						<l n="105" num="9.1">Des siècles ont passé sans affliger sa gloire !</l>
						<l n="106" num="9.2">Il nargue le Vieillard ailé qui fauche tout ;</l>
						<l n="107" num="9.3">À son pied souverain, dans l’onde affreuse et noire,</l>
						<l n="108" num="9.4">Des siècles sombreront : il restera debout !</l>
					</lg>
					<lg n="10">
						<l n="109" num="10.1">Combien de soirs sont morts, combien d’aubes sont nées</l>
						<l n="110" num="10.2">Sur son front dédaigneux des terrestres années ?</l>
						<l n="111" num="10.3">Combien de fois encor l’Océan va blêmir,</l>
						<l n="112" num="10.4">Combien de soirs silencieux vont s’endormir</l>
						<l n="113" num="10.5">Sur ce front dont l’orgueil dominera les âges</l>
						<l n="114" num="10.6">De plus haut qu’il ne règne au milieu des nuages ?</l>
						<l n="115" num="10.7">Quand sur le sol Laurentien seront passés</l>
						<l n="116" num="10.8">Des jours dont le calcul nous entraîne au vertige ;</l>
						<l n="117" num="10.9">Sur les sables mouvants quand seront effacés</l>
						<l n="118" num="10.10">Notre éphémère empreinte et nos derniers vestiges ;</l>
						<l n="119" num="10.11">Quand nous aurons été par d’autres remplacés,</l>
						<l n="120" num="10.12">Et, quand à leur déclin, le vent des cimetières</l>
						<l n="121" num="10.13">Aura sur d’autres morts roulé d’autres poussières ;</l>
						<l n="122" num="10.14">Plus loin dans l’avenir, peuples ensevelis,</l>
						<l n="123" num="10.15">Quand le linceul du temps vous aura dans ses plis ;</l>
					</lg>
					<lg n="11">
						<l n="124" num="11.1">Après votre néant, quand d’autres millénaires</l>
						<l n="125" num="11.2">Sur d’autres vanités tendront d’autres oublis,</l>
						<l n="126" num="11.3">Le Cap sera debout sur les eaux solitaires,</l>
						<l n="127" num="11.4">Debout sur les débris des nations altières ;</l>
						<l n="128" num="11.5">Le Cap Éternité dressé sur l’Infini</l>
						<l n="129" num="11.6">Sera debout dans son armure de granit.</l>
						<l n="130" num="11.7">Oh ! combien de destins, dans les nuits infernales,</l>
						<l n="131" num="11.8">Auront subi l’assaut des tourmentes fatales !…</l>
					</lg>
					<lg n="12">
						<l n="132" num="12.1">Que verra-t-il, dans l’avenir mystérieux ?</l>
						<l n="133" num="12.2">Quels déclins ! mais aussi quels essors merveilleux</l>
						<l n="134" num="12.3">D’audace et de calcul, quel art, quelle magie,</l>
						<l n="135" num="12.4">Quelles éclosions de patient génie,</l>
						<l n="136" num="12.5">Et quels profonds secrets conquis sur l’inconnu !</l>
					</lg>
					<lg n="13">
						<l n="137" num="13.1">Verra-t-il au ciel bleu l’homme enfin parvenu,</l>
						<l n="138" num="13.2">Planer en sûreté sur ses ailes rigides</l>
						<l n="139" num="13.3">Ou frôler l’eau qui dort sans y laisser de rides ?…</l>
						<l n="140" num="13.4">Que verra-t-il dans l’avenir ? quels monuments</l>
						<l n="141" num="13.5">D’orgueil et de laideur, et quels effondrements ?…</l>
					</lg>
					<lg n="14">
						<l n="142" num="14.1">La prospère beauté des campagnes fertiles</l>
						<l n="143" num="14.2">Au loin remplacera la beauté des forêts.</l>
						<l n="144" num="14.3">Après des ans, des ans, les antiques guérets</l>
						<l n="145" num="14.4">Feront place aux pavés assourdissants des villes :</l>
						<l n="146" num="14.5">Où vibraient des chansons, sourdront des clameurs viles ;</l>
						<l n="147" num="14.6">Où bruissaient les pins, sonneront les louis d’or.</l>
						<l n="148" num="14.7">Au grand mot de « progrès » qui servira d’excuse,</l>
						<l n="149" num="14.8">Les peuples se fieront à des hommes de ruse</l>
						<l n="150" num="14.9">Qui viendront établir, par leur œuvre de mort,</l>
						<l n="151" num="14.10">Le règne de la force et du mercantilisme ;</l>
						<l n="152" num="14.11">Et ce sera l’oubli des siècles d’héroïsme.</l>
						<l n="153" num="14.12">Mais l’humaine pensée, à l’antique Idéal</l>
						<l n="154" num="14.13">Offrira le retour d’un âge moins pratique.</l>
						<l n="155" num="14.14">Mourant d’avoir cherché le bien-être physique,</l>
						<l n="156" num="14.15">Les hommes chercheront le bien-être moral.</l>
						<l n="157" num="14.16">Les brutales laideurs du fer et de la suie</l>
						<l n="158" num="14.17">Se perdront aux lointains de leur époque enfuie,</l>
						<l n="159" num="14.18">Et les canons affreux pour longtemps se tairont,</l>
						<l n="160" num="14.19">Car, las de se tuer, les peuples s’aimeront.</l>
						<l n="161" num="14.20">Puis, les déclins retourneront aux origines,</l>
						<l n="162" num="14.21">Et la forêt reverdira sur les ruines.</l>
						<l n="163" num="14.22">Le sort confondra tout dans ses antiques lois,</l>
						<l n="164" num="14.23">Et tout sera joyeux comme aux jours d’autrefois…</l>
						<l n="165" num="14.24">Et pendant tout ce temps, majestueux emblème,</l>
						<l n="166" num="14.25">Le Cap Éternité demeurera le même !</l>
					</lg>
					<lg n="15">
						<l n="167" num="15.1">Malgré, sa majesté, l’homme le détruirait.</l>
						<l n="168" num="15.2">Cet atome rampant peut saper cette pierre</l>
						<l n="169" num="15.3">Imposante et sublime, et réduire en poussière</l>
						<l n="170" num="15.4">Le géant, pour un sou de plus à l’intérêt.</l>
						<l n="171" num="15.5">Mais nul n’a trouvé d’or à l’ombre de ta gloire :</l>
						<l n="172" num="15.6">Les morsures des vers rongeurs t’épargneront ;</l>
						<l n="173" num="15.7">Ô Rocher ! ta noblesse évite leur affront.</l>
						<l n="174" num="15.8">L’affamé cherche ailleurs un gain aléatoire.</l>
						<l n="175" num="15.9">Sphinx des passés perdus, il pose à l’avenir</l>
						<l n="176" num="15.10">Le problème infini du temps et de l’espace.</l>
						<l n="177" num="15.11">Il contemple an zénith l’Éternel face à face,</l>
						<l n="178" num="15.12">Et son terrible nom lui peut seul convenir.</l>
					</lg>
					<lg n="16">
						<l n="179" num="16.1">Dans le déclin des jours, il projette son ombre</l>
						<l n="180" num="16.2">Qui tourne en s’allongeant au loin sur le flot sombre ;</l>
						<l n="181" num="16.3">Depuis midi jusqu’aux ultimes feux du soir,</l>
						<l n="182" num="16.4">Sur l’onde fugitive il marque l’heure en noir</l>
						<l n="183" num="16.5">Et compte la naissance et la mort des années,</l>
						<l n="184" num="16.6">Pour quel monde inquiet, quelles races damnées,</l>
						<l n="185" num="16.7">Pour quels hôtes grinçants, pour quels spectres maudits,</l>
						<l n="186" num="16.8">Pour quels vieux prisonniers de l’infernal abîme,</l>
						<l n="187" num="16.9">Cette horloge implacable, éternelle et sublime,</l>
						<l n="188" num="16.10">Marque-t-elle l’essor des âges infinis !</l>
					</lg>
					<lg n="17">
						<l n="189" num="17.1">Celui qui le premier l’a nommé sur la terre,</l>
						<l n="190" num="17.2">Avait de l’être humain mesuré le cercueil,</l>
						<l n="191" num="17.3">Et, plus haut que l’essor de notre immense orgueil,</l>
						<l n="192" num="17.4">Habitué son rêve à la pleine lumière !</l>
						<l n="193" num="17.5">Est-ce toi, vieux Champlain ?… Non ! la postérité</l>
						<l n="194" num="17.6">Demande vainement à l’histoire incomplète,</l>
						<l n="195" num="17.7">Quel apôtre, quel preux, quel sublime poète</l>
						<l n="196" num="17.8">Devant tant de grandeur a dit : Éternité !</l>
					</lg>
					<ab type="dot"> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<ab type="dot"> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="18">
						<l n="197" num="18.1">Pourtant, il passera ! Les mois, les millénaires,</l>
						<l n="198" num="18.2">Les secondes, les ans, les siècles et les jours,</l>
						<l n="199" num="18.3">Devant l’éternité coulent d’un même cours.</l>
						<l n="200" num="18.4">L’atome misérable et les célestes sphères,</l>
						<l n="201" num="18.5">Tout passe, croule, meurt… et le monde et le ciel</l>
						<l n="202" num="18.6">Ne sont que vanité devant l’Être Éternel,</l>
						<l n="203" num="18.7">Car le monde et le ciel passeront avec l’heure,</l>
						<l n="204" num="18.8">Devant le Seigneur Dieu dont le verbe demeure.</l>
					</lg>
				</div></body></text></TEI>