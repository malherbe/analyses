<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉTOILES FILANTES</head><div type="poem" key="GIL23">
					<head type="main">Vive la Canadienne</head>
					<lg n="1">
						<l n="1" num="1.1"><space quantity="6" unit="char"></space>Dans maint pays, la voix du peuple entonne</l>
						<l n="2" num="1.2">L’hymne national pour fêter la couronne,</l>
						<l n="3" num="1.3"><space quantity="6" unit="char"></space>Ou la révolte, ou le sinistre airain</l>
						<l n="4" num="1.4"><space quantity="6" unit="char"></space>Qui gronde et tue en la sanglante plaine.</l>
						<l n="5" num="1.5"><space quantity="6" unit="char"></space>Plus poétique est notre gai refrain :</l>
						<l n="6" num="1.6"><space quantity="20" unit="char"></space>Vive la Canadienne !</l>
						<l n="7" num="1.7">Nous préférons chanter sur des rythmes joyeux,</l>
						<l n="8" num="1.8">Parmi tant de bonheurs que le sort nous enlève,</l>
						<l n="9" num="1.9">Le charme délicat et troublant des beaux yeux</l>
						<l n="10" num="1.10">Qui planent sur notre âme en y versant leur rêve,</l>
						<l n="11" num="1.11">Et, dans l’ombre morose étincellent pour nous ;</l>
						<l n="12" num="1.12">Ils semblent refléter, aux feux de leurs prunelles,</l>
						<l n="13" num="1.13">De nos soleils absents les splendeurs immortelles,</l>
						<l n="14" num="1.14">Vivent la Canadienne et ses jolis yeux doux !</l>
					</lg>
					<lg n="2">
						<l n="15" num="2.1"><space quantity="6" unit="char"></space>Restés Français par la galanterie,</l>
						<l n="16" num="2.2">Ensemble nous fêtons la femme et la patrie.</l>
						<l n="17" num="2.3"><space quantity="6" unit="char"></space>Si la vertu n’est pas un vague mot,</l>
						<l n="18" num="2.4"><space quantity="6" unit="char"></space>Notre chanson n’est frivole ni vaine ;</l>
						<l n="19" num="2.5"><space quantity="6" unit="char"></space>Et l’avenir le prouvera bientôt…</l>
						<l n="20" num="2.6"><space quantity="20" unit="char"></space>Vive la Canadienne !</l>
						<l n="21" num="2.7">Pour saluer l’orgueil des drapeaux outragés</l>
						<l n="22" num="2.8">Qui flottent, solennels, dans les grands jours de fièvre,</l>
						<l n="23" num="2.9">Elle sait l’art des chants tragiques ou légers ;</l>
						<l n="24" num="2.10">Et les fiers souvenirs frissonnent sur sa lèvre.</l>
						<l n="25" num="2.11">Nous mettons un espoir sublime à ses genoux,</l>
						<l n="26" num="2.12">Car c’est en bon français qu’elle nous dit : Je t’aime…</l>
						<l n="27" num="2.13">Entre ses bras divins s’écrit notre poème.</l>
						<l n="28" num="2.14">Vivent la Canadienne et ses jolis yeux doux !</l>
					</lg>
					<lg n="3">
						<l n="29" num="3.1"><space quantity="6" unit="char"></space>Nos conquérants ont flétri leur histoire.</l>
						<l n="30" num="3.2">Aussi, le justicier qui mesure la gloire</l>
						<l n="31" num="3.3"><space quantity="6" unit="char"></space>Des nations et leur iniquité,</l>
						<l n="32" num="3.4"><space quantity="6" unit="char"></space>Saura venger notre sœur acadienne</l>
						<l n="33" num="3.5"><space quantity="6" unit="char"></space>Au tribunal de la postérité…</l>
						<l n="34" num="3.6"><space quantity="20" unit="char"></space>Vive la Canadienne !</l>
						<l n="35" num="3.7">Ils ont fait arracher, magnanimes vainqueurs,</l>
						<l n="36" num="3.8">L’amoureux à la vierge, et l’époux à la femme,</l>
						<l n="37" num="3.9">Et l’enfant à la mère ; ils ont brisé des cœurs.</l>
						<l n="38" num="3.10">Ils ont, pour effrayer l’opprimé qui réclame,</l>
						<l n="39" num="3.11">Dressé des échafauds et forgé des verrous.</l>
						<l n="40" num="3.12">Mais ce n’est pas assez pour qu’une France tombe !</l>
						<l n="41" num="3.13">Ils ont en vain creusé dans leur nuit notre tombe.</l>
						<l n="42" num="3.14">Vivent la Canadienne et ses jolis yeux doux !</l>
					</lg>
					<lg n="4">
						<l n="43" num="4.1"><space quantity="6" unit="char"></space>En supprimant notre langue à l’école,</l>
						<l n="44" num="4.2">Ils ont cru vers leur port fausser notre boussole ;</l>
						<l n="45" num="4.3"><space quantity="6" unit="char"></space>Ils ont pensé pouvoir briser le sceau</l>
						<l n="46" num="4.4"><space quantity="6" unit="char"></space>Éblouissant de la patrie ancienne,</l>
						<l n="47" num="4.5"><space quantity="6" unit="char"></space>Que nous portons au front dès le berceau.</l>
						<l n="48" num="4.6"><space quantity="20" unit="char"></space>Vive la Canadienne !</l>
						<l n="49" num="4.7">Qui donc empêchera, dans les roses printemps,</l>
						<l n="50" num="4.8">Les jeunesses qui vont jaser sous les érables</l>
						<l n="51" num="4.9">D’échanger en français, à l’aube des vingt ans,</l>
						<l n="52" num="4.10">Les éternels serments des amours périssables.</l>
						<l n="53" num="4.11">Une école demeure : ils se rappellent tous</l>
						<l n="54" num="4.12">Les mots harmonieux des tendresses premières,</l>
						<l n="55" num="4.13">Quand ils sautaient, bambins, sur les genoux des mères.</l>
						<l n="56" num="4.14">Vivent la Canadienne et ses jolis yeux doux !</l>
					</lg>
					<lg n="5">
						<l n="57" num="5.1"><space quantity="6" unit="char"></space>Moins que jamais notre horizon est sombre.</l>
						<l n="58" num="5.2">Le sol natal est vaste et nous gagnons en nombre ;</l>
						<l n="59" num="5.3"><space quantity="6" unit="char"></space>Malgré ceux-là qu’une terre d’exil</l>
						<l n="60" num="5.4"><space quantity="6" unit="char"></space>Vers l’industrie et l’aventure entraîne,</l>
						<l n="61" num="5.5"><space quantity="6" unit="char"></space>Chaque an de plus amoindrit le péril.</l>
						<l n="62" num="5.6"><space quantity="20" unit="char"></space>Vive la Canadienne !</l>
						<l n="63" num="5.7">Notre sol, aux vainqueurs le travail le reprend :</l>
						<l n="64" num="5.8">Le Canadien, soldat de la sublime guerre</l>
						<l n="65" num="5.9">Qui vainc la forêt vierge, est le vrai conquérant ;</l>
						<l n="66" num="5.10">Il arrache la vie aux trésors de la terre.</l>
						<l n="67" num="5.11">Dans ces rudes chemins la femme suit l’époux ;</l>
						<l n="68" num="5.12">Elle va près de lui, simple, héroïque et pure,</l>
						<l n="69" num="5.13">Demander l’avenir à la grande Nature.</l>
						<l n="70" num="5.14">Vivent la Canadienne et ses jolis yeux doux !</l>
					</lg>
					<lg n="6">
						<l n="71" num="6.1"><space quantity="6" unit="char"></space>Sur les sentiers où vont nos destinées</l>
						<l n="72" num="6.2"><space quantity="6" unit="char"></space>Combien de pauvres fleurs hélas ! gisent fanées ;</l>
						<l n="73" num="6.3"><space quantity="6" unit="char"></space>Mais il en est dont les grands vents du Nord</l>
						<l n="74" num="6.4"><space quantity="6" unit="char"></space>N’ont pas terni la beauté souveraine :</l>
						<l n="75" num="6.5"><space quantity="6" unit="char"></space>Nous saurons bien les ravir à la mort…</l>
						<l n="76" num="6.6"><space quantity="20" unit="char"></space>Vive la Canadienne !</l>
						<l n="77" num="6.7">Fils d’Albion ! Dieu mit des obstacles sacrés</l>
						<l n="78" num="6.8">Devant nos cœurs français qui narguent les conquêtes.</l>
						<l n="79" num="6.9">Notre peuple, jamais vous ne l’engloutirez</l>
						<l n="80" num="6.10">Dans l’océan vorace où grondent vos tempêtes.</l>
						<l n="81" num="6.11">Vous n’étoufferez pas, sous un jargon jaloux,</l>
						<l n="82" num="6.12">La langue maternelle, élégante et sonore !</l>
						<l n="83" num="6.13">Vous n’éteindrez jamais l’astre de notre aurore :</l>
						<l n="84" num="6.14"><space quantity="12" unit="char"></space>La Canadienne aux beaux yeux doux !</l>
					</lg>
				</div></body></text></TEI>