<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CAP ÉTERNITÉ</head><div type="poem" key="GIL2">
					<head type="number">Chant I</head>
					<head type="main">Le Goéland</head>
					<lg n="1">
						<l n="1" num="1.1">Le soleil moribond ensanglantait les flots,</l>
						<l n="2" num="1.2">Et le jour endormait ses suprêmes échos.</l>
						<l n="3" num="1.3">La brise du Surouet roulait des houles lentes.</l>
						<l n="4" num="1.4">Dans mon canot d’écorce aux courbes élégantes,</l>
						<l n="5" num="1.5">Que Paul l’Abénaquis habile avait construit,</l>
						<l n="6" num="1.6">Je me hâtais vers Tadoussac et vers la nuit.</l>
						<l n="7" num="1.7">À grands coups cadencés, mon aviron de frêne</l>
						<l n="8" num="1.8">Poussait le « Goéland » vers la rive lointaine ;</l>
						<l n="9" num="1.9">Sous mes impulsions rythmiques, il glissait,</l>
						<l n="10" num="1.10">Le beau canot léger que doucement berçait</l>
						<l n="11" num="1.11">La courbe harmonieuse et lente de la houle.</l>
					</lg>
					<lg n="2">
						<l n="12" num="2.1">Sur la pourpre du ciel se profilait la « Boule »,</l>
						<l n="13" num="2.2">Sphère énorme dans l’onde enfonçant à demi,</l>
						<l n="14" num="2.3">Sentinelle qui veille au seuil du gouffre ami</l>
						<l n="15" num="2.4">Pour ramener la nef à l’inconnu livrée,</l>
						<l n="16" num="2.5">Et du fleuve sans fond marquer de loin l’entrée.</l>
						<l n="17" num="2.6">Ô globe ! as-tu surgi du flot mystérieux ?</l>
						<l n="18" num="2.7">Ou bien, aux anciens jours, es-tu tombé des cieux,</l>
						<l n="19" num="2.8">Comme un monde égaré dans l’orbe planétaire,</l>
						<l n="20" num="2.9">Et qui, pris de vertige, aurait frappé la Terre ?</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Dans le grand air du large et dans la paix des bois,</l>
						<l n="22" num="3.2">Dans les calmes matins et les soirs pleins d’effrois,</l>
						<l n="23" num="3.3">Dans la nuit où le cœur abandonné frissonne,</l>
						<l n="24" num="3.4">Dans le libre inconnu je fuyais Babylone…</l>
						<l n="25" num="3.5">Celle où la pauvreté du juste est un défaut ;</l>
						<l n="26" num="3.6">Celle où les écus d’or sauvent de l’échafaud ;</l>
						<l n="27" num="3.7">Où maint gredin puissant, respecté par la foule,</l>
						<l n="28" num="3.8">Est un vivant outrage au vieil honneur qu’il foule,</l>
						<l n="29" num="3.9">La ville où la façade à l’atroce ornement</l>
						<l n="30" num="3.10">Cache mal la ruelle où traîne l’excrément ;</l>
						<l n="31" num="3.11">Celle où ce qui digère écrase ce qui pense ;</l>
						<l n="32" num="3.12">Où se meurent les arts, où languit la science ;</l>
						<l n="33" num="3.13">Où des empoisonneurs l’effréné péculat</l>
						<l n="34" num="3.14">Des petits innocents trame l’assassinat ;</l>
						<l n="35" num="3.15">Où ton nom dans les cœurs s’oublie, ô Maisonneuve !</l>
						<l n="36" num="3.16">Celle où l’on voit de loin, sur les bords du grand fleuve.</l>
						<l n="37" num="3.17">Les temples du dollar affliger le ciel bleu,</l>
						<l n="38" num="3.18">En s’élevant plus haut que les temples de Dieu !</l>
					</lg>
					<lg n="4">
						<l n="39" num="4.1">Les dernières clartés du jour allaient s’éteindre.</l>
						<l n="40" num="4.2">Depuis longtemps je me croyais tout près d’atteindre</l>
						<l n="41" num="4.3">La rive montagneuse et farouche du Nord,</l>
						<l n="42" num="4.4">D’où le noir Saguenay, le fleuve de la Mort,</l>
						<l n="43" num="4.5">Surgi de sa crevasse ouverte au flanc du monde,</l>
						<l n="44" num="4.6">Se joint au Saint-Laurent dont il refoule l’onde.</l>
						<l n="45" num="4.7">La rive paraissait grandir avec la nuit,</l>
						<l n="46" num="4.8">Et l’ombre s’aggravait d’un lamentable bruit :</l>
						<l n="47" num="4.9">Plaintes des eaux, soupirs, rumeurs sourdes et vagues.</l>
						<l n="48" num="4.10">La houle harmonieuse avait fait place aux vagues ;</l>
						<l n="49" num="4.11">Le ciel s’était voilé d’épais nuages gris,</l>
						<l n="50" num="4.12">Et les oiseaux de mer regagnaient leurs abris.</l>
						<l n="51" num="4.13">Le « Goéland » rapide avançait vers la côte</l>
						<l n="52" num="4.14">Dont la masse effrayante et de plus en plus haute</l>
						<l n="53" num="4.15">Se dressait. L’aviron voltigeait à mon bras,</l>
						<l n="54" num="4.16">Et je luttais toujours, mais je n’arrivais pas.</l>
					</lg>
					<lg n="5">
						<l n="55" num="5.1">Le violet des monts se changeait en brun sombre.</l>
						<l n="56" num="5.2">Vainement j’avais cru traverser avant l’ombre,</l>
						<l n="57" num="5.3">Car de ces hauts sommets le décevant rempart</l>
						<l n="58" num="5.4">Égare le calcul et trompe le regard.</l>
						<l n="59" num="5.5">Maintenant, sur les flots qui roulaient des désastres.</l>
						<l n="60" num="5.6">La nuit, tombait, tragique, effrayante, sans astres ;</l>
						<l n="61" num="5.7">Et sur ma vie en proie à maint fatal décret,</l>
						<l n="62" num="5.8">Sombre pareillement la grande nuit tombait.</l>
						<l n="63" num="5.9">Je tentais d’étouffer, au fracas de la lame,</l>
						<l n="64" num="5.10">La voix du souvenir qui pleurait dans mon âme ;</l>
						<l n="65" num="5.11">En vain je voulais fuir un douloureux passé,</l>
						<l n="66" num="5.12">Et le sombre remords à mes côtés dressé.</l>
					</lg>
					<lg n="6">
						<l n="67" num="6.1">Mais je me demandais si les tragiques ondes</l>
						<l n="68" num="6.2">N’allaient m’ensevelir dans leurs vagues profondes.</l>
						<l n="69" num="6.3">Je regardais la vie et la mort d’assez haut.</l>
						<l n="70" num="6.4">Ma liberté, mon aviron et mon canot</l>
						<l n="71" num="6.5">Étant mes seuls trésors en ce monde éphémère.</l>
						<l n="72" num="6.6">Aussi, me rappelant mainte douleur amère :</l>
						<l n="73" num="6.7">— « Autant sombrer ici que dans le désespoir !</l>
						<l n="74" num="6.8">Allons, vieux « Goéland » ! qu’importe tout ce noir !</l>
						<l n="75" num="6.9">Le parcours est affreux, mais, du moins, il est libre !</l>
						<l n="76" num="6.10">N’embarque pas trop d’eau ! défends ton équilibre !</l>
						<l n="77" num="6.11">Ton maître s’est mépris en jugeant le trajet :</l>
						<l n="78" num="6.12">Oppose ta souplesse au furieux Surouet !</l>
					</lg>
					<lg n="7">
						<l n="79" num="7.1">Comme un oiseau craintif qui fuit devant l’orage,</l>
						<l n="80" num="7.2">Le grand canot filait vers la lointaine plage,</l>
						<l n="81" num="7.3">Sur les flots déchaînés qu’à peine il effleurait</l>
						<l n="82" num="7.4">Quand, dans l’obscurité, gronda le mascaret…</l>
						<l n="83" num="7.5">Le canot se cabra sur la masse liquide,</l>
						<l n="84" num="7.6">Tournoya sur lui-même et bondit dans le vide,</l>
						<l n="85" num="7.7">Prit la vague de biais, releva du devant,</l>
						<l n="86" num="7.8">Mais un coup d’aviron le coucha sous le vent.</l>
					</lg>
					<lg n="8">
						<l n="87" num="8.1">Alors, des jours heureux me vint la souvenance</l>
						<l n="88" num="8.2">Je me revis au seuil de mon adolescence ;</l>
						<l n="89" num="8.3">Je revis le Sauvage inventif, assemblant</l>
						<l n="90" num="8.4">L’écorce d’où son art tirait le « Goéland » ;</l>
						<l n="91" num="8.5">Comme un sculpteur épris d’un chef-d’œuvre qu’il crée,</l>
						<l n="92" num="8.6">Il flattait du regard la carène cambrée,</l>
						<l n="93" num="8.7">Calculait telle courbe à la largeur des bords</l>
						<l n="94" num="8.8">Et des proportions ménageait les rapports.</l>
						<l n="95" num="8.9">Je me remémorai sa parole prudente</l>
						<l n="96" num="8.10">Au temps déjà lointain où j’allais sous la tente</l>
						<l n="97" num="8.11">Causer des vieux chasseurs et voir de jour en jour</l>
						<l n="98" num="8.12">L’écorce prendre forme en son svelte contour ;</l>
						<l n="99" num="8.13">Quand je lui demandai pour la proue ou la poupe</l>
						<l n="100" num="8.14">Un ornement futile et d’élégante coupe,</l>
						<l n="101" num="8.15">Comme ceux que j’avais au jardin admirés</l>
						<l n="102" num="8.16">Sur des petits canots de guirlandes parés,</l>
						<l n="103" num="8.17">— Le vent, avait-il dit, prendrait dans ces girouettes !</l>
						<l n="104" num="8.18">Tu remercieras Paul au milieu des tempêtes,</l>
						<l n="105" num="8.19">Quand tu traverseras où d’autres sombreront !</l>
					</lg>
					<lg n="9">
						<l n="106" num="9.1">Cependant, j’approchais du Saguenay sans fond ;</l>
						<l n="107" num="9.2">Mon aviron heurta la Pointe aux Alouettes.</l>
						<l n="108" num="9.3">Je ne distinguais rien des grandes silhouettes,</l>
						<l n="109" num="9.4">Mais un phare apparut à mon regard chercheur :</l>
						<l n="110" num="9.5">Le brasier qui flambait au foyer d’un pêcheur</l>
						<l n="111" num="9.6">Guida ma randonnée, et j’atteignis la plage</l>
						<l n="112" num="9.7">De la petite baie, au pied du vieux village.</l>
					</lg>
				</div></body></text></TEI>