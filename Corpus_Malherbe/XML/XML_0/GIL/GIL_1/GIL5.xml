<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CAP ÉTERNITÉ</head><div type="poem" key="GIL5">
					<head type="number">Chant IV</head>
					<head type="main">Le Silence et l’Oubli</head>
					<lg n="1">
						<l n="1" num="1.1">Un vent faible soufflait après l’âpre tempête.</l>
						<l n="2" num="1.2">J’aperçus, en doublant le dangereux rocher,</l>
						<l n="3" num="1.3">Deux anges qui tournaient au-dessus de ma tête ;</l>
						<l n="4" num="1.4">Peu à peu, je les vis du canot s’approcher.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">L’un tenait son index en croix avec sa lèvre.</l>
						<l n="6" num="2.2">Bien qu’il trahît l’ardeur d’une mystique fièvre,</l>
						<l n="7" num="2.3">Son regard tourmenté pour l’âme était muet ;</l>
						<l n="8" num="2.4">En vain j’y voulus lire un suprême langage,</l>
						<l n="9" num="2.5">Comme en ceux des mortels dont la lèvre se tait.</l>
						<l n="10" num="2.6">Un fugitif sourire effleura son visage,</l>
						<l n="11" num="2.7">Quand de ses yeux ardents j’affrontai le reflet,</l>
						<l n="12" num="2.8">Au bleu rayonnement de l’antique veilleuse.</l>
						<l n="13" num="2.9">Son aile lunulée et double rappelait</l>
						<l n="14" num="2.10">Du tremblant papillon l’aile silencieuse,</l>
						<l n="15" num="2.11">Par son brillant velours et son vol indécis</l>
						<l n="16" num="2.12">Plein de grâce légère et de souple élégance.</l>
						<l n="17" num="2.13">À son geste, à ses yeux, à son vol j’ai compris</l>
						<l n="18" num="2.14">Que ce frère de l’ombre était le doux Silence.</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1">L’autre levait son front serein vers l’Infini.</l>
						<l n="20" num="3.2">Le calme auréolait sa figure impassible,</l>
						<l n="21" num="3.3">Et pourtant la pitié divine était visible</l>
						<l n="22" num="3.4">Sur la sombre grandeur de ce masque bruni</l>
						<l n="23" num="3.5">Par le hâle éternel de l’empire nocturne,</l>
						<l n="24" num="3.6">Ou par quelque soleil depuis longtemps éteint.</l>
						<l n="25" num="3.7">Il portait à son pied le signe de Saturne,</l>
						<l n="26" num="3.8">Ce vieux dispensateur du temps et du destin,</l>
						<l n="27" num="3.9">Comme s’il eût voulu mépriser ce qui dure.</l>
						<l n="28" num="3.10">Du puissant albatros il avait l’envergure ;</l>
						<l n="29" num="3.11">Lent et mystérieux et grave, il descendait</l>
						<l n="30" num="3.12">Au fixe déploiement de ses ailes royales ;</l>
						<l n="31" num="3.13">Son large vol plané décrivait des spirales.</l>
						<l n="32" num="3.14">Impénétrable et froid, son regard se perdait</l>
						<l n="33" num="3.15">Plus loin que la pensée et plus loin que les astres.</l>
						<l n="34" num="3.16">Alors je me suis dit que l’effroi des désastres</l>
						<l n="35" num="3.17">Allait dans le passé rester enseveli,</l>
						<l n="36" num="3.18">Car j’avais reconnu le bienfaisant Oubli.</l>
					</lg>
					<lg n="4">
						<l n="37" num="4.1">Mon cœur a murmuré tout bas dans ma poitrine :</l>
						<l n="38" num="4.2">— Bons esprits qui venez des grands cieux inconnus,</l>
						<l n="39" num="4.3">Dans ma nuit sans repos soyez les bienvenus ;</l>
						<l n="40" num="4.4">Au malheureux errant versez la paix divine !</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1">Le Silence ploya son aile de velours,</l>
						<l n="42" num="5.2">Et, précédant l’Oubli, comme il le fait toujours,</l>
						<l n="43" num="5.3">Il vint dans mon canot s’installer à la proue.</l>
						<l n="44" num="5.4">Mais l’Oubli près de moi sur la poupe s’assit ;</l>
						<l n="45" num="5.5">De ma tempe glacée il approcha sa joue.</l>
						<l n="46" num="5.6">Sur mon front douloureux que ridait le souci,</l>
						<l n="47" num="5.7">L’ange daigna poser ses lèvres éternelles,</l>
						<l n="48" num="5.8">Puis il m’enveloppa dans l’ombre de ses ailes.</l>
						<l n="49" num="5.9">Son âme m’envahit, et je sentis enfin,</l>
						<l n="50" num="5.10">Aux célestes frissons de ce baiser divin,</l>
						<l n="51" num="5.11">La paix de l’Infini dans mon être descendre.</l>
					</lg>
					<lg n="6">
						<l n="52" num="6.1">L’Oubli se redressa debout derrière moi ;</l>
						<l n="53" num="6.2">Je vis son envergure immense au vent se tendre,</l>
						<l n="54" num="6.3">Et dans le noir néant plonger son regard froid.</l>
						<l n="55" num="6.4">Le doux Silence ouvrit ses ailes veloutées…</l>
						<l n="56" num="6.5">Et le canot glissa sur le gouffre, sans bruit,</l>
						<l n="57" num="6.6">Cependant que le souffle apaisé de la nuit</l>
						<l n="58" num="6.7">Caressait doucement ses voiles enchantées.</l>
						<l n="59" num="6.8">Ainsi je remontais le fleuve de la Mort,</l>
						<l n="60" num="6.9">À l’étrange pilote abandonnant mon sort.</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1">Le Silence imposa son règne aux bruits du monde</l>
						<l n="62" num="7.2">Dont mon âme évoquait encore les échos :</l>
						<l n="63" num="7.3">Clameurs, bourdonnement de la haine qui gronde,</l>
						<l n="64" num="7.4">Vils affronts de l’envie et cruauté des mots,</l>
						<l n="65" num="7.5">Tout s’est évanoui dans une paix profonde.</l>
					</lg>
					<lg n="8">
						<l n="66" num="8.1">Et l’aile de l’Oubli sur la poupe dressé,</l>
						<l n="67" num="8.2">Empêchant mon regard d’observer en arrière</l>
						<l n="68" num="8.3">Le sillage d’argent par l’écorce tracé,</l>
						<l n="69" num="8.4">Cachait en même temps les lointains du passé.</l>
						<l n="70" num="8.5">L’Oubli tendit son aile au seuil du noir mystère ;</l>
						<l n="71" num="8.6">Du chemin déjà fait je perdis le parcours,</l>
						<l n="72" num="8.7">Et je ne vis plus rien dans le recul des jours :</l>
						<l n="73" num="8.8">Le remords s’endormit au fond de ma mémoire.</l>
						<l n="74" num="8.9">L’Oubli sur mon passé tendit son aile noire,</l>
						<l n="75" num="8.10">Tendit son aile noire entre l’ombre et mes yeux !</l>
						<l n="76" num="8.11">Ô bienfaisant retour des heureuses années !</l>
						<l n="77" num="8.12">J’ai scruté sans faiblir la loi des destinées,</l>
						<l n="78" num="8.13">Et j’ai levé le front sans crainte vers les cieux.</l>
						<l n="79" num="8.14">Devant le souvenir l’ange étendit son aile !</l>
						<l n="80" num="8.15">Tout s’est évanoui, remords, chagrin, rancœur :</l>
						<l n="81" num="8.16">J’ai senti le pardon céleste dans mon cœur,</l>
						<l n="82" num="8.17">Et le souffle de Dieu dans mon âme immortelle.</l>
					</lg>
					<lg n="9">
						<l n="83" num="9.1">Ainsi je remontais le fleuve de la Mort,</l>
						<l n="84" num="9.2">Au sublime pilote abandonnant mon sort.</l>
					</lg>
				</div></body></text></TEI>