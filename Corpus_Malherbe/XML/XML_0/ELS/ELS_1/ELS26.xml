<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Enluminures</title>
				<title type="medium">Édition électronique</title>
				<author key="ELS">
					<name>
						<forename>Max</forename>
						<surname>ELSKAMP</surname>
					</name>
					<date from="1862" to="1931">1862-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>718 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">ELS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Enluminures</title>
						<author>Max Elskamp</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/maxelskampenluminures.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Enluminures / paysages, heures, vies, chansons, grotesques</title>
						<author>Max Elskamp</author>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k10575703.r=%22max%20elskamp%22?rk=42918;4</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>Paul Lacomblez, Éditeur</publisher>
							<date when="1898">1898</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1898">1898</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">GROTESQUES</head><div type="poem" key="ELS26">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Et maintenant voici que l’on boit et qu’on mange,</l>
						<l n="2" num="1.2">que les lèvres ont joie, que la bouche est aux anges,</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1">et qu’à fruits d’ornement, figue, amande et raisins,</l>
						<l n="4" num="2.2">tout compte fait ici c’est mon livre à sa fin,</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1">car à présent voici que l’on rit et l’on danse</l>
						<l n="6" num="3.2">à la mode d’Espagne, à la mode de France,</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1">et que c’est vous et moi tous mes paroissiens</l>
						<l n="8" num="4.2">qui trouvons joie ainsi à nous tenir les mains</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1">pour la douceur qu’on a d’être sœurs, d’être frères,</l>
						<l n="10" num="5.2">à s’entraîner ici en maisons, bois et terres.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1">Or Flandre dite alors de toutes les manières,</l>
						<l n="12" num="6.2">à la façon des fils, à la façon des pères,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1">c’est le roi qui boit et tout le monde qui rit,</l>
						<l n="14" num="7.2">hommes, femmes, enfants et les bêtes aussi,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1">puis dimanche avec vous, soldats et militaires,</l>
						<l n="16" num="8.2">et lundi menuisiers, et marchés maraîchères,</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1">meuniers voici le vent, et printemps jardiniers,</l>
						<l n="18" num="9.2">et drapeaux mis aussi sur mes plus beaux clochers.</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1">Mais lors douceur à tous, car tout est bien au monde,</l>
						<l n="20" num="10.2">quand c’ est plaisir aux yeux, de jardins et verdures,</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1">et midi des repas faisant les tables rondes,</l>
						<l n="22" num="11.2">voici rire la vie et mes mains en peinture</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1">s’aller à vos souhaits enfin miens des villages</l>
						<l n="24" num="12.2">en rouge autour des toits et foi au raisin vert</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1">et selon mon cœur d’ici qui sait vos usages,</l>
						<l n="26" num="13.2">prendre joie avec vous du ciel et de la mer,</l>
					</lg>
					<lg n="14">
						<l n="27" num="14.1">car en toutes choses c’est simple le meilleur</l>
						<l n="28" num="14.2">et d’ornement au corps comme à l’âme santé,</l>
					</lg>
					<lg n="15">
						<l n="29" num="15.1">et bonnes gens alors en musique à son heure</l>
						<l n="30" num="15.2">voici mon livre ouvert comme salle à danser.</l>
					</lg>
				</div></body></text></TEI>