<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Enluminures</title>
				<title type="medium">Édition électronique</title>
				<author key="ELS">
					<name>
						<forename>Max</forename>
						<surname>ELSKAMP</surname>
					</name>
					<date from="1862" to="1931">1862-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>718 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">ELS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Enluminures</title>
						<author>Max Elskamp</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/maxelskampenluminures.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Enluminures / paysages, heures, vies, chansons, grotesques</title>
						<author>Max Elskamp</author>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k10575703.r=%22max%20elskamp%22?rk=42918;4</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>Paul Lacomblez, Éditeur</publisher>
							<date when="1898">1898</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1898">1898</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">HEURES</head><div type="poem" key="ELS9" rhyme="none">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Dormez-vous encor, paroissiens,</l>
						<l n="2" num="1.2">hier n’est plus, les anges causent</l>
						<l n="3" num="1.3">dans leurs jardins de fleurs de roses,</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1">et c’est matin villes en bleu,</l>
						<l n="5" num="2.2">villes en blanc, villes en Dieu,</l>
						<l n="6" num="2.3">avec les clochers au milieu</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">des maisons, des toits, des bâtisses,</l>
						<l n="8" num="3.2">des chapelles et des églises</l>
						<l n="9" num="3.3">et des oiseaux, haut, plein les cieux.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">Or, ici, et plus près la terre,</l>
						<l n="11" num="4.2">voici oraisons et prières,</l>
						<l n="12" num="4.3">et baptême, mauvais et bons ;</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">puis c’est le ciel vu de la mer,</l>
						<l n="14" num="5.2">et les vaisseaux par le travers,</l>
						<l n="15" num="5.3">et le soleil par le milieu,</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1">et lors le monde à son grand vœu,</l>
						<l n="17" num="6.2">et lors, au loin, toujours la mer,</l>
						<l n="18" num="6.3">et puis, ici, sur les chemins,</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1">mes bonnes villes familières,</l>
						<l n="20" num="7.2">où chacun a joie de sa pierre,</l>
						<l n="21" num="7.3">de sa maison et de ses saints.</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1">Mais alors c’est vous tous les miens,</l>
						<l n="23" num="8.2">et dormez-vous ? car le temps passe</l>
						<l n="24" num="8.3">et le pêcheur est à ses nasses ;</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">mais alors c’est vous tous les miens,</l>
						<l n="26" num="9.2">et dormez-vous ? car le temps vient ;</l>
						<l n="27" num="9.3">or le boulanger cuit son pain,</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1">et si sommeil vous est un bien,</l>
						<l n="29" num="10.2">voici passé le temps de grâce ;</l>
						<l n="30" num="10.3">dormez-vous encor, paroissiens ?</l>
					</lg>
				</div></body></text></TEI>