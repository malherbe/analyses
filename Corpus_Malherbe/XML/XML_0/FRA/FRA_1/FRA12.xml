<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les poèmes dorés</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>727 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Poèmes dorés</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesie-francaise.fr</publisher>
						<idno type="URL">http://www.poesie-francaise.fr/anatole-france-les-poemes-dores/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les poèmes dorés</title>
						<author>Anatole France</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>EDOUARD-JOSEPH, EDITEUR</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Remise en ordre des poèmes conforme à l’édition de référence.</p>
				<p>Les dates de fin de poème ont été ajoutées (édition : EDOUARD-JOSEPH, EDITEUR)</p>
				<p>Les deux sections manquantes du poème "Le Désir" ont été ajoutées (édition : EDOUARD-JOSEPH, EDITEUR)</p>
				<p>Nombreuses corrections de ponctuation</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>

				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA12">
				<head type="main">Le désir</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Je sais la vanité de tout désir profane.</l>
						<l n="2" num="1.2">A peine gardons-nous de tes amours défunts,</l>
						<l n="3" num="1.3">Femme, ce que la fleur qui sur ton sein se fane</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space>Y laisse d’âme et de parfums.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Ils n’ont, les plus beaux bras, que des chaînes d’argile,</l>
						<l n="6" num="2.2">Indolentes autour du col le plus aimé ;</l>
						<l n="7" num="2.3">Avant d’être rompu leur doux cercle fragile</l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space>Ne s’était pas même fermé.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Mélancolique nuit des chevelures sombres,</l>
						<l n="10" num="3.2">A quoi bon s’attarder dans ton enivrement,</l>
						<l n="11" num="3.3">Si, comme dans la mort, nul ne peut sous tes ombres</l>
						<l n="12" num="3.4"><space quantity="8" unit="char"></space>Se plonger éternellement ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Narines qui gonflez vos ailes de colombe,</l>
						<l n="14" num="4.2">Avec les longs dédains d’une belle fierté,</l>
						<l n="15" num="4.3">Pour la dernière fois, à l’odeur de la tombe,</l>
						<l n="16" num="4.4"><space quantity="8" unit="char"></space>Vous aurez déjà palpité.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Lèvres, vivantes fleurs, nobles roses sanglantes,</l>
						<l n="18" num="5.2">Vous épanouissant lorsque nous vous baisons,</l>
						<l n="19" num="5.3">Quelques feux de cristal en quelques nuits brûlantes</l>
						<l n="20" num="5.4"><space quantity="8" unit="char"></space>Sèchent vos brèves floraisons.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Où tend le vain effort de deux bouches unies ?</l>
						<l n="22" num="6.2">Le plus long des baisers trompe notre dessein ;</l>
						<l n="23" num="6.3">Et comment appuyer nos langueurs infinies</l>
						<l n="24" num="6.4"><space quantity="8" unit="char"></space>Sur la fragilité d’un sein ?</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="25" num="1.1">Mais la vague beauté des regards, d’où vient-elle,</l>
						<l n="26" num="1.2">Pour nous mettre en passant tant d’espérance au front.?</l>
						<l n="27" num="1.3">Et pourquoi rêvons-nous de lumière immortelle</l>
						<l n="28" num="1.4"><space quantity="8" unit="char"></space>Devant deux yeux qui s’éteindront.</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1">Femme, qui vous donna cette clarté sacrée</l>
						<l n="30" num="2.2">Dont vous avez béni la ferveur de mes yeux ?</l>
						<l n="31" num="2.3">Et d’où vient qu’en suivant votre trace adorée</l>
						<l n="32" num="2.4"><space quantity="8" unit="char"></space>Je sens un dieu mystérieux</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="33" num="1.1"><space quantity="8" unit="char"></space>Oh ! montrez un moment au monde</l>
						<l n="34" num="1.2"><space quantity="8" unit="char"></space>Votre fragilité féconde,</l>
						<l n="35" num="1.3"><space quantity="8" unit="char"></space>Et semez la vie à vos pieds !</l>
						<l n="36" num="1.4"><space quantity="8" unit="char"></space>Puis passez, formes éphémères;</l>
						<l n="37" num="1.5"><space quantity="8" unit="char"></space>Femmes, puisque vous êtes mères,</l>
						<l n="38" num="1.6"><space quantity="8" unit="char"></space>C’est qu’il convient que vous mouriez.</l>
					</lg>
					<lg n="2">
						<l n="39" num="2.1"><space quantity="8" unit="char"></space>Votre divinité ne dure,</l>
						<l n="40" num="2.2"><space quantity="8" unit="char"></space>Douces forces de la Nature,</l>
						<l n="41" num="2.3"><space quantity="8" unit="char"></space>Que ce qu’il faut pour son dessein.</l>
						<l n="42" num="2.4"><space quantity="8" unit="char"></space>La race impérissable et belle,</l>
						<l n="43" num="2.5"><space quantity="8" unit="char"></space>Voilà cette chose immortelle</l>
						<l n="44" num="2.6"><space quantity="8" unit="char"></space>Que l’on rêve sur votre sein !</l>
					</lg>
					<lg n="3">
						<l n="45" num="3.1"><space quantity="8" unit="char"></space>C’est par vous que l’heureuse vie</l>
						<l n="46" num="3.2"><space quantity="8" unit="char"></space>Tour à tour en la chair ravie</l>
						<l n="47" num="3.3"><space quantity="8" unit="char"></space>S’allume, et ne s’éteindra pas.</l>
						<l n="48" num="3.4"><space quantity="8" unit="char"></space>En vous la vie universelle</l>
						<l n="49" num="3.5"><space quantity="8" unit="char"></space>Éclate, et tout homme chancelle,</l>
						<l n="50" num="3.6"><space quantity="8" unit="char"></space>Ivre de beauté, sur vos pas.</l>
					</lg>
					<lg n="4">
						<l n="51" num="4.1"><space quantity="8" unit="char"></space>Vivez, mourez, pleines de grâce;</l>
						<l n="52" num="4.2"><space quantity="8" unit="char"></space>Les hommes et les dieux, tout passe</l>
						<l n="53" num="4.3"><space quantity="8" unit="char"></space>Mais la vie existe à jamais.</l>
						<l n="54" num="4.4"><space quantity="8" unit="char"></space>Et toi, forme, parfum, lumière,</l>
						<l n="55" num="4.5"><space quantity="8" unit="char"></space>Qui fleuris ma vertu première,</l>
						<l n="56" num="4.6"><space quantity="8" unit="char"></space>Ah! je sais pourquoi je t’aimais!</l>
					</lg>
				</div>
				<closer>
					<dateline>
						<date when="1869">Juin 1869</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>