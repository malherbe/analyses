<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les poèmes dorés</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>727 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Poèmes dorés</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesie-francaise.fr</publisher>
						<idno type="URL">http://www.poesie-francaise.fr/anatole-france-les-poemes-dores/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les poèmes dorés</title>
						<author>Anatole France</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>EDOUARD-JOSEPH, EDITEUR</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Remise en ordre des poèmes conforme à l’édition de référence.</p>
				<p>Les dates de fin de poème ont été ajoutées (édition : EDOUARD-JOSEPH, EDITEUR)</p>
				<p>Les deux sections manquantes du poème "Le Désir" ont été ajoutées (édition : EDOUARD-JOSEPH, EDITEUR)</p>
				<p>Nombreuses corrections de ponctuation</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>

				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA4">
				<head type="main">La mort du singe</head>
				<lg n="1">
					<l n="1" num="1.1">Dans la serre vitrée où de rigides plantes,</l>
					<l n="2" num="1.2">Filles d’une jeune île et d’un lointain soleil,</l>
					<l n="3" num="1.3">Sous un ciel toujours gris, sommeillant sans réveil,</l>
					<l n="4" num="1.4">Dressent leurs dards aigus et leurs floraisons lentes,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Lui, tremblant, secoué par la fièvre et la toux,</l>
					<l n="6" num="2.2">Tordant son triste corps sous des lambeaux de laine,</l>
					<l n="7" num="2.3">Entre ses longues dents pousse une rauque haleine</l>
					<l n="8" num="2.4">Et sur son sein velu croise ses longs bras roux.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ses yeux, vides de crainte et vides d’espérance,</l>
					<l n="10" num="3.2">Entre eux et chaque chose ignorent tout lien ;</l>
					<l n="11" num="3.3">Ils sont empreints, ces yeux qui ne regardent rien,</l>
					<l n="12" num="3.4">De la douceur que donne aux brutes la souffrance.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ses membres presque humains sont brûlants et frileux ;</l>
					<l n="14" num="4.2">Ses lèvres en s’ouvrant découvrent les gencives ;</l>
					<l n="15" num="4.3">Et, comme il va mourir, ses paumes convulsives</l>
					<l n="16" num="4.4">Ont caché pour jamais ses pouces musculeux.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Mais voici qu’il a vu le soleil disparaître</l>
					<l n="18" num="5.2">Derrière les huniers assemblés dans le port ;</l>
					<l n="19" num="5.3">Il l’a vu : son front bas se ride sous l’effort</l>
					<l n="20" num="5.4">Qu’il tente brusquement pour rassembler son être.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Songe-t-il que, parmi ses frères forestiers,</l>
					<l n="22" num="6.2">Alors qu’un chaud soleil descendait des cieux calmes,</l>
					<l n="23" num="6.3">Repu du lait des noix et couché sur les palmes,</l>
					<l n="24" num="6.4">Il s’endormait heureux dans ses frais cocotiers,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Avant qu’un grand navire, allant vers des mers froides,</l>
					<l n="26" num="7.2">L’emportât au milieu des clameurs des marins,</l>
					<l n="27" num="7.3">Pour qu’un jour, dans le vent, qui lui mordît les reins,</l>
					<l n="28" num="7.4">La toile, au long des mâts, glaçât ses membres roides ?</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">À cause de la fièvre aux souvenirs vibrants</l>
					<l n="30" num="8.2">Et du jeûne qui donne aux âmes l’allégeance,</l>
					<l n="31" num="8.3">Grâce à cette suprême et brève intelligence</l>
					<l n="32" num="8.4">Qui s’allume si claire au cerveau des mourants,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Ce muet héritier d’une race stupide</l>
					<l n="34" num="9.2">D’un rêve unique emplit ses esprits exaltés :</l>
					<l n="35" num="9.3">Il voit les bons soleils de ses jeunes étés,</l>
					<l n="36" num="9.4">Il abreuve ses yeux de leur flamme limpide.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Puis une vague nuit pèse en son crâne épais.</l>
					<l n="38" num="10.2">Laissant tomber sa nuque et ses lourdes mâchoires,</l>
					<l n="39" num="10.3">Il râle. Autour de lui croissent les ombres noires :</l>
					<l n="40" num="10.4">Minuit, l’heure où l’on meurt, lui versera la paix.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1872">1872</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>