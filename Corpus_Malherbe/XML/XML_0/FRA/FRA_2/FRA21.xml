<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES ET LÉGENDES</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>860 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/anatolfrancepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Anatole_France_-_Po%C3%A9sies.djvu</idno>
					</publicationStmt>
					<sourceDesc>
					<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les pièces manquantes ont été ajoutées à partir de Wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA21">
				<head type="main">Homaï</head>
				<lg n="1">
					<l n="1" num="1.1">Devant Djioun la blanche aux parfums de jacinthe,</l>
					<l n="2" num="1.2">Les fils au front cuivré des mangeurs de lézards,</l>
					<l n="3" num="1.3">À qui le Chamelier enseigna la loi sainte,</l>
					<l n="4" num="1.4">Avaient dressé leur camp et leurs bleus étendards.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ils s’étaient abattus comme des sauterelles.</l>
					<l n="6" num="2.2">Et déjà trente jours étaient passés depuis</l>
					<l n="7" num="2.3">Qu’ils entouraient la ville et que leurs sentinelles</l>
					<l n="8" num="2.4">Gardaient tous les sentiers des monts et tous les puits.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Or, tandis que, poussant une sifflante haleine,</l>
					<l n="10" num="3.2">Accroupis sur les murs, les hommes du pays</l>
					<l n="11" num="3.3">Voyaient les feux guerriers s’allumer par la plaine</l>
					<l n="12" num="3.4">Et les chevaux d’Yémen tondre les verts maïs,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Une femme, à pas sourds glissant, voilée et belle,</l>
					<l n="14" num="4.2">Par les bazars déserts et les noirs escaliers</l>
					<l n="15" num="4.3">Et les portes de cèdre ouvertes devant elle,</l>
					<l n="16" num="4.4">S’en allait dans la plaine au camp des cavaliers.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Une esclave, portant le vin et les olives,</l>
					<l n="18" num="5.2">Noire, au nez un anneau, la suivait en riant</l>
					<l n="19" num="5.3">Vers la tente où pendaient des crânes aux solives,</l>
					<l n="20" num="5.4">Près des yatagans nus d’acier souple et brillant.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Là, sur une peau fauve et de blanc étoilée,</l>
					<l n="22" num="6.2">Croisant les jambes, grave et seul, et de sa main</l>
					<l n="23" num="6.3">Lissant sa barbe courte, odorante et bouclée,</l>
					<l n="24" num="6.4">L’émir songeait : « Allah ! hâtons notre chemin. »</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Mais la femme à travers les ténèbres venue</l>
					<l n="26" num="7.2">Devant la tente ouverte apparut dans la nuit,</l>
					<l n="27" num="7.3">S’étant fait vers l’émir une route inconnue.</l>
					<l n="28" num="7.4">Quand la femme nous vient, sait-on qui la conduit ?</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Elle entra. Du nuage incertain de ses voiles</l>
					<l n="30" num="8.2">L’astre pur de son front se levait calme et blanc ;</l>
					<l n="31" num="8.3">Ses cheveux, comme un ciel, étaient semés d’étoiles,</l>
					<l n="32" num="8.4">Les gouttes froides des saphirs mouillaient son flanc ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Ses pieds nus s’avançaient dans la lueur des bagues,</l>
					<l n="34" num="9.2">Les rubis à l’orteil dardaient leurs yeux ardents.</l>
					<l n="35" num="9.3">Et dans l’air enivré d’odeurs tièdes et vagues</l>
					<l n="36" num="9.4">Elle sourit avec de la lumière aux dents.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Et la voyant sourire à travers l’ombre noire,</l>
					<l n="38" num="10.2">L’émir se crut ravi dans le séjour divin,</l>
					<l n="39" num="10.3">Et joyeux il eut peur et frémit, prêt à boire</l>
					<l n="40" num="10.4">À cette bouche offerte un délicieux vin.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">« Ô Beauté que l’Iran et la Nuit m’ont donnée,</l>
					<l n="42" num="11.2">Salut, dit-il ; et toi, Nuit de l’Irân, merci !</l>
					<l n="43" num="11.3">L’instant de ton regard vaut bien plus qu’une année,</l>
					<l n="44" num="11.4">Femme, car j’ai changé depuis que te voici.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">« Autrefois, au-devant du sabre et de la lance,</l>
					<l n="46" num="12.2">Au front des cavaliers, dans le sang et les cris.</l>
					<l n="47" num="12.3">Sur ma noire jument j’avançais en silence,</l>
					<l n="48" num="12.4">Méditant les versets sur ma poitrine écrits.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">« Quand, derrière mes pas, une ville naguère,</l>
					<l n="50" num="13.2">Brûlant comme un soleil qu’allumait ma vertu,</l>
					<l n="51" num="13.3">Faisait des croupes d’or à mes chevaux de guerre,</l>
					<l n="52" num="13.4">Je demandais quel nom cette ville avait eu.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">« Mes yeux ne voyaient pas la beauté des captives,</l>
					<l n="54" num="14.2">Je ne regardais pas où je versais la mort,</l>
					<l n="55" num="14.3">Mon oreille était loin des nations plaintives,</l>
					<l n="56" num="14.4">Et j’étais seulement la Colère du Sort.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">« Mais à l’heure où tes yeux jettent leurs puissants charmes,</l>
					<l n="58" num="15.2">Est-il encore un monde et des colères ? non !</l>
					<l n="59" num="15.3">Ô vierge, dont les bras sont plus beaux que des armes,</l>
					<l n="60" num="15.4">Me connais-tu ? Celui qui t’aime est mon seul nom.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">« Voyant ton sein blanchir l’étoffe aux molles trames,</l>
					<l n="62" num="16.2">Dont la myrrhe a charmé les plis mystérieux,</l>
					<l n="63" num="16.3">Je pleure, ainsi que font les fils des jeunes femmes</l>
					<l n="64" num="16.4">Quand un songe mauvais entre dans leurs doux yeux.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">« Mon âme, que je sens s’exhaler en tendresse,</l>
					<l n="66" num="17.2">Flotte comme une haleine autour de ta beauté :</l>
					<l n="67" num="17.3">Me voici devenu faible de ta faiblesse,</l>
					<l n="68" num="17.4">Et je puis être atteint dans ta fragilité.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">« Ne me fais pas de mal, ô compagne étrangère !</l>
					<l n="70" num="18.2">À quoi bon me trahir ? je veux ce que tu veux,</l>
					<l n="71" num="18.3">Et mon esprit n’est plus qu’une essence légère</l>
					<l n="72" num="18.4">Qui se mêle en riant au nard de tes cheveux.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">« Ne me fais pas de mal ! mon salut et ma perte</l>
					<l n="74" num="19.2">Sont deux enfants jumeaux couchés dans tes bras nus,</l>
					<l n="75" num="19.3">Et ma vie et ma mort sur ta lèvre entr’ouverte</l>
					<l n="76" num="19.4">Tiennent conseil. Pourquoi tes pieds sont-ils venus ?</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">« Dis-moi ton nom : qu’il soit plus doux à mon oreille</l>
					<l n="78" num="20.2">Que le bruit d’une source au fond des déserts blancs ! »</l>
					<l n="79" num="20.3">La vierge alors parla ; sa voix sonnait, pareille</l>
					<l n="80" num="20.4">Au vent frais du matin dans les rosiers tremblants :</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">« Dans les jardins d’Irân, parmi les tubéreuses,</l>
					<l n="82" num="21.2">Naguère on me nommait Homaï, l’oiseau clair ;</l>
					<l n="83" num="21.3">Mais je veux, étranger, de tes lèvres heureuses</l>
					<l n="84" num="21.4">Recevoir le seul nom qui me restera cher.</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">« Pourquoi je suis venue ? Et pourquoi les étoiles</l>
					<l n="86" num="22.2">Viennent-elles au ciel fidèlement le soir ? »</l>
					<l n="87" num="22.3">Elle mêla ces mots au frisson de ses voiles,</l>
					<l n="88" num="22.4">Et sur la toison fauve alla tout droit s’asseoir.</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">La ceinture, où des mots brillaient pleins de mystère,</l>
					<l n="90" num="23.2">Glissa comme un serpent blessé sur ses genoux.</l>
					<l n="91" num="23.3">L’émir dit : « Nous allons étouffer sur la terre :</l>
					<l n="92" num="23.4">Le monde des vivants est trop étroit pour nous.</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">« Au dos de mon cheval veux-tu que je te couche ?</l>
					<l n="94" num="24.2">Son galop vers la mer bercera ton sommeil,</l>
					<l n="95" num="24.3">Les vagues baiseront tes pieds, tes flancs, ta bouche,</l>
					<l n="96" num="24.4">Et je te porterai dans le lit du soleil ! »</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Homaï, dans ses bras immobile et sereine,</l>
					<l n="98" num="25.2">Laissait son clair regard se refléter en noir</l>
					<l n="99" num="25.3">Dans le sabre pendu contre un pilier d’ébène :</l>
					<l n="100" num="25.4">Elle se contemplait au fond de ce miroir.</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Puis, en se renversant, sa tête inerte et belle</l>
					<l n="102" num="26.2">Entraîna son regard qui flotta mollement.</l>
					<l n="103" num="26.3">Vers l’heure où le nopal fleurit, l’émir près d’elle</l>
					<l n="104" num="26.4">S’endormit dans la joie et dans l’apaisement.</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1">Le sabre nu brillait dans l’ombre vague et terne.</l>
					<l n="106" num="27.2">Sur son coude pensif se dressant à demi,</l>
					<l n="107" num="27.3">Comme un enfant se penche au bord d’une citerne,</l>
					<l n="108" num="27.4">La femme se pencha sur l’émir endormi.</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1">Son sommeil comparable à des eaux paresseuses,</l>
					<l n="110" num="28.2">Pleines d’îles de fleurs, coulait heureux et lent.</l>
					<l n="111" num="28.3">Homâï, de la voix chantante des berceuses,</l>
					<l n="112" num="28.4">Dit, en rendant plus doux son regard indolent :</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1">« Je voudrais n’être pas près de toi pour ta perte,</l>
					<l n="114" num="29.2">Mais tout vouloir est vain : je t’aime, et tu mourras.</l>
					<l n="115" num="29.3">Un Esprit est en moi ; mon âme assiste inerte</l>
					<l n="116" num="29.4">À tout ce que l’Esprit accomplit par mon bras.</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1">« Un soir que je croisais les bras sur ma terrasse,</l>
					<l n="118" num="30.2">Les Mages m’ont parlé : « Qu’Ormuzd soit obéi.</l>
					<l n="119" num="30.3">« Ormuzd a mis en toi le salut de ta race. »</l>
					<l n="120" num="30.4">Hélas ! j’ai, ce soir-là, cessé d’être Homaï.</l>
				</lg>
				<lg n="31">
					<l n="121" num="31.1">« Car ils m’ont fait rester, six jours, sans nourriture,</l>
					<l n="122" num="31.2">Dans un lieu souterrain, à la façon des morts.</l>
					<l n="123" num="31.3">C’est là que j’ai perdu mon humaine nature,</l>
					<l n="124" num="31.4">Et qu’un Esprit subtil est entré dans mon corps.</l>
				</lg>
				<lg n="32">
					<l n="125" num="32.1">« Puis ils m’ont dit : « Revêts d’une étoffe éclatante</l>
					<l n="126" num="32.2">« Ta chair purifiée et qui dompta l’effroi,</l>
					<l n="127" num="32.3">« Ô vierge, et va frapper l’ennemi dans sa tente. »</l>
					<l n="128" num="32.4">Ils m’ont dit, et mes pieds sont allés jusqu’à toi.</l>
				</lg>
				<lg n="33">
					<l n="129" num="33.1">« J’ai goûté l’herbe en fleur dont la vertu savante</l>
					<l n="130" num="33.2">Nous ravit loin du corps dans un monde divin ;</l>
					<l n="131" num="33.3">C’est pourquoi désormais l’ennui d’être vivante,</l>
					<l n="132" num="33.4">Comme un champ de pavots, remplira tout mon sein.</l>
				</lg>
				<lg n="34">
					<l n="133" num="34.1">« Quand ma main aura fait ce que l’Esprit ordonne,</l>
					<l n="134" num="34.2">Je la contemplerai sans haine et sans regrets :</l>
					<l n="135" num="34.3">Je sais que vivre est vain, et que la mort est bonne,</l>
					<l n="136" num="34.4">Qu’elle a des charmes doux et de profonds secrets. »</l>
				</lg>
				<lg n="35">
					<l n="137" num="35.1">Elle dit, souleva du doigt le bras tranquille</l>
					<l n="138" num="35.2">Qui s’était replié tiède et brun sur son flanc ;</l>
					<l n="139" num="35.3">Souple, elle en dégagea sans bruit sa taille habile</l>
					<l n="140" num="35.4">Et sur le tapis sourd assura son pied blanc ;</l>
				</lg>
				<lg n="36">
					<l n="141" num="36.1">Et, chaude encor du lit, dans sa robe froissée,</l>
					<l n="142" num="36.2">Lente, elle s’approcha du pilier de bois noir,</l>
					<l n="143" num="36.3">Et saisit la poignée éclatante et glacée</l>
					<l n="144" num="36.4">Du sabre dont l’acier lui servit de miroir.</l>
				</lg>
				<lg n="37">
					<l n="145" num="37.1">Elle dit : « Astres clairs, qui contemplez ma face,</l>
					<l n="146" num="37.2">Nuit, qui suspends la vie et ses œuvres mauvais,</l>
					<l n="147" num="37.3">Je ferai devant vous ce qu’il faut que je fasse,</l>
					<l n="148" num="37.4">Et vous connaîtrez seuls les raisons que j’avais. »</l>
				</lg>
				<lg n="38">
					<l n="149" num="38.1">Elle embrassa l’émir d’un regard calme et tendre,</l>
					<l n="150" num="38.2">Éleva lentement le sabre, sans effort,</l>
					<l n="151" num="38.3">Et dans le cou, que l’homme avait pris soin de tendre,</l>
					<l n="152" num="38.4">Plongea, les yeux fermés, le tranchant et la mort.</l>
				</lg>
				<lg n="39">
					<l n="153" num="39.1">L’esclave alors saisit cette tête aux chairs mates</l>
					<l n="154" num="39.2">Que la femme venait de baiser longuement,</l>
					<l n="155" num="39.3">La mit dans une coupe avec des aromates,</l>
					<l n="156" num="39.4">Et murmura d’orgueil et de contentement.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Juin 1870.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>