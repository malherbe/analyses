<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DOULEURS ET COLÈRES</head><head type="sub_part">Vers trouvés sur la berge</head><div type="poem" key="CRO150">
					<head type="main">Aux imbéciles</head>
					<lg n="1">
						<l n="1" num="1.1"><space quantity="6" unit="char"></space>Quant nous irisons</l>
						<l n="2" num="1.2"><space quantity="6" unit="char"></space>Tous nos horizons</l>
						<l n="3" num="1.3">D’émeraudes et de cuivre,</l>
						<l n="4" num="1.4"><space quantity="6" unit="char"></space>Les gens bien assis</l>
						<l n="5" num="1.5"><space quantity="6" unit="char"></space>Exempts de soucis</l>
						<l n="6" num="1.6">Ne doivent pas nous poursuivre.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><space quantity="6" unit="char"></space>On devient très fin,</l>
						<l n="8" num="2.2"><space quantity="6" unit="char"></space>Mais on meurt de faim,</l>
						<l n="9" num="2.3">A jouer de la guitare,</l>
						<l n="10" num="2.4"><space quantity="6" unit="char"></space>On n’est emporté,</l>
						<l n="11" num="2.5"><space quantity="6" unit="char"></space>L’hiver ni l’été,</l>
						<l n="12" num="2.6">Dans le train d’aucune gare.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><space quantity="6" unit="char"></space>Le chemin de fer</l>
						<l n="14" num="3.2"><space quantity="6" unit="char"></space>Est vraiment trop cher.</l>
						<l n="15" num="3.3">Le steamer fendeur de l’onde</l>
						<l n="16" num="3.4"><space quantity="6" unit="char"></space>Est plus cher encor ;</l>
						<l n="17" num="3.5"><space quantity="6" unit="char"></space>Il faut beaucoup d’or</l>
						<l n="18" num="3.6">Pour aller au bout du monde.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><space quantity="6" unit="char"></space>Donc, gens bien assis,</l>
						<l n="20" num="4.2"><space quantity="6" unit="char"></space>Exempts de soucis,</l>
						<l n="21" num="4.3">Méfiez-vous du poète,</l>
						<l n="22" num="4.4"><space quantity="6" unit="char"></space>Qui peut, ayant faim,</l>
						<l n="23" num="4.5"><space quantity="6" unit="char"></space>Vous mettre, à la fin,</l>
						<l n="24" num="4.6">Quelques balles dans la tête.</l>
					</lg>
				</div></body></text></TEI>