<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSE</head><div type="poem" key="CRO160">
					<head type="main">Aux femmes</head>
					<lg n="1">
						<l n="1" num="1.1">Noyez dans un regard limpide, aérien,</l>
						<l n="2" num="1.2"><space quantity="20" unit="char"></space>Les douleurs.</l>
						<l n="3" num="1.3">Ne dites rien de mal, ne dites rien de bien,</l>
						<l n="4" num="1.4"><space quantity="20" unit="char"></space>Soyez fleurs.</l>
						<l n="5" num="1.5">Soyez fleurs : par ces temps enragés, enfumés</l>
						<l n="6" num="1.6"><space quantity="20" unit="char"></space>De charbon,</l>
						<l n="7" num="1.7">Soyez roses et lys. Et puis, aimez, aimez !</l>
						<l n="8" num="1.8"><space quantity="20" unit="char"></space>C’est si bon !…</l>
					</lg>
					<ab type="star">※※※</ab>
					<lg n="2">
						<l n="9" num="2.1"><space quantity="4" unit="char"></space>Il y a la fleur, il y a la femme,</l>
						<l n="10" num="2.2"><space quantity="4" unit="char"></space>Il y a le bois où l’on peut courir</l>
						<l n="11" num="2.3"><space quantity="4" unit="char"></space>Il y a l’étang où l’on peut mourir.</l>
						<l n="12" num="2.4"><space quantity="4" unit="char"></space>Alors, que nous fait l’éloge ou le blâme ?</l>
					</lg>
					<ab type="star">※※※</ab>
					<lg n="3">
						<l n="13" num="3.1"><space quantity="8" unit="char"></space>L’aurore naît et la mort vient.</l>
						<l n="14" num="3.2"><space quantity="8" unit="char"></space>Qu’ai-je fait de mal ou de bien ?</l>
						<l n="15" num="3.3"><space quantity="8" unit="char"></space>Je suis emporté par l’orage,</l>
						<l n="16" num="3.4"><space quantity="8" unit="char"></space>Riant, pleurant, mais jamais sage.</l>
					</lg>
					<ab type="star">※※※</ab>
					<lg n="4">
						<l n="17" num="4.1"><space quantity="8" unit="char"></space>Ceux qui dédaignent les amours</l>
						<l n="18" num="4.2"><space quantity="20" unit="char"></space>Ont tort, ont tort,</l>
						<l n="19" num="4.3"><space quantity="8" unit="char"></space>Car le soleil brille toujours ;</l>
						<l n="20" num="4.4"><space quantity="20" unit="char"></space>La Mort, la Mort</l>
						<l n="21" num="4.5"><space quantity="8" unit="char"></space>Vient vite et les sentiers sont courts.</l>
					</lg>
					<ab type="star">※※※</ab>
					<lg n="5">
						<l n="22" num="5.1"><space quantity="8" unit="char"></space>Comme tu souffres, mon pays,</l>
						<l n="23" num="5.2"><space quantity="8" unit="char"></space>Ô lumineuse, ô douce France,</l>
						<l n="24" num="5.3"><space quantity="8" unit="char"></space>Et tous les peuples ébahis</l>
						<l n="25" num="5.4"><space quantity="8" unit="char"></space>Ne comprennent pas ta souffrance.</l>
					</lg>
				</div></body></text></TEI>