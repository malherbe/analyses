<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS PERPÉTUELLES</head><div type="poem" key="CRO21">
					<head type="main">Chanson de route arya</head>
					<lg n="1">
						<l n="1" num="1.1">Fiers sur nos chevaux, tribu souveraine</l>
						<l n="2" num="1.2">Poussons devant nous les troupeaux bêlants,</l>
						<l n="3" num="1.3">Les bœufs mugissants. Que chacun emmène,</l>
						<l n="4" num="1.4">Enlacée à lui de ses beaux bras blancs,</l>
						<l n="5" num="1.5">L’amoureuse. Car la halte est prochaine.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">Partis du pays des hauts pics neigeux,</l>
						<l n="7" num="2.2">Des vallons mouillés par les sources vives,</l>
						<l n="8" num="2.3">Nous en emportons les rires, les jeux,</l>
						<l n="9" num="2.4">Les frais souvenirs, sans chansons plaintives.</l>
						<l n="10" num="2.5">Aux pays nouveaux nous trouverons mieux.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">Esclaves, hâtez la marche endormante</l>
						<l n="12" num="3.2">Des troupeaux. L’agneau sait téter alors</l>
						<l n="13" num="3.3">Que la brebis court en broutant la menthe.</l>
						<l n="14" num="3.4">De vos aiguillons piquez les bœufs forts,</l>
						<l n="15" num="3.5">Les bœufs paresseux que le taon tourmente.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">Mais on ralentit l’allure, à travers</l>
						<l n="17" num="4.2">Les bois ; on descend de cheval ; et, blanches,</l>
						<l n="18" num="4.3">Nos filles s’en,vont dans les buissons verts,</l>
						<l n="19" num="4.4">Les cheveux au vent, écartant les branches,</l>
						<l n="20" num="4.5">Cherchant avec nous des chemins ouverts.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">Posant leurs pieds blancs sur les feuilles sèches,</l>
						<l n="22" num="5.2">Elles font tinter autour de leurs cous</l>
						<l n="23" num="5.3">Et sur leurs beaux seins, doux comme les pêches,</l>
						<l n="24" num="5.4">L’or et l’argent fins, sonores bijoux.</l>
						<l n="25" num="5.5">Ainsi nous marchons dans les forêts fraîches.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1">Voici l’aube. Allons ! Assez de sommeil !</l>
						<l n="27" num="6.2">N’attendons pas ceux qui sont lents à suivre,</l>
						<l n="28" num="6.3">Voici que le jour s’est levé vermeil.</l>
						<l n="29" num="6.4">Nous vaincrons les nains d’ébène ou de cuivre</l>
						<l n="30" num="6.5">Dans les beaux pays chauffés du soleil.</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1">Et les nains, sachant nos cœurs indomptables,</l>
						<l n="32" num="7.2">Seront conducteurs et graisseurs tremblants</l>
						<l n="33" num="7.3">De nos chariots, nettoyeurs d’étables,</l>
						<l n="34" num="7.4">Pour garder vos doigts rosés, vos bras blancs</l>
						<l n="35" num="7.5">Filles de sang pur, aux yeux désirables.</l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1">Aux peuples soumis, à terre ployés</l>
						<l n="37" num="8.2">Les soins du labour et du pâturage,</l>
						<l n="38" num="8.3">Nous sur les hauteurs et dans les halliers</l>
						<l n="39" num="8.4">Consultant le vent, les bruits du feuillage,</l>
						<l n="40" num="8.5">Combattons les loups et les sangliers.</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1">Laissons les troupeaux brouter dans la plaine.</l>
						<l n="42" num="9.2">Ne tuez jamais les douces brebis,</l>
						<l n="43" num="9.3">Car nous leur prenons le lait et la laine.</l>
						<l n="44" num="9.4">La vache offre aussi le lait de ses pis.</l>
						<l n="45" num="9.5">Réjouissons-nous quand la vache est pleine.</l>
					</lg>
					<lg n="10">
						<l n="46" num="10.1">Le lait rend joyeux les roses enfants</l>
						<l n="47" num="10.2">Qui font oublier la pensée amère.</l>
						<l n="48" num="10.3">Du lait nous faisons les fromages blancs,</l>
						<l n="49" num="10.4">Piquants, bons avec le vin et la bière.</l>
						<l n="50" num="10.5">Ne tuez jamais la vache aux beaux flancs.</l>
					</lg>
					<lg n="11">
						<l n="51" num="11.1">Les bœufs mugissants ont la lente allure</l>
						<l n="52" num="11.2">Qui laisse dormir dans les chariots</l>
						<l n="53" num="11.3">Nos enfants, parmi la chaude fourrure.</l>
						<l n="54" num="11.4">Respectez les bœufs, aussi les taureaux</l>
						<l n="55" num="11.5">Fécondeurs jaloux, puissants d’encolure.</l>
					</lg>
					<lg n="12">
						<l n="56" num="12.1">Le soir nous rentrons de la chasse, fiers</l>
						<l n="57" num="12.2">Du gibier conquis. Le chevreuil, le lièvre,</l>
						<l n="58" num="12.3">Le sanglier noir, odorantes chairs</l>
						<l n="59" num="12.4">Et les tourdes gras, nourris de genièvre</l>
						<l n="60" num="12.5">Rôtissent, fumants, devant les feux clairs.</l>
					</lg>
					<lg n="13">
						<l n="61" num="13.1">Les femmes alors nous montrent contentes</l>
						<l n="62" num="13.2">La laine et le lin qu’elles ont filés,</l>
						<l n="63" num="13.3">Pendant que jouaient, à l’ombre des tentes,</l>
						<l n="64" num="13.4">Les enfants bruyants, aux yeux éveillés</l>
						<l n="65" num="13.5">Sous le buisson roux des boucles flottantes.</l>
					</lg>
					<lg n="14">
						<l n="66" num="14.1">Aux repas du soir, avant le repos,</l>
						<l n="67" num="14.2">Alors que sont cuits les lièvres, les tourdes,</l>
						<l n="68" num="14.3">Quand la bière d’or mousse dans les pots,</l>
						<l n="69" num="14.4">Quand le vin vermeil sonne dans les gourdes,</l>
						<l n="70" num="14.5">On chante les faits des anciens héros.</l>
					</lg>
					<lg n="15">
						<l n="71" num="15.1">La lueur des feux, les rayons de lune</l>
						<l n="72" num="15.2">Éclairent, la nuit, vos souples contours,</l>
						<l n="73" num="15.3">Filles de sang pur. Ô vous, que chacune</l>
						<l n="74" num="15.4">A chacun de nous donne ses amours,</l>
						<l n="75" num="15.5">Et livre son corps blanc, dans la nuit brune.</l>
					</lg>
					<lg n="16">
						<l n="76" num="16.1">Il fait mal celui qui, loin des amis</l>
						<l n="77" num="16.2">Se glisse, oublieux de nos lois hautaines,</l>
						<l n="78" num="16.3">Sous les chariots où sont endormis</l>
						<l n="79" num="16.4">Nos fils purs, et va, la nuit vers les naines</l>
						<l n="80" num="16.5">Filles sans beauté des peuples soumis.</l>
					</lg>
					<lg n="17">
						<l n="81" num="17.1">De là, lis enfants mêlés, détestables,</l>
						<l n="82" num="17.2">Serviteurs mauvais de nos enfants purs,</l>
						<l n="83" num="17.3">Qui, multipliés ainsi que les sables</l>
						<l n="84" num="17.4">Feront révolter, dans les jours futurs,</l>
						<l n="85" num="17.5">Les peuples soumis, nettoyeurs d’étables.</l>
					</lg>
					<lg n="18">
						<l n="86" num="18.1">Donc, il faut chasser les instincts troublants</l>
						<l n="87" num="18.2">Et laisser entre eux s’unir les esclaves,</l>
						<l n="88" num="18.3">Graisseurs des moyeux, piqueurs des bœufs lents,</l>
						<l n="89" num="18.4">Tandis que, le soir, nos filles suaves</l>
						<l n="90" num="18.5">S’enlacent à nous de leurs beaux bras blancs.</l>
					</lg>
					<lg n="19">
						<l n="91" num="19.1">En route, à cheval, tribu souveraine,</l>
						<l n="92" num="19.2">Héros descendus des hauts pics neigeux ;</l>
						<l n="93" num="19.3">Filles aux pieds blancs que chacun emmène !</l>
						<l n="94" num="19.4">Nous retrouverons les rires, les jeux,</l>
						<l n="95" num="19.5">Et l’amour ce soir ; la halte est prochaine.</l>
					</lg>
				</div></body></text></TEI>