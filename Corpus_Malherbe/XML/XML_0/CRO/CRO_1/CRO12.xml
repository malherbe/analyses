<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS PERPÉTUELLES</head><div type="poem" key="CRO12">
					<head type="main">L’Archet</head>
					<opener>
						<salute>A Mademoiselle Hjardemaal.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Elle avait de beaux cheveux, blonds</l>
						<l n="2" num="1.2">Comme une moisson d’août, si longs</l>
						<l n="3" num="1.3">Qu’ils lui tombaient jusqu’aux talons.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1">Elle avait une voix étrange,</l>
						<l n="5" num="2.2">Musicale, de fée ou d’ange,</l>
						<l n="6" num="2.3">Des yeux verts sous leur noire frange.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="3">
						<l n="7" num="3.1">Lui, ne craignait pas de rival,</l>
						<l n="8" num="3.2">Quand il traversait mont ou val,</l>
						<l n="9" num="3.3">En l’emportant sur son cheval.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">Car, pour tous ceux de la contrée,</l>
						<l n="11" num="4.2">Altière elle s’était montrée,</l>
						<l n="12" num="4.3">Jusqu’au jour qu’il l’eut rencontrée.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="5">
						<l n="13" num="5.1">L’amour la prit si fort au cœur,</l>
						<l n="14" num="5.2">Que pour un sourire moqueur,</l>
						<l n="15" num="5.3">Il lui vint un mal de langueur.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1">Et dans ses dernières caresses :</l>
						<l n="17" num="6.2">« Fais un archet avec mes tresses,</l>
						<l n="18" num="6.3">Pour charmer tes autres maîtresses. »</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1">Puis, dans un long baiser nerveux,</l>
						<l n="20" num="7.2">Elle mourut. Suivant ses vœux,</l>
						<l n="21" num="7.3">Il fit l’archet de ses cheveux.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="8">
						<l n="22" num="8.1">Comme un aveugle qui marmonne,</l>
						<l n="23" num="8.2">Sur un violon de Crémone</l>
						<l n="24" num="8.3">Il jouait, demandant l’aumône.</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">Tous avaient d’enivrants frissons</l>
						<l n="26" num="9.2">A l’écouter. Car dans ces sons</l>
						<l n="27" num="9.3">Vivaient la morte et ses chansons.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="10">
						<l n="28" num="10.1">Le roi, charmé, fit sa fortune.</l>
						<l n="29" num="10.2">Lui, sut plaire à la reine brune</l>
						<l n="30" num="10.3">Et l’enlever au clair de lune.</l>
					</lg>
					<lg n="11">
						<l n="31" num="11.1">Mais, chaque fois qu’il y touchait</l>
						<l n="32" num="11.2">Pour plaire à la reine, l’archet</l>
						<l n="33" num="11.3">Tristement le lui reprochait.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="12">
						<l n="34" num="12.1">Au son du funèbre langage,</l>
						<l n="35" num="12.2">Ils moururent à mi-voyage.</l>
						<l n="36" num="12.3">Et la morte reprit son gage.</l>
					</lg>
					<lg n="13">
						<l n="37" num="13.1">Elle reprit ses cheveux, blonds</l>
						<l n="38" num="13.2">Comme une moisson d’août, si longs</l>
						<l n="39" num="13.3">Qu’ils lui tombaient jusqu’aux talons.</l>
					</lg>
				</div></body></text></TEI>