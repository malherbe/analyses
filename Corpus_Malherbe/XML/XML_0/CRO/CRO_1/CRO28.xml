<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO28">
					<head type="main">Triolets fantaisistes</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Sidonie a plus d’un amant,</l>
							<l n="2" num="1.2">C’est une chose bien connue</l>
							<l n="3" num="1.3">Qu’elle avoue, elle, fièrement.</l>
							<l n="4" num="1.4">Sidonie a plus d’un amant</l>
							<l n="5" num="1.5">Parce que, pour elle, être nue</l>
							<l n="6" num="1.6">Est son plus charmant vêtement.</l>
							<l n="7" num="1.7">C’est une chose bien connue,</l>
							<l n="8" num="1.8">Sidonie a plus d’un amant.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="9" num="1.1">Elle en prend à ses cheveux blonds</l>
							<l n="10" num="1.2">Comme, à sa toile, l’araignée</l>
							<l n="11" num="1.3">Prend les mouches et les frelons.</l>
							<l n="12" num="1.4">Elle en prend à ses cheveux blonds.</l>
							<l n="13" num="1.5">Vers sa prunelle ensoleillée</l>
							<l n="14" num="1.6">Ils volent, pauvres papillons.</l>
							<l n="15" num="1.7">Comme, à sa toile, l’araignée</l>
							<l n="16" num="1.8">Elle en prend à ses cheveux blonds.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="17" num="1.1">Elle en attrape avec les dents</l>
							<l n="18" num="1.2">Quand le rire entr’ouvre sa bouche</l>
							<l n="19" num="1.3">Et dévore les imprudents.</l>
							<l n="20" num="1.4">Elle en attrape avec les dents.</l>
							<l n="21" num="1.5">Sa bouche, quand elle se couche,</l>
							<l n="22" num="1.6">Reste rose et ses dents dedans.</l>
							<l n="23" num="1.7">Quand le rire entr’ouvre sa bouche</l>
							<l n="24" num="1.8">Elle en attrape avec les dents.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<lg n="1">
							<l n="25" num="1.1">Elle les mène par le nez,</l>
							<l n="26" num="1.2">Comme fait, dit-on, le crotale</l>
							<l n="27" num="1.3">Des oiseaux qu’il a fascinés.</l>
							<l n="28" num="1.4">Elle les mène par le nez.</l>
							<l n="29" num="1.5">Quand dans une moue elle étale</l>
							<l n="30" num="1.6">Sa langue à leurs yeux étonnés,</l>
							<l n="31" num="1.7">Comme fait, dit-on, le crotale</l>
							<l n="32" num="1.8">Elle les mène par le nez.</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="number">V</head>
						<lg n="1">
							<l n="33" num="1.1">Sidonie a plus d’un amant,</l>
							<l n="34" num="1.2">Qu’on le lui reproche ou l’en loue</l>
							<l n="35" num="1.3">Elle s’en moque également.</l>
							<l n="36" num="1.4">Sidonie a plus d’un amant.</l>
							<l n="37" num="1.5">Aussi, jusqu’à ce qu’on la cloue</l>
							<l n="38" num="1.6">Au sapin de l’enterrement,</l>
							<l n="39" num="1.7">Qu’on le lui reproche ou l’en loue,</l>
							<l n="40" num="1.8">Sidoine aura plus d’un amant.</l>
						</lg>
					</div>
				</div></body></text></TEI>