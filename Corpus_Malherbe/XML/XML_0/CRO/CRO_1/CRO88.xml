<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO88">
					<head type="main">Le Hareng saur</head>
					<opener>
						<salute>A Guy</salute>
					</opener>
					<lg n="1">
						<l rhyme="none" n="1" num="1.1">Il était un grand mur blanc <del hand="RR" reason="analysis" type="irrelevant">— nu, nu, nu,</del></l>
						<l rhyme="none" n="2" num="1.2">Contre le mur une échelle <del hand="RR" reason="analysis" type="irrelevant">— haute, haute, haute,</del></l>
						<l rhyme="none" n="3" num="1.3">Et, par terre, un hareng saur <del hand="RR" reason="analysis" type="irrelevant">— sec, sec, sec,</del></l>
					</lg>
					<lg n="2">
						<l rhyme="none" n="4" num="2.1">Il vient, tenant dans ses mains<del hand="RR" reason="analysis" type="irrelevant">— sales, sales, sales,</del></l>
						<l rhyme="none" n="5" num="2.2">Un marteau lourd, un grand clou <del hand="RR" reason="analysis" type="irrelevant">— pointu, pointu, pointu</del></l>
						<l rhyme="none" n="6" num="2.3">Un peloton de ficelle <del hand="RR" reason="analysis" type="irrelevant">— gros, gros, gros,</del></l>
					</lg>
					<lg n="3">
						<l rhyme="none" n="7" num="3.1">Alors il monte à l’échelle <del hand="RR" reason="analysis" type="irrelevant">— haute, haute, haute,</del></l>
						<l rhyme="none" n="8" num="3.2">Et plante le clou pointu <del hand="RR" reason="analysis" type="irrelevant">— toc, toc, toc,</del></l>
						<l rhyme="none" n="9" num="3.3">Tout en haut du grand mur blanc <del hand="RR" reason="analysis" type="irrelevant">— nu, nu, nu</del></l>
					</lg>
					<lg n="4">
						<l rhyme="none" n="10" num="4.1">Il laisse aller le marteau <del hand="RR" reason="analysis" type="irrelevant">— qui tombe, qui tombe, qui tombe,</del></l>
						<l rhyme="none" n="11" num="4.2">Attache au clou la ficelle <del hand="RR" reason="analysis" type="irrelevant">— longue, longue, longue,</del></l>
						<l rhyme="none" n="12" num="4.3">Et, au bout, le hareng saur <del hand="RR" reason="analysis" type="irrelevant">— sec, sec, sec,</del></l>
					</lg>
					<lg n="5">
						<l rhyme="none" n="13" num="5.1">Il redescend de l’échelle <del hand="RR" reason="analysis" type="irrelevant">— haute, haute, haute,</del></l>
						<l rhyme="none" n="14" num="5.2">L’emporte avec le marteau <del hand="RR" reason="analysis" type="irrelevant">— lourd, lourd, lourd,</del></l>
						<l rhyme="none" n="15" num="5.3">Et puis, il s’en va ailleurs <del hand="RR" reason="analysis" type="irrelevant">— loin, loin, loin,</del></l>
					</lg>
					<lg n="6">
						<l rhyme="none" n="16" num="6.1">Et, depuis, le hareng saur <del hand="RR" reason="analysis" type="irrelevant">— sec, sec, sec,</del></l>
						<l rhyme="none" n="17" num="6.2">Au bout de cette ficelle <del hand="RR" reason="analysis" type="irrelevant">— longue, longue, longue,</del></l>
						<l rhyme="none" n="18" num="6.3">Très lentement se balance <del hand="RR" reason="analysis" type="irrelevant">— toujours, toujours, toujours,</del></l>
					</lg>
					<lg n="7">
						<l rhyme="none" n="19" num="7.1">J’ai composé cette histoire <del hand="RR" reason="analysis" type="irrelevant">— simple, simple, simple,</del></l>
						<l rhyme="none" n="20" num="7.2">Pour mettre en fureur les gens <del hand="RR" reason="analysis" type="irrelevant">— graves, graves, graves,</del></l>
						<l rhyme="none" n="21" num="7.3">Et amuser les enfants <del hand="RR" reason="analysis" type="irrelevant">— petits, petits, petits.</del></l>
					</lg>
				</div></body></text></TEI>