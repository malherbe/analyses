<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO42">
					<head type="main">Rancœur lasse</head>
					<lg n="1">
						<l n="1" num="1.1">Malgré sa folle trahison</l>
						<l n="2" num="1.2">N’est-elle pas encor la même ?</l>
						<l n="3" num="1.3">La fierté n’est plus de saison.</l>
						<l n="4" num="1.4"><space quantity="18" unit="char"></space>Je l’aime.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="2">
						<l n="5" num="2.1">Je sais qu’elle reste, malgré</l>
						<l n="6" num="2.2">D’impurs contacts, vierge éternelle,</l>
						<l n="7" num="2.3">Qu’aucun venin n’a pénétré</l>
						<l n="8" num="2.4"><space quantity="18" unit="char"></space>En elle,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Marbre trop charnel qui subit</l>
						<l n="10" num="3.2">Toutes souillures, mais les brave ;</l>
						<l n="11" num="3.3">Puisque la pluie, en une nuit,</l>
						<l n="12" num="3.4"><space quantity="18" unit="char"></space>Le lave.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="4">
						<l n="13" num="4.1">Même au temps des premiers regards,</l>
						<l n="14" num="4.2">Je la savais vaine et perverse.</l>
						<l n="15" num="4.3">Mais l’âme aux menaçants hasards</l>
						<l n="16" num="4.4"><space quantity="18" unit="char"></space>Se berce.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Fermant les yeux, je me livrais</l>
						<l n="18" num="5.2">A sa suavité malsaine,</l>
						<l n="19" num="5.3">Pensant bien que j’en porterais</l>
						<l n="20" num="5.4"><space quantity="18" unit="char"></space>La peine.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="6">
						<l n="21" num="6.1">Mordu, mourant, d’avoir serré</l>
						<l n="22" num="6.2">Sur ma poitrine la panthère,</l>
						<l n="23" num="6.3">J’en veux rester fier, et saurai</l>
						<l n="24" num="6.4"><space quantity="18" unit="char"></space>Me taire.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="7">
						<l n="25" num="7.1">Ce mois d’avril, je veux bannir</l>
						<l n="26" num="7.2">De mon cœur les rêves moroses.</l>
						<l n="27" num="7.3">Je veux orner son souvenir</l>
						<l n="28" num="7.4"><space quantity="18" unit="char"></space>De roses.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="8">
						<l n="29" num="8.1">Et je reprends la liberté</l>
						<l n="30" num="8.2">D’adorer sa grâce suprême.</l>
						<l n="31" num="8.3">Tel que j’étais je suis resté.</l>
						<l n="32" num="8.4"><space quantity="18" unit="char"></space>Je l’aime.</l>
					</lg>
				</div></body></text></TEI>