<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DRAMES ET FANTAISIES</head><head type="main_subpart">Drame en trois ballades</head><div type="poem" key="CRO57">
						<head type="number">II</head>
						<lg n="1">
							<l n="1" num="1.1">Nous nous sommes assis au bois</l>
							<l n="2" num="1.2">Dans les clairières endormantes.</l>
							<l n="3" num="1.3">Mon esprit naguère aux abois</l>
							<l n="4" num="1.4">Se rassure à l’odeur des menthes.</l>
							<l n="5" num="1.5">Le vent, qui gémissait hier,</l>
							<l n="6" num="1.6">Aujourd’hui rit et me caresse.</l>
							<l n="7" num="1.7">Les oiseaux chantent. Je suis fier,</l>
							<l n="8" num="1.8">Car j’ai retrouvé ma maîtresse.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1">La rue a de joyeuses voix,</l>
							<l n="10" num="2.2">Les ouvrières sous leurs mantes</l>
							<l n="11" num="2.3">Frissonnent, en courant. Je vois</l>
							<l n="12" num="2.4">Les amants joindre les amantes.</l>
							<l n="13" num="2.5">Aux cafés, voilà le gaz clair,</l>
							<l n="14" num="2.6">Lumière vive et charmeresse.</l>
							<l n="15" num="2.7">Il y a du bonheur dans l’air,</l>
							<l n="16" num="2.8">Car j’ai retrouvé ma maîtresse.</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1">Et dans tes bras, sur tes seins froids,</l>
							<l n="18" num="3.2">J’ai des lassitudes charmantes.</l>
							<l n="19" num="3.3">Qu’as-tu fait au loin ? Je te crois,</l>
							<l n="20" num="3.4">Que tu sois vraie ou que tu mentes.</l>
							<l n="21" num="3.5">Tes seins berceurs comme la mer,</l>
							<l n="22" num="3.6">Comme la mer calme et traîtresse,</l>
							<l n="23" num="3.7">M’endorment… Plus de doute amer !</l>
							<l n="24" num="3.8">Car j’ai retrouvé ma maîtresse.</l>
						</lg>
						<lg n="4">
							<head type="form">ENVOI</head>
							<l n="25" num="4.1">A toi, merci ! chemin de fer,</l>
							<l n="26" num="4.2">J’étais seul ; mais un soir d’ivresse,</l>
							<l n="27" num="4.3">Tu m’as tiré de cet enfer,</l>
							<l n="28" num="4.4">Car j’ai retrouvé ma maîtresse.</l>
						</lg>
					</div></body></text></TEI>