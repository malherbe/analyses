<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ARV">
					<name>
						<forename>Félix</forename>
						<surname>ARVERS</surname>
					</name>
					<date from="1806" to="1850">1806-1850</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4560 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ARV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/felixarverspoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Félix Arvers: mes heures perdues. Pièces inédites</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">http://www.archive.org/details/posiesdeflixOOarve </idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Félix Arvers</author>
								<imprint>
									<publisher>H. Floury, éditeur</publisher>
									<date when="1900">1900</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MES HEURES PERDUES</head><div type="poem" key="ARV7">
					<head type="main">Le Commencement de l’Année</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>Écoutez bien : l’heure est sonnée ;</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>La dernière du dernier jour,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Le dernier adieu d’une année</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Qui vient de s’enfuir sans retour !</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>Encore une étoile pâlie ;</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Encore une page remplie</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space>Du livre immuable du Temps !</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>Encore un pas fait vers la tombe,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>Encore une feuille qui tombe</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>De la couronne de nos ans !</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">Et toi qui viens à nous, jeune vierge voilée,</l>
						<l n="12" num="2.2">Dis-nous, dois-tu passer joyeuse ou désolée ?</l>
						<l n="13" num="2.3">Apprends-nous les secrets enfermés dans ta main :</l>
						<l n="14" num="2.4">Quels dons apportes-tu dans les plis de ta robe,</l>
						<l n="15" num="2.5">Vierge ; et qui nous dira le mot que nous dérobe,</l>
						<l n="16" num="2.6"><space unit="char" quantity="8"></space>Le grand mystère de demain ?</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><space unit="char" quantity="8"></space>Dois-tu, comme la bien-aimée</l>
						<l n="18" num="3.2"><space unit="char" quantity="8"></space>Au souffle du vent matinal,</l>
						<l n="19" num="3.3"><space unit="char" quantity="8"></space>Passer rieuse et parfumée</l>
						<l n="20" num="3.4"><space unit="char" quantity="8"></space>Des senteurs du lit virginal ?</l>
						<l n="21" num="3.5"><space unit="char" quantity="8"></space>Dois-tu nous apparaître amère</l>
						<l n="22" num="3.6"><space unit="char" quantity="8"></space>Comme la douleur d’une mère</l>
						<l n="23" num="3.7"><space unit="char" quantity="8"></space>Au tombeau de ses enfans morts.</l>
						<l n="24" num="3.8"><space unit="char" quantity="8"></space>Ou, comme un lamentable drame,</l>
						<l n="25" num="3.9"><space unit="char" quantity="8"></space>Laisser pour adieu dans notre âme</l>
						<l n="26" num="3.10"><space unit="char" quantity="8"></space>Le désespoir et le remords ?</l>
					</lg>
					<lg n="4">
						<l n="27" num="4.1">Mais qu’importe, mon Dieu, ce que ta main enserre</l>
						<l n="28" num="4.2">De pluie ou de soleil, de joie ou de misère !</l>
						<l n="29" num="4.3">Pourquoi tenter si loin le muet avenir ?</l>
						<l n="30" num="4.4">Combien, dans cette foule à la mort destinée.</l>
						<l n="31" num="4.5">Qui voyant aujourd’hui commencer cette année.</l>
						<l n="32" num="4.6"><space unit="char" quantity="8"></space>Ne doivent pas la voir finir !</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><space unit="char" quantity="8"></space>Moi-même, qui fais le prophète.</l>
						<l n="34" num="5.2"><space unit="char" quantity="8"></space>Que sais-je, hélas ! si ce flambeau</l>
						<l n="35" num="5.3"><space unit="char" quantity="8"></space>Qui m’éclaire dans une fête</l>
						<l n="36" num="5.4"><space unit="char" quantity="8"></space>Ne luira pas sur mon tombeau ?</l>
						<l n="37" num="5.5"><space unit="char" quantity="8"></space>Peut-être une main redoutable</l>
						<l n="38" num="5.6"><space unit="char" quantity="8"></space>M’entraînera hors de la table</l>
						<l n="39" num="5.7"><space unit="char" quantity="8"></space>Avant le signal de la fin.</l>
						<l n="40" num="5.8"><space unit="char" quantity="8"></space>Comme une marâtre inhumaine</l>
						<l n="41" num="5.9"><space unit="char" quantity="8"></space>Qui guette un enfant, et l’emmène</l>
						<l n="42" num="5.10"><space unit="char" quantity="8"></space>Sans qu’il ait assouvi sa faim.</l>
					</lg>
					<lg n="6">
						<l n="43" num="6.1">Et l’homme cependant, si pauvre et si fragile.</l>
						<l n="44" num="6.2">Passager d’un moment dans sa maison d’argile,</l>
						<l n="45" num="6.3">Misérable bateau sur l’Océan jeté,</l>
						<l n="46" num="6.4">Dans cet amas confus de rumeurs incertaines,</l>
						<l n="47" num="6.5">Sent au fond de son cœur comme des voix lointaines</l>
						<l n="48" num="6.6"><space unit="char" quantity="8"></space>Qui lui parlent d’éternité.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1"><space unit="char" quantity="8"></space>Et quoiqu’un terrible mystère</l>
						<l n="50" num="7.2"><space unit="char" quantity="8"></space>Lui laisse ignorer pour toujours</l>
						<l n="51" num="7.3"><space unit="char" quantity="8"></space>Si sa part d’avenir sur terre</l>
						<l n="52" num="7.4"><space unit="char" quantity="8"></space>Se compte par ans ou par jours,</l>
						<l n="53" num="7.5"><space unit="char" quantity="8"></space>Il croit, dans sa pensée altière.</l>
						<l n="54" num="7.6"><space unit="char" quantity="8"></space>Que pour jamais à la matière</l>
						<l n="55" num="7.7"><space unit="char" quantity="8"></space>Ce rayon de l’âme est uni :</l>
						<l n="56" num="7.8"><space unit="char" quantity="8"></space>Il cherche un but insaisissable :</l>
						<l n="57" num="7.9"><space unit="char" quantity="8"></space>Pour le rocher prenant le sable.</l>
						<l n="58" num="7.10"><space unit="char" quantity="8"></space>Et l’inconnu pour l’infini.</l>
					</lg>
					<lg n="8">
						<l n="59" num="8.1">Mais regarde en arrière, et compte tes années,</l>
						<l n="60" num="8.2">Si promptes à fleurir et si vite fanées :</l>
						<l n="61" num="8.3">Celles-là ne devaient non plus jamais finir :</l>
						<l n="62" num="8.4">Qu’à des rêves moins longs ton âme s’abandonne,</l>
						<l n="63" num="8.5">Imprudent ! et du moins que le passé te donne</l>
						<l n="64" num="8.6"><space unit="char" quantity="8"></space>La mesure de l’avenir.</l>
					</lg>
					<lg n="9">
						<l n="65" num="9.1"><space unit="char" quantity="8"></space>Toutefois de l’an qui commence</l>
						<l n="66" num="9.2"><space unit="char" quantity="8"></space>Saluons la nativité,</l>
						<l n="67" num="9.3"><space unit="char" quantity="8"></space>Cet anneau de la chaîne immense</l>
						<l n="68" num="9.4"><space unit="char" quantity="8"></space>Qui se perd dans l’éternité ;</l>
						<l n="69" num="9.5"><space unit="char" quantity="8"></space>Et s’il est vrai que cette année</l>
						<l n="70" num="9.6"><space unit="char" quantity="8"></space>Par grâce encor nous soit donnée,</l>
						<l n="71" num="9.7"><space unit="char" quantity="8"></space>N’usons pas nos derniers instans</l>
						<l n="72" num="9.8"><space unit="char" quantity="8"></space>A chercher si de son visage</l>
						<l n="73" num="9.9"><space unit="char" quantity="8"></space>Ce voile épais est le présage</l>
						<l n="74" num="9.10"><space unit="char" quantity="8"></space>De la tempête ou du beau temps.</l>
					</lg>
					<lg n="10">
						<l n="75" num="10.1">Et vous tous, mes amis, vous qui sur cette terre</l>
						<l n="76" num="10.2">Semez d’ombre et de fleurs mon sentier solitaire,</l>
						<l n="77" num="10.3">Des biens que je n’ai pas puisse Dieu vous doter ;</l>
						<l n="78" num="10.4">Sitôt que la clarté doive m’être ravie,</l>
						<l n="79" num="10.5">Puisse-t-il ajouter aux jours de votre vie</l>
						<l n="80" num="10.6"><space unit="char" quantity="8"></space>Ceux qu’il lui plaira de m’ôter !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1833">1<hi rend="sup">er</hi> Janvier 1833</date>.
							</dateline>
					</closer>
				</div></body></text></TEI>