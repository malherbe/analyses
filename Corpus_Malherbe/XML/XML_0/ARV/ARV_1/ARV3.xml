<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ARV">
					<name>
						<forename>Félix</forename>
						<surname>ARVERS</surname>
					</name>
					<date from="1806" to="1850">1806-1850</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4560 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ARV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/felixarverspoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Félix Arvers: mes heures perdues. Pièces inédites</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">http://www.archive.org/details/posiesdeflixOOarve </idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Félix Arvers</author>
								<imprint>
									<publisher>H. Floury, éditeur</publisher>
									<date when="1900">1900</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MES HEURES PERDUES</head><div type="poem" key="ARV3">
					<head type="main">A M. Victor Hugo</head>
					<lg n="1">
						<l n="1" num="1.1">D’illusions fantastiques</l>
						<l n="2" num="1.2">Quel doux esprit t’a bercé ?</l>
						<l n="3" num="1.3">Qui t’a dit ces airs antiques,</l>
						<l n="4" num="1.4">Ces contes du temps passé ?</l>
						<l n="5" num="1.5">Que j’aime quand tu nous chantes</l>
						<l n="6" num="1.6">Ces complaintes si touchantes,</l>
						<l n="7" num="1.7">Ces cantiques de la foi,</l>
						<l n="8" num="1.8">Que m’avait chantés mon père,</l>
						<l n="9" num="1.9">Et que chanteront, j’espère,</l>
						<l n="10" num="1.10">Ceux qui viendront après moi.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">Quand le soir, à la chaumière,</l>
						<l n="12" num="2.2">La lampe unit tristement</l>
						<l n="13" num="2.3">La pâleur de sa lumière</l>
						<l n="14" num="2.4">Au vif éclat du sarment,</l>
						<l n="15" num="2.5">Assis dans le coin de l’âtre,</l>
						<l n="16" num="2.6">Sans doute tu vis le pâtre</l>
						<l n="17" num="2.7">Rappeler des anciens jours,</l>
						<l n="18" num="2.8">Récits d’amour, de constance.</l>
						<l n="19" num="2.9">Et redire à l’assistance</l>
						<l n="20" num="2.10">Ces airs qu’on retient toujours.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Il a de vieilles ballades,</l>
						<l n="22" num="3.2">Il a de joyeux refrains :</l>
						<l n="23" num="3.3">Et pour les brebis malades</l>
						<l n="24" num="3.4">Des remèdes souverains :</l>
						<l n="25" num="3.5">Il connaît les noirs présages :</l>
						<l n="26" num="3.6">Perçant le voile des âges</l>
						<l n="27" num="3.7">Son œil lit dans l’avenir,</l>
						<l n="28" num="3.8">Il donne des amulettes,</l>
						<l n="29" num="3.9">Et prédit aux bachelettes</l>
						<l n="30" num="3.10">Quand l’amour doit leur venir.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">Il ta montré la relique</l>
						<l n="32" num="4.2">Et la croix qu’un pénitent</l>
						<l n="33" num="4.3">A la sainte basilique</l>
						<l n="34" num="4.4">A fait bénir en partant.</l>
						<l n="35" num="4.5">Il t’a dit les eaux fangeuses</l>
						<l n="36" num="4.6">Où dans les nuits orageuses</l>
						<l n="37" num="4.7">Errent de pâles lueurs,</l>
						<l n="38" num="4.8">Puis sur l’autel de la Vierge</l>
						<l n="39" num="4.9">Il t’a fait brûler un cierge</l>
						<l n="40" num="4.10">A la mère des douleurs.</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1">Il a deviné ta peine,</l>
						<l n="42" num="5.2">Il t’a conseillé parfois</l>
						<l n="43" num="5.3">D’aller faire une neuvaine</l>
						<l n="44" num="5.4">A Notre-Dame-des-Bois ;</l>
						<l n="45" num="5.5">De partir pour la Galice ;</l>
						<l n="46" num="5.6">Ou, vêtu du noir cilice</l>
						<l n="47" num="5.7">D’aller, pieux voyageur,</l>
						<l n="48" num="5.8">Déposer ton humble hommage</l>
						<l n="49" num="5.9">Au pied de la vieille image</l>
						<l n="50" num="5.10">De Saint Jacques-le-Majeur.</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1">Dans une chapelle basse,</l>
						<l n="52" num="6.2">Devers la Saint-Jean d’été,</l>
						<l n="53" num="6.3">Il t’a fait baiser la châsse</l>
						<l n="54" num="6.4">Dont l’antique sainteté</l>
						<l n="55" num="6.5">Donne à la foi populaire</l>
						<l n="56" num="6.6">Le précieux scapulaire</l>
						<l n="57" num="6.7">Qui du malin nous défend,</l>
						<l n="58" num="6.8">Et sans travail, ni souffrance,</l>
						<l n="59" num="6.9">Abrège la délivrance</l>
						<l n="60" num="6.10">Des femmes en mal d’enfant.</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1">Il t’a fait dans les bruyères</l>
						<l n="62" num="7.2">Voir, de loin, les lieux maudits</l>
						<l n="63" num="7.3">Où l’on dit que les sorcières</l>
						<l n="64" num="7.4">S’assemblent les samedis ;</l>
						<l n="65" num="7.5">Où pour d’impurs sortilèges</l>
						<l n="66" num="7.6">A leurs festins sacrilèges</l>
						<l n="67" num="7.7">S’asseoit l’archange déchu ;</l>
						<l n="68" num="7.8">Où le voyageur qui passe</l>
						<l n="69" num="7.9">S’enfuit en voyant la trace</l>
						<l n="70" num="7.10">Qu’y grava son pied fourchu.</l>
					</lg>
					<lg n="8">
						<l n="71" num="8.1">Mais à l’angle de deux routes</l>
						<l n="72" num="8.2">Il te recommande à Dieu :</l>
						<l n="73" num="8.3">Il part ; et toi tu l’écoutes</l>
						<l n="74" num="8.4">Après qu’il t’a dit adieu.</l>
						<l n="75" num="8.5">Puis tu reviens et nous chantes</l>
						<l n="76" num="8.6">Ces complaintes si touchantes,</l>
						<l n="77" num="8.7">Ces cantiques de la foi</l>
						<l n="78" num="8.8">Que m’avait chantés mon père,</l>
						<l n="79" num="8.9">Et que chanteront, j’espère.</l>
						<l n="80" num="8.10">Ceux qui viendront après moi.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1828">Janvier 1828</date>.
							</dateline>
					</closer>
				</div></body></text></TEI>