<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ARV">
					<name>
						<forename>Félix</forename>
						<surname>ARVERS</surname>
					</name>
					<date from="1806" to="1850">1806-1850</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4560 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ARV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/felixarverspoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Félix Arvers: mes heures perdues. Pièces inédites</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">http://www.archive.org/details/posiesdeflixOOarve </idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Félix Arvers</author>
								<imprint>
									<publisher>H. Floury, éditeur</publisher>
									<date when="1900">1900</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PIÈCES INÉDITES</head><div type="poem" key="ARV19">
					<head type="main">La Femme adultère</head>
					<lg n="1">
						<l n="1" num="1.1">Écoutez ce que c’est que la femme adultère.</l>
						<l n="2" num="1.2">Sa joie est un tourment, sa douleur un mystère :</l>
						<l n="3" num="1.3">Dans son cœur dégradé que le crime avilit</l>
						<l n="4" num="1.4">Un autre a pris la place à l’époux réservée ;</l>
						<l n="5" num="1.5">D’impures voluptés elle s’est abreuvée ;</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Un autre est venu dans son lit.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Dévorée au dedans d’une flamme cachée,</l>
						<l n="8" num="2.2">Toujours, devant les yeux son image attachée</l>
						<l n="9" num="2.3">Jusqu’aux bras d’un époux vient encor la troubler ;</l>
						<l n="10" num="2.4">Elle reste au logis des heures à l’attendre.</l>
						<l n="11" num="2.5">Prête l’oreille et dit, quand elle croit l’entendre,</l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space>A ses enfans de s’en aller.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Son complice ! des lois il brave la vengeance !</l>
						<l n="14" num="3.2">Qui pourrait, trahissant leur sourde intelligence,</l>
						<l n="15" num="3.3">Éveiller dans les cœurs le soupçon endormi ?</l>
						<l n="16" num="3.4">De son crime impuni le succès l’encourage,</l>
						<l n="17" num="3.5">La mère lui sourit, et l’époux qu’il outrage</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space>L’embrasse en disant : mon ami.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Voici venir enfin l’heure tant retardée ;</l>
						<l n="20" num="4.2">Les voilà seuls, la porte est close et bien gardée :</l>
						<l n="21" num="4.3">Pourquoi cet air pensif, pourquoi cet œil distrait ?</l>
						<l n="22" num="4.4">Pourquoi toujours trembler et pâlir d’épouvante ?</l>
						<l n="23" num="4.5">Personne ne l’a vu monter, et la suivante</l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space>A reçu le prix du secret.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Dans un festin brillant le hasard les rassemble ;</l>
						<l n="26" num="5.2">Leurs sièges sont voisins. Que vont-ils dire ensemble ?</l>
						<l n="27" num="5.3">Quel sinistre bonheur dans leurs regards a lui !</l>
						<l n="28" num="5.4">Oh retiens les éclairs de ta prunelle ardente,</l>
						<l n="29" num="5.5">Garde de te trahir, et de boire, imprudente !</l>
						<l n="30" num="5.6"><space unit="char" quantity="8"></space>Dans la même coupe après lui !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Que dis-je ? Du mépris et de l’indifférence</l>
						<l n="32" num="6.2">Elle sait à son œil imposer l’apparence :</l>
						<l n="33" num="6.3">Un regard indiscret jamais ne révéla</l>
						<l n="34" num="6.4">De son cœur déchiré la sombre inquiétude.</l>
						<l n="35" num="6.5">Elle s’observe, et sait, à force d’habitude,</l>
						<l n="36" num="6.6"><space unit="char" quantity="8"></space>Rester froide quand il est là !</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Ses tourmens sont cachés à tous, soyez sans crainte ;</l>
						<l n="38" num="7.2">Aussi regardez-la sans gêne et sans contrainte</l>
						<l n="39" num="7.3">Répondre à vingt propos, sourire… oh si du moins,</l>
						<l n="40" num="7.4">Pour apaiser l’ardeur dont elle est embrasée,</l>
						<l n="41" num="7.5">Elle pouvait, auprès d’une obscure croisée,</l>
						<l n="42" num="7.6"><space unit="char" quantity="8"></space>L’avoir un instant sans témoins !</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Sentir le bruit léger de sa robe froissée,</l>
						<l n="44" num="8.2">Dans les plis de satin sa jambe entrelacée,</l>
						<l n="45" num="8.3">Lui donner d’un regard l’heure du lendemain,</l>
						<l n="46" num="8.4">Et, dans ce tourbillon qui roule et qui l’emporte.</l>
						<l n="47" num="8.5">Lui dire… ou seulement debout, près de la porte,</l>
						<l n="48" num="8.6"><space unit="char" quantity="8"></space>En passant lui serrer la main !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Cependant, pas à pas, la vieillesse est venue</l>
						<l n="50" num="9.2">Troubler son cœur flétri d’une crainte inconnue.</l>
						<l n="51" num="9.3">Le prestige enivrant s’est enfin dissipé :</l>
						<l n="52" num="9.4">Il faut quitter l’amour, l’amour et son ivresse ;</l>
						<l n="53" num="9.5">Il faut se trouver seule et subir la tendresse</l>
						<l n="54" num="9.6"><space unit="char" quantity="8"></space>De cet homme qu’elle a trompé.</l>
					</lg>
				</div></body></text></TEI>