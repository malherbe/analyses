<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ARV">
					<name>
						<forename>Félix</forename>
						<surname>ARVERS</surname>
					</name>
					<date from="1806" to="1850">1806-1850</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4560 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ARV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/felixarverspoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Félix Arvers: mes heures perdues. Pièces inédites</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">http://www.archive.org/details/posiesdeflixOOarve </idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Félix Arvers</author>
								<imprint>
									<publisher>H. Floury, éditeur</publisher>
									<date when="1900">1900</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MES HEURES PERDUES</head><div type="poem" key="ARV4">
					<head type="main">La Première Passion</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">« Minuit ! ma mère dort : je me suis relevée :</l>
							<l n="2" num="1.2">Je craignais de laisser ma lettre inachevée ;</l>
							<l n="3" num="1.3">J’ai voulu me hâter, car peut-être ma main</l>
							<l n="4" num="1.4">Ne sera-t-elle plus assez forte demain !</l>
							<l n="5" num="1.5">Tu connais mon malheur ; je t’ai dit que mon père</l>
							<l n="6" num="1.6">A voulu me dicter un choix, et qu’il espère</l>
							<l n="7" num="1.7">Sans doute me trouver trop faible pour oser</l>
							<l n="8" num="1.8">Refuser cet époux qu’il prétend m’imposer.</l>
							<l n="9" num="1.9">O toi qui m’appartiens ! ô toi qui me fis naître</l>
							<l n="10" num="1.10">Au bonheur, à l’amour que tu m’as fait connaître ;</l>
							<l n="11" num="1.11">Toi qui sus le premier deviner le secret</l>
							<l n="12" num="1.12">Et trouver le chemin d’un cœur qui s’ignorait,</l>
							<l n="13" num="1.13">Crois-tu qu’à d’autres lois ton amante enchaînée</l>
							<l n="14" num="1.14">Méconnaisse jamais la foi qu’elle a donnée ;</l>
							<l n="15" num="1.15">Qu’elle puisse oublier ces rapides momens</l>
							<l n="16" num="1.16">Où nos voix ont ensemble échangé leurs sermens,</l>
							<l n="17" num="1.17">Où sa tremblante main a frémi dans la tienne,</l>
							<l n="18" num="1.18">Et qu’à d’autre qu’à toi jamais elle appartienne ?</l>
							<l n="19" num="1.19">Tu veux fuir, m’as-tu dit : fuis ; mais n’espère pas</l>
							<l n="20" num="1.20">M’empêcher de te suivre attachée à tes pas !</l>
							<l n="21" num="1.21">Qu’importe où nous soyons si nous sommes ensemble ;</l>
							<l n="22" num="1.22">Est-il donc un désert si triste, qui ne semble</l>
							<l n="23" num="1.23">Plus riant qu’un palais, quand il est animé</l>
							<l n="24" num="1.24">Par l’aspect du bonheur et de l’objet aimé ?</l>
							<l n="25" num="1.25">Et que me font à moi tous ces biens qui m’attendent ?</l>
							<l n="26" num="1.26">Lorsqu’on s’est dit : je t’aime ! et que les cœurs s’entendent,</l>
							<l n="27" num="1.27">Que sont tous les trésors, qu’est l’univers pour eux.</l>
							<l n="28" num="1.28">Et que demandent-ils de plus pour être heureux ?</l>
							<l n="29" num="1.29">Mais comment fuir ? comment tromper la vigilance</l>
							<l n="30" num="1.30">D’un père soupçonneux qui m’épie en silence ?</l>
							<l n="31" num="1.31">Je m’abusais ! Eh bien, écoute le serment</l>
							<l n="32" num="1.32">Que te jure ma bouche en cet affreux moment :</l>
							<l n="33" num="1.33">Puisqu’on l’a résolu, puisqu’on me sacrifie.</l>
							<l n="34" num="1.34">Puisqu’on veut mon malheur, eh bien ! je les défie :</l>
							<l n="35" num="1.35">Ils ne m’auront que morte, et je n’aurai laissé</l>
							<l n="36" num="1.36">Pour traîner à l’autel qu’un cadavre glacé ! »</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="37" num="1.1">Lorsque je l’ai revue, elle était mariée</l>
							<l n="38" num="1.2">Depuis cinq ans passés : « Ah ! s’est-elle écriée,</l>
							<l n="39" num="1.3">C’est vous ! bien vous a pris d’être venu nous voir :</l>
							<l n="40" num="1.4">Mais où donc étiez-vous ? Et ne peut-on savoir</l>
							<l n="41" num="1.5">Pourquoi, depuis un siècle, éloigné de la France,</l>
							<l n="42" num="1.6">Vous nous avez ainsi laissés dans l’ignorance ?</l>
							<l n="43" num="1.7">Quant à nous, tout va bien : le sort nous a souri.</l>
							<l n="44" num="1.8">— J’ai parlé bien souvent de vous à mon mari ;</l>
							<l n="45" num="1.9">C’est un homme d’honneur, que j’aime et je révère,</l>
							<l n="46" num="1.10">Sage négociant, de probité sévère,</l>
							<l n="47" num="1.11">Qui par son zèle actif chaque jour agrandit</l>
							<l n="48" num="1.12">L’essor de son commerce, et double son crédit :</l>
							<l n="49" num="1.13">Et puisque le hasard à la fin nous rassemble ;</l>
							<l n="50" num="1.14">Je vous présenterai, vous causerez ensemble ;</l>
							<l n="51" num="1.15">Il vous recevra bien, empressé de saisir</l>
							<l n="52" num="1.16">Pareille occasion de me faire plaisir.</l>
							<l n="53" num="1.17">Vous verrez mes enfans : j’en ai trois. Mon aînée</l>
							<l n="54" num="1.18">Est chez mes belles-sœurs, qui me l’ont emmenée ;</l>
							<l n="55" num="1.19">Je l’attends samedi matin : vous la verrez.</l>
							<l n="56" num="1.20">Oh, c’est qu’elle est charmante ! ensuite, vous saurez</l>
							<l n="57" num="1.21">Qu’elle lit couramment, écrit même, et commence</l>
							<l n="58" num="1.22">A jouer la sonate et chanter la romance.</l>
							<l n="59" num="1.23">Et mon fils ! il aura ses trois ans et demi</l>
							<l n="60" num="1.24">Le vingt du mois prochain ; du reste, mon ami,</l>
							<l n="61" num="1.25">Vous verrez comme il est grand et fort pour son âge ;</l>
							<l n="62" num="1.26">C’est le plus bel enfant de tout le voisinage.</l>
							<l n="63" num="1.27">Et puis, j’ai mon petit. — Je ne l’ai pas nourri :</l>
							<l n="64" num="1.28">Mes couches ont été pénibles ; mon mari,</l>
							<l n="65" num="1.29">Qui craignait pour mon lait, a voulu que je prisse</l>
							<l n="66" num="1.30">Sur moi de le laisser aux mains d’une nourrice.</l>
							<l n="67" num="1.31">Mais de cet embarras je vais me délivrer,</l>
							<l n="68" num="1.32">Et le docteur a dit qu’on pouvait le sevrer.</l>
							<l n="69" num="1.33">— Ainsi dans mes enfans, dans un époux qui m’aime,</l>
							<l n="70" num="1.34">J’ai trouvé le bonheur domestique ; et vous même,</l>
							<l n="71" num="1.35">Vous dépendez de vous, j’imagine, et partant</l>
							<l n="72" num="1.36">Qui peut vous empêcher d’en faire un jour autant ?</l>
							<l n="73" num="1.37">Je sais qu’en pareil cas le choix est difficile.</l>
							<l n="74" num="1.38">Que vous avez parfois une humeur indocile ;</l>
							<l n="75" num="1.39">Mais on peut réussir, et vous réussirez :</l>
							<l n="76" num="1.40">Vous prendrez une femme, et nous l’amènerez,</l>
							<l n="77" num="1.41">Elle viendra passer l’été dans notre terre :</l>
							<l n="78" num="1.42">Jusque-là toutefois, libre et célibataire,</l>
							<l n="79" num="1.43">Pensez à vos amis, et venez en garçon</l>
							<l n="80" num="1.44">Nous demander dimanche à dîner sans façon. »</l>
						</lg>
					</div>
				</div></body></text></TEI>