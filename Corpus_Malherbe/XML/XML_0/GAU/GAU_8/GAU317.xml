<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU317">
				<head type="main">A L’IMPÉRATRICE</head>
				<div type="section" n="1">
					<lg n="1">
						<l n="1" num="1.1">Suave et pur jasmin d’Espagne</l>
						<l n="2" num="1.2">Où se posa l’abeille d’or,</l>
						<l n="3" num="1.3">Une grâce vous accompagne</l>
						<l n="4" num="1.4">Et vous possédez un trésor ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Vous, le sourire de la force,</l>
						<l n="6" num="2.2">Le charme de la majesté,</l>
						<l n="7" num="2.3">Vous avez la puissante amorce</l>
						<l n="8" num="2.4">Qui prend les âmes — la bonté !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Et, derrière l’Impératrice</l>
						<l n="10" num="3.2">A la couronne de rayons,</l>
						<l n="11" num="3.3">Apparaît la consolatrice</l>
						<l n="12" num="3.4">De toutes les afflictions.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Sans que votre cœur ne l’entende</l>
						<l n="14" num="4.2">Il ne saurait tomber un pleur ;</l>
						<l n="15" num="4.3">Quelle est la main qui ne se tende</l>
						<l n="16" num="4.4">Vers vous, du fond de son malheur ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Pensive, auguste et maternelle,</l>
						<l n="18" num="5.2">Tenant compte des maux soufferts,</l>
						<l n="19" num="5.3">Vous rafraîchissez de votre aile</l>
						<l n="20" num="5.4">Les feux mérités des enfers.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Ce n’est pas seulement vers l’ombre</l>
						<l n="22" num="6.2">Que va le regard de vos yeux,</l>
						<l n="23" num="6.3">Dans la cellule étroite et sombre</l>
						<l n="24" num="6.4">Faisant briller l’azur des cieux ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Ce regard que chacun implore,</l>
						<l n="26" num="7.2">Qui luit sur tous comme un flambeau,</l>
						<l n="27" num="7.3">S’arrête, plus touchant encore,</l>
						<l n="28" num="7.4">Quand il a rencontré le Beau.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">L’enthousiasme y met sa flamme</l>
						<l n="30" num="8.2">Sans en altérer la douceur ;</l>
						<l n="31" num="8.3">Si le génie est une femme,</l>
						<l n="32" num="8.4">Vous lui dites : «Venez, ma sœur,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">«Je mettrai sur vous cette gloire</l>
						<l n="34" num="9.2">Qui fait les hommes radieux,</l>
						<l n="35" num="9.3">Ce ruban teint par la victoire,</l>
						<l n="36" num="9.4">Pourpre humaine digne des dieux !»</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Et votre main d’où tout ruisselle</l>
						<l n="38" num="10.2">Sur le sein de Rosa Bonheur</l>
						<l n="39" num="10.3">Allumant la rouge étincelle,</l>
						<l n="40" num="10.4">Fait jaillir l’astre de l’Honneur !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="41" num="1.1">Oh ! quelle joie au séjour morne</l>
						<l n="42" num="1.2">Des pauvres Enfants détenus,</l>
						<l n="43" num="1.3">Limbes grises, tombeau que borne</l>
						<l n="44" num="1.4">Un horizon de grands murs nus.</l>
					</lg>
					<lg n="2">
						<l n="45" num="2.1">Lorsque la porte qui s’entr’ouvre,</l>
						<l n="46" num="2.2">Laissant passer le jour vermeil,</l>
						<l n="47" num="2.3">A leurs yeux ravis vous découvre</l>
						<l n="48" num="2.4">Comme un ange dans le soleil !</l>
					</lg>
					<lg n="3">
						<l n="49" num="3.1">Pour le penseur chose effrayante !</l>
						<l n="50" num="3.2">L’homme jetant à la prison</l>
						<l n="51" num="3.3">La faute encore inconsciente</l>
						<l n="52" num="3.4">Et le crime avant la raison !</l>
					</lg>
					<lg n="4">
						<l n="53" num="4.1">Là sont des Cartouches en herbe</l>
						<l n="54" num="4.2">Dont les dents de lait ont mordu,</l>
						<l n="55" num="4.3">Comme un gâteau, le fruit acerbe</l>
						<l n="56" num="4.4">Qui pend à l’arbre défendu ;</l>
					</lg>
					<lg n="5">
						<l n="57" num="5.1">Des scélérats sevrés à peine ;</l>
						<l n="58" num="5.2">De petits bandits de douze ans,</l>
						<l n="59" num="5.3">D’un mauvais sol mauvaise graine,</l>
						<l n="60" num="5.4">Tous coupables mais innocents !</l>
					</lg>
					<lg n="6">
						<l n="61" num="6.1">Hélas ! pour beaucoup la famille</l>
						<l n="62" num="6.2">Fut le repaire et non le nid,</l>
						<l n="63" num="6.3">La caverne où gronde et fourmille</l>
						<l n="64" num="6.4">Le monde fauve qu’on bannit.</l>
					</lg>
					<lg n="7">
						<l n="65" num="7.1">Vous arrivez là, douce femme,</l>
						<l n="66" num="7.2">Lorsque sommeille encor Paris,</l>
						<l n="67" num="7.3">Faisant l’aumône de votre âme</l>
						<l n="68" num="7.4">A ces pauvres enfants surpris.</l>
					</lg>
					<lg n="8">
						<l n="69" num="8.1">Vous accueillez leur plainte amère</l>
						<l n="70" num="8.2">Leur long désir de liberté,</l>
						<l n="71" num="8.3">Et chacun d’eux vous croit sa mère</l>
						<l n="72" num="8.4">A se voir si bien écouté.</l>
					</lg>
					<lg n="9">
						<l n="73" num="9.1">Vous leur parlez de Dieu, de l’homme,</l>
						<l n="74" num="9.2">Du saint travail et du devoir,</l>
						<l n="75" num="9.3">Des grands exemples qu’on renomme,</l>
						<l n="76" num="9.4">Du repentir qui suit l’espoir ;</l>
					</lg>
					<lg n="10">
						<l n="77" num="10.1">Et la prison tout éblouie</l>
						<l n="78" num="10.2">Par la céleste vision,</l>
						<l n="79" num="10.3">De la lumière évanouie</l>
						<l n="80" num="10.4">Conserve longtemps un rayon !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="81" num="1.1">Il est d’autres cités dolentes</l>
						<l n="82" num="1.2">Que d’autres Dante décriront ;</l>
						<l n="83" num="1.3">Les heures s’y traînent bien lentes,</l>
						<l n="84" num="1.4">La faute a la rougeur au front.</l>
					</lg>
					<lg n="2">
						<l n="85" num="2.1">Là gémissent les vierges folles</l>
						<l n="86" num="2.2">Qui vont sans lampe dans la nuit ;</l>
						<l n="87" num="2.3">Les paresseuses aux mains molles</l>
						<l n="88" num="2.4">Que l’éclat d’un bijou séduit ;</l>
					</lg>
					<lg n="3">
						<l n="89" num="3.1">La coupable, presque novice,</l>
						<l n="90" num="3.2">Trébuchée au chemin glissant,</l>
						<l n="91" num="3.3">Et toutes celles que le vice</l>
						<l n="92" num="3.4">Sur son char emporte en passant.</l>
					</lg>
					<lg n="4">
						<l n="93" num="4.1">Sans craindre pour vos pieds la fange,</l>
						<l n="94" num="4.2">Vous traversez ces lieux maudits,</l>
						<l n="95" num="4.3">Comme en enfer, un bel Archange</l>
						<l n="96" num="4.4">Qui descendrait du Paradis.</l>
					</lg>
					<lg n="5">
						<l n="97" num="5.1">Vous visitez dortoirs, chapelle,</l>
						<l n="98" num="5.2">Et la cellule et l’atelier,</l>
						<l n="99" num="5.3">Allant où chacun vous appelle</l>
						<l n="100" num="5.4">Et ne voulant rien oublier.</l>
					</lg>
					<lg n="6">
						<l n="101" num="6.1">Si, dans la triste infirmerie,</l>
						<l n="102" num="6.2">Au chevet, où râle la mort,</l>
						<l n="103" num="6.3">Vous trouvez une sœur qui prie,</l>
						<l n="104" num="6.4">L’innocence près du remord,</l>
					</lg>
					<lg n="7">
						<l n="105" num="7.1">Vous ployez les genoux, et l’âme,</l>
						<l n="106" num="7.2">Dont l’aile bat pour le départ,</l>
						<l n="107" num="7.3">Croit voir resplendir Notre-Dame</l>
						<l n="108" num="7.4">A travers son vague regard.</l>
					</lg>
					<lg n="8">
						<l n="109" num="8.1">Lorsque se tait la litanie,</l>
						<l n="110" num="8.2">Vous vous penchez pour mieux saisir</l>
						<l n="111" num="8.3">Sur les lèvres de l’agonie</l>
						<l n="112" num="8.4">Le suprême et secret désir.</l>
					</lg>
					<lg n="9">
						<l n="113" num="9.1">La jeune mourante, éperdue,</l>
						<l n="114" num="9.2">Qui ne parlait plus qu’avec Dieu,</l>
						<l n="115" num="9.3">D’une voix à peine entendue</l>
						<l n="116" num="9.4">Confie à votre cœur son vœu.</l>
					</lg>
					<lg n="10">
						<l n="117" num="10.1">Cet humble vœu, dernier caprice,</l>
						<l n="118" num="10.2">Est recueilli pieusement,</l>
						<l n="119" num="10.3">Et de l’enfant l’Impératrice</l>
						<l n="120" num="10.4">Exécute le testament.</l>
					</lg>
				</div>
				<closer>
					<dateline>
						<date when="1866">15 août 1866</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>