<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU355">
				<head type="main">L’ORESTIE</head>
				<head type="sub_1">TRAGÉDIE ANTIQUE</head>
				<head type="sub_1">FRAGMENT</head>
				<div type="section" n="1">
					<head type="main">LYNCÉE (LE VEILLEUR), sur la tour.</head>
					<lg n="1">
						<l n="1" num="1.1">Voici dix ans bientôt que du haut de ma tour</l>
						<l n="2" num="1.2">De la flotte des Grecs je guette le retour</l>
						<l n="3" num="1.3">Attendant, sans espoir, qu’à l’horizon flamboie</l>
						<l n="4" num="1.4">Le signal convenu pour la prise de Troie.</l>
						<l n="5" num="1.5">Hélas ! j’ai beau plonger mes regards dans l’azur,</l>
						<l n="6" num="1.6">Rien ne s’allume au fond de ce lointain obscur.</l>
						<l n="7" num="1.7">Nulle rougeur de feux, nulle blancheur de voiles !</l>
						<l n="8" num="1.8">— C’est ainsi que je vis, seul avec les étoiles,</l>
						<l n="9" num="1.9">Veillant, quand le sommeil a fermé tous les yeux,</l>
						<l n="10" num="1.10">Excepté les yeux d’or qui s’éveillent aux cieux !</l>
						<l n="11" num="1.11">Trempé par la rosée et sans toit qui l’abrite,</l>
						<l n="12" num="1.12">D’aucun songe mon lit ne reçoit la visite,</l>
						<l n="13" num="1.13">Et si parfois dans l’ombre, aux noirs échos des nuits,</l>
						<l n="14" num="1.14">Je jette une chanson pour charmer mes ennuis,</l>
						<l n="15" num="1.15">En pensant aux malheurs de la maison d’Atride</l>
						<l n="16" num="1.16">Je sens dans mon gosier mourir ma voix timide !</l>
						<l n="17" num="1.17">De ce rude labeur délivrez-moi, grands Dieux,</l>
						<l n="18" num="1.18">Et laissez le sommeil s’abattre sur mes yeux !</l>
						<l n="19" num="1.19">Ah ! quel rude métier ! Quelle pénible tâche !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="main">CLYTEMNESTRE, au pied de la tour.</head>
					<lg n="1">
						<l n="20" num="1.1">Qui parle donc là-haut ? — Pauvre chien à l’attache,</l>
						<l n="21" num="1.2">C’est toi ? — Tu peux quitter ton gîte aérien,</l>
						<l n="22" num="1.3">Descends. A l’horizon il ne paraîtra rien,</l>
						<l n="23" num="1.4">Car souillée au départ du sang d’Iphigénie,</l>
						<l n="24" num="1.5">La flotte par les dieux ne peut être bénie ;</l>
						<l n="25" num="1.6">Les Grecs sont morts, ou bien égarés sur les mers,</l>
						<l n="26" num="1.7">De leurs débris errants, ils sèment l’univers !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="main">ÉLECTRE</head>
					<lg n="1">
						<l n="27" num="1.1">Ah ! par pitié pour moi, ne descends pas, Lyncée !</l>
						<l n="28" num="1.2">Le feu peut luire encor, l’heure n’est point passée !</l>
						<ab type="dot"> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<ab type="dot"> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					</lg>
				</div>
			</div></body></text></TEI>