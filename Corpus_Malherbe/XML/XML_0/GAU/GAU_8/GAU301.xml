<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU301">
				<head type="main">LA NEIGE</head>
				<head type="form">FANTAISIE D’HIVER</head>
				<lg n="1">
					<l n="1" num="1.1">La bruine toujours pleure</l>
					<l n="2" num="1.2">Sur notre sol consterné ;</l>
					<l n="3" num="1.3">Le soleil piteux demeure</l>
					<l n="4" num="1.4">De brouillards enfariné.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">La neige, fourrure blanche,</l>
					<l n="6" num="2.2">Ourle le rebord des toits ;</l>
					<l n="7" num="2.3">Elle poudre chaque branche</l>
					<l n="8" num="2.4">De la perruque des bois.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Sous son linceul, elle enferme</l>
					<l n="10" num="3.2">Les plus lointains horizons ;</l>
					<l n="11" num="3.3">A la barbe du dieu Terme</l>
					<l n="12" num="3.4">Elle suspend des glaçons.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Dans ses rêts froids et tenaces,</l>
					<l n="14" num="4.2">Au vol elle abat l’oiseau,</l>
					<l n="15" num="4.3">Et se durcissant en glaces,</l>
					<l n="16" num="4.4">Fige le poisson dans l’eau.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Sur la vitre des mansardes</l>
					<l n="18" num="5.2">Elle étale ses pâleurs,</l>
					<l n="19" num="5.3">Et fait aux lunes blafardes</l>
					<l n="20" num="5.4">Un teint de pâles couleurs.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Des Vénus trop court vêtues</l>
					<l n="22" num="6.2">En cachant la nudité,</l>
					<l n="23" num="6.3">La neige tisse aux statues</l>
					<l n="24" num="6.4">Un voile de chasteté.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Bonne en ces heures maussades,</l>
					<l n="26" num="7.2">En ces mortelles saisons,</l>
					<l n="27" num="7.3">Elle fournit des glissades</l>
					<l n="28" num="7.4">Pour le jeu des polissons !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Elle coiffe la montagne</l>
					<l n="30" num="8.2">D’un cimier fol et changeant,</l>
					<l n="31" num="8.3">Et jette sur la campagne</l>
					<l n="32" num="8.4">Son manteau de vif-argent.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Sous les pieds de la fillette</l>
					<l n="34" num="9.2">Elle étend son blanc tapis,</l>
					<l n="35" num="9.3">Et pour l’amant qui la guette</l>
					<l n="36" num="9.4">Rend ses pas plus assoupis.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Elle attache la pituite</l>
					<l n="38" num="10.2">Au nez transi des bourgeois ;</l>
					<l n="39" num="10.3">Mais au rêveur qui médite</l>
					<l n="40" num="10.4">Elle dit, trouvant la voix :</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">«C’est moi qui suis ta Giselle,</l>
					<l n="42" num="11.2">Ta vaporeuse willi ;</l>
					<l n="43" num="11.3">Je suis jeune, je suis belle,</l>
					<l n="44" num="11.4">J’ai froid ; — ouvre-moi ton lit ?</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Déposant ma houppelande</l>
					<l n="46" num="12.2">Et mes gants en peau de daim,</l>
					<l n="47" num="12.3">Je te dirai la légende</l>
					<l n="48" num="12.4">Du grand paradis d’Odin.»</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Or, un poëte un peu tendre</l>
					<l n="50" num="13.2">Et qui chez lui fait du feu,</l>
					<l n="51" num="13.3">Ne peut jamais faire attendre</l>
					<l n="52" num="13.4">Une fillette à l’œil bleu !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1850"> janvier 1850</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>