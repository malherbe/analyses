<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU348">
				<head type="main">ALLITÉRATIONS</head>
				<head type="form">IMITÉES DE CELLES DU ROMANCERO</head>
				<lg n="1">
					<l rhyme="none" n="1" num="1.1">Monté sur son fidèle barbe</l>
					<l rhyme="none" n="2" num="1.2">Vêtu d’un albornez d’azur,</l>
					<l rhyme="none" n="3" num="1.3">Emblème d’amour et de foi,</l>
					<l rhyme="none" n="4" num="1.4">Le vaillant Grenadin Gazul</l>
					<l rhyme="none" n="5" num="1.5">Passait sur la Vivarambla.</l>
					<l rhyme="none" n="6" num="1.6">Il était si beau que chacun</l>
					<l rhyme="none" n="7" num="1.7">Se retournait en le voyant.</l>
					<l rhyme="none" n="8" num="1.8">A son balcon, Fatmé la brune</l>
					<l rhyme="none" n="9" num="1.9">Prenait le frais avec ses femmes.</l>
					<l rhyme="none" n="10" num="1.10">Le More au milieu de la rue</l>
					<l rhyme="none" n="11" num="1.11">Arrêtant son cheval lancé,</l>
					<l rhyme="none" n="12" num="1.12">Sur ses étriers d’or s’assure,</l>
					<l rhyme="none" n="13" num="1.13">Et, se haussant jusqu’au balcon,</l>
					<l rhyme="none" n="14" num="1.14">Dit : — Toi qui luis comme la lune</l>
					<l rhyme="none" n="15" num="1.15">Au milieu des étoiles d’or,</l>
					<l rhyme="none" n="16" num="1.16">Fatmé, perle de la nature,</l>
					<l rhyme="none" n="17" num="1.17">Fleur du Xenil et de l’Espagne,</l>
					<l rhyme="none" n="18" num="1.18">Réponds à mes feux je t’assure,</l>
					<l rhyme="none" n="19" num="1.19">Par jour, trois têtes de chrétien !</l>
					<l rhyme="none" n="20" num="1.20">— Sur mes genoux, vaillant Gazul,</l>
					<l rhyme="none" n="21" num="1.21">Pose la tienne chaque soir,</l>
					<l rhyme="none" n="22" num="1.22">Et je te promets, sans parjure,</l>
					<l rhyme="none" n="23" num="1.23">De t’adorer jusqu’au matin !</l>
				</lg>
			</div></body></text></TEI>