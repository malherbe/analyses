<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU289">
				<head type="main">ÉPIGRAPHE</head>
				<head type="sub_1">PLACÉE EN TÊTE DE : <hi rend="ital">SOUS LA TABLE</hi></head>
				<head type="sub_2">(Dans <hi rend="ital">les Jeunes-France</hi>)</head>
				<lg n="1">
					<l n="1" num="1.1">Qu’est-ce que la vertu ? Rien, moins que rien, un mot</l>
					<l n="2" num="1.2">A rayer de la langue. Il faudrait être sot</l>
					<l n="3" num="1.3">Comme un provincial débarqué par le coche,</l>
					<l n="4" num="1.4">Pour y croire. Un filou, la main dans votre poche,</l>
					<l n="5" num="1.5">Concourra pour le prix Monthyon. Chaude encor</l>
					<l n="6" num="1.6">D’adultères baisers payés au poids de l’or,</l>
					<l n="7" num="1.7">Votre femme dira : Je suis honnête femme.</l>
					<l n="8" num="1.8">Mentez, pillez, tuez, soyez un homme infâme,</l>
					<l n="9" num="1.9">Ne croyez pas en Dieu, vous serez marguillier ;</l>
					<l n="10" num="1.10">Et, quand vous serez mort, un joyeux héritier,</l>
					<l n="11" num="1.11">Ponctuant chaque mot de larmes ridicules,</l>
					<l n="12" num="1.12">Fera, sur votre tombe, en lettres majuscules</l>
					<l n="13" num="1.13">Écrire : Bon ami, bon père, bon époux,</l>
					<l n="14" num="1.14">Excellent citoyen, et regretté de tous.</l>
					<l n="15" num="1.15">La vertu ! c’était bon quand on était dans l’arche.</l>
					<l n="16" num="1.16">La mode en est passée, et le siècle qui marche</l>
					<l n="17" num="1.17">Laisse au bord du chemin, ainsi que des haillons,</l>
					<l n="18" num="1.18">Toutes les vieilles lois des vieilles nations.</l>
					<l n="19" num="1.19">Donc, sans nous soucier de la morale antique,</l>
					<l n="20" num="1.20">Nous tous, enfants perdus de cet âge critique,</l>
					<l n="21" num="1.21">Au bruit sourd du passé qui s’écroule au néant,</l>
					<l n="22" num="1.22">Dansons gaîment au bord de l’abîme béant,</l>
					<l n="23" num="1.23">Voici le punch qui bout et siffle dans la coupe :</l>
					<l n="24" num="1.24">Que la bande joyeuse autour du bol se groupe !</l>
					<l n="25" num="1.25">En avant les viveurs ! Usons bien nos beaux ans ;</l>
					<l n="26" num="1.26">Faisons les lords Byrons et les petits dons Juans ;</l>
					<l n="27" num="1.27">Fumons notre cigare, embrassons nos maîtresses ;</l>
					<l n="28" num="1.28">Enivrons-nous, amis, de toutes les ivresses,</l>
					<l n="29" num="1.29">Jusqu’à ce que la Mort, cette vieille catin,</l>
					<l n="30" num="1.30">Nous tire par la manche au sortir d’un festin,</l>
					<l n="31" num="1.31">Et, nous amadouant de sa voix douce et fausse,</l>
					<l n="32" num="1.32">Nous fasse aller cuver notre vin dans la fosse.</l>
				</lg>
				<closer>
					(La Farce du Monde, <hi rend="ital">Moralité</hi>.)
				</closer>
			</div></body></text></TEI>