<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU311">
				<head type="main">A ERNEST HÉBERT</head>
				<head type="sub_2">SUR SON TABLEAU</head>
				<head type="sub_2">LE BANC DE PIERRE</head>
				<lg n="1">
					<l n="1" num="1.1">Au fond du parc, dans une ombre indécise,</l>
					<l n="2" num="1.2">Il est un banc, solitaire et moussu,</l>
					<l n="3" num="1.3">Où l’on croit voir la Rêverie assise,</l>
					<l n="4" num="1.4">Triste et songeant à quelque amour déçu.</l>
					<l n="5" num="1.5">Le souvenir dans les arbres murmure,</l>
					<l n="6" num="1.6">Se racontant les bonheurs expiés,</l>
					<l n="7" num="1.7">Et, comme un pleur, de la grêle ramure</l>
					<l n="8" num="1.8"><space quantity="2" unit="char"></space>Une feuille tombe à vos pieds.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Ils venaient là, beau couple qui s’enlace,</l>
					<l n="10" num="2.2">Aux yeux jaloux tous deux se dérobant,</l>
					<l n="11" num="2.3">Et réveillaient, pour s’asseoir à sa place,</l>
					<l n="12" num="2.4">Le clair de lune endormi sur le banc.</l>
					<l n="13" num="2.5">Ce qu’ils disaient, la maîtresse l’oublie ;</l>
					<l n="14" num="2.6">Mais l’amoureux, cœur blessé, s’en souvient,</l>
					<l n="15" num="2.7">Et, dans le bois, avec mélancolie,</l>
					<l n="16" num="2.8"><space quantity="2" unit="char"></space>Au rendez-vous, tout seul, revient.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Pour l’œil qui sait voir les larmes des choses,</l>
					<l n="18" num="3.2">Ce banc désert regrette le passé,</l>
					<l n="19" num="3.3">Les longs baisers et le bouquet de roses,</l>
					<l n="20" num="3.4">Comme un signal à son angle placé.</l>
					<l n="21" num="3.5">Sur lui la branche à l’abandon retombe,</l>
					<l n="22" num="3.6">La mousse est jaune et la fleur sans parfum ;</l>
					<l n="23" num="3.7">La pierre grise a l’aspect de la tombe</l>
					<l n="24" num="3.8"><space quantity="2" unit="char"></space>Qui recouvre l’amour défunt !…</l>
				</lg>
				<closer>
					<dateline>
						<date when="1865">1865</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>