<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU3">
				<head type="main">LE POÈME DE LA FEMME</head>
				<head type="sub">marbre de paros</head>
				<lg n="1">
					<l n="1" num="1.1">Un jour, au doux rêveur qui l’aime,</l>
					<l n="2" num="1.2">En train de montrer ses trésors,</l>
					<l n="3" num="1.3">Elle voulut lire un poème,</l>
					<l n="4" num="1.4">Le poème de son beau corps.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">D’abord, superbe et triomphante</l>
					<l n="6" num="2.2">Elle vint en grand apparat,</l>
					<l n="7" num="2.3">Traînant avec des airs d’infante</l>
					<l n="8" num="2.4">Un flot de velours nacarat :</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Telle qu’au rebord de sa loge</l>
					<l n="10" num="3.2">Elle brille aux Italiens,</l>
					<l n="11" num="3.3">Écoutant passer son éloge</l>
					<l n="12" num="3.4">Dans les chants des musiciens</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ensuite, en sa verve d’artiste,</l>
					<l n="14" num="4.2">Laissant tomber l’épais velours,</l>
					<l n="15" num="4.3">Dans un nuage de batiste</l>
					<l n="16" num="4.4">Elle ébaucha ses fiers contours.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Glissant de l’épaule à la hanche,</l>
					<l n="18" num="5.2">La chemise aux plis nonchalants,</l>
					<l n="19" num="5.3">Comme une tourterelle blanche</l>
					<l n="20" num="5.4">Vint s’abattre sur ses pieds blancs.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Pour Apelle ou pour Cléomène,</l>
					<l n="22" num="6.2">Elle semblait, marbre de chair,</l>
					<l n="23" num="6.3">En Vénus Anadyomène</l>
					<l n="24" num="6.4">Poser nue au bord de la mer.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">De grosses perles de Venise</l>
					<l n="26" num="7.2">Roulaient au lieu de gouttes d’eau,</l>
					<l n="27" num="7.3">Grains laiteux qu’un rayon irise,</l>
					<l n="28" num="7.4">Sur le frais satin de sa peau.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Oh ! quelles ravissantes choses</l>
					<l n="30" num="8.2">Dans sa divine nudité,</l>
					<l n="31" num="8.3">Avec les strophes de ses poses,</l>
					<l n="32" num="8.4">Chantait cet hymne de beauté !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Comme les flots baisant le sable</l>
					<l n="34" num="9.2">Sous la lune aux tremblants rayons,</l>
					<l n="35" num="9.3">Sa grâce était intarissable</l>
					<l n="36" num="9.4">En molles ondulations.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Mais bientôt, lasse d’art antique,</l>
					<l n="38" num="10.2">De Phidias et de Vénus,</l>
					<l n="39" num="10.3">Dans une autre stance plastique</l>
					<l n="40" num="10.4">Elle groupe ses charmes nus.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Sur un tapis de Cachemire,</l>
					<l n="42" num="11.2">C’est la sultane du sérail,</l>
					<l n="43" num="11.3">Riant au miroir qui l’admire</l>
					<l n="44" num="11.4">Avec un rire de corail ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">La Géorgienne indolente,</l>
					<l n="46" num="12.2">Avec son souple narguilhé,</l>
					<l n="47" num="12.3">Étalant sa hanche opulente,</l>
					<l n="48" num="12.4">Un pied sous l’autre replié,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Et comme l’odalisque d’Ingres,</l>
					<l n="50" num="13.2">De ses reins cambrant les rondeurs,</l>
					<l n="51" num="13.3">En dépit des vertus malingres,</l>
					<l n="52" num="13.4">En dépit des maigres pudeurs !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Paresseuse odalisque, arrière !</l>
					<l n="54" num="14.2">Voici le tableau dans son jour,</l>
					<l n="55" num="14.3">Le diamant dans sa lumière ;</l>
					<l n="56" num="14.4">Voici la beauté dans l’amour !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Sa tête penche et se renverse ;</l>
					<l n="58" num="15.2">Haletante, dressant les seins,</l>
					<l n="59" num="15.3">Aux bras du rêve qui la berce,</l>
					<l n="60" num="15.4">Elle tombe sur ses coussins.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Ses paupières battent des ailes</l>
					<l n="62" num="16.2">Sur leurs globes d’argent bruni,</l>
					<l n="63" num="16.3">Et l’on voit monter ses prunelles</l>
					<l n="64" num="16.4">Dans la nacre de l’infini.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">D’un linceul de point d’Angleterre</l>
					<l n="66" num="17.2">Que l’on recouvre sa beauté :</l>
					<l n="67" num="17.3">L’extase l’a prise à la terre ;</l>
					<l n="68" num="17.4">Elle est morte de volupté !</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Que les violettes de Parme,</l>
					<l n="70" num="18.2">Au lieu des tristes fleurs des morts</l>
					<l n="71" num="18.3">Où chaque perle est une larme,</l>
					<l n="72" num="18.4">Pleurent en bouquets sur son corps !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Et que mollement on la pose</l>
					<l n="74" num="19.2">Sur son lit, tombeau blanc et doux,</l>
					<l n="75" num="19.3">Où le poète, à la nuit close,</l>
					<l n="76" num="19.4">Ira prier à deux genoux.</l>
				</lg>
			</div></body></text></TEI>