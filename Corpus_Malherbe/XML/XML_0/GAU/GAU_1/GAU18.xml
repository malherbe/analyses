<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU18">
				<head type="main">INÈS DE LAS SIERRAS</head>
				<head type="sub">A LA PETRA CAMARA</head>
				<lg n="1">
					<l n="1" num="1.1">Nodier raconte qu’en Espagne</l>
					<l n="2" num="1.2">Trois officiers cherchant un soir,</l>
					<l n="3" num="1.3">Une venta dans la campagne,</l>
					<l n="4" num="1.4">Ne trouvèrent qu’un vieux manoir ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Un vrai château d’Anne Radcliffe,</l>
					<l n="6" num="2.2">Aux plafonds que le temps ploya,</l>
					<l n="7" num="2.3">Aux vitraux rayés par la griffe</l>
					<l n="8" num="2.4">Des chauves-souris de Goya,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Aux vastes salles délabrées,</l>
					<l n="10" num="3.2">Aux couloirs livrant leur secret,</l>
					<l n="11" num="3.3">Architectures effondrées</l>
					<l n="12" num="3.4">Où Piranèse se perdrait.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Pendant le souper, que regarde</l>
					<l n="14" num="4.2">Une collection d’aïeux</l>
					<l n="15" num="4.3">Dans leurs cadres montant la garde,</l>
					<l n="16" num="4.4">Un cri répond aux chants joyeux ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">D’un long corridor en décombres,</l>
					<l n="18" num="5.2">Par la lune bizarrement</l>
					<l n="19" num="5.3">Entrecoupé de clairs et d’ombres,</l>
					<l n="20" num="5.4">Débusque un fantôme charmant ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Peigne au chignon, basquine aux hanches,</l>
					<l n="22" num="6.2">Une femme accourt en dansant,</l>
					<l n="23" num="6.3">Dans les bandes noires et blanches</l>
					<l n="24" num="6.4">Apparaissant, disparaissant.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Avec une volupté morte,</l>
					<l n="26" num="7.2">Cambrant les reins, penchant le cou,</l>
					<l n="27" num="7.3">Elle s’arrête sur la porte,</l>
					<l n="28" num="7.4">Sinistre et belle à rendre fou.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Sa robe, passée et fripée</l>
					<l n="30" num="8.2">Au froid humide des tombeaux,</l>
					<l n="31" num="8.3">Fait luire, d’un rayon frappée,</l>
					<l n="32" num="8.4">Quelques paillons sur ses lambeaux ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">D’un pétale découronnée</l>
					<l n="34" num="9.2">A chaque soubresaut nerveux,</l>
					<l n="35" num="9.3">Sa rose, jaunie et fanée,</l>
					<l n="36" num="9.4">S’effeuille dans ses noirs cheveux.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Une cicatrice, pareille</l>
					<l n="38" num="10.2">A celle d’un coup de poignard,</l>
					<l n="39" num="10.3">Forme une couture vermeille</l>
					<l n="40" num="10.4">Sur sa gorge d’un ton blafard ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Et ses mains pâles et fluettes,</l>
					<l n="42" num="11.2">Au nez des soupeurs pleins d’effroi</l>
					<l n="43" num="11.3">Entre-choquent les castagnettes,</l>
					<l n="44" num="11.4">Comme des dents claquant de froid.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Elle danse, morne bacchante,</l>
					<l n="46" num="12.2">La cachucha sur un vieil air,</l>
					<l n="47" num="12.3">D’une grâce si provocante,</l>
					<l n="48" num="12.4">Qu’on la suivrait même en enfer.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Ses cils palpitent sur ses joues</l>
					<l n="50" num="13.2">Comme des ailes d’oiseau noir,</l>
					<l n="51" num="13.3">Et sa bouche arquée a des moues</l>
					<l n="52" num="13.4">A mettre un saint au désespoir.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Quand de sa jupe qui tournoie</l>
					<l n="54" num="14.2">Elle soulève le volant,</l>
					<l n="55" num="14.3">Sa jambe, sous le bas de soie,</l>
					<l n="56" num="14.4">Prend des lueurs de marbre blanc.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Elle se penche jusqu’à terre,</l>
					<l n="58" num="15.2">Et sa main, d’un geste coquet,</l>
					<l n="59" num="15.3">Comme on fait des fleurs d’un parterre</l>
					<l n="60" num="15.4">Groupe les désirs en bouquet.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Est-ce un fantôme ? est-ce une femme ?</l>
					<l n="62" num="16.2">Un rêve, une réalité,</l>
					<l n="63" num="16.3">Qui scintille comme une flamme</l>
					<l n="64" num="16.4">Dans un tourbillon de beauté ?</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Cette apparition fantasque,</l>
					<l n="66" num="17.2">C’est l’Espagne du temps passé,</l>
					<l n="67" num="17.3">Aux frissons du tambour de basque</l>
					<l n="68" num="17.4">S’élançant de son lit glacé,</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Et, brusquement ressuscitée</l>
					<l n="70" num="18.2">Dans un suprême boléro,</l>
					<l n="71" num="18.3">Montrant sous sa jupe argentée</l>
					<l n="72" num="18.4">La <hi rend="ital">divisa</hi> prise au taureau.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">La cicatrice qu’elle porte,</l>
					<l n="74" num="19.2">C’est le coup de grâce donné</l>
					<l n="75" num="19.3">A la génération morte,</l>
					<l n="76" num="19.4">Par chaque siècle nouveau-né.</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">J’ai vu ce fantôme au Gymnase,</l>
					<l n="78" num="20.2">Où Paris entier l’admira,</l>
					<l n="79" num="20.3">Lorsque dans son linceul de gaze</l>
					<l n="80" num="20.4">Parut la Petra Camara,</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Impassible et passionnée,</l>
					<l n="82" num="21.2">Fermant ses yeux morts de langueur,</l>
					<l n="83" num="21.3">Et comme Inès l’assassinée</l>
					<l n="84" num="21.4">Dansant, un poignard dans le cœur !</l>
				</lg>
			</div></body></text></TEI>