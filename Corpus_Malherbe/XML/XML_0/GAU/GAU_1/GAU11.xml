<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU11">
				<head type="main">CÆRULEI OCULI</head>
				<lg n="1">
					<l n="1" num="1.1">Une femme mystérieuse,</l>
					<l n="2" num="1.2">Dont la beauté trouble mes sens</l>
					<l n="3" num="1.3">Se tient debout, silencieuse,</l>
					<l n="4" num="1.4">Au bord des flots retentissants.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ses yeux, où le ciel se reflète,</l>
					<l n="6" num="2.2">Mêlent à leur azur amer,</l>
					<l n="7" num="2.3">Qu’étoile une humide paillette,</l>
					<l n="8" num="2.4">Les teintes glauques de la mer.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Dans les langueurs de leurs prunelles,</l>
					<l n="10" num="3.2">Une grâce triste sourit ;</l>
					<l n="11" num="3.3">Les pleurs mouillent les étincelles</l>
					<l n="12" num="3.4">Et la lumière s’attendrit ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et leurs cils comme des mouettes</l>
					<l n="14" num="4.2">Qui rasent le flot aplani,</l>
					<l n="15" num="4.3">Palpitent, ailes inquiètes,</l>
					<l n="16" num="4.4">Sur leur azur indéfini.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Comme dans l’eau bleue et profonde,</l>
					<l n="18" num="5.2">Où dort plus d’un trésor coulé,</l>
					<l n="19" num="5.3">On y découvre à travers l’onde</l>
					<l n="20" num="5.4">La coupe du roi de Thulé.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Sous leur transparence verdâtre,</l>
					<l n="22" num="6.2">Brille, parmi le goémon,</l>
					<l n="23" num="6.3">L’autre perle de Cléopâtre</l>
					<l n="24" num="6.4">Près de l’anneau de Salomon.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">La couronne au gouffre lancée</l>
					<l n="26" num="7.2">Dans la ballade de Schiller,</l>
					<l n="27" num="7.3">Sans qu’un plongeur l’ait ramassée,</l>
					<l n="28" num="7.4">Y jette encor son reflet clair.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Un pouvoir magique m’entraîne</l>
					<l n="30" num="8.2">Vers l’abîme de ce regard,</l>
					<l n="31" num="8.3">Comme au sein des eaux la sirène</l>
					<l n="32" num="8.4">Attirait Harald Harfagar.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Mon âme, avec la violence</l>
					<l n="34" num="9.2">D’un irrésistible désir,</l>
					<l n="35" num="9.3">Au milieu du gouffre s’élance</l>
					<l n="36" num="9.4">Vers l’ombre impossible à saisir.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Montrant son sein, cachant sa queue,</l>
					<l n="38" num="10.2">La sirène amoureusement</l>
					<l n="39" num="10.3">Fait ondoyer sa blancheur bleue</l>
					<l n="40" num="10.4">Sous l’émail vert du flot dormant.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">L’eau s’enfle comme une poitrine</l>
					<l n="42" num="11.2">Aux soupirs de la passion ;</l>
					<l n="43" num="11.3">Le vent, dans sa conque marine,</l>
					<l n="44" num="11.4">Murmure une incantation.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">«Oh ! viens dans ma couche de nacre,</l>
					<l n="46" num="12.2">Mes bras d’onde t’enlaceront ;</l>
					<l n="47" num="12.3">Les flots, perdant leur saveur âcre,</l>
					<l n="48" num="12.4">Sur ta bouche, en miel couleront.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">«Laissant bruire sur nos têtes,</l>
					<l n="50" num="13.2">La mer qui ne peut s’apaiser,</l>
					<l n="51" num="13.3">Nous boirons l’oubli des tempêtes</l>
					<l n="52" num="13.4">Dans la coupe de mon baiser.»</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Ainsi parle la voix humide</l>
					<l n="54" num="14.2">De ce regard céruléen,</l>
					<l n="55" num="14.3">Et mon cœur, sous l’onde perfide,</l>
					<l n="56" num="14.4">Se noie et consomme l’hymen.</l>
				</lg>
			</div></body></text></TEI>