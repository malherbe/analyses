<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU28">
				<head type="main">LA MONTRE</head>
				<lg n="1">
					<l n="1" num="1.1">Deux fois je regarde ma montre,</l>
					<l n="2" num="1.2">Et deux fois à mes yeux distraits</l>
					<l n="3" num="1.3">L’aiguille au même endroit se montre ;</l>
					<l n="4" num="1.4">Il est une heure…, une heure après.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">La figure de la pendule</l>
					<l n="6" num="2.2">En rit dans le salon voisin,</l>
					<l n="7" num="2.3">Et le timbre d’argent module</l>
					<l n="8" num="2.4">Deux coups vibrant comme un tocsin.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Le cadran solaire me raille</l>
					<l n="10" num="3.2">En m’indiquant, de son long doigt,</l>
					<l n="11" num="3.3">Le chemin que sur la muraille</l>
					<l n="12" num="3.4">A fait son ombre qui s’accroît.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Le clocher avec ironie</l>
					<l n="14" num="4.2">Dit le vrai chiffre et le beffroi,</l>
					<l n="15" num="4.3">Reprenant la note finie,</l>
					<l n="16" num="4.4">A l’air de se moquer de moi.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Tiens ! la petite bête est morte.</l>
					<l n="18" num="5.2">Je n’ai pas mis hier encor,</l>
					<l n="19" num="5.3">Tant ma rêverie était forte,</l>
					<l n="20" num="5.4">Au trou de rubis la clef d’or !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Et je ne vois plus, dans sa boîte,</l>
					<l n="22" num="6.2">Le fin ressort du balancier</l>
					<l n="23" num="6.3">Aller, venir, à gauche, à droite,</l>
					<l n="24" num="6.4">Ainsi qu’un papillon d’acier.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">C’est bien de moi ! Quand je chevauche</l>
					<l n="26" num="7.2">L’Hippogriffe, au pays du Bleu,</l>
					<l n="27" num="7.3">Mon corps sans âme se débauche,</l>
					<l n="28" num="7.4">Et s’en va comme il plaît à Dieu !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">L’éternité poursuit son cercle</l>
					<l n="30" num="8.2">Autour de ce cadran muet,</l>
					<l n="31" num="8.3">Et le temps, l’oreille au couvercle,</l>
					<l n="32" num="8.4">Cherche ce cœur qui remuait ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Ce cœur que l’enfant croit en vie,</l>
					<l n="34" num="9.2">Et dont chaque pulsation</l>
					<l n="35" num="9.3">Dans notre poitrine est suivie</l>
					<l n="36" num="9.4">D’une égale vibration,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Il ne bat plus, mais son grand frère</l>
					<l n="38" num="10.2">Toujours palpite à mon côté.</l>
					<l n="39" num="10.3">— Celui que rien ne peut distraire,</l>
					<l n="40" num="10.4">Quand je dormais, l’a remonté !</l>
				</lg>
			</div></body></text></TEI>