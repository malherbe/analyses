<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU15">
				<head type="main">TRISTESSE EN MER</head>
				<lg n="1">
					<l n="1" num="1.1">Les mouettes volent et jouent ;</l>
					<l n="2" num="1.2">Et les blancs coursiers de la mer,</l>
					<l n="3" num="1.3">Cabrés sur les vagues secouent</l>
					<l n="4" num="1.4">Leurs crins échevelés dans l’air.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Le jour tombe ; une fine pluie</l>
					<l n="6" num="2.2">Éteint les fournaises du soir,</l>
					<l n="7" num="2.3">Et le steam-boat crachant la suie</l>
					<l n="8" num="2.4">Rabat son long panache noir.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Plus pâle que le ciel livide</l>
					<l n="10" num="3.2">Je vais au pays du charbon,</l>
					<l n="11" num="3.3">Du brouillard et du suicide ;</l>
					<l n="12" num="3.4">— Pour se tuer le temps est bon.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Mon désir avide se noie</l>
					<l n="14" num="4.2">Dans le gouffre amer qui blanchit ;</l>
					<l n="15" num="4.3">Le vaisseau danse, l’eau tournoie,</l>
					<l n="16" num="4.4">Le vent de plus en plus fraîchit.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Oh ! je me sens l’âme navrée ;</l>
					<l n="18" num="5.2">L’Océan gonfle, en soupirant,</l>
					<l n="19" num="5.3">Sa poitrine désespérée,</l>
					<l n="20" num="5.4">Comme un ami qui me comprend.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Allons, peines d’amour perdues,</l>
					<l n="22" num="6.2">Espoirs lassés, illusions</l>
					<l n="23" num="6.3">Du socle idéal descendues,</l>
					<l n="24" num="6.4">Un saut dans les moites sillons !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">A la mer, souffrances passées,</l>
					<l n="26" num="7.2">Qui revenez toujours, pressant</l>
					<l n="27" num="7.3">Vos blessures cicatrisées</l>
					<l n="28" num="7.4">Pour leur faire pleurer du sang !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">A la mer, spectre de mes rêves,</l>
					<l n="30" num="8.2">Regrets aux mortelles pâleurs</l>
					<l n="31" num="8.3">Dans un cœur rouge ayant sept glaives,</l>
					<l n="32" num="8.4">Comme la Mère des douleurs.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Chaque fantôme plonge et lutte</l>
					<l n="34" num="9.2">Quelques instants avec le flot</l>
					<l n="35" num="9.3">Qui sur lui ferme sa volute</l>
					<l n="36" num="9.4">Et l’engloutit dans un sanglot.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Lest de l’âme, pesant bagage,</l>
					<l n="38" num="10.2">Trésors misérables et chers,</l>
					<l n="39" num="10.3">Sombrez, et dans votre naufrage</l>
					<l n="40" num="10.4">Je vais vous suivre au fond des mers !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Bleuâtre, enflé, méconnaissable,</l>
					<l n="42" num="11.2">Bercé par le flot qui bruit,</l>
					<l n="43" num="11.3">Sur l’humide oreiller du sable</l>
					<l n="44" num="11.4">Je dormirai bien cette nuit !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">… Mais une femme dans sa mante</l>
					<l n="46" num="12.2">Sur le pont assise à l’écart,</l>
					<l n="47" num="12.3">Une femme jeune et charmante</l>
					<l n="48" num="12.4">Lève vers moi son long regard.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Dans ce regard, à ma détresse</l>
					<l n="50" num="13.2">La Sympathie aux bras ouverts</l>
					<l n="51" num="13.3">Parle et sourit, sœur ou maîtresse.</l>
					<l n="52" num="13.4">Salut, yeux bleus ! bonsoir, flots verts !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Les mouettes volent et jouent ;</l>
					<l n="54" num="14.2">Et les blancs coursiers de la mer,</l>
					<l n="55" num="14.3">Cabrés sur les vagues, secouent</l>
					<l n="56" num="14.4">Leurs crins échevelés dans l’air.</l>
				</lg>
			</div></body></text></TEI>