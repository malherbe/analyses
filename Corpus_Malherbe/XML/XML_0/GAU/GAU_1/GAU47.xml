<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU47">
				<head type="main">L’ART</head>
				<lg n="1">
					<l n="1" num="1.1">Oui, l’œuvre sort plus belle</l>
					<l n="2" num="1.2">D’une forme au travail</l>
					<l n="3" num="1.3"><space quantity="8" unit="char"></space>Rebelle,</l>
					<l n="4" num="1.4">Vers, marbre, onyx, émail.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Point de contraintes fausses !</l>
					<l n="6" num="2.2">Mais que pour marcher droit</l>
					<l n="7" num="2.3"><space quantity="8" unit="char"></space>Tu chausses,</l>
					<l n="8" num="2.4">Muse, un cothurne étroit.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Fi du rhythme commode,</l>
					<l n="10" num="3.2">Comme un soulier trop grand,</l>
					<l n="11" num="3.3"><space quantity="8" unit="char"></space>Du mode</l>
					<l n="12" num="3.4">Que tout pied quitte et prend !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Statuaire, repousse</l>
					<l n="14" num="4.2">L’argile que pétrit</l>
					<l n="15" num="4.3"><space quantity="8" unit="char"></space>Le pouce</l>
					<l n="16" num="4.4">Quand flotte ailleurs l’esprit,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Lutte avec le carrare,</l>
					<l n="18" num="5.2">Avec le paros dur</l>
					<l n="19" num="5.3"><space quantity="8" unit="char"></space>Et rare,</l>
					<l n="20" num="5.4">Gardiens du contour pur ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Emprunte à Syracuse</l>
					<l n="22" num="6.2">Son bronze où fermement</l>
					<l n="23" num="6.3"><space quantity="8" unit="char"></space>S’accuse</l>
					<l n="24" num="6.4">Le trait fier et charmant ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">D’une main délicate</l>
					<l n="26" num="7.2">Poursuis dans un filon</l>
					<l n="27" num="7.3"><space quantity="8" unit="char"></space>D’agate</l>
					<l n="28" num="7.4">Le profil d’Apollon.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Peintre, fuis l’aquarelle,</l>
					<l n="30" num="8.2">Et fixe la couleur</l>
					<l n="31" num="8.3"><space quantity="8" unit="char"></space>Trop frêle</l>
					<l n="32" num="8.4">Au four de l’émailleur.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Fais les sirènes bleues,</l>
					<l n="34" num="9.2">Tordant de cent façons</l>
					<l n="35" num="9.3"><space quantity="8" unit="char"></space>Leurs queues,</l>
					<l n="36" num="9.4">Les monstres des blasons,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Dans son nimbe trilobe</l>
					<l n="38" num="10.2">La Vierge et son Jésus,</l>
					<l n="39" num="10.3"><space quantity="8" unit="char"></space>Le globe</l>
					<l n="40" num="10.4">Avec la croix dessus.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Tout passe.— L’art robuste</l>
					<l n="42" num="11.2">Seul a l’éternité,</l>
					<l n="43" num="11.3"><space quantity="8" unit="char"></space>Le buste</l>
					<l n="44" num="11.4">Survit à la cité.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Et la médaille austère</l>
					<l n="46" num="12.2">Que trouve un laboureur</l>
					<l n="47" num="12.3"><space quantity="8" unit="char"></space>Sous terre</l>
					<l n="48" num="12.4">Révèle un empereur.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Les dieux eux-mêmes meurent,</l>
					<l n="50" num="13.2">Mais les vers souverains</l>
					<l n="51" num="13.3"><space quantity="8" unit="char"></space>Demeurent</l>
					<l n="52" num="13.4">Plus forts que les airains.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Sculpte, lime, cisèle ;</l>
					<l n="54" num="14.2">Que ton rêve flottant</l>
					<l n="55" num="14.3"><space quantity="8" unit="char"></space>Se scelle</l>
					<l n="56" num="14.4">Dans le bloc résistant !</l>
				</lg>
			</div></body></text></TEI>