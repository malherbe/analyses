<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU129">
				<head type="main">PENSÉES D’AUTOMNE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								La rica autouna s’es passada <lb></lb>
								L’hiver suz un cari tourat <lb></lb>
								S’en ven la capa ementoulada <lb></lb>
								D’un veû neblouz enjalibrat.
							</quote>
							<bibl>
								<hi rend="ital">Son autounous.</hi>
							</bibl>
						</cit>
						<cit>
							<quote>
								J’entends siffler la bise aux branchages rouillés <lb></lb>
								Des saules qui là-bas se balancent mouillés.
							</quote>
							<bibl>
								<name>Auguste M.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">L’automne va finir ; au milieu du ciel terne,</l>
					<l n="2" num="1.2">Dans un cercle blafard et livide que cerne</l>
					<l n="3" num="1.3">Un nuage plombé, le soleil dort : du fond</l>
					<l n="4" num="1.4">Des étangs remplis d’eau monte un brouillard qui fond</l>
					<l n="5" num="1.5">Collines, champs, hameaux dans une même teinte.</l>
					<l n="6" num="1.6">Sur les carreaux la pluie en larges gouttes tinte ;</l>
					<l n="7" num="1.7">La froide bise siffle ; un sourd frémissement</l>
					<l n="8" num="1.8">Sort du sein des forêts ; les oiseaux tristement,</l>
					<l n="9" num="1.9">Mêlant leurs cris plaintifs aux cris des bêtes fauves,</l>
					<l n="10" num="1.10">Sautent de branche en branche à travers les bois chauves,</l>
					<l n="11" num="1.11">Et semblent aux beaux jours envolés dire adieu.</l>
					<l n="12" num="1.12">Le pauvre paysan se recommande à Dieu,</l>
					<l n="13" num="1.13">Craignant un hiver rude ; et moi, dans les vallées,</l>
					<l n="14" num="1.14">Quand je vois le gazon sous les blanches gelées</l>
					<l n="15" num="1.15">Disparaître et mourir, je reviens à pas lents</l>
					<l n="16" num="1.16">M’asseoir le cœur navré près des tisons brûlants,</l>
					<l n="17" num="1.17">Et là je me souviens du soleil de septembre</l>
					<l n="18" num="1.18">Qui donnait à la grappe un jaune reflet d’ambre,</l>
					<l n="19" num="1.19">Des pommiers du chemin pliant sous leur fardeau,</l>
					<l n="20" num="1.20">Et du trèfle fleuri, pittoresque rideau</l>
					<l n="21" num="1.21">S’étendant à longs plis sur la plaine rayée,</l>
					<l n="22" num="1.22">Et de la route étroite en son milieu frayée,</l>
					<l n="23" num="1.23">Et surtout des bleuets et des coquelicots,</l>
					<l n="24" num="1.24">Points de pourpre et d’azur dans l’or des blés égaux.</l>
				</lg>
			</div></body></text></TEI>