<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU143">
				<head type="main">MARIA</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
									 … meæ puellæ <lb></lb>
								Flendo turgiduli rubent ocelli.
							</quote>
							<bibl>
								<name>V. Catullus.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Ne pleure pas…
							</quote>
							<bibl>
								<name>Dovalle.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">De tes longs cils de jais que ta main blanche essuie,</l>
					<l n="2" num="1.2">Comme des gouttes d’eau d’un arbre après la pluie,</l>
					<l n="3" num="1.3">Ou comme la rosée, au point du jour, des fleurs</l>
					<l n="4" num="1.4">Qu’un pied inattentif froisse, j’ai vu des pleurs</l>
					<l n="5" num="1.5">Tomber et ruisseler en perles sur ta joue :</l>
					<l n="6" num="1.6">En vain de la gaîté l’éclair à présent joue</l>
					<l n="7" num="1.7">Dans tes yeux bruns ; en vain ta bouche me sourit ;</l>
					<l n="8" num="1.8">D’inquiètes terreurs agitent mon esprit.</l>
					<l n="9" num="1.9">Qu’avais-tu, Maria, toi, rieuse et folâtre,</l>
					<l n="10" num="1.10">Toi, de plaisirs bruyants et de danse idolâtre,</l>
					<l n="11" num="1.11">Le soir, quand le soleil incline à l’horizon,</l>
					<l n="12" num="1.12">La première à fouler l’émail vert du gazon,</l>
					<l n="13" num="1.13">La première à poursuivre en sa rapide course</l>
					<l n="14" num="1.14">La demoiselle bleue aux bords frais de la source,</l>
					<l n="15" num="1.15">A chanter des chansons, à reprendre un refrain ?</l>
					<l n="16" num="1.16">Toi qui n’as jamais su ce qu’était un chagrin,</l>
					<l n="17" num="1.17">A l’écart tu pleurais. Réponds-moi, quel orage</l>
					<l n="18" num="1.18">Avait terni l’éclat de ton ciel sans nuage ?</l>
					<l n="19" num="1.19">Ton passereau chéri bat de l’aile, joyeux,</l>
					<l n="20" num="1.20">Les barreaux de sa cage, et sur son lit soyeux</l>
					<l n="21" num="1.21">Ton jeune épagneul dort, tout va bien, et tes roses</l>
					<l n="22" num="1.22">Répandent leurs parfums, heureusement écloses.</l>
					<l n="23" num="1.23">Qu’avais-tu donc, enfant ? quel malheur imprévu</l>
					<l n="24" num="1.24">Te faisait triste ? — Hier je ne t’avais pas vu.</l>
				</lg>
			</div></body></text></TEI>