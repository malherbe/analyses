<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU136">
				<head type="main">VOYAGE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Il me faut du nouveau n’en fût-il plus au monde.
							</quote>
							<bibl>
								<name>Jean de La Fontaine.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Jam mens prætrepidans avet vagari, <lb></lb>
							Jam læti studio pedes vigescunt.
							</quote>
							<bibl>
								<name>Catulle.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Au travers de la vitre blanche</l>
					<l n="2" num="1.2">Le soleil rit, et sur les murs</l>
					<l n="3" num="1.3">Traçant de grands angles, épanche</l>
					<l n="4" num="1.4">Ses rayons splendides et purs :</l>
					<l n="5" num="1.5">Par un si beau temps, à la ville</l>
					<l n="6" num="1.6">Rester parmi la foule vile !</l>
					<l n="7" num="1.7">Je veux voir des sites nouveaux :</l>
					<l n="8" num="1.8">Postillons, sellez vos chevaux.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Au sein d’un nuage de poudre,</l>
					<l n="10" num="2.2">Par un galop précipité,</l>
					<l n="11" num="2.3">Aussi promptement que la foudre</l>
					<l n="12" num="2.4">Comme il est doux d’être emporté !</l>
					<l n="13" num="2.5">Le sable bruit sous la roue,</l>
					<l n="14" num="2.6">Le vent autour de vous se joue ;</l>
					<l n="15" num="2.7">Je veux voir des sites nouveaux :</l>
					<l n="16" num="2.8">Postillons, pressez vos chevaux.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Les arbres qui bordent la route</l>
					<l n="18" num="3.2">Paraissent fuir rapidement,</l>
					<l n="19" num="3.3">Leur forme obscure dont l’œil doute</l>
					<l n="20" num="3.4">Ne se dessine qu’un moment ;</l>
					<l n="21" num="3.5">Le ciel, tel qu’une banderole,</l>
					<l n="22" num="3.6">Par-dessus les bois roule et vole ;</l>
					<l n="23" num="3.7">Je veux voir des sites nouveaux :</l>
					<l n="24" num="3.8">Postillons, pressez vos chevaux.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Chaumières, fermes isolées,</l>
					<l n="26" num="4.2">Vieux châteaux que flanque une tour,</l>
					<l n="27" num="4.3">Monts arides, fraîches vallées,</l>
					<l n="28" num="4.4">Forêts se suivent tour à tour ;</l>
					<l n="29" num="4.5">Parfois au milieu d’une brume,</l>
					<l n="30" num="4.6">Un ruisseau dont la chute écume ;</l>
					<l n="31" num="4.7">Je veux voir des sites nouveaux :</l>
					<l n="32" num="4.8">Postillons, pressez vos chevaux.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Puis, une hirondelle qui passe,</l>
					<l n="34" num="5.2">Rasant la grève au sable d’or,</l>
					<l n="35" num="5.3">Puis, semés dans un large espace,</l>
					<l n="36" num="5.4">Les moutons d’un berger qui dort ;</l>
					<l n="37" num="5.5">De grandes perspectives bleues,</l>
					<l n="38" num="5.6">Larges et longues de vingt lieues ;</l>
					<l n="39" num="5.7">Je veux voir des sites nouveaux :</l>
					<l n="40" num="5.8">Postillons, pressez vos chevaux.</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">Une montagne : l’on enraye,</l>
					<l n="42" num="6.2">Au bord du rapide penchant</l>
					<l n="43" num="6.3">D’un mont dont la hauteur effraye :</l>
					<l n="44" num="6.4">Les chevaux glissent en marchant,</l>
					<l n="45" num="6.5">L’essieu grince, le pavé fume,</l>
					<l n="46" num="6.6">Et la roue un instant s’allume ;</l>
					<l n="47" num="6.7">Je veux voir des sites nouveaux :</l>
					<l n="48" num="6.8">Postillons, pressez vos chevaux.</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1">La côte raide est descendue.</l>
					<l n="50" num="7.2">Recouverte de sable fin,</l>
					<l n="51" num="7.3">La route, à chaque instant perdue,</l>
					<l n="52" num="7.4">S’étend comme un ruban sans fin.</l>
					<l n="53" num="7.5">Que cette plaine est monotone !</l>
					<l n="54" num="7.6">On dirait un matin d’automne,</l>
					<l n="55" num="7.7">Je veux voir des sites nouveaux :</l>
					<l n="56" num="7.8">Postillons, pressez vos chevaux.</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1">Une ville d’un aspect sombre,</l>
					<l n="58" num="8.2">Avec ses tours et ses clochers</l>
					<l n="59" num="8.3">Qui montent dans les airs, sans nombre,</l>
					<l n="60" num="8.4">Comme des mâts ou des rochers,</l>
					<l n="61" num="8.5">Où mille lumières flamboient</l>
					<l n="62" num="8.6">Au sein des ombres qui la noient ;</l>
					<l n="63" num="8.7">Je veux voir des sites nouveaux :</l>
					<l n="64" num="8.8">Postillons, pressez vos chevaux !</l>
				</lg>
				<lg n="9">
					<l n="65" num="9.1">Mais ils sont las, et leurs narines,</l>
					<l n="66" num="9.2">Rouges de sang, soufflent du feu ;</l>
					<l n="67" num="9.3">L’écume inonde leurs poitrines</l>
					<l n="68" num="9.4">Il faut nous arrêter un peu.</l>
					<l n="69" num="9.5">Halte ! demain, plus vite encore,</l>
					<l n="70" num="9.6">Aussitôt que poindra l’aurore,</l>
					<l n="71" num="9.7">Postillons, pressez vos chevaux,</l>
					<l n="72" num="9.8">Je veux voir des sites nouveaux.</l>
				</lg>
			</div></body></text></TEI>