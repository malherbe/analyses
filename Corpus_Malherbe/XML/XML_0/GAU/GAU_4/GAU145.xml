<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU145">
				<head type="main">LE JARDIN DES PLANTES</head>
				<opener>
					<epigraph>
						<cit>
							<quote>L’homme propose et Dieu dispose.</quote>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">J’étais parti, voyant le ciel limpide et clair</l>
					<l n="2" num="1.2">Et les chemins séchés, afin de prendre l’air,</l>
					<l n="3" num="1.3">D’ouïr le vent qui pleure aux branches du mélèze,</l>
					<l n="4" num="1.4">Et de mieux travailler : car on est plus à l’aise</l>
					<l n="5" num="1.5">Pour méditer le plan d’un drame projeté,</l>
					<l n="6" num="1.6">Refondre un vers pesant et sans grâce jeté,</l>
					<l n="7" num="1.7">Ou d’une rime faible à sa sœur mal unie</l>
					<l n="8" num="1.8">Par un son plus exact réparer l’harmonie,</l>
					<l n="9" num="1.9">Sous les arbres touffus inclinés en arceaux</l>
					<l n="10" num="1.10">Du labyrinthe vert, quand des milliers d’oiseaux</l>
					<l n="11" num="1.11">Chantent auprès de vous, et que la brise joue</l>
					<l n="12" num="1.12">Dans vos cheveux épars et baise votre joue,</l>
					<l n="13" num="1.13">Qu’on ne l’est dans sa chambre, un bureau devant soi,</l>
					<l n="14" num="1.14">S’étant fait d’y rester une pénible loi,</l>
					<l n="15" num="1.15">Et, comme un ouvrier que son devoir attache,</l>
					<l n="16" num="1.16">De ne pas s’arrêter qu’on n’ait fini sa tâche,</l>
					<l n="17" num="1.17">Remis le tout au net, et bien dûment serré</l>
					<l n="18" num="1.18">L’œuvre dans un tiroir aux profanes sacré,</l>
					<l n="19" num="1.19">Et je m’étais promis de rapporter la feuille</l>
					<l n="20" num="1.20">Où, du crayon aidé, mon doigt fixe et recueille</l>
					<l n="21" num="1.21">Mes pensers vagabonds, pleine jusques aux bords</l>
					<l n="22" num="1.22">De vers harmonieux, poétiques trésors,</l>
					<l n="23" num="1.23">Destinés à grossir un trop mince volume.</l>
					<l n="24" num="1.24">Vains projets ! notre esprit est pareil à la plume,</l>
					<l n="25" num="1.25">Un souffle d’air l’emporte hors de son droit chemin,</l>
					<l n="26" num="1.26">Et nul ne peut prévoir ce qu’il fera demain.</l>
					<l n="27" num="1.27">Aussi moi, pauvre fou, séduit par l’étincelle</l>
					<l n="28" num="1.28">Qui, furtive, jaillit d’une noire prunelle,</l>
					<l n="29" num="1.29">Par un rire qui livre aux yeux de blanches dents</l>
					<l n="30" num="1.30">Oubliant prose et vers, de mes regards ardents</l>
					<l n="31" num="1.31">Je suis la jeune fille, et bientôt, moins timide,</l>
					<l n="32" num="1.32">J’égale à son pas leste et prompt mon pas rapide,</l>
					<l n="33" num="1.33">Je risque quelques mots et place sous mon bras,</l>
					<l n="34" num="1.34">Quoiqu’on dise : Méchant ! et qu’on ne veuille pas,</l>
					<l n="35" num="1.35">Une main potelée ; et nous allons à l’ombre,</l>
					<l n="36" num="1.36">Dans un lieu du jardin bien tranquille et bien sombre,</l>
					<l n="37" num="1.37">Faire mieux connaissance, et jouer et causer</l>
					<l n="38" num="1.38">Et sur le banc de pierre après nous reposer,</l>
					<l n="39" num="1.39">Et nous nous promettons de nous revoir dimanche,</l>
					<l n="40" num="1.40">Et je reviens avec ma feuille toute blanche.</l>
				</lg>
			</div></body></text></TEI>