<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU160">
				<head type="main">JUSTIFICATION</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Vous êtes mal pour moi, vous avez quelque chose.
							</quote>
							<bibl>
								<hi rend="ital">Marion Delorme.</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Celui que chaque soir votre parole élève,</l>
					<l n="2" num="1.2"><space quantity="4" unit="char"></space>Qui pense avec vous de moitié ;</l>
					<l n="3" num="1.3">Celui dont vous savez le plus intime rêve</l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space>Et qui vit de votre amitié ;</l>
					<l n="5" num="1.5">Celui que vous avez laissé voir dans votre âme,</l>
					<l n="6" num="1.6"><space quantity="4" unit="char"></space>Et s’approcher de votre cœur,</l>
					<l n="7" num="1.7">Afin de lui montrer ce que Dieu dans la femme</l>
					<l n="8" num="1.8"><space quantity="4" unit="char"></space>A mis d’amour et de bonheur,</l>
					<l n="9" num="1.9">Quand il n’y croyait plus et n’avait d’autre envie,</l>
					<l n="10" num="1.10"><space quantity="4" unit="char"></space>Las de traîner depuis vingt ans</l>
					<l n="11" num="1.11">Son boulet de forçat au bagne de la vie,</l>
					<l n="12" num="1.12"><space quantity="4" unit="char"></space>Que de n’y pas finir son temps ;</l>
					<l n="13" num="1.13">— Celui-là ne sera jamais, il vous le jure</l>
					<l n="14" num="1.14"><space quantity="4" unit="char"></space>Sur ce cœur que vous avez fait,</l>
					<l n="15" num="1.15">Un de ces hommes vils, dont la pensée impure</l>
					<l n="16" num="1.16"><space quantity="4" unit="char"></space>Aux choses basses se complaît. —</l>
					<l n="17" num="1.17">L’âme que vous avez mariée à la vôtre</l>
					<l n="18" num="1.18"><space quantity="4" unit="char"></space>Pourrait jusque-là s’oublier !…</l>
					<l n="19" num="1.19">— Dans le cloaque infect où le canard se vautre</l>
					<l n="20" num="1.20"><space quantity="4" unit="char"></space>Voit-on s’abattre l’aigle altier ?</l>
					<l n="21" num="1.21">Non, — l’aigle vit tout seul sur la plus haute cime,</l>
					<l n="22" num="1.22"><space quantity="4" unit="char"></space>— Le tonnerre rugit en bas,</l>
					<l n="23" num="1.23">L’avalanche s’écrase et roule dans l’abîme ;</l>
					<l n="24" num="1.24"><space quantity="4" unit="char"></space>Le torrent hurle : — il n’entend pas ;</l>
					<l n="25" num="1.25">Immobile, de l’ongle étreignant quelque pierre,</l>
					<l n="26" num="1.26"><space quantity="4" unit="char"></space>Quelque bras de pin foudroyé,</l>
					<l n="27" num="1.27">Il attache au soleil son grand œil sans paupière,</l>
					<l n="28" num="1.28"><space quantity="4" unit="char"></space>D’ineffables lueurs noyé.</l>
				</lg>
			</div></body></text></TEI>