<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU157">
				<head type="main">PAN DE MUR</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								La mousse des vieux jours qui brunit sa surface, <lb></lb>
								Et d’hiver en hiver incrustée à ses flancs, <lb></lb>
								Donne en lettre vivante une date à ses ans.
							</quote>
							<bibl>
								<hi rend="ital">Harmonies.</hi>
							</bibl>
						</cit>
						<cit>
							<quote>
								… Qu’il vienne à ma croisée.
							</quote>
							<bibl>
								<name>Petrus Borel</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">De la maison momie enterrée au Marais</l>
					<l n="2" num="1.2">Où, du monde cloîtré, jadis je demeurais,</l>
					<l n="3" num="1.3">L’on a pour perspective une muraille sombre</l>
					<l n="4" num="1.4">Où des pignons voisins tombe, à grands angles, l’ombre.</l>
					<l n="5" num="1.5">— A ses flancs dégradés par la pluie et les ans,</l>
					<l n="6" num="1.6">Pousse dans les gravois l’ortie aux feux cuisants,</l>
					<l n="7" num="1.7">Et sur ses pieds moisis, comme un tapis verdâtre,</l>
					<l n="8" num="1.8">La mousse se déploie et fait gercer le plâtre.</l>
					<l n="9" num="1.9">— Une treille stérile avec ses bras grimpants</l>
					<l n="10" num="1.10">Jusqu’au premier étage en festonne les pans ;</l>
					<l n="11" num="1.11">Le bleu volubilis dans les fentes s’accroche,</l>
					<l n="12" num="1.12">La capucine rouge épanouit sa cloche,</l>
					<l n="13" num="1.13">Et, mariant en l’air leurs tranchantes couleurs,</l>
					<l n="14" num="1.14">A sa fenêtre font comme un cadre de fleurs :</l>
					<l n="15" num="1.15">Car elle n’en a qu’une, et sans cesse vous lorgne</l>
					<l n="16" num="1.16">De son regard unique ainsi que fait un borgne,</l>
					<l n="17" num="1.17">Allumant aux brasiers du soir, comme autant d’yeux,</l>
					<l n="18" num="1.18">Dans leurs mailles de plomb ses carreaux chassieux.</l>
					<l n="19" num="1.19">— Une caisse d’œillets, un pot de giroflée</l>
					<l n="20" num="1.20">Qui laisse choir au vent sa feuille étiolée,</l>
					<l n="21" num="1.21">Et du soleil oblique implore le regard,</l>
					<l n="22" num="1.22">Une cage d’osier où saute un geai criard,</l>
					<l n="23" num="1.23">C’est un tableau tout fait qui vaut qu’on l’étudie ;</l>
					<l n="24" num="1.24">Mais il faut pour le rendre une touche hardie,</l>
					<l n="25" num="1.25">Une palette riche où luise plus d’un ton,</l>
					<l n="26" num="1.26">Celle de Boulanger ou bien de Bonnington.</l>
				</lg>
			</div></body></text></TEI>