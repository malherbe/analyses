<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU138">
				<head type="main">LA TÊTE DE MORT</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Ton test n’aura plus de peau, <lb></lb>
							Et ton visage si beau <lb></lb>
							N’aura veines ni artères, <lb></lb>
							Tu n’auras plus que des dents <lb></lb>
							Telles qu’on les voit dedans <lb></lb>
							Les têtes des cimetières.
							</quote>
							<bibl>
								<name>Pierre Ronsard.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								La mort nous fait dormir une éternelle nuit.
							</quote>
							<bibl>
								<name>Joachim du Bellay.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Personne ne voulait aller dans cette chambre,</l>
					<l n="2" num="1.2">Surtout pendant les nuits si tristes de décembre,</l>
					<l n="3" num="1.3">Quand la bise gémit et pousse des sanglots,</l>
					<l n="4" num="1.4">Et que du ciel obscur tombe la pluie à flots.</l>
					<l n="5" num="1.5">Car c’était une chambre antique, inhabitée,</l>
					<l n="6" num="1.6">A minuit, disait-on, de revenants hantée,</l>
					<l n="7" num="1.7">Une chambre où les ais du parquet désuni</l>
					<l n="8" num="1.8">S’agitent sous vos pieds, où le plafond jauni</l>
					<l n="9" num="1.9">Se partage et s’écroule, où la tapisserie</l>
					<l n="10" num="1.10">A personnages tremble, et sur la boiserie</l>
					<l n="11" num="1.11">Ondule à plis poudreux au moindre ébranlement.</l>
					<l n="12" num="1.12">On en avait ôté les meubles ; seulement,</l>
					<l n="13" num="1.13">Entre de vieux portraits, un crucifix d’ivoire,</l>
					<l n="14" num="1.14">Avec du buis bénit, sur une étoffe noire,</l>
					<l n="15" num="1.15">Pendait du mur : au bas, en guise de support,</l>
					<l n="16" num="1.16">On avait mis jadis une tête de mort ;</l>
					<l n="17" num="1.17">Et me ressouvenant des fables qu’on débite,</l>
					<l n="18" num="1.18">Enfant, je croyais voir au fond de cet orbite</l>
					<l n="19" num="1.19">Que l’œil n’anime plus, de blafardes lueurs ;</l>
					<l n="20" num="1.20">Et, quand il me fallait passer là, des sueurs</l>
					<l n="21" num="1.21">M’inondaient, tour à tour brûlantes et glacées :</l>
					<l n="22" num="1.22">J’aurais fait le serment que les dents déchaussées</l>
					<l n="23" num="1.23">De cet épouvantail en ricanant grinçaient,</l>
					<l n="24" num="1.24">Et que confusément des mots s’en élançaient.</l>
					<l n="25" num="1.25">A présent jeune encor, mais certain que notre âme,</l>
					<l n="26" num="1.26">Inexplicable essence, insaisissable flamme,</l>
					<l n="27" num="1.27">Une fois exhalée, en nous tout est néant,</l>
					<l n="28" num="1.28">Et que rien ne ressort de l’abîme béant</l>
					<l n="29" num="1.29">Où vont, tristes jouets du temps, nos destinées,</l>
					<l n="30" num="1.30">Comme au cours des ruisseaux les feuilles entraînées,</l>
					<l n="31" num="1.31">Sans peur je la regarde, et je dis : Quelques ans,</l>
					<l n="32" num="1.32">Que sais-je ! quelques mois, un espace de temps</l>
					<l n="33" num="1.33">Beaucoup plus court, demain, après-demain peut-être,</l>
					<l n="34" num="1.34">Les yeux de mes amis ne pourront me connaître,</l>
					<l n="35" num="1.35">Tête de mort livide à mon tour. — Celle-ci</l>
					<l n="36" num="1.36">Est celle d’une femme autrefois morte ici,</l>
					<l n="37" num="1.37">Dont voilà le portrait qui, dans son cadre, semble</l>
					<l n="38" num="1.38">Vous regarder, sourire et remuer ; l’ensemble</l>
					<l n="39" num="1.39">De ses traits ingénus, de fraîcheur éclatants,</l>
					<l n="40" num="1.40">Montre qu’elle touchait à peine à son printemps.</l>
					<l n="41" num="1.41">Pourtant elle mourut ; bien des larmes coulèrent</l>
					<l n="42" num="1.42">Sans doute à son convoi, bien des fleurs s’effeuillèrent</l>
					<l n="43" num="1.43">Sur sa tombe, tributs de pieuses douleurs</l>
					<l n="44" num="1.44">Sans doute. — Mais le temps sait arrêter les pleurs,</l>
					<l n="45" num="1.45">Et, des premiers chagrins l’amertume passée,</l>
					<l n="46" num="1.46">Bientôt l’on oublia la belle trépassée.</l>
					<l n="47" num="1.47">— Belle, qui le dirait ? où sont ces cheveux blonds,</l>
					<l n="48" num="1.48">Qui roulent vers son col si soyeux et si longs ;</l>
					<l n="49" num="1.49">Cette joue aux contours ondoyants, aussi fraîche</l>
					<l n="50" num="1.50">Qu’au beau soleil d’été le duvet d’une pêche,</l>
					<l n="51" num="1.51">Ces lèvres de corail au sourire enfantin,</l>
					<l n="52" num="1.52">Ce front charmant à voir, cette peau de satin,</l>
					<l n="53" num="1.53">Où comme un fil d’azur transparaît chaque veine,</l>
					<l n="54" num="1.54">Ces yeux bleus que l’amour, passion creuse et vaine,</l>
					<l n="55" num="1.55">N’a jamais fait pleurer ? — Un crâne blanc et nu,</l>
					<l n="56" num="1.56">Deux trous noirs et profonds où l’œil fut contenu,</l>
					<l n="57" num="1.57">Une face sans nez, informe et grimaçante,</l>
					<l n="58" num="1.58">Du sort qui nous attend image menaçante ;</l>
					<l n="59" num="1.59">Voilà ce qu’il en reste avec un souvenir</l>
					<l n="60" num="1.60">Qui s’éteindra bientôt dans le vaste avenir.</l>
				</lg>
			</div></body></text></TEI>