<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU121">
				<head type="main">LES DEUX AGES</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
							La petite fille est devenue jeune fille.
							</quote>
							<bibl>
								<name>Victor Hugo.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Ce n’était, l’an passé, qu’une enfant blanche et blonde</l>
					<l n="2" num="1.2">Dont l’œil bleu, transparent et calme comme l’onde</l>
					<l n="3" num="1.3">Du lac qui réfléchit le ciel riant d’été,</l>
					<l n="4" num="1.4">N’exprimait que bonheur et naïve gaîté.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Que j’aimais dans le parc la voir sur la pelouse</l>
					<l n="6" num="2.2">Parmi ses jeunes sœurs courir, voler, jalouse</l>
					<l n="7" num="2.3">D’arriver la première ! Avec grâce les vents</l>
					<l n="8" num="2.4">Berçaient de ses cheveux les longs anneaux mouvants ;</l>
					<l n="9" num="2.5">Son écharpe d’azur se jouait autour d’elle</l>
					<l n="10" num="2.6">Par la course agitée, et, souvent infidèle,</l>
					<l n="11" num="2.7">Trahissait une épaule aux contours gracieux,</l>
					<l n="12" num="2.8">Un sein déjà gonflé, trésor mystérieux,</l>
					<l n="13" num="2.9">Un col éblouissant de fraîcheur, dont l’albâtre</l>
					<l n="14" num="2.10">Sous la peau laisse voir une veine bleuâtre,</l>
					<l n="15" num="2.11">— Dans son petit jardin que j’aimais à la voir</l>
					<l n="16" num="2.12">A grand’peine portant un léger arrosoir,</l>
					<l n="17" num="2.13">Distribuer en pluie, à ses fleurs desséchées</l>
					<l n="18" num="2.14">Par la chaleur du jour, et vers le sol penchées,</l>
					<l n="19" num="2.15">Une eau douce et limpide ; à ses oiseaux ravis,</l>
					<l n="20" num="2.16">Des tiges de plantain, des grains de chènevis !…</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1">C’est une jeune fille à présent blanche et blonde,</l>
					<l n="22" num="3.2">La même ; mais l’œil bleu, jadis pur comme l’onde</l>
					<l n="23" num="3.3">Du lac qui réfléchit le ciel riant d’été,</l>
					<l n="24" num="3.4">N’exprime plus bonheur et naïve gaîté.</l>
				</lg>
			</div></body></text></TEI>