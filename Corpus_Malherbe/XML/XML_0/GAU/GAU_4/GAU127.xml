<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU127">
				<head type="main">L’OISEAU CAPTIF</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Car quand il pleut et le soleil des cieux <lb></lb>
								Ne reluit point, tout homme est soucieux.
							</quote>
							<bibl>
								<name>Clément Marot.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								 ...... yet shall reascend <lb></lb>
								Self raised, and repossess its native seat.
							</quote>
							<bibl>
								<name>Lord Byron.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Depuis de si longs jours prisonnier, tu t’ennuies,</l>
					<l n="2" num="1.2">Pauvre oiseau, de ne voir qu’intarissables pluies,</l>
					<l n="3" num="1.3">De filets gris rayant un ciel noir et brumeux,</l>
					<l n="4" num="1.4">Que toits aigus baignés de nuages fumeux.</l>
					<l n="5" num="1.5">Aux gémissements sourds du vent d’hiver qui passe</l>
					<l n="6" num="1.6">Promenant la tourmente au milieu de l’espace,</l>
					<l n="7" num="1.7">Tu n’oses plus chanter : mais vienne le printemps</l>
					<l n="8" num="1.8">Avec son soleil d’or aux rayons éclatants,</l>
					<l n="9" num="1.9">Qui d’un regard bleuit l’émail du ciel limpide,</l>
					<l n="10" num="1.10">Ramène d’outre-mer l’hirondelle rapide,</l>
					<l n="11" num="1.11">Et jette sur les bois son manteau velouté,</l>
					<l n="12" num="1.12">Alors tu reprendras ta voix et ta gaîté ;</l>
					<l n="13" num="1.13">Et si, toujours constant à ta douleur austère,</l>
					<l n="14" num="1.14">Tu regrettais encor la forêt solitaire,</l>
					<l n="15" num="1.15">L’orme du grand chemin, le rocher, le buisson,</l>
					<l n="16" num="1.16">La campagne que dore une jaune moisson,</l>
					<l n="17" num="1.17">La rivière, le lac aux ondes transparentes,</l>
					<l n="18" num="1.18">Que plissent en passant les brises odorantes,</l>
					<l n="19" num="1.19">Je t’abandonnerais à ton joyeux essor.</l>
					<l n="20" num="1.20">Tous les deux cependant nous avons même sort,</l>
					<l n="21" num="1.21">Mon âme est comme toi : de sa cage mortelle</l>
					<l n="22" num="1.22">Elle s’ennuie, hélas ! et souffre, et bat de l’aile,</l>
					<l n="23" num="1.23">Elle voudrait planer dans l’océan du ciel,</l>
					<l n="24" num="1.24">Ange elle-même, suivre un ange Ithuriel,</l>
					<l n="25" num="1.25">S’enivrer d’infini, d’amour et de lumière,</l>
					<l n="26" num="1.26">Et remonter enfin à la cause première ;</l>
					<l n="27" num="1.27">Mais, grand Dieu ! quelle main ouvrira sa prison,</l>
					<l n="28" num="1.28">Quelle main à son vol livrera l’horizon ?</l>
				</lg>
			</div></body></text></TEI>