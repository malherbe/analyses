<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU79">
				<head type="main">CE MONDE-CI ET L’AUTRE</head>
				<lg n="1">
					<l n="1" num="1.1">Vos premières saisons à peine sont écloses,</l>
					<l n="2" num="1.2">Enfant, et vous avez déjà vu plus de choses</l>
					<l n="3" num="1.3">Qu’un vieillard qui trébuche au seuil de son tombeau ;</l>
					<l n="4" num="1.4">Tout ce que la nature a de grand et de beau,</l>
					<l n="5" num="1.5">Tout ce que Dieu nous fit de sublimes spectacles,</l>
					<l n="6" num="1.6">Les deux mondes ensemble avec tout leurs miracles :</l>
					<l n="7" num="1.7">Que n’avez-vous pas vu ? les montagnes, la mer,</l>
					<l n="8" num="1.8">La neige et les palmiers, le printemps et l’hiver,</l>
					<l n="9" num="1.9">L’Europe décrépite et la jeune Amérique :</l>
					<l n="10" num="1.10">Car votre peau cuivrée aux ardeurs du tropique,</l>
					<l n="11" num="1.11">Sous le soleil en flamme et les cieux toujours bleus,</l>
					<l n="12" num="1.12">S’est faite presque blanche à nos étés frileux.</l>
					<l n="13" num="1.13">Votre enfance joyeuse, a passé comme un rêve</l>
					<l n="14" num="1.14">Dans la verte savane et sur la blonde grève ;</l>
					<l n="15" num="1.15">Le vent vous apportait des parfums inconnus ;</l>
					<l n="16" num="1.16">Le sauvage Océan baisait vos beaux pieds nus,</l>
					<l n="17" num="1.17">Et comme une nourrice, au seuil de sa demeure,</l>
					<l n="18" num="1.18">Chante et jette un hochet au nouveau-né qui pleure,</l>
					<l n="19" num="1.19">Quand il vous voyait triste, il poussait devant vous</l>
					<l n="20" num="1.20">Ses coquilles de moire et son murmure doux.</l>
					<l n="21" num="1.21">Pour vous laisser passer, jam-roses et lianes</l>
					<l n="22" num="1.22">Écartaient dans les bois leurs rideaux diaphanes ;</l>
					<l n="23" num="1.23">Les tamaniers en fleurs vous prêtaient des abris ;</l>
					<l n="24" num="1.24">Vous aviez pour jouer des nids de colibris ;</l>
					<l n="25" num="1.25">Les papillons dorés vous éventaient de l’aile,</l>
					<l n="26" num="1.26">L’oiseau-mouche valsait avec la demoiselle ;</l>
					<l n="27" num="1.27">Les magnolias penchaient la tête en souriant ;</l>
					<l n="28" num="1.28">La fontaine au flot clair s’en allait babillant ;</l>
					<l n="29" num="1.29">Les bengalis coquets, se mirant à son onde,</l>
					<l n="30" num="1.30">Vous chantaient leur romance, et, seule et vagabonde,</l>
					<l n="31" num="1.31">Vous marchiez sans savoir par les petits chemins,</l>
					<l n="32" num="1.32">Un refrain à la bouche et des fleurs dans les mains !</l>
					<l n="33" num="1.33">Aux heures du midi, nonchalante créole,</l>
					<l n="34" num="1.34">Vous aviez le hamac et la sieste espagnole,</l>
					<l n="35" num="1.35">Et la bonne négresse aux dents blanches qui rit,</l>
					<l n="36" num="1.36">Chassant les moucherons d’auprès de votre lit.</l>
					<l n="37" num="1.37">Vous aviez tous les biens, heureuse créature,</l>
					<l n="38" num="1.38">La belle liberté dans la belle nature :</l>
					<l n="39" num="1.39">Et puis un grand désir d’inconnu vous a pris,</l>
					<l n="40" num="1.40">Vous avez voulu voir et la France et Paris ;</l>
					<l n="41" num="1.41">La brise a du vaisseau fait onder la bannière,</l>
					<l n="42" num="1.42">Le vieux monstre Océan, secouant sa crinière,</l>
					<l n="43" num="1.43">Et courbant devant vous sa tête de lion</l>
					<l n="44" num="1.44">Sur son épaule bleue avec soumission,</l>
					<l n="45" num="1.45">Vous a jusques aux bords de la France vantée,</l>
					<l n="46" num="1.46">Sans rugir une fois, fidèlement portée.</l>
					<l n="47" num="1.47">Après celles de Dieu les merveilles de l’art</l>
					<l n="48" num="1.48">Ont étonné votre âme avec votre regard.</l>
					<l n="49" num="1.49">Vous avez vu nos tours, nos palais, nos églises,</l>
					<l n="50" num="1.50">Nos monuments tout noirs et nos coupoles grises,</l>
					<l n="51" num="1.51">Nos beaux jardins royaux, où, de Grèce venus,</l>
					<l n="52" num="1.52">Étrangers comme vous, frissonnent les dieux nus,</l>
					<l n="53" num="1.53">Notre ciel morne et froid, notre horizon de brume,</l>
					<l n="54" num="1.54">Où chaque maison dresse une gueule qui fume.</l>
					<l n="55" num="1.55">Quel spectacle pour vous, ô fille du soleil !</l>
					<l n="56" num="1.56">Vous toute brune encor de son baiser vermeil.</l>
					<l n="57" num="1.57">La pluie a ruisselé sur vos vitres jaunies,</l>
					<l n="58" num="1.58">Et triste entre vos sœurs au foyer réunies,</l>
					<l n="59" num="1.59">En entendant pleurer les bûches dans le feu,</l>
					<l n="60" num="1.60">Vous avez regretté l’Amérique au ciel bleu,</l>
					<l n="61" num="1.61">Et la mer amoureuse avec ses tièdes lames,</l>
					<l n="62" num="1.62">Qui se brodent d’argent et chantent sous les rames ;</l>
					<l n="63" num="1.63">Les beaux lataniers verts, les palmiers chevelus,</l>
					<l n="64" num="1.64">Les mangliers traînant leurs bras irrésolus ;</l>
					<l n="65" num="1.65">Toute cette nature orientale et chaude,</l>
					<l n="66" num="1.66">Où chaque herbe flamboie et semble une émeraude,</l>
					<l n="67" num="1.67">Et vous avez souffert, votre cœur a saigné,</l>
					<l n="68" num="1.68">Vos yeux se sont levés vers ce ciel gris, baigné</l>
					<l n="69" num="1.69">D’une vapeur étrange et d’un brouillard de houille ;</l>
					<l n="70" num="1.70">Vers ces arbres chargés d’un feuillage de rouille,</l>
					<l n="71" num="1.71">Et vous avez compris, pâle fleur du désert,</l>
					<l n="72" num="1.72">Que loin du sol natal votre arôme se perd,</l>
					<l n="73" num="1.73">Qu’il vous faut le soleil et la blanche rosée</l>
					<l n="74" num="1.74">Dont vous étiez là-bas toute jeune arrosée ;</l>
					<l n="75" num="1.75">Les baisers parfumés des brises de la mer,</l>
					<l n="76" num="1.76">La place libre au ciel, l’espace et le grand air,</l>
					<l n="77" num="1.77">Et pour s’y renouer, l’hymne saint des poëtes,</l>
					<l n="78" num="1.78">Au fond de vous trouva des fibres toutes prêtes ;</l>
					<l n="79" num="1.79">Au chœur mélodieux votre voix put s’unir ;</l>
					<l n="80" num="1.80">Le prisme du regret dorant le souvenir</l>
					<l n="81" num="1.81">De cent petits détails, de mille circonstances,</l>
					<l n="82" num="1.82">Les vers naissaient en foule et se groupaient par stances.</l>
					<l n="83" num="1.83">Chaque larme furtive échappée à vos yeux</l>
					<l n="84" num="1.84">Se condensait en perle, en joyau précieux ;</l>
					<l n="85" num="1.85">Dans le rhythme profond, votre jeune pensée</l>
					<l n="86" num="1.86">Brillait plus savamment, chaque jour enchâssée ;</l>
					<l n="87" num="1.87">Vous avez pénétré les mystères de l’art ;</l>
					<l n="88" num="1.88">Aussi, tout éplorée, avant votre départ,</l>
					<l n="89" num="1.89">Pour vous baiser au front, la belle poésie</l>
					<l n="90" num="1.90">Vous a parmi vos sœurs avec amour choisie :</l>
					<l n="91" num="1.91">Pour dire votre cœur vous avez une voix,</l>
					<l n="92" num="1.92">Entre deux univers Dieu vous laissait le choix ;</l>
					<l n="93" num="1.93">Vous avez pris de l’un, heureux sort que le vôtre !</l>
					<l n="94" num="1.94">De quoi vous faire aimer et regretter dans l’autre.</l>
				</lg>
			</div></body></text></TEI>