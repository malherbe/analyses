<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU97">
				<head type="main">COMPENSATION</head>
				<lg n="1">
					<l n="1" num="1.1">Il naît sous le soleil de nobles créatures,</l>
					<l n="2" num="1.2">Unissant ici-bas tout ce qu’on peut rêver,</l>
					<l n="3" num="1.3">Corps de fer, cœur de flamme, admirables natures ;</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1">Dieu semble les produire afin de se prouver ;</l>
					<l n="5" num="2.2">Il prend, pour les pétrir, une argile plus douce,</l>
					<l n="6" num="2.3">Et souvent passe un siècle à les parachever.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">Il met, comme un sculpteur, l’empreinte de son pouce</l>
					<l n="8" num="3.2">Sur leurs fronts rayonnants de la gloire des cieux,</l>
					<l n="9" num="3.3">Et l’ardente auréole en gerbes d’or y pousse.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1">Ces hommes-là s’en vont, calmes et radieux,</l>
					<l n="11" num="4.2">Sans quitter un instant leur pose solennelle,</l>
					<l n="12" num="4.3">Avec l’œil immobile et le maintien des dieux.</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1">Leur moindre fantaisie est une œuvre éternelle,</l>
					<l n="14" num="5.2">Tout cède devant eux ; les sables inconstants,</l>
					<l n="15" num="5.3">Gardent leurs pas empreints, comme un airain fidèle.</l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1">Ne leur donnez qu’un jour ou donnez-leur cent ans,</l>
					<l n="17" num="6.2">L’orage ou le repos, la palette ou le glaive,</l>
					<l n="18" num="6.3">Ils mèneront à bout, leurs destins éclatants.</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1">Leur existence étrange est le réel du rêve ;</l>
					<l n="20" num="7.2">Ils exécuteront votre plan idéal,</l>
					<l n="21" num="7.3">Comme un maître savant le croquis d’un élève.</l>
				</lg>
				<lg n="8">
					<l n="22" num="8.1">Vos désirs inconnus, sous l’arceau triomphal,</l>
					<l n="23" num="8.2">Dont votre esprit en songe, arrondissait la voûte,</l>
					<l n="24" num="8.3">Passent assis en croupe au dos de leur cheval.</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1">D’un pied sûr, jusqu’au bout, ils ont suivi la route,</l>
					<l n="26" num="9.2">Où, dès les premiers pas, vous vous êtes assis,</l>
					<l n="27" num="9.3">N’osant prendre une branche au carrefour du doute.</l>
				</lg>
				<lg n="10">
					<l n="28" num="10.1">De ceux-là, chaque peuple en compte cinq ou six,</l>
					<l n="29" num="10.2">Cinq ou six, tout au plus, dans les siècles prospères,</l>
					<l n="30" num="10.3">Types toujours vivants dont on fait des récits.</l>
				</lg>
				<lg n="11">
					<l n="31" num="11.1">Nature avare ; ô toi ! si féconde en vipères,</l>
					<l n="32" num="11.2">En serpents, en crapauds tout gonflés de venins ;</l>
					<l n="33" num="11.3">Si prompte à repeupler tes immondes repaires ;</l>
				</lg>
				<lg n="12">
					<l n="34" num="12.1">Pour tant d’animaux vils, d’idiots et de nains,</l>
					<l n="35" num="12.2">Pour tant d’avortements et d’œuvres imparfaites,</l>
					<l n="36" num="12.3">Tant de monstres impurs échappés de tes mains ;</l>
				</lg>
				<lg n="13">
					<l n="37" num="13.1">Nature, tu nous dois encor bien des poëtes !</l>
				</lg>
			</div></body></text></TEI>