<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU96">
				<head type="main">QUI SERA ROI ?</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<head type="main">BÉHÉMOT</head>
					<lg n="1">
						<l n="1" num="1.1">Moi, je suis Béhémot, l’éléphant, le colosse.</l>
						<l n="2" num="1.2">Mon dos prodigieux, dans la plaine, fait bosse</l>
						<l n="3" num="1.3"><space quantity="12" unit="char"></space>Comme le dos d’un mont.</l>
						<l n="4" num="1.4">Je suis une montagne animée et qui marche :</l>
						<l n="5" num="1.5">Au déluge, je fis presque chavirer l’arche,</l>
						<l n="6" num="1.6">Et quand j’y mis le pied, l’eau monta jusqu’au pont.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Je porte, en me jouant, des tours sur mon épaule ;</l>
						<l n="8" num="2.2">Les murs tombent broyés sous mon flanc qui les frôle</l>
						<l n="9" num="2.3"><space quantity="12" unit="char"></space>Comme sous un bélier.</l>
						<l n="10" num="2.4">Quel est le bataillon que d’un choc je ne rompe ?</l>
						<l n="11" num="2.5">J’enlève cavaliers et chevaux dans ma trompe,</l>
						<l n="12" num="2.6">Et je les jette en l’air sans plus m’en soucier !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Les piques, sous mes pieds, se couchent comme l’herbe</l>
						<l n="14" num="3.2">Je jette à chaque pas, sur la terre, une gerbe</l>
						<l n="15" num="3.3"><space quantity="12" unit="char"></space>De blessés et de morts.</l>
						<l n="16" num="3.4">Au cœur de la bataille, aux lieux où la mêlée</l>
						<l n="17" num="3.5">Rugit plus furieuse et plus échevelée,</l>
						<l n="18" num="3.6">Comme un mortier sanglant, je vais gâchant les corps.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Les flèches font sur moi le pétillement grêle,</l>
						<l n="20" num="4.2">Que par un jour d’hiver font les grains de la grêle</l>
						<l n="21" num="4.3"><space quantity="12" unit="char"></space>Sur les tuiles d’un toit.</l>
						<l n="22" num="4.4">Les plus forts javelots, qui faussent les cuirasses,</l>
						<l n="23" num="4.5">Effleurent mon cuir noir sans y laisser de traces,</l>
						<l n="24" num="4.6">Et par tous les chemins je marche toujours droit.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Quand devant moi je trouve un arbre, je le casse ;</l>
						<l n="26" num="5.2">A travers les bambous, je folâtre et je passe</l>
						<l n="27" num="5.3">Comme un faon dans les blés.</l>
						<l n="28" num="5.4">Si je rencontre un fleuve en route, je le pompe,</l>
						<l n="29" num="5.5">Je dessèche son urne avec ma grande trompe,</l>
						<l n="30" num="5.6">Et laisse sur le sec ses hôtes écaillés.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Mes défenses d’ivoire éventreraient le monde,</l>
						<l n="32" num="6.2">Je porterais le ciel et sa coupole ronde</l>
						<l n="33" num="6.3"><space quantity="12" unit="char"></space>Tout aussi bien qu’Atlas.</l>
						<l n="34" num="6.4">Rien ne me semble lourd ; pour soutenir le pôle ;</l>
						<l n="35" num="6.5">Je pourrais lui prêter ma rude et forte épaule.</l>
						<l n="36" num="6.6">Je le remplacerai quand il sera trop las !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="37" num="1.1">Quand Béhémot eut dit jusqu’au bout sa harangue,</l>
						<l n="38" num="1.2">Léviathan, ainsi, répondit, en sa langue.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<head type="main">LÉVIATHAN</head>
					<lg n="1">
						<l n="39" num="1.1">Taisez-vous, Béhémot, je suis Léviathan ;</l>
						<l n="40" num="1.2">Comme un enfant mutin je fouette l’Océan</l>
						<l n="41" num="1.3"><space quantity="12" unit="char"></space>Du revers de ma large queue.</l>
						<l n="42" num="1.4">Mes vieux os sont plus durs que des barres d’airain,</l>
						<l n="43" num="1.5">Aussi Dieu m’a fait roi de l’univers marin,</l>
						<l n="44" num="1.6"><space quantity="12" unit="char"></space>Seigneur de l’immensité bleue.</l>
					</lg>
					<lg n="2">
						<l n="45" num="2.1">Le requin endenté d’un triple rang de dents,</l>
						<l n="46" num="2.2">Le dauphin monstrueux, aux longs fanons pendants,</l>
						<l n="47" num="2.3"><space quantity="12" unit="char"></space>Le kraken qu’on prend pour une île,</l>
						<l n="48" num="2.4">L’orque immense et difforme et le lourd cachalot,</l>
						<l n="49" num="2.5">Tout le peuple squameux qui laboure le flot,</l>
						<l n="50" num="2.6"><space quantity="12" unit="char"></space>Du cétacé jusqu’au nautile ;</l>
					</lg>
					<lg n="3">
						<l n="51" num="3.1">Le grand serpent de mer et le poisson Macar,</l>
						<l n="52" num="3.2">Les baleines du pôle, à l’œil rond et hagard,</l>
						<l n="53" num="3.3"><space quantity="12" unit="char"></space>Qui soufflent l’eau par la narine ;</l>
						<l n="54" num="3.4">Le triton fabuleux, la sirène aux chants clairs,</l>
						<l n="55" num="3.5">Sur le flanc d’un rocher, peignant ses cheveux verts</l>
						<l n="56" num="3.6"><space quantity="12" unit="char"></space>Et montrant sa blanche poitrine ;</l>
					</lg>
					<lg n="4">
						<l n="57" num="4.1">Les oursons étoilés et les crabes hideux,</l>
						<l n="58" num="4.2">Comme des coutelas agitant autour d’eux</l>
						<l n="59" num="4.3"><space quantity="12" unit="char"></space>L’arsenal crochu de leurs pinces ;</l>
						<l n="60" num="4.4">Tous, d’un commun accord, m’ont reconnu pour roi.</l>
						<l n="61" num="4.5">Dans leurs antres profonds, ils se cachent d’effroi</l>
						<l n="62" num="4.6"><space quantity="12" unit="char"></space>Quand je visite mes provinces.</l>
					</lg>
					<lg n="5">
						<l n="63" num="5.1">Pour l’œil qui peut plonger au fond du gouffre noir,</l>
						<l n="64" num="5.2">Mon royaume est superbe et magnifique à voir :</l>
						<l n="65" num="5.3"><space quantity="12" unit="char"></space>Des végétations étranges,</l>
						<l n="66" num="5.4">Éponges, polypiers, madrépores, coraux,</l>
						<l n="67" num="5.5">Comme dans les forêts, s’y courbent en arceaux,</l>
						<l n="68" num="5.6"><space quantity="12" unit="char"></space>S’y découpent en vertes franges.</l>
					</lg>
					<lg n="6">
						<l n="69" num="6.1">Le frisson de mon dos fait trembler l’Océan,</l>
						<l n="70" num="6.2">Ma respiration soulève l’ouragan</l>
						<l n="71" num="6.3"><space quantity="12" unit="char"></space>Et se condense en noirs nuages ;</l>
						<l n="72" num="6.4">Le souffle impétueux de mes larges naseaux,</l>
						<l n="73" num="6.5">Fait, comme un tourbillon, couler bas les vaisseaux</l>
						<l n="74" num="6.6"><space quantity="12" unit="char"></space>Avec les pâles équipages.</l>
					</lg>
					<lg n="7">
						<l n="75" num="7.1">Ainsi, vous avez tort de tant faire le fier ;</l>
						<l n="76" num="7.2">Pour avoir une peau plus dure que le fer</l>
						<l n="77" num="7.3"><space quantity="12" unit="char"></space>Et renversé quelque muraille ;</l>
						<l n="78" num="7.4">Ma gueule vous pourrait engloutir aisément.</l>
						<l n="79" num="7.5">Je vous ai regardé, Béhémot, et vraiment</l>
						<l n="80" num="7.6"><space quantity="12" unit="char"></space>Vous êtes de petite taille.</l>
					</lg>
					<lg n="8">
						<l n="81" num="8.1">L’empire revient donc à moi, prince des eaux ;</l>
						<l n="82" num="8.2">Qui mène chaque soir les difformes troupeaux</l>
						<l n="83" num="8.3"><space quantity="12" unit="char"></space>Paître dans les moites campagnes ;</l>
						<l n="84" num="8.4">Moi témoin du déluge et des temps disparus ;</l>
						<l n="85" num="8.5">Moi qui noyai jadis avec mes flots accrus</l>
						<l n="86" num="8.6"><space quantity="12" unit="char"></space>Les grands aigles sur les montagnes !</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">IV</head>
					<lg n="1">
						<l n="87" num="1.1">Léviathan se tut et plongea sous les flots ;</l>
						<l n="88" num="1.2">Ses flancs ronds reluisaient comme de noirs îlots.</l>
					</lg>
				</div>
				<div type="section" n="5">
					<head type="number">V</head>
					<head type="main">L’OISEAU ROCK</head>
					<lg n="1">
						<l n="89" num="1.1"><space quantity="12" unit="char"></space>Là bas, tout là bas, il me semble</l>
						<l n="90" num="1.2"><space quantity="12" unit="char"></space>Que j’entends quereller ensemble</l>
						<l n="91" num="1.3"><space quantity="12" unit="char"></space>Béhémot et Léviathan ;</l>
						<l n="92" num="1.4"><space quantity="12" unit="char"></space>Chacun des deux rivaux aspire,</l>
						<l n="93" num="1.5"><space quantity="12" unit="char"></space>Ambition folle, à l’empire</l>
						<l n="94" num="1.6"><space quantity="12" unit="char"></space>De la terre et de l’Océan.</l>
					</lg>
					<lg n="2">
						<l n="95" num="2.1"><space quantity="12" unit="char"></space>Eh quoi ! Léviathan l’énorme,</l>
						<l n="96" num="2.2"><space quantity="12" unit="char"></space>S’asseoirait, majesté difforme,</l>
						<l n="97" num="2.3"><space quantity="12" unit="char"></space>Sur le trône de l’univers !</l>
						<l n="98" num="2.4"><space quantity="12" unit="char"></space>N’a-t-il pas ses grottes profondes,</l>
						<l n="99" num="2.5"><space quantity="12" unit="char"></space>Son palais d’azur sous les ondes ?</l>
						<l n="100" num="2.6"><space quantity="12" unit="char"></space>N’est-il pas roi des peuples verts ?</l>
					</lg>
					<lg n="3">
						<l n="101" num="3.1"><space quantity="12" unit="char"></space>Béhémot, dans sa patte immonde,</l>
						<l n="102" num="3.2"><space quantity="12" unit="char"></space>Veut prendre le sceptre du monde</l>
						<l n="103" num="3.3"><space quantity="12" unit="char"></space>Et se poser en souverain.</l>
						<l n="104" num="3.4"><space quantity="12" unit="char"></space>Béhémot, avec son gros ventre,</l>
						<l n="105" num="3.5"><space quantity="12" unit="char"></space>Veut faire venir à son antre,</l>
						<l n="106" num="3.6"><space quantity="12" unit="char"></space>L’Univers terrestre et marin.</l>
					</lg>
					<lg n="4">
						<l n="107" num="4.1"><space quantity="12" unit="char"></space>La prétention est étrange</l>
						<l n="108" num="4.2"><space quantity="12" unit="char"></space>Pour ces deux pétrisseurs de fange,</l>
						<l n="109" num="4.3"><space quantity="12" unit="char"></space>Qui ne sauraient quitter le sol.</l>
						<l n="110" num="4.4"><space quantity="12" unit="char"></space>C’est moi, l’oiseau Rock, qui dois être,</l>
						<l n="111" num="4.5"><space quantity="12" unit="char"></space>De ce monde, seigneur et maître,</l>
						<l n="112" num="4.6"><space quantity="12" unit="char"></space>Et je suis roi de par mon vol.</l>
					</lg>
					<lg n="5">
						<l n="113" num="5.1"><space quantity="12" unit="char"></space>Je pourrais, dans ma forte serre,</l>
						<l n="114" num="5.2"><space quantity="12" unit="char"></space>Prendre la boule de la terre</l>
						<l n="115" num="5.3"><space quantity="12" unit="char"></space>Avec le ciel pour écusson.</l>
						<l n="116" num="5.4"><space quantity="12" unit="char"></space>Créez deux mondes ; je me flatte</l>
						<l n="117" num="5.5"><space quantity="12" unit="char"></space>D’en tenir un dans chaque patte,</l>
						<l n="118" num="5.6"><space quantity="12" unit="char"></space>Comme les aigles du blason.</l>
					</lg>
					<lg n="6">
						<l n="119" num="6.1"><space quantity="12" unit="char"></space>Je nage en plein dans la lumière,</l>
						<l n="120" num="6.2"><space quantity="12" unit="char"></space>Et ma prunelle sans paupière</l>
						<l n="121" num="6.3"><space quantity="12" unit="char"></space>Regarde en face le soleil.</l>
						<l n="122" num="6.4"><space quantity="12" unit="char"></space>Lorsque, par les airs, je voyage,</l>
						<l n="123" num="6.5"><space quantity="12" unit="char"></space>Mon ombre, comme un grand nuage,</l>
						<l n="124" num="6.6"><space quantity="12" unit="char"></space>Obscurcit l’horizon vermeil.</l>
					</lg>
					<lg n="7">
						<l n="125" num="7.1"><space quantity="12" unit="char"></space>Je cause avec l’étoile bleue</l>
						<l n="126" num="7.2"><space quantity="12" unit="char"></space>Et la comète à pâle queue ;</l>
						<l n="127" num="7.3"><space quantity="12" unit="char"></space>Dans la lune je fais mon nid ;</l>
						<l n="128" num="7.4"><space quantity="12" unit="char"></space>Je perche sur l’arc d’une sphère ;</l>
						<l n="129" num="7.5"><space quantity="12" unit="char"></space>D’un coup de mon aile légère,</l>
						<l n="130" num="7.6"><space quantity="12" unit="char"></space>Je fais le tour de l’infini.</l>
					</lg>
				</div>
				<div type="section" n="6">
					<head type="number">VI</head>
					<head type="main">L’HOMME</head>
					<lg n="1">
						<l n="131" num="1.1">Léviathan, je vais, malgré les deux cascades</l>
						<l n="132" num="1.2">Qui de tes noirs évents jaillissent en arcades ;</l>
						<l n="133" num="1.3">La mer qui se soulève à tes reniflements,</l>
						<l n="134" num="1.4">Et les glaces du pôle et tous les éléments,</l>
						<l n="135" num="1.5">Monté sur une barque entr’ouverte et disjointe,</l>
						<l n="136" num="1.6">T’enfoncer dans le flanc une mortelle pointe ;</l>
						<l n="137" num="1.7">Car il faut un peu d’huile à ma lampe le soir,</l>
						<l n="138" num="1.8">Quant le soleil s’éteint et qu’on n’y peut plus voir.</l>
						<l n="139" num="1.9">Béhémot, à genoux, que je pose la charge</l>
						<l n="140" num="1.10">Sur ta croupe arrondie et ton épaule large ;</l>
						<l n="141" num="1.11">Je ne suis pas ému de ton énormité ;</l>
						<l n="142" num="1.12">Je ferai de tes dents quelque hochet sculpté,</l>
						<l n="143" num="1.13">Et je te couperai tes immenses oreilles,</l>
						<l n="144" num="1.14">Avec leurs plis pendants, à des drapeaux pareilles</l>
						<l n="145" num="1.15">Pour en orner ma toque et gonfler mon chevet.</l>
						<l n="146" num="1.16">Oiseau Rock, prête-moi ta plume et ton duvet,</l>
						<l n="147" num="1.17">Mon plomb saura t’atteindre, et, l’aile fracassée,</l>
						<l n="148" num="1.18">Sans pouvoir achever la courbe commencée,</l>
						<l n="149" num="1.19">Des sommités du ciel, à mes pieds, sur le roc,</l>
						<l n="150" num="1.20">Tu tomberas tout droit, orgueilleux oiseau Rock.</l>
					</lg>
				</div>
			</div></body></text></TEI>