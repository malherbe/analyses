<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU86">
				<head type="main">CHANT DU GRILLON II</head>
				<lg n="1">
					<l n="1" num="1.1">Regardez les branches,</l>
					<l n="2" num="1.2">Comme elles sont blanches ;</l>
					<l n="3" num="1.3">Il neige des fleurs !</l>
					<l n="4" num="1.4">Riant dans la pluie,</l>
					<l n="5" num="1.5">Le soleil essuie</l>
					<l n="6" num="1.6">Les saules en pleurs,</l>
					<l n="7" num="1.7">Et le ciel reflète</l>
					<l n="8" num="1.8">Dans la violette,</l>
					<l n="9" num="1.9">Ses pures couleurs.</l>
				</lg>
				<lg n="2">
					<l n="10" num="2.1">La nature en joie</l>
					<l n="11" num="2.2">Se pare et déploie</l>
					<l n="12" num="2.3">Son manteau vermeil.</l>
					<l n="13" num="2.4">Le paon qui se joue,</l>
					<l n="14" num="2.5">Fait tourner en roue,</l>
					<l n="15" num="2.6">Sa queue au soleil.</l>
					<l n="16" num="2.7">Tout court, tout s’agite,</l>
					<l n="17" num="2.8">Pas un lièvre au gîte ;</l>
					<l n="18" num="2.9">L’ours sort du sommeil.</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1">La mouche ouvre l’aile,</l>
					<l n="20" num="3.2">Et la demoiselle</l>
					<l n="21" num="3.3">Aux prunelles d’or,</l>
					<l n="22" num="3.4">Au corset de guêpe,</l>
					<l n="23" num="3.5">Dépliant son crêpe,</l>
					<l n="24" num="3.6">A repris l’essor.</l>
					<l n="25" num="3.7">L’eau gaîment babille,</l>
					<l n="26" num="3.8">Le goujon frétille,</l>
					<l n="27" num="3.9">Un printemps encor !</l>
				</lg>
				<lg n="4">
					<l n="28" num="4.1">Tout se cherche et s’aime ;</l>
					<l n="29" num="4.2">Le crapaud lui-même,</l>
					<l n="30" num="4.3">Les aspics méchants ;</l>
					<l n="31" num="4.4">Toute créature,</l>
					<l n="32" num="4.5">Selon sa nature :</l>
					<l n="33" num="4.6">La feuille a des chants ;</l>
					<l n="34" num="4.7">Les herbes résonnent,</l>
					<l n="35" num="4.8">Les buissons bourdonnent ;</l>
					<l n="36" num="4.9">C’est concert aux champs.</l>
				</lg>
				<lg n="5">
					<l n="37" num="5.1">Moi seul je suis triste ;</l>
					<l n="38" num="5.2">Qui sait si j’existe,</l>
					<l n="39" num="5.3">Dans mon palais noir ?</l>
					<l n="40" num="5.4">Sous la cheminée,</l>
					<l n="41" num="5.5">Ma vie enchaînée,</l>
					<l n="42" num="5.6">Coule sans espoir.</l>
					<l n="43" num="5.7">Je ne puis, malade,</l>
					<l n="44" num="5.8">Chanter ma ballade</l>
					<l n="45" num="5.9">Aux hôtes du soir.</l>
				</lg>
				<lg n="6">
					<l n="46" num="6.1">Si la brise tiède</l>
					<l n="47" num="6.2">Au vent froid succède ;</l>
					<l n="48" num="6.3">Si le ciel est clair,</l>
					<l n="49" num="6.4">Moi, ma cheminée</l>
					<l n="50" num="6.5">N’est illuminée</l>
					<l n="51" num="6.6">Que d’un pâle éclair ;</l>
					<l n="52" num="6.7">Le cercle folâtre</l>
					<l n="53" num="6.8">Abandonne l’âtre :</l>
					<l n="54" num="6.9">Pour moi c’est l’hiver.</l>
				</lg>
				<lg n="7">
					<l n="55" num="7.1">Sur la cendre grise,</l>
					<l n="56" num="7.2">La pincette brise</l>
					<l n="57" num="7.3">Un charbon sans feu.</l>
					<l n="58" num="7.4">Adieu les paillettes,</l>
					<l n="59" num="7.5">Les blondes aigrettes ;</l>
					<l n="60" num="7.6">Pour six mois adieu</l>
					<l n="61" num="7.7">La maîtresse bûche,</l>
					<l n="62" num="7.8">Où sous la peluche,</l>
					<l n="63" num="7.9">Sifflait le gaz bleu.</l>
				</lg>
				<lg n="8">
					<l n="64" num="8.1">Dans ma niche creuse,</l>
					<l n="65" num="8.2">Ma natte boiteuse</l>
					<l n="66" num="8.3">Me tient en prison.</l>
					<l n="67" num="8.4">Quand l’insecte rôde,</l>
					<l n="68" num="8.5">Comme une émeraude,</l>
					<l n="69" num="8.6">Sous le vert gazon,</l>
					<l n="70" num="8.7">Moi seul je m’ennuie ;</l>
					<l n="71" num="8.8">Un mur, noir de suie,</l>
					<l n="72" num="8.9">Est mon horizon.</l>
				</lg>
			</div></body></text></TEI>