<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU103">
				<head type="main">LA BONNE JOURNÉE</head>
				<lg n="1">
					<l n="1" num="1.1">Ce jour, je l’ai passé ployé sur mon pupitre,</l>
					<l n="2" num="1.2">Sans jeter une fois l’œil à travers la vitre.</l>
					<l n="3" num="1.3">Par Apollo ! cent vers ; je devrais être las,</l>
					<l n="4" num="1.4">On le serait à moins ; mais je ne le suis pas ;</l>
					<l n="5" num="1.5">Je ne sais quelle joie intime et souveraine</l>
					<l n="6" num="1.6">Me fait le regard vif et la face sereine,</l>
					<l n="7" num="1.7">Comme après la rosée une petite fleur ;</l>
					<l n="8" num="1.8">Mon front se lève en haut avec moins de pâleur ;</l>
					<l n="9" num="1.9">Un sourire d’orgueil sur mes lèvres rayonne,</l>
					<l n="10" num="1.10">Et mon souffle pressé plus fortement résonne.</l>
					<l n="11" num="1.11">J’ai rempli mon devoir comme un brave ouvrier.</l>
					<l n="12" num="1.12">Rien ne m’a pu distraire ; en vain mon lévrier,</l>
					<l n="13" num="1.13">Entre mes deux genoux posant sa longue tête,</l>
					<l n="14" num="1.14">Semblait me dire : — En chasse ! en vain d’un air de fête</l>
					<l n="15" num="1.15">Le ciel tout bleu dardait, par le coin du carreau,</l>
					<l n="16" num="1.16">Un filet de soleil jusque sur mon bureau ;</l>
					<l n="17" num="1.17">Près de ma pipe, en vain, ma joyeuse bouteille</l>
					<l n="18" num="1.18">M’étalait son gros ventre et souriait vermeille ;</l>
					<l n="19" num="1.19">En vain ma bien-aimée, avec son beau sein nu,</l>
					<l n="20" num="1.20">Se penchait en riant de son rire ingénu ;</l>
					<l n="21" num="1.21">Sur mon fauteuil gothique, et dans ma chevelure</l>
					<l n="22" num="1.22">Répandait les parfums de son haleine pure.</l>
					<l n="23" num="1.23">Sourd comme saint Antoine à la tentation,</l>
					<l n="24" num="1.24">J’ai poursuivi mon œuvre avec religion ;</l>
					<l n="25" num="1.25">L’œuvre de mon amour qui mort me fera vivre,</l>
					<l n="26" num="1.26">Et ma journée ajoute un feuillet à mon livre.</l>
				</lg>
			</div></body></text></TEI>