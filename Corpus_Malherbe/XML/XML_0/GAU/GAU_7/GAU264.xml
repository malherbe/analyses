<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU264">
					<head type="main">STANCES</head>
					<lg n="1">
						<l n="1" num="1.1">Maintenant, — dans la plaine ou bien dans la montagne,</l>
						<l n="2" num="1.2">Chêne ou sapin, un arbre est en train de pousser,</l>
						<l n="3" num="1.3">En France, en Amérique, en Turquie, en Espagne,</l>
						<l n="4" num="1.4">Un arbre sous lequel un jour je puis passer.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Maintenant, — sur le seuil d’une pauvre chaumière,</l>
						<l n="6" num="2.2">Une femme, du pied agitant un berceau,</l>
						<l n="7" num="2.3">Sans se douter qu’elle est la parque filandière,</l>
						<l n="8" num="2.4">Allonge entre ses doigts l’étoupe d’un fuseau.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Maintenant, — loin du ciel à la splendeur divine,</l>
						<l n="10" num="3.2">Comme une taupe aveugle en son étroit couloir,</l>
						<l n="11" num="3.3">Pour arracher le fer au ventre de la mine,</l>
						<l n="12" num="3.4">Sous le sol des vivants plonge un travailleur noir.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Maintenant, — dans un coin du monde que j’ignore,</l>
						<l n="14" num="4.2">Il existe une place où le gazon fleurit,</l>
						<l n="15" num="4.3">Où le soleil joyeux boit les pleurs de l’aurore,</l>
						<l n="16" num="4.4">Où l’abeille bourdonne, où l’oiseau chante et rit.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Cet arbre qui soutient tant de nids sur ses branches,</l>
						<l n="18" num="5.2">Cet arbre épais et vert, frais et riant à l’œil,</l>
						<l n="19" num="5.3">Dans son tronc renversé l’on taillera des planches,</l>
						<l n="20" num="5.4">Les planches dont un jour on fera mon cercueil !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Cette étoupe qu’on file et qui, tissée en toile,</l>
						<l n="22" num="6.2">Donne une aile au vaisseau dans le port engourdi,</l>
						<l n="23" num="6.3">A l’orgie une nappe, à la pudeur un voile,</l>
						<l n="24" num="6.4">Linceul, revêtira mon cadavre verdi !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Ce fer que le mineur cherche au fond de la terre</l>
						<l n="26" num="7.2">Aux brumeuses clartés de son pâle fanal,</l>
						<l n="27" num="7.3">Hélas ! le forgeron quelque jour en doit faire</l>
						<l n="28" num="7.4">Le clou qui fermera le couvercle fatal !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">A cette même place où mille fois peut-être</l>
						<l n="30" num="8.2">J’allai m’asseoir, le cœur plein de rêves charmants,</l>
						<l n="31" num="8.3">S’entr’ouvrira le gouffre où je dois disparaître,</l>
						<l n="32" num="8.4">Pour descendre au séjour des épouvantements !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Manche</placeName>,
							<date when="1843">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>