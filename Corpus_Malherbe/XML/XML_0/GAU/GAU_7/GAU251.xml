<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU251">
					<head type="main">LA FONTAINE DU CIMETIÈRE</head>
					<lg n="1">
						<l n="1" num="1.1">A la morne Chartreuse, entre des murs de pierre,</l>
						<l n="2" num="1.2">En place du jardin l’on voit un cimetière,</l>
						<l n="3" num="1.3">Un cimetière nu comme un sillon fauché,</l>
						<l n="4" num="1.4">Sans croix, sans monument, sans tertre qui se hausse :</l>
						<l n="5" num="1.5">L’oubli couvre le nom, l’herbe couvre la fosse ;</l>
						<l n="6" num="1.6">La mère ignorerait où son fils est couché.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Les végétations maladives du cloître</l>
						<l n="8" num="2.2">Seules sur ce terrain peuvent germer et croître,</l>
						<l n="9" num="2.3">Dans l’humidité froide à l’ombre des longs murs ;</l>
						<l n="10" num="2.4">Des morts abandonnés douces consolatrices,</l>
						<l n="11" num="2.5">Les fleurs n’oseraient pas incliner leurs calices</l>
						<l n="12" num="2.6">Sur le vague tombeau de ces dormeurs obscurs.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Au milieu, deux cyprès à la noire verdure</l>
						<l n="14" num="3.2">Profilent tristement leur silhouette dure,</l>
						<l n="15" num="3.3">Longs soupirs de feuillage élancés vers les cieux,</l>
						<l n="16" num="3.4">Pendant que du bassin d’une avare fontaine</l>
						<l n="17" num="3.5">Tombe en frange effilée une nappe incertaine,</l>
						<l n="18" num="3.6">Comme des pleurs furtifs qui débordent des yeux.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Par les saints ossements des vieux moines filtrée,</l>
						<l n="20" num="4.2">L’eau coule à flots si clairs dans la vasque éplorée,</l>
						<l n="21" num="4.3">Que pour en boire un peu je m’approchai du bord.</l>
						<l n="22" num="4.4">Dans le cristal glacé quand je trempai ma lèvre,</l>
						<l n="23" num="4.5">Je me sentis saisi par un frisson de fièvre :</l>
						<l n="24" num="4.6">Cette eau de diamant avait un goût de mort !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1841">Cartuja de Miraflores, 1841</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>