<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
				<title type="sub">Poèmes absents des précédentes éditions</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>504 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">GAU_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Th._Gautier_qui_ne_figureront_pas_dans_ses_%C5%93uvres</idno>
						<p>Exporté de Wikisource le 26 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
								<author>Théophile Gautier</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k72036w</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Impr. particulière</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
				<p>Cette édition ne contient que les poèmes de Th. Gautier non inclus dans les précédentes éditions.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU375">
				<head type="main">XV DÉCEMBRE MDCCCXL</head>
				<opener>
					<dateline>
						<date when="1869">29 avril 1869</date>.
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Quand sous l’arc triomphal où s’inscrivent nos gloires</l>
					<l n="2" num="1.2">Passait le sombre char couronné de victoires</l>
					<l n="3" num="1.3"><space unit="char" quantity="12"></space>Aux longues ailes d’or,</l>
					<l n="4" num="1.4">Et qu’enfin Sainte-Hélène, après tant de souffrance,</l>
					<l n="5" num="1.5">Délivrait la grande ombre et rendait à la France</l>
					<l n="6" num="1.6"><space unit="char" quantity="12"></space>Son funèbre trésor,</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Un rêveur, un captif, derrière ses murailles,</l>
					<l n="8" num="2.2">Triste de ne pouvoir, aux saintes funérailles,</l>
					<l n="9" num="2.3"><space unit="char" quantity="12"></space>Assister, l’œil en pleurs,</l>
					<l n="10" num="2.4">Dans l’étroite prison, sans échos et muette,</l>
					<l n="11" num="2.5">Mêlant sa note émue à l’ode du poète,</l>
					<l n="12" num="2.6"><space unit="char" quantity="12"></space>Épanchait ses douleurs :</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">« Sire, vous revenez dans votre capitale,</l>
					<l n="14" num="3.2">Et moi, qu’en un cachot tient une loi fatale,</l>
					<l n="15" num="3.3"><space unit="char" quantity="12"></space>Exilé de Paris,</l>
					<l n="16" num="3.4">J’apercevrai de loin, comme sur une cime,</l>
					<l n="17" num="3.5">Le soleil descendant sur le cercueil sublime</l>
					<l n="18" num="3.6"><space unit="char" quantity="12"></space>Dans la foule aux longs cris.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Oh ! non ! n’en veuillez pas, sire, à votre famille</l>
					<l n="20" num="4.2">De n’avoir pas formé, sous le rayon qui brille,</l>
					<l n="21" num="4.3"><space unit="char" quantity="12"></space>Un groupe filial,</l>
					<l n="22" num="4.4">Pour recevoir, au seuil de son apothéose,</l>
					<l n="23" num="4.5">Comme Hercule ayant fait sa tâche grandiose,</l>
					<l n="24" num="4.6"><space unit="char" quantity="12"></space>L’ancêtre impérial !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Vos malheurs sont finis ; toujours durent les nôtres.</l>
					<l n="26" num="5.2">Vous êtes mort là-bas, enchaîné, loin des vôtres,</l>
					<l n="27" num="5.3"><space unit="char" quantity="12"></space>Titan sur un écueil ;</l>
					<l n="28" num="5.4">Pas de fils pour fermer vos yeux que l’ombre inonde ;</l>
					<l n="29" num="5.5">Même ici, nul parent, — oh ! misère profonde ! —</l>
					<l n="30" num="5.6"><space unit="char" quantity="12"></space>Conduisant votre deuil !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Montholon, le plus cher comme le plus fidèle,</l>
					<l n="32" num="6.2">Jusqu’au bout, du vautour affrontant les coups d’aile,</l>
					<l n="33" num="6.3"><space unit="char" quantity="12"></space>Vous a gardé sa foi ;</l>
					<l n="34" num="6.4">Près du dieu foudroyé, qu’un vaste ennui dévore,</l>
					<l n="35" num="6.5">Il se tenait debout, et même il est encore</l>
					<l n="36" num="6.6"><space unit="char" quantity="12"></space>En prison avec moi.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Un navire, conduit par un noble jeune homme,</l>
					<l n="38" num="7.2">Sous l’arbre où vous dormiez, Sire, votre long somme,</l>
					<l n="39" num="7.3"><space unit="char" quantity="12"></space>Captif dans le trépas,</l>
					<l n="40" num="7.4">Est allé vous chercher avec une escadrille ;</l>
					<l n="41" num="7.5">Mais votre œil sur le pont cherchait votre famille</l>
					<l n="42" num="7.6"><space unit="char" quantity="12"></space>Qui ne s’y trouvait pas.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Quand la nef aborda, France, ton sol antique,</l>
					<l n="44" num="8.2">Votre âme réveillée à ce choc électrique,</l>
					<l n="45" num="8.3"><space unit="char" quantity="12"></space>Au bruit des voix, des pas,</l>
					<l n="46" num="8.4">De sa prunelle d’ombre entrevit dans l’aurore,</l>
					<l n="47" num="8.5">Palpiter vaguement un drapeau tricolore</l>
					<l n="48" num="8.6"><space unit="char" quantity="12"></space>Où l’aigle n’était pas.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">Comme autrefois le peuple autour de vous s’empresse ;</l>
					<l n="50" num="9.2">Cris d’amour furieux, délirante tendresse,</l>
					<l n="51" num="9.3"><space unit="char" quantity="12"></space>À genoux, chapeau bas !</l>
					<l n="52" num="9.4">Dans l’acclamation, les prudents et les sages</l>
					<l n="53" num="9.5">Murmurent, au César faisant sa part d’hommages :</l>
					<l n="54" num="9.6"><space unit="char" quantity="12"></space>« Dieu ! ne l’éveillez pas ! »</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1">Vous les avez revus — peuple élu de votre âme —</l>
					<l n="56" num="10.2">Ces Français tant aimés que votre nom enflamme,</l>
					<l n="57" num="10.3"><space unit="char" quantity="12"></space>Héros des grands combats ;</l>
					<l n="58" num="10.4">Mais sur ton sol sacré, patrie autrefois crainte,</l>
					<l n="59" num="10.5">Du pas de l’étranger on distingue une empreinte</l>
					<l n="60" num="10.6"><space unit="char" quantity="12"></space>Qui ne s’efface pas.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1">Voyez la jeune armée, où les fils de nos braves,</l>
					<l n="62" num="11.2">Avides d’action, impatients d’entraves,</l>
					<l n="63" num="11.3"><space unit="char" quantity="12"></space>Voudraient presser le pas ;</l>
					<l n="64" num="11.4">Votre nom les émeut, car vous êtes la gloire ;</l>
					<l n="65" num="11.5">Mais on leur dit : « Laissez reposer la Victoire ;</l>
					<l n="66" num="11.6"><space unit="char" quantity="12"></space>Assez. Croisez les bras. »</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1">Sur le pays, le peuple, étoffe rude et forte,</l>
					<l n="68" num="12.2">S’étend comme un manteau qui vaillamment supporte</l>
					<l n="69" num="12.3"><space unit="char" quantity="12"></space>L’orage et les frimas ;</l>
					<l n="70" num="12.4">Mais ces grands si petits, chamarrés de dorures,</l>
					<l n="71" num="12.5">Qui cachent leur néant sous de riches parures,</l>
					<l n="72" num="12.6"><space unit="char" quantity="12"></space>Ne les regrettez pas.</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1">Comme ils ont renié, troupe au parjure agile,</l>
					<l n="74" num="13.2">Votre nom, votre sang, vos lois, votre évangile,</l>
					<l n="75" num="13.3"><space unit="char" quantity="12"></space>Pour vous suivre trop las !</l>
					<l n="76" num="13.4">Et quand j’ai devant eux parlé de votre cause,</l>
					<l n="77" num="13.5">Comme ils ont dit, tournés déjà vers autre chose :</l>
					<l n="78" num="13.6"><space unit="char" quantity="12"></space>« Nous ne comprenons pas. »</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1">Laissez-les dire et faire, et sur eux soit la honte !</l>
					<l n="80" num="14.2">Qu’importe pierre ou sable au char qui toujours monte</l>
					<l n="81" num="14.3"><space unit="char" quantity="12"></space>Et les broie en éclats ?</l>
					<l n="82" num="14.4">En vain vous nomment-ils « fugitif météore, »</l>
					<l n="83" num="14.5">Votre gloire est à nous, elle rayonne encore ;</l>
					<l n="84" num="14.6"><space unit="char" quantity="12"></space>Ils ne la prendront pas.</l>
				</lg>
				<lg n="15">
					<l n="85" num="15.1">Sire, c’est un grand jour que le quinze décembre !</l>
					<l n="86" num="15.2">Votre voix, est-ce un rêve ? a parlé dans ma chambre :</l>
					<l n="87" num="15.3"><space unit="char" quantity="12"></space>« Toi qui souffres pour moi,</l>
					<l n="88" num="15.4">Ami, de la prison le lent et dur martyre,</l>
					<l n="89" num="15.5">Je quitte mon triomphe et je viens pour te dire :</l>
					<l n="90" num="15.6"><space unit="char" quantity="12"></space>Je suis content de toi ! »</l>
				</lg>
				<p>
					Tout le monde connaît la pièce qui a inspiré ces vers ; cependant, peut-être nous saura-t-on gré de la reproduire.<lb></lb>
					<lb></lb>
					Citadelle de Ham, le 15 décembre 1840<lb></lb>
					<lb></lb>
					« Sire, <lb></lb>
					« Vous revenez dans votre capitale, et le peuple en foule salue votre retour ; mais moi, du fond de mon cachot, je ne puis apercevoir qu’un rayon du soleil qui éclaire vos funérailles.
					« N’en veuillez pas à votre famille de ce qu’elle n’est pas là pour vous recevoir. Votre exil et vos malheurs ont cessé avec votre vie ; mais les nôtres durent toujours ! Vous êtes mort sur un rocher, loin de la patrie et des vôtres ; la main d’un fils n’a point fermé vos yeux. Aujourd’hui encore aucun parent ne conduira votre deuil.
					« Montholon, lui que vous aimiez le plus parmi vos dévoués compagnons, vous a rendu les soins d’un fils : il est resté fidèle à votre pensée, à vos dernières volontés ; il m’a rapporté vos dernières paroles ; il est en prison avec moi !
					« Un vaisseau français, conduit par un noble jeune homme, est allé réclamer vos cendres ; mais c’est en vain que vous cherchiez sur le pont quelques-uns des vôtres ; votre famille n’y était pas.
					« En abordant au sol français, un choc électrique s’est fait sentir ; vous vous êtes soulevé dans votre cercueil ; vos yeux un moment se sont rouverts : le drapeau tricolore flottait sur le rivage, mais votre aigle n’y était pas.
					« Le peuple se presse comme autrefois sur votre passage, il vous salue de ses acclamations comme si vous étiez vivant ; mais les grands du jour, tout en vous rendant hommage, diront tout bas : « Dieu ! ne l’éveillez pas ! »
					« Vous avez enfin revu ces Français que vous aimiez tant ; vous êtes revenu dans cette France que vous avez rendue si grande ; mais l’étranger y a laissé des traces que toutes les pompes de votre retour n’effaceront pas !
					« Voyez cette jeune armée ; ce sont les fils de vos braves ; ils vous vénèrent, car vous êtes la gloire ; mais on leur dit : « Croisez vos bras ! »
					« Sire, le peuple, c’est la bonne étoffe qui couvre notre beau pays ; mais ces hommes que vous avez faits si grands, et qui étaient si petits, ah ! Sire, ne les regrettez pas.
					« Ils ont renié votre évangile, votre idée, votre gloire, votre sang ; quand je leur ai parlé de votre cause, il nous ont dit : Nous ne comprenons pas.
					« Laissez-les dire, laissez-les faire ; qu’importent, au char qui monte, les grains de sable qui se jettent sous les roues ! Ils ont beau dire que vous êtes un météore qui ne laisse pas de trace ; ils ont beau nier votre gloire civile ; ils ne vous déshériteront pas !
					« Sire, le 15 décembre est un grand jour pour la France et pour moi. Du milieu de votre somptueux cortège, dédaignant certains hommages, vous avez un instant jeté vos regards sur ma sombre demeure, et vous souvenant des caresses que vous prodiguiez à mon enfance, vous m’avez dit : « <hi rend="ital">Tu souffres pour moi, je suis content de toi.</hi> »
					<lb></lb>
					Louis-Napoléon.
					<lb></lb>
					Ces rimes françaises sur une matière à mettre en vers latins, et la prose qui les suit, sont une réimpression textuelle de la brochure : <hi rend="ital">XV Décembre MDCCCXL</hi> ; imp. du Journal officiel, MDCCCLXIX, A. Wittersheim et Cie, in-4, 16 p., papier vergé, tirée à 44 exemplaires, offerts à Napoléon III, disparus pour la plupart dans l’incendie des Tuileries.
				</p>
			</div></body></text></TEI>