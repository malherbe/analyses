<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU194">
				<head type="main">LA CHANSON DE MIGNON</head>
				<lg n="1">
					<l n="1" num="1.1">Ange de poésie, ô vierge blanche et blonde,</l>
					<l n="2" num="1.2">Tu me veux donc quitter et courir par le monde ?</l>
					<l n="3" num="1.3">Toi qui, voyant passer du seuil de la maison</l>
					<l n="4" num="1.4">Les nuages du soir sur le rouge horizon,</l>
					<l n="5" num="1.5">Contente d’admirer leurs beaux reflets de cuivre,</l>
					<l n="6" num="1.6">Ne t’es jamais surprise à les désirer suivre ;</l>
					<l n="7" num="1.7">Toi, même au ciel d’été, par le jour le plus bleu,</l>
					<l n="8" num="1.8">Frileuse Cendrillon, tapie au coin du feu,</l>
					<l n="9" num="1.9">Quel grand désir te prend, ô ma folle hirondelle !</l>
					<l n="10" num="1.10">D’abandonner le nid et de déployer l’aile ?</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1">Ah ! restons tous les deux près du foyer assis,</l>
					<l n="12" num="2.2">Restons ; je te ferai, petite, des récits,</l>
					<l n="13" num="2.3">Des contes merveilleux, à tenir ton oreille</l>
					<l n="14" num="2.4">Ouverte avec ton œil tout le temps de la veille.</l>
					<l n="15" num="2.5">Le vent râle et se plaint comme un agonisant ;</l>
					<l n="16" num="2.6">Le dogue réveillé hurle au bruit du passant ;</l>
					<l n="17" num="2.7">Il fait froid : c’est l’hiver ; la grêle à grand bruit fouette</l>
					<l n="18" num="2.8">Les carreaux palpitants ; la rauque girouette</l>
					<l n="19" num="2.9">Comme un hibou criaille au bord du toit pointu.</l>
					<l part="I" n="20" num="2.10">Où veux-tu donc aller ? </l>
				</lg>
				<lg n="3">
					<l part="F" n="20">O mon maître, sais-tu</l>
					<l n="21" num="3.1">La chanson que Mignon chante à Wilhelm dans Gœthe ?</l>
					<l n="22" num="3.2">«Ne la connais-tu pas la terre du poëte,</l>
					<l n="23" num="3.3">La terre du soleil où le citron mûrit,</l>
					<l n="24" num="3.4">Où l’orange aux tons d’or dans les feuilles sourit ?</l>
					<l n="25" num="3.5">C’est là, maître, c’est là qu’il faut mourir et vivre,</l>
					<l n="26" num="3.6">C’est là qu’il faut aller, c’est là qu’il me faut suivre.</l>
				</lg>
				<lg n="4">
					<l n="27" num="4.1">«Restons, enfant, restons : ce beau ciel toujours bleu,</l>
					<l n="28" num="4.2">Cette terre sans ombre et ce soleil de feu,</l>
					<l n="29" num="4.3">Brûleraient la peau blanche et ta chair diaphane.</l>
					<l n="30" num="4.4">La pâle violette au vent d’été se fane ;</l>
					<l n="31" num="4.5">Il lui faut la rosée et le gazon épais,</l>
					<l n="32" num="4.6">L’ombre de quelque saule, au bord d’un ruisseau frais ;</l>
					<l n="33" num="4.7">C’est une fleur du Nord, et telle est sa nature.</l>
					<l n="34" num="4.8">Fille du Nord comme elle, ô frêle créature !</l>
					<l n="35" num="4.9">Que ferais-tu là-bas sur le sol étranger ?</l>
					<l n="36" num="4.10">Ah ! la patrie est belle et l’on perd à changer.</l>
					<l part="I" n="37" num="4.11">Crois-moi, garde ton rêve. </l>
				</lg>
				<lg n="5">
					<l part="F" n="37">«Italie ! Italie !</l>
					<l n="38" num="5.1">Si riche et si dorée, oh ! comme ils t’ont salie !</l>
					<l n="39" num="5.2">Les pieds des nations ont battu tes chemins ;</l>
					<l n="40" num="5.3">Leur contact a limé tes vieux angles romains,</l>
					<l n="41" num="5.4">Les faux dilettanti s’érigeant en artistes,</l>
					<l n="42" num="5.5">Les riches ennuyés et les rimeurs touristes,</l>
					<l n="43" num="5.6">Les petits lords Byrons fondent de toutes parts</l>
					<l n="44" num="5.7">Sur ton cadavre à terre, ô mère des Césars !</l>
					<l n="45" num="5.8">Ils s’en vont mesurant la colonne et l’arcade ;</l>
					<l n="46" num="5.9">L’un se pâme au rocher et l’autre à la cascade :</l>
					<l n="47" num="5.10">Ce sont, à chaque pas, des admirations,</l>
					<l n="48" num="5.11">Des yeux levés en l’air et des contorsions.</l>
					<l n="49" num="5.12">Au moindre bloc informe et dévoré de mousse,</l>
					<l n="50" num="5.13">Au moindre pan de mur où le lentisque pousse,</l>
					<l n="51" num="5.14">On pleure d’aise, on tombe en des ravissements,</l>
					<l n="52" num="5.15">A faire de pitié rire les monuments.</l>
					<l n="53" num="5.16">L’un avec son lorgnon, collant le nez aux fresques,</l>
					<l n="54" num="5.17">Tâche de trouver beaux tes damnés gigantesques,</l>
					<l n="55" num="5.18">O pauvre Michel-Ange, et cherche en son cahier</l>
					<l n="56" num="5.19">Pour savoir si c’est là qu’il doit s’extasier ;</l>
					<l n="57" num="5.20">L’autre, plus amateur de ruines antiques,</l>
					<l n="58" num="5.21">Ne rêve que frontons, corniches et portiques,</l>
					<l n="59" num="5.22">Baise chaque pavé de la Via-Lata,</l>
					<l n="60" num="5.23">Ne croit qu’en Jupiter et jure par Vesta.</l>
					<l n="61" num="5.24">De mots italiens fardant leurs rimes blêmes,</l>
					<l n="62" num="5.25">Ceux-ci vont arrangeant leur voyage en poëmes,</l>
					<l n="63" num="5.26">Et sur de grands tableaux font de petits sonnets :</l>
					<l n="64" num="5.27">Artistes et dandys, roturiers, baronnets,</l>
					<l n="65" num="5.28">Chacun te tire aux dents, belle Italie antique,</l>
					<l n="66" num="5.29">Afin de remporter un pan de ta tunique !</l>
				</lg>
				<lg n="6">
					<l n="67" num="6.1">«Restons, car au retour on court risque souvent</l>
					<l n="68" num="6.2">De ne retrouver plus son vieux père vivant,</l>
					<l n="69" num="6.3">Et votre chien vous mord, ne sachant plus connaître</l>
					<l n="70" num="6.4">Dans l’étranger bruni celui qui fut son maître :</l>
					<l n="71" num="6.5">Les cœurs qui vous étaient ouverts se sont fermés,</l>
					<l n="72" num="6.6">D’autres en ont la clef, et, dans vos mieux aimés,</l>
					<l n="73" num="6.7">Il ne reste de vous qu’un vain nom qui s’efface.</l>
					<l n="74" num="6.8">Lorsque vous revenez vous n’avez plus de place :</l>
					<l n="75" num="6.9">Le monde où vous viviez s’est arrangé sans vous,</l>
					<l n="76" num="6.10">Et l’on a divisé votre part entre tous.</l>
					<l n="77" num="6.11">Vous êtes comme un mort qu’on croit au cimetière,</l>
					<l n="78" num="6.12">Et qui, rompant un soir le linceul et la bière,</l>
					<l n="79" num="6.13">Retourne à sa maison croyant trouver encor</l>
					<l n="80" num="6.14">Sa femme tout en pleurs et son coffre plein d’or ;</l>
					<l n="81" num="6.15">Mais sa femme a déjà comblé la place vide,</l>
					<l n="82" num="6.16">Et son or est aux mains d’un héritier avide ;</l>
					<l n="83" num="6.17">Ses amis sont changés, en sorte que le mort,</l>
					<l n="84" num="6.18">Voyant qu’il a mal fait et qu’il est dans son tort,</l>
					<l n="85" num="6.19">Ne demandera plus qu’à rentrer sous la terre</l>
					<l n="86" num="6.20">Pour dormir sans réveil dans son lit solitaire.</l>
					<l n="87" num="6.21">C’est le monde. Le cœur de l’homme est plein d’oubli :</l>
					<l n="88" num="6.22">C’est une eau qui remue et ne garde aucun pli.</l>
					<l n="89" num="6.23">L’herbe pousse moins vite aux pierres de la tombe</l>
					<l n="90" num="6.24">Qu’un autre amour dans l’âme, et la larme qui tombe</l>
					<l n="91" num="6.25">N’est pas séchée encor, que la bouche sourit,</l>
					<l n="92" num="6.26">Et qu’aux pages du cœur un autre nom s’écrit.</l>
				</lg>
				<lg n="7">
					<l n="93" num="7.1">«Restons pour être aimés, et pour qu’on se souvienne</l>
					<l n="94" num="7.2">Que nous sommes au monde ; il n’est amour qui tienne</l>
					<l n="95" num="7.3">Contre une longue absence : oh ! malheur aux absents !</l>
					<l n="96" num="7.4">Les absents sont des morts et, comme eux, impuissants.</l>
					<l n="97" num="7.5">Dès qu’aux yeux bien aimés votre vue est ravie,</l>
					<l n="98" num="7.6">Rien ne reste de vous qui prouve votre vie ;</l>
					<l n="99" num="7.7">Dès que l’on n’entend plus le son de votre voix,</l>
					<l n="100" num="7.8">Que l’on ne peut sentir le toucher de vos doigts,</l>
					<l n="101" num="7.9">Vous êtes mort ; vos traits se troublent et s’effacent</l>
					<l n="102" num="7.10">Au fond de la mémoire, et d’autres les remplacent.</l>
					<l n="103" num="7.11">Pour qu’on lui soit fidèle il faut que le ramier</l>
					<l n="104" num="7.12">Ne quitte pas le nid et vive au colombier.</l>
					<l n="105" num="7.13">Restons au colombier. Après tout, notre France</l>
					<l n="106" num="7.14">Vaut bien ton Italie, et, comme dans Florence,</l>
					<l n="107" num="7.15">Rome, Naple ou Venise, on peut trouver ici</l>
					<l n="108" num="7.16">De beaux palais à voir et des tableaux aussi.</l>
					<l n="109" num="7.17">Nous avons des donjons, de vieilles cathédrales</l>
					<l n="110" num="7.18">Aussi haut que Saint-Pierre élevant leurs spirales ;</l>
					<l n="111" num="7.19">Notre-Dame tendant ses deux grands bras en croix,</l>
					<l n="112" num="7.20">Saint-Severin dardant sa flèche entre les toits,</l>
					<l n="113" num="7.21">Et la Sainte-Chapelle aux minarets mauresques,</l>
					<l n="114" num="7.22">Et Saint-Jacques hurlant sous ses monstres grotesques ;</l>
					<l n="115" num="7.23">Nous avons de grands bois et des oiseaux chanteurs,</l>
					<l n="116" num="7.24">Des fleurs embaumant l’air de divines senteurs,</l>
					<l n="117" num="7.25">Des ruisseaux babillards dans de belles prairies,</l>
					<l n="118" num="7.26">Où l’on peut suivre en paix ses chères rêveries ;</l>
					<l n="119" num="7.27">Nous avons, nous aussi, des fruits blonds comme miel,</l>
					<l n="120" num="7.28">Des archipels d’argent aux flots de notre ciel,</l>
					<l n="121" num="7.29">Et ce qui ne se trouve en aucun lieu du monde,</l>
					<l n="122" num="7.30">Ce qui vaut mieux que tout, ô belle vagabonde,</l>
					<l n="123" num="7.31">Le foyer domestique, ineffable en douceurs,</l>
					<l n="124" num="7.32">Avec la mère au coin et les petites sœurs,</l>
					<l n="125" num="7.33">Et le chat familier qui se joue et se roule,</l>
					<l n="126" num="7.34">Et, pour hâter le temps quand goutte à goutte il coule,</l>
					<l n="127" num="7.35">Quelques anciens amis causant de vers et d’art,</l>
					<l n="128" num="7.36">Qui viennent de bonne heure et ne s’en vont que tard.»</l>
				</lg>
				<closer>
					<dateline>
						<date when="1833">1833</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>