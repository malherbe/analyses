<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU193">
				<head type="main">PENSÉE DE MINUIT</head>
				<lg n="1">
					<l n="1" num="1.1">Une minute encor, madame, et cette année,</l>
					<l n="2" num="1.2">Commencée avec vous, avec vous terminée,</l>
					<l n="3" num="1.3"><space quantity="4" unit="char"></space>Ne sera plus qu’un souvenir.</l>
					<l n="4" num="1.4">Minuit : voilà son glas que la pendule sonne,</l>
					<l n="5" num="1.5">Elle s’en est allée en un lieu d’où personne</l>
					<l n="6" num="1.6"><space quantity="4" unit="char"></space>Ne peut la faire revenir :</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Quelque part, loin, bien loin, par delà les étoiles.</l>
					<l n="8" num="2.2">Dans un pays sans nom, ombreux et plein de voiles.</l>
					<l n="9" num="2.3"><space quantity="4" unit="char"></space>Sur le bord du néant jeté ;</l>
					<l n="10" num="2.4">Limbes de l’impalpable, invisible royaume</l>
					<l n="11" num="2.5">Où va ce qui n’a pas de corps ni de fantôme,</l>
					<l n="12" num="2.6"><space quantity="4" unit="char"></space>Ce qui n’est rien ayant été ;</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Où va le son, où va le souffle, où va la flamme,</l>
					<l n="14" num="3.2">La vision qu’en rêve on perçoit avec l’âme,</l>
					<l n="15" num="3.3"><space quantity="4" unit="char"></space>L’amour de notre cœur chassé ;</l>
					<l n="16" num="3.4">La pensée inconnue éclose en notre tête ;</l>
					<l n="17" num="3.5">L’ombre qu’en s’y mirant dans la glace on projette ;</l>
					<l n="18" num="3.6"><space quantity="4" unit="char"></space>Le présent qui se fait passé ;</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Un à-compte d’un an pris sur les ans qu’à vivre</l>
					<l n="20" num="4.2">Dieu veut bien nous prêter ; une feuille du livre</l>
					<l n="21" num="4.3"><space quantity="4" unit="char"></space>Tournée avec le doigt du temps ;</l>
					<l n="22" num="4.4">Une scène nouvelle à rajouter au drame,</l>
					<l n="23" num="4.5">Un chapitre de plus au roman dont la trame</l>
					<l n="24" num="4.6"><space quantity="4" unit="char"></space>S’embrouille d’instants en instants ;</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Un autre pas de fait dans cette route morne,</l>
					<l n="26" num="5.2">De la vie et du temps, dont la dernière borne,</l>
					<l n="27" num="5.3"><space quantity="4" unit="char"></space>Proche ou lointaine, est un tombeau ;</l>
					<l n="28" num="5.4">Où l’on ne peut poser le pied qu’il ne s’enfonce ;</l>
					<l n="29" num="5.5">Où de votre bonheur toujours à chaque ronce</l>
					<l n="30" num="5.6"><space quantity="4" unit="char"></space>Derrière vous reste un lambeau.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Du haut de cette année avec labeur gravie,</l>
					<l n="32" num="6.2">Me tournant vers ce moi qui n’est plus dans ma vie</l>
					<l n="33" num="6.3"><space quantity="4" unit="char"></space>Qu’un souvenir presque effacé,</l>
					<l n="34" num="6.4">Avant qu’il ne se plonge au sein de l’ombre noire,</l>
					<l n="35" num="6.5">Je contemple un moment, des yeux de la mémoire,</l>
					<l n="36" num="6.6"><space quantity="4" unit="char"></space>Le vaste horizon du passé.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Ainsi le voyageur, du haut de la colline,</l>
					<l n="38" num="7.2">Avant que tout à fait le versant qui s’incline</l>
					<l n="39" num="7.3"><space quantity="4" unit="char"></space>Ne les dérobe à son regard,</l>
					<l n="40" num="7.4">Jette un dernier coup d’œil sur les campagnes bleues</l>
					<l n="41" num="7.5">Qu’il vient de parcourir, comptant combien de lieues</l>
					<l n="42" num="7.6"><space quantity="4" unit="char"></space>Il a fait depuis son départ.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Mes ans évanouis à mes pieds se déploient</l>
					<l n="44" num="8.2">Comme une plaine obscure où quelques points chatoient</l>
					<l n="45" num="8.3"><space quantity="4" unit="char"></space>D’un rayon de soleil frappés :</l>
					<l n="46" num="8.4">Sur les plans éloignés qu’un brouillard d’oubli cache,</l>
					<l n="47" num="8.5">Une époque, un détail nettement se détache</l>
					<l n="48" num="8.6"><space quantity="4" unit="char"></space>Et revit à mes yeux trompés.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">Ce qui fut moi jadis m’apparaît : silhouette</l>
					<l n="50" num="9.2">Qui ne ressemble plus au moi qu’elle répète ;</l>
					<l n="51" num="9.3"><space quantity="4" unit="char"></space>Portrait sans modèle aujourd’hui ;</l>
					<l n="52" num="9.4">Spectre dont le cadavre est vivant ; ombre morte</l>
					<l n="53" num="9.5">Que le passé ravit au présent qu’il emporte ;</l>
					<l n="54" num="9.6"><space quantity="4" unit="char"></space>Reflet dont le corps s’est enfui.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1">J’hésite en me voyant devant moi reparaître,</l>
					<l n="56" num="10.2">Hélas ! et j’ai souvent peine à me reconnaître</l>
					<l n="57" num="10.3">Sous ma figure d’autrefois.</l>
					<l n="58" num="10.4">Comme un homme qu’on met tout à coup en présence</l>
					<l n="59" num="10.5">De quelque ancien ami dont l’âge et dont l’absence</l>
					<l n="60" num="10.6"><space quantity="4" unit="char"></space>Ont changé les traits et la voix.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1">Tant de choses depuis par cette pauvre tête,</l>
					<l n="62" num="11.2">Ont passé ! dans cette âme et ce cœur de poëte,</l>
					<l n="63" num="11.3"><space quantity="4" unit="char"></space>Comme dans l’aire des aiglons,</l>
					<l n="64" num="11.4">Tant d’œuvres que couva l’aile de ma pensée</l>
					<l n="65" num="11.5">Se débattent, heurtant leur coquille brisée</l>
					<l n="66" num="11.6"><space quantity="4" unit="char"></space>Avec leurs ongles déjà longs !</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1">Je ne suis plus le même : âme et corps, tout diffère ;</l>
					<l n="68" num="12.2">Hors le nom, rien de moi n’est resté ; mais qu’y faire ?</l>
					<l n="69" num="12.3"><space quantity="4" unit="char"></space>Marcher en avant, oublier.</l>
					<l n="70" num="12.4">On ne peut sur le temps reprendre une minute,</l>
					<l n="71" num="12.5">Ni faire remonter un grain après sa chute</l>
					<l n="72" num="12.6"><space quantity="4" unit="char"></space>Au fond du fatal sablier.</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1">La tête de l’enfant n’est plus dans cette tête</l>
					<l n="74" num="13.2">Maigre, décolorée, ainsi que me l’ont faite</l>
					<l n="75" num="13.3"><space quantity="4" unit="char"></space>L’étude austère et les soucis.</l>
					<l n="76" num="13.4">Vous n’en trouveriez rien sur ce front qui médite</l>
					<l n="77" num="13.5">Et dont quelque tourmente intérieure agite</l>
					<l n="78" num="13.6"><space quantity="4" unit="char"></space>Comme deux serpents les sourcils.</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1">Ma joue était sans plis, toute rose, et ma lèvre</l>
					<l n="80" num="14.2">Aux coins toujours arqués riait ; jamais la fièvre</l>
					<l n="81" num="14.3"><space quantity="4" unit="char"></space>N’en avait noirci le corail.</l>
					<l n="82" num="14.4">Mes yeux, vierges de pleurs, avaient des étincelles</l>
					<l n="83" num="14.5">Qu’ils n’ont plus maintenant, et leurs claires prunelles</l>
					<l n="84" num="14.6"><space quantity="4" unit="char"></space>Doublaient le ciel dans leur émail.</l>
				</lg>
				<lg n="15">
					<l n="85" num="15.1">Mon cœur avait mon âge, il ignorait la vie ;</l>
					<l n="86" num="15.2">Aucune illusion, amèrement ravie,</l>
					<l n="87" num="15.3"><space quantity="4" unit="char"></space>Jeune, ne l’avait rendu vieux ;</l>
					<l n="88" num="15.4">Il s’épanouissait à toute chose belle,</l>
					<l n="89" num="15.5">Et, dans cette existence encor pour lui nouvelle,</l>
					<l n="90" num="15.6"><space quantity="4" unit="char"></space>Le mal était bien, le bien mieux.</l>
				</lg>
				<lg n="16">
					<l n="91" num="16.1">Ma poésie, enfant à la grâce ingénue,</l>
					<l n="92" num="16.2">Les cheveux dénoués, sans corset, jambe nue,</l>
					<l n="93" num="16.3"><space quantity="4" unit="char"></space>Un brin de folle avoine en main,</l>
					<l n="94" num="16.4">Avec son collier fuit de perles de rosée,</l>
					<l n="95" num="16.5">Sa robe prismatique au soleil irisée,</l>
					<l n="96" num="16.6"><space quantity="4" unit="char"></space>Allait chantant par le chemin.</l>
				</lg>
				<lg n="17">
					<l n="97" num="17.1">Et puis l’âge est venu qui donne la science,</l>
					<l n="98" num="17.2">J’ai lu Werther, René, son frère d’alliance ;</l>
					<l n="99" num="17.3"><space quantity="4" unit="char"></space>Ces livres, vrais poisons du cœur,</l>
					<l n="100" num="17.4">Qui déflorent la vie et nous dégoûtent d’elle,</l>
					<l n="101" num="17.5">Dont chaque mot vous porte une atteinte mortelle ;</l>
					<l n="102" num="17.6"><space quantity="4" unit="char"></space>Byron et son don Juan moqueur.</l>
				</lg>
				<lg n="18">
					<l n="103" num="18.1">Ce fut un dur réveil : ayant vu que les songes</l>
					<l n="104" num="18.2">Dont je m’étais bercé n’étaient que des mensonges,</l>
					<l n="105" num="18.3"><space quantity="4" unit="char"></space>Les croyances, des hochets creux,</l>
					<l n="106" num="18.4">Je cherchai la gangrène au fond de tout, et, comme</l>
					<l n="107" num="18.5">Je la trouvai toujours, je pris en haine l’homme,</l>
					<l n="108" num="18.6"><space quantity="4" unit="char"></space>Et je devins bien malheureux.</l>
				</lg>
				<lg n="19">
					<l n="109" num="19.1">La pensée et la forme ont passé comme un rêve.</l>
					<l n="110" num="19.2">Mais que fait donc le temps de ce qu’il nous enlève ?</l>
					<l n="111" num="19.3"><space quantity="4" unit="char"></space>Dans quel coin du chaos met-il</l>
					<l n="112" num="19.4">Ces aspects oubliés comme l’habit qu’on change,</l>
					<l n="113" num="19.5">Tous ces moi du même homme ? et quel royaume étrange</l>
					<l n="114" num="19.6"><space quantity="4" unit="char"></space>Leur sert de patrie ou d’exil ?</l>
				</lg>
				<lg n="20">
					<l n="115" num="20.1">Dieu seul peut le savoir ; c’est un profond mystère ;</l>
					<l n="116" num="20.2">Nous le saurons peut-être à la fin, car la terre</l>
					<l n="117" num="20.3"><space quantity="4" unit="char"></space>Que la pioche jette au cercueil</l>
					<l n="118" num="20.4">Avec sa sombre voix explique bien des choses ;</l>
					<l n="119" num="20.5">Des effets, dans la tombe, on comprend mieux les causes.</l>
					<l n="120" num="20.6"><space quantity="4" unit="char"></space>L’éternité commence au seuil.</l>
				</lg>
				<lg n="21">
					<l n="121" num="21.1">L’on voit… Mais veuillez bien me pardonner, madame,</l>
					<l n="122" num="21.2">De vous entretenir de tout cela. Mon âme,</l>
					<l n="123" num="21.3"><space quantity="4" unit="char"></space>Ainsi qu’un vase trop rempli,</l>
					<l n="124" num="21.4">Déborde, laissant choir mille vagues pensées,</l>
					<l n="125" num="21.5">Et ces ressouvenirs d’illusions passées</l>
					<l n="126" num="21.6"><space quantity="4" unit="char"></space>Rembrunissent mon front pâli.</l>
				</lg>
				<lg n="22">
					<l n="127" num="22.1">Eh ! que vous fait cela, dites-vous, tête folle,</l>
					<l n="128" num="22.2">De vous inquiéter d’une ombre qui s’envole ?</l>
					<l n="129" num="22.3"><space quantity="4" unit="char"></space>Pourquoi donc vouloir retenir,</l>
					<l n="130" num="22.4">Comme un enfant mutin, sa mère par la robe,</l>
					<l n="131" num="22.5">Ce passé qui s’en va ? De ce qu’il vous dérobe</l>
					<l n="132" num="22.6"><space quantity="4" unit="char"></space>Consolez-vous par l’avenir.</l>
				</lg>
				<lg n="23">
					<l n="133" num="23.1">Regardez ; devant vous l’horizon est immense.</l>
					<l n="134" num="23.2">C’est l’aube de la vie, et votre jour commence ;</l>
					<l n="135" num="23.3"><space quantity="4" unit="char"></space>Le ciel est bleu, le soleil luit.</l>
					<l n="136" num="23.4">La route de ce monde est pour vous une allée,</l>
					<l n="137" num="23.5">Comme celle d’un parc, pleine d’ombre et sablée :</l>
					<l n="138" num="23.6"><space quantity="4" unit="char"></space>Marchez où le temps vous conduit.</l>
				</lg>
				<lg n="24">
					<l n="139" num="24.1">Que voulez-vous de plus ? tout vous rit, l’on vous aime.</l>
					<l n="140" num="24.2">Oh ! vous avez raison, je me le dis moi-même,</l>
					<l n="141" num="24.3"><space quantity="4" unit="char"></space>L’avenir devrait m’être cher ;</l>
					<l n="142" num="24.4">Mais c’est en vain, hélas ! que votre voix m’exhorte ;</l>
					<l n="143" num="24.5">Je rêve, et mon baiser à votre front avorte,</l>
					<l n="144" num="24.6"><space quantity="4" unit="char"></space>Et je me sens le cœur amer.</l>
				</lg>
			</div></body></text></TEI>