<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU225">
				<head type="main">LE SOMMET DE LA TOUR</head>
				<lg n="1">
					<l n="1" num="1.1">Lorsque l’on veut monter aux tours des cathédrales,</l>
					<l n="2" num="1.2">On prend l’escalier noir qui roule ses spirales,</l>
					<l n="3" num="1.3">Comme un serpent de pierre au ventre d’un clocher.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1">L’on chemine d’abord dans une nuit profonde,</l>
					<l n="5" num="2.2">Sans trèfle de soleil et de lumière blonde,</l>
					<l n="6" num="2.3">Tâtant le mur des mains, de peur de trébucher ;</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">Car les hautes maisons voisines de l’église</l>
					<l n="8" num="3.2">Vers le pied de la tour versent leur ombre grise,</l>
					<l n="9" num="3.3">Qu’un rayon lumineux ne vient jamais trancher.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1">S’envolant tout à coup, les chouettes peureuses</l>
					<l n="11" num="4.2">Vous flagellent le front de leurs ailes poudreuses,</l>
					<l n="12" num="4.3">Et les chauves-souris s’abattent sur vos bras :</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1">Les spectres, les terreurs qui hantent les ténèbres,</l>
					<l n="14" num="5.2">Vous frôlent en passant de leurs crêpes funèbres ;</l>
					<l n="15" num="5.3">Vous les entendez geindre et chuchoter tout bas.</l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1">A travers l’ombre on voit la chimère accroupie</l>
					<l n="17" num="6.2">Remuer, et l’écho de la voûte assoupie</l>
					<l n="18" num="6.3">Derrière votre pas suscite un autre pas.</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1">Vous sentez à l’épaule une pénible haleine,</l>
					<l n="20" num="7.2">Un souffle intermittent, comme d’une âme en peine</l>
					<l n="21" num="7.3">Qu’on aurait éveillée et qui vous poursuivrait ;</l>
				</lg>
				<lg n="8">
					<l n="22" num="8.1">Et si l’humidité fait, des yeux de la voûte,</l>
					<l n="23" num="8.2">Larmes du monument, tomber l’eau goutte à goutte,</l>
					<l n="24" num="8.3">Il semble qu’on dérange une ombre qui pleurait.</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1">Chaque fois que la vis, en tournant, se dérobe,</l>
					<l n="26" num="9.2">Sur la dernière marche un dernier pli de robe,</l>
					<l n="27" num="9.3">Irritante terreur, brusquement disparaît.</l>
				</lg>
				<lg n="10">
					<l n="28" num="10.1">Bientôt le jour, filtrant par les fentes étroites,</l>
					<l n="29" num="10.2">Sur le mur opposé trace des lignes droites,</l>
					<l n="30" num="10.3">Comme une barre d’or sur un écusson noir.</l>
				</lg>
				<lg n="11">
					<l n="31" num="11.1">L’on est déjà plus haut que les toits de la ville,</l>
					<l n="32" num="11.2">Édifices sans nom, masse confuse et vile,</l>
					<l n="33" num="11.3">Et par les arceaux gris le ciel bleu se fait voir.</l>
				</lg>
				<lg n="12">
					<l n="34" num="12.1">Les hiboux disparus font place aux tourterelles,</l>
					<l n="35" num="12.2">Qui lustrent au soleil le satin de leurs ailes</l>
					<l n="36" num="12.3">Et semblent roucouler des promesses d’espoir.</l>
				</lg>
				<lg n="13">
					<l n="37" num="13.1">Des essaims familiers perchent sur les tarasques,</l>
					<l n="38" num="13.2">Et, sans se rebuter de la laideur des masques,</l>
					<l n="39" num="13.3">Dans chaque bouche ouverte un oiseau fait son nid.</l>
				</lg>
				<lg n="14">
					<l n="40" num="14.1">Les guivres, les dragons et les formes étranges</l>
					<l n="41" num="14.2">Ne sont plus maintenant que des figures d’anges,</l>
					<l n="42" num="14.3">Séraphiques gardiens taillés dans le granit,</l>
				</lg>
				<lg n="15">
					<l n="43" num="15.1">Qui depuis huit cents ans, pensives sentinelles,</l>
					<l n="44" num="15.2">Dans leurs niches de pierre, appuyés sur leurs ailes,</l>
					<l n="45" num="15.3">Montent leur faction qui jamais ne finit.</l>
				</lg>
				<lg n="16">
					<l n="46" num="16.1">Vous débouchez enfin sur une plate-forme,</l>
					<l n="47" num="16.2">Et vous apercevez, ainsi qu’un monstre énorme,</l>
					<l n="48" num="16.3">La Cité grommelante, accroupie alentour.</l>
				</lg>
				<lg n="17">
					<l n="49" num="17.1">Comme un requin, ouvrant ses immenses mâchoires,</l>
					<l n="50" num="17.2">Elle mord l’horizon de ses mille dents noires,</l>
					<l n="51" num="17.3">Dont chacune est un dôme, un clocher, une tour.</l>
				</lg>
				<lg n="18">
					<l n="52" num="18.1">A travers le brouillard, de ses naseaux de plâtre,</l>
					<l n="53" num="18.2">Elle souffle dans l’air son haleine bleuâtre,</l>
					<l n="54" num="18.3">Que dore par flocons un chaud reflet de jour.</l>
				</lg>
				<lg n="19">
					<l n="55" num="19.1">Comme sur l’eau qui bout monte et chante l’écume,</l>
					<l n="56" num="19.2">Sur la ville toujours plane une ardente brume,</l>
					<l n="57" num="19.3">Un bourdonnement sourd fait de cent bruits confus.</l>
				</lg>
				<lg n="20">
					<l n="58" num="20.1">Ce sont les tintements et les grêles volées</l>
					<l n="59" num="20.2">Des cloches, de leurs voix sonores ou fêlées,</l>
					<l n="60" num="20.3">Chantant à plein gosier dans leurs beffrois touffus ;</l>
				</lg>
				<lg n="21">
					<l n="61" num="21.1">C’est le vent dans le ciel et l’homme sur la terre ;</l>
					<l n="62" num="21.2">C’est le bruit des tambours et des clairons de guerre,</l>
					<l n="63" num="21.3">Ou des canons grondeurs sonnant sur leurs affûts ;</l>
				</lg>
				<lg n="22">
					<l n="64" num="22.1">C’est la rumeur des chars, dont la prompte lanterne</l>
					<l n="65" num="22.2">File comme une étoile à travers l’ombre terne,</l>
					<l n="66" num="22.3">Emportant un heureux aux bras de son désir ;</l>
				</lg>
				<lg n="23">
					<l n="67" num="23.1">Le soupir de la vierge au balcon accoudée,</l>
					<l n="68" num="23.2">Le marteau sur l’enclume et le fait sur l’idée,</l>
					<l n="69" num="23.3">Le cri de la douleur ou le chant du plaisir.</l>
				</lg>
				<lg n="24">
					<l n="70" num="24.1">Dans cette symphonie au colossal orchestre,</l>
					<l n="71" num="24.2">Que n’écrira jamais musicien terrestre,</l>
					<l n="72" num="24.3">Chaque objet fait sa note impossible à saisir.</l>
				</lg>
				<lg n="25">
					<l n="73" num="25.1">Vous pensiez être en haut ; mais voici qu’une aiguille,</l>
					<l n="74" num="25.2">Où le ciel découpé par dentelles scintille,</l>
					<l n="75" num="25.3">Se présente soudain devant vos pieds lassés.</l>
				</lg>
				<lg n="26">
					<l n="76" num="26.1">Il faut monter encor, dans la mince tourelle,</l>
					<l n="77" num="26.2">L’escalier qui serpente en spirale plus frêle,</l>
					<l n="78" num="26.3">Se pendant aux crampons de loin en loin placés.</l>
				</lg>
				<lg n="27">
					<l n="79" num="27.1">Le vent, d’un air moqueur, à vos oreilles siffle,</l>
					<l n="80" num="27.2">La goule étend sa griffe et la guivre renifle,</l>
					<l n="81" num="27.3">Le vertige alourdit vos pas embarrassés.</l>
				</lg>
				<lg n="28">
					<l n="82" num="28.1">Vous voyez loin de vous, comme dans des abîmes</l>
					<l n="83" num="28.2">S’aplanir les clochers et les plus hautes cimes,</l>
					<l n="84" num="28.3">Des aigles les plus fiers vous dominez l’essor.</l>
				</lg>
				<lg n="29">
					<l n="85" num="29.1">Votre sueur se fige à votre front en nage ;</l>
					<l n="86" num="29.2">L’air trop vif vous étouffe : allons, enfant, courage !</l>
					<l n="87" num="29.3">Vous êtes près des cieux ; allons, un pas encor !</l>
				</lg>
				<lg n="30">
					<l n="88" num="30.1">Et vous pourrez toucher, de votre main surprise,</l>
					<l n="89" num="30.2">L’archange colossal que fait tourner la brise,</l>
					<l n="90" num="30.3">Le saint Michel géant qui tient un glaive d’or ;</l>
				</lg>
				<lg n="31">
					<l n="91" num="31.1">Et si, vous accoudant sur la rampe de marbre,</l>
					<l n="92" num="31.2">Qui palpite au grand vent, comme une branche d’arbre,</l>
					<l n="93" num="31.3">Vous dirigez en bas un œil moins effrayé,</l>
				</lg>
				<lg n="32">
					<l n="94" num="32.1">Vous verrez la campagne à plus de trente lieues,</l>
					<l n="95" num="32.2">Un immense horizon, bordé de franges bleues,</l>
					<l n="96" num="32.3">Se déroulant sous vous comme un tapis rayé ;</l>
				</lg>
				<lg n="33">
					<l n="97" num="33.1">Les carrés de blé d’or, les cultures zébrées,</l>
					<l n="98" num="33.2">Les plaques de gazon de troupeaux noirs tigrées ;</l>
					<l n="99" num="33.3">Et, dans le sainfoin rouge, un chemin blanc frayé ;</l>
				</lg>
				<lg n="34">
					<l n="100" num="34.1">Les cités, les hameaux, nids semés dans la plaine,</l>
					<l n="101" num="34.2">Et, partout où se groupe une famille humaine,</l>
					<l n="102" num="34.3">Un clocher vers le ciel comme un doigt s’allongeant.</l>
				</lg>
				<lg n="35">
					<l n="103" num="35.1">Vous verrez dans le golfe, aux bras des promontoires,</l>
					<l n="104" num="35.2">La mer se diaprer et se gaufrer de moires,</l>
					<l n="105" num="35.3">Comme un kandjiar turc damasquiné d’argent ;</l>
				</lg>
				<lg n="36">
					<l n="106" num="36.1">Les vaisseaux, alcyons balancés sur leurs ailes,</l>
					<l n="107" num="36.2">Piquer l’azur lointain de blanches étincelles</l>
					<l n="108" num="36.3">Et croiser en tous sens leur vol intelligent.</l>
				</lg>
				<lg n="37">
					<l n="109" num="37.1">Comme un sein plein de lait gonflant leurs voiles rondes,</l>
					<l n="110" num="37.2">Sur la foi de l’aimant, ils vont chercher des mondes,</l>
					<l n="111" num="37.3">Des rivages nouveaux sur de nouvelles mers :</l>
				</lg>
				<lg n="38">
					<l n="112" num="38.1">Dans l’Inde, de parfums, d’or et de soleil pleine,</l>
					<l n="113" num="38.2">Dans la Chine bizarre, aux tours de porcelaine,</l>
					<l n="114" num="38.3">Chimérique pays peuplé de dragons verts ;</l>
				</lg>
				<lg n="39">
					<l n="115" num="39.1">Ou vers Otaïti, la belle fleur des ondes,</l>
					<l n="116" num="39.2">De ses longs cheveux noirs tordant les perles blondes,</l>
					<l n="117" num="39.3">Comme une autre Vénus, fille des flots amers ;</l>
				</lg>
				<lg n="40">
					<l n="118" num="40.1">A Ceylan, à Java, plus loin encor peut-être,</l>
					<l n="119" num="40.2">Dans quelque île déserte et dont on se rend maître,</l>
					<l n="120" num="40.3">Vers une autre Amérique échappée à Colomb.</l>
				</lg>
				<lg n="41">
					<l n="121" num="41.1">Hélas ! et vous aussi, sans crainte, ô mes pensées,</l>
					<l n="122" num="41.2">Livrant aux vents du ciel vos ailes empressées,</l>
					<l n="123" num="41.3">Vous tentez un voyage aventureux et long.</l>
				</lg>
				<lg n="42">
					<l n="124" num="42.1">Si la foudre et le nord respectent vos antennes,</l>
					<l n="125" num="42.2">Des pays inconnus et des îles lointaines</l>
					<l n="126" num="42.3">Que rapporterez-vous ? de l’or, ou bien du plomb ?…</l>
				</lg>
				<lg n="43">
					<l n="127" num="43.1">La spirale soudain s’interrompt et se brise.</l>
					<l n="128" num="43.2">Comme celui qui monte au clocher de l’église,</l>
					<l n="129" num="43.3">Me voici maintenant au sommet de ma tour.</l>
				</lg>
				<lg n="44">
					<l n="130" num="44.1">J’ai planté le drapeau tout au haut de mon œuvre.</l>
					<l n="131" num="44.2">Ah ! que depuis longtemps, pauvre et rude manœuvre,</l>
					<l n="132" num="44.3">Insensible à la joie, à la vie, à l’amour,</l>
				</lg>
				<lg n="45">
					<l n="133" num="45.1">Pour garder mon dessin avec ses lignes pures,</l>
					<l n="134" num="45.2">J’émousse mon ciseau contre des pierres dures,</l>
					<l n="135" num="45.3">Élevant à grand’peine une assise par jour !</l>
				</lg>
				<lg n="46">
					<l n="136" num="46.1">Pendant combien de mois suis-je resté sous terre,</l>
					<l n="137" num="46.2">Creusant comme un mineur ma fouille solitaire,</l>
					<l n="138" num="46.3">Et cherchant le roc vif pour mes fondations !</l>
				</lg>
				<lg n="47">
					<l n="139" num="47.1">Et pourtant le soleil riait sur la nature ;</l>
					<l n="140" num="47.2">Les fleurs faisaient l’amour et toute créature</l>
					<l n="141" num="47.3">Livrait sa fantaisie au vent des passions.</l>
				</lg>
				<lg n="48">
					<l n="142" num="48.1">Le printemps dans les bois faisait courir la sève,</l>
					<l n="143" num="48.2">Et le flot, en chantant, venait baiser la grève ;</l>
					<l n="144" num="48.3">Tout n’était que parfum, plaisir, joie et rayons !</l>
				</lg>
				<lg n="49">
					<l n="145" num="49.1">Patient architecte, avec mes mains pensives</l>
					<l n="146" num="49.2">Sur mes piliers trapus inclinant mes ogives,</l>
					<l n="147" num="49.3">Je fouillais sous l’église un temple souterrain.</l>
				</lg>
				<lg n="50">
					<l n="148" num="50.1">Puis l’église elle-même, avec ses colonnettes,</l>
					<l n="149" num="50.2">Qui semble, tant elle a d’aiguilles et d’arêtes,</l>
					<l n="150" num="50.3">Un madrépore immense, un polypier marin ;</l>
				</lg>
				<lg n="51">
					<l n="151" num="51.1">Et le clocher hardi, grand peuplier de pierre,</l>
					<l n="152" num="51.2">Où gazouillent, quand vient l’heure de la prière</l>
					<l n="153" num="51.3">Avec les blancs ramiers, des nids d’oiseaux d’airain.</l>
				</lg>
				<lg n="52">
					<l n="154" num="52.1">Du haut de cette tour à grand’peine achevée,</l>
					<l n="155" num="52.2">Pourrai-je t’entrevoir, perspective rêvée,</l>
					<l n="156" num="52.3">Terre de Chanaan où tendait mon effort ?</l>
				</lg>
				<lg n="53">
					<l n="157" num="53.1">Pourrai-je apercevoir la figure du monde,</l>
					<l n="158" num="53.2">Les astres dans le ciel accomplissant leur ronde,</l>
					<l n="159" num="53.3">Et les vaisseaux quittant et regagnant le port ?</l>
				</lg>
				<lg n="54">
					<l n="160" num="54.1">Si mon clocher passait seulement de la tête</l>
					<l n="161" num="54.2">Les toits et les tuyaux de la ville, ou le faîte</l>
					<l n="162" num="54.3">De ce donjon aigu qui du brouillard ressort ;</l>
				</lg>
				<lg n="55">
					<l n="163" num="55.1">S’il était assez haut pour découvrir l’étoile</l>
					<l n="164" num="55.2">Que la colline bleue avec son dos me voile,</l>
					<l n="165" num="55.3">Le croissant qui s’écorne au toit de la maison ;</l>
				</lg>
				<lg n="56">
					<l n="166" num="56.1">Pour voir, au ciel de smalt, les flottantes nuées</l>
					<l n="167" num="56.2">Par le vent du matin mollement remuées,</l>
					<l n="168" num="56.3">Comme un troupeau de l’air secouer leur toison ;</l>
				</lg>
				<lg n="57">
					<l n="169" num="57.1">Et la gloire, la gloire, astre et soleil de l’âme,</l>
					<l n="170" num="57.2">Dans un océan d’or, avec le globe en flamme,</l>
					<l n="171" num="57.3">Majestueusement monter à l’horizon !</l>
				</lg>
			</div></body></text></TEI>