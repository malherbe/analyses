<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU173">
				<head type="main">TÉNÈBRES</head>
				<lg n="1">
					<l n="1" num="1.1">Taisez-vous, ô mon cœur ! taisez-vous, ô mon âme !</l>
					<l n="2" num="1.2">Et n’allez plus chercher de querelles au sort ;</l>
					<l n="3" num="1.3">Le néant vous appelle et l’oubli vous réclame.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1">Mon cœur, ne battez plus, puisque vous êtes mort ;</l>
					<l n="5" num="2.2">Mon âme, repliez le reste de vos ailes,</l>
					<l n="6" num="2.3">Car vous avez tenté votre suprême effort.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">Vos deux linceuls sont prêts, et vos fosses jumelles</l>
					<l n="8" num="3.2">Ouvrent leur bouche sombre au flanc de mon passé,</l>
					<l n="9" num="3.3">Comme au flanc d’un guerrier deux blessures mortelles.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1">Couchez-vous tout du long dans votre lit glacé.</l>
					<l n="11" num="4.2">Puisse avec vos tombeaux, que va recouvrir l’herbe,</l>
					<l n="12" num="4.3">Votre souvenir être à jamais effacé !</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1">Vous n’aurez pas de croix ni de marbre superbe,</l>
					<l n="14" num="5.2">Ni d’épitaphe d’or, où quelque saule en pleurs</l>
					<l n="15" num="5.3">Laisse les doigts du vent éparpiller sa gerbe.</l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1">Vous n’aurez ni blasons, ni chants, ni vers, ni fleurs ;</l>
					<l n="17" num="6.2">On ne répandra pas les larmes argentées</l>
					<l n="18" num="6.3">Sur le funèbre drap, noir manteau des douleurs.</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1">Votre convoi muet, comme ceux des athées,</l>
					<l n="20" num="7.2">Sur le triste chemin rampera dans la nuit :</l>
					<l n="21" num="7.3">Vos cendres sans honneur seront au vent jetées.</l>
				</lg>
				<lg n="8">
					<l n="22" num="8.1">La pierre qui s’abîme en tombant fait son bruit ;</l>
					<l n="23" num="8.2">Mais vous, vous tomberez sans que l’onde s’émeuve,</l>
					<l n="24" num="8.3">Dans ce gouffre sans fond où le remords nous suit.</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1">Vous ne ferez pas même un seul rond sur le fleuve,</l>
					<l n="26" num="9.2">Nul ne s’apercevra que vous soyez absents,</l>
					<l n="27" num="9.3">Aucune âme ici-bas ne se sentira veuve.</l>
				</lg>
				<lg n="10">
					<l n="28" num="10.1">Et le chaste secret du rêve de vos ans</l>
					<l n="29" num="10.2">Périra tout entier sous votre tombe obscure</l>
					<l n="30" num="10.3">Où rien n’attirera le regard des passants.</l>
				</lg>
				<lg n="11">
					<l n="31" num="11.1">Que voulez-vous ? hélas ! notre mère Nature,</l>
					<l n="32" num="11.2">Comme toute autre mère, a ses enfants gâtés,</l>
					<l n="33" num="11.3">Et pour les malvenus elle est avare et dure.</l>
				</lg>
				<lg n="12">
					<l n="34" num="12.1">Aux uns tous les bonheurs et toutes les beautés !</l>
					<l n="35" num="12.2">L’occasion leur est toujours bonne et fidèle :</l>
					<l n="36" num="12.3">Ils trouvent au désert des palais enchantés,</l>
				</lg>
				<lg n="13">
					<l n="37" num="13.1">Ils tettent librement la féconde mamelle ;</l>
					<l n="38" num="13.2">La chimère à leur voix s’empresse d’accourir,</l>
					<l n="39" num="13.3">Et tout l’or du Pactole entre leurs doigts ruisselle.</l>
				</lg>
				<lg n="14">
					<l n="40" num="14.1">Les autres moins aimés ont beau tordre et pétrir</l>
					<l n="41" num="14.2">Avec leurs maigres mains la mamelle tarie,</l>
					<l n="42" num="14.3">Leur frère a bu le lait qui les devait nourrir.</l>
				</lg>
				<lg n="15">
					<l n="43" num="15.1">S’il éclôt quelque chose au milieu de leur vie,</l>
					<l n="44" num="15.2">Une petite fleur sous leur pâle gazon,</l>
					<l n="45" num="15.3">Le sabot du vacher l’aura bientôt flétrie.</l>
				</lg>
				<lg n="16">
					<l n="46" num="16.1">Un rayon de soleil brille à leur horizon,</l>
					<l n="47" num="16.2">Il fait beau dans leur âme ; à coup sûr un nuage</l>
					<l n="48" num="16.3">Avec un flot de pluie éteindra le rayon.</l>
				</lg>
				<lg n="17">
					<l n="49" num="17.1">L’espoir le mieux fondé, le projet le plus sage,</l>
					<l n="50" num="17.2">Rien ne leur réussit ; tout les trompe et leur ment.</l>
					<l n="51" num="17.3">Ils se perdent en mer sans quitter le rivage.</l>
				</lg>
				<lg n="18">
					<l n="52" num="18.1">L’aigle, pour le briser, du haut du firmament,</l>
					<l n="53" num="18.2">Sur leur front découvert lâchera la tortue,</l>
					<l n="54" num="18.3">Car ils doivent périr inévitablement.</l>
				</lg>
				<lg n="19">
					<l n="55" num="19.1">L’aigle manque son coup ; quelque vieille statue</l>
					<l n="56" num="19.2">Sans tremblement de terre, on ne sait pas pourquoi,</l>
					<l n="57" num="19.3">Quitte son piédestal, les écrase et les tue.</l>
				</lg>
				<lg n="20">
					<l n="58" num="20.1">Le cœur qu’ils ont choisi ne garde pas sa foi ;</l>
					<l n="59" num="20.2">Leur chien même les mord et leur donne la rage ;</l>
					<l n="60" num="20.3">Un ami jurera qu’ils ont trahi le roi.</l>
				</lg>
				<lg n="21">
					<l n="61" num="21.1">Fils du Danube, ils vont se noyer dans le Tage ;</l>
					<l n="62" num="21.2">D’un bout du monde à l’autre ils courent à leur mort,</l>
					<l n="63" num="21.3">Ils auraient pu du moins s’épargner le voyage !</l>
				</lg>
				<lg n="22">
					<l n="64" num="22.1">Si dur qu’il soit, il faut qu’ils remplissent leur sort ;</l>
					<l n="65" num="22.2">Nul n’y peut résister, et le genou d’Hercule</l>
					<l n="66" num="22.3">Pour un pareil athlète est à peine assez fort.</l>
				</lg>
				<lg n="23">
					<l n="67" num="23.1">Après la vie obscure une mort ridicule ;</l>
					<l n="68" num="23.2">Après le dur grabat un cercueil sans repos</l>
					<l n="69" num="23.3">Au bord d’un carrefour où la foule circule.</l>
				</lg>
				<lg n="24">
					<l n="70" num="24.1">Ils tombent inconnus de la mort des héros,</l>
					<l n="71" num="24.2">Et quelque ambitieux, pour se hausser la taille,</l>
					<l n="72" num="24.3">Se fait effrontément un socle de leurs os.</l>
				</lg>
				<lg n="25">
					<l n="73" num="25.1">Sur son trône d’airain, le Destin qui s’en raille</l>
					<l n="74" num="25.2">Imbibe leur éponge avec du fiel amer,</l>
					<l n="75" num="25.3">Et la Nécessité les tord dans sa tenaille.</l>
				</lg>
				<lg n="26">
					<l n="76" num="26.1">Tout buisson trouve un dard pour déchirer leur chair,</l>
					<l n="77" num="26.2">Tout beau chemin pour eux cache une chausse-trappe,</l>
					<l n="78" num="26.3">Et les chaînes de fleurs leur sont chaînes de fer.</l>
				</lg>
				<lg n="27">
					<l n="79" num="27.1">Si le tonnerre tombe, entre mille il les frappe ;</l>
					<l n="80" num="27.2">Pour eux l’aveugle nuit semble prendre des yeux,</l>
					<l n="81" num="27.3">Tout plomb vole à leur cœur et pas un seul n’échappe.</l>
				</lg>
				<lg n="28">
					<l n="82" num="28.1">La tombe vomira leur fantôme odieux.</l>
					<l n="83" num="28.2">Vivants, ils ont servi de bouc expiatoire ;</l>
					<l n="84" num="28.3">Morts, ils seront bannis de la terre et des cieux.</l>
				</lg>
				<lg n="29">
					<l n="85" num="29.1">Cette histoire sinistre est votre propre histoire,</l>
					<l n="86" num="29.2">O mon âme ! ô mon cœur ! peut-être même, hélas !</l>
					<l n="87" num="29.3">La vôtre est-elle encor plus sinistre et plus noire.</l>
				</lg>
				<lg n="30">
					<l n="88" num="30.1">C’est une histoire simple où l’on ne trouve pas</l>
					<l n="89" num="30.2">De grands événements et des malheurs de drame,</l>
					<l n="90" num="30.3">Une douleur qui chante et fait un grand fracas ;</l>
				</lg>
				<lg n="31">
					<l n="91" num="31.1">Quelques fils bien communs en composent la trame,</l>
					<l n="92" num="31.2">Et cependant elle est plus triste et sombre à voir</l>
					<l n="93" num="31.3">Que celle qu’un poignard dénoue avec sa lame.</l>
				</lg>
				<lg n="32">
					<l n="94" num="32.1">Puisque rien ne vous veut, pourquoi donc tout vouloir ;</l>
					<l n="95" num="32.2">Quand il vous faut mourir, pourquoi donc vouloir vivre,</l>
					<l n="96" num="32.3">Vous qui ne croyez pas et n’avez pas d’espoir ?</l>
				</lg>
				<lg n="33">
					<l n="97" num="33.1">O vous que nul amour et que nul vin n’enivre,</l>
					<l n="98" num="33.2">Frères désespérés, vous devez être prêts</l>
					<l n="99" num="33.3">Tour descendre au néant où mon corps vous doit suivre !</l>
				</lg>
				<lg n="34">
					<l n="100" num="34.1">Le néant a des lits et des ombrages frais.</l>
					<l n="101" num="34.2">La Mort fait mieux dormir que son frère Morphée,</l>
					<l n="102" num="34.3">Et les pavots devraient jalouser les cyprès.</l>
				</lg>
				<lg n="35">
					<l n="103" num="35.1">Sous la cendre à jamais, dors, ô flamme étouffée !</l>
					<l n="104" num="35.2">Orgueil, courbe ton front jusque sur tes genoux,</l>
					<l n="105" num="35.3">Comme un Scythe captif qui supporte un trophée.</l>
				</lg>
				<lg n="36">
					<l n="106" num="36.1">Cesse de te roidir contre le sort jaloux,</l>
					<l n="107" num="36.2">Dans l’eau du noir Léthé plonge de bonne grâce,</l>
					<l n="108" num="36.3">Et laisse à ton cercueil planter les derniers clous.</l>
				</lg>
				<lg n="37">
					<l n="109" num="37.1">Le sable des chemins ne garde pas ta trace,</l>
					<l n="110" num="37.2">L’écho ne redit pas ta chanson, et le mur</l>
					<l n="111" num="37.3">Ne veut pas se charger de ton ombre qui passe.</l>
				</lg>
				<lg n="38">
					<l n="112" num="38.1">Pour y graver un nom ton airain est bien dur,</l>
					<l n="113" num="38.2">O Corinthe ! et souvent, froide et blanche Carrare</l>
					<l n="114" num="38.3">Le ciseau ne mord pas sur ton marbre si pur.</l>
				</lg>
				<lg n="39">
					<l n="115" num="39.1">Il faut un grand génie avec un bonheur rare</l>
					<l n="116" num="39.2">Pour faire jusqu’au ciel monter son monument,</l>
					<l n="117" num="39.3">Et de ce double don le destin est avare.</l>
				</lg>
				<lg n="40">
					<l n="118" num="40.1">Hélas ! et le poëte est pareil à l’amant,</l>
					<l n="119" num="40.2">Car ils ont tous les deux leur maîtresse idéale,</l>
					<l n="120" num="40.3">Quelque rêve chéri caressé chastement :</l>
				</lg>
				<lg n="41">
					<l n="121" num="41.1">Eldorado lointain, pierre philosophale</l>
					<l n="122" num="41.2">Qu’ils poursuivent toujours sans l’atteindre jamais ;</l>
					<l n="123" num="41.3">Un astre impérieux, une étoile fatale.</l>
				</lg>
				<lg n="42">
					<l n="124" num="42.1">L’étoile fuit toujours, ils lui courent après ;</l>
					<l n="125" num="42.2">Et le matin venu, la lueur poursuivie,</l>
					<l n="126" num="42.3">Quand ils la vont saisir, s’éteint dans un marais.</l>
				</lg>
				<lg n="43">
					<l n="127" num="43.1">C’est une belle chose et digne qu’on l’envie</l>
					<l n="128" num="43.2">Que de trouver son rêve au milieu du chemin,</l>
					<l n="129" num="43.3">Et d’avoir devant soi le désir de sa vie.</l>
				</lg>
				<lg n="44">
					<l n="130" num="44.1">Quel plaisir quand on voit briller le lendemain</l>
					<l n="131" num="44.2">Le baiser du soleil aux frêles colonnades</l>
					<l n="132" num="44.3">Du palais que la nuit éleva de sa main !</l>
				</lg>
				<lg n="45">
					<l n="133" num="45.1">Il est beau qu’un plongeur, comme dans les ballades,</l>
					<l n="134" num="45.2">Descende au gouffre amer chercher la coupe d’or,</l>
					<l n="135" num="45.3">Et perce triomphant les vitreuses arcades.</l>
				</lg>
				<lg n="46">
					<l n="136" num="46.1">Il est beau d’arriver où tendait son essor,</l>
					<l n="137" num="46.2">De trouver sa beauté, d’aborder à son monde,</l>
					<l n="138" num="46.3">Et, quand on a fouillé, d’exhumer un trésor ;</l>
				</lg>
				<lg n="47">
					<l n="139" num="47.1">De faire, du plus creux de son âme profonde,</l>
					<l n="140" num="47.2">Rayonner son idée ou bien sa passion,</l>
					<l n="141" num="47.3">D’être l’oiseau qui chante et la foudre qui gronde ;</l>
				</lg>
				<lg n="48">
					<l n="142" num="48.1">D’unir heureusement le rêve à l’action,</l>
					<l n="143" num="48.2">D’aimer et d’être aimé, de gagner quand on joue,</l>
					<l n="144" num="48.3">Et de donner un trône à son ambition ;</l>
				</lg>
				<lg n="49">
					<l n="145" num="49.1">D’arrêter, quand on veut, la Fortune et sa roue,</l>
					<l n="146" num="49.2">Et de sentir, la nuit, quelque baiser royal</l>
					<l n="147" num="49.3">Se suspendre en tremblant aux fleurs de votre joue.</l>
				</lg>
				<lg n="50">
					<l n="148" num="50.1">Ceux-là sont peu nombreux dans notre âge fatal.</l>
					<l n="149" num="50.2">Polycrate aujourd’hui pourrait garder sa bague :</l>
					<l n="150" num="50.3">Nul bonheur insolent n’ose appeler le mal.</l>
				</lg>
				<lg n="51">
					<l n="151" num="51.1">L’eau s’avance et nous gagne, et pas à pas la vague,</l>
					<l n="152" num="51.2">Montant les escaliers qui mènent à nos tours,</l>
					<l n="153" num="51.3">Mêle aux chants du festin son chant confus et vague.</l>
				</lg>
				<lg n="52">
					<l n="154" num="52.1">Les phoques monstrueux, traînant leurs ventres lourds,</l>
					<l n="155" num="52.2">Viennent jusqu’à la table, et leurs larges mâchoires</l>
					<l n="156" num="52.3">S’ouvrent avec des cris et des grognements sourds.</l>
				</lg>
				<lg n="53">
					<l n="157" num="53.1">Sur les autels déserts des basiliques noires,</l>
					<l n="158" num="53.2">Les saints désespérés, et reniant leur Dieu,</l>
					<l n="159" num="53.3">S’arrachent à pleins poings l’or chevelu des gloires.</l>
				</lg>
				<lg n="54">
					<l n="160" num="54.1">Le soleil désolé, penchant son œil de feu,</l>
					<l n="161" num="54.2">Pleure sur l’univers une larme sanglante ;</l>
					<l n="162" num="54.3">L’ange dit à la terre un éternel adieu.</l>
				</lg>
				<lg n="55">
					<l n="163" num="55.1">Rien ne sera sauvé, ni l’homme ni la plante ;</l>
					<l n="164" num="55.2">L’eau recouvrira tout : la montagne et la tour ;</l>
					<l n="165" num="55.3">Car la vengeance vient, quoique boiteuse et lente.</l>
				</lg>
				<lg n="56">
					<l n="166" num="56.1">Les plumes s’useront aux ailes du vautour,</l>
					<l n="167" num="56.2">Sans qu’il trouve une place où rebâtir son aire,</l>
					<l n="168" num="56.3">Et du monde vingt fois il refera le tour ;</l>
				</lg>
				<lg n="57">
					<l n="169" num="57.1">Puis il retombera dans cette eau solitaire</l>
					<l n="170" num="57.2">Où le rond de sa chute ira s’élargissant :</l>
					<l n="171" num="57.3">Alors tout sera dit pour cette pauvre terre.</l>
				</lg>
				<lg n="58">
					<l n="172" num="58.1">Rien ne sera sauvé, pas même l’innocent.</l>
					<l n="173" num="58.2">Ce sera, cette fois, un déluge sans arche ;</l>
					<l n="174" num="58.3">Les eaux seront les pleurs des hommes et leur sang.</l>
				</lg>
				<lg n="59">
					<l n="175" num="59.1">Plus de mont Ararat où se pose, en sa marche,</l>
					<l n="176" num="59.2">Le vaisseau d’avenir qui cache en ses flancs creux</l>
					<l n="177" num="59.3">Les trois nouveaux Adams et le grand patriarche.</l>
				</lg>
				<lg n="60">
					<l n="178" num="60.1">Entendez-vous là-haut ces craquements affreux ?</l>
					<l n="179" num="60.2">Le vieil Atlas lassé retire son épaule</l>
					<l n="180" num="60.3">Au lourd entablement de ce ciel ténébreux.</l>
				</lg>
				<lg n="61">
					<l n="181" num="61.1">L’essieu du monde ploie ainsi qu’un brin de saule ;</l>
					<l n="182" num="61.2">La terre ivre a perdu son chemin dans le ciel ;</l>
					<l n="183" num="61.3">L’aimant déconcerté ne trouve plus son pôle.</l>
				</lg>
				<lg n="62">
					<l n="184" num="62.1">Le Christ, d’un ton railleur, tord l’éponge de fiel</l>
					<l n="185" num="62.2">Sur les lèvres en feu du monde à l’agonie,</l>
					<l n="186" num="62.3">Et Dieu, dans son Delta, rit d’un rire cruel.</l>
				</lg>
				<lg n="63">
					<l n="187" num="63.1">Quand notre passion sera-t-elle finie ?</l>
					<l n="188" num="63.2">Le sang coule avec l’eau de notre flanc ouvert ;</l>
					<l n="189" num="63.3">La sueur ronge teint notre face jaunie.</l>
				</lg>
				<lg n="64">
					<l n="190" num="64.1">Assez comme cela ! nous avons trop souffert ;</l>
					<l n="191" num="64.2">De nos lèvres, Seigneur, détournez ce calice,</l>
					<l n="192" num="64.3">Car pour nous racheter votre Fils s’est offert.</l>
				</lg>
				<lg n="65">
					<l n="193" num="65.1">Christ n’y peut rien : il faut que le sort s’accomplisse ;</l>
					<l n="194" num="65.2">Pour sauver ce vieux monde il faut un Dieu nouveau,</l>
					<l n="195" num="65.3">Et le prêtre demande un autre sacrifice.</l>
				</lg>
				<lg n="66">
					<l n="196" num="66.1">Voici bien deux mille ans que l’on saigne l’Agneau ;</l>
					<l n="197" num="66.2">Il est mort à la fin, et sa gorge épuisée</l>
					<l n="198" num="66.3">N’a plus assez de sang pour teindre le couteau.</l>
				</lg>
				<lg n="67">
					<l n="199" num="67.1">Le Dieu ne viendra pas. L’Église est renversée.</l>
				</lg>
			</div></body></text></TEI>