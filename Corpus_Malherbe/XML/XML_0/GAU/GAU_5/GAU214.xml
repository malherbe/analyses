<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU214">
				<head type="main">TRISTESSE</head>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="6" unit="char"></space>Avril est de retour.</l>
					<l n="2" num="1.2"><space quantity="6" unit="char"></space>La première des roses,</l>
					<l n="3" num="1.3"><space quantity="6" unit="char"></space>De ses lèvres mi-closes,</l>
					<l n="4" num="1.4"><space quantity="6" unit="char"></space>Rit au premier beau jour ;</l>
					<l n="5" num="1.5"><space quantity="6" unit="char"></space>La terre bienheureuse</l>
					<l n="6" num="1.6"><space quantity="6" unit="char"></space>S’ouvre et s’épanouit ;</l>
					<l n="7" num="1.7"><space quantity="6" unit="char"></space>Tout aime, tout jouit.</l>
					<l n="8" num="1.8">Hélas ! j’ai dans le cœur une tristesse affreuse.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space quantity="6" unit="char"></space>Les buveurs en gaîté,</l>
					<l n="10" num="2.2"><space quantity="6" unit="char"></space>Dans leurs chansons vermeilles,</l>
					<l n="11" num="2.3"><space quantity="6" unit="char"></space>Célèbrent sous les treilles</l>
					<l n="12" num="2.4"><space quantity="6" unit="char"></space>Le vin et la beauté ;</l>
					<l n="13" num="2.5"><space quantity="6" unit="char"></space>La musique joyeuse,</l>
					<l n="14" num="2.6"><space quantity="6" unit="char"></space>Avec leur rire clair</l>
					<l n="15" num="2.7"><space quantity="6" unit="char"></space>S’éparpille dans l’air.</l>
					<l n="16" num="2.8">Hélas ! j’ai dans le cœur une tristesse affreuse.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space quantity="6" unit="char"></space>En déshabillés blancs,</l>
					<l n="18" num="3.2"><space quantity="6" unit="char"></space>Les jeunes demoiselles</l>
					<l n="19" num="3.3"><space quantity="6" unit="char"></space>S’en vont sous les tonnelles</l>
					<l n="20" num="3.4"><space quantity="6" unit="char"></space>Au bras de leurs galants ;</l>
					<l n="21" num="3.5"><space quantity="6" unit="char"></space>La lune langoureuse</l>
					<l n="22" num="3.6"><space quantity="6" unit="char"></space>Argente leurs baisers</l>
					<l n="23" num="3.7"><space quantity="6" unit="char"></space>Longuement appuyés.</l>
					<l n="24" num="3.8">Hélas ! j’ai dans le cœur une tristesse affreuse.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space quantity="6" unit="char"></space>Moi, je n’aime plus rien,</l>
					<l n="26" num="4.2"><space quantity="6" unit="char"></space>Ni l’homme, ni la femme,</l>
					<l n="27" num="4.3"><space quantity="6" unit="char"></space>Ni mon corps, ni mon âme,</l>
					<l n="28" num="4.4"><space quantity="6" unit="char"></space>Pas même mon vieux chien.</l>
					<l n="29" num="4.5"><space quantity="6" unit="char"></space>Allez dire qu’on creuse,</l>
					<l n="30" num="4.6"><space quantity="6" unit="char"></space>Sous le pâle gazon,</l>
					<l n="31" num="4.7"><space quantity="6" unit="char"></space>Une fosse sans nom.</l>
					<l n="32" num="4.8">Hélas ! j’ai dans le cœur une tristesse affreuse.</l>
				</lg>
			</div></body></text></TEI>