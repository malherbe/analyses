<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU183">
				<head type="main">LA DIVA</head>
				<lg n="1">
					<l n="1" num="1.1">On donnait à Favart <hi rend="ital">Mosé</hi>. Tamburini</l>
					<l n="2" num="1.2">Le basso cantante, le ténor Rubini,</l>
					<l n="3" num="1.3">Devaient jouer tous deux dans la pièce ; et la salle,</l>
					<l n="4" num="1.4">Quand on l’eut élargie et faite colossale,</l>
					<l n="5" num="1.5">Grande comme Saint-Charle ou comme la Scala,</l>
					<l n="6" num="1.6">N’aurait pu contenir son public ce soir-là.</l>
					<l n="7" num="1.7">Moi, plus heureux que tous, j’avais tout à connaître,</l>
					<l n="8" num="1.8">Et la voix des chanteurs et l’ouvrage du maître.</l>
					<l n="9" num="1.9">Aimant peu l’opéra, c’est hasard si j’y vais,</l>
					<l n="10" num="1.10">Et je n’avais pas vu le <hi rend="ital">Moïse</hi> français ;</l>
					<l n="11" num="1.11">Car notre idiome, à nous, rauque et sans prosodie,</l>
					<l n="12" num="1.12">Fausse toute musique ; et la note hardie,</l>
					<l n="13" num="1.13">Contre quelque mot dur se heurtant dans son vol,</l>
					<l n="14" num="1.14">Brise ses ailes d’or et tombe sur le sol.</l>
					<l n="15" num="1.15">J’étais là, les deux bras en croix sur la poitrine,</l>
					<l n="16" num="1.16">Pour contenir mon cœur plein d’extase divine ;</l>
					<l n="17" num="1.17">Mes artères chantant avec un sourd frisson,</l>
					<l n="18" num="1.18">Mon oreille tendue et buvant chaque son ;</l>
					<l n="19" num="1.19">Attentif comme au bruit de la grêle fanfare</l>
					<l n="20" num="1.20">Un cheval ombrageux qui palpite et s’effare.</l>
					<l n="21" num="1.21">Toutes les voix criaient, toutes les mains frappaient,</l>
					<l n="22" num="1.22">A force d’applaudir les gants blancs se rompaient ;</l>
					<l n="23" num="1.23">Et la toile tomba. C’était le premier acte.</l>
					<l n="24" num="1.24">Alors je regardai ; plus nette et plus exacte,</l>
					<l n="25" num="1.25">A travers le lorgnon dans mes yeux moins distraits,</l>
					<l n="26" num="1.26">Chaque tête à son tour passait avec ses traits.</l>
					<l n="27" num="1.27">Certes, sous l’éventail et la grille dorée,</l>
					<l n="28" num="1.28">Roulant dans leurs doigts blancs la cassolette ambrée,</l>
					<l n="29" num="1.29">Au reflet des joyaux, au feu des diamants,</l>
					<l n="30" num="1.30">Avec leurs colliers d’or et tous leurs ornements,</l>
					<l n="31" num="1.31">J’en vis plus d’une belle et méritant éloge ;</l>
					<l n="32" num="1.32">Du moins je le croyais, quand au fond d’une loge</l>
					<l n="33" num="1.33">J’aperçus une femme. Il me sembla d’abord,</l>
					<l n="34" num="1.34">La loge lui formant un cadre de son bord,</l>
					<l n="35" num="1.35">Que c’était un tableau de Titien ou Giorgione,</l>
					<l n="36" num="1.36">Moins la fumée antique et moins le vernis jaune,</l>
					<l n="37" num="1.37">Car elle se tenait dans l’immobilité,</l>
					<l n="38" num="1.38">Regardant devant elle avec simplicité,</l>
					<l n="39" num="1.39">La bouche épanouie en un demi-sourire,</l>
					<l n="40" num="1.40">Et comme un livre ouvert son front se laissant lire.</l>
					<l n="41" num="1.41">Sa coiffure était basse, et ses cheveux moirés</l>
					<l n="42" num="1.42">Descendaient vers sa tempe en deux flots séparés.</l>
					<l n="43" num="1.43">Ni plumes, ni rubans, ni gaze, ni dentelle ;</l>
					<l n="44" num="1.44">Pour parure et bijoux, sa grâce naturelle ;</l>
					<l n="45" num="1.45">Pas d’œillade hautaine ou de grand air vainqueur,</l>
					<l n="46" num="1.46">Rien que le repos d’âme et la bonté de cœur.</l>
					<l n="47" num="1.47">Au bout de quelque temps, la belle créature,</l>
					<l n="48" num="1.48">Se lassant d’être ainsi, prit une autre posture,</l>
					<l n="49" num="1.49">Le col un peu penché, le menton sur la main,</l>
					<l n="50" num="1.50">De façon à montrer son beau profil romain,</l>
					<l n="51" num="1.51">Son épaule et son dos aux tons chauds et vivaces,</l>
					<l n="52" num="1.52">Où l’ombre avec le clair flottaient par larges masses.</l>
					<l n="53" num="1.53">Tout perdait son éclat, tout tombait à côté</l>
					<l n="54" num="1.54">De cette virginale et sereine beauté ;</l>
					<l n="55" num="1.55">Mon âme tout entière à cet aspect magique</l>
					<l n="56" num="1.56">Ne se souvenait plus d’écouter la musique,</l>
					<l n="57" num="1.57">Tant cette morbidezze et ce laisser-aller</l>
					<l n="58" num="1.58">Était chose charmante et douce à contempler,</l>
					<l n="59" num="1.59">Tant l’œil se reposait avec mélancolie</l>
					<l n="60" num="1.60">Sur ce pâle jasmin transplanté d’Italie.</l>
					<l n="61" num="1.61">Moins épris des beaux sons qu’épris des beaux contours,</l>
					<l n="62" num="1.62">Même au <hi rend="ital">parlar spiegar</hi>, je regardais toujours ;</l>
					<l n="63" num="1.63">J’admirais à part moi la gracieuse ligne</l>
					<l n="64" num="1.64">Du col se repliant comme le col d’un cygne,</l>
					<l n="65" num="1.65">L’ovale de la tête et la forme du front,</l>
					<l n="66" num="1.66">La main pure et correcte, avec le beau bras rond ;</l>
					<l n="67" num="1.67">Et je compris pourquoi, s’exilant de la France,</l>
					<l n="68" num="1.68">Ingres fit si longtemps ses amours de Florence.</l>
					<l n="69" num="1.69">Jusqu’à ce jour j’avais en vain cherché le beau ;</l>
					<l n="70" num="1.70">Ces formes sans puissance et cette fade peau</l>
					<l n="71" num="1.71">Sous laquelle le sang ne court que par la fièvre</l>
					<l n="72" num="1.72">Et que jamais soleil ne mordit de sa lèvre,</l>
					<l n="73" num="1.73">Ce dessin lâche et mou, ce coloris blafard,</l>
					<l n="74" num="1.74">M’avaient fait blasphémer la sainteté de l’art.</l>
					<l n="75" num="1.75">J’avais dit : L’art est faux, les rois de la peinture</l>
					<l n="76" num="1.76">D’un habit idéal revêtent la nature.</l>
					<l n="77" num="1.77">Ces tons harmonieux, ces beaux linéaments,</l>
					<l n="78" num="1.78">N’ont jamais existé qu’aux cerveaux des amants ;</l>
					<l n="79" num="1.79">J’avais dit, n’ayant vu que la laideur française :</l>
					<l n="80" num="1.80">Raphaël a menti comme Paul Véronèse !</l>
					<l n="81" num="1.81">Vous n’avez pas menti, non, maîtres ; voilà bien</l>
					<l n="82" num="1.82">Le marbre grec doré par l’ambre italien,</l>
					<l n="83" num="1.83">L’œil de flamme, le feint passionnément pâle,</l>
					<l n="84" num="1.84">Blond comme le soleil sous son voile de hâle,</l>
					<l n="85" num="1.85">Dans la mate blancheur les noirs sourcils marqués,</l>
					<l n="86" num="1.86">Le nez sévère et droit, la bouche aux coins arqués,</l>
					<l n="87" num="1.87">Les ailes de cheveux s’abattant sur les tempes,</l>
					<l n="88" num="1.88">Et tous les nobles traits de vos saintes estampes.</l>
					<l n="89" num="1.89">Non, vous n’avez pas fait un rêve de beauté,</l>
					<l n="90" num="1.90">C’est la vie elle-même et la réalité.</l>
					<l n="91" num="1.91">Votre Madone est là ; dans sa loge elle pose,</l>
					<l n="92" num="1.92">Près d’elle vainement l’on bourdonne et l’on cause ;</l>
					<l n="93" num="1.93">Elle reste immobile et sous le même jour,</l>
					<l n="94" num="1.94">Gardant comme un trésor l’harmonieux contour.</l>
					<l n="95" num="1.95">Artistes souverains, en copistes fidèles,</l>
					<l n="96" num="1.96">Vous avez reproduit vos superbes modèles !</l>
					<l n="97" num="1.97">Pourquoi, découragé par vos divins tableaux,</l>
					<l n="98" num="1.98">Ai-je, enfant paresseux, jeté là mes pinceaux,</l>
					<l n="99" num="1.99">Et pris pour vous fixer le crayon du poëte,</l>
					<l n="100" num="1.100">Beaux rêves, obsesseurs de mon âme inquiète,</l>
					<l n="101" num="1.101">Doux fantômes bercés dans les bras du désir,</l>
					<l n="102" num="1.102">Formes que la parole en vain cherche à saisir ?</l>
					<l n="103" num="1.103">Pourquoi, lassé trop tôt dans une heure de doute,</l>
					<l n="104" num="1.104">Peinture bien-aimée, ai-je quitté ta route ?</l>
					<l n="105" num="1.105">Que peuvent tous nos vers pour rendre la beauté,</l>
					<l n="106" num="1.106">Que peuvent de vains mots sans dessin arrêté,</l>
					<l n="107" num="1.107">Et l’épithète creuse et la rime incolore ?</l>
					<l n="108" num="1.108">Ah ! combien je regrette et comme je déplore</l>
					<l n="109" num="1.109">De ne plus être peintre, en te voyant ainsi</l>
					<l n="110" num="1.110">A <hi rend="ital">Mosé</hi>, dans ta loge, ô Julia Grisi !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1838">1838</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>