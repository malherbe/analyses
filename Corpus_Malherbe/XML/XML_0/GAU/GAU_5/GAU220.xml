<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU220">
				<head type="main">LE THERMODON</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">J’ai, dans mon cabinet, une bataille énorme</l>
						<l n="2" num="1.2">Qui s’agite et se tord comme un serpent difforme,</l>
						<l n="3" num="1.3">Et dont l’étrange aspect arrête l’œil surpris ;</l>
						<l n="4" num="1.4">On dirait qu’on entend, avec un sourd murmure,</l>
						<l n="5" num="1.5">La gravure sonner comme une vieille armure,</l>
						<l n="6" num="1.6">Et le papier muet semble jeter des cris.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Un pont par où se rue une foule en démence,</l>
						<l n="8" num="2.2">Arc-en-ciel de carnage, ouvre sa courbe immense,</l>
						<l n="9" num="2.3">Et d’un cadre de pierre entoure le tableau ;</l>
						<l n="10" num="2.4">A travers l’arche on voit une ville enflammée,</l>
						<l n="11" num="2.5">D’où montent, en tournant, de longs flots de fumée</l>
						<l n="12" num="2.6">Dont le rouge reflet brille et tremble sur l’eau.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Une barque, pareille à la barque des ombres,</l>
						<l n="14" num="3.2">Glisse sinistrement au dos des vagues sombres,</l>
						<l n="15" num="3.3">Portant, triste fardeau, des vaincus et des morts ;</l>
						<l n="16" num="3.4">Une averse de sang pleut des têtes coupées ;</l>
						<l n="17" num="3.5">Des mains par l’agonie éperdument crispées,</l>
						<l n="18" num="3.6">Avec leurs doigts noueux s’accrochent à ses bords.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Pour recevoir le corps, mort ou vivant, qui tombe,</l>
						<l n="20" num="4.2">Le grand fleuve a toujours toute prête une tombe ;</l>
						<l n="21" num="4.3">Il le berce un moment, et puis il l’engloutit ;</l>
						<l n="22" num="4.4">Les flots toujours béants, de leurs gueules voraces,</l>
						<l n="23" num="4.5">Dévorent cavaliers, chevaux, casques, cuirasses,</l>
						<l n="24" num="4.6">Tout ce que le combat jette à leur appétit.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Ici c’est un cheval qui s’effare et se cabre,</l>
						<l n="26" num="5.2">Et se fait, dans sa chute, une blessure, au sabre</l>
						<l n="27" num="5.3">Qu’un mourant tient encor dans son poing fracassé ;</l>
						<l n="28" num="5.4">Plus loin, c’est un carquois plein de flèches, qui verse</l>
						<l n="29" num="5.5">Ses dards en pluie aiguë, et dont chaque trait perce</l>
						<l n="30" num="5.6">Un cadavre déjà de cent coups traversé.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">C’est un rude combat ! chevelures, crinières,</l>
						<l n="32" num="6.2">Panaches et cimiers, enseignes et bannières,</l>
						<l n="33" num="6.3">Au souffle des clairons volent échevelés ;</l>
						<l n="34" num="6.4">Les lances, ces épis de la moisson sanglante,</l>
						<l n="35" num="6.5">S’inclinent à leur vent en tranche étincelante,</l>
						<l n="36" num="6.6">Comme sous une pluie on voit pencher des blés.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Les glaives dentelés font d’affreuses morsures ;</l>
						<l n="38" num="7.2">Le poignard altéré, plongeant dans les blessures,</l>
						<l n="39" num="7.3">Comme dans une coupe, y boit à flots le sang ;</l>
						<l n="40" num="7.4">Et les épieux, rompant les armes les plus fortes,</l>
						<l n="41" num="7.5">Pour le ciel ou l’enfer ouvrent de larges portes</l>
						<l n="42" num="7.6">Aux âmes qui des corps sortent en rugissant.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Quelle férocité de dessin et de touche !</l>
						<l n="44" num="8.2">Quelle sauvagerie et quelle ardeur farouche !</l>
						<l n="45" num="8.3">Qui signa ce poëme étrange et véhément ?</l>
						<l n="46" num="8.4">C’est toi, maître suprême, à la main turbulente,</l>
						<l n="47" num="8.5">Peintre au nom rouge, roi de la couleur brûlante,</l>
						<l n="48" num="8.6">Divin Néerlandais, Michel-Ange flamand !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">C’est toi, Rubens, c’est toi dont la rage sublime</l>
						<l n="50" num="9.2">Pencha cette bataille au bord de cet abîme,</l>
						<l n="51" num="9.3">Qui joignis ses deux bouts comme un bracelet d’or,</l>
						<l n="52" num="9.4">Et lui mis pour camée un beau groupe de femmes</l>
						<l n="53" num="9.5">Si blanches, que le fleuve aux triomphantes lames</l>
						<l n="54" num="9.6">S’apaise et n’ose pas les submerger encor !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="55" num="1.1">Car ce sont, ô pitié ! des femmes, des guerrières</l>
						<l n="56" num="1.2">Que la mêlée étreint de ses mains meurtrières.</l>
						<l n="57" num="1.3"><space quantity="4" unit="char"></space>Sous l’armure une gorge bat ;</l>
						<l n="58" num="1.4">Les écailles d’airain couvrent des seins d’ivoire,</l>
						<l n="59" num="1.5">Où, nourrisson cruel, la mort pâle vient boire</l>
						<l n="60" num="1.6"><space quantity="4" unit="char"></space>Le lait empourpré du combat.</l>
					</lg>
					<lg n="2">
						<l n="61" num="2.1">Regardez ! regardez ! les chevelures blondes</l>
						<l n="62" num="2.2">Coulent en ruisseaux d’or se mêler sous les ondes</l>
						<l n="63" num="2.3"><space quantity="4" unit="char"></space>Aux cheveux glauques des roseaux.</l>
						<l n="64" num="2.4">Voyez ces belles chairs, plus pures que l’albâtre,</l>
						<l n="65" num="2.5">Où, dans la blancheur mate, une veine bleuâtre</l>
						<l n="66" num="2.6"><space quantity="4" unit="char"></space>Circule en transparents réseaux.</l>
					</lg>
					<lg n="3">
						<l n="67" num="3.1">Hélas ! sur tous ces corps à la teinte nacrée,</l>
						<l n="68" num="3.2">La mort a déjà mis sa pâleur azurée ;</l>
						<l n="69" num="3.3"><space quantity="4" unit="char"></space>Ils n’ont de rose que le sang.</l>
						<l n="70" num="3.4">Leurs bras abandonnés trempent, les mains ouvertes,</l>
						<l n="71" num="3.5">Dans la vase du fleuve, entre les algues vertes,</l>
						<l n="72" num="3.6"><space quantity="4" unit="char"></space>Où l’eau les soulève en passant.</l>
					</lg>
					<lg n="4">
						<l n="73" num="4.1">Le cheval de bataille à la croupe tigrée,</l>
						<l n="74" num="4.2">Secouant dans les cieux sa crinière effarée,</l>
						<l n="75" num="4.3"><space quantity="4" unit="char"></space>Les foule avec ses durs sabots ;</l>
						<l n="76" num="4.4">Et le lâche vainqueur, dans sa rage brutale,</l>
						<l n="77" num="4.5">Sur leur ventre appuyant sa poudreuse sandale,</l>
						<l n="78" num="4.6"><space quantity="4" unit="char"></space>Tire à lui leurs derniers lambeaux.</l>
					</lg>
					<lg n="5">
						<l n="79" num="5.1">Bientôt du haut des monts les vautours au col chauve,</l>
						<l n="80" num="5.2">Les corbeaux vernissés, les aigles à l’œil fauve,</l>
						<l n="81" num="5.3">L’orfraie au regard clandestin,</l>
						<l n="82" num="5.4">Les loups se balançant sur leurs échines maigres,</l>
						<l n="83" num="5.5">Les renards, les chakals, accourront, tout allègres,</l>
						<l n="84" num="5.6"><space quantity="4" unit="char"></space>Prendre leur part au grand festin.</l>
					</lg>
					<lg n="6">
						<l n="85" num="6.1">Ce splendide banquet réparera leurs jeûnes.</l>
						<l n="86" num="6.2">O misère ! ô douleur ! tous ces corps frais et jeunes,</l>
						<l n="87" num="6.3"><space quantity="4" unit="char"></space>Ces beaux seins d’un si pur contour,</l>
						<l n="88" num="6.4">Faits pour les chauds baisers d’une amoureuse bouche,</l>
						<l n="89" num="6.5">Fouillés par le museau de l’hyène farouche,</l>
						<l n="90" num="6.6"><space quantity="4" unit="char"></space>Piqués par le bec du vautour !</l>
					</lg>
					<lg n="7">
						<l n="91" num="7.1">Cessez de vains efforts, ô braves amazones !</l>
						<l n="92" num="7.2">A quoi vous sert d’avoir, ainsi que des Bellones,</l>
						<l n="93" num="7.3"><space quantity="4" unit="char"></space>Le casque grec empanaché,</l>
						<l n="94" num="7.4">La cuirasse de fer, de clous d’or étoilée,</l>
						<l n="95" num="7.5">Si votre main trop faible, au fort de la mêlée,</l>
						<l n="96" num="7.6"><space quantity="4" unit="char"></space>Lâche votre glaive ébréché ?</l>
					</lg>
					<lg n="8">
						<l n="97" num="8.1">Votre armure faussée, entre ces bras robustes,</l>
						<l n="98" num="8.2">Comme un mince carton s’aplatit sur ces bustes</l>
						<l n="99" num="8.3"><space quantity="4" unit="char"></space>Où le poil pousse en plein terrain ;</l>
						<l n="100" num="8.4">Avec ces forts lutteurs, les plus puissantes armes,</l>
						<l n="101" num="8.5">O guerrières ! seraient les appas et les charmes</l>
						<l n="102" num="8.6"><space quantity="4" unit="char"></space>Cachés sous vos corsets d’airain.</l>
					</lg>
					<lg n="9">
						<l n="103" num="9.1">S’ils n’étaient repoussés par les rudes écailles,</l>
						<l n="104" num="9.2">Par les mailles d’acier qui hérissent vos tailles,</l>
						<l n="105" num="9.3"><space quantity="4" unit="char"></space>Les bras se suspendraient autour ;</l>
						<l n="106" num="9.4">Si vous aviez voulu, douce et modeste gloire,</l>
						<l n="107" num="9.5">Vous auriez sans combat remporté la victoire,</l>
						<l n="108" num="9.6"><space quantity="4" unit="char"></space>Car la force cède à l’amour.</l>
					</lg>
					<lg n="10">
						<l n="109" num="10.1">Penchez-vous sur le col de vos promptes cavales,</l>
						<l n="110" num="10.2">Qui volent, de la brise et de l’éclair rivales ;</l>
						<l n="111" num="10.3"><space quantity="4" unit="char"></space>Fuyez sans vous tourner pour voir,</l>
						<l n="112" num="10.4">Et ne vous arrêtez qu’en des retraites sûres</l>
						<l n="113" num="10.5">Où se trouve un flot clair pour laver vos blessures,</l>
						<l n="114" num="10.6"><space quantity="4" unit="char"></space>Et du gazon pour vous asseoir !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="115" num="1.1">C’est la nécessité ! c’est la règle fatale !</l>
						<l n="116" num="1.2">Toujours l’esprit le cède à la force brutale ;</l>
						<l n="117" num="1.3">Et quand la passion, aux beaux élans divins,</l>
						<l n="118" num="1.4">Avec le positif veut en venir aux mains,</l>
						<l n="119" num="1.5">Ardente, et n’écoutant que le feu qui l’anime,</l>
						<l n="120" num="1.6">Engage le combat sur le pont de l’abîme,</l>
						<l n="121" num="1.7">Elle ne peut tenir avec ses mains d’enfant</l>
						<l n="122" num="1.8">Contre ces grands chevaux à forme d’éléphant,</l>
						<l n="123" num="1.9">Cabrés et renversés sur leurs énormes croupes,</l>
						<l n="124" num="1.10">Contre ces forts guerriers et ces robustes troupes</l>
						<l n="125" num="1.11">Aux bras durs et noueux comme des chênes verts,</l>
						<l n="126" num="1.12">Aux musculeux poitrails de buffle recouverts ;</l>
						<l n="127" num="1.13">Toujours le pied lui manque, et, de flèches criblée,</l>
						<l n="128" num="1.14">Elle tombe en hurlant dans l’onde flagellée,</l>
						<l n="129" num="1.15">Où son corps va trouver les caïmans du fond.</l>
						<l n="130" num="1.16">Cependant les vainqueurs, sur la crête du pont,</l>
						<l n="131" num="1.17">Sans donner une plainte aux victimes noyées,</l>
						<l n="132" num="1.18">Passent, tambours battants, enseignes déployées.</l>
						<l n="133" num="1.19">Cette planche, gravée en six cartons divers</l>
						<l n="134" num="1.20">Par Lucas Vostermann, d’après Rubens d’Anvers,</l>
						<l n="135" num="1.21">Femmes au cœur hautain, pâles cariatides,</l>
						<l n="136" num="1.22">Qui ployez à regret des têtes moins timides</l>
						<l n="137" num="1.23">Sous le fronton pesant des devoirs et des lois,</l>
						<l n="138" num="1.24">Et qui vous refusez à porter votre croix,</l>
						<l n="139" num="1.25">De votre destinée est l’effrayant symbole,</l>
						<l n="140" num="1.26">Et je l’y vois écrite en sombre parabole.</l>
						<l n="141" num="1.27">Comme vous autrefois, folles de liberté,</l>
						<l n="142" num="1.28">Des femmes au grand cœur, à la mâle beauté,</l>
						<l n="143" num="1.29">Se brûlèrent un sein, et mirent à la place</l>
						<l n="144" num="1.30">La Méduse sculptée au cœur de la cuirasse ;</l>
						<l n="145" num="1.31">Elles laissèrent là l’aiguille et les fuseaux,</l>
						<l n="146" num="1.32">La navette qui court à travers les réseaux,</l>
						<l n="147" num="1.33">Les travaux de la femme et les soins du ménage,</l>
						<l n="148" num="1.34">Pour la lance et l’épée, instruments de carnage ;</l>
						<l n="149" num="1.35">Négligeant la parure, et n’ayant pour se voir</l>
						<l n="150" num="1.36">Qu’un bouclier d’airain, fauve et louche miroir,</l>
						<l n="151" num="1.37">Au Thermodon, qu’enjambe un pont d’une seule arche,</l>
						<l n="152" num="1.38">Leur troupe rencontra la grande armée en marche,</l>
						<l n="153" num="1.39">Ce fut un choc terrible, et sur le pont, longtemps,</l>
						<l n="154" num="1.40">Incertaine marée, on vit les combattants,</l>
						<l n="155" num="1.41">Les chevelures d’or ou bien les têtes brunes,</l>
						<l n="156" num="1.42">Femmes, soldats, suivant leurs diverses fortunes,</l>
						<l n="157" num="1.43">Pousser et repousser leur flux et leur reflux,</l>
						<l n="158" num="1.44">Et longtemps la victoire aux pieds irrésolus,</l>
						<l n="159" num="1.45">Mesurant le terrain et supputant les pertes,</l>
						<l n="160" num="1.46">Erra d’un camp à l’autre avec ses palmes vertes.</l>
						<l n="161" num="1.47">De fatigue à la fin, les bras frêles et blancs</l>
						<l n="162" num="1.48">Laissèrent, tout meurtris, choir leurs glaives sanglants,</l>
						<l n="163" num="1.49">Trop faibles ouvriers pour de si fortes âmes,</l>
						<l n="164" num="1.50">Et dans l’eau, jusqu’au soir, il plut des corps de femmes !</l>
					</lg>
				</div>
			</div></body></text></TEI>