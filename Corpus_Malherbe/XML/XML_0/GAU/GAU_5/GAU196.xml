<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU196">
				<head type="main">LE SPECTRE DE LA ROSE</head>
				<lg n="1">
					<l n="1" num="1.1">Soulève ta paupière close</l>
					<l n="2" num="1.2">Qu’effleure un songe virginal ;</l>
					<l n="3" num="1.3">Je suis le spectre d’une rose</l>
					<l n="4" num="1.4">Que tu portais hier au bal.</l>
					<l n="5" num="1.5">Tu me pris encore emperlée</l>
					<l n="6" num="1.6">Des pleurs d’argent de l’arrosoir,</l>
					<l n="7" num="1.7">Et parmi la fête étoilée</l>
					<l n="8" num="1.8">Tu me promenas tout le soir.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">O toi qui de ma mort fus cause,</l>
					<l n="10" num="2.2">Sans que tu puisses le chasser,</l>
					<l n="11" num="2.3">Toute la nuit mon spectre rose</l>
					<l n="12" num="2.4">A ton chevet viendra danser.</l>
					<l n="13" num="2.5">Mais ne crains rien, je ne réclame</l>
					<l n="14" num="2.6">Ni messe ni <hi rend="ital">De profundis</hi> ;</l>
					<l n="15" num="2.7">Ce léger parfum est mon âme,</l>
					<l n="16" num="2.8">Et j’arrive du paradis.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Mon destin fut digne d’envie :</l>
					<l n="18" num="3.2">Pour avoir un trépas si beau,</l>
					<l n="19" num="3.3">Plus d’un aurait donné sa vie,</l>
					<l n="20" num="3.4">Car j’ai ta gorge pour tombeau,</l>
					<l n="21" num="3.5">Et sur l’albâtre où je repose</l>
					<l n="22" num="3.6">Un poëte avec un baiser</l>
					<l n="23" num="3.7">Écrivit : Ci-gît une rose</l>
					<l n="24" num="3.8">Que tous les rois vont jalouser.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1837">1837</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>