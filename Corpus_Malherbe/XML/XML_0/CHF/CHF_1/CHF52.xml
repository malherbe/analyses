<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF52">
					<head type="main">VERS COMPOSÉS</head>
					<head type="sub_1">A L’OCCASION DE LA FÊTE DE M. DE VAUDREUIL</head>
					<lg n="1">
						<l n="1" num="1.1">Du patronage il faut chanter la fête :</l>
						<l n="2" num="1.2">A votre tour, Saint-Joseph, aujourd’hui</l>
						<l n="3" num="1.3">Qu’à vous louer ici chacun s’apprête !</l>
						<l n="4" num="1.4">Chacun de nous en vous trouve un appui.</l>
						<l n="5" num="1.5">Celui qu’on vit jadis en Galilée,</l>
						<l n="6" num="1.6">Benin mari, s’endormir en son lit,</l>
						<l n="7" num="1.7">Quand près de lui Marie, un peu troublée,</l>
						<l n="8" num="1.8">Dévotement cachait le Saint-Esprit,</l>
						<l n="9" num="1.9">N’est point le saint qu’aujourd’hui ma voix chante ;</l>
						<l n="10" num="1.10">J’aime l’hymen, mais je hais un mari,</l>
						<l n="11" num="1.11">Qui, sourd aux vœux d’une beauté touchante,</l>
						<l n="12" num="1.12">Dort aux transports d’un cœur qui le trahit.</l>
						<l n="13" num="1.13">Que l’innocent, armé de sa verloppe,</l>
						<l n="14" num="1.14">Joigne sans art les ais mal assortis</l>
						<l n="15" num="1.15">Du vieux sapin qui forme son échoppe,</l>
						<l n="16" num="1.16">J’en suis fâché : les grâces et les ris,</l>
						<l n="17" num="1.17">Par cette fente en sa couche introduits,</l>
						<l n="18" num="1.18">Des doux plaisirs allumeront l’amorce ;</l>
						<l n="19" num="1.19">Et son honneur, par le ciel compromis,</l>
						<l n="20" num="1.20">Piteusement reçoit plus d’un entorse.</l>
						<l n="21" num="1.21">Quoiqu’en ce monde il soit plus d’un Joseph,</l>
						<l n="22" num="1.22">Au vieux patron le mien point ne ressemble ;</l>
						<l n="23" num="1.23">De son honneur il a gardé la clef ;</l>
						<l n="24" num="1.24">Cornes au front pour lui font triste ensemble ;</l>
						<l n="25" num="1.25">Il n’est besoin, quand l’amour éveillé</l>
						<l n="26" num="1.26">Des voluptés ouvre l’ardente coupe,</l>
						<l n="27" num="1.27">Qu’un doux pigeon tout à coup révélé</l>
						<l n="28" num="1.28">Entre les draps se glisse et monte en poupe ;</l>
						<l n="29" num="1.29">Il n’est pour lui d’esprit si merveilleux,</l>
						<l n="30" num="1.30">Qu’il ne surpasse en exploits amoureux ;</l>
						<l n="31" num="1.31">Prompt sans désirs, il n’attend point qu’un autre</l>
						<l n="32" num="1.32">Cueille en son lieu la rose du plaisir ;</l>
						<l n="33" num="1.33">L’amour n’a point de plus ardent apôtre,</l>
						<l n="34" num="1.34">Et l’amitié de plus noble visir.</l>
						<l n="35" num="1.35">Chantons en chœur, amis, chantons la fête</l>
						<l n="36" num="1.36">De ce Joseph pour nous si précieux ;</l>
						<l n="37" num="1.37">Qu’à le louer chacun de nous s’apprête,</l>
						<l n="38" num="1.38">Qu’un gai refrain charme ce jour heureux.</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="39" num="1.39">Docile aux vœux de son cœur éperdu</l>
						<l n="40" num="1.40">Amour pour lui fait de plus doux miracles,</l>
						<l n="41" num="1.41">Entre ses mains son arc toujours tendu,</l>
						<l n="42" num="1.42">D’un trait brûlant, perce tous les obstacles ;</l>
						<l n="43" num="1.43">Et nul oiseau par l’amour alléché</l>
						<l n="44" num="1.44">N’est en son lit entre deux draps couché,</l>
						<l n="45" num="1.45">Sinon l’oiseau qui, d’une aile légère,</l>
						<l n="46" num="1.46">Message au bec, court au sein des hasards,</l>
						<l n="47" num="1.47">De Cythérée aimable messagère,</l>
						<l n="48" num="1.48">Porter au loin un billet doux à Mars ;</l>
						<l n="49" num="1.49">Ou bien aussi le maître de l’aurore,</l>
						<l n="50" num="1.50">Qui, fier des feux dont son front se décore,</l>
						<l n="51" num="1.51">Avec orgueil chante, au sein de sa cour,</l>
						<l n="52" num="1.52">Les longs transports de son prodigue amour ;</l>
						<l n="53" num="1.53">Ou bien l’oiseau que le bon La Fontaine</l>
						<l n="54" num="1.54">Met dans les mains de certaine beauté,</l>
						<l n="55" num="1.55">Quand tout à coup, de soupçons agité,</l>
						<l n="56" num="1.56">Auprès du lit où la belle incertaine</l>
						<l n="57" num="1.57">Rêve l’amour dont la réalité</l>
						<l n="58" num="1.58">Naguère encor parfumait son haleine ;</l>
						<l n="59" num="1.59">Mère en courroux et respirant à peine,</l>
						<l n="60" num="1.60">Paraît et voit, dans ce simple appareil</l>
						<l n="61" num="1.61">De deux amans que charme le sommeil,</l>
						<l n="62" num="1.62">Sa fille aux bras d’un superbe jeune homme,</l>
						<l n="63" num="1.63">Beau comme Adam avant qu’il eût mangé</l>
						<l n="64" num="1.64">Le pépin vert de la première pomme ;</l>
						<l n="65" num="1.65">Et près de lui, côte à côte rangés,</l>
						<l n="66" num="1.66">Les charmes nus de sa fille endormie,</l>
						<l n="67" num="1.67">Rêvant d’amour, d’espoir et d’insomnie.</l>
					</lg>
				</div></body></text></TEI>