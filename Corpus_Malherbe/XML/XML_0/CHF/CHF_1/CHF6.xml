<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ODES</head><div type="poem" key="CHF6">
					<head type="main">LA GRANDEUR DE L’HOMME</head>
					<head type="form">ODE</head>
					<lg n="1">
						<l n="1" num="1.1">Quand Dieu, du haut du ciel, a promené sa vue</l>
						<l n="2" num="1.2">Sur ces mondes divers, semés dans l’étendue,</l>
						<l n="3" num="1.3">Sur ces nombreux soleils, brillans de sa splendeur,</l>
						<l n="4" num="1.4">Il arrête les yeux sur le globe où nous sommes :</l>
						<l n="5" num="1.5"><space unit="char" quantity="12"></space>Il contemple les hommes,</l>
						<l n="6" num="1.6">Et dans notre âme enfin va chercher sa grandeur.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Apprends de lui, mortel, à respecter ton être.</l>
						<l n="8" num="2.2">Cet orgueil généreux n’offense point ton maître :</l>
						<l n="9" num="2.3">Sentir ta dignité, c’est bénir ses faveurs ;</l>
						<l n="10" num="2.4">Tu dois ce juste hommage à sa bonté suprême :</l>
						<l n="11" num="2.5"><space unit="char" quantity="12"></space>C’est l’oubli de toi-même</l>
						<l n="12" num="2.6">Qui, du sein des forfaits, fit naître tes malheurs.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Mon âme se transporte aux premiers jours du monde</l>
						<l n="14" num="3.2">Est-ce là cette terre, aujourd’hui si féconde ?</l>
						<l n="15" num="3.3">Qu’ai-je vu ? des déserts, des rochers, des forêts :</l>
						<l n="16" num="3.4">Ta faim demande au chêne une vile pâture ;</l>
						<l n="17" num="3.5"><space unit="char" quantity="12"></space>Une caverne obscure</l>
						<l n="18" num="3.6">Du roi de l’univers est le premier palais.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Tout naît, tout s’embellit sous ta main fortunée :</l>
						<l n="20" num="4.2">Ces déserts ne sont plus, et la terre étonnée</l>
						<l n="21" num="4.3">Voit son fertile sein ombragé de moissons.</l>
						<l n="22" num="4.4">Dans ces vastes cités quel pouvoir invincible</l>
						<l n="23" num="4.5"><space unit="char" quantity="12"></space>Dans un calme paisible</l>
						<l n="24" num="4.6">Des humains réunis endort les passions ?</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Le commerce t’appelle au bout de l’hémisphère ;</l>
						<l n="26" num="5.2">L’Océan, sous tes pas, abaisse sa barrière ;</l>
						<l n="27" num="5.3">L’aimant, fidèle au nord, te conduit sur ses eaux ;</l>
						<l n="28" num="5.4">Tu sais l’art d’enchaîner l’Aquilon dans tes voiles ;</l>
						<l n="29" num="5.5"><space unit="char" quantity="12"></space>Tu lis sur les étoiles</l>
						<l n="30" num="5.6">Les routes que le ciel prescrit à tes vaisseaux.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Séparés par les mers, deux continens s’unissent ;</l>
						<l n="32" num="6.2">L’un de l’autre étonnés, l’un de l’autre jouissent ;</l>
						<l n="33" num="6.3">Tu forces la nature à trahir ses secrets ;</l>
						<l n="34" num="6.4">De la terre au soleil tu marques la distance,</l>
						<l n="35" num="6.5"><space unit="char" quantity="12"></space>Et des feux qu’il te lance</l>
						<l n="36" num="6.6">Le prisme audacieux a divisé les traits.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Tes yeux ont mesuré ce ciel qui te couronne ;</l>
						<l n="38" num="7.2">Ta main pèse les airs qu’un long tube emprisonne ;</l>
						<l n="39" num="7.3">La foudre menaçante obéit à tes lois ;</l>
						<l n="40" num="7.4">Un charme impérieux, une force inconnue</l>
						<l n="41" num="7.5"><space unit="char" quantity="12"></space>Arrache de la nue</l>
						<l n="42" num="7.6">Le tonnerre indigné de descendre à ta voix.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">O prodige plus grand ! ô vertu que j’adore !</l>
						<l n="44" num="8.2">C’est par toi que nos cœurs s’ennoblissent encore :</l>
						<l n="45" num="8.3">Quoi ! ma voix chante l’homme, et j’ai pu t’oublier !</l>
						<l n="46" num="8.4">Je célèbre avant toi… Pardonne, beauté pure ;</l>
						<l n="47" num="8.5"><space unit="char" quantity="12"></space>Pardonne cette injure :</l>
						<l n="48" num="8.6">Inspire-moi des sons dignes de l’expier.</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Mes vœux sont entendus : ta main m’ouvre ton temple ;</l>
						<l n="50" num="9.2">Je tombe à vos genoux, héros que je contemple,</l>
						<l n="51" num="9.3">Pères, époux, amis, citoyens vertueux :</l>
						<l n="52" num="9.4">Votre exemple, vos noms, ornement de l’histoire,</l>
						<l n="53" num="9.5"><space unit="char" quantity="12"></space>Consacrés par la gloire,</l>
						<l n="54" num="9.6">Élèvent jusqu’à vous les mortels généreux.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">Là, tranquille au milieu d’une foule abattue,</l>
						<l n="56" num="10.2">Tu me fais, ô Socrate, envier ta ciguë ;</l>
						<l n="57" num="10.3">Là, c’est ce fier Romain, plus grand que son vainqueur ;</l>
						<l n="58" num="10.4">C’est Caton sans courroux déchirant sa blessure :</l>
						<l n="59" num="10.5"><space unit="char" quantity="12"></space>Son âme libre et pure</l>
						<l n="60" num="10.6">S’enfuit loin des tyrans au sein de son auteur.</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">Quelle femme descend sous cette voûte obscure ?</l>
						<l n="62" num="11.2">Son père dans les fers mourait sans nourriture.</l>
						<l n="63" num="11.3">Elle approche… ô tendresse ! amour ingénieux !</l>
						<l n="64" num="11.4">De son lait… se peut-il ? oui, de son propre père</l>
						<l n="65" num="11.5"><space unit="char" quantity="12"></space>Elle devient la mère :</l>
						<l n="66" num="11.6">La nature trompée applaudit à tous deux.</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">Une autre femme, hélas ! près d’un lit de tristesse,</l>
						<l n="68" num="12.2">Pleure un fils expirant, soutien de sa vieillesse ;</l>
						<l n="69" num="12.3">Il lègue à son ami le droit de la nourrir :</l>
						<l n="70" num="12.4">L’ami tombe à ses pieds, et, fier de son partage,</l>
						<l n="71" num="12.5"><space unit="char" quantity="12"></space>Bénit son héritage,</l>
						<l n="72" num="12.6">Et rend grâce à la main qui vient de l’enrichir.</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1">Et si je célébrais d’une voix éloquente</l>
						<l n="74" num="13.2">La vertu couronnée et la vertu mourante,</l>
						<l n="75" num="13.3">Et du monde attendri les bienfaiteurs fameux,</l>
						<l n="76" num="13.4">Et Titus, qu’à genoux tout un peuple environne,</l>
						<l n="77" num="13.5"><space unit="char" quantity="12"></space>Pleurant au pied du trône</l>
						<l n="78" num="13.6">Le jour qu’il a perdu sans faire des heureux ?</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1">Oui, j’ose le penser, ces mortels magnanimes</l>
						<l n="80" num="14.2">Sont honorés, grand Dieu ! de tes regards sublimes.</l>
						<l n="81" num="14.3">Tu ne négliges pas leurs sublimes destins ;</l>
						<l n="82" num="14.4">Tu daignes t’applaudir d’avoir formé leur être,</l>
						<l n="83" num="14.5"><space unit="char" quantity="12"></space>Et ta bonté peut-être</l>
						<l n="84" num="14.6">Pardonne en leur faveur au reste des humains.</l>
					</lg>
				</div></body></text></TEI>