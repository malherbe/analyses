<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CONTES</head><div type="poem" key="CHF33">
					<head type="main">LE CONNAISSEUR</head>
					<lg n="1">
						<l n="1" num="1.1">Que de sots renommés pour l’esprit, pour le goût,</l>
						<l n="2" num="1.2">N’ont eu que des grands airs, du jargon, de l’audace !</l>
						<l n="3" num="1.3">C’est ainsi qu’autrefois maint courtisan surtout</l>
						<l n="4" num="1.4">Cachait bien peu de fond sous beaucoup de surface.</l>
						<l n="5" num="1.5">Nous avons tous connu le célèbre Milfleur,</l>
						<l n="6" num="1.6">Né, comme ses ayeux, duc, riche et connaisseur ;</l>
						<l n="7" num="1.7">Il devait des talens se montrer idolâtre.</l>
						<l n="8" num="1.8">Aussi dans son palais avait-il un théâtre,</l>
						<l n="9" num="1.9">Des bronzes, des tableaux, des médailles en or :</l>
						<l n="10" num="1.10"><space unit="char" quantity="12"></space>Mais son plus cher trésor</l>
						<l n="11" num="1.11">Était un pavillon tapissé de gravures ;</l>
						<l n="12" num="1.12">Il en faisait d’abord admirer les bordures,</l>
						<l n="13" num="1.13">Le sujet, le dessin ; ensuite il s’écriait :</l>
						<l n="14" num="1.14"><space unit="char" quantity="12"></space>« Remarquez, s’il vous plait,</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space>Que toutes sont <hi rend="ital">avant la lettre.</hi> »</l>
						<l n="16" num="1.16"><space unit="char" quantity="12"></space>Or, comme il retenait,</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space>Ou bien qu’il écrivait peut-être,</l>
						<l n="18" num="1.18">Ce qu’en le visitant chaque amateur disait,</l>
						<l n="19" num="1.19"><space unit="char" quantity="12"></space>Et qu’il le répétait ;</l>
						<l n="20" num="1.20">Effleurant des beaux arts la surface agréable,</l>
						<l n="21" num="1.21">Il semblait marier la palme du savant</l>
						<l n="22" num="1.22"><space unit="char" quantity="12"></space>Au bouquet séduisant</l>
						<l n="23" num="1.23"><space unit="char" quantity="12"></space>Du petit maître aimable.</l>
						<l n="24" num="1.24">Une de nos Laïs, un jour, dit-on, s’y prit ;</l>
						<l n="25" num="1.25">Et son cœur partageait l’erreur de son esprit,</l>
						<l n="26" num="1.26">Lorsque Milfleur voulant brusquer cette conquête,</l>
						<l n="27" num="1.27">Écrivit un billet, mais si plat, mais si bête,</l>
						<l n="28" num="1.28"><space unit="char" quantity="12"></space>Que la nymphe en rougit,</l>
						<l n="29" num="1.29"><space unit="char" quantity="12"></space>Et que, dans son dépit,</l>
						<l n="30" num="1.30"><space unit="char" quantity="4"></space>Sur l’enveloppe elle se borne à mettre ;</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space>« Vous n’êtes plus <hi rend="ital">avant la lettre.</hi></l>
					</lg>
				</div></body></text></TEI>