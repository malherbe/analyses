<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CONTES</head><div type="poem" key="CHF34">
					<head type="main">LA PRUDE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="12"></space>Amour et pruderie</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space>Eurent toujours quelque léger débat ;</l>
						<l n="3" num="1.3">La dame par orgueil donne à tout de l’éclat ;</l>
						<l n="4" num="1.4">Puis, je ne sais comment elle fait sa partie,</l>
						<l n="5" num="1.5">Elle finit toujours par avoir le dessous.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">« A propos de cela, messieurs, connaissez-vous</l>
						<l n="7" num="2.2">La prude Arsinoé ? — Qui ? cette présidente</l>
						<l n="8" num="2.3">Dont le cœur a quinze ans, le visage quarante ?</l>
						<l n="9" num="2.4"><space unit="char" quantity="4"></space>— Précisément ; veuve depuis trois mois,</l>
						<l n="10" num="2.5">On la voit convoler pour la troisième fois.</l>
						<l n="11" num="2.6"><space unit="char" quantity="4"></space>Dorval, hier, a fait cette conquête ;</l>
						<l n="12" num="2.7"><space unit="char" quantity="12"></space>Il est intéressant ;</l>
						<l n="13" num="2.8"><space unit="char" quantity="12"></space>Chez le peuple insurgent,</l>
						<l n="14" num="2.9"><space unit="char" quantity="12"></space>Il abattit la tête</l>
						<l n="15" num="2.10"><space unit="char" quantity="12"></space>De maint et maint forban ;</l>
						<l n="16" num="2.11">Et troqua ses deux bras contre un double ruban.</l>
						<l n="17" num="2.12">Je ne vous peindrai pas la modeste grimace,</l>
						<l n="18" num="2.13">Qu’en prononçant son <hi rend="ital">oui</hi>, notre bégueule fit.</l>
						<l n="19" num="2.14">Après bien des façons, la voilà dans son lit ;</l>
						<l n="20" num="2.15">De ceci, de cela, je vous fais <choice hand="RR" type="false verse" reason="analysis"><sic>encore</sic><corr source="none">encor</corr></choice> grâce ;</l>
						<l n="21" num="2.16">Le désir, sous le lin, comme un zéphyr léger,</l>
						<l n="22" num="2.17">Circule en murmurant ; c’est l’heure du berger.</l>
						<l n="23" num="2.18">L’époux était de feu, l’épouse résignée</l>
						<l n="24" num="2.19">Dédiait ses soupirs au dieu de l’hyménée,</l>
						<l n="25" num="2.20">Quand… hélas ! — Vous riez ? Ah ! plaignons-les plutôt.</l>
						<l n="26" num="2.21">Si faudrait-il au moins qu’hymen ne fut manchot.</l>
						<l n="27" num="2.22">Le Tantale nouveau, de la voix et du geste,</l>
						<l n="28" num="2.23">Appelle un prompt secours, que sa position</l>
						<l n="29" num="2.24">Devant tout cœur bien fait, sollicite de reste.</l>
						<l n="30" num="2.25">La volupté dit oui, mais la pudeur dit non.</l>
						<l n="31" num="2.26">On supplie, on refuse, on presse, on boude, on peste ;</l>
						<l n="32" num="2.27">On avance en tremblant un doigt, puis deux, puis trois ;</l>
						<l n="33" num="2.28">Enfin, notre héroïne est réduite aux abois,</l>
						<l n="34" num="2.29">De l’humanité sainte elle écoute la voix ;</l>
						<l n="35" num="2.30">Déjà son protégé l’en payait par deux fois ;</l>
						<l n="36" num="2.31">Quand par un trait nouveau de fine pruderie,</l>
						<l n="37" num="2.32"><space unit="char" quantity="12"></space>La voilà qui s’écrie :</l>
						<l n="38" num="2.33">« Devoir, tu l’as voulu, mais j’en jure par toi !</l>
						<l n="39" num="2.34">L’ôtera qui voudra, ce ne sera pas moi. »</l>
					</lg>
				</div></body></text></TEI>