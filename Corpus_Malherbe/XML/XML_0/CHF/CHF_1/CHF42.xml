<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF42">
					<head type="main">IMITATION D’OVIDE</head>
					<lg n="1">
						<l n="1" num="1.1">Je ne sais point porter de chaînes éternelles,</l>
						<l n="2" num="1.2">Et j’ose me vanter de ma légèreté :</l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space>Quand l’univers nous offre tant de belles,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Pourquoi n’aimer qu’une beauté ?</l>
						<l n="5" num="1.5">Si je vois une fille innocente et tranquille,</l>
						<l n="6" num="1.6">Qui baisse ses regards sur un sein immobile,</l>
						<l n="7" num="1.7">Son timide embarras, sa naïve candeur,</l>
						<l n="8" num="1.8">Sont des pièges cachés qui surprennent mon cœur.</l>
						<l n="9" num="1.9">Si, marchant d’un air leste et la tête assurée,</l>
						<l n="10" num="1.10">Attaquant, provoquant la jeunesse enivrée,</l>
						<l n="11" num="1.11">Laïs vient à paraître, elle enflamme mes sens ;</l>
						<l n="12" num="1.12">J’ai bientôt oublié ma modeste bergère,</l>
						<l n="13" num="1.13">Et c’est la volupté, c’est l’art que je préfère,</l>
						<l n="14" num="1.14">Afin de savourer des plaisirs différens.</l>
						<l n="15" num="1.15">Du haut de sa grandeur, de sa tige éclatante,</l>
						<l n="16" num="1.16">J’aime à faire descendre une superbe amante ;</l>
						<l n="17" num="1.17">Et je crois, triomphant d’elle et de ses aïeux,</l>
						<l n="18" num="1.18">M’élever dans ses bras jusques au sein des dieux.</l>
						<l n="19" num="1.19">Tu n’as pas moins de droits sur mon âme inconstante,</l>
						<l n="20" num="1.20">Toi, dont l’esprit orné rend l’entretien charmant :</l>
						<l n="21" num="1.21">Aux plaisirs de l’amour se borne l’ignorante,</l>
						<l n="22" num="1.22">Et ses soins délicats flattent un tendre amant.</l>
						<l n="23" num="1.23">Que la voix de Cloé me pénètre et me touche !</l>
						<l n="24" num="1.24">Quel plaisir, quand le cœur et l’oreille sont pris,</l>
						<l n="25" num="1.25"><space unit="char" quantity="4"></space>D’interpréter, par un baiser surpris,</l>
						<l n="26" num="1.26">Les sons pleins de douceur qui sortent de sa bouche !</l>
						<l n="27" num="1.27"><space unit="char" quantity="4"></space>Je ne puis voir, sans un trouble soudain,</l>
						<l n="28" num="1.28">Dans les bras d’une belle une harpe enlacée,</l>
						<l n="29" num="1.29">Et mon œil suit en feu, sur la corde pincée,</l>
						<l n="30" num="1.30">Le jeu vif et brillant d’une charmante main.</l>
						<l n="31" num="1.31">Les grâces de Cinthie et sa taille légère</l>
						<l n="32" num="1.32">M’offrent les souvenirs des nymphes de nos bois ;</l>
						<l n="33" num="1.33">Et quand ses pas hardis l’enlèvent de la terre,</l>
						<l n="34" num="1.34">Je voudrais, embrassant sa taille entre mes doigts,</l>
						<l n="35" num="1.35">La porter en triomphe aux bosquets de Cythère.</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space>Le frais matin de la beauté,</l>
						<l n="37" num="1.37"><space unit="char" quantity="8"></space>Les premiers jours de sa naissance,</l>
						<l n="38" num="1.38"><space unit="char" quantity="8"></space>Portent, dans mon sein agité,</l>
						<l n="39" num="1.39"><space unit="char" quantity="8"></space>La plus active effervescence.</l>
						<l n="40" num="1.40"><space unit="char" quantity="4"></space>Son été même a des charmes pour moi.</l>
						<l n="41" num="1.41">O femmes ! je ne vis que pour vous dans le monde ;</l>
						<l n="42" num="1.42">Mais j’aime à partager l’encens que je vous doi,</l>
						<l n="43" num="1.43">Et la brune me rend infidèle à la blonde :</l>
						<l n="44" num="1.44">Mon cœur ne brave pas un seul de vos attraits.</l>
						<l n="45" num="1.45">Enfin, quelque beauté que l’on cite dans Rome,</l>
						<l n="46" num="1.46">Que l’univers possède et l’univers renomme,</l>
						<l n="47" num="1.47">Elle est d’abord l’objet de mes ardens souhaits ;</l>
						<l n="48" num="1.48"><space unit="char" quantity="8"></space>Et comme un nouvel Alexandre,</l>
						<l n="49" num="1.49"><space unit="char" quantity="8"></space>Animé d’un feu tout divin,</l>
						<l n="50" num="1.50">Dans mon ambition, prêt à tout entreprendre,</l>
						<l n="51" num="1.51">Je voudrais conquérir le monde féminin.</l>
					</lg>
				</div></body></text></TEI>