<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CONTES</head><div type="poem" key="CHF17">
					<head type="main">CALCUL PATRIOTIQUE</head>
					<lg n="1">
						<l n="1" num="1.1">Cent mille écus pour la justice !</l>
						<l n="2" num="1.2">Deux cents pour la religion !</l>
						<l n="3" num="1.3">Prêtres, juges, la nation</l>
						<l n="4" num="1.4">Surpaie un peu votre service.</l>
						<l n="5" num="1.5">Mais aussi, vous craignez, dit-on,</l>
						<l n="6" num="1.6">Qu’habilement on ne saisisse</l>
						<l n="7" num="1.7">Cette attrayante occasion</l>
						<l n="8" num="1.8">D’opérer, par suppression</l>
						<l n="9" num="1.9">De maint office et bénéfice,</l>
						<l n="10" num="1.10">Quelque bonification :</l>
						<l n="11" num="1.11">Et vraiment, vous avez raison,</l>
						<l n="12" num="1.12">Plaise au ciel qu’on y réussisse !</l>
						<l n="13" num="1.13">Croire et plaider sont deux impôts</l>
						<l n="14" num="1.14">Que tout peuple met sur lui-même ;</l>
						<l n="15" num="1.15">Aux dépens des heureux travaux</l>
						<l n="16" num="1.16">De Bacchus et de Triptolême ;</l>
						<l n="17" num="1.17">Croire et plaider sont deux besoins</l>
						<l n="18" num="1.18">De notre mince et folle espèce,</l>
						<l n="19" num="1.19">Que la France, dans sa détresse,</l>
						<l n="20" num="1.20">Tâche de satisfaire à moins.</l>
						<l n="21" num="1.21">De nos jours la philosophie</l>
						<l n="22" num="1.22">A porté quelqu’économie</l>
						<l n="23" num="1.23">Dans la dépense du chrétien.</l>
						<l n="24" num="1.24">Mettons de côté l’autre vie :</l>
						<l n="25" num="1.25">Ce qu’on perd en théologie,</l>
						<l n="26" num="1.26">En finance on le gagne bien.</l>
						<l n="27" num="1.27">L’américaine prud’hommie</l>
						<l n="28" num="1.28">Croit très-peu pour ne payer rien.</l>
						<l n="29" num="1.29">Que dites-vous de ce moyen ?</l>
						<l n="30" num="1.30">Il est bien fort pour ma patrie ;</l>
						<l n="31" num="1.31">Mais elle y viendra, je parie.</l>
						<l n="32" num="1.32">En attendant un si grand bien,</l>
						<l n="33" num="1.33">Je me console, en citoyen,</l>
						<l n="34" num="1.34">Des malheurs de la sacristie.</l>
						<l n="35" num="1.35">Courage ! allons, mes chers Français,</l>
						<l n="36" num="1.36">Méritez un second succès :</l>
						<l n="37" num="1.37">Attaquez cette autre manie :</l>
						<l n="38" num="1.38">Émondez l’arbre des procès ;</l>
						<l n="39" num="1.39">Et mettant de même au rabais</l>
						<l n="40" num="1.40">De <hi rend="ital">messieurs</hi> l’avare industrie :</l>
						<l n="41" num="1.41">Économisez sur les frais</l>
						<l n="42" num="1.42">De la seconde maladie,</l>
						<l n="43" num="1.43">Dont nous ne guérissons jamais.</l>
					</lg>
				</div></body></text></TEI>