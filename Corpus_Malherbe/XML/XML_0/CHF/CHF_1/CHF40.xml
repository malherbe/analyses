<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF40">
					<head type="main">L’HEUREUX TEMPS</head>
					<lg n="1">
						<l n="1" num="1.1">Temps heureux où régnaient Louis et Pompadour !</l>
						<l n="2" num="1.2">Temps heureux où chacun ne s’occupait en France</l>
						<l n="3" num="1.3">Que de vers, de romans, de musique, de danse,</l>
						<l n="4" num="1.4">Des prestiges des arts, des douceurs de l’amour !</l>
						<l n="5" num="1.5">Le seul soin qu’on connût était celui de plaire ;</l>
						<l n="6" num="1.6">On dormait deux la nuit, on riait tout le jour ;</l>
						<l n="7" num="1.7">Varier ses plaisirs était l’unique affaire.</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>A midi, dès qu’on s’éveillait,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>Pour nouvelle on se demandait</l>
						<l n="10" num="1.10">Quel enfant de Thalie, ou bien de Melpomène,</l>
						<l n="11" num="1.11">D’un chef-d’œuvre nouveau devait orner la scène ;</l>
						<l n="12" num="1.12">Quel tableau paraîtrait cette année au Salon ;</l>
						<l n="13" num="1.13">Quel marbre s’animait sous l’art de Bouchardon ;</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space>Ou quelle fille de Cythère,</l>
						<l n="15" num="1.15">Astre encore inconnu, levé sur l’horison,</l>
						<l n="16" num="1.16">Commençait du plaisir l’attrayante carrière.</l>
						<l n="17" num="1.17">On courait applaudir Dumesnil ou Clairon,</l>
						<l n="18" num="1.18">Profiter des leçons que nous donnait Voltaire,</l>
						<l n="19" num="1.19">Voir peindre la nature à grands traits par Buffon.</l>
						<l n="20" num="1.20">Du profond Diderot l’éloquence hardie</l>
						<l n="21" num="1.21">Traçait le vaste plan de l’Encyclopédie ;</l>
						<l n="22" num="1.22">Montesquieu nous donnait l’esprit de chaque loi ;</l>
						<l n="23" num="1.23">Nos savans, mesurant la terre et les planètes,</l>
						<l n="24" num="1.24">Éclairant, calculant le retour des comètes,</l>
						<l n="25" num="1.25">Des peuples ignorans calmaient le vain effroi.</l>
						<l n="26" num="1.26">La renommée alors annonçait nos conquêtes ;</l>
						<l n="27" num="1.27">Les dames couronnaient, au milieu de nos fêtes,</l>
						<l n="28" num="1.28">Les vainqueurs de Lawfeld et ceux de Fontenoy.</l>
						<l n="29" num="1.29">Sur le vaisseau public, les passagers tranquilles</l>
						<l n="30" num="1.30">Coulaient leurs jours gaîment dans un heureux repos,</l>
						<l n="31" num="1.31">Et sans se tourmenter de soucis inutiles,</l>
						<l n="32" num="1.32">Sans interroger l’air, et les vents et les flots,</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space>Sans vouloir diriger la flotte,</l>
						<l n="34" num="1.34">Ils laissaient la manœuvre aux mains des matelots,</l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space>Et le gouvernail au pilote.</l>
					</lg>
				</div></body></text></TEI>