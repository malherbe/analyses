<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CONTES</head><div type="poem" key="CHF31">
					<head type="main">LA CONSOLATION DES COCUS</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="4"></space>D’un préambule, ami, je vous dispense,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space>Figurez-vous, au sein de la Provence,</l>
						<l n="3" num="1.3"><space unit="char" quantity="12"></space>Un couvent de nonains,</l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space>Bien desservi par deux Bénédictins,</l>
						<l n="5" num="1.5">Chacun d’eux y remplit son devoir en bon prêtre ;</l>
						<l n="6" num="1.6">L’un absout les péchés ; l’autre les fait commettre.</l>
						<l n="7" num="1.7">Ce dernier, jeune encor, vigoureux compagnon,</l>
						<l n="8" num="1.8"><space unit="char" quantity="4"></space>A très-bon droit nommé père Tampon,</l>
						<l n="9" num="1.9"><space unit="char" quantity="12"></space>Au par-dessus beau sire,</l>
						<l n="10" num="1.10">Était chéri surtout de la mère Alison,</l>
						<l n="11" num="1.11">La fabriquante en chef d’Enfans-Jésus de cire.</l>
						<l n="12" num="1.12">Aussi l’histoire dit, et sans peine on le croit,</l>
						<l n="13" num="1.13">Qu’Enfans-Jésus sortis de sa manufacture,</l>
						<l n="14" num="1.14">Ressemblaient à Tampon toujours par quelqu’endroit,</l>
						<l n="15" num="1.15">Et que cet endroit-là n’était en mignature.</l>
						<l n="16" num="1.16">Mais comme bon chrétien voit tout du bon côté,</l>
						<l n="17" num="1.17"><space unit="char" quantity="4"></space>Il n’était pas une seule béate</l>
						<l n="18" num="1.18">Qui, loin de se choquer de cette disparate,</l>
						<l n="19" num="1.19">N’y crût voir l’attribut de la divinité,</l>
						<l n="20" num="1.20">Et n’eût dit volontiers son bénédicité.</l>
						<l n="21" num="1.21">Tout allait bien enfin, quand la reconnaissance</l>
						<l n="22" num="1.22">Persuada, sans doute, à l’amoureux Tampon,</l>
						<l n="23" num="1.23">Que pour payer les soins de la tendre Alison,</l>
						<l n="24" num="1.24"><space unit="char" quantity="4"></space>Il devait faire aussi sa ressemblance ;</l>
						<l n="25" num="1.25">Et dès le même soir, il ébauche un poupon ;</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space>Ce poupon là n’était de cire ;</l>
						<l n="27" num="1.27">Érgó, point ne fondit : et les nones de rire ;</l>
						<l n="28" num="1.28">J’entends celles qu’Amour tenait sous son empire,</l>
						<l n="29" num="1.29"><space unit="char" quantity="12"></space>Et qui risquaient souvent</l>
						<l n="30" num="1.30">Dans les bras du plaisir pareil événement.</l>
						<l n="31" num="1.31">Les vieilles de gronder, et cela va sans dire ;</l>
						<l n="32" num="1.32">Elles ne faisaient plus un péché si charmant.</l>
						<l n="33" num="1.33">Après maint ris moqueur, mainte antienne fâcheuse,</l>
						<l n="34" num="1.34">Pour la maison des champs, mère Alison partit ;</l>
						<l n="35" num="1.35"><space unit="char" quantity="12"></space>Et la sœur accoucheuse,</l>
						<l n="36" num="1.36">Layette sous le bras, aussitôt la suivit.</l>
						<l n="37" num="1.37">En secret, tant qu’on put, l’accouchement se fit ;</l>
						<l n="38" num="1.38">Le jardinier pourtant en apprit quelque chose ;</l>
						<l n="39" num="1.39">Et ne pouvant garder sur ce point lettre close,</l>
						<l n="40" num="1.40"><space unit="char" quantity="12"></space>Le dimanche suivant,</l>
						<l n="41" num="1.41">En portant le cerfeuil, le concombre, au couvent,</l>
						<l n="42" num="1.42"><space unit="char" quantity="4"></space>Il en lâcha deux mots à la tourière,</l>
						<l n="43" num="1.43">Qui vous le chapitra d’une étrange manière ;</l>
						<l n="44" num="1.44">Et lui montrant un Christ, lui dit : « Pauvre idiot,</l>
						<l n="45" num="1.45">Avec un tel époux, veux-tu qu’une récluse</l>
						<l n="46" num="1.46"><space unit="char" quantity="12"></space>Puisse faire un marmot ?</l>
						<l n="47" num="1.47"><space unit="char" quantity="4"></space>Le rustre alors se prosterne à genoux,</l>
						<l n="48" num="1.48">Et s’écrie : « Ah, bon Dieu ! comme l’on vous abuse ;</l>
						<l n="49" num="1.49">De ces béguines-là si vous êtes l’époux,</l>
						<l n="50" num="1.50">Las ! vous êtes cocu tout aussi bien que nous.</l>
					</lg>
				</div></body></text></TEI>