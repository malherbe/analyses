<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES66" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="1(aabb) 5(abab)">
						<head type="main">POINT D’ADIEU</head>
						<lg n="1" type="quatrain" rhyme="aabb">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="1.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.7" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>dr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="2.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>d</w> <w n="2.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="2.6">s</w>’<w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="2.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="2.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="2.10" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>vr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">n</w>’<w n="3.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>ffr<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="3.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.9">y<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="3.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="3.11" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</rhyme></w>,</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M/mp">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="4.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.4" punct="vg:6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg" caesura="1">e</seg>rs</w>,<caesura></caesura> <w n="4.5">s</w>’<w n="4.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ls</w> <w n="4.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>t</w> <w n="4.9" punct="pt:12">v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>s</rhyme></w>.</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abab">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="5.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="5.3">l</w>’<w n="5.4" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" punct="vg">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.7">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="5.10" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg>r<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="6.5">t</w>’<w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.8">l</w>’<w n="6.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>ti<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg></rhyme></w>,</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="7.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="7.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="7.6">p<seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.7" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11" mp="M">ê</seg>v<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="8.3" punct="vg:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg" caesura="1">e</seg>t</w>,<caesura></caesura> <w n="8.4" punct="ps:9">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="ps" mp="F">e</seg></w>… <w n="8.5">c</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="8.8" punct="pt:12">ti<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pt">en</seg></rhyme></w>.</l>
						</lg>
						<lg n="3" type="quatrain" rhyme="abab">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="1">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="9.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="9.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="9.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ts</w> <w n="9.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10">ein</seg>s</w> <w n="9.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="9.10" punct="pt:12">l<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="Lp">i</seg>s</w>-<w n="10.2" punct="dp:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="dp">oi</seg></w> : <w n="10.3">Qu</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.6" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>rt</w>,<caesura></caesura> <w n="10.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="10.10" punct="vg:10">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="vg">ain</seg>s</w>, <w n="10.11">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>l</w> <w n="10.12" punct="pe:12">s<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pe">o</seg>rt</rhyme></w> !</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.6">j</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ds</w><caesura></caesura> <w n="11.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="11.9">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.10">g<seg phoneme="u" type="vs" value="1" rule="425" place="9">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.11">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="11.12" punct="vg:12">ch<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
							<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="Lp">i</seg>s</w>-<w n="12.2" punct="dp:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="dp">oi</seg></w> : <w n="12.3">Qu</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="12.6" punct="pe:6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="6" punct="pe" caesura="1">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !<caesura></caesura> <w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="12.9" punct="vg:9">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="vg">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="12.11" punct="pt:12">d<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rt</rhyme></w>.</l>
						</lg>
						<lg n="4" type="quatrain" rhyme="abab">
							<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">m</w>’<w n="13.4" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" punct="vg">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.5" punct="pe:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pe" caesura="1">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !<caesura></caesura> <w n="13.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="13.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>x</w> <w n="13.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1" punct="vg:2">Su<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="M">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, <w n="14.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="14.4" punct="vg:6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="14.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="14.7" punct="vg:12">s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
							<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="15.4">m<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>ts</w><caesura></caesura> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="15.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="15.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="15.8" punct="dp:12">br<seg phoneme="y" type="vs" value="1" rule="445" place="11" mp="M">û</seg>l<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
							<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="16.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="16.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="16.6" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>rt</w>,<caesura></caesura> <w n="16.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="16.8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="16.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="16.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="16.11" punct="pe:12">ci<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</rhyme></w> !</l>
						</lg>
						<lg n="5" type="quatrain" rhyme="abab">
							<l n="17" num="5.1" lm="8" met="8"><space quantity="6" unit="char"></space><w n="17.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="17.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="17.4">n</w>’<w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="17.6">qu</w>’<w n="17.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="17.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="17.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="17.10">l</w>’<w n="17.11" punct="vg:8"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="18" num="5.2" lm="8" met="8"><space quantity="6" unit="char"></space><w n="18.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="18.2">l</w>’<w n="18.3"><seg phoneme="e" type="vs" value="1" rule="354" place="2">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="18.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="18.7" punct="dp:8">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ch<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp">i</seg>r</rhyme></w> :</l>
							<l n="19" num="5.3" lm="8" met="8"><space quantity="6" unit="char"></space><w n="19.1">C</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.7">nu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t</w> <w n="19.8" punct="vg:8">s<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="20" num="5.4" lm="8" met="8"><space quantity="6" unit="char"></space><w n="20.1">C</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="20.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="20.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="20.5">qu</w>’<w n="20.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="20.7">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>t</w> <w n="20.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.9" punct="pt:8">fl<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</rhyme></w>.</l>
						</lg>
						<lg n="6" type="quatrain" rhyme="abab">
							<l n="21" num="6.1" lm="12" met="6+6"><w n="21.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="21.4" punct="pt:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt" caesura="1">i</seg>t</w>.<caesura></caesura> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">E</seg>t</w> <w n="21.6" punct="vg:8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>, <w n="21.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="21.8">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="21.9">t</w>’<w n="21.10" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tt<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
							<l n="22" num="6.2" lm="12" met="6+6"><w n="22.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="22.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="22.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="22.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="22.6">qu</w>’<w n="22.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.8"><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="22.9" punct="dp:12">pr<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>v<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="dp">oi</seg>r</rhyme></w> :</l>
							<l n="23" num="6.3" lm="12" met="6+6"><w n="23.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>t</w> <w n="23.2">d</w>’<w n="23.3" punct="pv:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="pv">eu</seg></w> ; <w n="23.4" punct="vg:4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="23.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="23.6">m<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>t</w><caesura></caesura> <w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="23.8">l</w>’<w n="23.9"><seg phoneme="e" type="vs" value="1" rule="353" place="8" mp="M">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg></w> <w n="23.10">d</w>’<w n="23.11"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="23.12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="11">œu</seg>r</w> <w n="23.13" punct="pv:12">t<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
							<l n="24" num="6.4" lm="12" met="6+6"><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="24.4" punct="vg:3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="24.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="24.6">m</w>’<w n="24.7" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">en</seg>ds</w>,<caesura></caesura> <w n="24.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.9">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="24.10" punct="dp:9">cr<seg phoneme="i" type="vs" value="1" rule="469" place="9" punct="dp">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> : <w n="24.11"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">Au</seg></w> <w n="24.12" punct="pe:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pe">oi</seg>r</rhyme></w> !</l>
						</lg>
					</div></body></text></TEI>