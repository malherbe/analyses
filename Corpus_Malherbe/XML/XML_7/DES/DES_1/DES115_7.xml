<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES115" modus="sp" lm_max="8" metProfile="6, 8" form="suite de strophes" schema="1[ababa] 1[ababcbc] 2[ababab]">
						<head type="main">C’EST MOI</head>
						<lg n="1" type="regexp" rhyme="ababaa">
							<l n="1" num="1.1" lm="6" met="6"><space quantity="2" unit="char"></space><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="1.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="2" num="1.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="2.1">S</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="2.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d</w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.7" punct="vg:6">b<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>s</rhyme></w>,</l>
							<l n="3" num="1.3" lm="6" met="6"><space quantity="2" unit="char"></space><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="4" num="1.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="4.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w>-<w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.4" punct="pi:6">v<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pi">oi</seg>x</rhyme></w> ?</l>
							<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w>-<w n="6.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="6.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="6.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="6.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="6.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.8" punct="pi:8">v<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pi">oi</seg>s</rhyme></w> ?</l>
						</lg>
						<lg n="2" type="regexp" rhyme="babcbc">
							<l n="7" num="2.1" lm="6" met="6"><space quantity="2" unit="char"></space><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.3">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">s</w>’<w n="7.5" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ff<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="8" num="2.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="8.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="8.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.5" punct="vg:6">p<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</rhyme></w>,</l>
							<l n="9" num="2.3" lm="6" met="6"><space quantity="2" unit="char"></space><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="9.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.5" punct="vg:6">c<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="345" place="6">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="10" num="2.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="10.1" punct="pe:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="pe">i</seg>s</w> ! <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w>-<w n="10.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="10.6" punct="pi:6">p<rhyme label="c" id="5" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pi">a</seg>s</rhyme></w> ?</l>
							<l n="11" num="2.5" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="11.4" punct="vg:4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4" punct="vg">ein</seg></w>, <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.7" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>c<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="345" place="8">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="12" num="2.6" lm="8" met="8"><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="12.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="2">om</seg></w> <w n="12.3">s</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>xh<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="12.5">t</w>-<w n="12.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="12.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="12.8" punct="pi:8">b<rhyme label="c" id="5" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi">a</seg>s</rhyme></w> ?</l>
						</lg>
						<lg n="3" type="regexp" rhyme="ababab">
							<l n="13" num="3.1" lm="6" met="6"><space quantity="2" unit="char"></space><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="13.3">l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="13.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="13.5">t</w>’<w n="13.6" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<rhyme label="a" id="6" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="14" num="3.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="14.1">T</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="14.3">t</w>-<w n="14.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="14.6" punct="pi:6">v<rhyme label="b" id="7" gender="m" type="a" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="248" place="6" punct="pi">œu</seg>x</rhyme></w> ?</l>
							<l n="15" num="3.3" lm="6" met="6"><space quantity="2" unit="char"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<rhyme label="a" id="6" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="16" num="3.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="16.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.4" punct="vg:6">y<rhyme label="b" id="7" gender="m" type="e" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</rhyme></w>,</l>
							<l n="17" num="3.5" lm="8" met="8"><w n="17.1">N</w>’<w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="17.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="17.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="17.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="17.7" punct="pi:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="a" id="6" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
							<l n="18" num="3.6" lm="8" met="8"><w n="18.1">N</w>’<w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="18.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="18.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.8" punct="pi:8">v<rhyme label="b" id="7" gender="m" type="a" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pi">eu</seg>x</rhyme></w> ?</l>
						</lg>
						<lg n="4" type="regexp" rhyme="ababab">
							<l n="19" num="4.1" lm="6" met="6"><space quantity="2" unit="char"></space><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="19.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3" punct="vg:6">fr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<rhyme label="a" id="8" gender="f" type="a" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="20" num="4.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="20.1">L</w>’<w n="20.2"><seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="20.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="20.6" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<rhyme label="b" id="9" gender="m" type="a" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</rhyme></w>,</l>
							<l n="21" num="4.3" lm="6" met="6"><space quantity="2" unit="char"></space><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="21.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3" punct="vg:6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>ss<rhyme label="a" id="8" gender="f" type="e" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="22" num="4.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="22.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="22.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="22.5" punct="pv:6">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<rhyme label="b" id="9" gender="m" type="e" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pv">an</seg>t</rhyme></w> ;</l>
							<l n="23" num="4.5" lm="8" met="8"><w n="23.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w>-<w n="23.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="23.3" punct="vg:3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="23.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="23.6">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bs<rhyme label="a" id="8" gender="f" type="a" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
							<l n="24" num="4.6" lm="8" met="8"><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="24.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="24.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="24.5">t</w>’<w n="24.6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="24.8" punct="pt:8">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>r<rhyme label="b" id="9" gender="m" type="a" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>