<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES112" modus="sp" lm_max="8" metProfile="6, 8" form="suite de strophes" schema="2[aaa] 1[abba] 1[abbba] 2[aa] 1[abbaa]">
						<head type="main">L’ORAGE</head>
						<lg n="1" type="regexp" rhyme="aaaabbaa">
							<l n="1" num="1.1" lm="6" met="6"><space quantity="2" unit="char"></space><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4" punct="vg:6">br<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg>l<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="2.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="2.6" punct="pe:6">l<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
							<l n="3" num="1.3" lm="6" met="6"><space quantity="2" unit="char"></space><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="3.4" punct="vg:6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="4" num="1.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">l</w>’<w n="4.4" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="a" id="2" gender="m" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>r</rhyme></w>.</l>
							<l n="5" num="1.5" lm="6" met="6"><space quantity="2" unit="char"></space><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="5.3">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="5.4">l</w>’<w n="5.5" punct="pv:6"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<rhyme label="b" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="6" num="1.6" lm="6" met="6"><space quantity="2" unit="char"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.4" punct="pt:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<rhyme label="b" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
							<l n="7" num="1.7" lm="8" met="8"><w n="7.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w>-<w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ls</w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">j<rhyme label="a" id="2" gender="m" type="e" stanza="2"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></w></l>
							<l n="8" num="1.8" lm="8" met="8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="8.6">d</w>’<w n="8.7" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pi">ou</seg>r</rhyme></w> ?</l>
						</lg>
						<lg n="2" type="regexp" rhyme="bbbaaaaa">
							<l n="9" num="2.1" lm="6" met="6"><space quantity="2" unit="char"></space><w n="9.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.2"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5" punct="vg:6">pl<rhyme label="b" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="10" num="2.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="10.1" punct="vg:2">F<seg phoneme="ɥi" type="vs" value="1" rule="462" place="1">u</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="10.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="10.3" punct="pv:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<rhyme label="b" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="11" num="2.3" lm="6" met="6"><space quantity="2" unit="char"></space><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="11.2">m</w>’<w n="11.3"><seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="11.5">s<rhyme label="b" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="12" num="2.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="12.4">l</w>’<w n="12.5" punct="pi:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pi">ou</seg>r</rhyme></w> ?</l>
							<l n="13" num="2.5" lm="6" met="6"><space quantity="2" unit="char"></space><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="13.2">m<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="13.5" punct="vg:6"><rhyme label="a" id="6" gender="f" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="14" num="2.6" lm="6" met="6"><space quantity="2" unit="char"></space><w n="14.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2" punct="vg:2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="14.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5" punct="vg:6">fl<rhyme label="a" id="6" gender="f" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="15" num="2.7" lm="8" met="8"><w n="15.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w>-<w n="15.2" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>ls</w>, <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.5" punct="vg:8">j<rhyme label="a" id="7" gender="m" type="a" stanza="5"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
							<l n="16" num="2.8" lm="8" met="8"><w n="16.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="16.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="16.5">d</w>’<w n="16.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="7" gender="m" type="e" stanza="5"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>r</rhyme></w> !</l>
						</lg>
						<lg n="3" type="regexp" rhyme="aaaabbaa">
							<l n="17" num="3.1" lm="6" met="6"><space quantity="2" unit="char"></space><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="17.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>r<rhyme label="a" id="8" gender="f" type="a" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="18" num="3.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="18.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="18.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<rhyme label="a" id="8" gender="f" type="e" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="19" num="3.3" lm="6" met="6"><space quantity="2" unit="char"></space><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="19.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.5">j</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<rhyme label="a" id="8" gender="f" type="a" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="20" num="3.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="20.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="20.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="20.4">l</w>’<w n="20.5" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="a" id="9" gender="m" type="a" stanza="7"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>r</rhyme></w>.</l>
							<l n="21" num="3.5" lm="6" met="6"><space quantity="2" unit="char"></space><w n="21.1">L</w>’<w n="21.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="21.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rm<rhyme label="b" id="10" gender="f" type="a" stanza="7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="22" num="3.6" lm="6" met="6"><space quantity="2" unit="char"></space><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="22.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="22.5">l</w>’<w n="22.6" punct="dp:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="b" id="10" gender="f" type="e" stanza="7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></w> :</l>
							<l n="23" num="3.7" lm="8" met="8"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="23.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="23.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="23.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="23.5">j</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ds</w> <w n="23.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.8" punct="vg:8">j<rhyme label="a" id="9" gender="m" type="e" stanza="7"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
							<l n="24" num="3.8" lm="8" met="8"><w n="24.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w>-<w n="24.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="24.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="24.5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="24.6">d</w>’<w n="24.7" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="9" gender="m" type="a" stanza="7"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pi">ou</seg>r</rhyme></w> ?</l>
						</lg>
					</div></body></text></TEI>