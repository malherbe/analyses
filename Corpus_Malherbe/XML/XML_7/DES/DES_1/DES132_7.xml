<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES132" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="4(abab)">
						<head type="main">UN BEAU JOUR</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="1" num="1.1" lm="7" met="7"><w n="1.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="1.2" punct="pe:3">M<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="pe">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="1.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5" punct="pt:7">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
							<l n="2" num="1.2" lm="7" met="7"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.4" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>rs</rhyme></w>,</l>
							<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="3.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.5">ch<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
							<l n="4" num="1.4" lm="7" met="7"><w n="4.1">M</w>’<w n="4.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w> <w n="4.5" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pt">ou</seg>rs</rhyme></w>.</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abab">
							<l n="5" num="2.1" lm="7" met="7"><w n="5.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="5.2" punct="vg:4">l<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="5.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.5">ch<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
							<l n="6" num="2.2" lm="7" met="7"><w n="6.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="6.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.5" punct="pv:7">pl<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pv">eu</seg>rs</rhyme></w> ;</l>
							<l n="7" num="2.3" lm="7" met="7"><w n="7.1">L</w>’<w n="7.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="7.3">qu</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="7.6" punct="vg:7">l<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
							<l n="8" num="2.4" lm="7" met="7"><w n="8.1">T</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="8.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.5" punct="pt:7">fl<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pt">eu</seg>rs</rhyme></w>.</l>
						</lg>
						<lg n="3" type="quatrain" rhyme="abab">
							<l n="9" num="3.1" lm="7" met="7"><w n="9.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="9.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3" punct="vg:7">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="10" num="3.2" lm="7" met="7"><w n="10.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.4" punct="pe:7">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pt<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pe">é</seg></rhyme></w> !</l>
							<l n="11" num="3.3" lm="7" met="7"><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="469" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
							<l n="12" num="3.4" lm="7" met="7"><w n="12.1">S</w>’<w n="12.2"><seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></rhyme></w></l>
						</lg>
						<lg n="4" type="quatrain" rhyme="abab">
							<l n="13" num="4.1" lm="7" met="7"><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="13.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">n</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="13.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="13.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.8" punct="vg:7">f<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="14" num="4.2" lm="7" met="7"><w n="14.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="14.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="14.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="14.4" punct="pe:7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" punct="pe">em</seg>ps</rhyme></w> !</l>
							<l n="15" num="4.3" lm="7" met="7"><w n="15.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="15.2">l</w>’<w n="15.3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.4">s</w>’<w n="15.5" punct="pv:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cl<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="16" num="4.4" lm="7" met="7"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="16.3" punct="dp:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="dp">aî</seg>t</w> : <w n="16.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="16.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="16.6" punct="pe:7">t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" punct="pe">em</seg>ps</rhyme></w> !</l>
						</lg>
					</div></body></text></TEI>