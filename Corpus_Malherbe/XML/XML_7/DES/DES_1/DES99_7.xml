<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES99" modus="sm" lm_max="6" metProfile="6" form="suite périodique" schema="4(abbacddc)">
						<head type="main">LA FIANCÉE</head>
						<lg n="1" type="huitain" rhyme="abbacddc">
							<l n="1" num="1.1" lm="6" met="6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="1.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="469" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="2" num="1.2" lm="6" met="6"><w n="2.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="2.4" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">A</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pv">ou</seg>r</rhyme></w> ;</l>
							<l n="3" num="1.3" lm="6" met="6"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w>-<w n="3.3">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.5" punct="vg:6">j<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</rhyme></w>,</l>
							<l n="4" num="1.4" lm="6" met="6"><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3" punct="pi:6">ch<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></rhyme></w> ?</l>
							<l n="5" num="1.5" lm="6" met="6"><w n="5.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.3">m</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="5.6" punct="vg:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></rhyme></w>,</l>
							<l n="6" num="1.6" lm="6" met="6"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="6.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="6.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.5" punct="dp:6">m<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></w> :</l>
							<l n="7" num="1.7" lm="6" met="6"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">m</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.4">d<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="7.5">l</w>’<w n="7.6" punct="vg:6"><rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="8" num="1.8" lm="6" met="6"><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="8.5" punct="pt:6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt">er</seg></rhyme></w>.</l>
						</lg>
						<lg n="2" type="huitain" rhyme="abbacddc">
							<l n="9" num="2.1" lm="6" met="6"><w n="9.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w>-<w n="9.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="9.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.6">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="10" num="2.2" lm="6" met="6"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>s</w> <w n="10.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>rs</rhyme></w>,</l>
							<l n="11" num="2.3" lm="6" met="6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="11.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rs</rhyme></w></l>
							<l n="12" num="2.4" lm="6" met="6"><w n="12.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">m</w>’<w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="12.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="12.6" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ti<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="13" num="2.5" lm="6" met="6"><w n="13.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w>-<w n="13.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="13.5">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<rhyme label="c" id="7" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></rhyme></w></l>
							<l n="14" num="2.6" lm="6" met="6"><w n="14.1">N</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="14.4">s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="14.6" punct="vg:6">l<rhyme label="d" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></w>,</l>
							<l n="15" num="2.7" lm="6" met="6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">qu</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="15.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="15.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="15.6">ch<rhyme label="d" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
							<l n="16" num="2.8" lm="6" met="6"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="16.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="16.5" punct="pt:6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<rhyme label="c" id="7" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pt">eau</seg></rhyme></w>.</l>
						</lg>
						<lg n="3" type="huitain" rhyme="abbacddc">
							<l n="17" num="3.1" lm="6" met="6"><w n="17.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w>-<w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="17.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="18" num="3.2" lm="6" met="6"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="18.4" punct="pi:6">pl<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pi">eu</seg>rs</rhyme></w> ?</l>
							<l n="19" num="3.3" lm="6" met="6"><w n="19.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="19.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="19.3">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="19.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="19.5" punct="vg:6">fl<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>rs</rhyme></w>,</l>
							<l n="20" num="3.4" lm="6" met="6"><w n="20.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="20.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="20.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.4" punct="pt:6"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
							<l n="21" num="3.5" lm="6" met="6"><w n="21.1">L</w>’<w n="21.2"><seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="21.3">m</w>’<w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="21.6" punct="ps:6">t<rhyme label="c" id="11" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="ps">oi</seg></rhyme></w>…</l>
							<l n="22" num="3.6" lm="6" met="6"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="22.2" punct="vg:3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3" punct="vg">ain</seg></w>, <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="22.4">l</w>’<w n="22.5" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>r<rhyme label="d" id="12" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="23" num="3.7" lm="6" met="6"><w n="23.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w>-<w n="23.3">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<rhyme label="d" id="12" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="24" num="3.8" lm="6" met="6"><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="24.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="24.5" punct="pi:6">m<rhyme label="c" id="11" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pi">oi</seg></rhyme></w> ?</l>
						</lg>
						<lg n="4" type="huitain" rhyme="abbacddc">
							<l n="25" num="4.1" lm="6" met="6"><w n="25.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="25.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="25.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>x</w> <w n="25.5">t<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
							<l n="26" num="4.2" lm="6" met="6"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="26.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="26.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="26.4" punct="pi:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pi">ou</seg>r</rhyme></w> ?</l>
							<l n="27" num="4.3" lm="6" met="6"><w n="27.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="27.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="27.3" punct="vg:4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4" punct="vg">en</seg></w>, <w n="27.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">A</seg>m<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</rhyme></w>,</l>
							<l n="28" num="4.4" lm="6" met="6"><w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="28.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="28.4">l</w>’<w n="28.5" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="29" num="4.5" lm="6" met="6"><w n="29.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="29.2">n</w>’<w n="29.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="29.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="29.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="29.6" punct="vg:6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<rhyme label="c" id="15" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>x</rhyme></w>,</l>
							<l n="30" num="4.6" lm="6" met="6"><w n="30.1">C</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="30.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="30.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="30.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="30.7" punct="pe:6">p<rhyme label="d" id="16" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
							<l n="31" num="4.7" lm="6" met="6"><w n="31.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="31.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="31.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="31.4" punct="vg:6">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>sp<rhyme label="d" id="16" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
							<l n="32" num="4.8" lm="6" met="6"><w n="32.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="32.3">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="32.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="32.5" punct="pt:6">n<rhyme label="c" id="15" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>s</rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>