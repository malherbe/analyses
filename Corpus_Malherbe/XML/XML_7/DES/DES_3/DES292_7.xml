<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES292" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="5(abab)">
				<head type="main">DORS !</head>
				<opener>
					<salute>Dieu donne l’intelligence aux petits.</salute>
					</opener>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="1.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="1.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="1.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="1.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="1.10" punct="pv:12">v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="2.3">pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="2.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="2.6" punct="pv:6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="pv" caesura="1">o</seg>rt</w> ;<caesura></caesura> <w n="2.7">j</w>’<w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="2.9">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="8" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="2.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.11">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="2.12" punct="pv:12">pl<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>rs</rhyme></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="3.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.5" punct="vg:6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="3.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.8">l</w>’<w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="3.10" punct="pv:12">su<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="4.4" punct="vg:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg>s</w>,<caesura></caesura> <w n="4.5">j</w>’<w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="4.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="4.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="4.10" punct="pt:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="5.4">l</w>’<w n="5.5" punct="pi:6"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pi" caesura="1">é</seg></w> ?<caesura></caesura> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="5.8">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d</w> <w n="5.9">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="5.10"><seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.11" punct="pe:12"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="6.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="6.5" punct="pv:6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>bt<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pv" caesura="1">u</seg></w> ;<caesura></caesura> <w n="6.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></w> <w n="6.7" punct="pv:9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="pv">é</seg></w> ; <w n="6.8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10">en</seg></w> <w n="6.9" punct="dp:12">gu<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="dp">i</seg></rhyme></w> :</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="7.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="7.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.7">qu</w>’<w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="7.9">s<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="7.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.11" punct="vg:12">fl<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="8.4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="M">oi</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="8.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.6">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>d</w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="8.8" punct="pt:12">m<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg>rtr<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="9.6" punct="dp:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="dp" caesura="1">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> :<caesura></caesura> <w n="9.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</w> ! <w n="9.8" punct="vg:9">m<seg phoneme="wa" type="vs" value="1" rule="423" place="9" punct="vg">oi</seg></w>, <w n="9.9">j</w>’<w n="9.10"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.11" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="10.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="10.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="10.8" punct="dp:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="dp">in</seg></rhyme></w> :</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.7">l</w>’<w n="11.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>r<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="12.2">j</w>’<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="12.5" punct="pv:6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pv" caesura="1">eu</seg>x</w> ;<caesura></caesura> <w n="12.6" punct="vg:7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>, <w n="12.7">j</w>’<w n="12.8"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="12.10" punct="pt:12">m<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">ain</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="13.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.9">br<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">L</w>’<w n="14.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="14.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="14.6" punct="dp:12">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="dp">eu</seg>rs</rhyme></w> :</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="15.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.5">fl<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="15.7">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="15.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="15.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="15.10" punct="pv:12">br<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1" punct="pe:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="pe">o</seg>rs</w> ! <w n="16.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="16.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="16.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="16.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="16.8">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="16.9">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>t</w> <w n="16.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="16.11" punct="pt:12">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="17.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="17.6">n</w>’<w n="17.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d</w> <w n="17.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="17.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="17.10" punct="vg:12"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">En</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="18.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="18.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="18.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="18.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="18.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="18.7" punct="vg:12">b<seg phoneme="o" type="vs" value="1" rule="315" place="11" mp="M">eau</seg>t<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
					<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="19.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="19.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="19.6" punct="dp:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11" mp="M">in</seg>c<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg>s</rhyme></w> :</l>
					<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="20.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="20.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="20.4">l</w>’<w n="20.5">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="4">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="20.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">am</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>m<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="20.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="20.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="20.10" punct="pt:12">c<seg phoneme="o" type="vs" value="1" rule="415" place="11" mp="M">ô</seg>t<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>