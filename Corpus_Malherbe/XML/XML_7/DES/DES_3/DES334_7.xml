<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES334" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="4(abbaab)">
				<head type="main">L’ENFANT ABANDONNÉ</head>
				<lg n="1" type="sizain" rhyme="abbaab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="1.3" punct="pe:4">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe" mp="F">e</seg></w> ! <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="1.5" punct="pe:6">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="pe" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !<caesura></caesura> <w n="1.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="1.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="1.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="1.9" punct="pi:12">p<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="2.2" punct="vg:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="2.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="2.4">j</w>’<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="2.6" punct="vg:6">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="2.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="2.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="9">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>c</w> <w n="2.10" punct="pv:12">lu<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pv">i</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="3.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="3.3">qu</w>’<w n="3.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="3.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>x</w> <w n="3.9" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ppu<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pv">i</seg></rhyme></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="4.2" punct="pv:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="pv">ai</seg>t</w> ; <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M/mp">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="4.4" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="4.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="4.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="4.8" punct="dp:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>sp<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="5.3" punct="pe:4">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe" mp="F">e</seg></w> ! <w n="5.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="5.5" punct="pe:6">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="pe" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !<caesura></caesura> <w n="5.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="5.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="5.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="5.9" punct="pi:12">p<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="6.3" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>rs</w>, <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="6.5" punct="pe:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pe" caesura="1">ou</seg>r</w> !<caesura></caesura> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">j<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="6.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="Lc">ou</seg>rd</w>’<w n="6.9" punct="pe:12">hu<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pe">i</seg></rhyme></w> !</l>
				</lg>
				<lg n="2" type="sizain" rhyme="abbaab">
					<l n="7" num="2.1" lm="12" met="6+6"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="7.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="7.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.8">v<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="7.10" punct="vg:12">gr<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="8" num="2.2" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2" punct="pe:2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.4" punct="pe:4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pe">oi</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="8.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="8.9">cr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="8.10" punct="pv:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rfl<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pv">u</seg>s</rhyme></w> ;</l>
					<l n="9" num="2.3" lm="12" met="6+6"><w n="9.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="9.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>d</w><caesura></caesura> <w n="9.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="9.6" punct="pv:12"><seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pv">u</seg>s</rhyme></w> ;</l>
					<l n="10" num="2.4" lm="12" met="6+6"><w n="10.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="10.4">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="10.8">qu</w>’<w n="10.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="10.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="10.11">lu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="C">i</seg></w> <w n="10.12" punct="pe:12">r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="11" num="2.5" lm="12" met="6+6"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="11.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="11.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="11.8">v<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="11.10" punct="dp:12">gr<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
					<l n="12" num="2.6" lm="12" met="6+6"><w n="12.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="12.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="12.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.8">s<seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s</w> <w n="12.9" punct="pe:12">pl<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pe">u</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="3" type="sizain" rhyme="abbaab">
					<l n="13" num="3.1" lm="12" met="6+6"><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="13.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="13.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="13.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="13.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.9" punct="vg:12">f<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="3.2" lm="12" met="6+6"><w n="14.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="14.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="14.3">m</w>’<w n="14.4" punct="dp:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="dp" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> :<caesura></caesura> <w n="14.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="14.6">br<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="14.9" punct="pe:12">d<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>x</rhyme></w> !</l>
					<l n="15" num="3.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="15.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="15.4" punct="pv:6"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pv" caesura="1">i</seg></w> ;<caesura></caesura> <w n="15.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="15.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="15.7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt</w> <w n="15.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>c</w> <w n="15.9" punct="vg:12">v<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</rhyme></w>,</l>
					<l n="16" num="3.4" lm="12" met="6+6"><w n="16.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="16.2" punct="pe:2">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="pe">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="16.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="16.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="16.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="16.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="16.8">nu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t</w> <w n="16.9">s</w>’<w n="16.10" punct="dp:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>c<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
					<l n="17" num="3.5" lm="12" met="6+6"><w n="17.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="17.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="17.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="17.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="17.9" punct="vg:12">f<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="18" num="3.6" lm="12" met="6+6"><w n="18.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="18.2" punct="vg:3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="vg">em</seg>ps</w>, <w n="18.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="18.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="18.5" punct="vg:6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>l</w>,<caesura></caesura> <w n="18.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="18.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="18.9" punct="pe:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>x</rhyme></w> !</l>
				</lg>
				<lg n="4" type="sizain" rhyme="abbaab">
					<l n="19" num="4.1" lm="12" met="6+6"><w n="19.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="19.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="19.3">p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" mp="M">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="19.5">j</w>’<w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="19.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="19.8">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="19.9" punct="pv:12">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
					<l n="20" num="4.2" lm="12" met="6+6"><w n="20.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="20.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="20.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="20.4" punct="dp:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="dp" caesura="1">ai</seg>t</w> :<caesura></caesura> <w n="20.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="20.6">l</w>’<w n="20.7"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="20.8">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d</w> <w n="20.9">j</w>’<w n="20.10"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="11">ai</seg></w> <w n="20.11" punct="pv:12">p<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>r</rhyme></w> ;</l>
					<l n="21" num="4.3" lm="12" met="6+6"><w n="21.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="21.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="21.3">l</w>’<w n="21.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="21.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="21.6">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="21.7">qu</w>’<w n="21.8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="21.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="21.10">ch<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>d</w> <w n="21.11">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="21.12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="21.13" punct="pv:12">c<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pv">œu</seg>r</rhyme></w> ;</l>
					<l n="22" num="4.4" lm="12" met="6+6"><w n="22.1">C</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="22.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="22.6">d</w>’<w n="22.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="22.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="P">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.9"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="22.10"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="22.11">m</w> <w n="22.12"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="10">e</seg>s</w> <w n="22.13" punct="dp:12">p<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>pi<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg>s</rhyme></w> :</l>
					<l n="23" num="4.5" lm="12" met="6+6"><w n="23.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="23.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="23.3">p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" mp="M">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="23.5">j</w>’<w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="23.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="23.8">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="23.9" punct="vg:12">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="24" num="4.6" lm="12" met="6+6"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="24.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg></w><caesura></caesura> <w n="24.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="24.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="24.8" punct="pe:12">Cr<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pe">eu</seg>r</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>