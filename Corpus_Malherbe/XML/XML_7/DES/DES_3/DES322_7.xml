<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES322" modus="cm" lm_max="10" metProfile="5+5" form="suite périodique" schema="4(aabb)">
				<head type="main">RAHEL LA CRÉOLE</head>
				<lg n="1" type="quatrain" rhyme="aabb">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="1.2" punct="vg:2">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="1.4" punct="pe:5">R<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="pe" caesura="1">e</seg>l</w> !<caesura></caesura> <w n="1.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="1.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="1.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.8" punct="pe:10">f<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-27" place="2" mp="F">e</seg></w> <w n="2.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="2.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="2.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>t</w><caesura></caesura> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="2.7">p<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="2.8" punct="pt:10">br<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="3.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="3.7">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="3.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="3.9" punct="pv:10">p<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="pv">o</seg>rt</rhyme></w> ;</l>
					<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="4.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="2">om</seg></w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="4.4" punct="dp:5">b<seg phoneme="o" type="vs" value="1" rule="315" place="4" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="dp" caesura="1">é</seg></w> :<caesura></caesura> <w n="4.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="4.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>x</w> <w n="4.7" punct="pe:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>p<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="pe">o</seg>rt</rhyme></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="aabb">
					<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>s</w>, <w n="5.4" punct="vg:5">R<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="5.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="5.8">l</w>’<w n="5.9" punct="vg:10"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.4">sc<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="6.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" caesura="1">e</seg>l</w><caesura></caesura> <w n="6.7">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="6.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="6.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="6.10" punct="pv:10">v<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>s<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2">gr<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>t</w><caesura></caesura> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="7.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="7.8" punct="dp:10">fi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rt<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="dp">é</seg></rhyme></w> :</l>
					<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1" punct="vg:2">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2" punct="vg">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="8.3" punct="vg:5">R<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="8.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="8.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.6" punct="pe:10">b<seg phoneme="o" type="vs" value="1" rule="315" place="9" mp="M">eau</seg>t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pe">é</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="aabb">
					<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fm">e</seg></w>-<w n="9.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="9.3">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="4" mp="M">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="P">a</seg>r</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.6" punct="pv:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>mm<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="10.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5" caesura="1">oin</seg></w><caesura></caesura> <w n="10.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="10.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.8" punct="pt:10">f<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="M">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="11.6">d</w>’<w n="11.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.8" punct="dp:10">fl<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="dp">eu</seg>r</rhyme></w> :</l>
					<l n="12" num="3.4" lm="10" met="5+5"><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fm">e</seg></w>-<w n="12.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="12.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" mp="P">e</seg>rs</w> <w n="12.4" punct="vg:5">F<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg" caesura="1">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="12.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="12.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="12.9" punct="pt:10">c<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="pt">œu</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="aabb">
					<l n="13" num="4.1" lm="10" met="5+5"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="13.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="13.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" caesura="1">an</seg>s</w><caesura></caesura> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="13.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="13.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="13.7" punct="vg:10">tr<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="4.2" lm="10" met="5+5"><w n="14.1">S</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ls</w> <w n="14.3">v<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="14.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>t</w><caesura></caesura> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w> <w n="14.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="14.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="14.9" punct="vg:10">gr<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="15" num="4.3" lm="10" met="5+5"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="Lp">ai</seg>s</w>-<w n="15.3" punct="vg:3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg></w>, <w n="15.4" punct="pi:5">R<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="pi" caesura="1">e</seg>l</w> ?<caesura></caesura> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="15.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="15.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="15.8" punct="vg:10">tr<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>p<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg></rhyme></w>,</l>
					<l n="16" num="4.4" lm="10" met="5+5"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="16.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="16.4">m<seg phoneme="e" type="vs" value="1" rule="353" place="4" mp="M">e</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>t</w><caesura></caesura> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="16.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="16.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg></w> <w n="16.8" punct="pt:10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>p<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pt">eau</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>