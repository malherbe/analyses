<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES333" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(aabbcc)">
				<head type="main">L’ENFANT BÉNI</head>
				<head type="sub_1">À MARIE B…</head>
				<lg n="1" type="sizain" rhyme="aabbcc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="1.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>d</rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w>-<w n="2.4" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="2.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="2.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.4">ch<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.5" punct="vg:8">j<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2" punct="vg:3">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg>x</w>, <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="4.5" punct="vg:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Qu</w>’<w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.3">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w>-<w n="5.8" punct="pv:8">b<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>s</rhyme></w> ;</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="6.3" punct="pv:4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pv">ez</seg></w> ; <w n="6.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="6.6" punct="pt:8">p<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabbcc">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="7.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="7.6" punct="pv:8">d<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pv">o</seg>rt</rhyme></w> ;</l>
					<l n="8" num="2.2" lm="8" met="8"><w n="8.1">J<seg phoneme="u" type="vs" value="1" rule="d-2" place="1">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="8.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="8.6">d</w>’<w n="8.7" punct="dp:8"><rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="dp">o</seg>r</rhyme></w> :</l>
					<l n="9" num="2.3" lm="8" met="8"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="9.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="9.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="9.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="10.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="10.5" punct="pe:8">b<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
					<l n="11" num="2.5" lm="8" met="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">f<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="11.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="11.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.7" punct="dp:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ch<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="dp">er</seg></rhyme></w> :</l>
					<l n="12" num="2.6" lm="8" met="8"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w> <w n="12.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="12.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="12.7" punct="pt:8">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rch<rhyme label="c" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabbcc">
					<l n="13" num="3.1" lm="8" met="8"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="13.2">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="13.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t</w> <w n="13.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="13.6" punct="vg:8">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</rhyme></w>,</l>
					<l n="14" num="3.2" lm="8" met="8"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="14.2">m</w>’<w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="14.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg></w> <w n="14.6" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</rhyme></w>.</l>
					<l n="15" num="3.3" lm="8" met="8"><w n="15.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="15.6" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="3.4" lm="8" met="8"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="16.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="16.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="16.5">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="16.6" punct="pv:8">s<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="17" num="3.5" lm="8" met="8"><w n="17.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="17.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="17.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.6" punct="vg:8">r<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></rhyme></w>,</l>
					<l n="18" num="3.6" lm="8" met="8"><w n="18.1">N</w>’<w n="18.2"><seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="18.3">qu</w>’<w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="18.5" punct="vg:4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></w>, <w n="18.6">n</w>’<w n="18.7"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="18.8">qu</w>’<w n="18.9"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="18.10" punct="pe:8">m<rhyme label="c" id="9" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></rhyme></w> !</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabbcc">
					<l n="19" num="4.1" lm="8" met="8"><w n="19.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="19.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="19.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="19.5" punct="vg:8">b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>t</rhyme></w>,</l>
					<l n="20" num="4.2" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="20.4">bl<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="20.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="20.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.7" punct="vg:8">n<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>d</rhyme></w>,</l>
					<l n="21" num="4.3" lm="8" met="8"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="21.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="21.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="21.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="21.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="21.7" punct="pv:8">t<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="22" num="4.4" lm="8" met="8"><w n="22.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="22.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="22.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="22.5">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg></w> <w n="22.6" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="23" num="4.5" lm="8" met="8"><w n="23.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="23.2">fru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="23.3" punct="vg:3">m<seg phoneme="y" type="vs" value="1" rule="445" place="3" punct="vg">û</seg>r</w>, <w n="23.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="23.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="23.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.7" punct="vg:8">fr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="vg">en</seg>t</rhyme></w>,</l>
					<l n="24" num="4.6" lm="8" met="8"><w n="24.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="24.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="24.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="24.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="24.5" punct="pe:8">g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>m<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="pe">en</seg>t</rhyme></w> !</l>
				</lg>
				<lg n="5" type="sizain" rhyme="aabbcc">
					<l n="25" num="5.1" lm="8" met="8"><w n="25.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="25.2">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="25.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="25.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="25.7" punct="vg:8">y<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="26" num="5.2" lm="8" met="8"><w n="26.1">L</w>’<w n="26.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.3">qu</w>’<w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="26.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>’<w n="26.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="26.8" punct="pv:8">ci<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg>x</rhyme></w> ;</l>
					<l n="27" num="5.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="27.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="27.6" punct="vg:8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="28" num="5.4" lm="8" met="8"><w n="28.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="28.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">l</w>’<w n="28.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w>-<w n="28.5">J<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="28.6" punct="vg:8">bl<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>tt<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="29" num="5.5" lm="8" met="8"><w n="29.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="29.2">n</w>’<w n="29.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="29.4">qu</w>’<w n="29.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="29.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="29.7" punct="dp:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<rhyme label="c" id="15" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="dp">er</seg></rhyme></w> :</l>
					<l n="30" num="5.6" lm="8" met="8"><w n="30.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.3">l</w>’<w n="30.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="30.5" punct="pe:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="c" id="15" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg></rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>