<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES453" modus="sm" lm_max="6" metProfile="6" form="suite périodique" schema="6(ababcdcd)">
					<head type="main">LALY GALINE SEULE</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="6" met="6"><w n="1.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4" punct="vg:6">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="6" met="6"><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="2.5" punct="vg:6">m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="6" met="6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="3.2">t</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="3.5" punct="ps:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></w>…</l>
						<l n="4" num="1.4" lm="6" met="6"><w n="4.1">N</w> ’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="4.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="4.7" punct="pi:6">t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pi">oi</seg></rhyme></w> ?</l>
						<l n="5" num="1.5" lm="6" met="6"><w n="5.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="5.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>s</w> <w n="5.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
						<l n="6" num="1.6" lm="6" met="6"><w n="6.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="6.4" punct="vg:6">b<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</rhyme></w>,</l>
						<l n="7" num="1.7" lm="6" met="6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="7.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="7.6">pl<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
						<l n="8" num="1.8" lm="6" met="6"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="8.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="8.5" punct="pe:6">p<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababcdcd">
						<l n="9" num="2.1" lm="6" met="6"><w n="9.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">f<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="10" num="2.2" lm="6" met="6"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="10.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.4">pl<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</rhyme></w></l>
						<l n="11" num="2.3" lm="6" met="6"><w n="11.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.4">t<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="12" num="2.4" lm="6" met="6"><w n="12.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="12.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.4" punct="pe:6">fl<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pe">eu</seg>rs</rhyme></w> !</l>
						<l n="13" num="2.5" lm="6" met="6"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="13.5">l</w>’<w n="13.6">h<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="14" num="2.6" lm="6" met="6"><w n="14.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="14.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="14.4" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="pv">e</seg>r</rhyme></w> ;</l>
						<l n="15" num="2.7" lm="6" met="6"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="15.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="15.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="16" num="2.8" lm="6" met="6"><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="16.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="16.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="16.6" punct="pt:6">m<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="pt">e</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababcdcd">
						<l n="17" num="3.1" lm="6" met="6"><w n="17.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="17.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="17.4">h<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="18" num="3.2" lm="6" met="6"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="18.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.4">ci<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</rhyme></w></l>
						<l n="19" num="3.3" lm="6" met="6"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="19.2">fr<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="19.5"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="20" num="3.4" lm="6" met="6"><w n="20.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="20.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>ts</w> <w n="20.3" punct="pt:6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg>x</rhyme></w>.</l>
						<l n="21" num="3.5" lm="6" met="6"><w n="21.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="21.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="22" num="3.6" lm="6" met="6"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="22.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="22.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="22.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="22.5" punct="dp:6">c<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="dp">œu</seg>r</rhyme></w> :</l>
						<l n="23" num="3.7" lm="6" met="6"><w n="23.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w>-<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="24" num="3.8" lm="6" met="6"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt</w> <w n="24.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="24.5" punct="pi:6">b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nh<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pi">eu</seg>r</rhyme></w> ?</l>
					</lg>
					<lg n="4" type="huitain" rhyme="ababcdcd">
						<l n="25" num="4.1" lm="6" met="6"><w n="25.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="25.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="25.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="26" num="4.2" lm="6" met="6"><w n="26.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="26.4">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>t</w> <w n="26.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="26.6">fl<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>ts</rhyme></w></l>
						<l n="27" num="4.3" lm="6" met="6"><w n="27.1">J</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="27.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="27.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="27.5">v<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="28" num="4.4" lm="6" met="6"><w n="28.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="28.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="28.4" punct="pv:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="pv">o</seg>ts</rhyme></w> ;</l>
						<l n="29" num="4.5" lm="6" met="6"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="29.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.3">l</w>’<w n="29.4">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="4">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="29.5">fl<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="30" num="4.6" lm="6" met="6"><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="30.2">s</w>’<w n="30.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="a" type="vs" value="1" rule="365" place="5">e</seg>mm<rhyme label="d" id="16" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</rhyme></w></l>
						<l n="31" num="4.7" lm="6" met="6"><w n="31.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="31.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="31.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="31.6"><rhyme label="c" id="15" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="32" num="4.8" lm="6" met="6"><w n="32.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="32.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="32.3" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="d" id="16" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pt">an</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="5" type="huitain" rhyme="ababcdcd">
						<l n="33" num="5.1" lm="6" met="6"><w n="33.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="33.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="33.3" punct="vg:3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rt</w>, <w n="33.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="33.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="33.6">ph<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="34" num="5.2" lm="6" met="6"><w n="34.1">M<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="34.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="34.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="34.4" punct="vg:6">j<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</rhyme></w>,</l>
						<l n="35" num="5.3" lm="6" met="6"><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="35.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="35.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="35.4">s</w>’<w n="35.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="36" num="5.4" lm="6" met="6"><w n="36.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="36.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg></w> <w n="36.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="36.5" punct="pt:6">s<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>j<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>r</rhyme></w>.</l>
						<l n="37" num="5.5" lm="6" met="6"><w n="37.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w>-<w n="37.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="37.3">qu</w>’<w n="37.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="37.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="37.6" punct="vg:6">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<rhyme label="c" id="19" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="38" num="5.6" lm="6" met="6"><w n="38.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="38.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rd</w>’<w n="38.4" punct="pt:6">hu<rhyme label="d" id="20" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="pt">i</seg></rhyme></w>.</l>
						<l n="39" num="5.7" lm="6" met="6"><w n="39.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="39.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="39.3">qu</w>’<w n="39.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="39.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="39.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="39.7">n<rhyme label="c" id="19" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="40" num="5.8" lm="6" met="6"><w n="40.1">S</w>’<w n="40.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="40.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="40.4" punct="pt:6">lu<rhyme label="d" id="20" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="pt">i</seg></rhyme></w>.</l>
					</lg>
					<lg n="6" type="huitain" rhyme="ababcdcd">
						<l n="41" num="6.1" lm="6" met="6"><w n="41.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="41.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="41.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="41.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">im</seg>pl<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<rhyme label="a" id="21" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="42" num="6.2" lm="6" met="6"><w n="42.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="42.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="42.3" punct="vg:6">r<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<rhyme label="b" id="22" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></rhyme></w>,</l>
						<l n="43" num="6.3" lm="6" met="6"><w n="43.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="43.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="43.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<rhyme label="a" id="21" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="44" num="6.4" lm="6" met="6"><w n="44.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="44.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="44.3" punct="pt:6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<rhyme label="b" id="22" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt">a</seg></rhyme></w>.</l>
						<l n="45" num="6.5" lm="6" met="6"><w n="45.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="45.2">qu</w>’<w n="45.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="45.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="45.5" punct="vg:6">n<seg phoneme="ø" type="vs" value="1" rule="405" place="5">eu</seg>v<rhyme label="c" id="23" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="46" num="6.6" lm="6" met="6"><w n="46.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="46.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="46.3">d</w>’<w n="46.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="46.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg></w> <w n="46.6" punct="vg:6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rd<rhyme label="d" id="24" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg></rhyme></w>,</l>
						<l n="47" num="6.7" lm="6" met="6"><w n="47.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="47.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="47.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="47.4">r<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="c" id="23" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="48" num="6.8" lm="6" met="6"><w n="48.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="48.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="48.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="48.4" punct="pt:6">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<rhyme label="d" id="24" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg></rhyme></w>.</l>
					</lg>
					<closer>
						<placeName>Rochefort.</placeName>
					</closer>
				</div></body></text></TEI>