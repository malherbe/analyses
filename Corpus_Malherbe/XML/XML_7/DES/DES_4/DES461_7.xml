<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES461" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="2(abab)">
					<head type="main">L’ESPÉRANCE</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="pe:2"><seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">Ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="pe">ez</seg></w> ! <w n="1.2" punct="pe:4"><seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pe" caesura="1">ez</seg></w> !<caesura></caesura> <w n="1.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="1.5">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.6" punct="pe:10">n<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="2.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" caesura="1">oin</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="2.7">pi<seg phoneme="e" type="vs" value="1" rule="241" place="7">e</seg>ds</w> <w n="2.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="2.9" punct="pt:10">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>dr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg>x</rhyme></w>.</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="3.2">m</w>’<w n="3.3" punct="dp:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>di<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="dp" caesura="1">ez</seg></w> :<caesura></caesura> <w n="3.4">j</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7" mp="P">è</seg>s</w> <w n="3.7">qu</w>’<w n="3.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="3.9">m</w>’<w n="3.10" punct="dp:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>pp<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1" punct="pe:2"><seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">Ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="pe">ez</seg></w> ! <w n="4.2">J</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg>s</w> <w n="4.7" punct="pt:10">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg>br<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg>x</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="5.2" punct="vg:4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="5.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="5.4" punct="vg:7">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></w>, <w n="5.5">pu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s</w> <w n="5.6" punct="dp:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>c<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.3">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="6.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="6.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="6.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg>x</w> <w n="6.8" punct="pt:10">j<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pt">ou</seg>rs</rhyme></w>.</l>
						<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pe">eu</seg></w> ! <w n="7.2">J</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" caesura="1">en</seg>ds</w><caesura></caesura> <w n="7.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="7.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>x</w> <w n="7.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="7.7">m</w>’<w n="7.8" punct="pv:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">im</seg>pl<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="8.3" punct="vg:4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="4" punct="vg" caesura="1">om</seg></w>,<caesura></caesura> <w n="8.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="8.6" punct="pt:10">t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>j<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pt">ou</seg>rs</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>