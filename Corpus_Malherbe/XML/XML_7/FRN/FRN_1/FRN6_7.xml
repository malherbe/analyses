<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2023-06-25" who="RR" type="analyse">Étape 7 de l'analyse automatique du corpus : Traitement des rimes, des strophes et de la forme globale.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN6" modus="cp" lm_max="16" metProfile="4, 8, 12, 4÷6, 5=8, 3=6, (6), (11), (14)">
		<head type="main">SENSATIONS<lb></lb>DES ENDROITS DE LUXURE</head>
			<lg n="1">
				<l n="1" num="1.1" lm="12" mp6="M"><w n="1.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="1.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="1.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="1.5" punct="vg:9">m<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.7" punct="vg:12">N<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
				<l n="2" num="1.2" lm="16"><w n="2.1">Fr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>sc<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.4" punct="vg:11">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="11" punct="vg">a</seg></w>, <w n="2.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="12" mp="M">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="13">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="14" mp="F">e</seg></w> <w n="2.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="15" mp="C">e</seg>s</w> <w n="2.7" punct="pt:16">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="16">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="pt" mp="F">e</seg>s</w>.</l>
			</lg>
			<lg n="2">
				<l n="3" num="2.1" lm="4" met="4"><space unit="char" quantity="12"></space><w n="3.1" punct="vg:1">D<seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="vg">o</seg></w>, <w n="3.2" punct="vg:2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg></w>, <w n="3.3" punct="vg:3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="3.4" punct="vg:4">d<seg phoneme="o" type="vs" value="1" rule="444" place="4" punct="vg">o</seg></w>,</l>
				<l n="4" num="2.2" lm="6"><space unit="char" quantity="12"></space><w n="4.1" punct="vg:3">P<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="o" type="vs" value="1" rule="438" place="3" punct="vg">o</seg>s</w>, <w n="4.2" punct="pt:6">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="pt">o</seg>s</w>.</l>
				<l n="5" num="2.3" lm="10" met="4+6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2" punct="vg:2">s<seg phoneme="œ" type="vs" value="1" rule="249" place="2" punct="vg">œu</seg>rs</w>, <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="5.4" punct="vg:4">s<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg" caesura="1">œu</seg>rs</w>,<caesura></caesura> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="6" mp="M">u</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.7" punct="vg:10">b<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
				<l n="6" num="2.4" lm="8" met="8"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="6.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="6.4">l</w>’<w n="6.5" punct="pt:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rg<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				<l n="7" num="2.5" lm="8" met="8">— <w n="7.1" punct="vg:2">H<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="vg">à</seg></w>, <w n="7.2" punct="pe:3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="pe">o</seg></w> ! <w n="7.3">p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.4" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>br<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="8" num="2.6" lm="13" mp5="Mem/mp" met="8+5"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M/mp">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M/mp">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem/mp">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="Lp">a</seg>s</w>-<w n="8.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" caesura="1">a</seg>s</w><caesura></caesura> <w n="8.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-24" place="12" mp="F">e</seg></w> <w n="8.8" punct="pi:13">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="13">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="14" punct="pi" mp="F">e</seg></w> ?</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1" lm="8" met="8"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3">en</seg>t</w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="9.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="9.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="9.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p</w> <w n="9.8">d</w>’<w n="9.9" punct="vg:8"><seg phoneme="œ" type="vs" value="1" rule="286" place="8" punct="vg">œ</seg>il</w>,</l>
				<l n="10" num="3.2" lm="8" met="8"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg>s</w>, <w n="10.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="10.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="10.6" punct="vg:8">f<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>ils</w>,</l>
				<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="11.5">c<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.7" punct="vg:8">gl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
				<l n="12" num="3.4" lm="8" met="8"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2" punct="vg:2">d<seg phoneme="o" type="vs" value="1" rule="438" place="2" punct="vg">o</seg>s</w>, <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="12.5">qu<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rts</w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.8" punct="pv:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
				<l n="13" num="3.5" lm="10" met="4−6" mp6="M" mp5="F"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3" punct="vg:5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3" mp="M">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg" mp="F">e</seg>s</w>,<caesura></caesura> <w n="13.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="13.5" punct="vg:10">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="10" punct="vg">ô</seg>t</w>,</l>
				<l n="14" num="3.6" lm="13" met="5−8" mp5="C" mp8="Pem"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="14.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">om</seg>pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C" caesura="1">e</seg></w><caesura></caesura> <w n="14.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" mp="M">in</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="14.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.8" punct="pt:13">B<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19" place="12" mp="Mem">e</seg>r<seg phoneme="o" type="vs" value="1" rule="315" place="13" punct="pt">eau</seg></w>.</l>
			</lg>
			<lg n="4">
				<l n="15" num="4.1" lm="12" mp6="M"><w n="15.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="15.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="15.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="15.5" punct="vg:9">m<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="15.7" punct="vg:12">N<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
				<l n="16" num="4.2" lm="16"><w n="16.1">Fr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="16.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>sc<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="16.4" punct="vg:11">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="11" punct="vg">a</seg></w>, <w n="16.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="12" mp="M">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="13">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="14" mp="F">e</seg></w> <w n="16.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="15" mp="C">e</seg>s</w> <w n="16.7" punct="pt:16">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="16">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="pt" mp="F">e</seg>s</w>.</l>
			</lg>
			<lg n="5">
				<l n="17" num="5.1" lm="11"><w n="17.1" punct="vg:2">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg" mp="F">e</seg>s</w>, <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="17.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="17.5" punct="vg:9">r<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" punct="vg">en</seg>ts</w>, <w n="17.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="17.7" punct="vg:11">l<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg>s</w>,</l>
				<l n="18" num="5.2" lm="13" met="8+5"><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="18.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>pç<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>nt</w> <w n="18.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" caesura="1">ai</seg>s</w><caesura></caesura> <w n="18.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="18.6">p<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="18.7" punct="pv:13"><seg phoneme="e" type="vs" value="1" rule="409" place="12" mp="M">é</seg>tr<seg phoneme="y" type="vs" value="1" rule="450" place="13">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="14" punct="pv" mp="F">e</seg>s</w> ;</l>
				<l n="19" num="5.3" lm="9" met="3+6"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2" punct="vg:3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="19.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="19.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="19.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>fl<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="19.6" punct="vg:9">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9" punct="vg">en</seg></w>,</l>
				<l n="20" num="5.4" lm="14"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="20.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="20.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="20.5">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="20.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>tr<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="20.7">n</w>’<w n="20.8"><seg phoneme="i" type="vs" value="1" rule="497" place="11">y</seg></w> <w n="20.9">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" mp="M">e</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="306" place="13">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.10" punct="pt:14">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="14" punct="pt">en</seg></w>.</l>
				<l n="21" num="5.5" lm="9" mp3="M" met="6+3"><w n="21.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="21.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="21.3">pr<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" mp="F">e</seg>s</w></l>
				<l n="22" num="5.6" lm="12"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="22.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="22.4" punct="vg:6">n<seg phoneme="a" type="vs" value="1" rule="343" place="5" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="6" punct="vg">ï</seg>f</w>, <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="22.6">l</w>’<w n="22.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="22.8" punct="pv:12">n<seg phoneme="a" type="vs" value="1" rule="343" place="11" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="12">ï</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
				<l n="23" num="5.7" lm="10" met="4+6"><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="23.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="23.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="23.5" punct="vg:10">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="M">o</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg></w>,</l>
				<l n="24" num="5.8" lm="9" met="3+6"><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="24.3">l</w>’<w n="24.4"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">A</seg>M<seg phoneme="u" type="vs" value="1" rule="425" place="3" caesura="1">OU</seg>R</w><caesura></caesura> <w n="24.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="24.7" punct="tc:9">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="M">o</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="8" mp="M">u</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="ti">on</seg>s</w> —</l>
				<l n="25" num="5.9" lm="4" met="4">(<w n="25.1" punct="pt:4">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="pt ti">en</seg>t</w>.) —</l>
			</lg>
			<lg n="6">
				<l n="26" num="6.1" lm="12" mp6="M"><w n="26.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="26.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="26.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="26.5" punct="vg:9">m<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="26.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="26.7">N<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
				<l n="27" num="6.2" lm="16"><w n="27.1">Fr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="27.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>sc<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="27.4" punct="vg:11">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="11" punct="vg">a</seg></w>, <w n="27.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="12" mp="M">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="13">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="14" mp="F">e</seg></w> <w n="27.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="15" mp="C">e</seg>s</w> <w n="27.7" punct="pt:16">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="16">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="pt" mp="F">e</seg>s</w>.</l>
			</lg>
	</div></body></text></TEI>