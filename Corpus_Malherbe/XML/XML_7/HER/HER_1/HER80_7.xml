<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><head type="main_subpart">Les conquérants</head><div type="poem" key="HER80" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
						<head type="main">Jouvence</head>
						<lg n="1" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="y" type="vs" value="1" rule="d-3" place="1" mp="M">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg></w> <w n="1.2">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="1.4" punct="vg:6">Le<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.7">Di<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.8" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="2.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="2.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg></w><caesura></caesura> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="3.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rts</w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="3.9" punct="vg:12">r<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Pr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="4.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.7">S<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.9" punct="pt:12">S<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="5.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="5.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">A</seg>rm<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="5.5">d</w>’<w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="5.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></w> <w n="5.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-27" place="10" mp="F">e</seg></w> <w n="5.9" punct="vg:12">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>xpl<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.6">gl<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="6.7" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="7.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.5">br<seg phoneme="u" type="vs" value="1" rule="427" place="8" mp="M">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rd</w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="7.7" punct="vg:12">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="8.2">Fl<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>t</w><caesura></caesura> <w n="8.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="8.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="8.7" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3" punct="vg:6">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="M">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="9.4">b<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.6" punct="vg:12">f<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>t</w> <w n="10.2">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="10.4">p<seg phoneme="ɛ" type="vs" value="1" rule="366" place="5" mp="M">e</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>bl<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="11.6">s</w>’<w n="11.7"><seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="11.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="11.9" punct="pt:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>b<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg></rhyme></w>.</l>
						</lg>
						<lg n="4" rhyme="ede">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:2">Vi<seg phoneme="e" type="vs" value="1" rule="383" place="1" mp="M">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rd</w>, <w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="12.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="12.4" punct="vg:6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="12.9">t<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="13.3" punct="vg:3">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rt</w>, <w n="13.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="13.5" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="13.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="13.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="13.8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="13.10" punct="pv:12">b<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pv">eau</seg></rhyme></w> ;</l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="14.2">Gl<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">t</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.5">d<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="14.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.8" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>