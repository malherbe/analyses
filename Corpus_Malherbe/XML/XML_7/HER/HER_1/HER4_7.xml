<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><head type="main_subpart">Hercule et les Centaures</head><div type="poem" key="HER4" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
						<head type="main">Némée</head>
						<lg n="1" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="1.4">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>pt<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.8">f<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t</rhyme></w></l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.5">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>l</w><caesura></caesura> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="2.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>pr<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1" punct="vg">eu</seg>l</w>, <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="3.3">r<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="3.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="C">eu</seg>r</w> <w n="3.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>tr<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="4.2">s</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="4.4" punct="pt:3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="pt">u</seg></w>. <w n="4.5">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="4.7">s</w>’<w n="4.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="8">î</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.10" punct="pt:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">aî</seg>t</rhyme></w>.</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="5.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.4" punct="vg:6">h<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.6">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="5.9" punct="vg:12">gu<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>t</rhyme></w>,</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.5">s</w>’<w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="6.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" mp="P">e</seg>rs</w> <w n="6.8">T<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>r<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="12">yn</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2" punct="vg:2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="7.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.7"><seg phoneme="œ" type="vs" value="1" rule="286" place="6" caesura="1">œ</seg>il</w><caesura></caesura> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="7.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="7.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.11">cr<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="8.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rd</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="8.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="8.8">f<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="8.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rr<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12" punct="pt">ê</seg>t</rhyme></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2">s</w>’<w n="9.3" punct="pt:3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="469" place="3" punct="pt">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>. <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">I</seg>l</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.6">v<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="9.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.10">N<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="10.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="10.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="10.8">gu<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rm<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">cr<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="11.7">s<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="11.8" punct="pv:12">cr<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="437" place="12" punct="pv">o</seg>cs</rhyme></w> ;</l>
						</lg>
						<lg n="4" rhyme="eed">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="12.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="12.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.7">cr<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>sc<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1" punct="vg:1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>t</w>, <w n="13.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>s</w> <w n="13.3">l</w>’<w n="13.4">h<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="13.5">p<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="13.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="13.7">fl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="13.9">d</w>’<w n="13.10" punct="vg:12">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rc<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1" mp="M">ê</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="14.2">l</w>’<w n="14.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="14.6" punct="vg:6">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="vg" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="14.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>str<seg phoneme="y" type="vs" value="1" rule="454" place="9" mp="M">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="14.9" punct="pt:12">h<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pt">o</seg>s</rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>