<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Nature et le Rêve</head><div type="poem" key="HER96" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
					<head type="main">Médaille antique</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">E</seg>tn<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">m<seg phoneme="y" type="vs" value="1" rule="445" place="3" mp="M">û</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rpr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.8">l</w>’<w n="1.9"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r</w> <w n="1.10">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="1.11">v<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">É</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="7" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="2.6" punct="pv:12">Th<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>cr<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="3.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="3.5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="3.9">f<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>t</w> <w n="3.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>cr<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="Lc">ou</seg>rd</w>’<w n="4.4">hu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="4.8" punct="pt:12">v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">ain</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="5.3">p<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="5.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="5.7" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">A</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>th<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="6.7">f<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2" mp="M">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="7.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="7.5">v<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6" caesura="1">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g</w> <w n="7.9">gr<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="7.10">s</w>’<w n="7.11"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>rr<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="8.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="8.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="9">ue</seg>il</w> <w n="8.7" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="9.3" punct="pt:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pt" mp="F">e</seg></w>. <w n="9.4">T<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="9.5" punct="pt:6">m<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt" caesura="1">eu</seg>rt</w>.<caesura></caesura> <w n="9.6">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="9.9">s</w>’<w n="9.10" punct="pt:12"><rhyme label="c" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>gr<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="10.5">qu</w>’<w n="10.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.7" punct="vg:8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="10.9">S<seg phoneme="i" type="vs" value="1" rule="493" place="10" mp="M">y</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt</w> <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>s</w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="11.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="11.5">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>l</w><caesura></caesura> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="11.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="11.9" punct="pv:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>lg<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pv">en</seg>t</rhyme></w> ;</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="12.5">m<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="12.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7">l</w>’<w n="12.8"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="12.9">f<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="12.10">d<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>c<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="13.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="13.5" punct="vg:6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="13.7">m<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>d<seg phoneme="a" type="vs" value="1" rule="307" place="9">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="13.8">d</w>’<w n="13.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rg<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>t</rhyme></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="14.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="5" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="14.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="14.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="14.7" punct="pt:12">S<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>c<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>