<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PREMIÈRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>588 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Diverses</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/louiseackermaNpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1862">1862</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK10" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="5(abab)">
				<head type="number">X</head>
				<head type="main">Le Fantôme</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D</w>’<w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" mp="M">in</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>ni<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r</w> <w n="1.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="1.9">c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>p</w> <w n="1.10">s</w>’<w n="1.11" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>b<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="2.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="2.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5" mp="M">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="2.6">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7">s</w>’<w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="2.9" punct="vg:12">dr<seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg>ss<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="3.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="3.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="3.5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.6">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="415" place="12">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="4.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.7">br<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="4.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="4.9" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3" mp="M">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.5" punct="vg:6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">oin</seg></w>,<caesura></caesura> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="5.9">ch<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="6.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="6.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="6.6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.8" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</rhyme></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t</w> <w n="7.6"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="7.7">l<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.6">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="8.8">d</w>’<w n="8.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="9.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ls</w> <w n="9.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>ps</w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="9.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="10" mp="M">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>s</w> <w n="9.8">d</w>’<w n="9.9"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="10.3">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" caesura="1">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="10.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="9">en</seg>t</w> <w n="10.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="10.7" punct="pe:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>du<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pe">i</seg>t</rhyme></w> !</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="11.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="11.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="11.6" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>rt</w>,<caesura></caesura> <w n="11.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="11.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="11.9">d</w>’<w n="11.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="11.11">v<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="11.12" punct="vg:12">s<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.3">l<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>s</w> <w n="12.7" punct="pt:12">lu<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="13.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>rs</w> <w n="13.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">S</w>’<w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="14.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="14.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="14.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="14.6">d</w>’<w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="14.8">s<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="14.9">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.10"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="14.11" punct="pv:12">d<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>x</rhyme></w> ;</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="15.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="15.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="15.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="15.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="15.7"><seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="15.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>r</w> <w n="15.9" punct="vg:12">pl<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="16.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="16.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="Lc">u</seg>squ</w>’<w n="16.7"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="16.8" punct="pt:12">n<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ts</w> <w n="17.4">c<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="17.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="17.7" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="18.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="18.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="18.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="6" caesura="1">ô</seg>t</w><caesura></caesura> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="18.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="18.8" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>cr<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pv">oî</seg>t</rhyme></w> ;</l>
					<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="19.4">d</w>’<w n="19.5">H<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">am</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="19.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="19.7">fu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="19.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="19.9">s</w>’<w n="19.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>p<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="20.2">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="20.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">aî</seg>t</w><caesura></caesura> <w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="20.5" punct="dp:9">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" punct="dp">an</seg>t</w> : <w n="20.6">S<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M/mp">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="11" mp="Lp">en</seg>s</w>-<w n="20.7" punct="pe:12">t<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>