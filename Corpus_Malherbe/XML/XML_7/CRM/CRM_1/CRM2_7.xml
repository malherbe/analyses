<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM2" modus="cp" lm_max="9" metProfile="8, (9)" form="suite de strophes" schema="8[aa] 1[abab] 1[abba]">
				<head type="main">RUE DES FONTAINES</head>
				<lg n="1" type="regexp" rhyme="aa">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="1.3">n<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="1.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="1.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.8" punct="vg:8">p<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="2.2">n<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.5">r<seg phoneme="y" type="vs" value="1" rule="457" place="5">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.7" punct="pt:8">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="aa">
					<l n="3" num="2.1" lm="8" met="8"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ts</w> <w n="3.3">n</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="3.6">d</w>’<w n="3.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<rhyme label="a" id="2" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="vg">en</seg>t</rhyme></w>,</l>
					<l n="4" num="2.2" lm="8" met="8"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="4.3" punct="vg:3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="4.7" punct="vg:8">bl<rhyme label="a" id="2" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>c</rhyme></w>,</l>
				</lg>
				<lg n="3" type="regexp" rhyme="aa">
					<l n="5" num="3.1" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">D<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="5.6">pr<rhyme label="a" id="3" gender="m" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</rhyme></w></l>
					<l n="6" num="3.2" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.6" punct="pt:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rs<rhyme label="a" id="3" gender="m" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pt">e</seg>t</rhyme></w>.</l>
				</lg>
				<lg n="4" type="regexp" rhyme="aa">
					<l n="7" num="4.1" lm="8" met="8"><w n="7.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">l</w>’<w n="7.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<rhyme label="a" id="4" gender="m" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>t</rhyme></w>,</l>
					<l n="8" num="4.2" lm="8" met="8"><w n="8.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="8.4" punct="vg:6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>t</w>, <w n="8.5" punct="pt:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="a" id="4" gender="m" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>t</rhyme></w>.</l>
				</lg>
				<lg n="5" type="regexp" rhyme="aa">
					<l n="9" num="5.1" lm="9"><w n="9.1" punct="vg:2">P<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="9.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="M">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg></w> <w n="9.6" punct="pv:9">ci<rhyme label="a" id="12" gender="m" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="9" punct="pv">e</seg>l</rhyme></w> ;</l>
					<l n="10" num="5.2" lm="8" met="8"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.5" punct="pt:8">h<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<rhyme label="a" id="12" gender="f" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="6" type="regexp" rhyme="aa">
					<l n="11" num="6.1" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2" punct="vg:2">l<seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="vg">à</seg></w>, <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="11.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.6" punct="vg:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<rhyme label="a" id="5" gender="m" type="a" stanza="6"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</rhyme></w>,</l>
					<l n="12" num="6.2" lm="8" met="8"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ppr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>rs</w> <w n="12.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="12.6">b<rhyme label="a" id="5" gender="m" type="e" stanza="6"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</rhyme></w></l>
				</lg>
				<lg n="7" type="regexp" rhyme="aa">
					<l n="13" num="7.1" lm="8" met="8"><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="13.3" punct="vg:3">f<seg phoneme="œ" type="vs" value="1" rule="304" place="2">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>t</w>, <w n="13.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="13.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="13.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="13.7" punct="vg:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rd<rhyme label="a" id="6" gender="m" type="a" stanza="7"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></w>,</l>
					<l n="14" num="7.2" lm="8" met="8"><w n="14.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="14.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l</w> <w n="14.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="14.5" punct="pt:8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<rhyme label="a" id="6" gender="m" type="e" stanza="7"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></rhyme></w>.</l>
				</lg>
				<lg n="8" type="regexp" rhyme="aa">
					<l n="15" num="8.1" lm="8" met="8"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="15.3" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="15.4">c</w>’<w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="15.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="a" id="7" gender="m" type="a" stanza="8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></w></l>
					<l n="16" num="8.2" lm="8" met="8"><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">ch<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="16.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="16.4">l</w>’<w n="16.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="16.7" punct="pt:8">gr<rhyme label="a" id="7" gender="m" type="e" stanza="8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>d</rhyme></w>.</l>
				</lg>
				<lg n="9" type="regexp" rhyme="">
					<l rhyme="none" n="17" num="9.1" lm="8" met="8"><w n="17.1">J</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="17.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="17.4" punct="tc:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="ti">é</seg></w> — <w n="17.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="17.7" punct="vg:8">b<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l rhyme="none" n="18" num="9.2" lm="8" met="8"><w n="18.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.5">l</w>’<w n="18.6" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe in">an</seg></w> ! ‒</l>
				</lg>
				<lg n="10" type="regexp" rhyme="ab">
					<l n="19" num="10.1" lm="8" met="8"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="19.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="19.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="19.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="19.7">pr<rhyme label="a" id="8" gender="f" type="a" stanza="9"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="20" num="10.2" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="20.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="20.5">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>r<rhyme label="b" id="9" gender="m" type="a" stanza="9"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></w></l>
				</lg>
				<lg n="11" type="regexp" rhyme="ab">
					<l n="21" num="11.1" lm="8" met="8"><w n="21.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">l</w>’<w n="21.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.4">s<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>lv<rhyme label="a" id="8" gender="f" type="e" stanza="9"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="22" num="11.2" lm="8" met="8"><w n="22.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="22.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="22.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.6">v<rhyme label="b" id="9" gender="m" type="e" stanza="9"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</rhyme></w></l>
				</lg>
				<lg n="12" type="regexp" rhyme="ab">
					<l n="23" num="12.1" lm="8" met="8"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="23.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="23.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="a" id="10" gender="m" type="a" stanza="10"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></w></l>
					<l n="24" num="12.2" lm="8" met="8"><w n="24.1" punct="vg:3">S<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>pl<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3" punct="vg">en</seg>t</w>, <w n="24.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="24.5" punct="vg:8">v<rhyme label="b" id="11" gender="f" type="a" stanza="10"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="13" type="regexp" rhyme="ba">
					<l n="25" num="13.1" lm="8" met="8"><w n="25.1" punct="vg:2">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg></w>, <w n="25.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="25.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="25.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="25.5" punct="vg:8">pl<rhyme label="b" id="11" gender="f" type="e" stanza="10"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="26" num="13.2" lm="8" met="8"><w n="26.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="26.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="26.4">l<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="26.5">d</w>’<w n="26.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<rhyme label="a" id="10" gender="m" type="e" stanza="10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>