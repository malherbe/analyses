<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM191" modus="cm" lm_max="12" metProfile="6÷6" form="suite périodique" schema="7(aa)">
				<head type="main">C’EST A DÉSESPÉRER…</head>
				<lg n="1" type="distique" rhyme="aa">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="1.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.6">n</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="1.9" punct="vg:12">p<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="2.5">g<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w><caesura></caesura> <w n="2.6">qu</w>’<w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="2.8"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="2.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="2.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.11" punct="vg:12">t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="2" type="distique" rhyme="aa">
					<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="3.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="3.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">r<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="3.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="3.8">f<rhyme label="a" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></rhyme></w></l>
					<l n="4" num="2.2" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="4.6">s</w>’<w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="4.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="4.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>p<rhyme label="a" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>s</rhyme></w></l>
				</lg>
				<lg n="3" type="distique" rhyme="aa">
					<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="497" place="4" mp="C">y</seg></w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="5.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="5.8">ch<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="5.10">d<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="6" num="3.2" lm="12" mp6="C" met="6−6"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3" punct="vg:4">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="3" mp="M">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w>, <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C" caesura="1">e</seg>s</w><caesura></caesura> <w n="6.6" punct="vg:8">pr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8" punct="vg">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w>, <w n="6.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="6.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.9">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
				</lg>
				<lg n="4" type="distique" rhyme="aa">
					<l n="7" num="4.1" lm="12" mp7="F" met="6−6" mp4="Pem" mp5="Mem" mp8="C" mp9="None"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="7.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F" caesura="1">e</seg></w><caesura></caesura> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="7.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>s</w> <w n="7.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="7.8" punct="vg:12">f<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="8" num="4.2" lm="12" met="6+6"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="M">e</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ls</w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="8.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="411" place="10">ê</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="8.8" punct="vg:12">br<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>nt</rhyme></w>,</l>
				</lg>
				<lg n="5" type="distique" rhyme="aa">
					<l n="9" num="5.1" lm="12" met="6+6"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="9.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.6">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rt</w><caesura></caesura> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.9">n<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="9.11">bl<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</rhyme></w></l>
					<l n="10" num="5.2" lm="12" mp7="F" met="4+8" mp8="C" mp9="M"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="2">er</seg></w><caesura></caesura> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="10.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="10.7">p<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="10.8" punct="vg:12">n<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>cr<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</rhyme></w>,</l>
				</lg>
				<lg n="6" type="distique" rhyme="aa">
					<l n="11" num="6.1" lm="12" met="6+6"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.4">qu</w>’<w n="11.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="11.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>t</w><caesura></caesura> <w n="11.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="11.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg></w> <w n="11.9">h<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>t</rhyme></w></l>
					<l n="12" num="6.2" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.4">qu</w>’<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>d</w><caesura></caesura> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rs</w> <w n="12.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>r</w> <w n="12.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="12.10" punct="vg:12"><seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg>x</rhyme></w>,</l>
				</lg>
				<lg n="7" type="distique" rhyme="aa">
					<l n="13" num="7.1" lm="12" met="6+6"><w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="13.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="13.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3" mp="Lc">i</seg></w>-<w n="13.4" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="13.5">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="13.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="13.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="13.9"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="13.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.11" punct="dp:12">f<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="dp">oi</seg>s</rhyme></w> :</l>
					<l n="14" num="7.2" lm="12" met="6+6"><w n="14.1" punct="vg:2">H<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="14.2">br<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="14.3">d</w>’<w n="14.4" punct="vg:5">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="14.5" punct="vg:6">c<seg phoneme="a" type="vs" value="1" rule="307" place="6" punct="vg" caesura="1">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>il</w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="14.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>d</w> <w n="14.9">b<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>s</rhyme></w></l>
				</lg>
			</div></body></text></TEI>