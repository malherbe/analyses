<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM184" modus="sm" lm_max="6" metProfile="6" form="" schema="">
				<head type="main">LAISSE TOUS CES CHEMINS…</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="1.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="2.5" punct="pt:6">m<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ssi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>ds</w>-<w n="3.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5" punct="vg:6">th<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="6" punct="vg">ym</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.3" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="6" met="6"><w n="5.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w>-<w n="5.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="5.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<rhyme label="e" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></rhyme></w></l>
					<l n="6" num="2.2" lm="6" met="6"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="6.4">b<rhyme label="f" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
					<l n="7" num="2.3" lm="6" met="6"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5" punct="vg:6">Th<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<rhyme label="e" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="6" met="6"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="8.3">d</w>’<w n="8.4" punct="pi:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>rkh<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<rhyme label="f" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pi">e</seg>lsk</rhyme></w> ?</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="6" met="6"><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w>-<w n="9.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="9.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="9.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>li<rhyme label="e" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></rhyme></w></l>
					<l n="10" num="3.2" lm="6" met="6"><w n="10.1">V<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>gu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="10.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="10.3">Z<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>z<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>b<rhyme label="h" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</rhyme></w></l>
					<l n="11" num="3.3" lm="6" met="6"><w n="11.1">V<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">n<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>f</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pi<rhyme label="e" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="6" met="6"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.6" punct="vg:6">m<rhyme label="h" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="4" rhyme="None">
					<l n="13" num="4.1" lm="6" met="6"><w n="13.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="13.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>lc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg></w> <w n="13.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="13.5">ci<rhyme label="j" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</rhyme></w></l>
					<l n="14" num="4.2" lm="6" met="6"><w n="14.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="14.2">d</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<rhyme label="j" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
					<l n="15" num="4.3" lm="6" met="6"><w n="15.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w> <w n="15.2">f<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="15.4">cl<rhyme label="e" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</rhyme></w></l>
					<l n="16" num="4.4" lm="6" met="6"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="16.3">t<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="16.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.6" punct="pi:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<rhyme label="j" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="pi">e</seg>il</rhyme></w> ?</l>
				</lg>
			</div></body></text></TEI>