<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM112" modus="cm" lm_max="12" metProfile="6÷6" form="sonnet peu classique" schema="abab baba ccd ede">
				<head type="main">DIMANCHE MATIN</head>
				<lg n="1" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="1.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="1.4" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="1.6">d<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.7" punct="pt:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></rhyme></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="2.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w><caesura></caesura> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="2.6">ch<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>d</w> <w n="2.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="2.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="C">eu</seg>r</w> <w n="2.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>gl<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w><caesura></caesura> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.7">f<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</w> <w n="3.8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg>s</rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="4.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="4.4">r<seg phoneme="y" type="vs" value="1" rule="457" place="6" caesura="1">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="4.6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8">en</seg>t</w> <w n="4.7">fr<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="4.9" punct="pt:12">br<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="baba">
					<l n="5" num="2.1" lm="12" mp7="F" met="8+4" mp4="M" mp5="None"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">j<seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w> <w n="5.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="5.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8" caesura="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w><caesura></caesura> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="5.8" punct="pt:12">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<rhyme label="b" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					<l n="6" num="2.2" lm="12" mp7="F" met="6−6" mp4="F" mp5="M" mp8="Pem" mp9="None"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="6.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.3">s</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F" caesura="1">e</seg></w><caesura></caesura> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="6.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.8">m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>l<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></rhyme></w></l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="7.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="7.4" punct="vg:6">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="7.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="7.9" punct="pt:12">c<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg>t<rhyme label="b" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>c</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>d</w><caesura></caesura> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="8.7" punct="pt:12">tr<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">ain</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w><caesura></caesura> <w n="9.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="9.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="9.7">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="10" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="11" mp="M">y</seg>s<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="10" num="3.2" lm="12" mp6="M" mp4="F" met="8+4"><w n="10.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="10.3">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="10.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="8" caesura="1">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w><caesura></caesura> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.8">f<seg phoneme="œ" type="vs" value="1" rule="304" place="11" mp="M">ai</seg>s<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="11.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="11.5" punct="vg:4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="11.6">s</w>’<w n="11.7"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="11.8">gr<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="11.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="11.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.11">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rv<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s</rhyme></w></l>
				</lg>
				<lg n="4" rhyme="ede">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="12.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="12.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="12.4">qu</w>’<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="12.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="12.8" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>d<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></rhyme></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="13.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.5" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="13.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>rpr<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s</rhyme></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="14.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="14.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="14.8" punct="pt:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rr<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg>x</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>