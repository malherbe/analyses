<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO27" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(ababbccdcd)">
					<head type="main">LES CINQ POINTS</head>
					<opener>
						<salute>A MADEMOISELLE DE ***</salute>
					</opener>
					<lg n="1" type="dizain" rhyme="ababbccdcd">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="1.3">qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg" caesura="1">an</seg>s</w>,<caesura></caesura> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="1.6">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="1.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="1.8">s<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.10" punct="vg:10">g<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>rd</rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="2.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="2.6">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>q</w> <w n="2.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>ts</w> <w n="2.8" punct="dp:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>xpr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="dp">è</seg>s</rhyme></w> :</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1" punct="vg:4">Pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" mp="M">è</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="3.2">s</w>’<w n="3.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6" mp="P">à</seg></w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.7" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>g<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>rd</rhyme></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="4.4">pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="4.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="4.7" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>pr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="pv">è</seg>s</rhyme></w> ;</l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>tt<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="5.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="5.7" punct="vg:10">pr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="vg">è</seg>s</rhyme></w>,</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="6.3">c<seg phoneme="ø" type="vs" value="1" rule="398" place="3" mp="Lp">eu</seg>x</w>-<w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" caesura="1">à</seg></w><caesura></caesura> <w n="6.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="F">e</seg>nt</w> <w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="6.8" punct="vg:10">p<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10" punct="vg">oin</seg>t</rhyme></w>,</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="7.2" punct="ps:3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3" punct="ps in">e</seg>st</w>… ‒ <w n="7.3" punct="pi:4">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="4" punct="pi in" caesura="1">oi</seg></w> ?<caesura></caesura> ‒ <w n="7.4">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg></w> <w n="7.8" punct="pv:10">p<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10" punct="pv">oin</seg>t</rhyme></w> ;</l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">s</w>’<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="8.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="8.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">aî</seg>t</w><caesura></caesura> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="8.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="8.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.9">v<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="C">ou</seg>s</w> <w n="8.10" punct="vg:10">r<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">m<seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" caesura="1">ai</seg></w><caesura></caesura> <w n="9.4">v<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="M">on</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="9.6" punct="vg:10">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rp<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10" punct="vg">oin</seg>t</rhyme></w>,</l>
						<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="10.3" punct="vg:4">n<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg>d</w>,<caesura></caesura> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ppr<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>