<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO75" modus="cp" lm_max="12" metProfile="8, 6+6, 4+6" form="suite de strophes" schema="3[abab] 1[ababa] 1[ababb]">
					<head type="main">L’EXCUSE INGÉNIEUSE</head>
					<lg n="1" type="regexp" rhyme="ababababababababaababb">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="1.4" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>r</w>,<caesura></caesura> <w n="1.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="1.7" punct="vg:12">D<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>ch<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="2.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.3">M<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="2.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.7">m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">ain</seg></rhyme></w></l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">A</seg>lt<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="4.4" punct="vg:5">cr<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg></w>, <w n="4.5">c</w>’<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="4.8" punct="dp:8">v<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="dp">ain</seg></rhyme></w> :</l>
						<l n="5" num="1.5" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="5.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="5.3">qu</w>’<w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="5.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>f</w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>g<seg phoneme="ɥi" type="vs" value="1" rule="274" place="8" mp="M">ui</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="5.7" punct="vg:10">pr<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="6.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="6.6" punct="vg:8">tr<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">Cl<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>qu<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="7.5" punct="pt:8">c<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space>« <w n="8.1">F<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="8.3" punct="vg:5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>c</w>, <w n="8.4" punct="pi:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pi">in</seg></rhyme></w> ?</l>
						<l n="9" num="1.9" lm="12" met="6+6">» <w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="9.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="9.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="Lc">e</seg>lqu</w>’<w n="9.4" punct="pe:4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" punct="pe">un</seg></w> ! <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="9.6" punct="vg:6">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="9.7" punct="vg:9">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.9" punct="pe:12">J<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg>n<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> ! »</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2" punct="vg:3">M<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="vg">eu</seg>rs</w>, <w n="10.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="10.4">b<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>v<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="10.6">C<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>t</w> <w n="10.7" punct="vg:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></rhyme></w>,</l>
						<l n="11" num="1.11" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="11.1">N</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>d<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="11.8" punct="pt:10">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">aî</seg>tr<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="12" num="1.12" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="12.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="12.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="12.7" punct="pt:10">f<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pt">in</seg></rhyme></w>.</l>
						<l n="13" num="1.13" lm="10" met="4+6"><space unit="char" quantity="4"></space>‒ « <w n="13.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="13.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="13.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="13.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6" mp="M">a</seg>y<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10">ain</seg></rhyme></w></l>
						<l n="14" num="1.14" lm="10" met="4+6"><space unit="char" quantity="4"></space>» <w n="14.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="14.3">d</w>’<w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="14.7" punct="dp:10">sc<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<rhyme label="b" id="8" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="15" num="1.15" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="15.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="15.4">tr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="15.6" punct="pe:8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pe">ain</seg></rhyme></w> !</l>
						<l n="16" num="1.16" lm="8" met="8"><space unit="char" quantity="8"></space>» ‒ <w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="16.3">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="16.4">d</w>’<w n="16.5" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<rhyme label="b" id="8" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>, »</l>
						<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="17.3">M<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r</w> <w n="17.7" punct="pv:12">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12" punct="pv">ein</seg></rhyme></w> ;</l>
						<l n="18" num="1.18" lm="8" met="8"><space unit="char" quantity="8"></space>« <w n="18.1">J</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="18.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="18.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="18.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.7" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>tt<rhyme label="a" id="9" gender="f" type="a" stanza="5"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="19" num="1.19" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="19.3">m</w>’<w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="19.5">v<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="19.6" punct="dp:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="dp">u</seg></rhyme></w> :</l>
						<l n="20" num="1.20" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="20.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="20.7" punct="pi:8">d<rhyme label="a" id="9" gender="f" type="e" stanza="5"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
						<l n="21" num="1.21" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="21.1" punct="pe:1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg>s</w> ! <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="21.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="21.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.6" punct="vg:8">f<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<rhyme label="b" id="10" gender="m" type="e" stanza="5"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></rhyme></w>,</l>
						<l n="22" num="1.22" lm="12" met="6+6">» <w n="22.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="22.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="22.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="22.5">c<seg phoneme="œ" type="vs" value="1" rule="389" place="6" caesura="1">oeu</seg>r</w><caesura></caesura> <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="22.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="22.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="22.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="22.10" punct="pe:12">c<rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pe">u</seg></rhyme></w> ! »</l>
					</lg>
				</div></body></text></TEI>