<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO41" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="3[abab] 1[abaab] 1[aa] 1[abba]">
					<head type="main">LA CONFIDENCE</head>
					<lg n="1" type="regexp" rhyme="abababababaabaaabababba">
						<l n="1" num="1.1" lm="8" met="8">« <w n="1.1" punct="vg:2">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" punct="vg">e</seg>t</w>, <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="1.5" punct="pi:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gr<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pi">in</seg></rhyme></w> ?</l>
						<l n="2" num="1.2" lm="8" met="8">» ‒ <w n="2.1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2" punct="vg:3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3" punct="vg">en</seg>t</w>, <w n="2.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="2.5" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="8" met="8">» ‒ <w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3" punct="pi:3">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="3" punct="pi in">oi</seg></w> ? ‒ <w n="3.4">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7">M<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8">» <w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="4.2">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w>-<w n="4.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="4.4">m</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.6" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="5" num="1.5" lm="8" met="8">» ‒ <w n="5.1" punct="pe:1">Ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1" punct="ps pe">e</seg>l</w>… ! <w n="5.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="5.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="5.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5" punct="pt:8">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></w>.</l>
						<l n="6" num="1.6" lm="8" met="8">» ‒ <w n="6.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="6.2" punct="vg:3">M<seg phoneme="œ" type="vs" value="1" rule="151" place="2">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="vg">eu</seg>r</w>, <w n="6.3">c</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="6.6" punct="dp:8">D<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="7.3" punct="vg:3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</w>, <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">D<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="7.6" punct="vg:8">l<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.5" punct="pv:8">bl<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="9" num="1.9" lm="8" met="8"><w n="9.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="9.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.5">m</w>’<w n="9.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></w>.</l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.6" punct="vg:8">gr<rhyme label="b" id="6" gender="f" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="11" num="1.11" lm="8" met="8"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="11.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="11.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="11.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="11.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="11.7" punct="pt:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rqu<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="wa" type="vs" value="1" rule="281" place="8" punct="pt">oi</seg></rhyme></w>.</l>
						<l n="12" num="1.12" lm="8" met="8">« <w n="12.1" punct="vg:2">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" punct="vg">e</seg>t</w>, » <w n="12.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w>-<w n="12.4" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg in">i</seg>l</w>, « <w n="12.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="12.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.7" punct="vg:8">f<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></rhyme></w>,</l>
						<l n="13" num="1.13" lm="8" met="8">» <w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="13.6" punct="pe:8"><rhyme label="b" id="6" gender="f" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> ! »</l>
						<l n="14" num="1.14" lm="8" met="8"><w n="14.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rs</w> <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="14.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="14.7" punct="vg:8">c<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="vg">oin</seg></rhyme></w>,</l>
						<l n="15" num="1.15" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="15.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="15.4">d</w>’<w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="15.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="15.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="15.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.9" punct="vg:8">f<rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="vg">oin</seg></rhyme></w>,</l>
						<l n="16" num="1.16" lm="8" met="8"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="16.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ts</w> <w n="16.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="16.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.6" punct="pt:8">b<rhyme label="a" id="8" gender="f" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="17" num="1.17" lm="8" met="8"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2" punct="dp:3">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="dp">oi</seg>s</w> : <w n="17.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="17.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5">s<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="17.7" punct="vg:8">c<rhyme label="b" id="9" gender="m" type="a" stanza="5"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">ou</seg></rhyme></w>,</l>
						<l n="18" num="1.18" lm="8" met="8"><w n="18.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="18.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="18.6" punct="vg:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>v<rhyme label="a" id="8" gender="f" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="19" num="1.19" lm="8" met="8"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="19.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="19.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="19.7"><rhyme label="b" id="9" gender="m" type="e" stanza="5"><seg phoneme="u" type="vs" value="1" rule="426" place="8">où</seg></rhyme></w></l>
						<l n="20" num="1.20" lm="8" met="8"><w n="20.1" punct="ps:1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" punct="ps">Un</seg></w>… <w n="20.2">ch<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="20.6" punct="dp:8">cl<rhyme label="a" id="10" gender="m" type="a" stanza="6"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="dp">ou</seg></rhyme></w> :</l>
						<l n="21" num="1.21" lm="8" met="8">« <w n="21.1" punct="vg:2">L<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, » <w n="21.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w>-<w n="21.4" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg in">i</seg>l</w>, « <w n="21.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="21.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="21.7" punct="pe:8">p<rhyme label="b" id="11" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> ! »</l>
						<l n="22" num="1.22" lm="8" met="8"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="22.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="22.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.6" punct="vg:8">P<rhyme label="b" id="11" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="23" num="1.23" lm="8" met="8"><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">qu</w>’<w n="23.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="23.5">n</w>’<w n="23.6"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="23.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">î</seg>t</w> <w n="23.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="23.9" punct="pt:8">tr<rhyme label="a" id="10" gender="m" type="e" stanza="6"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="pt">ou</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>