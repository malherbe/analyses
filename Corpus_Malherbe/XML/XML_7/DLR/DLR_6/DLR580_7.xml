<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">LE SPHINX</head><div type="poem" key="DLR580" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
					<head type="main">A LA DÉSSE MAUT</head>
					<head type="sub_1">A TÊTES DE CHAT</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="1.2" punct="vg:3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="1.4" punct="vg:6">M<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg" caesura="1">au</seg>t</w>,<caesura></caesura> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="Fc">e</seg>s</w> <w n="1.6" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg>t</rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="Fc">e</seg>s</w> <w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="2.3" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="2.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.6" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="3.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">pr<seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="M">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="Lc">ou</seg>rd</w>’<w n="3.7">hu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="3.8" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>ss<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="4.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="4.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rv<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="Fc">e</seg>s</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.8" punct="pe:12">fr<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg>t</rhyme></w> !</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="5.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" mp="P">e</seg>rs</w> <w n="5.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="5.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rps</w><caesura></caesura> <w n="5.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="5.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="5.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ffr<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>t</rhyme></w>.</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2">s</w>’<w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>t</w> <w n="6.6" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="6.7" punct="vg:9">D<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="6.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.9">t<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="6.10" punct="vg:12">s<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1" punct="vg:2">R<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>ds</w>, <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="7.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="Fm">e</seg>s</w>-<w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="6" caesura="1">à</seg></w><caesura></caesura> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="7.7" punct="vg:12">ph<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>sph<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>sc<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="8.3" punct="vg:6">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="8.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.5" punct="pt:12">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>r<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="Lp">i</seg>s</w>-<w n="9.2" punct="dp:2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="dp in">eu</seg>r</w> : « <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="9.4" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="9.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="9.6">s<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>rs</w><caesura></caesura> <w n="9.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.8">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s</w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="9.10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fm">e</seg></w>-<w n="9.11" punct="pt:12">f<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="193" place="12">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2" punct="vg:4">p<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>r</w><caesura></caesura> <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="10.6" punct="vg:9">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="10.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="10.8">qu</w>’<w n="10.9"><seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.10" punct="vg:12"><rhyme label="c" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="11.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="11.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6">« <w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">A</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M/mp">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="12.2" punct="pe:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pe">oi</seg></w> ! <w n="12.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>ct</w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="12.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="12.9" punct="pt:12">m<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg>st<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="13.3">l</w>’<w n="13.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="13.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.9" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="14.2">qu</w>’<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="M">eu</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="14.6">f<seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="14.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="14.9" punct="pt:12">t<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="in pt" mp="F">e</seg></rhyme></w>« .</l>
					</lg>
				</div></body></text></TEI>