<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR624" modus="cp" lm_max="12" metProfile="8, 5+5, 6+6" form="suite périodique" schema="1(ababcc) 3(abbacc)">
					<head type="main">DÉPART</head>
					<lg n="1" type="sizain" rhyme="ababcc">
						<l n="1" num="1.1" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.4" punct="vg:5">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="1.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.7" punct="vg:10">s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">bl<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>r</rhyme></w>.</l>
						<l n="3" num="1.3" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="3.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="3.7" punct="vg:10">r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.4">l<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="4.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="4.7" punct="pt:8">f<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</rhyme></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="5.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.6" punct="vg:8">t<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>r</w> <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="6.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="6.10" punct="pt:12">gu<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="sizain" rhyme="abbacc">
						<l n="7" num="2.1" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="7.4">v<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>s</w><caesura></caesura> <w n="7.5">s</w>’<w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="7.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="7.9">l</w>’<w n="7.10" punct="vg:10"><rhyme label="a" id="4" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="8" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="8.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.5" punct="pt:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rs<seg phoneme="e" type="vs" value="1" rule="383" place="7">e</seg>ill<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="9" num="2.3" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="9.3" punct="vg:5">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.6" punct="pe:10">f<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="10" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7" punct="vg:8">fl<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="vg">o</seg>t</rhyme></w>,</l>
						<l n="11" num="2.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="11.5" punct="vg:5">f<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>ls</w>, <w n="11.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="11.8" punct="ps:8">p<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></w>…</l>
						<l n="12" num="2.6" lm="12" met="6+6"><w n="12.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="12.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>r</w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="12.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="12.10" punct="pt:12">gu<rhyme label="c" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="sizain" rhyme="abbacc">
						<l n="13" num="3.1" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="13.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" caesura="1">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.4"><seg phoneme="y" type="vs" value="1" rule="453" place="6" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="13.7" punct="vg:10">s<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>g</rhyme></w>,</l>
						<l n="14" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">v<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="14.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.6" punct="vg:8">c<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="15" num="3.3" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="15.2">r<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.3">br<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>l</w><caesura></caesura> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="15.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-27" place="9" mp="F">e</seg></w> <w n="15.7" punct="vg:10">h<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="16" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="16.2" punct="vg:4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" punct="vg">e</seg>s</w>, <w n="16.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="16.4" punct="pe:8">pu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</rhyme></w> !</l>
						<l n="17" num="3.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="17.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="17.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ff<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="18" num="3.6" lm="12" met="6+6"><w n="18.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="18.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="18.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="18.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="18.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>r</w> <w n="18.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="18.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="18.10" punct="pt:12">gu<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="sizain" rhyme="abbacc">
						<l n="19" num="4.1" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="19.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="19.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="19.3">s</w>’<w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="19.5">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>t</w><caesura></caesura> <w n="19.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" mp="P">e</seg>rs</w> <w n="19.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="19.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="19.9">h<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>s<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rd</rhyme></w></l>
						<l n="20" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">s</w>’<w n="20.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.7" punct="pt:8">f<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="21" num="4.3" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="21.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="21.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mp">e</seg>vi<seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="3" mp="M/mp">en</seg>dr<seg phoneme="e" type="vs" value="1" rule="347" place="4" mp="Lp">ez</seg></w>-<w n="21.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>s</w><caesura></caesura> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="21.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="21.6" punct="vg:10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">em</seg>p<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="22" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="22.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="22.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="22.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="22.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="22.6" punct="pi:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi ps">a</seg>rt</rhyme></w> ?…</l>
						<l n="23" num="4.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="23.1">L</w>’<w n="23.2">h<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="23.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.6" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="c" id="12" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="24" num="4.6" lm="12" met="6+6"><w n="24.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="24.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="24.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="24.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="24.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="24.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>r</w> <w n="24.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="24.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="24.10" punct="pe:12">gu<rhyme label="c" id="12" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>