<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE V</head><head type="main_part">Intimité</head><div type="poem" key="DLR1075" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="3(abba)">
					<head type="main">Femmes</head>
					<head type="sub_2">FERVEUR, Editions de la Revue Blanche, 1902</head>
					<opener>
						<epigraph>
							<cit>
								<quote><hi rend="ital">Et tout dit à la femme</hi> : « <hi rend="ital">Allez à la douleur !</hi> »</quote>
								<bibl>
									<name>M. D.-V.</name>
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="355" place="2">e</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="1.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.6" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:1">F<seg phoneme="a" type="vs" value="1" rule="193" place="1" punct="vg">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">am</seg>ph<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.6"><seg phoneme="u" type="vs" value="1" rule="426" place="9">où</seg></w> <w n="2.7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.9" punct="vg:12">j<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="3.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7" punct="vg:9">m<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="M">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>t</w>, <w n="3.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="3.9" punct="vg:12">pr<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="œ" type="vs" value="1" rule="249" place="1">Œu</seg>f</w> <w n="4.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="4.4">g<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">î</seg>t</w><caesura></caesura> <w n="4.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="4.6" punct="vg:12">p<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="366" place="10" mp="M">e</seg>nn<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">F<seg phoneme="a" type="vs" value="1" rule="193" place="1">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.2">gu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="5.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rds</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5">v<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="5.7">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="5.8"><seg phoneme="u" type="vs" value="1" rule="426" place="9">où</seg></w> <w n="5.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="5.10">j<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg>n<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1" punct="vg:2">Tr<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.3">gu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="6.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="6.8" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rfl<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="7.8" punct="vg:12">pl<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="8.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="8.10" punct="vg:12">bl<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">H<seg phoneme="y" type="vs" value="1" rule="453" place="1" mp="M">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="9.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="9.3" punct="vg:6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.5">m<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>ti<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="10.2" punct="vg:2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="10.4">c<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" punct="vg">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg></w> <w n="10.7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="11" mp="Lc">oi</seg></w>-<w n="10.8" punct="pe:12">m<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg></rhyme></w> !…</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:2">F<seg phoneme="a" type="vs" value="1" rule="193" place="1">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="11.2" punct="vg:4">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="11.3">gu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="11.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>c</w><caesura></caesura> <w n="11.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="11.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="11.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="11.9">t</w>’<w n="11.10"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="12.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="12.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="12.5">gr<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="12.6">d</w>’<w n="12.7"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="12.10">l<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rd</w> <w n="12.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="12.12" punct="pi:12">p<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ti<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pi">é</seg></rhyme></w> ?</l>
					</lg>
				</div></body></text></TEI>