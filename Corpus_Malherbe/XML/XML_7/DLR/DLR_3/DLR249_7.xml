<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSES</head><div type="poem" key="DLR249" modus="cp" lm_max="12" metProfile="8, 6=6" form="suite de strophes" schema="2(aabb) 3(abab)">
					<head type="main">EXHORTATION</head>
					<lg n="1" type="quatrain" rhyme="aabb">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">N</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="1.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="1.5" punct="vg:6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg">e</seg>s</w>, <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.7" punct="pe:8">f<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="2.3" punct="vg:4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg" mp="F">e</seg>s</w>, <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="2.6">s</w>’<w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="2.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="2.9">v<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="2.10"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Cr<seg phoneme="wa" type="vs" value="1" rule="440" place="1" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="3.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t</w> <w n="3.10" punct="vg:12">l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="12" punct="vg">à</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="4.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="4.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="4.8" punct="pi:12">D<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pi">a</seg></rhyme></w> ?</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="vg:2">F<seg phoneme="a" type="vs" value="1" rule="193" place="1">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="5.2">n</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="5.5">l</w>’<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.7">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2" punct="vg:2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="6.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="6.6" punct="pe:6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pe" caesura="1">ou</seg>x</w> !<caesura></caesura> <w n="6.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">I</seg>l</w> <w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="6.9" punct="pe:9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" punct="pe">em</seg>ps</w> ! <w n="6.10"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="C">I</seg>l</w> <w n="6.11"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="6.12">t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">em</seg>ps</rhyme></w></l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="2" mp="M">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="7.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="7.6">p<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="440" place="8" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="7.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>r</w> <w n="7.8">f<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">s</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t</w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="8.7">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8">e</seg>ds</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="8.9">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="8.10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>gt</w> <w n="8.11" punct="pt:12"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="12" mp6="M" mp4="F" met="8+4"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.3" punct="vg:4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg" mp="F">e</seg>s</w>, <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="10.5" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">â</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="10.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="10.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.8" punct="pe:12">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pe">in</seg>s</rhyme></w> !</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="11.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="9">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.7" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="ø" type="vs" value="1" rule="398" place="1">Eu</seg>x</w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="12.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="12.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="12.7">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>cs</w> <w n="12.8" punct="pt:12">f<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffr<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="13.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="13.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="14.2">d<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="14.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7" mp="M">en</seg>t<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="14.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="14.6" punct="pt:12">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>sp<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="pt">e</seg>ct</rhyme></w>.</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="15.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="15.5">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" caesura="1">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="15.8">r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">N<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>c<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="16.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="C">eu</seg>r</w> <w n="16.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="16.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="16.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="16.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="16.9" punct="pt:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>r<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="pt">e</seg>ct</rhyme></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="aabb">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="17.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="18.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rsqu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="18.4">c<seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="18.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>rs</w> <w n="18.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="18.7" punct="pt:12">g<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="19" num="5.3" lm="12" mp6="M/mp" met="4+4+4"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" mp="M/mp">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="19.3" punct="pe:4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4" punct="pe" caesura="1">en</seg></w> !<caesura></caesura> <w n="19.4">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M/mp">o</seg>rl<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M/mp">o</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="7" mp="Lp">ez</seg></w>-<w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" caesura="2">e</seg>s</w><caesura></caesura> <w n="19.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="19.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="19.8" punct="pe:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>x</rhyme></w> !</l>
						<l n="20" num="5.4" lm="12" met="6+6">—<w n="20.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="20.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>ls</w> <w n="20.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="20.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rts</w> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="20.6" punct="vg:6">fi<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg" caesura="1">e</seg>rs</w>,<caesura></caesura> <w n="20.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="20.8">qu</w>’<w n="20.9"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M/mp">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9" mp="Lp">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>-<w n="20.10" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>ls</w>,<w n="20.11">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="20.12" punct="pi:12">v<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pi">ou</seg>s</rhyme></w> ?</l>
					</lg>
				</div></body></text></TEI>