<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSES</head><div type="poem" key="DLR243" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="3(abba)">
					<head type="main">ACCUEIL</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="1.2" punct="po:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="3" mp="M">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r</w><caesura></caesura> (<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.5">d</w>’<w n="1.6" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" caesura="1">ai</seg></w><caesura></caesura> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="2.7">s<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>il</rhyme></w></l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="3.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="3.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" mp="P">e</seg>rs</w> <w n="3.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="3.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="3.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="3.9" punct="vg:10">f<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="4.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="4.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="4.6" punct="pt:10">n<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pt">oi</seg>rs</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="5.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3" punct="pe:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe" caesura="1">a</seg>s</w> !<caesura></caesura> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.5">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rs</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="5.8" punct="vg:10">s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">t</w>’<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" caesura="1">ai</seg></w><caesura></caesura> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.6">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="6.7">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="6.9" punct="pv:10">m<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="pv">o</seg>l</rhyme></w> ;</l>
						<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">t</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" caesura="1">ai</seg></w><caesura></caesura> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.7">d</w>’<w n="7.8" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>d<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">t</w>’<w n="8.3"><seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" caesura="1">ai</seg></w><caesura></caesura> <w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="8.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="8.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="8.8" punct="pt:10">d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pt">oi</seg>gts</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="9.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="9.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="9.4" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>t</w>, <w n="9.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="9.6" punct="ps:10">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<rhyme label="a" id="6" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></w>…</l>
						<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="10.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="10.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>h<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rs</rhyme></w></l>
						<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>t</w><caesura></caesura> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.8">d</w>’<w n="11.9"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r</rhyme></w></l>
						<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="12.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="12.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="12.7" punct="pt:10">b<seg phoneme="o" type="vs" value="1" rule="315" place="9" mp="M">eau</seg>t<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>