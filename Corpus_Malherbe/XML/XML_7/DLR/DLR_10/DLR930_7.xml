<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR930" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="3(aaa)">
				<head type="main">BOUTONS D’OR</head>
				<lg n="1" type="tercet" rhyme="aaa">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="1.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.6">pr<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="2.3" punct="vg:3">m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3" punct="vg">ai</seg></w>, <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="2.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</rhyme></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="3.5">h<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ts</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.7" punct="pt:8">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" type="tercet" rhyme="aaa">
					<l n="4" num="2.1" lm="8" met="8"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.6" punct="vg:8">bl<rhyme label="a" id="2" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="5" num="2.2" lm="8" met="8"><w n="5.1">C<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="5.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.6" punct="pv:8">qu<rhyme label="a" id="2" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="6" num="2.3" lm="8" met="8"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="6.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.7" punct="pt:8">li<rhyme label="a" id="2" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="tercet" rhyme="aaa">
					<l n="7" num="3.1" lm="8" met="8"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="7.4">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="7.5" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>r</w>, <w n="7.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.7">cr<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</rhyme></w></l>
					<l n="8" num="3.2" lm="8" met="8"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="8.3" punct="vg:3">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="8.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="8.9" punct="vg:8">fr<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>d</rhyme></w>,</l>
					<l n="9" num="3.3" lm="8" met="8"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="9.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="9.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>ps</w>-<w n="9.6" punct="pt:8">r<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pt">oi</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>