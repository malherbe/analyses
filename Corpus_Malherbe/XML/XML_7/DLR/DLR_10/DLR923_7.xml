<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR923" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2(abab) 2(abba)">
				<head type="main">MAI</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">m<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="vg">ai</seg></w>, <w n="1.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">l</w>’<w n="1.8"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="2.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="2.5" punct="pe:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pe">in</seg></rhyme></w> !</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="3.3" punct="vg:3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="3.4">l</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w>-<w n="3.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="3.7">m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="4.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rt</w> <w n="5.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.7" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rv<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="6.3" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="6.4">l</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.6" punct="pt:8">c<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rt</rhyme></w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6">j<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="8.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g</w> <w n="8.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.8" punct="pe:8">v<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="9.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="9.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="9.7"><rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.5" punct="vg:8">n<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1" punct="vg:1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" punct="vg">e</seg></w>, <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="11.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="353" place="6">e</seg>ffr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="12.6" punct="pe:8">l<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>t</rhyme></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2" punct="vg:2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.5" punct="vg:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">d</w>’<w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="14.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="14.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>r</rhyme></w>.</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="15.2" punct="pe:2">m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2" punct="pe">ai</seg></w> ! <w n="15.3">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>mm<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="16.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="16.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="16.5">n</w>’<w n="16.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="16.7" punct="pt:8">m<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>