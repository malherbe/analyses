<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES TERRESTRES</head><div type="poem" key="DLR155" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="3(abba)">
					<head type="main">PREMIÈRE DÉCEMBRALE</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pt:3">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pt">e</seg></w>. <w n="1.2">T<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="1.3">n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="1.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6" punct="vg:8">plu<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="2.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="2.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="2.5">b<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</rhyme></w></l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">c<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.7">v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>x</rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="4.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.6" punct="pt:8">plu<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="vg:3">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rge<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="5.5">br<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="6.3" punct="pv:8">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pv">en</seg>t</rhyme></w> ;</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="7.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</rhyme></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="8.3">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l</w> <w n="8.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="8.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.7" punct="pt:8">br<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="pt:3">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pt">e</seg></w>. <w n="9.2">L<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="9.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="9.5" punct="pv:8">v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="10.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="10.7" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>h<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rs</rhyme></w>.</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="11.6">m<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rts</rhyme></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="12.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>t</w> <w n="12.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="12.7" punct="pe:8">v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></rhyme></w> !…</l>
					</lg>
				</div></body></text></TEI>