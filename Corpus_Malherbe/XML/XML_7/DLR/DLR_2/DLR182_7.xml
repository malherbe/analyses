<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME DES CHOSES</head><div type="poem" key="DLR182" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="3(abba)">
					<head type="main">LE RIDEAU</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="1.4">l</w>’<w n="1.5">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="1.6">d</w>’<w n="1.7">h<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>b<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s</rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="2.2" punct="vg:3">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="2.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g</w> <w n="2.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7">m<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="3.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t</w>, <w n="3.4" punct="vg:6">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="3.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="3.6">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.7">d</w>’<w n="3.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="4.4">ch<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>ch<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="4.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="4.7" punct="pt:12">pl<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="5.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="5.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="5.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.7">R<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.8">f<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10" mp="M">ai</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="6.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="6.6">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.7" punct="vg:12">g<seg phoneme="ɛ" type="vs" value="1" rule="366" place="11" mp="M">e</seg>mm<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="7.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="7.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.6">g<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="7.7">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="8.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.8" punct="vg:12">b<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="9.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>t</w><caesura></caesura> <w n="9.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="9.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="9.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>g<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rgn<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="10.6">l</w>’<w n="10.7" punct="vg:12"><seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>s<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="11.4">br<seg phoneme="u" type="vs" value="1" rule="427" place="4" mp="M">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rds</w> <w n="11.5">l<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rds</w><caesura></caesura> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.7">s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="11.8"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.10" punct="vg:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>s<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="12.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="12.5"><seg phoneme="i" type="vs" value="1" rule="497" place="6" caesura="1">Y</seg>s</w><caesura></caesura> <w n="12.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="12.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9" punct="ps:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">E</seg>ls<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="ps">eu</seg>r</rhyme></w>…</l>
					</lg>
				</div></body></text></TEI>