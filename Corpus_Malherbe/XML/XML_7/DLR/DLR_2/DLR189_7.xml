<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FUMÉES</head><div type="poem" key="DLR189" modus="cm" lm_max="12" metProfile="6=6" form="suite de strophes" schema="3[abab]">
					<head type="main">LES COUSSINS OÙ FLAMBOIENT…</head>
					<lg n="1" type="regexp" rhyme="ab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="1.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="1.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="1.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-29">e</seg>nt</w><caesura></caesura> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="1.9">m<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12">ain</seg>s</rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>rs</w> <w n="2.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>tr<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="ab">
						<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="3.2">br<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.4">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.7" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rm<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg>s</rhyme></w>,</l>
						<l n="4" num="2.2" lm="12" met="6+6"><w n="4.1">H<seg phoneme="o" type="vs" value="1" rule="435" place="1" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="4.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ds</w> <w n="4.3" punct="vg:6">r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg" caesura="1">eau</seg>x</w>,<caesura></caesura> <w n="4.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="4.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>ils</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="4.7" punct="pe:12">fr<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="3" type="regexp" rhyme="ab">
						<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="5.2">j<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="5.4" punct="vg:4">r<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>x</w>, <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="5.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rt</w><caesura></caesura> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="5.8" punct="vg:8">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></w>, <w n="5.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="5.10">m<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.11"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="5.12" punct="vg:12">gr<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="6" num="3.2" lm="12" met="6+6"><w n="6.1">L</w>’<w n="6.2" punct="vg:1"><seg phoneme="œ" type="vs" value="1" rule="286" place="1" punct="vg">œ</seg>il</w>, <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="6.5">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="6.6" punct="vg:9"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>s</w>, <w n="6.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rd</w> <w n="6.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="6.9" punct="pv:12">p<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="4" type="regexp" rhyme="ab">
						<l n="7" num="4.1" lm="12" met="6+6"><w n="7.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="7.2" punct="vg:3">m<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg></w> <w n="7.6">qu</w>’<w n="7.7"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="7.9" punct="vg:12">pr<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="8" num="4.2" lm="12" mp6="M" mp4="P" met="8+4"><w n="8.1">N<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="8.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d</w> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.6">cl<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="8.7" punct="vg:8">p<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="8.9" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="477" place="12">ï</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="5" type="regexp" rhyme="ab">
						<l n="9" num="5.1" lm="12" met="6+6"><w n="9.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="9.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg" mp="F">e</seg>s</w>, <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rc</w><caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="9.8"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.9">b<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg>x</w> <w n="9.10">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>tr<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>x</rhyme></w></l>
						<l n="10" num="5.2" lm="12" met="6+6"><w n="10.1" punct="vg:3">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="355" place="2">e</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="10.2"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="10.3">s</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.7" punct="ps:12">b<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>z<rhyme label="b" id="6" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></w>…</l>
					</lg>
					<lg n="6" type="regexp" rhyme="ab">
						<l n="11" num="6.1" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="P">a</seg>r</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="11.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.5">l<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ct</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="11.9" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rr<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg>x</rhyme></w>,</l>
						<l n="12" num="6.2" lm="12" met="6+6"><w n="12.1">H<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="12.2">P<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="12.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg></w><caesura></caesura> <w n="12.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="12.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10">e</seg>r</w> <w n="12.8" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rb<rhyme label="b" id="6" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>