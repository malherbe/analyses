<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES TERRESTRES</head><div type="poem" key="DLR143" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="2(aabba) 2(aabab)">
					<head type="main">AU PRINTEMPS</head>
					<lg n="1" type="quintil" rhyme="aabba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="1.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>s</w> <w n="1.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="1.9">j<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.4">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="2.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="2.8">s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3" punct="vg:3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>rs</w>, <w n="3.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="3.5" punct="vg:6">ti<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="3.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="3.7" punct="vg:9"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="9" punct="vg">eu</seg>r</w>, <w n="3.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="3.9" punct="vg:12">p<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12" punct="vg">en</seg>t</rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="P">o</seg>rs</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="4.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rge<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="4.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="4.6">cl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="4.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12">en</seg>t</rhyme></w></l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2" punct="vg:3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>r</w>, <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.5">j<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="5.8" punct="pt:12">v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quintil" rhyme="aabba">
						<l n="6" num="2.1" lm="12" met="6+6"><w n="6.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.8"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg>x</rhyme></w></l>
						<l n="7" num="2.2" lm="12" met="6+6"><w n="7.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="7.3">g<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>ts</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="7.6" punct="pt:12"><seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg>x</rhyme></w>.</l>
						<l n="8" num="2.3" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">li<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="8.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="8.7">s</w>’<w n="8.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="9" num="2.4" lm="12" met="6+6"><w n="9.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="9.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="9.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5" mp="M">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="9.6">d</w>’<w n="9.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.8">fl<seg phoneme="y" type="vs" value="1" rule="445" place="9">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.9" punct="vg:12">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">am</seg>p<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="2.5" lm="12" met="6+6"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>ct</w> <w n="10.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="10.9">n<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="10.10" punct="pt:12"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="432" place="12" punct="pt">o</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quintil" rhyme="aabab">
						<l n="11" num="3.1" lm="12" met="6+6"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="11.4" punct="vg:4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" punct="vg">e</seg></w>, <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>g</w><caesura></caesura> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="11.8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="11.9" punct="vg:12">b<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="12" num="3.2" lm="12" met="6+6"><w n="12.1">M<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>ss<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="12.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="13" num="3.3" lm="12" met="6+6"><w n="13.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="13.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="13.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.7">pr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s</w> <w n="13.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="13.9" punct="vg:12">fl<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="14" num="3.4" lm="12" met="6+6"><w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="14.2">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="14.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg>s</w><caesura></caesura> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="14.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>rs</w> <w n="14.6">m<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="15" num="3.5" lm="12" met="6+6"><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="15.3">gr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="15.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="15.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rds</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="15.7" punct="vg:12">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">aî</seg>ch<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></w>,</l>
					</lg>
					<lg n="4" type="quintil" rhyme="aabab">
						<l n="16" num="4.1" lm="12" met="6+6"><w n="16.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="Lc">u</seg>squ</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="16.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="16.4" punct="vg:4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" punct="vg">e</seg></w>, <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="16.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.8"><seg phoneme="u" type="vs" value="1" rule="426" place="10">où</seg></w> <w n="16.9">l</w>’<w n="16.10"><seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg></w> <w n="16.11" punct="vg:12">br<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="17" num="4.2" lm="12" met="6+6"><w n="17.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="17.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="5" mp="M">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>t</w><caesura></caesura> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="17.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="17.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="18" num="4.3" lm="12" met="6+6"><w n="18.1" punct="vg:1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" punct="vg">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="18.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="18.5">d</w>’<w n="18.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="18.7">f<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="18.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="18.9" punct="vg:12">m<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="12" punct="vg">ai</seg></rhyme></w>,</l>
						<l n="19" num="4.4" lm="12" met="6+6"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="19.2">s</w>’<w n="19.3" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rt</w>, <w n="19.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="19.6">l</w>’<w n="19.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="19.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="19.9">l</w>’<w n="19.10" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="20" num="4.5" lm="12" met="6+6"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="20.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="20.5">gr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="20.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="20.7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>ps</w> <w n="20.8" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>m<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>