<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HEURES INTIMES</head><div type="poem" key="DLR219" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="2(aabb) 1(abab) 1(abba)">
					<head type="main">L’ORAGE À LA FENÊTRE</head>
					<lg n="1" type="quatrain" rhyme="aabb">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="1.3" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="1.5" punct="vg:9">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="9" punct="vg">er</seg></w>, <w n="1.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="1.8" punct="vg:12">d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="2.5">gr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="2.6" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="3.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ds</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rcs</w><caesura></caesura> <w n="3.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.8">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="4.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="C">eu</seg>rs</w> <w n="4.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>rs<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="4.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="355" place="8">e</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg>x</w> <w n="4.6" punct="pt:12"><seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="5.3" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="5.5" punct="vg:6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">oin</seg></w>,<caesura></caesura> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.7">tu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="5.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="5.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="5.10">t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>t</rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1" punct="vg:2">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2" punct="vg">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="6.7">d</w>’<w n="6.8" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>st<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" mp="M">im</seg>pl<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="7.6">c<seg phoneme="o" type="vs" value="1" rule="415" place="9" mp="M">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="7.8" punct="vg:12">t<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>f<seg phoneme="a" type="vs" value="1" rule="307" place="5" mp="M">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.6">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="9" mp="M">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="8.7"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg>x</w> <w n="8.8" punct="pv:12">t<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="aabb">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="9.2">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="P">o</seg>rs</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="9.5" punct="vg:9">m<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="9.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>ti<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>s</rhyme></w></l>
						<l n="10" num="3.2" lm="12" met="6+6">‒ <w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="10.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>c</w><caesura></caesura> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="10.7" punct="vg:12">fr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ss<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg in">on</seg>s</rhyme></w>, ‒</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="11.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="11.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="11.7">m<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.6" punct="pv:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" mp="M">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="13.4" punct="vg:8">Kl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>pst<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>ck</w>, <w n="13.5"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="13.6" punct="vg:12">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rd<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></rhyme></w>,</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="14.2" punct="vg:3">M<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="14.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="14.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8" mp="M">ê</seg>v<seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="14.6" punct="vg:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="15.4" punct="vg:5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg">e</seg>l</w>, <w n="15.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg></w><caesura></caesura> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="15.7">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="15.8" punct="vg:12">m<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">T<seg phoneme="o" type="vs" value="1" rule="435" place="1" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="16.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="16.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="16.8" punct="ps:12">j<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rd<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="ps">in</seg></rhyme></w>…</l>
					</lg>
				</div></body></text></TEI>