<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FUMÉES</head><div type="poem" key="DLR195" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="1[abab] 1[ababa] 1[aa]">
					<head type="main">L’AMPLE VILLE…</head>
					<lg n="1" type="regexp" rhyme="ab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">am</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>t</w><caesura></caesura> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="1.9" punct="vg:12">l<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="vg">oin</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="2.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="2.8" punct="pt:12">f<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>m<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="ab">
						<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="3.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="3.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="3.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="3.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="3.9" punct="vg:12">c<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="vg">oin</seg></rhyme></w>,</l>
						<l n="4" num="2.2" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg></w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>s</w> <w n="4.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10" mp="Lc">en</seg></w>-<w n="4.8" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11" mp="M/mc">ai</seg>m<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="3" type="regexp" rhyme="ab">
						<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">pl<seg phoneme="ø" type="vs" value="1" rule="404" place="2" mp="M">eu</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="5.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6" caesura="1">en</seg>s</w><caesura></caesura> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="5.9" punct="vg:10">t<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>ts</w>, <w n="5.10">l<seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="Lc">à</seg></w>-<w n="5.11" punct="pv:12">b<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pv">a</seg>s</rhyme></w> ;</l>
						<l n="6" num="3.2" lm="12" met="6+6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="6.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="6.5">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="7" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="6.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="6.8" punct="pv:12">cl<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="4" type="regexp" rhyme="ab">
						<l n="7" num="4.1" lm="12" met="6+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2">pl<seg phoneme="ø" type="vs" value="1" rule="404" place="2" mp="M">eu</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="7.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="9">en</seg>s</w> <w n="7.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="7.8">n</w>’<w n="7.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t</w> <w n="7.10">p<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s</rhyme></w></l>
						<l n="8" num="4.2" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">l<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="8.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="8.5">ch<seg phoneme="e" type="vs" value="1" rule="347" place="7" mp="P">ez</seg></w> <w n="8.6" punct="vg:8"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>, <w n="8.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="8.9" punct="pv:12">m<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="5" type="regexp" rhyme="aa">
						<l n="9" num="5.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="9.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.4">gr<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="9.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="9.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="9.7">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="9.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="9.9">y<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="9.10" punct="vg:12">l<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</rhyme></w>,</l>
						<l n="10" num="5.2" lm="12" met="6+6"><w n="10.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="10.2">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="10.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>rs</w> <w n="10.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8" punct="vg:12">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg><rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="6" type="regexp" rhyme="a">
						<l n="11" num="6.1" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="11.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.5">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>gts</w><caesura></caesura> <w n="11.6">d<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="11.8" punct="ps:12">Cl<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>p<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></w>…</l>
					</lg>
				</div></body></text></TEI>