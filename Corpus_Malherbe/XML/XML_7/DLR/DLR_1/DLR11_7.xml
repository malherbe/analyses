<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR11" modus="cm" lm_max="10" metProfile="5+5" form="suite de strophes" schema="1[abab] 2[abba] 3[aa]">
					<head type="main">PAR LES PRÉS</head>
					<lg n="1" type="regexp" rhyme="abababbaabbaaaaaaa">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="P">a</seg>r</w> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.6">pr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="1.7" punct="vg:10">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rm<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>ds</rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="2.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>s</w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="2.7">l</w>’<w n="2.8">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.9" punct="dp:10">dr<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="454" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="3.2">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg></w><caesura></caesura> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="3.5">m<seg phoneme="wa" type="vs" value="1" rule="421" place="7" mp="M">oi</seg>n<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</w> <w n="3.6">g<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rm<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ds</rhyme></w></l>
						<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="5" caesura="1">ô</seg>t</w><caesura></caesura> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="4.5">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="5" num="1.5" lm="10" met="5+5"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.3">cr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="5.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" caesura="1">en</seg>t</w><caesura></caesura> <w n="5.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="5.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="5.9">m<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>t</rhyme></w></l>
						<l n="6" num="1.6" lm="10" met="5+5"><w n="6.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>s</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="6.4">p<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg>s</w><caesura></caesura> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="6.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="6.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w> <w n="6.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="6.9" punct="vg:10">b<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="10" met="5+5"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="7.4">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" caesura="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="7.6">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.7">r<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="10" met="5+5"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="8.5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5" caesura="1">e</seg>ds</w><caesura></caesura> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="8.8">bru<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="8.9" punct="pt:10">m<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>t</rhyme></w>.</l>
						<l n="9" num="1.9" lm="10" met="5+5"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="9.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.7" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>mm<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="1.10" lm="10" met="5+5"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="10.2">gr<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="10.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="10.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>r</w><caesura></caesura> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="10.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="10.8" punct="pv:10">h<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pv">ou</seg>x</rhyme></w> ;</l>
						<l n="11" num="1.11" lm="10" met="5+5"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="11.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="11.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="11.5" punct="vg:5">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="5" punct="vg" caesura="1">eu</seg>s</w>,<caesura></caesura> <w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="11.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</w> <w n="11.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="11.9" punct="pv:10">r<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pv">ou</seg>x</rhyme></w> ;</l>
						<l n="12" num="1.12" lm="10" met="5+5"><w n="12.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="315" place="5" caesura="1">eau</seg>x</w><caesura></caesura> <w n="12.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="C">u</seg></w> <w n="12.7">ru<seg phoneme="i" type="vs" value="1" rule="491" place="7" mp="M">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="12.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="12.9" punct="vg:10">d<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="13" num="1.13" lm="10" met="5+5"><w n="13.1">L</w>’<w n="13.2">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.3">h<seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="M">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>t</w><caesura></caesura> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="13.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="13.7" punct="pt:10">v<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" punct="pt">e</seg>rt</rhyme></w>.</l>
						<l n="14" num="1.14" lm="10" met="5+5"><w n="14.1">J</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="14.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg>s</w><caesura></caesura> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="14.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="14.8">l</w>’<w n="14.9">h<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>v<rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="10">e</seg>r</rhyme></w></l>
						<l n="15" num="1.15" lm="10" met="5+5"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">Ai</seg>t</w> <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>gl<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="15.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="15.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="15.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="15.7" punct="vg:10">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rm<rhyme label="a" id="8" gender="f" type="a" stanza="5"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="16" num="1.16" lm="10" met="5+5"><w n="16.1" punct="vg:2">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="16.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="16.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="16.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="16.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</w> <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="16.7" punct="vg:10">g<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<rhyme label="a" id="8" gender="f" type="e" stanza="5"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="17" num="1.17" lm="10" met="5+5"><w n="17.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="17.2" punct="vg:3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</w>, <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="17.4">gr<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="17.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="17.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>s</w> <w n="17.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>t<rhyme label="a" id="9" gender="m" type="a" stanza="6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ts</rhyme></w></l>
						<l n="18" num="1.18" lm="10" met="5+5"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">c<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="18.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>s</w><caesura></caesura> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="18.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="18.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>gt</w> <w n="18.8" punct="pt:10"><rhyme label="a" id="9" gender="m" type="e" stanza="6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pt">an</seg>s</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>