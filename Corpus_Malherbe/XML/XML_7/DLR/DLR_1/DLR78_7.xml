<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RONDELS</head><div type="poem" key="DLR78" modus="sm" lm_max="8" metProfile="8" form="rondel classique" schema="ABba abAB abbaA">
					<head type="main">LES CRABES</head>
					<lg n="1" rhyme="ABba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="1.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="1.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="1.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="A" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="2.2">cr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="2.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="2.6" punct="pt:8">fl<rhyme label="B" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="3.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.6">l<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t</w> <w n="4.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="4.6" punct="pt:8">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abAB">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="5.2">gr<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="5.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="5.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</rhyme></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">g<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6" punct="pv:8">pl<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="7.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="7.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="7.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="A" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="8.2">cr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="8.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.6" punct="pt:8">fl<rhyme label="B" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="abbaA">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="9.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="10.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="10.5" punct="vg:8">cl<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="11.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="11.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.6" punct="vg:8">cl<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1" punct="vg:3">H<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="12.2">b<seg phoneme="u" type="vs" value="1" rule="d-2" place="4">ou</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>v<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></w></l>
						<l n="13" num="3.5" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="13.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="13.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="13.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="A" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>