<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR14" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="9((aa))">
					<head type="main">BEAU JOUR</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1" mp="C">e</seg>t</w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="1.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>x</w><caesura></caesura> <w n="1.6">qu</w>’<w n="1.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="1.8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="1.11">t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="pt:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" punct="pt">o</seg>r</w>. <w n="2.2">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="2.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="2.5">j</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="2.7">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="2.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.10"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="3.2" punct="vg:5">chr<seg phoneme="i" type="vs" value="1" rule="493" place="2" mp="M">y</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="3.3">l</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" caesura="1">un</seg></w><caesura></caesura> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="3.7">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</w> <w n="3.8" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>cl<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pv">o</seg>s</rhyme></w> ;</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="4.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="4.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="4.8">m<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="4.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="4.10">cl<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="12">o</seg>s</rhyme></w></l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="5.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="5.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="5.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="5.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="5.9">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="11">e</seg>r</w> <w n="5.10" punct="vg:12">bl<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.5" punct="vg:6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">oin</seg>gs</w>,<caesura></caesura> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="6.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="6.8">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="6.10">qu<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">En</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="7.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="7.4" punct="vg:6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" punct="vg" caesura="1">e</seg>ds</w>,<caesura></caesura> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="7.6" punct="vg:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</w>, <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="7.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="7.9">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>s</w> <w n="7.10">l<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>ts</rhyme></w></l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1" punct="vg:2">P<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="8.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="8.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>rs</w> <w n="8.5">gr<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="8.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="8.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>ts</rhyme></w>,</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="9.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="9.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="9.5">p<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="9.6">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="9.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="9.9" punct="vg:12"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">t<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>r<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="10.3">s</w>’<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="10.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="10.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="10.9">p<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3">g<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.4">qu</w>’<w n="11.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="11.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="11.8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="11.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="11.10" punct="vg:12">pr<seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg>ss<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>rs</rhyme></w>,</l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="12.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="12.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8" mp="M">o</seg>mn<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="12.7" punct="vg:12">s<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>rs</rhyme></w>,</l>
						<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="13.2" punct="vg:2">bru<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>t</w>, <w n="13.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="13.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="13.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="13.8">r<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>x</w> <w n="13.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="13.10" punct="vg:12">br<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="14.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="14.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="14.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="14.7" punct="pt:12">bl<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="15.2" punct="vg:3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="15.3">j</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="15.5">v<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="15.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="15.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">im</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></w></l>
						<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="16.2" punct="vg:3">ch<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="16.3"><seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="16.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="16.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></w></l>
						<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1" punct="vg:3">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="355" place="2">e</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="17.2" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="17.3" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="17.4" punct="vg:12">ps<seg phoneme="i" type="vs" value="1" rule="493" place="10" mp="M">y</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="18" num="1.18" lm="12" met="6+6"><w n="18.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1" mp="C">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="18.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="18.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="18.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="18.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7"><seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>gl<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>