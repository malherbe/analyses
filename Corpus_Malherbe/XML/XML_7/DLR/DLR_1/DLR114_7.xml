<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR114" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="1(aabb) 1(abab) 1(abba)">
					<head type="main">QUATRIÈME NOCTURNE</head>
					<lg n="1" type="quatrain" rhyme="aabb">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="4">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="9" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="476" place="10">ï</seg>s</w> <w n="1.8" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">F<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="2.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="2.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>rm<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="10">en</seg>t</w> <w n="2.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="2.8">s<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>s</rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">m<seg phoneme="i" type="vs" value="1" rule="493" place="2" mp="M">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d</w><caesura></caesura> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.5">f<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.6" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="b" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="4.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>ps</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.7" punct="pe:8">r<rhyme label="b" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="5.5">s</w>’<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="5.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="5.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="6.4">s</w>’<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="6.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="6.9">pl<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="7.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="7.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="7.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="7.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>ds</w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="8.5">d</w>’<w n="8.6" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<rhyme label="b" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>t</w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="9.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem/mp">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="Lp">a</seg>rds</w>-<w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="6" caesura="1">à</seg></w><caesura></caesura> <w n="9.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="9.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="9.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="9.8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>rs</w> <w n="9.9" punct="vg:12">p<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>rs</rhyme></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">c<seg phoneme="ø" type="vs" value="1" rule="398" place="2" mp="Lp">eu</seg>x</w>-<w n="10.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="10.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="10.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="10.7">l<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8" punct="vg:12">m<seg phoneme="u" type="vs" value="1" rule="428" place="11" mp="M">ou</seg>ill<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="11.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="11.5">r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.5">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.6" punct="pt:8">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>rs</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>