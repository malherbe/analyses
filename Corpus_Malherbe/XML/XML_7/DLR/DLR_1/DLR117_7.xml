<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR117" modus="cp" lm_max="12" metProfile="8, 6−6" form="suite de strophes" schema="1[abab] 6[aa] 1[abba]">
					<head type="main">LE SOMMEIL I</head>
					<lg n="1" type="regexp" rhyme="ababa">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rç<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.6">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.7">d</w>’<w n="1.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>r</rhyme></w> ;</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="2.6" punct="vg:8">d<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rt</rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>ts</w> <w n="3.3">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>ts</w> <w n="3.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg" caesura="1">e</seg>c</w>,<caesura></caesura> <w n="3.5" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</w>, <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="3.7">r<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg>x</w> <w n="3.8">l<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rds</rhyme></w></l>
						<l n="4" num="1.4" lm="12" mp6="C" met="6−6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" mp="M">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="4.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="4.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="8" mp="M">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="4.7">m<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt</rhyme></w></l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="3">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.4">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5" mp="M">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="5.5">l</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l</w> <w n="5.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="5.9" punct="pt:12">l<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="aaaaa">
						<l n="6" num="2.1" lm="12" met="6+6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3">g<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="6.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="6" caesura="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="6.7">ch<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="6.8"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="P">a</seg>r</w> <w n="6.10"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="7" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="7.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="7.5">v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>ts</w> <w n="7.6">j<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>ts</rhyme></w></l>
						<l n="8" num="2.3" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="8.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="8.7">dr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ps</w> <w n="8.8">f<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="11">en</seg>t</w> <w n="8.9" punct="vg:12"><rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="vg">oin</seg>ts</rhyme></w>,</l>
						<l n="9" num="2.4" lm="12" met="6+6"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="9.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.4">c<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="9.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="9.8">p<rhyme label="a" id="5" gender="f" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="2.5" lm="12" met="6+6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="10.4">t<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.7" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>p<rhyme label="a" id="5" gender="f" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="3" type="regexp" rhyme="abbaa">
						<l n="11" num="3.1" lm="12" met="6+6"><w n="11.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="11.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="11.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>s</w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>ts</w><caesura></caesura> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="11.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.8">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="11.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="11.10" punct="vg:12">p<rhyme label="a" id="6" gender="f" type="a" stanza="5"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="12" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="12.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>ds</w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="12.5" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<rhyme label="b" id="7" gender="m" type="a" stanza="5"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="13" num="3.3" lm="12" met="6+6"><w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.4">l<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="13.5">pr<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.7">r<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="13.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="13.9">l<rhyme label="b" id="7" gender="m" type="e" stanza="5"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t</rhyme></w></l>
						<l n="14" num="3.4" lm="12" met="6+6"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="14.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="14.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="14.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="14.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="14.9" punct="vg:12">b<rhyme label="a" id="6" gender="f" type="e" stanza="5"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="15" num="3.5" lm="12" met="6+6"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rps</w> <w n="15.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="15.4">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="15.5">d</w>’<w n="15.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="15.9">h<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.10"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="15.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="15.12" punct="vg:12">t<rhyme label="a" id="8" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="4" type="regexp" rhyme="aaaaa">
						<l n="16" num="4.1" lm="12" met="6+6"><w n="16.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>pt<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="16.4">v<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="16.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="16.6">l<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rd</w> <w n="16.7">cr<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>x</w> <w n="16.8" punct="vg:12">j<rhyme label="a" id="8" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="17" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="17.1">D</w>’<w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="17.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="17.4">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.5" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="a" id="9" gender="f" type="a" stanza="7"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="18" num="4.3" lm="12" met="6+6"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="18.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="18.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt</w> <w n="18.7" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>th<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="9" gender="f" type="e" stanza="7"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="19" num="4.4" lm="12" met="6+6"><w n="19.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="19.2">qu</w>’<w n="19.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="19.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="19.6"><seg phoneme="i" type="vs" value="1" rule="497" place="7" mp="C">y</seg></w> <w n="19.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="19.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="19.9"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>cr<rhyme label="a" id="10" gender="m" type="a" stanza="8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>s</rhyme></w></l>
						<l n="20" num="4.5" lm="12" met="6+6"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="2" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="20.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="20.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="20.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="20.7">b<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>x</w> <w n="20.8">p<seg phoneme="a" type="vs" value="1" rule="343" place="11" mp="M">a</seg>ï<rhyme label="a" id="10" gender="m" type="e" stanza="8"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12">en</seg>s</rhyme></w></l>
					</lg>
				</div></body></text></TEI>