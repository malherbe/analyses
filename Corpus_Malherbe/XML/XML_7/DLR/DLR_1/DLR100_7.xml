<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR100" modus="sp" lm_max="8" metProfile="8, 4" form="suite périodique" schema="7(aabb)">
					<head type="main">POUR D’AUCUNS</head>
					<lg n="1" type="quatrain" rhyme="aabb">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</w> <w n="1.7">f<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="2.4" punct="vg:4"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="4" punct="vg">où</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="3.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pe">e</seg>s</w> ! <w n="3.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="3.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="4.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="4.2" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></w>,</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="aabb">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="5.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></w></l>
						<l n="6" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="6.4">pr<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</rhyme></w></l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="1">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="7.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>xp<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.3">gr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="8.4" punct="vg:4">p<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="aabb">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="9.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pe">e</seg>s</w> ! <w n="9.3">V<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="9.4">n</w>’<w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="9.6">p<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
						<l n="10" num="3.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="10.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w>-<w n="10.3">b<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</rhyme></w></l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="7">y</seg>s<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="12" num="3.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="12.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="12.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="12.3" punct="vg:4">s<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></w>,</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="aabb">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="13.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="13.4" punct="vg:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</rhyme></w>,</l>
						<l n="14" num="4.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2" punct="vg:4">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg>s</rhyme></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="15.3">m<seg phoneme="y" type="vs" value="1" rule="445" place="7">û</seg>r<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="16" num="4.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="16.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="16.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></w>,</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="aabb">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="435" place="1">O</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="17.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>cs</w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.5" punct="vg:8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="18" num="5.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="18.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="18.2">s<seg phoneme="y" type="vs" value="1" rule="445" place="2">û</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></rhyme></w></l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="19.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="19.5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>d<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="20" num="5.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="20.1">H<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="20.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="20.3" punct="pv:4">d<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg>s</rhyme></w> ;</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="aabb">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="21.2">l</w>’<w n="21.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-27" place="4" punct="vg">e</seg></w>, <w n="21.4">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rs</w> <w n="21.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="21.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>br<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="22" num="6.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="22.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="22.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="22.4" punct="vg:4">gr<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.4">gl<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="23.6">p<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="24" num="6.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="24.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="24.3" punct="vg:4">ch<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="aabb">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1">L</w>’<w n="25.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="25.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="25.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="25.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</rhyme></w></l>
						<l n="26" num="7.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="26.1">L</w>’<w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="26.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="26.4">m<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</rhyme></w></l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="27.2" punct="vg:2">L<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="27.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="27.4" punct="vg:5">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="27.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="27.6" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>s<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="28" num="7.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">Â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="28.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="28.3" punct="pe:4">p<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>