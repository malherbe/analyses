<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME DES RUES</head><div type="poem" key="DLR71" modus="cm" lm_max="12" metProfile="6=6" form="suite de strophes" schema="1[abba] 2[abab] 1[aa]">
					<head type="main">SILHOUETTE</head>
					<lg n="1" type="regexp" rhyme="abbaa">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="1.2">qu</w>’<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.5">V<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="1.8">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg></w> <w n="1.9">C<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="249" place="12">œu</seg>r</rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="2.6" punct="vg:12">f<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">H<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="3.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="3.4">pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.6">h<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>t</w><caesura></caesura> <w n="3.7">d</w>’<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rch<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-27" place="11" mp="F">e</seg></w> <w n="3.9" punct="vg:12">h<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="4.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.5">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="4.7">bl<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>sph<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.8" punct="vg:12">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11" mp="M">o</seg>qu<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>tm<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rtr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.2">dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="5.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="5.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="5.7" punct="ps:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>th<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></w>…</l>
					</lg>
					<lg n="2" type="regexp" rhyme="babababaa">
						<l n="6" num="2.1" lm="12" met="6+6"><w n="6.1">N<seg phoneme="wa" type="vs" value="1" rule="440" place="1" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="6.4">gr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>s</rhyme></w></l>
						<l n="7" num="2.2" lm="12" met="6+6"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>xh<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="7.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="7.6"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>n<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.3" lm="12" mp7="Fm" met="8+4" mp4="P" mp5="C"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="8.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.5">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fm">e</seg></w>-<w n="8.6">c<seg phoneme="i" type="vs" value="1" rule="468" place="8" caesura="1">i</seg></w><caesura></caesura> <w n="8.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="8.8" punct="pv:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pv">on</seg>s</rhyme></w> ;</l>
						<l n="9" num="2.4" lm="12" met="6+6"><w n="9.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="9.2">d</w>’<w n="9.3">h<seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="M">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="9.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.6">pl<seg phoneme="i" type="vs" value="1" rule="469" place="9">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="9.8" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>pl<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="2.5" lm="12" met="6+6"><w n="10.1" punct="pe:2">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>tm<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>rtr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> « <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="451" place="6" caesura="1">u</seg>m</w><caesura></caesura> <w n="10.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rd<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> » <w n="10.6" punct="vg:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rp<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="vg">e</seg>l</rhyme></w>,</l>
						<l n="11" num="2.6" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="11.3">l</w>’<w n="11.4">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.7">r<seg phoneme="y" type="vs" value="1" rule="457" place="9">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="11.9" punct="vg:12">f<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="2.7" lm="12" mp6="M" met="4+4+4"><w n="12.1" punct="pe:2">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>tm<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>rtr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4" caesura="1">en</seg>s</w><caesura></caesura> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.4" punct="pe:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe" caesura="2">on</seg>s</w> !<caesura></caesura> <w n="12.5">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="12">e</seg>l</rhyme></w></l>
						<l n="13" num="2.8" lm="12" met="6+6"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="13.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="13.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="13.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>l<rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="14" num="2.9" lm="12" met="6+6"><w n="14.1" punct="ps:3">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>tm<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rtr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="ps" mp="F">e</seg></w>… <w n="14.2">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" mp="C">e</seg>t</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="14.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="14.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="14.6" punct="pe:12">B<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg>l<rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>