<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">MAI, JOLI MAI !</head><div type="poem" key="DLR806" modus="cm" lm_max="11" metProfile="5+6" form="suite périodique" schema="3(abba) 1(abab)">
					<head type="main">CRÉPUSCULE</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="11" met="5+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="1.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>r</w><caesura></caesura> <w n="1.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="1.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="1.8">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>rn<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="11" met="5+6"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="2.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>t</w><caesura></caesura> <w n="2.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="2.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="2.9" punct="pt:11">f<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" punct="pt">ai</seg>t</rhyme></w>.</l>
						<l n="3" num="1.3" lm="11" met="5+6"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" caesura="1">en</seg>d</w><caesura></caesura> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="3.7" punct="pt:11"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>ff<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="11" punct="pt">e</seg>t</rhyme></w>.</l>
						<l n="4" num="1.4" lm="11" met="5+6"><w n="4.1" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="vg">en</seg>t</w>, <w n="4.2">l</w>’<w n="4.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" caesura="1">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="4.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.6" punct="pt:11"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="11" met="5+6"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="5.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="5" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.6">p<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg>s</w> <w n="5.7">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="10" mp="M">eu</seg>r<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s</rhyme></w></l>
						<l n="6" num="2.2" lm="11" met="5+6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="6.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="6.4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg>s</w><caesura></caesura> <w n="6.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="6.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="6.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.9" punct="pt:11">br<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="7" num="2.3" lm="11" met="5+6"><w n="7.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>ff<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>s</w>, <w n="7.2" punct="vg:3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>ds</w>, <w n="7.3" punct="vg:5">l<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg" caesura="1">er</seg>s</w>,<caesura></caesura> <w n="7.4">g<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="7.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="7.6" punct="vg:11">bl<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="8" num="2.4" lm="11" met="5+6"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="8.2" punct="pe:3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pe">i</seg></w> ! <w n="8.3">L</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>r</w><caesura></caesura> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7">en</seg>t</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="8.7">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="9">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.8" punct="pt:11">gr<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="11" punct="pt">i</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="11" met="5+6"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="9.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="9.5">m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" caesura="1">ai</seg></w><caesura></caesura> <w n="9.6">f<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t</w> <w n="9.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="9.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt</w> <w n="9.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="9.10">s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="9.11">t<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="11" met="5+6"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="10.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>r</w><caesura></caesura> <w n="10.5">n</w>’<w n="10.6"><seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg></w> <w n="10.7">p<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="10.8" punct="pt:11">p<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11" punct="pt">oin</seg>t</rhyme></w>.</l>
						<l n="11" num="3.3" lm="11" met="5+6"><w n="11.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="11.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="11.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="11.8" punct="vg:11">l<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11" punct="vg">oin</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="11" met="5+6"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="12.2" punct="vg:2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>t</w>, <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="Lc">ou</seg>rd</w>’<w n="12.4" punct="vg:5">hu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="12.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="12.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="12.8" punct="pt:11">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>sc<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="11" met="5+6"><w n="13.1" punct="pe:3">M<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="13.2">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>r</w><caesura></caesura> <w n="13.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>t</w> <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="13.7" punct="pe:11">c<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="11" punct="pe">ou</seg>p</rhyme></w> !</l>
						<l n="14" num="4.2" lm="11" met="5+6"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="14.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">p<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="14.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">em</seg>bl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.7" punct="ps:11">br<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="ps" mp="F">e</seg></rhyme></w>…</l>
						<l n="15" num="4.3" lm="11" met="5+6"><w n="15.1">C</w>’<w n="15.2" punct="vg:1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1" punct="vg">e</seg>st</w>, <w n="15.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.4" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="15.5" punct="vg:6">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6" punct="vg">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="15.7">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="9">en</seg></w> <w n="15.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="15.9" punct="vg:11">l<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="11" punct="vg">ou</seg>p</rhyme></w>,</l>
						<l n="16" num="4.4" lm="11" met="5+6"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="16.3" punct="vg:5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="16.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="16.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="16.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="16.8" punct="pt:11">l<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>