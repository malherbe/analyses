<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">CI-GÎT</head><div type="poem" key="DLR823" modus="sp" lm_max="8" metProfile="8, 2" form="suite périodique" schema="1(abba) 5(abab)">
					<head type="main">RETOUR</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="1.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6" punct="vg:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="2" met="2"><space unit="char" quantity="12"></space><w n="2.1" punct="pi:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="pi">Où</seg></w> ? <w n="2.2" punct="pi:2">Qu<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="pi ps">an</seg>d</rhyme></w> ?…</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="3.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>lc<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="2" met="2"><space unit="char" quantity="12"></space><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2" punct="pt:2">gu<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="5.4" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg">e</seg>s</w>, <w n="5.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="5.6" punct="vg:8">t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>t</rhyme></w>,</l>
						<l n="6" num="2.2" lm="2" met="2"><space unit="char" quantity="12"></space><w n="6.1" punct="pt:2">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pt">e</seg></rhyme></w>.</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="7.5">t</w>-<w n="7.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.8" punct="vg:8">t<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="2" met="2"><space unit="char" quantity="12"></space><w n="8.1" punct="pi:2">P<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>r<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pi">e</seg></rhyme></w> ?</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:2">P<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2" punct="vg">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>lb<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>rt</w>, <hi rend="ital"><w n="9.3" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.4">c<seg phoneme="e" type="vs" value="1" rule="272" place="6">æ</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w></hi>,</l>
						<l n="10" num="3.2" lm="2" met="2"><space unit="char" quantity="12"></space><w n="10.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="10.2">d</w>’<w n="10.3" punct="vg:2"><rhyme label="b" id="6" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="11.2">Qu</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w>-<w n="11.4">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3">e</seg></w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="2" met="2"><space unit="char" quantity="12"></space><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2" punct="pi:2">n<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pi">e</seg>s</rhyme></w> ?</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="13.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="13.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="13.4" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>ts</rhyme></w>,</l>
						<l n="14" num="4.2" lm="2" met="2"><space unit="char" quantity="12"></space><w n="14.1" punct="pe:2">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>v<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></rhyme></w> !</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="15.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="15.5">d<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>ts</rhyme></w></l>
						<l n="16" num="4.4" lm="2" met="2"><space unit="char" quantity="12"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2" punct="pt:2">gr<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.5" punct="vg:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>rs</rhyme></w>,</l>
						<l n="18" num="5.2" lm="2" met="2"><space unit="char" quantity="12"></space><w n="18.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg>s</rhyme></w></l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="19.4">qu<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="19.5">h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</rhyme></w></l>
						<l n="20" num="5.4" lm="2" met="2"><space unit="char" quantity="12"></space><w n="20.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="20.2" punct="vg:2"><rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg>s</rhyme></w>,</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1" punct="vg:3">M<seg phoneme="y" type="vs" value="1" rule="d-3" place="1">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="21.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="21.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="21.4" punct="vg:8">l<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></w>,</l>
						<l n="22" num="6.2" lm="2" met="2"><space unit="char" quantity="12"></space><w n="22.1" punct="pt:2">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ffr<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pt">e</seg></rhyme></w>.</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w>-<w n="23.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="23.4">qu</w>’<w n="23.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="23.6">t</w>’<w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="23.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="23.9" punct="vg:8">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w>,</l>
						<l n="24" num="6.4" lm="2" met="2"><space unit="char" quantity="12"></space><w n="24.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="24.2" punct="pi:2">Fr<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pi">e</seg></rhyme></w> ?</l>
					</lg>
				</div></body></text></TEI>