<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">EN DEUIL</head><div type="poem" key="DLR681" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="1(abab) 3(abba)">
					<head type="main">AU COIN DU FEU</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="1.5" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="2.2">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="2.4" punct="vg:4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></w>, <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.6" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="2.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="y" type="vs" value="1" rule="d-3" place="9" mp="M">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="2.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="2.10" punct="vg:12">b<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="3.5" punct="vg:6">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="3.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7" mp="M">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="3.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="4.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.4" punct="vg:6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="4.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="4.8">v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>x</rhyme></w></l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.4" punct="vg:6">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg">e</seg>s</w>, <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.6" punct="dp:8">ch<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</rhyme></w> :</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="6.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.6">gr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="6.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="6.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="6.9" punct="vg:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">â</seg>t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="vg">ain</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="7.3" punct="vg:4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>d</w>, <w n="7.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="7.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="7.8" punct="vg:10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" punct="vg">an</seg>t</w>, <w n="7.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="7.10">t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg>t</rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="8.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="8.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="8.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="8.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="8.7">r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1" punct="vg:3">Fr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="9.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="9.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>t</rhyme></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="10.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="10.6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="10.7">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="6" caesura="1">um</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="10.10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.11" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>c<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="11.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="11.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="11.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="11.6">qu</w>’<w n="11.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="11.8">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.10" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>ss<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg></w>, <w n="12.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.3" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg></w>,<caesura></caesura> <w n="12.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="12.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.7">s<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s</w> <w n="12.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="12.9">l<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="12">à</seg></rhyme></w></l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="8" met="8"><space unit="char" quantity="8"></space>… <w n="13.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="13.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.3">n</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.7" punct="pt:8">p<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="14.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="6" caesura="1">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>s</w> <w n="14.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="14.8" punct="pt:12">t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>t</rhyme></w>.</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="15.2">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="15.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="15.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="15.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="15.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="15.8" punct="vg:8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>, <w n="15.9">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="15.10">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.11"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="15.12" punct="vg:12">t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></rhyme></w>,</l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="16.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="16.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="16.6">t</w>’<w n="16.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="16.8">d</w>’<w n="16.9"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="16.10" punct="pt:12">m<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>