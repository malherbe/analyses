<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR713" modus="sp" lm_max="8" metProfile="8, 5" form="suite de strophes" schema="1(aabbab) 1(aabb)">
				<head type="main">La Mauvaise Rencontre</head>
				<lg n="1" type="sizain" rhyme="aabbab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">An</seg>t<seg phoneme="wa" type="vs" value="1" rule="421" place="2">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.2">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.5">n<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="2.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="2.6" punct="pt:8">v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">H<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.2">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rgn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.5">l<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rgn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="4.2">d</w>’<w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="4.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>g</w> <w n="4.7">l</w>’<w n="4.8" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>b<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rgn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="5" met="5"><space unit="char" quantity="6"></space>« <w n="5.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="5.2" punct="pe:2"><seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="pe">o</seg>h</w> ! » <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="5.4" punct="pt:5">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="pt">oi</seg>r</rhyme></w>.</l>
					<l n="6" num="1.6" lm="5" met="5"><space unit="char" quantity="6"></space>« <w n="6.1" punct="pe:1">H<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="pe">i</seg></w> ! <w n="6.2" punct="pe:2">h<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pe ps">i</seg></w> !… » <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="6.4" punct="pt:5">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>b<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rgn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="aabb">
					<l n="7" num="2.1" lm="5" met="5"><space unit="char" quantity="6"></space>‒ <w n="7.1" punct="vg:3">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="7.2" punct="vg:5">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="8" num="2.2" lm="5" met="5"><space unit="char" quantity="6"></space><w n="8.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w>-<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="8.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="8.4" punct="vg:5">n<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="9" num="2.3" lm="8" met="8"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="9.4">s</w>’<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>nt</w>-<w n="9.6"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="10.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="10.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>x</w> <w n="10.6" punct="pi:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</rhyme></w> ?</l>
				</lg>
			</div></body></text></TEI>