<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR760" modus="sp" lm_max="8" metProfile="7, 8, (3)" form="suite de strophes" schema="1(abba) 1(aabb)">
				<head type="main">Mon Ombre</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="1.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="1.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="1.6">n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</rhyme></w></l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="3.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="3"><space unit="char" quantity="10"></space><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="4.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="4.3" punct="pt:3">m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="aabb">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">N<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w>-<w n="5.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="5.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="6">y</seg>s</w> <w n="5.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="5.7">rh<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="451" place="8">u</seg>m</rhyme></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.7" punct="vg:8">T<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="116" place="8" punct="vg">o</seg>m</rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.4" punct="vg:5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5" punct="vg">e</seg>ds</w>, <w n="7.5">c</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.8" punct="pt:8">r<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="8" num="2.4" lm="8" met="8">‒ <w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.3" punct="vg:4">n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="vg">è</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.4" punct="pe:5"><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="pe">o</seg>h</w> ! <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6">c</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="8.8" punct="pe:8">dr<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>