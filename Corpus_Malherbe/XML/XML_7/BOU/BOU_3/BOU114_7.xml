<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU114" modus="cp" lm_max="10" metProfile="7, (4+6)" form="strophe unique avec vers clausule" schema="1(abba) 1(a)">
				<head type="number">XLVII</head>
				<head type="main">LE VIEILLARD LIBRE</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="7" met="7"><space unit="char" quantity="6"></space><w n="1.1" punct="vg:1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="1" punct="vg">ê</seg>t</w>, <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="1.3">l</w>’<w n="1.4" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" punct="vg">au</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="1.6" punct="vg:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>g<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="vg">er</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><space unit="char" quantity="6"></space><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="2.6" punct="pt:7">n<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="7" met="7"><space unit="char" quantity="6"></space><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="3.5">pu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ts</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.7">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="6">oi</seg></w> <w n="3.8" punct="vg:7">b<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><space unit="char" quantity="6"></space><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="5">oi</seg></w> <w n="4.6" punct="ps:7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="ps">er</seg></rhyme></w>…</l>
				</lg>
				<lg n="2" type="vers clausule" rhyme="a">
					<l n="5" num="2.1" lm="10" met="4+6" met_alone="True"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="5.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w>-<w n="5.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="6" mp="Fm">e</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="5.7" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>g<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pe ps">er</seg></rhyme></w> !…</l>
				</lg>
				<closer>
					<signed>Auteur chinois inconnu.</signed>
				</closer>
			</div></body></text></TEI>