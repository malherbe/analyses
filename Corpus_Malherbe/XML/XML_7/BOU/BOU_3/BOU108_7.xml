<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU108" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(abab)">
				<head type="number">XLI</head>
				<head type="main">JASMIN</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3">c<seg phoneme="œ" type="vs" value="1" rule="345" place="2" mp="M">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="1.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="1.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="1.6" punct="vg:6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.9">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.10" punct="vg:12">fl<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>tr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>sm<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="P">a</seg>r</w> <w n="2.4">l</w>’<w n="2.5">h<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="2.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="2.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.9" punct="pt:12">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>r</rhyme></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="3.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="3.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="3.5" punct="vg:6">c<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>dr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="4.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>t</w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="4.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>r</rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Château de La Roche-Guyon</placeName>.
						<date not-after="1859" not-before="1850">185.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>