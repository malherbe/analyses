<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU11" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="3(abba)">
				<head type="main">INTÉRIEUR</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="1.4">f<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="M">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.8" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="2.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="2.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.6">c<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="2.7" punct="pt:12">v<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">s</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="3.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.9" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="4.2">qu</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg>t</w><caesura></caesura> <w n="4.7">f<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="4.9">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.10" punct="pv:12">t<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pv">on</seg></rhyme></w> ;</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">s</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="5.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="5.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="5.10" punct="vg:12">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">pr<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="6.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="6.8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="6.9">d</w>’<w n="6.10" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="11" mp="M">en</seg>nu<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pv">i</seg></rhyme></w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="7.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="7.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="7.6" punct="vg:6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>rts</w>,<caesura></caesura> <w n="7.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="7.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="7.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="7.10" punct="vg:12">lu<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="8.7">l</w>’<w n="8.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>cc<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="9.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="9.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.6">n<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>f</w> <w n="9.7" punct="vg:9"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" punct="vg">an</seg>s</w>, <w n="9.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="9.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.10" punct="pe:12">m<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="pe">oin</seg>s</rhyme></w> !</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="10.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.3" punct="vg:6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">en</seg>d</w>,<caesura></caesura> <w n="10.4" punct="vg:7">v<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg></w>, <w n="10.5" punct="vg:8">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8" punct="vg">en</seg>t</w>, <w n="10.6" punct="vg:9">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>rt</w>, <w n="10.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="10.8" punct="vg:12">tr<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="11.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="11.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6" caesura="1">en</seg>s</w><caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6">gr<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="11.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="11.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>x</w> <w n="11.9" punct="vg:12">d<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">M<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="œ" type="vs" value="1" rule="286" place="5">œ</seg>il</w> <w n="12.5" punct="vg:6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="12.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="12.7">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="8" mp="M">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="12.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="12.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="12.10" punct="pe:12">f<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="pe">oin</seg>s</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>