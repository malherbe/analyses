<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VINGT SONNETS</head><div type="poem" key="CRO79" modus="cm" lm_max="10" metProfile="5+5" form="sonnet classique" schema="abab abab ccd eed">
					<head type="main">Don Juan</head>
					<opener>
						<salute>A Antoine Cros</salute>
					</opener>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="1.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>g</w><caesura></caesura> <w n="1.6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="1.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.8">l</w>’<w n="1.9"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="1.10">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="1.11">r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="2.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5" caesura="1">e</seg>t</w><caesura></caesura> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="2.7">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="2.8">d</w>’<w n="2.9" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.3" punct="vg:5">j<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>sm<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" punct="vg" caesura="1">in</seg>s</w>,<caesura></caesura> <w n="3.4">f<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="3.5">l</w>’<w n="3.6">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.7">h<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">A</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="4.3" punct="vg:5">j<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="4.4" punct="vg:7">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" mp="M">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>r</w>, <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></rhyme></w></l>
					</lg>
					<lg n="2" rhyme="abab">
						<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="5.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" caesura="1">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>m<seg phoneme="y" type="vs" value="1" rule="457" place="7">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="5.8">t<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>m<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2" punct="vg:3">f<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="6.4" punct="vg:5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>r</w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="6.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg>tt<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></rhyme></w></l>
						<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">f<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3" punct="tc:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="ti" caesura="1">i</seg></w> —<caesura></caesura> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s</w> <w n="7.5">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>d</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.7">v<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Où</seg></w>, <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ts</w> <w n="8.4" punct="vg:5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="vg" caesura="1">o</seg>rts</w>,<caesura></caesura> <w n="8.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="8.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></w> <w n="8.7">n</w>’<w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="8.9" punct="pi:10">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>st<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pi">é</seg></rhyme></w> ?</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="2">um</seg></w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="9.3" punct="vg:5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="M">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="9.4">v<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="9.5">cr<seg phoneme="y" type="vs" value="1" rule="454" place="9" mp="M">u</seg><rhyme label="c" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="C">u</seg></w> <w n="10.5">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l</w> <w n="10.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.7">l<seg phoneme="i" type="vs" value="1" rule="493" place="9">y</seg>s</w> <w n="10.8">fr<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="11.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">â</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="11.8" punct="ps:10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg>b<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="ps">eau</seg>x</rhyme></w>…</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="10" met="5+5"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>str<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg></w><caesura></caesura> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="12.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ls</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="12.6">ch<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></w></l>
						<l n="13" num="4.2" lm="10" met="5+5"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="1" mp="Lp">E</seg>s</w>-<w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="13.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="13.4" punct="pi:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="pi" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ?<caesura></caesura> <w n="13.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="189" place="6" punct="vg">E</seg>t</w>, <w n="13.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="13.8" punct="vg:10">l<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="14" num="4.3" lm="10" met="5+5"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="14.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="14.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>rs</w><caesura></caesura> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="14.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="Lp">on</seg>t</w>-<w n="14.7"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ls</w> <w n="14.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="14.9" punct="pi:10">b<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pi">eau</seg>x</rhyme></w> ?</l>
					</lg>
				</div></body></text></TEI>