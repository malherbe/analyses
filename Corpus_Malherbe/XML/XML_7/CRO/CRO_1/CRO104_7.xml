<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO104" modus="cm" lm_max="12" metProfile="6−6" form="suite de distiques" schema="5((aa))">
					<head type="main">Jours d’épreuve</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.3">l<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301" place="5">ai</seg>s</w> <w n="1.4" punct="vg:6">h<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg" caesura="1">au</seg>t</w>,<caesura></caesura> <w n="1.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="1.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.8" punct="dp:12">g<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>tti<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="2" num="1.2" lm="12" mp6="C" met="6−6"><w n="2.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="2.5">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="2.7" punct="vg:12">t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ti<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1" mp="M">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="3.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="3.4" punct="vg:6">m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="vg" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="3.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="3.7">d</w>’<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>ts</w> <w n="3.9" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>b<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ds</w><caesura></caesura> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.6" punct="vg:10">l<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="9">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="4.7">d</w>’<w n="4.8" punct="dp:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="dp">i</seg>ts</rhyme></w> :</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="5.3">t<seg phoneme="ɥi" type="vs" value="1" rule="462" place="3" mp="M">u</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="5.5" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>ts</w>,<caesura></caesura> <w n="5.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7" mp="Lc">e</seg>f</w>-<w n="5.7">d</w>’<w n="5.8"><seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="5.10" punct="vg:12">f<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>m<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">R<seg phoneme="ɛ" type="vs" value="1" rule="339" place="1" mp="M">a</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d</w><caesura></caesura> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="6.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ds</w> <w n="6.9">y<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="6.10">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="6.11" punct="vg:12">tr<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="7.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="7.6">bru<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.8" punct="vg:12">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>l<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>ts</rhyme></w>,</l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3" punct="vg:4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="8.4">j</w>’<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="8.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.7">gr<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="8.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="8.10">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="12">o</seg>ts</rhyme></w></l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">â</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="9.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="9.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="9.7">phth<seg phoneme="i" type="vs" value="1" rule="493" place="8" mp="M">y</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="9.8">j<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg>n<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="10.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="10.4">ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="10.6">d</w>’<w n="10.7" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">â</seg>n<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>