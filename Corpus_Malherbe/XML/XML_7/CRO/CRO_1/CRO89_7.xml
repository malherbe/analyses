<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO89" modus="sm" lm_max="8" metProfile="8" form="suite de distiques" schema="9((aa))">
					<head type="main">Intérieur</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="8" met="8">« <w n="1.1" punct="vg:2">J<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="426" place="2" punct="vg">ou</seg></w>, <w n="1.2" punct="vg:4">p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="1.3" punct="vg:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>, <w n="1.4" punct="pt:8">d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8" punct="pt">o</seg></rhyme></w>. »</l>
						<l n="2" num="1.2" lm="8" met="8">« <w n="2.1" punct="vg:1">D<seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="vg">o</seg></w>, <w n="2.2" punct="vg:2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg></w>, <w n="2.3" punct="vg:3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="2.4" punct="vg:4">f<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="2.5" punct="vg:5">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg">o</seg>l</w>, <w n="2.6" punct="vg:6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>, <w n="2.7" punct="vg:7">s<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></w>, <w n="2.8" punct="pt:8">d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8" punct="pt">o</seg></rhyme></w>. »</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="3.3" punct="vg:4">gu<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.6">s<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</w> <w n="3.7">t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="4.4">cl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6" punct="pt:8">P<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="5.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></rhyme></w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="6.6" punct="pt:8">b<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></rhyme></w>.</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>j<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="8.3">gl<seg phoneme="y" type="vs" value="1" rule="454" place="5">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.5" punct="vg:8">f<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="9" num="1.9" lm="8" met="8"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3" punct="pt:4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="pt">e</seg>s</w>. <w n="9.4">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ls</w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>n<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></w></l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1" punct="vg:1">C<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="10.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="11" num="1.11" lm="8" met="8"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.6" punct="vg:8">tr<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="1.12" lm="8" met="8"><w n="12.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="12.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="12.4" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" punct="vg">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.7" punct="vg:8">s<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="13" num="1.13" lm="8" met="8"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.5">M<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd</rhyme></w></l>
						<l n="14" num="1.14" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="14.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="7">o</seg>p</w> <w n="14.6" punct="pv:8">t<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>rd</rhyme></w> ;</l>
						<l n="15" num="1.15" lm="8" met="8"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="15.4">l</w>’<w n="15.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="15.6">s</w>’<w n="15.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>v<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="16" num="1.16" lm="8" met="8"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="354" place="3">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.4" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="17" num="1.17" lm="8" met="8">« <w n="17.1" punct="vg:1">D<seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="vg">o</seg></w>, <w n="17.2" punct="vg:2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg></w>, <w n="17.3" punct="vg:3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="17.4" punct="vg:4">f<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="17.5" punct="vg:5">s<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></w>, <w n="17.6" punct="vg:6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>, <w n="17.7" punct="vg:7">s<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></w>, <w n="17.8">d<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg></rhyme></w> »</l>
						<l n="18" num="1.18" lm="8" met="8">« <w n="18.1" punct="vg:2">J<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="426" place="2" punct="vg">ou</seg></w>, <w n="18.2" punct="vg:4">p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="18.3" punct="vg:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>, <w n="18.4" punct="pt:8">d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>d<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8" punct="pt">o</seg></rhyme></w>. »</l>
					</lg>
				</div></body></text></TEI>