<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VINGT SONNETS</head><div type="poem" key="CRO80" modus="sm" lm_max="8" metProfile="8" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
					<head type="main">Sonnet</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.6">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="2.2">th<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="2.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rs</w> <w n="2.5" punct="dp:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="dp">e</seg>rs</rhyme></w> :</l>
						<l n="3" num="1.3" lm="8" met="8">— <w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.2" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>s</w>, <w n="3.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ds</w> <w n="3.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="3.5" punct="tc:8">v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="ti">e</seg>rts</rhyme></w> —</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="4.3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.5" punct="pt:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="5.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="5.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="3">o</seg>p</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="5.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd</w> <w n="5.6" punct="vg:8">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>v<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</rhyme></w></l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="7.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.4" punct="pt:8">v<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pt">e</seg>rs</rhyme></w>.</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="8.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">m</w>’<w n="8.6" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="9.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="9.6">d</w>’<w n="9.7"><rhyme label="c" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rt</rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="10.5">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="10.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.8" punct="vg:8">f<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rd</rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="11.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="11.6">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>ts</w> <w n="11.7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w>-<w n="11.8" punct="pt:8">m<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="8" met="8"><w n="12.1" punct="pt:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pt">on</seg></w>. <w n="12.2">S<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="12.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</rhyme></w></l>
						<l n="13" num="4.2" lm="8" met="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="13.3">l</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.5"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>pt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="13.7" punct="vg:8">v<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</rhyme></w>,</l>
						<l n="14" num="4.3" lm="8" met="8"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="14.2">c</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.5">r<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="14.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.8">j</w>’<w n="14.9" punct="pt:8"><rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>