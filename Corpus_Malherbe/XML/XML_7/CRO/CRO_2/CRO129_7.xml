<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VISIONS</head><div type="poem" key="CRO129" modus="sm" lm_max="5" metProfile="5" form="suite périodique" schema="6(abba)">
					<head type="main">Rêve</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="5" met="5"><w n="1.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5" punct="pe:5">l<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="493" place="5" punct="pe">y</seg>s</rhyme></w> !</l>
						<l n="2" num="1.2" lm="5" met="5"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="2.4" punct="vg:5">bl<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="5" met="5"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="3.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">p<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="5" met="5"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="4.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>ts</w> <w n="4.4" punct="pe:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pe">i</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="5" met="5"><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="3">um</seg></w> <w n="5.3">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="5" met="5"><w n="6.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="6.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">mi<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</rhyme></w></l>
						<l n="7" num="2.3" lm="5" met="5"><w n="7.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.3" punct="vg:5">ci<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg">e</seg>l</rhyme></w>,</l>
						<l n="8" num="2.4" lm="5" met="5"><w n="8.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.2">l</w>’<w n="8.3" punct="pt:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>scl<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="5" met="5"><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.2">l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</rhyme></w></l>
						<l n="10" num="3.2" lm="5" met="5"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="10.4">d<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="5" met="5"><w n="11.1" punct="vg:2">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="11.2" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="11.3" punct="pt:5">p<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>.</l>
						<l n="12" num="3.4" lm="5" met="5"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4" punct="pi:5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="pi">an</seg>t</rhyme></w> ?</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="5" met="5"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="13.4" punct="pv:5">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>qu<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="14" num="4.2" lm="5" met="5"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">gl<seg phoneme="a" type="vs" value="1" rule="343" place="2">a</seg>ï<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l</w> <w n="14.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>gl<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</rhyme></w></l>
						<l n="15" num="4.3" lm="5" met="5"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">l<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>s</w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="15.5">bl<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c</rhyme></w></l>
						<l n="16" num="4.4" lm="5" met="5"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.4" punct="pt:5">f<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="5" met="5"><w n="17.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="17.5" punct="vg:5">r<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>s</rhyme></w>,</l>
						<l n="18" num="5.2" lm="5" met="5"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="18.3" punct="vg:5">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="19" num="5.3" lm="5" met="5"><w n="19.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">phr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="19.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</rhyme></w></l>
						<l n="20" num="5.4" lm="5" met="5"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="20.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="20.3" punct="pe:5">fr<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="pe">oi</seg>ds</rhyme></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abba">
						<l n="21" num="6.1" lm="5" met="5"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="21.3">s</w>’<w n="21.4" punct="pv:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ppr<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="22" num="6.2" lm="5" met="5"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="22.2">l<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>s</w> <w n="22.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="22.4" punct="pt:5">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="4">eu</seg>r<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pt">i</seg>r</rhyme></w>.</l>
						<l n="23" num="6.3" lm="5" met="5"><w n="23.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="23.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="23.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</rhyme></w></l>
						<l n="24" num="6.4" lm="5" met="5"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="24.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.3" punct="pt:5">f<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>