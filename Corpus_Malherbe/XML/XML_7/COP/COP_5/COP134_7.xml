<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP134" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
					<head type="main">A MONSIEUR ÉVELART</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:2">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="1.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>g<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5" mp="M">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="1.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>cts</w> <w n="1.6" punct="vg:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rv<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>rs</rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">â</seg>n<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="2.6">f<seg phoneme="œ" type="vs" value="1" rule="304" place="8" mp="M">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="2.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l</w> <w n="2.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="2.9" punct="vg:12">th<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="3.3">gr<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="3.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>ls</w>,<caesura></caesura> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="3.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="3.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>d</w> <w n="3.8" punct="vg:12">m<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="4.2">m</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="4.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="4.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>fl<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="4.6">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>q</w> <w n="4.7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg>ts</w> <w n="4.8" punct="pt:12">v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>rs</rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="5.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="5.4">l<seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg>s</w> <w n="5.5" punct="vg:12">v<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="vg">e</seg>rts</rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M/mc">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="Lc">ou</seg>rd</w>’<w n="6.2">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="6.8">l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="6.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="6.10" punct="pv:12">p<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>s</w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="7.5">j</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="7.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="7.8">j<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="7.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.10" punct="vg:12">qu<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.4" punct="pt:6">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt" caesura="1">i</seg>r</w>.<caesura></caesura> <w n="8.5">C</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="8.10">l</w>’<w n="8.11" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>v<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>rs</rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> « <w n="9.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="451" place="3">u</seg>m</w> » <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>rt</w>,<caesura></caesura> <w n="9.5">r<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M/mp">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M/mp">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="9" mp="Lp">ez</seg></w>-<w n="9.6" punct="vg:10">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>s</w>, <w n="9.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="11">e</seg>r</w> <w n="9.8" punct="vg:12">m<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="10.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="10.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>t</w> <w n="10.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="10.9"><seg phoneme="i" type="vs" value="1" rule="497" place="11" mp="C">y</seg></w> <w n="10.10">m<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">tr<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="11.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="9" mp="C">o</seg>s</w> <w n="11.8">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="11.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>f<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ts</rhyme></w>,</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="12.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="P">a</seg>r</w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="12.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="12.7">s<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>ts</w> <w n="12.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="12.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="12.10" punct="vg:12">r<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rs</w> <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="13.4" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="13.6">l<seg phoneme="e" type="vs" value="1" rule="353" place="8" mp="M">e</seg>ttr<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="13.7"><seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="13.8" punct="vg:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ts</rhyme></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="14.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="14.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="14.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="14.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="14.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="14.8">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="14.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>