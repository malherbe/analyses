<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP78" modus="cm" lm_max="12" metProfile="6−6" form="suite de distiques" schema="5((aa))">
				<head type="main">Dans la rue, le soir</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>f</w> <w n="1.2" punct="pt:3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pt" mp="F">e</seg>s</w>. <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">On</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>d</w><caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg>x</w> <w n="1.8" punct="pt:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">am</seg>b<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</rhyme></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">gr<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="2.3">s</w>’<w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="2.5">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="2.7">c<seg phoneme="o" type="vs" value="1" rule="415" place="8" mp="M">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="2.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="2.9" punct="vg:12">f<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>b<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rgs</rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="3.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.6" punct="pt:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>li<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="4.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.7">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" caesura="1">e</seg>d</w><caesura></caesura> <w n="4.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="4.9"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="4.10" punct="pt:12">f<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>li<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="5.2" punct="vg:4">w<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rpr<seg phoneme="u" type="vs" value="1" rule="78" place="4" punct="vg">oo</seg>f</w>, <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>c</w><caesura></caesura> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="5.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="5.8" punct="vg:12">cu<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg>r</rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1" punct="vg:3">R<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>t</w><caesura></caesura> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="6.7">c<seg phoneme="o" type="vs" value="1" rule="415" place="9" mp="M">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s</w> <w n="6.8">s</w>’<w n="6.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>fu<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>r</rhyme></w></l>
					<l n="7" num="1.7" lm="12" mp6="P" met="6−6"><w n="7.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="7.3">f<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="7.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" mp="P" caesura="1">e</seg>rs</w><caesura></caesura> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="7.8" punct="pt:12">cl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>st<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					<l n="8" num="1.8" lm="12" met="6+6">— <w n="8.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ss<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.3" punct="vg:6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="8.4">p<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>qu<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.6" punct="vg:12">b<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>tt<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="9.3">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>tp<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.6" punct="vg:12">M<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>lm<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>t<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</rhyme></w>,</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1" punct="vg:2">H<seg phoneme="œ" type="vs" value="1" rule="407" place="1" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="10.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="10.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="10.5" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>t</w>,<caesura></caesura> <w n="10.6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="10.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="10.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="10.9" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tt<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="pe">en</seg>d</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>