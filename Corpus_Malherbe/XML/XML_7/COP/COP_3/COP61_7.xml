<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP61" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="6(abab)">
				<head type="main">Aubade parisienne</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="1.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="1.3">t</w>’<w n="1.4" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6" punct="vg:7">ch<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>cs</w> <w n="2.5" punct="vg:7">ru<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>ss<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="vg">eau</seg>x</rhyme></w>,</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">j</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="3.7">l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>g<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">j</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="4.4">p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.6" punct="pt:7"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="pt">eau</seg>x</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="7" met="7"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="5.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="5.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w>-<w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="5.5" punct="pi:5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pi">on</seg>c</w> ? <w n="5.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">I</seg>l</w> <w n="5.7" punct="vg:7">g<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="7" met="7"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="6.6" punct="pt:7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" punct="pt">em</seg>ps</rhyme></w>.</l>
					<l n="7" num="2.3" lm="7" met="7"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="7.2" punct="vg:3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg></w>, <w n="7.3" punct="pi:7">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg></rhyme></w> ?</l>
					<l n="8" num="2.4" lm="7" met="7"><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="8.2">m</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="8.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="8.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>gt</w> <w n="8.7" punct="pt:7"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="pt">an</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="7" met="7"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="9.2">m</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="9.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.6" punct="pt:7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>n<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="10" num="3.2" lm="7" met="7"><w n="10.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.5">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="10.6" punct="vg:7">m<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" punct="vg">o</seg>rt</rhyme></w>,</l>
					<l n="11" num="3.3" lm="7" met="7"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="11.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="11.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="11.7" punct="pv:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="12" num="3.4" lm="7" met="7"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="12.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t</w> <w n="12.5" punct="pe:7">f<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" punct="pe">o</seg>rt</rhyme></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="7" met="7"><w n="13.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w>-<w n="13.4" punct="pi:4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pi">i</seg>l</w> ? <w n="13.5">T<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="13.6" punct="pv:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>j<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg>s</rhyme></w> ;</l>
					<l n="14" num="4.2" lm="7" met="7"><w n="14.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="14.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">fru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rds</w> <w n="14.6" punct="pt:7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>d<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="pt">an</seg>s</rhyme></w>.</l>
					<l n="15" num="4.3" lm="7" met="7"><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="15.3" punct="vg:3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</w>, <w n="15.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="15.5">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="15.6" punct="vg:7">j<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="16" num="4.4" lm="7" met="7"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">j</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="16.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="16.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="16.7" punct="pt:7">d<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7" punct="pt">en</seg>ts</rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="7" met="7"><w n="17.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="17.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="17.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w>-<w n="17.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="17.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.6" punct="pt:7">ch<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="18" num="5.2" lm="7" met="7"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">n</w>’<w n="18.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.4" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="18.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.6">v<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</rhyme></w></l>
					<l n="19" num="5.3" lm="7" met="7"><w n="19.1">S</w>’<w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="19.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="19.5">r<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="20" num="5.4" lm="7" met="7"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="20.5" punct="pt:7">v<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="pt">oi</seg>x</rhyme></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="7" met="7"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">t</w>’<w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="21.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.6">t</w>’<w n="21.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.8" punct="pv:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="22" num="6.2" lm="7" met="7"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="22.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="22.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>ds</w> <w n="22.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>s</w> <w n="22.6">m</w>’<w n="22.7" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="257" place="7" punct="pt">eoi</seg>r</rhyme></w>.</l>
					<l n="23" num="6.3" lm="7" met="7"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="23.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="23.3" punct="pv:5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pv">e</seg></w> ; <w n="23.4">j</w>’<w n="23.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="24" num="6.4" lm="7" met="7"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="24.5" punct="pe:7">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>d<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="pe">oi</seg>r</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>