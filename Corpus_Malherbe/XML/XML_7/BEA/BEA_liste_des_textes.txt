
	▪ Henri BEAUCLAIR [BEA]

 		Liste des recueils de poésies
 		─────────────────────────────
		▫ L’ÉTERNELLE CHANSON, 1884 [BEA_1]
		▫ LES HORIZONTALES, 1885 [BEA_2]
		▫ POÈMES DIVERS, 1887-1914 [BEA_3]
