<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GENS DE MER</head><div type="poem" key="CRB87" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="1(abba) 5(abab)">
					<head type="main">LETTRE DU MEXIQUE</head>
					<opener>
						<dateline>
							<placeName>La Vera-Cruz</placeName> 10 février.
						</dateline>
					</opener>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2">m</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="1.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.6" punct="pt:9">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="pt ti">i</seg>t</w>. — <w n="1.7"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="C">I</seg>l</w> <w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="1.9" punct="pt:12">m<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rt</rhyme></w>.</l>
						<l n="2" num="1.2" lm="12" met="6+6">« <w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.5">c<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>c</w>, <w n="2.7">p<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="11">e</seg>r</w> <w n="2.9" punct="pt:12"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L</w>’<w n="3.2" punct="ps:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="M">i</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="ps">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> … <del reason="analysis" type="syneresis" hand="RR">y</del> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5" punct="pt:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt" caesura="1">u</seg>s</w>.<caesura></caesura> <w n="3.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">I</seg>l</w> <w n="3.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="3.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="11" mp="Lc">eu</seg>t</w>-<w n="3.9"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space quantity="8" unit="char"></space><w n="4.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg>s</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4" punct="pt:5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pt ti">ou</seg>s</w>. — <w n="4.5">C</w>’<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.8" punct="tc:8">s<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="ti">o</seg>rt</rhyme></w> » —</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6">« <w n="5.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="5.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="5.6" punct="tc:6">ç<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="ti" caesura="1">a</seg></w> —<caesura></caesura> <w n="5.7" punct="tc:9">M<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="9" punct="ti">o</seg>t</w> — <w n="5.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="5.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="5.10">h<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="6.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="6.7" punct="tc:9">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="ti" mp="F">e</seg></w> — <w n="6.8">C</w>’<w n="6.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="6.10">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="11">en</seg></w> <w n="6.11" punct="pt:12">s<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pt">u</seg>r</rhyme></w>.</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" punct="pt" caesura="1">en</seg>t</w>.<caesura></caesura> <w n="7.4">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="7.6" punct="dp:9">ç<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="dp">a</seg></w> : <w n="7.7">V<seg phoneme="wa" type="vs" value="1" rule="440" place="10" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="11">ez</seg></w> <w n="7.8">c<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="8" met="8"><space quantity="8" unit="char"></space><w n="8.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="8.5" punct="pt:8">d<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>r</rhyme></w>. »</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6">« <w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rqu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="9.5" punct="vg:6">ç<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="9.6" punct="vg:7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>, <w n="9.7">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <hi rend="ital"><w n="9.8">Fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fm">e</seg></w>-<w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="Lc">a</seg></w>-<w n="9.10" punct="pt:12">c<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="415" place="12">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</hi></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="10.3">d<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="10.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="10.5">p<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="10.6">j<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="9">en</seg>t</w> <w n="10.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="10.8">f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ç<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="11.4" punct="ps:6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="ps" caesura="1">er</seg></w>…<caesura></caesura> <w n="11.5" punct="vg:7">M<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>, <w n="11.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.7">n</w>’<w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="11.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="11.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.11" punct="dp:12">f<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="12" num="3.4" lm="8" met="8"><space quantity="8" unit="char"></space><w n="12.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l</w>-<w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="12.4">n</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.8" punct="pt:8">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></w>. »</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6">« <w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="13.2">f<seg phoneme="i" type="vs" value="1" rule="dc-1" place="2" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="13.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="13.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.6">M<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rs</w> <w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="13.8" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="14.2">c<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="14.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="14.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="14.7" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></w>.</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">z<seg phoneme="u" type="vs" value="1" rule="d-2" place="2" mp="M">ou</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.4">n<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="15.5" punct="tc:7">ç<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="ti">a</seg></w> — <w n="15.6">P<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>si<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10">en</seg></w> <w n="15.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="Lc">an</seg>d</w>-<w n="15.8" punct="tc:12">m<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ti" mp="F">e</seg></rhyme></w> —</l>
						<l n="16" num="4.4" lm="8" met="8"><space quantity="8" unit="char"></space>« <hi rend="ital"><w n="16.1" punct="pt:8">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">J<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="16.3">d</w>’<w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ccl<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ti<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></w></hi>. »</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="12" met="6+6">« <w n="17.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M/mp">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M/mp">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="17.2" punct="pt:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pt">ou</seg>s</w>. <w n="17.3">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="17.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="17.5"><seg phoneme="i" type="vs" value="1" rule="497" place="7" mp="C">y</seg></w> <w n="17.6">cr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="17.8" punct="pt:12">m<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="18" num="5.2" lm="12" met="6+6">… <w n="18.1">J</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="18.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="18.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="18.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="18.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>c</w><caesura></caesura> <w n="18.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="18.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="18.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="18.10" punct="dp:12">c<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="dp">œu</seg>r</rhyme></w> :</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="19.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="19.4" punct="vg:5">f<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="19.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="19.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="19.8" punct="vg:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><space quantity="8" unit="char"></space><w n="20.1" punct="dp:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="dp">E</seg>t</w> : <w n="20.2" punct="tc:3">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rqu<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="ti">é</seg></w> — <hi rend="ital"><w n="20.3" punct="pt:8">C<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="20.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="20.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.6">s<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pt ti">œu</seg>r</rhyme></w></hi>. » —</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="12" met="6+6">« <w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="21.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="21.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <hi rend="ital"><w n="21.5" punct="dp:6">m<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="dp" caesura="1">an</seg></w></hi> :<caesura></caesura> <w n="21.6">qu</w>’<w n="21.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="21.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="21.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="21.10">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="21.11" punct="pt:12">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="22" num="6.2" lm="12" met="6+6"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="22.2" punct="dp:3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="dp" mp="F">e</seg></w> : <w n="22.3">qu</w>’<w n="22.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="22.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="22.6">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="22.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="22.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="22.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="22.10" punct="pt:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>b<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>t</rhyme></w>.</l>
						<l n="23" num="6.3" lm="12" met="6+6"><w n="23.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="23.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="6" caesura="1">à</seg></w><caesura></caesura> <w n="23.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="23.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="23.7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="23.8" punct="dp:12">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rni<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="24" num="6.4" lm="8" met="8"><space quantity="8" unit="char"></space><w n="24.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="24.2" punct="pt:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="pt">o</seg>t</w>. <w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">Un</seg></w> <w n="24.4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="24.5" punct="pt:8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ld<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>t</rhyme></w>. »</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName> 24 mai.
						</dateline>
					</closer>
				</div></body></text></TEI>