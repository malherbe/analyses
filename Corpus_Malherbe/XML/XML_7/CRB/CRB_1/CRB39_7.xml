<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SÉRÉNADE DES SÉRÉNADES</head><div type="poem" key="CRB39" modus="sp" lm_max="7" metProfile="7, 5" form="sonnet non classique" schema="abab cdcd eef ggf">
					<head type="main">TOIT</head>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="1.2" punct="pe:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe">on</seg></w> ! <w n="1.3">J</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="1.5" punct="vg:7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>qu<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="485" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="2.1">Pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4" punct="vg:5">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>t</rhyme></w>,</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="3.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.6" punct="vg:7">tu<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.3" punct="pe:5">T<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pe">oi</seg></rhyme></w> !</l>
					</lg>
					<lg n="2" rhyme="cdcd">
						<l n="5" num="2.1" lm="7" met="7"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="5.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="5.4">l</w>’<w n="5.5" punct="vg:5">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="5.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.7">l<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2" punct="tc:2">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg ti">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, — <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lt<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></rhyme></w></l>
						<l n="7" num="2.3" lm="7" met="7"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <hi rend="ital"><w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.3">C<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="7.4">s<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></hi></l>
						<l n="8" num="2.4" lm="5" met="5"><space quantity="4" unit="char"></space><hi rend="ital"><w n="8.1" punct="pe:5">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">M<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="e" type="vs" value="1" rule="CRB39_1" place="3">e</seg>r<seg phoneme="e" type="vs" value="1" rule="CRB39_2" place="4">e</seg>r<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="CRB39_3" place="5" punct="pe">e</seg></rhyme></w></hi> !</l>
					</lg>
					<lg n="3" rhyme="eef">
						<l n="9" num="3.1" lm="7" met="7"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2" punct="tc:4">cr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="ti">ai</seg></w> — <w n="9.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="9.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.5" punct="pe:7">d<rhyme label="e" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe ti">e</seg></rhyme></w> ! —</l>
						<l n="10" num="3.2" lm="7" met="7"><w n="10.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2">t<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="2">ym</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg></w> <w n="10.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5">p<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="10.6">d</w>’<w n="10.7"><rhyme label="e" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="5" met="5"><space quantity="4" unit="char"></space><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="11.4" punct="pe:5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>b<rhyme label="f" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pe">ou</seg>r</rhyme></w> !</l>
					</lg>
					<lg n="4" rhyme="ggf">
						<l n="12" num="4.1" lm="7" met="7"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.3" punct="vg:4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oî</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="12.5" punct="pe:7">F<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<rhyme label="g" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
						<l n="13" num="4.2" lm="7" met="7"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="13.3" punct="vg:4">p<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="13.4">g<seg phoneme="i" type="vs" value="1" rule="468" place="5">î</seg>t</w> <w n="13.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w>-<w n="13.6" punct="ps:7"><rhyme label="g" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="ps">e</seg></rhyme></w>…</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="14" num="4.3" lm="5" met="5"><space quantity="4" unit="char"></space><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="14.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="14.3">m<seg phoneme="œ" type="vs" value="1" rule="151" place="3">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>r</w> <w n="14.4" punct="pe:5">s<rhyme label="f" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pe">ou</seg>rd</rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>