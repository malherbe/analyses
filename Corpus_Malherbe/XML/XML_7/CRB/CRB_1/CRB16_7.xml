<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB16" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abab abab ccd eed">
					<head type="main">I SONNET</head>
					<head type="sub_1">AVEC LA MANIÈRE DE S’EN SERVIR</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Réglons notre papier et formons bien nos lettres</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="1.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="1.9">pi<seg phoneme="e" type="vs" value="1" rule="241" place="9">e</seg>d</w> <w n="1.10" punct="vg:12"><seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>f<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">Em</seg>b<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oî</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="2.6">qu<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="2.8" punct="pv:12">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pv">on</seg></rhyme></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="3.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rqu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="3.5" punct="vg:6">c<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="3.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.8">qu<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.9">s</w>’<w n="3.10" punct="ps:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>d<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></w>…</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Ç<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="4.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="4.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="4.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ts</w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="4.8" punct="pt:12">pl<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">om</seg>b</rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <hi rend="ital"><w n="5.3">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>lw<seg phoneme="ɛ" type="vs" value="1" rule="323" place="4">ay</seg></w></hi> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="5.5">P<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.8" punct="vg:10">l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="5.10" punct="pv:12">f<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="6.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="6.4" punct="tc:6">t<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="dp ti" caesura="1">a</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> :<caesura></caesura> — <w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="6.7">su<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="6.8" punct="vg:10">qu<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="6.10" punct="pv:12">l<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pv">on</seg>g</rhyme></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="7.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3" punct="vg:4">pi<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></w>, <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="7.5" punct="tc:6">r<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="ti" caesura="1">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> —<caesura></caesura> <w n="7.6" punct="dp:9"><seg phoneme="e" type="vs" value="1" rule="354" place="7" mp="M">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="dp" mp="F">e</seg></w> : <hi rend="ital"><w n="7.7" punct="vg:12">chl<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>f<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w></hi>,</l>
						<l n="8" num="2.4" lm="12" met="6+6">— <w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="8.5" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>l</w>,<caesura></caesura> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.8">r<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="8.10" punct="pt:12">j<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6">— <w n="9.1">T<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>gr<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.2" punct="tc:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="ti" caesura="1">é</seg></w> —<caesura></caesura> <w n="9.3">20<add reason="analysis" type="phonemization" rend="hidden" hand="RR">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>gt</add></w> <w n="9.4" punct="pt:8">m<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pt ti">o</seg>ts</w>. — <w n="9.5">V<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="9.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="9.8" punct="ps:12"><rhyme label="c" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></w>…</l>
						<l n="10" num="3.2" lm="12" met="6+6">(<w n="10.1" punct="tc:2">S<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" punct="ti">e</seg>t</w> — <w n="10.2">c</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="10.5" punct="tc:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="ti" caesura="1">e</seg>t</w> —<caesura></caesura> ) <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="10.7">M<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.8">d</w>’<w n="10.9" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">A</seg>rch<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="11" num="3.3" lm="12" met="6+6">— <w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="11.2">pr<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="11.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="P">a</seg>r</w> <w n="11.8">l</w>’<w n="11.9" punct="dp:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>dd<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>t<seg phoneme="i" type="vs" value="1" rule="dc-1" place="11" mp="M">i</seg><rhyme label="d" id="6" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="dp">on</seg></rhyme></w> :</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="12" met="6+6">— <w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>s<seg phoneme="ə" type="vs" value="1" rule="CRB16_1" place="3">e</seg></w> <w n="12.3">4<add reason="analysis" type="phonemization" rend="hidden" hand="RR">qu<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tr</add></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5">4<add reason="analysis" type="phonemization" rend="hidden" hand="RR">qu<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>tr</add></w><caesura></caesura> = <w n="12.6" punct="pe:7">8<add reason="analysis" type="phonemization" rend="hidden" hand="RR">hu<seg phoneme="i" type="vs" value="1" rule="491" place="7" punct="pe">i</seg>t</add></w> ! <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rs</w> <w n="12.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="12.9" punct="vg:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>c<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="13.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="13.3">3<add reason="analysis" type="phonemization" rend="hidden" hand="RR">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</add></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.5" punct="pe:6">3<add reason="analysis" type="phonemization" rend="hidden" hand="RR">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pe ti" caesura="1">oi</seg>s</add></w> !<caesura></caesura> — <w n="13.6">T<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w> <w n="13.7">P<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="13.8" punct="dp:12">r<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="14" num="4.3" lm="12" met="6+6">« <w n="14.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="14.2" punct="pe:2">l<seg phoneme="i" type="vs" value="1" rule="493" place="2" punct="pe">y</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="415" place="3">Ô</seg></w> <w n="14.4" punct="pe:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pe">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="14.5" punct="ps:6"><seg phoneme="o" type="vs" value="1" rule="415" place="6" punct="ps ti" caesura="1">Ô</seg></w>…<caesura></caesura> » — <w n="14.6" punct="tc:8">S<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="ti">e</seg>t</w> — <w n="14.7" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="d" id="6" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg></rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Pic de la Maladetta</placeName> — Août.
						</dateline>
					</closer>
				</div></body></text></TEI>