<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RONDELS POUR APRÈS</head><div type="poem" key="CRB96" modus="cm" lm_max="10" metProfile="5+5" form="rondel relâché" schema="Abab baA babaA">
					<head type="main">RONDEL</head>
					<lg n="1" rhyme="Abab">
						<l n="1" num="1.1" lm="10" met="5+5"><hi rend="ital"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="1.3" punct="vg:3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="1.4" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="1.5">v<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="1.6">d</w>’<w n="1.7" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>c<rhyme label="A" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg>s</rhyme></w> !</hi></l>
						<l n="2" num="1.2" lm="10" met="5+5"><hi rend="ital"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="2.6" punct="vg:5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="vg" caesura="1">i</seg>ts</w>,<caesura></caesura> <w n="2.7"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">i</seg>l</w> <w n="2.8">n</w>’<w n="2.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="2.10">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="2.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="2.12" punct="pv:10">j<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pv">ou</seg>rs</rhyme></w> ;</hi></l>
						<l n="3" num="1.3" lm="10" met="5+5"><hi rend="ital"><w n="3.1" punct="ps:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="ps">o</seg>rs</w> … <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.6">c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></w></hi></l>
						<l n="4" num="1.4" lm="10" met="5+5"><hi rend="ital"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2" punct="dp:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3" punct="dp">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> : <w n="4.3" punct="pe:5">J<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pe" caesura="1">ai</seg>s</w> !<caesura></caesura> <w n="4.4">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="4.5" punct="dp:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="dp">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> : <w n="4.6" punct="pe:10">T<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>j<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pe">ou</seg>rs</rhyme></w> !</hi></l>
					</lg>
					<lg n="2" rhyme="baA">
						<l n="5" num="2.1" lm="10" met="5+5"><hi rend="ital"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M/mp">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="Lp">en</seg>ds</w>-<w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="5.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>rs</w> <w n="5.4" punct="pi:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pi ps" caesura="1">a</seg>s</w> ?<caesura></caesura>… <w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">I</seg>ls</w> <w n="5.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="5.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="5.9" punct="dp:10">l<rhyme label="b" id="3" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="dp">ou</seg>rds</rhyme></w> :</hi></l>
						<l n="6" num="2.2" lm="10" met="5+5"><hi rend="ital"><w n="6.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>ds</w> <w n="6.4" punct="pe:5">l<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pe ti" caesura="1">er</seg>s</w> !<caesura></caesura> — <w n="6.5">l</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="6.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="6.9" punct="ps:10"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg>s</rhyme></w>…</hi></l>
						<l n="7" num="2.3" lm="10" met="5+5"><hi rend="ital"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="7.3" punct="vg:3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="7.4" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="7.5">v<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="7.6">d</w>’<w n="7.7" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>c<rhyme label="A" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg>s</rhyme></w> !</hi></l>
					</lg>
					<lg n="3" rhyme="babaA">
						<l n="8" num="3.1" lm="10" met="5+5"><hi rend="ital"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M/mp">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="Lp">en</seg>ds</w>-<w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="8.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>rs</w> <w n="8.4" punct="pi:5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="pi ps" caesura="1">oi</seg>x</w> ?<caesura></caesura>… <w n="8.5">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="8.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</w> <w n="8.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="8.8" punct="pt:10">s<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pt">ou</seg>rds</rhyme></w>.</hi></l>
						<l n="9" num="3.2" lm="10" met="5+5"><hi rend="ital"><w n="9.1" punct="dp:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="dp">o</seg>rs</w> : <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">I</seg>l</w> <w n="9.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.4" punct="vg:5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="9.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="9.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>x</w> <w n="9.7">d</w>’<w n="9.8" punct="dp:10"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg>s</rhyme></w> :</hi></l>
						<l n="10" num="3.3" lm="10" met="5+5"><hi rend="ital"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" mp="M">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="10.4" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="10.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="10.8" punct="vg:10"><rhyme label="b" id="3" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>rs</rhyme></w>,</hi></l>
						<l n="11" num="3.4" lm="10" met="5+5"><hi rend="ital"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="C">eu</seg>r</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="11.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="11.6" punct="ps:10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg>s<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg>s</rhyme></w>…</hi></l>
						<l n="12" num="3.5" lm="10" met="5+5"><hi rend="ital"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="12.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="12.3" punct="vg:3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="12.4" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="12.5">v<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="12.6">d</w>’<w n="12.7" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>c<rhyme label="A" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg>s</rhyme></w> !</hi></l>
					</lg>
				</div></body></text></TEI>