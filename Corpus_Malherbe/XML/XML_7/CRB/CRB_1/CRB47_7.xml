<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SÉRÉNADE DES SÉRÉNADES</head><div type="poem" key="CRB47" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(abab)">
					<head type="main">PORTES ET FENÊTRES</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">N</w>’<w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="1.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.4" punct="pi:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pi ti">a</seg>s</w> ? — <w n="1.5">S<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g</w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="1.7" punct="pe:8">gu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ti">e</seg></rhyme></w> ! —</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="pe:2">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe ps">on</seg>ds</w> !… <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">d<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>mn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="2.5" punct="pt:8">f<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</rhyme></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">m</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.5" punct="vg:6">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="3.6" punct="vg:8">B<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rb<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="4.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="4.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="4.5" punct="pe:8">m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pe">o</seg>rt</rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5" punct="pe:8">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>rg<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></rhyme></w> !…</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="6.4">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="6.5">n</w>’<w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ds</w> <w n="6.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.8" punct="vg:8">p<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2"><seg phoneme="œ" type="vs" value="1" rule="286" place="2">œ</seg>il</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="7.4" punct="vg:4">cl<seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="vg">o</seg>s</w>, <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="7.8" punct="dp:8">n<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2" punct="tc:3">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="ti">e</seg></w> — <w n="8.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="8.6" punct="pt:8">p<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="9.3">j</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="9.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7" punct="pt:8">r<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">d<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="10.6" punct="ps:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="ps">oi</seg></rhyme></w>…</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="11.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.3" punct="pe:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">In</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="12.3">n</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="3">e</seg>s</w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="12.6" punct="ps:5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5" punct="ps">à</seg></w> … <w n="12.7">pr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7">en</seg>s</w>-<w n="12.8" punct="pe:8">m<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></rhyme></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="13.2">d<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="13.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">n</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="13.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="13.7">d</w>’<w n="13.8" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lc<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="14.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="14.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.7">d<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="14.8" punct="vg:8">m<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></rhyme></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="15.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="15.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="15.4" punct="ps:8">s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</rhyme></w>…</l>
						<l n="16" num="4.4" lm="8" met="8">— <w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="16.5">d<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="16.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.7" punct="pe:8">T<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>