<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO37" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(abab)">
					<head type="main">SUR L’ALBUM DE Mme DR M. F…</head>
					<head type="sub_1">(IMPROMPTU)</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="3" mp="M">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="1.4" punct="vg:8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">em</seg>ps</w>, <w n="1.5" punct="vg:10">M<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="2.2">c<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg></w><caesura></caesura> <w n="2.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="2.8">p<seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></rhyme></w></l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="3.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pe" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !<caesura></caesura> <w n="3.4" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">A</seg>h</w> ! <w n="3.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="3.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="4.2">f<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" caesura="1">in</seg></w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.5">l</w>’<w n="4.6">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="6">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="4.7" punct="pe:10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pe">é</seg></rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1880">A la kermesse des pauvres, à Québec, 1880.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>