<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HYMNES, ROMANCES ET CHANSONNETTES</head><div type="poem" key="CAO66" modus="sm" lm_max="5" metProfile="5" form="suite périodique" schema="4(ababcdcd)">
					<head type="main">ROSE, ÉCOUTE-MOI</head>
					<head type="tune">Musique de M. N. Crépault</head>
					<div type="section" n="1">
						<head type="main">I</head>
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<l n="1" num="1.1" lm="5" met="5"><w n="1.1" punct="vg:2">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="vg">oi</seg></w>, <w n="1.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3" punct="vg:5">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gn<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="5" met="5"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w>-<w n="2.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="2.4">p<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</rhyme></w></l>
							<l n="3" num="1.3" lm="5" met="5"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
							<l n="4" num="1.4" lm="5" met="5"><w n="4.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4" punct="pi:5">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pi">a</seg>s</rhyme></w> ?</l>
							<l n="5" num="1.5" lm="5" met="5"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.4" punct="vg:5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
							<l n="6" num="1.6" lm="5" met="5"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="6.3">f<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="6.5" punct="pv:5">t<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pv">oi</seg></rhyme></w> ;</l>
							<l n="7" num="1.7" lm="5" met="5"><w n="7.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="7.2">j<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="7.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="7.4">j<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w> <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="8" num="1.8" lm="5" met="5"><w n="8.1" punct="vg:1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="vg">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="8.3" punct="pe:5">m<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pe">oi</seg></rhyme></w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">II</head>
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<l n="9" num="1.1" lm="5" met="5"><w n="9.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
							<l n="10" num="1.2" lm="5" met="5"><w n="10.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4" punct="vg:5">fl<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg">eu</seg>rs</rhyme></w>,</l>
							<l n="11" num="1.3" lm="5" met="5"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">cr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
							<l n="12" num="1.4" lm="5" met="5"><w n="12.1">D<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="12.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.4" punct="pt:5">pl<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="pt">eu</seg>rs</rhyme></w>.</l>
							<l n="13" num="1.5" lm="5" met="5"><w n="13.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="13.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4" punct="vg:5">ch<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
							<l n="14" num="1.6" lm="5" met="5"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="14.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="14.5" punct="pe:5">t<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pe">oi</seg></rhyme></w> !</l>
							<l n="15" num="1.7" lm="5" met="5"><w n="15.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="15.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="15.4" punct="dp:5">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ch<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="dp">e</seg></rhyme></w> : <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="16" num="1.8" lm="5" met="5"><w n="16.1" punct="vg:1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="vg">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="16.3" punct="pe:5">m<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pe">oi</seg></rhyme></w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del>.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">III</head>
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<l n="17" num="1.1" lm="5" met="5"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="17.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="17.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="17.4" punct="vg:5">R<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
							<l n="18" num="1.2" lm="5" met="5"><w n="18.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="18.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="18.5" punct="pv:5">f<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="pv">eu</seg></rhyme></w> ;</l>
							<l n="19" num="1.3" lm="5" met="5"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">t</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="19.5">j</w>’<w n="19.6"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
							<l n="20" num="1.4" lm="5" met="5"><w n="20.1">T</w>’<w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="20.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.4">l</w>’<w n="20.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></rhyme></w></l>
							<l n="21" num="1.5" lm="5" met="5"><w n="21.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1" punct="pe">oi</seg></w> ! <w n="21.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.3">t</w>’<w n="21.4" punct="pi:5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ff<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pi">e</seg></rhyme></w> ?</l>
							<l n="22" num="1.6" lm="5" met="5"><w n="22.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="22.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.5" punct="pi:5">f<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pi">oi</seg></rhyme></w> ?</l>
							<l n="23" num="1.7" lm="5" met="5"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="23.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="2">o</seg>p</w> <w n="23.4">d</w>’<w n="23.5" punct="dp:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="dp">e</seg></rhyme></w> : <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="24" num="1.8" lm="5" met="5"><w n="24.1" punct="vg:1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="vg">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="24.3" punct="pe:5">m<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pe">oi</seg></rhyme></w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del>.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="main">IV</head>
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<l n="25" num="1.1" lm="5" met="5"><w n="25.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="25.2" punct="vg:2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="25.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="25.4" punct="vg:5">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>qu<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
							<l n="26" num="1.2" lm="5" met="5"><w n="26.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="26.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</rhyme></w></l>
							<l n="27" num="1.3" lm="5" met="5"><w n="27.1">L</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="27.3">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
							<l n="28" num="1.4" lm="5" met="5"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="28.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="28.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</w> <w n="28.4" punct="pv:5">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pv">a</seg>s</rhyme></w> ;</l>
							<l n="29" num="1.5" lm="5" met="5"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="29.2">d</w>’<w n="29.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="29.5">r<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
							<l n="30" num="1.6" lm="5" met="5"><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="30.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="30.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.4" punct="vg:5">r<rhyme label="d" id="16" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></rhyme></w>,</l>
							<l n="31" num="1.7" lm="5" met="5"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="31.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="31.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="31.4" punct="dp:5">p<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="dp">e</seg></rhyme></w> : <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="32" num="1.8" lm="5" met="5"><w n="32.1" punct="vg:1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="vg">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="32.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="32.3" punct="pe:5">t<rhyme label="d" id="16" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pe">oi</seg></rhyme></w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del>.</l>
						</lg>
					</div>
					<closer>
						<dateline>
							<date when="1882">12 février 1882</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>