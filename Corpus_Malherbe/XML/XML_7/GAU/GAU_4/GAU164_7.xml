<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU164" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba cdd cee">
				<head type="main">SONNET VII</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Liberté de juillet ! femme au buste divin, <lb></lb>
								Et dont le corps finit en queue !
							</quote>
							<bibl>
								<name>G. de Nerval</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								E la lor cieca vita è tanto bassa <lb></lb>
								ch’invidiosi son d’ogn’altra sorte.
							</quote>
							<bibl>
								<hi rend="ital">Inferno, canto</hi> III.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.3">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>f<seg phoneme="a" type="vs" value="1" rule="341" place="6" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="1.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>ps</w> <w n="1.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.9">l</w>’<w n="1.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="1.11" punct="pv:12">r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="2.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="2.5">d<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.7">d<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>gt</w> <w n="2.8">f<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l</w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="2.10">m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s</rhyme></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg>x</w> <w n="3.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="3.4">d</w>’<w n="3.5" punct="dp:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="dp" caesura="1">e</seg>r</w> :<caesura></caesura> <w n="3.6">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="3.7">d</w>’<w n="3.8" punct="pe:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pe ti">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! — <w n="3.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">A</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="170" place="1" mp="M">E</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</w>, <w n="4.2" punct="vg:5">p<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg" mp="F">e</seg>s</w>, <w n="4.3" punct="vg:6">r<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="4.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="4.6">j<seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="C">ou</seg>s</w> <w n="4.9" punct="pt:12">tr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="5.2">b<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>dg<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="5.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r</w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="5.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="5.9" punct="pt:12">tr<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="6.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>rs</w> <w n="6.3">tr<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.4">d</w>’<w n="6.5">h<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="6.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l</w> <w n="6.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="7.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>rs</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="7.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ch<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>ls</w> <w n="7.6">g<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="7.7" punct="vg:10">t<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>t</w>, <w n="7.8">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rm<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s</rhyme></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="8.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="8.3">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="8.5">s</w>’<w n="8.6" punct="vg:6"><seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.9">r<seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="8.10" punct="pt:12">p<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="cdd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="9.3" punct="vg:6">ju<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg" caesura="1">e</seg>t</w>,<caesura></caesura> <w n="9.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="9.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>g<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12" punct="vg">o</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="10.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>ls</w> <w n="10.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="10.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="10.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.9">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>m<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="11.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>x</w><caesura></caesura> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="d-2" place="9" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="476" place="10">ï</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="11.8" punct="pe:12">m<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="4" rhyme="cee">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:2">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="12.3">p<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="12.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="12.6">H<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>g<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg></rhyme></w></l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="13.5" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ç<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="13.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="13.9">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="14.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="14.7" punct="pt:12">r<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><rhyme label="e" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>