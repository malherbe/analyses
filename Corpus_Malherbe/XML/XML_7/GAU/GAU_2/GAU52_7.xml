<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU52" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="3(abab)">
				<head type="main">PANTOUM</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="1.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.5">n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">aim</seg>s</w> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.6" punct="pv:8">m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pv">e</seg>r</rhyme></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>x</w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="3.3" punct="vg:5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>cs</w>, <w n="3.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="3.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<rhyme label="a" id="1" gender="f" type="e" part="I"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></rhyme></w>-<w n="3.6"><rhyme label="a" id="1" gender="f" type="e" part="F">j<seg phoneme="ə" type="ef" value="1" rule="e-1" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="4.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">l</w>’<w n="4.7" punct="pi:8"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pi">ai</seg>r</rhyme></w> ?</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="5.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="5.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.6" punct="vg:8">b<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">b<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="6.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6" punct="vg:8">j<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">S</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ls</w> <w n="7.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="7.7" punct="vg:8"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1" punct="vg:2">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w>-<w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="8.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="8.5">j</w>’<w n="8.6" punct="pi:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pi">ai</seg>s</rhyme></w> ?</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="9.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="9.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="9.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="9.7">r<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="10.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="10.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="10.5" punct="vg:8">f<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8" punct="vg">ê</seg>ts</rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="11.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="11.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w>-<w n="11.7" punct="vg:8">cl<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="12.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="vg">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.6">j</w>’<w n="12.7"><seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg></w> <w n="12.8" punct="pt:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>