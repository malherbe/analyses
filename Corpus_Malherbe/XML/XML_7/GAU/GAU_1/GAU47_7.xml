<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU47" modus="sp" lm_max="6" metProfile="6, 2" form="suite périodique" schema="14(abab)">
				<head type="main">L’ART</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="1.2">l</w>’<w n="1.3"><seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="1.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="1.6">b<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="2.5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>il</rhyme></w></l>
					<l n="3" num="1.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="3.1" punct="vg:2">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>b<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1" punct="vg:1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" punct="vg">e</seg>rs</w>, <w n="4.2" punct="vg:2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.3" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>n<seg phoneme="i" type="vs" value="1" rule="493" place="4" punct="vg">y</seg>x</w>, <w n="4.4" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="6" punct="pt">a</seg>il</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="6" met="6"><w n="5.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>t</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="5.4" punct="pe:6">f<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg>s</rhyme></w> !</l>
					<l n="6" num="2.2" lm="6" met="6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="6.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="6.5">dr<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</rhyme></w></l>
					<l n="7" num="2.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="7.2" punct="vg:2">ch<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="8" num="2.4" lm="6" met="6"><w n="8.1" punct="vg:1">M<seg phoneme="y" type="vs" value="1" rule="450" place="1" punct="vg">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>th<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>tr<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pt">oi</seg>t</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="6" met="6"><w n="9.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="9.3">rh<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4" punct="vg:6">c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>mm<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="6" met="6"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="10.5" punct="vg:6">gr<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>d</rhyme></w>,</l>
					<l n="11" num="3.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="11.2">m<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="6" met="6"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="12.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>d</w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.6" punct="pe:6">pr<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pe">en</seg>d</rhyme></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="6" met="6"><w n="13.1" punct="vg:4">St<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="13.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>p<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="14" num="4.2" lm="6" met="6"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">p<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>tr<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</rhyme></w></l>
					<l n="15" num="4.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">p<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="16" num="4.4" lm="6" met="6"><w n="16.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="16.2">fl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="16.4">l</w>’<w n="16.5" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>spr<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>t</rhyme></w>,</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="6" met="6"><w n="17.1">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4" punct="vg:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="18" num="5.2" lm="6" met="6"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="18.4">d<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</rhyme></w></l>
					<l n="19" num="5.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2" punct="vg:2">r<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></rhyme></w>,</l>
					<l n="20" num="5.4" lm="6" met="6"><w n="20.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rdi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg>s</w> <w n="20.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="20.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="20.4" punct="pv:6">p<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pv">u</seg>r</rhyme></w> ;</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="6" met="6"><w n="21.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>pr<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="21.3">S<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="22" num="6.2" lm="6" met="6"><w n="22.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="22.2">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="22.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</rhyme></w></l>
					<l n="23" num="6.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="23.1">S</w>’<w n="23.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>cc<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="24" num="6.4" lm="6" met="6"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="24.3">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7" place="3">e</seg>r</w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="24.5" punct="pv:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pv">an</seg>t</rhyme></w> ;</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="6" met="6"><w n="25.1">D</w>’<w n="25.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="25.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="26" num="7.2" lm="6" met="6"><w n="26.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="26.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="26.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="26.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></rhyme></w></l>
					<l n="27" num="7.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="27.1">D</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="28" num="7.4" lm="6" met="6"><w n="28.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="28.3">d</w>’<w n="28.4" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>ll<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pt">on</seg></rhyme></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="6" met="6"><w n="29.1" punct="vg:2">P<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="29.2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="29.3">l</w>’<w n="29.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>q<seg phoneme="wa" type="vs" value="1" rule="186" place="5">ua</seg>r<rhyme label="a" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="30" num="8.2" lm="6" met="6"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="30.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="30.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</rhyme></w></l>
					<l n="31" num="8.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="31.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="31.2">fr<rhyme label="a" id="15" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="32" num="8.4" lm="6" met="6"><w n="32.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="32.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="32.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="32.4">l</w>’<w n="32.5" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<rhyme label="b" id="16" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt">eu</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="6" met="6"><w n="33.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="33.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="33.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="33.4" punct="vg:6">bl<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="34" num="9.2" lm="6" met="6"><w n="34.1">T<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="34.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="34.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="34.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ç<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</rhyme></w></l>
					<l n="35" num="9.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="35.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="35.2" punct="vg:2">qu<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="36" num="9.4" lm="6" met="6"><w n="36.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="36.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="36.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="36.4" punct="vg:6">bl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>s</rhyme></w>,</l>
				</lg>
				<lg n="10" type="quatrain" rhyme="abab">
					<l n="37" num="10.1" lm="6" met="6"><w n="37.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="37.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="37.3">n<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="37.4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<rhyme label="a" id="19" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="38" num="10.2" lm="6" met="6"><w n="38.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="38.2">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="38.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="38.5" punct="vg:6">J<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<rhyme label="b" id="20" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</rhyme></w>,</l>
					<l n="39" num="10.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="39.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="39.2">gl<rhyme label="a" id="19" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="40" num="10.4" lm="6" met="6"><w n="40.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="40.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="40.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>x</w> <w n="40.4" punct="pt:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>ss<rhyme label="b" id="20" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="11" type="quatrain" rhyme="abab">
					<l n="41" num="11.1" lm="6" met="6"><w n="41.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="41.2" punct="pt:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pt ti">e</seg></w>.— <w n="41.3">L</w>’<w n="41.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rt</w> <w n="41.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>b<rhyme label="a" id="21" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="42" num="11.2" lm="6" met="6"><w n="42.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l</w> <w n="42.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="42.3">l</w>’<w n="42.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<rhyme label="b" id="22" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></rhyme></w>,</l>
					<l n="43" num="11.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="43.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="43.2">b<rhyme label="a" id="21" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="44" num="11.4" lm="6" met="6"><w n="44.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="44.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="44.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="44.4" punct="pt:6">c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<rhyme label="b" id="22" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<lg n="12" type="quatrain" rhyme="abab">
					<l n="45" num="12.1" lm="6" met="6"><w n="45.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="45.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="45.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>d<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="45.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>st<rhyme label="a" id="23" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="46" num="12.2" lm="6" met="6"><w n="46.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="46.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="46.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="46.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<rhyme label="b" id="24" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</rhyme></w></l>
					<l n="47" num="12.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="47.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="47.2">t<rhyme label="a" id="23" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="48" num="12.4" lm="6" met="6"><w n="48.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="48.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="48.3" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<rhyme label="b" id="24" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt">eu</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="13" type="quatrain" rhyme="abab">
					<l n="49" num="13.1" lm="6" met="6"><w n="49.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="49.2">di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="49.3"><seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w>-<w n="49.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="49.5" punct="vg:6">m<rhyme label="a" id="25" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>nt</rhyme></w>,</l>
					<l n="50" num="13.2" lm="6" met="6"><w n="50.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="50.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="50.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="50.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<rhyme label="b" id="26" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>s</rhyme></w></l>
					<l n="51" num="13.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="51.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<rhyme label="a" id="25" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg>nt</rhyme></w></l>
					<l n="52" num="13.4" lm="6" met="6"><w n="52.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="52.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rts</w> <w n="52.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="52.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="52.5" punct="pt:6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<rhyme label="b" id="26" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="pt">ain</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="14" type="quatrain" rhyme="abab">
					<l n="53" num="14.1" lm="6" met="6"><w n="53.1" punct="vg:2">Sc<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>lpt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="53.2" punct="vg:4">l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="53.3" punct="pv:6">c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<rhyme label="a" id="27" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="54" num="14.2" lm="6" met="6"><w n="54.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="54.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="54.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="54.4">fl<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>tt<rhyme label="b" id="28" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</rhyme></w></l>
					<l n="55" num="14.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="55.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="55.2">sc<rhyme label="a" id="27" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></w></l>
					<l n="56" num="14.4" lm="6" met="6"><w n="56.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="56.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="56.3">bl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>c</w> <w n="56.4" punct="pe:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<rhyme label="b" id="28" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pe">an</seg>t</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>