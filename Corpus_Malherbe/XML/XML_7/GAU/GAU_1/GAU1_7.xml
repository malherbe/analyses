<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU1" modus="sm" lm_max="8" metProfile="8" form="sonnet classique" schema="abba abba cdd cee">
				<head type="main">PRÉFACE</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="1.3">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">l</w>’<w n="1.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>p<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:1">G<seg phoneme="e" type="vs" value="1" rule="250" place="1" punct="vg">œ</seg>th<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="2.3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="2.5">c<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="2.6" punct="vg:8">br<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <hi rend="ital"><w n="3.2" punct="vg:8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">D<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg></w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>cc<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</rhyme></w></hi>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt</w> <w n="4.6" punct="pt:8">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="5.2">N<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="5.4" punct="vg:8">Sh<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ksp<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="GAU1_1" place="8">ea</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5" punct="vg:8">ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">N<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="8.4">qu</w>’<w n="8.5">H<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>dh<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>d</w> <w n="8.6" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="cdd">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">G<seg phoneme="e" type="vs" value="1" rule="250" place="3">œ</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg></rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="10.2">W<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="10.3">s</w>’<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.6">ch<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d</w>’<w n="11.3">H<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>z</w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.6" punct="vg:8">r<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
				</lg>
				<lg n="4" rhyme="cee">
					<l n="12" num="4.1" lm="8" met="8"><w n="12.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg></rhyme></w></l>
					<l n="13" num="4.2" lm="8" met="8"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="13.2">fou<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="13.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="13.5" punct="vg:8">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="14" num="4.3" lm="8" met="8"><w n="14.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="14.2">j</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="14.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <hi rend="ital"><w n="14.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="4">É</seg>m<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="14.7">C<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w></hi>.</l>
				</lg>
			</div></body></text></TEI>