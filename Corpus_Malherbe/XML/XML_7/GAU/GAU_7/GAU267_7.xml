<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU267" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="3(aabccb)">
					<head type="main">J’ÉTAIS MONTÉ PLUS HAUT…</head>
					<lg n="1" type="sizain" rhyme="aabccb">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="1.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="1.5">h<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>t</w><caesura></caesura> <w n="1.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="1.11" punct="pv:12">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="2.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>ds</w> <w n="2.4">s</w>’<w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="2.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.8" punct="vg:12">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="10" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="11" mp="M">y</seg>s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rcl<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="3.2">d</w>’<w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="3.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="3.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="3.11" punct="pv:12">m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pv">e</seg>r</rhyme></w> ;</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="4.3">cr<seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="4.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="4.7">g<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="5.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">j<seg phoneme="a" type="vs" value="1" rule="307" place="4" mp="M">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>rs</w> <w n="5.6" punct="vg:12">b<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>cs</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="345" place="6" caesura="1">ue</seg>ils</w><caesura></caesura> <w n="6.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="6.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="6.7">g<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="2" type="sizain" rhyme="aabccb">
						<l n="7" num="2.1" lm="12" met="6+6"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="7.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>ts</w> <w n="7.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>n<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="8" num="2.2" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="8.2">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="8.3">gr<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="8.6">p<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="8.7" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ff<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="9" num="2.3" lm="12" met="6+6"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="9.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>cs</w> <w n="9.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="4">œ</seg>il</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="9.7">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6" caesura="1">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.10" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="10" num="2.4" lm="12" met="6+6"><w n="10.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.4">f<seg phoneme="œ" type="vs" value="1" rule="304" place="5" mp="M">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="10.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="10.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>r</w> <w n="10.9" punct="pv:12">cr<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="11" num="2.5" lm="12" met="6+6"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="11.2"><seg phoneme="y" type="vs" value="1" rule="251" place="2">eû</seg>t</w> <w n="11.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="11.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="11.6">pr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="11.8">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.10" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>p<rhyme label="c" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="2.6" lm="12" met="6+6"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="12.2">ch<seg phoneme="a" type="vs" value="1" rule="343" place="2" mp="M">a</seg><seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.5">m<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>t</w> <w n="12.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="12.7" punct="pt:12">Cr<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" type="sizain" rhyme="aabccb">
						<l n="13" num="3.1" lm="12" met="6+6"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="13.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="13.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="13.7" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="14" num="3.2" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="14.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="14.4">n<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="14.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="14.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="14.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="14.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ccr<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="454" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="15" num="3.3" lm="12" met="6+6"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">B<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>h<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>t</w> <w n="15.3">b<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="15.6" punct="pt:12">L<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>th<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg></rhyme></w>.</l>
						<l n="16" num="3.4" lm="12" met="6+6"><w n="16.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="16.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="16.5" punct="vg:6">ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" punct="vg" caesura="1">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="16.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="16.7" punct="vg:12">c<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>ti<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="17" num="3.5" lm="12" met="6+6"><w n="17.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="17.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rps</w> <w n="17.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>str<seg phoneme="y" type="vs" value="1" rule="454" place="5" mp="M">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="17.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="17.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="17.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="17.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="17.9" punct="vg:12">pi<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="18" num="3.6" lm="12" met="6+6"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="18.3">bl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>cs</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="18.5">gr<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="18.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="18.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="18.8"><seg phoneme="o" type="vs" value="1" rule="432" place="9">o</seg>s</w> <w n="18.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="18.10" punct="pe:12">T<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pe">an</seg></rhyme></w> !</l>
					</lg>
					<closer>
						<placeName>Sierra-Nevada</placeName>.
					</closer>
				</div></body></text></TEI>