<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU255" modus="sm" lm_max="6" metProfile="6" form="suite périodique" schema="10(abab)">
					<head type="main">LA PETITE FLEUR ROSE</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="6" met="6"><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.5" punct="vg:6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="6" met="6"><w n="2.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3" punct="vg:6">Gu<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="6" met="6"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="3.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">E</seg>sp<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="6" met="6"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.3" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt">a</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="6" met="6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="5.2">l</w>’<w n="5.3">h<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="5.5" punct="vg:6">b<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="6" met="6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">gr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">E</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5">i</seg><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</rhyme></w></l>
						<l n="7" num="2.3" lm="6" met="6"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.3">d<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4" punct="vg:6">m<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="6" met="6"><w n="8.1">N<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="3">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="8.5" punct="pv:6">r<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pv">a</seg>l</rhyme></w> ;</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="6" met="6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="6" met="6"><w n="10.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="10.2">br<seg phoneme="u" type="vs" value="1" rule="427" place="2">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="10.3" punct="vg:6">c<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="11" num="3.3" lm="6" met="6"><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="11.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="4">œ</seg>il</w> <w n="11.6">s</w>’<w n="11.7"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="11.8" punct="vg:6">tr<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="6" met="6"><w n="12.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>dr<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>d</w>, <w n="12.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="12.3" punct="pe:6">l<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pe">eu</seg>x</rhyme></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="6" met="6"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="13.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="13.5" punct="vg:6">h<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="4.2" lm="6" met="6"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>cs</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5">gr<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</rhyme></w></l>
						<l n="15" num="4.3" lm="6" met="6"><w n="15.1">N</w>’<w n="15.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="15.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.4">l</w>’<w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="15.7" punct="vg:6">h<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="16" num="4.4" lm="6" met="6"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="16.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="16.5" punct="pv:6">n<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pv">i</seg>d</rhyme></w> ;</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="6" met="6"><w n="17.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="17.2">l</w>’<w n="17.3">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ssi<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="18" num="5.2" lm="6" met="6"><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>cs</w> <w n="18.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>ts</rhyme></w>,</l>
						<l n="19" num="5.3" lm="6" met="6"><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.4" punct="vg:6">n<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="20" num="5.4" lm="6" met="6"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="20.3">vi<seg phoneme="e" type="vs" value="1" rule="383" place="4">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rds</w> <w n="20.4" punct="pt:6">bl<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pt">an</seg>cs</rhyme></w>.</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="6" met="6"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="21.4">cr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.5" punct="vg:6">p<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="22" num="6.2" lm="6" met="6"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="22.3">ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="22.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</rhyme></w></l>
						<l n="23" num="6.3" lm="6" met="6"><w n="23.1">D</w>’<w n="23.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.4">gu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>p<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="24" num="6.4" lm="6" met="6"><w n="24.1">B<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="24.3" punct="pv:6">h<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>z<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pv">on</seg>s</rhyme></w> ;</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="6" met="6"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="25.2">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="25.3" punct="vg:6">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>bl<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="26" num="7.2" lm="6" met="6"><w n="26.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="26.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.3">d</w>’<w n="26.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="26.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rb<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg></rhyme></w></l>
						<l n="27" num="7.3" lm="6" met="6"><w n="27.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="27.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="27.3">c<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
						<l n="28" num="7.4" lm="6" met="6"><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">plu<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="28.4">d</w>’<w n="28.5" punct="pv:6"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pv">an</seg></rhyme></w> ;</l>
					</lg>
					<lg n="8" type="quatrain" rhyme="abab">
						<l n="29" num="8.1" lm="6" met="6"><w n="29.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2" punct="vg:2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg></w>, <w n="29.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="29.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="29.5" punct="vg:6">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<rhyme label="a" id="15" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="30" num="8.2" lm="6" met="6"><w n="30.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="30.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="30.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="30.4" punct="vg:6">m<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="vg">ain</seg>s</rhyme></w>,</l>
						<l n="31" num="8.3" lm="6" met="6"><w n="31.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="31.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="31.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<rhyme label="a" id="15" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
						<l n="32" num="8.4" lm="6" met="6"><w n="32.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="32.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c</w> <w n="32.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="32.5" punct="vg:6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<rhyme label="b" id="16" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg>s</rhyme></w>,</l>
					</lg>
					<lg n="9" type="quatrain" rhyme="abab">
						<l n="33" num="9.1" lm="6" met="6"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="33.2">l</w>’<w n="33.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="33.4">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="34" num="9.2" lm="6" met="6"><w n="34.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="34.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="34.3">l</w>’<w n="34.4">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="34.5" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</rhyme></w>,</l>
						<l n="35" num="9.3" lm="6" met="6"><w n="35.1">D</w>’<w n="35.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="35.3">c<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="35.4" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="36" num="9.4" lm="6" met="6"><w n="36.1">Ch<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ch<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="36.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="4">om</seg></w> <w n="36.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="36.5" punct="pe:6">gr<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pe">an</seg>d</rhyme></w> !</l>
					</lg>
					<lg n="10" type="quatrain" rhyme="abab">
						<l n="37" num="10.1" lm="6" met="6"><w n="37.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="37.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="37.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="37.4" punct="vg:6">ch<rhyme label="a" id="19" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="38" num="10.2" lm="6" met="6"><w n="38.1">J</w>’<w n="38.2" punct="vg:1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" punct="vg">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="38.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="38.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="38.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="38.6" punct="vg:6">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ch<rhyme label="b" id="20" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></rhyme></w>,</l>
						<l n="39" num="10.3" lm="6" met="6"><w n="39.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="39.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="39.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="39.4" punct="vg:6">r<rhyme label="a" id="19" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
						<l n="40" num="10.4" lm="6" met="6"><w n="40.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="40.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="40.3">qu</w>’<w n="40.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="40.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="40.6" punct="pe:6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rch<rhyme label="b" id="20" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe">er</seg></rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Guadarrama</placeName>,
							<date not-before="1839" not-after="1849">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>