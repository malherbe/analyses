<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">UN DOUZAIN DE SONNETS</head><div type="poem" key="GAU329" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
					<head type="main">SONNET III</head>
					<head type="sub_1">BAISER ROSE, BAISER BLEU</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2" punct="vg:3">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="1.3">l</w>’<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="1.5" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="1.7">r<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.9" punct="vg:12">gu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>p<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="2.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="2.4">d</w>’<w n="2.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="2.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="2.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.9" punct="vg:12">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="3.3" punct="vg:4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4" punct="vg">ein</seg></w>, <w n="3.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="3.8" punct="vg:12">b<seg phoneme="o" type="vs" value="1" rule="315" place="11" mp="M">eau</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:2">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>t</w>, <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>s</w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="4.4" punct="vg:6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="4.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="4.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="4.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="4.8" punct="pt:12">p<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="5.2">tr<seg phoneme="o" type="vs" value="1" rule="415" place="2" mp="M">ô</seg>ni<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.4" punct="vg:6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="5.5">r<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.6" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>g<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="448" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="6.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="6.6">d</w>’<w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="6.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="6.10" punct="vg:12">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="11" mp="M">ein</seg>t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.6" punct="vg:6">fru<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="7.8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="7.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="7.10" punct="vg:12">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Gl<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="8.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg" caesura="1">au</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="8.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.7" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>p<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="9.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9" mp="M">ê</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="9.8"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="9.9" punct="vg:12">j<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">P<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="10.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="10.4" punct="vg:6">r<seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="vg" caesura="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="10.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg></w> <w n="10.8" punct="pv:12">bl<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg></rhyme></w> ;</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="11.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="11.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="11.6" punct="vg:6">l<seg phoneme="y" type="vs" value="1" rule="453" place="6" punct="vg" caesura="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="11.8">f<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="11.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="11.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.11">l</w>’<w n="11.12" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>lb<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="12.4" punct="vg:6">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg" caesura="1">eau</seg></w>,<caesura></caesura> <w n="12.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="12.7" punct="vg:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="vg">ai</seg>s</w>, <w n="12.8" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11" mp="M">ê</seg>v<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="13.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="13.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="13.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>t</w> <w n="13.8" punct="dp:12">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg><rhyme label="d" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="14" num="4.3" lm="12" met="6+6">«<w n="14.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="14.2">tr<seg phoneme="o" type="vs" value="1" rule="433" place="2">o</seg>p</w> <w n="14.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="14.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg" caesura="1">e</seg>ts</w>,<caesura></caesura> <w n="14.5">s</w>’<w n="14.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ls</w> <w n="14.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="C">eu</seg>r</w> <w n="14.9" punct="pe:12">b<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>nh<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pe">eu</seg>r</rhyme></w> !»</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Saint-Gratien</placeName>,
							<date not-before="1859" not-after="1869">25 juillet 186.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>