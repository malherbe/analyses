<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU321" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba cdc ddc">
				<head type="main">L’HIRONDELLE</head>
				<head type="form">SONNET</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.4">h<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="1.7"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="1.8" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="2.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="2.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="2.7" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>j<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</rhyme></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d</w> <w n="3.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="3.5">r<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="3.6">s</w>’<w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="4.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="4.5" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="4.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="4.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg>t</w> <w n="4.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="4.9" punct="pt:12">t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="5.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="5.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="5.4">j</w>’<w n="5.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="5.7">cr<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>n<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="5.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="5.9" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>rpl<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2" punct="vg:3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="6.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" caesura="1">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="6.7">r<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.9" punct="vg:12">j<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</rhyme></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="7.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>cs</w> <w n="7.4">m<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>ts</w><caesura></caesura> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7" mp="M">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="7.7">n<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="7.8" punct="vg:12">t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</rhyme></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>mm<seg phoneme="y" type="vs" value="1" rule="d-3" place="3" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="8.5">d</w>’<w n="8.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="8.7">j<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="8.8">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="8.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="8.10" punct="pt:12">t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="cdc">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">Au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.4">m</w>’<w n="9.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="vg" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="9.7">li<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="9.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="9.9">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.10" punct="vg:12">ti<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="12" punct="vg">en</seg>t</rhyme></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="10.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="6" caesura="1">y</seg>s</w><caesura></caesura> <w n="10.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>g<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="11.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.9" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>vi<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="12" punct="pt">en</seg>t</rhyme></w>.</l>
				</lg>
				<lg n="4" rhyme="ddc">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="12.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="12.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="12.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="12.9" punct="vg:12">l<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>g<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="13.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.4">l</w>’<w n="13.5" punct="vg:6"><seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="13.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">D</w>’<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="14.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="14.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="14.7">l</w>’<w n="14.8"><seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.9"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="14.10">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.11">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>r</w> <w n="14.12" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>vi<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="12" punct="pt">en</seg>t</rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1867">1867</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>