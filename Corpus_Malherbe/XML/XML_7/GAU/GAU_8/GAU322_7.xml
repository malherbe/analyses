<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU322" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(abab)">
				<head type="main">L’ODALISQUE A PARIS</head>
				<head type="sub_1">A MADAME RIMSKI KORSAKOW</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="1.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="1.4" punct="pi:4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pi">e</seg></w> ? <w n="1.5">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="361" place="7">e</seg>m</w> <w n="1.7">s</w>’<w n="1.8" punct="vg:8"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>gd<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d</w> <w n="2.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="2.5" punct="vg:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="3.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rds</w> <w n="4.6" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rpr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.3">l<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rb<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.5">d</w>’<w n="6.6" punct="pv:8"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pv">o</seg>r</rhyme></w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.3" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>t</w>, <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">M</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>f<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="8.5" punct="pt:8">tr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="10.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.4" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>p<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">S<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t</w> <w n="11.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.5">g<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="12.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.6" punct="pt:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="13.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="13.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>b<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">O</seg>shm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg></w> <w n="14.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-27" place="6">e</seg></w> <w n="14.6" punct="pe:8">h<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></rhyme></w> !</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="3">en</seg>n<seg phoneme="ɥi" type="vs" value="1" rule="462" place="4">u</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="15.4">d</w>’<w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="15.6" punct="vg:8">b<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>gn<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="4" punct="vg">o</seg></w>, <w n="16.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="16.4" punct="pe:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1867">1867</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>