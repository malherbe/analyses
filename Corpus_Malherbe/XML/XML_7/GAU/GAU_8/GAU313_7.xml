<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU313" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abab abab ccd eed">
				<head type="main">A MARGUERITE</head>
				<head type="sub_1">A MADAME MARGUERITE DARDENNE DE LA GRANGERIE</head>
				<head type="sub_2">SONNET I</head>
				<lg n="1" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="1.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="1.3" punct="vg:6">ch<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="11">en</seg>s</w> <w n="1.7" punct="vg:12">r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">L<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="Lc">i</seg></w>-<w n="2.4">T<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" mp="Lc">ai</seg></w>-<w n="2.5" punct="vg:6">P<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="2.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="2.7"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="C">i</seg>l</w> <w n="2.8">f<seg phoneme="œ" type="vs" value="1" rule="304" place="9" mp="M">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="2.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="2.10" punct="vg:12">v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>rs</rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="3.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>r</w> <w n="3.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="3.6">p<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>t</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="4.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>rs</w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="4.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>rs</w> <w n="4.9">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>rs</w> <w n="4.10" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="pt">e</seg>rts</rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">v<seg phoneme="y" type="vs" value="1" rule="457" place="2">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6" caesura="1">um</seg></w><caesura></caesura> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="5.9" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Mi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="6.4">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" mp="M">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="6.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>cs</w><caesura></caesura> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="6.9">s<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="6.10" punct="vg:12">v<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="vg">e</seg>rts</rhyme></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">In</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="7.3" punct="vg:6">l<seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ttr<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="7.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="7.7" punct="vg:12">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>scr<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="8.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="8.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w> <w n="8.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rs</w> <w n="8.8" punct="pt:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>rs</rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1" mp="C">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3" punct="vg:6">M<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.4"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="9.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="9.6" punct="vg:12">f<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.4">c<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="10.6">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="10.8" punct="vg:12">Ch<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="11.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="11.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="11.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rds</w> <w n="11.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="11" mp="M">ou</seg><rhyme label="d" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg>s</rhyme></w>,</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="12.4" punct="vg:6">M<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="12.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="12.7" punct="vg:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">Vi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="13.2">r<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>br<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.9" punct="vg:12">pr<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">Tr<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="14.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="14.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</w> <w n="14.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="11" mp="M">ou</seg><rhyme label="d" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg>s</rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1865">19 juillet 1865</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>