<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FANTAISIES</head><div type="poem" key="FER11" modus="sm" lm_max="3" metProfile="3" form="suite périodique" schema="5(ababcdcd)">
					<head type="main">LA BULLE DE SAVON</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="3" met="3"><w n="1.1" punct="vg:2">V<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="1.2">sph<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="3" met="3"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</rhyme></w></l>
						<l n="3" num="1.3" lm="3" met="3"><w n="3.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">f<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="3" met="3"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="4.2" punct="pe:3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="pe">an</seg>t</rhyme></w> !</l>
						<l n="5" num="1.5" lm="3" met="3"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l</w>’<w n="5.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="6" num="1.6" lm="3" met="3"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="6.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="6.4">d<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</rhyme></w></l>
						<l n="7" num="1.7" lm="3" met="3"><w n="7.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="3" met="3"><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2" punct="pe:3">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="pe">ou</seg>x</rhyme></w> !</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababcdcd">
						<l n="9" num="2.1" lm="3" met="3"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">r<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="10" num="2.2" lm="3" met="3"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3" punct="vg:3">su<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="11" num="2.3" lm="3" met="3"><w n="11.1" punct="vg:3">F<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="2.4" lm="3" met="3"><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="12.2">t</w>’<w n="12.3" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>fu<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="13" num="2.5" lm="3" met="3"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="13.3">r<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="14" num="2.6" lm="3" met="3"><w n="14.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>f</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="14.3">cl<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</rhyme></w></l>
						<l n="15" num="2.7" lm="3" met="3"><w n="15.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="15.2">z<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ph<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="16" num="2.8" lm="3" met="3"><w n="16.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="16.2">l</w>’<w n="16.3"><rhyme label="d" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</rhyme></w></l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababcdcd">
						<l n="17" num="3.1" lm="3" met="3"><w n="17.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="17.2" punct="pe:2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe">on</seg>c</w> ! <w n="17.3">b<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="18" num="3.2" lm="3" met="3"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></rhyme></w></l>
						<l n="19" num="3.3" lm="3" met="3"><w n="19.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="19.2">gl<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>b<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="20" num="3.4" lm="3" met="3"><w n="20.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="20.2" punct="pe:3">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="pe">on</seg></rhyme></w> !</l>
						<l n="21" num="3.5" lm="3" met="3"><w n="21.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="21.2">l</w>’<w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="22" num="3.6" lm="3" met="3"><w n="22.1">J</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="22.4">v<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</rhyme></w></l>
						<l n="23" num="3.7" lm="3" met="3"><w n="23.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="23.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rf<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="24" num="3.8" lm="3" met="3"><w n="24.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2" punct="pt:3">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="pt">oi</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="4" type="huitain" rhyme="ababcdcd">
						<l n="25" num="4.1" lm="3" met="3"><w n="25.1">Fr<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="26" num="4.2" lm="3" met="3"><w n="26.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">l</w>’<w n="26.3"><rhyme label="b" id="14" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></rhyme></w></l>
						<l n="27" num="4.3" lm="3" met="3"><w n="27.1">J<seg phoneme="a" type="vs" value="1" rule="307" place="1">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="28" num="4.4" lm="3" met="3"><w n="28.1">D</w>’<w n="28.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="28.3" punct="vg:3">ru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="29" num="4.5" lm="3" met="3"><w n="29.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="29.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>fl<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg>s</rhyme></w></l>
						<l n="30" num="4.6" lm="3" met="3"><w n="30.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="30.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="30.3">f<rhyme label="d" id="16" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</rhyme></w></l>
						<l n="31" num="4.7" lm="3" met="3"><w n="31.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="31.3">j<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg>s</rhyme></w></l>
						<l n="32" num="4.8" lm="3" met="3"><w n="32.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="32.2">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="32.3" punct="pt:3">bl<rhyme label="d" id="16" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="3" punct="pt">eu</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="5" type="huitain" rhyme="ababcdcd">
						<l n="33" num="5.1" lm="3" met="3"><w n="33.1" punct="vg:1">V<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="33.2"><seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg></w> <w n="33.3" punct="pe:3">sph<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pe">e</seg></rhyme></w> !</l>
						<l n="34" num="5.2" lm="3" met="3"><w n="34.1">Qu</w>’<w n="34.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="34.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</rhyme></w></l>
						<l n="35" num="5.3" lm="3" met="3"><w n="35.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="35.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3">f<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="36" num="5.4" lm="3" met="3"><w n="36.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="36.2" punct="pe:3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><rhyme label="b" id="18" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="pe">an</seg>t</rhyme></w> !</l>
						<l n="37" num="5.5" lm="3" met="3"><w n="37.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="37.2">l</w>’<w n="37.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<rhyme label="c" id="19" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="38" num="5.6" lm="3" met="3"><w n="38.1">D</w>’<w n="38.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="38.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="38.4">d<rhyme label="d" id="20" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</rhyme></w></l>
						<l n="39" num="5.7" lm="3" met="3"><w n="39.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="39.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<rhyme label="c" id="19" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="40" num="5.8" lm="3" met="3"><w n="40.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="40.2" punct="pe:3">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<rhyme label="d" id="20" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="pe">ou</seg>x</rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>