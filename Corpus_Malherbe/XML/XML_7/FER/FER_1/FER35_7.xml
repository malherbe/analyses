<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">D’ICI DE LÀ</head><div type="poem" key="FER35" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="3(abab)">
					<head type="main">CHANTE ENCORE !</head>
					<opener>
						<salute>À MISS E. EHRTONE (France)</salute>
					</opener>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="1.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5" punct="vg:8">g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="2.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="2.3">l</w>’<w n="2.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="2.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="2.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cc<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rd</rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="3.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>gs</w> <w n="3.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>ts</w> <w n="3.6">d</w>’<w n="3.7" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="4.3" punct="vg:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="4.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>r</rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="5.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.7" punct="vg:8">l<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="6.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="6.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="6.6" punct="pv:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg></rhyme></w> ;</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="7.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="7.5">qu</w>’<w n="7.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="7.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>dm<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="8.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="8.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="8.6">l</w>’<w n="8.7" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="10.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="10.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="10.7" punct="vg:8">b<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="11.3" punct="vg:4">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="11.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.5" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="12.2">C<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.4" punct="pt:8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>b<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>