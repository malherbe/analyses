<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR18" modus="cp" lm_max="12" metProfile="6, 6+6, 4+6" form="suite périodique" schema="2(ababcdcd)">
						<head type="main">Hymne Au Soleil</head>
						<opener>
							<salute>À ANDRÉ BOREL.</salute>
							<epigraph>
								<cit>
									<quote>Pauvre bougre !</quote>
									<bibl>
										<name>Jules JANIS</name>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="1.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="1.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="1.5" punct="vg:6">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="1.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="1.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="2.3">cl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="2.4" punct="vg:6">m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>l</rhyme></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="3.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>ffr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="3.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="3.10">t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.3">br<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt">a</seg>l</rhyme></w>.</l>
							<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="5.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5" punct="vg:6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="vg" caesura="1">aim</seg></w>,<caesura></caesura> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="5.10" punct="vg:12">pi<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="12"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.3" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>mm<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="vg">e</seg>il</rhyme></w>,</l>
							<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="7.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="7.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="7.6">br<seg phoneme="y" type="vs" value="1" rule="445" place="8" mp="M">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.7" punct="pv:12">p<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>pi<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
							<l n="8" num="1.8" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="8.3"><seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="8.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>c<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>t</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="8.7" punct="pe:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="10" punct="pe">e</seg>il</rhyme></w> !</l>
						</lg>
						<lg n="2" type="huitain" rhyme="ababcdcd">
							<l n="9" num="2.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="Lc">à</seg></w>-<w n="9.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="9.5" punct="vg:6">c<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rd<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="10" num="2.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>fs</w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="10.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="10.5" punct="dp:6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>p<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="dp">a</seg>rt</rhyme></w> :</l>
							<l n="11" num="2.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="11.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M/mc">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="Lc">on</seg></w>-<w n="11.3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="11.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>d</w><caesura></caesura> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="11.10" punct="pv:12">v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
							<l n="12" num="2.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="12.3" punct="vg:3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="12.4">j</w>’<w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="12.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.7" punct="pe:6">p<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>rt</rhyme></w> !</l>
							<l n="13" num="2.5" lm="12" met="6+6"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="13.3" punct="vg:3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="13.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>g<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>x</w><caesura></caesura> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="13.7" punct="vg:9">t<seg phoneme="wa" type="vs" value="1" rule="423" place="9" punct="vg">oi</seg></w>, <w n="13.8">s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="11">e</seg>il</w> <w n="13.9" punct="vg:12">j<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="14" num="2.6" lm="6" met="6"><space unit="char" quantity="12"></space><w n="14.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="14.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.4" punct="vg:6">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg>y<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>s</rhyme></w>,</l>
							<l n="15" num="2.7" lm="12" met="6+6"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="15.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="15.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="15.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>x</w><caesura></caesura> <w n="15.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="15.8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="15.9">d</w>’<w n="15.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="15.11">S<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.12" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>g<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="448" place="12">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="16" num="2.8" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg></w> <w n="16.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="16.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>t</w><caesura></caesura> <w n="16.5">d</w>’<w n="16.6"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="16.7">gu<seg phoneme="ø" type="vs" value="1" rule="403" place="7">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="16.9" punct="pt:10">h<seg phoneme="a" type="vs" value="1" rule="307" place="9" mp="M">a</seg>ill<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pt">on</seg>s</rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>