<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA26" modus="cp" lm_max="12" metProfile="8, 4+6, 6+6" form="suite de strophes" schema="3[abab] 1[abbaba] 1[abba]">
				<head type="number">XVI</head>
				<head type="main">Charlottembourg</head>
				<head type="sub">Ou le tombeau de la reine de Prusse</head>
				<opener>
					<dateline>
						<placeName>Berlin</placeName>,
						<date when="1821">1821</date>.
					</dateline>
				</opener>
				<lg n="1" type="regexp" rhyme="ab">
					<head type="main">Le voyageur</head>
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="1.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ts</w> <w n="1.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" caesura="1">in</seg>s</w><caesura></caesura> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="1.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="1.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="1.8" punct="vg:10">s<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:2">G<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rdi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2" punct="vg">en</seg></w>, <w n="2.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="Lp">i</seg>s</w>-<w n="2.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="2.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6" caesura="1">e</seg>st</w><caesura></caesura> <w n="2.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.7">m<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>n<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="10">en</seg>t</w> <w n="2.8" punct="pi:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pi">eau</seg></rhyme></w> ?</l>
				</lg>
				<lg n="2" type="regexp" rhyme="ab">
					<head type="main">Le gardien</head>
					<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="3.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="3.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="3.9" punct="dp:12">c<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg>s</rhyme></w> :</l>
					<l n="4" num="2.2" lm="8" met="8"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="4.2" punct="pe:4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pe">eu</seg>r</w> ! <w n="4.3">c</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="4.6" punct="pt:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="regexp" rhyme="">
					<head type="main">Le voyageur</head>
					<l part="I" n="5" num="3.1" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="5.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="5.5" punct="pi:6">li<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pi" caesura="1">eu</seg>x</w> ?<caesura></caesura> </l>
				</lg>
				<lg n="4" type="regexp" rhyme="a">
					<head type="main">Le gardien</head>
					<l part="F" n="5" lm="12" met="6+6"><w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">Un</seg></w> <w n="5.7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>t</w> <w n="5.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10">ein</seg></w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="5.10" punct="pt:12">ch<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="5" type="regexp" rhyme="">
					<head type="main">Le voyageur</head>
					<l part="I" n="6" num="5.1" lm="8" met="8"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="6.3" punct="pi:3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pi">a</seg></w> ? </l>
				</lg>
				<lg n="6" type="regexp" rhyme="b">
					<head type="main">Le gardien</head>
					<l part="F" n="6" lm="8" met="8"><w n="6.4">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="6.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t</w> <w n="6.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<lg n="7" type="regexp" rhyme="">
					<head type="main">Le voyageur</head>
					<l part="I" n="7" num="7.1" lm="8" met="8"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="7.2" punct="pt:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pt">oi</seg></w>. </l>
				</lg>
				<lg n="8" type="regexp" rhyme="a">
					<head type="main">Le gardien</head>
					<l part="F" n="7" lm="8" met="8"><w n="7.3">S<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="7.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="7.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>s</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.7" punct="vg:8">l<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l part="I" n="8" num="8.1" lm="8" met="8"><w n="8.1">N</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.3" punct="pt:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pt">a</seg>s</w>. </l>
				</lg>
				<lg n="9" type="regexp" rhyme="b">
					<head type="main">Le voyageur</head>
					<l part="F" n="8" lm="8" met="8"><w n="8.4">J</w>’<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="8.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="8.7" punct="pt:8">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>r<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<p>Le voyageur et le gardien entrent.</p>
				<lg n="10" type="regexp" rhyme="abba">
					<head type="main">Le voyageur</head>
					<l n="9" num="10.1" lm="8" met="8"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">Gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="i" type="vs" value="1" rule="468" place="6">I</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="10" num="10.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="10.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.8">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="10.10" punct="pt:12">m<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rts</rhyme></w>.</l>
					<l n="11" num="10.3" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="11.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.5">c<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="11.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="11.9" punct="pi:12">b<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pi">o</seg>rds</rhyme></w> ?</l>
					<l n="12" num="10.4" lm="8" met="8"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="12.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">An</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="12.5" punct="pi:8">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
				</lg>
				<lg n="11" type="regexp" rhyme="ba">
					<head type="main">Le gardien</head>
					<l n="13" num="11.1" lm="12" met="6+6"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="13.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="13.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>xc<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="13.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>sp<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rts</rhyme></w></l>
					<l n="14" num="11.2" lm="8" met="8"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="14.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="14.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.6" punct="pt:8">v<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="12" type="regexp" rhyme="ab">
					<head type="main">Le voyageur</head>
					<l n="15" num="12.1" lm="12" met="6+6"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="15.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="15.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="15.6">m<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>rs</w><caesura></caesura> <w n="15.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="15.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="15.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11" mp="M">ê</seg>t<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s</rhyme></w></l>
					<l n="16" num="12.2" lm="10" met="4+6"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="16.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg></w><caesura></caesura> <w n="16.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="16.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="16.5" punct="pi:10">f<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>n<rhyme label="b" id="8" gender="f" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg>s</rhyme></w> ?</l>
				</lg>
				<lg n="13" type="regexp" rhyme="ab">
					<head type="main">Le gardien</head>
					<l n="17" num="13.1" lm="8" met="8"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="17.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="17.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="17.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</rhyme></w></l>
					<l n="18" num="13.2" lm="8" met="8"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w>-<w n="18.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="18.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="18.4" punct="pt:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="b" id="8" gender="f" type="e" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="14" type="regexp" rhyme="">
					<head type="main">Le voyageur</head>
					<l part="I" n="19" num="14.1" lm="12" met="6+6"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="19.2" punct="pt:2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2" punct="pt">en</seg>t</w>. </l>
				</lg>
				<lg n="15" type="regexp" rhyme="ab">
					<head type="main">Le gardien</head>
					<l part="F" n="19" lm="12" met="6+6"><w n="19.3">C</w>’<w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="19.6" punct="dp:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="dp" caesura="1">ou</seg>x</w> :<caesura></caesura> <w n="19.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="19.8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.9"><seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="19.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="19.11">p<rhyme label="a" id="9" gender="m" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s</rhyme></w></l>
					<l n="20" num="15.1" lm="12" met="6+6"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="20.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="20.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="20.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="20.7" punct="pt:12">f<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>n<rhyme label="b" id="10" gender="f" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="16" type="regexp" rhyme="">
					<head type="main">Le voyageur</head>
					<l part="I" n="21" num="16.1" lm="12" met="6+6"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="21.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="21.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="21.5" punct="pi:6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pi" caesura="1">u</seg></w> ?<caesura></caesura> </l>
				</lg>
				<lg n="17" type="regexp" rhyme="b">
					<head type="main">Le gardien</head>
					<l part="F" n="21" lm="12" met="6+6"><w n="21.6" punct="dp:7">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="dp">on</seg></w> : <w n="21.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="21.8">tr<seg phoneme="o" type="vs" value="1" rule="415" place="9">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="21.9">lu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="C">i</seg></w> <w n="21.10" punct="pt:12">r<rhyme label="b" id="10" gender="f" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="18" type="regexp" rhyme="a">
					<head type="main">Le voyageur</head>
					<l n="22" num="18.1" lm="8" met="8"><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="22.2">tr<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="22.5" punct="pt:8">p<rhyme label="a" id="9" gender="m" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>