<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC32" modus="cp" lm_max="12" metProfile="8, 6, (6+6), 4+6, (4)" form="suite périodique" schema="1(ababccddc) 4(abba) 1(aabcbc) 2(abab)">
				<head type="main">L’heure d’Amour</head>
				<lg n="1" type="neuvain" rhyme="ababccddc">
					<head type="speaker">LUI</head>
					<l n="1" num="1.1" lm="4"><space unit="char" quantity="16"></space><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">su<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="2.3" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="2.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="3.3" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="3.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="3.6" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="7">en</seg>nu<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pv">i</seg>s</rhyme></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6" met_alone="True"><w n="4.1">S<seg phoneme="wa" type="vs" value="1" rule="440" place="1" mp="M">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="4.4" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="4.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10" mp="Lc">en</seg></w>-<w n="4.8" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11" mp="M/mc">ai</seg>m<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l</w>’<w n="5.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="5.6" punct="pt:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>t</rhyme></w>.</l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w>-<w n="6.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4" punct="vg">en</seg></w>, <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.5" punct="vg:8">fu<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="7.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="7.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.6" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">L</w>’<w n="8.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.3">d</w>’<w n="8.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="8.5">l</w>’<w n="8.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.7" punct="vg:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="9.2">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="9.4">s</w>’<w n="9.5" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pe">i</seg>t</rhyme></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<head type="speaker">ELLE</head>
					<l n="10" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.4" punct="pi:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></rhyme></w> ?</l>
					<l n="11" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">B<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="11.2">l</w>’<w n="11.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="11.5" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7">a</seg>y<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="12" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="12.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="12.4">l</w>’<w n="12.5" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" punct="vg">en</seg>d</w>, <w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="12.7">s<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="13" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">b<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="13.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="13.4">l</w>’<w n="13.5" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<head type="speaker">LUI</head>
					<l n="14" num="3.1" lm="6" met="6"><space unit="char" quantity="12"></space><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="14.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="14.3">m</w>’<w n="14.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="4">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></rhyme></w>,</l>
					<l n="15" num="3.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="15.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.5" punct="vg:6">v<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="6">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="3.3" lm="6" met="6"><space unit="char" quantity="12"></space><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="6">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="17" num="3.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="17.2">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</w> <w n="17.3">t</w>’<w n="17.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe">er</seg></rhyme></w> !</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabcbc">
					<head type="speaker">ELLE</head>
					<l n="18" num="4.1" lm="6" met="6"><space unit="char" quantity="12"></space><w n="18.1">C</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="18.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="18.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="18.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.7" punct="pe:6">v<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pe">i</seg>s</rhyme></w> !</l>
					<l n="19" num="4.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="19.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="19.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="19.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="19.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.6" punct="vg:6">v<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="20" num="4.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="20.4">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="5">um</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="20.6" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="21" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="21.1">S</w>’<w n="21.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>tr</w>’<w n="21.3"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="21.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>il</w> <w n="21.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.7" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="c" id="11" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
					<l n="22" num="4.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="22.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="22.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.5">ti<seg phoneme="ɛ" type="vs" value="1" rule="366" place="5">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="23" num="4.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="23.1">B<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="23.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="23.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="23.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="c" id="11" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>r</rhyme></w> !</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<head type="speaker">LUI</head>
					<l n="24" num="5.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="24.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w>-<w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="24.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="24.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="24.5">m<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>ts</w> <w n="24.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="24.7" punct="vg:8">d<rhyme label="a" id="12" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>x</rhyme></w>,</l>
					<l n="25" num="5.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="25.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="25.2">m<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="25.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="25.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="25.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s<rhyme label="b" id="13" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="26" num="5.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3">l</w>’<w n="26.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="26.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="26.7" punct="ps:8">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="a" id="12" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="ps">ou</seg>x</rhyme></w>…</l>
					<l n="27" num="5.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="27.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="27.2">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="27.3" punct="pe:4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pe ps">eu</seg>ls</w> !… <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="27.5">l</w>’<w n="27.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="27.7" punct="pe:8">v<rhyme label="b" id="13" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abba">
					<head type="speaker">ELLE</head>
					<l n="28" num="6.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="28.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w>-<w n="28.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4" punct="vg">en</seg></w>, <w n="28.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="28.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="28.5" punct="pv:8">fu<rhyme label="a" id="14" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pv">i</seg>t</rhyme></w> ;</l>
					<l n="29" num="6.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="29.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="29.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="29.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="29.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="29.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="29.6" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="b" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="30" num="6.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="30.1">L</w>’<w n="30.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="30.3">d</w>’<w n="30.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="30.5">l</w>’<w n="30.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="30.7" punct="vg:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="15" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="31" num="6.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="31.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="31.2">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="31.4">s</w>’<w n="31.5" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><rhyme label="a" id="14" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pe">i</seg>t</rhyme></w> !</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abba">
					<head type="speaker">LUI</head>
					<l n="32" num="7.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="32.1">Qu</w>’<w n="32.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="32.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="32.4" punct="pe:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="16" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
					<l n="33" num="7.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="33.1">B<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="33.2">l</w>’<w n="33.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="33.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="33.5" punct="pv:8">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7">a</seg>y<rhyme label="b" id="17" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="34" num="7.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="34.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="34.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="34.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="34.4">l</w>’<w n="34.5" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" punct="vg">en</seg>d</w>, <w n="34.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="34.7" punct="vg:8">s<rhyme label="b" id="17" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="35" num="7.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="35.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="35.2">b<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="35.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="35.4">l</w>’<w n="35.5" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="16" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<head type="speaker">ELLE et LUI</head>
					<l n="36" num="8.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="36.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pe">ou</seg>r</w> ! <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="36.3" punct="pe:8">m<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>st<rhyme label="a" id="18" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="37" num="8.2" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="37.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="1" mp="M/mp">En</seg><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fm">e</seg></w>-<w n="37.2" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="37.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="37.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="37.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="37.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="37.7" punct="pv:10">mi<rhyme label="b" id="19" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10" punct="pv">e</seg>l</rhyme></w> ;</l>
					<l n="38" num="8.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="38.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="38.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="38.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rds</w> <w n="38.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="38.6" punct="vg:8">t<rhyme label="a" id="18" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="39" num="8.4" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="39.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="39.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="39.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>rs</w><caesura></caesura> <w n="39.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="39.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="39.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="39.8" punct="pe:10">ci<rhyme label="b" id="19" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10" punct="pe">e</seg>l</rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
					<date when="1859">1859</date>
					</dateline>
				</closer>
			</div></body></text></TEI>