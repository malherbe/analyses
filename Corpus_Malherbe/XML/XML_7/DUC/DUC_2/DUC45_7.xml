<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC45" modus="sp" lm_max="8" metProfile="4, (8)" form="suite périodique" schema="2(aabcccb) 1(aabccb)">
				<head type="main">Ganzone</head>
				<lg n="1" type="septain" rhyme="aabcccb">
					<l n="1" num="1.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="1.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</rhyme></w></l>
					<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3" punct="vg:4">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</rhyme></w>,</l>
					<l n="3" num="1.3" lm="4" met="4"><space unit="char" quantity="8"></space><w n="3.1">L</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="3.3" punct="pt:4">h<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></w>.</l>
					<l n="4" num="1.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">bru<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</rhyme></w></l>
					<l n="5" num="1.5" lm="4" met="4"><space unit="char" quantity="8"></space><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t</w> <w n="5.4" punct="vg:4">su<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>t</rhyme></w>,</l>
					<l n="6" num="1.6" lm="4" met="4"><space unit="char" quantity="8"></space><w n="6.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="6.3" punct="vg:4">fu<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>t</rhyme></w>,</l>
					<l n="7" num="1.7" lm="4" met="4"><space unit="char" quantity="8"></space><w n="7.1" punct="vg:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>s</w>, <w n="7.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="7.3" punct="pe:4">v<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="2" type="septain" rhyme="aabcccb">
					<l n="8" num="2.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3" punct="vg:4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</rhyme></w>,</l>
					<l n="9" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="9.1">L</w>’<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4" punct="pv:4">p<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pv">eu</seg>r</rhyme></w> ;</l>
					<l n="10" num="2.3" lm="4" met="4"><space unit="char" quantity="8"></space><w n="10.1" punct="vg:1">Tr<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="10.3" punct="pe:4">h<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe ps">e</seg></rhyme></w> !…</l>
					<l n="11" num="2.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">l</w>’<w n="11.3">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="2">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.4" punct="vg:4">t<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>t</rhyme></w>,</l>
					<l n="12" num="2.5" lm="4" met="4"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="12.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4" punct="vg:4">v<rhyme label="c" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>t</rhyme></w>,</l>
					<l n="13" num="2.6" lm="4" met="4"><space unit="char" quantity="8"></space><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rç<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</rhyme></w></l>
					<l n="14" num="2.7" lm="4" met="4"><space unit="char" quantity="8"></space><w n="14.1">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="14.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="14.3" punct="pe:4">v<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="15" num="3.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="15.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></rhyme></w></l>
					<l n="16" num="3.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="16.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rr<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></rhyme></w></l>
					<l n="17" num="3.3" lm="4" met="4"><space unit="char" quantity="8"></space><w n="17.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="17.2">v<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
					<l n="18" num="3.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="18.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="18.3">l</w>’<w n="18.4" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rt</rhyme></w>,</l>
					<l n="19" num="3.5" lm="4" met="4"><space unit="char" quantity="8"></space><w n="19.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="19.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="19.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<rhyme label="c" id="9" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rd</rhyme></w>,</l>
					<l n="20" num="3.6" lm="8"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="20.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="20.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="20.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5" punct="pe:8">r<seg phoneme="e" type="vs" value="1" rule="213" place="6">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>sc<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1865">1865</date>
					</dateline>
				</closer>
			</div></body></text></TEI>