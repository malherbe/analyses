<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE7" modus="sm" lm_max="4" metProfile="4" form="suite périodique avec vers clausules" schema="1(abab) 2(ababc) 2(c) 1(abba)">
				<head type="main">COQUILLAGES</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="4" met="4"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ppr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="4" met="4"><w n="2.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="2.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</rhyme></w></l>
					<l n="3" num="1.3" lm="4" met="4"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3">gr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg>s</rhyme></w></l>
					<l n="4" num="1.4" lm="4" met="4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3" punct="pv:4">c<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pv">ou</seg>x</rhyme></w> ;</l>
				</lg>
				<lg n="2" type="quintil" rhyme="ababc">
					<l n="5" num="2.1" lm="4" met="4"><w n="5.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>d<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
					<l n="6" num="2.2" lm="4" met="4"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</rhyme></w></l>
					<l n="7" num="2.3" lm="4" met="4"><w n="7.1">D</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="7.4">s</w>’<w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>rr<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="4" met="4"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.3" punct="ps:4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="ps">i</seg>r</rhyme></w>…</l>
					<l n="9" num="2.5" lm="4" met="4">— <w n="9.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="9.2" punct="vg:4">r<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="3" type="vers clausule" rhyme="c">
					<l n="10" num="3.1" lm="4" met="4"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="10.2" punct="pe:4">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="3">i</seg>ll<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg>s</rhyme></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4" type="quatrain" rhyme="abba">
					<l n="11" num="4.1" lm="4" met="4"><w n="11.1">Vi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="11.2" punct="pt:4">b<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>b<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="pt">o</seg>ts</rhyme></w>.</l>
					<l n="12" num="4.2" lm="4" met="4"><w n="12.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="12.2" punct="dp:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg>s</rhyme></w> :</l>
					<l n="13" num="4.3" lm="4" met="4"><w n="13.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="13.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="13.3">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><rhyme label="b" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
					<l n="14" num="4.4" lm="4" met="4"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3" punct="pe:4">fl<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="pe">o</seg>ts</rhyme></w> !</l>
				</lg>
				<lg n="5" type="quintil" rhyme="ababc">
					<l n="15" num="5.1" lm="4" met="4"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2">M<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="15.3">h<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<rhyme label="a" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
					<l n="16" num="5.2" lm="4" met="4"><w n="16.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="16.3" punct="ps:4">v<rhyme label="b" id="9" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="ps">ou</seg>s</rhyme></w>…</l>
					<l n="17" num="5.3" lm="4" met="4"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3" punct="pt:4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t<rhyme label="a" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="18" num="5.4" lm="4" met="4"><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="18.2" punct="vg:4">b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>j<rhyme label="b" id="9" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>x</rhyme></w>,</l>
					<l n="19" num="5.5" lm="4" met="4"><w n="19.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="19.3">d</w>’<w n="19.4" punct="pe:4"><rhyme label="c" id="10" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="6" type="vers clausule" rhyme="c">
					<l n="20" num="6.1" lm="4" met="4">— <w n="20.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="20.2" punct="pt:4">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="3">i</seg>ll<rhyme label="c" id="10" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>