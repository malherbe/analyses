<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI49" modus="cp" lm_max="12" metProfile="6+6, (8)" form="strophe unique" schema="1(aabccb)">
				<head type="main">AUTRE</head>
				<head type="sub">sur le même sujet</head>
				<opener>
					<dateline>
						<date when="1693">(1693)</date>
					</dateline>
				</opener>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D</w>’<w n="1.2"><seg phoneme="u" type="vs" value="1" rule="426" place="1">où</seg></w> <w n="1.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.5" punct="vg:6">C<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="1.6" punct="vg:8">Pl<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>, <w n="1.7" punct="vg:10">V<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="1.8" punct="vg:12">H<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>m<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="2.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="2.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ds</w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="2.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10">e</seg>rs</w> <w n="2.9" punct="vg:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ts</w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="3.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="4" mp="C">o</seg>s</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ts</w><caesura></caesura> <w n="3.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="3.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="3.8" punct="pi:12">s<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pi">o</seg>ts</rhyme></w> ?</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:2">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rr<seg phoneme="o" type="vs" value="1" rule="318" place="2" punct="vg">au</seg>lt</w>, <w n="4.2">c</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="4.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ts</w> <w n="4.10">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>bl<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="5.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="5.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="5.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="5.6" punct="vg:10">b<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="5.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="5.8" punct="vg:12">r<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="6" num="1.6" lm="8"><space unit="char" quantity="8"></space><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.6" punct="pt:8">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="pt">au</seg>lt</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>