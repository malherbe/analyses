<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI47" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1[aabba] 1[abba]">
				<head type="main">Vers pour mettre au bas <lb></lb>du portrait de mon père,</head>
				<head type="sub_1">greffier de la Grand’Chambre <lb></lb>du Parlement de Paris</head>
				<opener>
					<dateline>
						<date when="1690">(1690)</date>
					</dateline>
				</opener>
				<lg n="1" type="regexp" rhyme="aabbaabba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2" punct="vg:3">gr<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ffi<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg></w>, <w n="1.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="1.5" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>f<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="2.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g</w> <w n="2.6">cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">N</w>’<w n="3.2"><seg phoneme="y" type="vs" value="1" rule="391" place="1">eu</seg>t</w> <w n="3.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>t</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="3.6" punct="pv:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg></rhyme></w> ;</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="4.2">f<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="4.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.5" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="5.6">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5">P<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</rhyme></w></l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="354" place="4">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.4" punct="vg:8">c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">Mi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="8.6" punct="vg:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.5" punct="pt:8">R<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pt">e</seg>ts</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>