<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI74" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(abba)">
				<head type="main">Vers pour mettre au bas du portrait de M. Racine</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="1.2">th<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="1.4">l</w>’<w n="1.5">h<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.8" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rv<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="2.3">r<seg phoneme="e" type="vs" value="1" rule="213" place="3" mp="M">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>sc<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="2.4">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>ph<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="2.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="2.7" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>cr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg>ts</rhyme></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt</w> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>rs</w> <w n="3.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.11" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>spr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>ts</rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>rp<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.2" punct="vg:6"><seg phoneme="ø" type="vs" value="1" rule="405" place="4" mp="M">Eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="4.5" punct="pt:12">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rn<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>