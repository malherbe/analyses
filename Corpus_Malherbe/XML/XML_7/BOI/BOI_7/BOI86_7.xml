<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">APPENDICE</title>
				<title type="sub_2">À L’ÉDITION DES ŒUVRES POÉTIQUES (édition Hachette, 1889)</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>125 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1654" to="1705">1654-1705</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI86" modus="cp" lm_max="12" metProfile="6, 5, 6+6" form="suite périodique" schema="2(abab) 1(abba)">
				<head type="number">I</head>
				<head type="main">Chanson à boire</head>
				<opener>
					<dateline>
						<date when="1654">(1654)</date>
					</dateline>
				</opener>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="1.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="1.4" punct="vg:6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="1.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="1.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="1.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="1.9" punct="vg:12">b<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="2.3">qu</w>’<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="2.5" punct="pv:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ffr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pv">i</seg>r</rhyme></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" mp="M">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="3.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="5" mp="C">o</seg>s</w> <w n="3.4" punct="vg:6">m<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg" caesura="1">au</seg>x</w>,<caesura></caesura> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="3.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="3.8">gl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="4.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="4.5" punct="pt:6">gu<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt">i</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="6" met="6"><space unit="char" quantity="12"></space><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="5.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</rhyme></w></l>
					<l n="6" num="2.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>cqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.3" punct="vg:6">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="7" num="2.3" lm="5" met="5"><space unit="char" quantity="14"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.4">tr<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="5" met="5"><space unit="char" quantity="14"></space><w n="8.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3" punct="pt:5">ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pt">on</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg></w>, <w n="9.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="9.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="9.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.7">cr<seg phoneme="y" type="vs" value="1" rule="454" place="11" mp="M">u</seg><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="10" num="3.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="10.4" punct="vg:6">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg>x</w> <w n="11.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rs</w> <w n="11.4" punct="vg:6">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg>s</w>,<caesura></caesura> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="11.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="11.7">qu</w>’<w n="11.8" punct="vg:12"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="12" num="3.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="12.3" punct="pt:6">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pt">on</seg></rhyme></w>.</l>
				</lg>
				<p>
					Cependant nous rirons, etc.
				</p>
			</div></body></text></TEI>