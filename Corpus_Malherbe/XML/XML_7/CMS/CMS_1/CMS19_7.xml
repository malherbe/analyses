<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS19" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
				<head type="main">DIGESTION</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ts</w> <w n="1.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ps</w> <w n="1.4">j</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>xc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="?" type="va" value="1" rule="162" place="10">en</seg>t</w> <w n="1.8" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>f<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="2.2">d</w>’<w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="2.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>gt</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="2.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>c</w><caesura></caesura> <w n="2.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9">an</seg>t</w> <w n="2.8">l</w>’<w n="2.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="250" place="10" mp="M">œ</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>ph<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="3.3">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="3.7">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">s</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>xh<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.5">pl<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="4.6">d</w>’<w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="4.8">h<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>ff<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4">en</seg>t</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>f</w><caesura></caesura> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="5.6" punct="pt:12">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>ph<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="Lc">o</seg>rs</w>-<w n="6.3">d</w>’<w n="6.4"><seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="6.6" punct="vg:6">r<seg phoneme="o" type="vs" value="1" rule="415" place="5" mp="M">ô</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="6.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="6.8">p<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="6.9"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="6.10" punct="pt:12">fr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>m<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="7.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l</w> <w n="7.4">pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="7.6">n</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="7.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="7.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="7.10">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.11" punct="pv:12">h<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>mm<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="8.2" punct="vg:3">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>t</w>, <w n="8.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg></w> <w n="8.4">f<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d</w><caesura></caesura> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="8.6">d<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nn<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="8.7" punct="pe:12">tr<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>ff<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>il</w> <w n="9.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="9.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="9.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="9.8" punct="pt:12">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>c<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>nt</rhyme></w>.</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">h<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" mp="M">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>ts</w> <w n="10.3"><seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="10.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="10.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="10.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="10.8">r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>pp<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>nt</rhyme></w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="11.3">b<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="11.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="9">ô</seg>t</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="11.8">f<seg phoneme="wa" type="vs" value="1" rule="423" place="11">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="11.9" punct="pt:12">gr<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="12.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="12.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="Lc">i</seg></w>-<w n="12.4" punct="vg:4">cl<seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="vg">o</seg>s</w>, <w n="12.5">j</w>’<w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="6" caesura="1">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="12.8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.9" punct="pt:12">b<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="13.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6" caesura="1">um</seg></w><caesura></caesura> <w n="13.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="13.8" punct="pi:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>gn<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>f<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="14.2">tr<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ff<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="14.4" punct="dp:6">m<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="dp in" caesura="1">é</seg></w> :<caesura></caesura> « <w n="14.5">C</w>’<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="14.7" punct="pe:8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe ps">oi</seg></w> !… <w n="14.8">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="14.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="14.10">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s</w> <w n="14.11" punct="pe:12">p<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>s</rhyme></w> ! »</l>
				</lg>
			</div></body></text></TEI>