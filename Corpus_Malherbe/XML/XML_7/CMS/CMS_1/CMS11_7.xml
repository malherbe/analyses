<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS11" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba cdc ddc">
				<head type="main">MASSAGE</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">AN</seg>S</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="1.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ts</w> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="1.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="1.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="C">ou</seg>s</w> <w n="1.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="1.10">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11" mp="M">ê</seg>m<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="2.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>rs</w> <w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="2.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="2.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="2.7" punct="vg:9">t<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="9" punct="vg">u</seg>s</w>, <w n="2.8"><seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg></w> <w n="2.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.10" punct="pe:12">s<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pe">œu</seg>r</rhyme></w> !</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="3.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>t</w><caesura></caesura> <w n="3.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2">tr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">Oin</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.4">hu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">am</seg>br<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="5.9" punct="pt:12">ch<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2" punct="vg:2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2" punct="vg">ain</seg>s</w>, <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="6.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="6.9" punct="pt:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>c<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></w>.</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ss<seg phoneme="?" type="va" value="1" rule="162" place="2">en</seg>t</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="Lc">an</seg>d</w>-<w n="7.4" punct="vg:6">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M/mc">o</seg>rs<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>l</w>,<caesura></caesura> <w n="7.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="7.6">l</w>’<w n="7.7" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>xt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></w>.</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ts</w> <w n="8.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="8.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="8.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="8.7" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="cdc">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="9.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="9.3" punct="vg:6">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="9.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="Lc">ou</seg>rd</w>’<w n="9.8" punct="pt:12">hu<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></rhyme></w>.</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="10.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>gu<seg phoneme="ø" type="vs" value="1" rule="403" place="4" mp="M">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="10.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ts</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="10.7" punct="pt:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rv<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w> .</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="11.4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="11.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.8">d</w>’<w n="11.9" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>tru<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" rhyme="ddc">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1" punct="vg">e</seg>l</w>, <w n="12.2">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>t</w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="12.4">br<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="12.5">n<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="12.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg></w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="12.10" punct="pt:12">bl<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">g<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8" mp="M">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="13.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="Lc">u</seg>squ</w>’<w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg>x</w> <w n="13.7" punct="pt:12">h<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">P<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="14.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ts</w> <w n="14.4" punct="tc:6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="ti" caesura="1">ain</seg>s</w> —<caesura></caesura> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="14.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="14.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="14.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="14.9">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="P">ou</seg>r</w> <w n="14.10" punct="pt:12">lu<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>