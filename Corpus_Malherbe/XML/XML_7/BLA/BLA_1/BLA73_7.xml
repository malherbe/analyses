<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA73" modus="sm" lm_max="3" metProfile="3" form="suite périodique" schema="10(aaabcccb)">
					<head type="main">AVRIL</head>
					<opener>
						<salute>A Achille Millien.</salute>
					</opener>
					<lg n="1" type="huitain" rhyme="aaabcccb">
						<l n="1" num="1.1" lm="3" met="3"><w n="1.1">M<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="1.2">d</w>’<w n="1.3" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="3" met="3"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="2.3">l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg>s</rhyme></w></l>
						<l n="3" num="1.3" lm="3" met="3"><w n="3.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2" punct="vg:3">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ch<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="3" met="3"><w n="4.1">M<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="4.2">d</w>’<w n="4.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">A</seg>vr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>l</rhyme></w>,</l>
						<l n="5" num="1.5" lm="3" met="3"><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="5.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg>s</rhyme></w></l>
						<l n="6" num="1.6" lm="3" met="3"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg>s</rhyme></w></l>
						<l n="7" num="1.7" lm="3" met="3"><w n="7.1">H<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg>s</rhyme></w></l>
						<l n="8" num="1.8" lm="3" met="3"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l</w>’<w n="8.3" punct="pv:3"><seg phoneme="e" type="vs" value="1" rule="354" place="2">e</seg>x<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pv">i</seg>l</rhyme></w> ;</l>
					</lg>
					<lg n="2" type="huitain" rhyme="aaabcccb">
						<l n="9" num="2.1" lm="3" met="3"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="9.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">tr<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="10" num="2.2" lm="3" met="3"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">l</w>’<w n="10.3" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="11" num="2.3" lm="3" met="3"><w n="11.1">Z<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>r</w> <w n="11.2">ch<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="12" num="2.4" lm="3" met="3"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2" punct="pv:3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="pv">an</seg>s</rhyme></w> ;</l>
						<l n="13" num="2.5" lm="3" met="3"><w n="13.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="14" num="2.6" lm="3" met="3"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">d<rhyme label="c" id="6" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="15" num="2.7" lm="3" met="3"><w n="15.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cl<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="16" num="2.8" lm="3" met="3"><w n="16.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="16.2" punct="pt:3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="pt">em</seg>ps</rhyme></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="aaabcccb">
						<l n="17" num="3.1" lm="3" met="3"><w n="17.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="17.2">n</w>’<w n="17.3"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>tr<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="18" num="3.2" lm="3" met="3"><w n="18.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="18.2" punct="pv:3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>ill<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="19" num="3.3" lm="3" met="3"><w n="19.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>t</w> <w n="19.2">d</w>’<w n="19.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="20" num="3.4" lm="3" met="3"><w n="20.1" punct="pt:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">Im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" punct="pt">un</seg></rhyme></w>.</l>
						<l n="21" num="3.5" lm="3" met="3"><w n="21.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">r<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="22" num="3.6" lm="3" met="3"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cl<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="23" num="3.7" lm="3" met="3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">t</w>’<w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="24" num="3.8" lm="3" met="3"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2" punct="pt:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="œ̃" type="vs" value="1" rule="268" place="3" punct="pt">um</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="huitain" rhyme="aaabcccb">
						<l n="25" num="4.1" lm="3" met="3"><w n="25.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="25.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rv<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="26" num="4.2" lm="3" met="3"><w n="26.1">Bl<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="26.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="26.3">bl<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="27" num="4.3" lm="3" met="3"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="27.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="27.3">p<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="28" num="4.4" lm="3" met="3"><w n="28.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="28.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="28.3" punct="pv:3">pl<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="pv">eu</seg>rs</rhyme></w> ;</l>
						<l n="29" num="4.5" lm="3" met="3"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2">l</w>’<w n="29.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<rhyme label="c" id="12" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="30" num="4.6" lm="3" met="3"><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="30.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>mm<rhyme label="c" id="12" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="31" num="4.7" lm="3" met="3"><w n="31.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>v<rhyme label="c" id="12" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="32" num="4.8" lm="3" met="3"><w n="32.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="32.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="32.3" punct="pt:3">fl<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="pt">eu</seg>rs</rhyme></w>.</l>
					</lg>
					<lg n="5" type="huitain" rhyme="aaabcccb">
						<l n="33" num="5.1" lm="3" met="3"><w n="33.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="33.2" punct="vg:3">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>v<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="34" num="5.2" lm="3" met="3"><w n="34.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="34.2">b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>qu<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="35" num="5.3" lm="3" met="3"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="36" num="5.4" lm="3" met="3"><w n="36.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="36.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="36.3" punct="vg:3">j<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</rhyme></w>,</l>
						<l n="37" num="5.5" lm="3" met="3"><w n="37.1" punct="vg:3">S<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="38" num="5.6" lm="3" met="3"><w n="38.1" punct="vg:3">S<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="39" num="5.7" lm="3" met="3"><w n="39.1">V<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="39.3">ch<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="40" num="5.8" lm="3" met="3"><w n="40.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="40.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="40.3" punct="pt:3">t<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="pt">ou</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="6" type="huitain" rhyme="aaabcccb">
						<l n="41" num="6.1" lm="3" met="3"><w n="41.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l</w> <w n="41.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="41.3">tr<rhyme label="a" id="16" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="42" num="6.2" lm="3" met="3"><w n="42.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w>-<w n="42.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="42.3" punct="pv:3">tr<rhyme label="a" id="16" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="43" num="6.3" lm="3" met="3"><w n="43.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="43.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c</w> <w n="43.3">s<rhyme label="a" id="16" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="44" num="6.4" lm="3" met="3"><w n="44.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="44.2" punct="pv:3">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<rhyme label="b" id="17" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="pv">oi</seg>r</rhyme></w> ;</l>
						<l n="45" num="6.5" lm="3" met="3"><w n="45.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="45.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="45.3" punct="vg:3"><rhyme label="c" id="18" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="3">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="46" num="6.6" lm="3" met="3"><w n="46.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="46.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<rhyme label="c" id="18" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="47" num="6.7" lm="3" met="3"><w n="47.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1">Y</seg></w> <w n="47.2" punct="vg:3">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<rhyme label="c" id="18" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="48" num="6.8" lm="3" met="3"><w n="48.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="48.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="48.3" punct="pt:3">v<rhyme label="b" id="17" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="pt">oi</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="7" type="huitain" rhyme="aaabcccb">
						<l n="49" num="7.1" lm="3" met="3"><w n="49.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="49.2">s</w>’<w n="49.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>cl<rhyme label="a" id="19" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="50" num="7.2" lm="3" met="3"><w n="50.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="50.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ll<rhyme label="a" id="19" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="51" num="7.3" lm="3" met="3"><w n="51.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="51.2">d<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<rhyme label="a" id="19" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="52" num="7.4" lm="3" met="3"><w n="52.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="52.2" punct="pt:3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<rhyme label="b" id="20" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="pt">er</seg></rhyme></w>.</l>
						<l n="53" num="7.5" lm="3" met="3"><w n="53.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="53.2">l</w>’<w n="53.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<rhyme label="c" id="21" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="54" num="7.6" lm="3" met="3"><w n="54.1">L</w>’<w n="54.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="54.3">t<rhyme label="c" id="21" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="55" num="7.7" lm="3" met="3"><w n="55.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="55.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="55.3">s<rhyme label="c" id="21" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="56" num="7.8" lm="3" met="3"><w n="56.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="56.2" punct="pt:3">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<rhyme label="b" id="20" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="pt">er</seg></rhyme></w>.</l>
					</lg>
					<lg n="8" type="huitain" rhyme="aaabcccb">
						<l n="57" num="8.1" lm="3" met="3"><w n="57.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="57.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="57.3">c<rhyme label="a" id="22" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="58" num="8.2" lm="3" met="3"><w n="58.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="58.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="58.3" punct="vg:3">pr<rhyme label="a" id="22" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="59" num="8.3" lm="3" met="3"><w n="59.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ts</w> <w n="59.2">d</w>’<w n="59.3" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<rhyme label="a" id="22" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="60" num="8.4" lm="3" met="3"><w n="60.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="60.2">d</w>’<w n="60.3" punct="pv:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>di<rhyme label="b" id="23" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="pv">eu</seg></rhyme></w> ;</l>
						<l n="61" num="8.5" lm="3" met="3"><w n="61.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="61.2">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><rhyme label="c" id="24" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="62" num="8.6" lm="3" met="3"><w n="62.1">S<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<rhyme label="c" id="24" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="63" num="8.7" lm="3" met="3"><w n="63.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="63.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="63.3">t<rhyme label="c" id="24" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="64" num="8.8" lm="3" met="3"><w n="64.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="64.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="64.3" punct="pt:3">Di<rhyme label="b" id="23" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="pt">eu</seg></rhyme></w>.</l>
					</lg>
					<lg n="9" type="huitain" rhyme="aaabcccb">
						<l n="65" num="9.1" lm="3" met="3"><w n="65.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="65.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="65.3" punct="vg:3">m<rhyme label="a" id="25" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="66" num="9.2" lm="3" met="3"><w n="66.1" punct="vg:2">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="66.2" punct="vg:3"><rhyme label="a" id="25" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="67" num="9.3" lm="3" met="3"><w n="67.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="67.2">qu</w>’<w n="67.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<rhyme label="a" id="25" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="68" num="9.4" lm="3" met="3"><w n="68.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="68.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="68.3" punct="vg:3">mi<rhyme label="b" id="26" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="3" punct="vg">e</seg>l</rhyme></w>,</l>
						<l n="69" num="9.5" lm="3" met="3"><w n="69.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="69.2" punct="vg:3">br<rhyme label="c" id="27" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="70" num="9.6" lm="3" met="3"><w n="70.1">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="70.2" punct="vg:3">gr<rhyme label="c" id="27" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="71" num="9.7" lm="3" met="3"><w n="71.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="71.2" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>gl<rhyme label="c" id="27" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="72" num="9.8" lm="3" met="3"><w n="72.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="72.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="72.3" punct="vg:3">ci<rhyme label="b" id="26" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="3" punct="vg">e</seg>l</rhyme></w>,</l>
					</lg>
					<lg n="10" type="huitain" rhyme="aaabcccb">
						<l n="73" num="10.1" lm="3" met="3"><w n="73.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="73.2" punct="vg:3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<rhyme label="a" id="28" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></w>,</l>
						<l n="74" num="10.2" lm="3" met="3"><w n="74.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="74.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<rhyme label="a" id="28" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="75" num="10.3" lm="3" met="3"><w n="75.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="75.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<rhyme label="a" id="28" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></w></l>
						<l n="76" num="10.4" lm="3" met="3"><w n="76.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="76.2" punct="pt:3">b<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nh<rhyme label="b" id="29" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="pt">eu</seg>r</rhyme></w>.</l>
						<l n="77" num="10.5" lm="3" met="3"><w n="77.1">H<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>n<rhyme label="c" id="30" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg>s</rhyme></w></l>
						<l n="78" num="10.6" lm="3" met="3"><w n="78.1" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<rhyme label="c" id="30" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="79" num="10.7" lm="3" met="3"><w n="79.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>x</w> <w n="79.2">b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<rhyme label="c" id="30" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg>s</rhyme></w></l>
						<l n="80" num="10.8" lm="3" met="3"><w n="80.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="80.2" punct="pe:3">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<rhyme label="b" id="29" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="pe ps">eu</seg>r</rhyme></w> !…</l>
					</lg>
				</div></body></text></TEI>