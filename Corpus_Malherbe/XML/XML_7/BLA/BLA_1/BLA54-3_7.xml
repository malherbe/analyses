<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" insert="3" modus="pi" key="BLA54" form="suite périodique" schema="3(aabccb)"><div id_part="3">
									<lg n="1" type="sizain" rhyme="aabccb">
										<l n="297" n_ins="1" num_ins="1.1" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="297.1" punct="vg:2">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2" punct="vg">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="297.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="297.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="297.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="297.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="297.6" punct="vg:8">r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</hi></l>
										<l n="298" n_ins="2" num_ins="1.2" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="298.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="298.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="298.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="298.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="298.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></hi></l>
										<l n="299" n_ins="3" num_ins="1.3" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="299.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="299.2">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="299.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="299.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rpr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="299.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="299.6" punct="pt:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pt">e</seg>il</rhyme></w>.</hi></l>
										<l n="300" n_ins="4" num_ins="1.4" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="300.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="300.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>t</w> <w n="300.3" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg></w>, <w n="300.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="300.5" punct="vg:8">v<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>pr<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</hi></l>
										<l n="301" n_ins="5" num_ins="1.5" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="301.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="301.2">pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="301.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="301.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="301.5">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="301.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rpr<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></hi></l>
										<l n="302" n_ins="6" num_ins="1.6" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="302.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="302.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="302.3">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>t</w> <w n="302.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="302.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="302.6" punct="pi:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pi">e</seg>il</rhyme></w> ?</hi></l>
									</lg>
								</div><div id_part="4">
									<lg n="1" type="sizain" rhyme="aabccb">
										<l n="305" n_ins="9" num_ins="1.1" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="305.1" punct="pe:1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg>s</w> ! <w n="305.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="305.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="305.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="305.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="305.6">d</w>’<w n="305.7" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</hi></l>
										<l n="306" n_ins="10" num_ins="1.2" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="306.1" punct="vg:2">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2" punct="vg">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="306.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="306.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="306.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="306.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="306.6" punct="pt:8">pl<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</hi></l>
										<l n="307" n_ins="11" num_ins="1.3" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="307.1" punct="pe:1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg>s</w> ! <w n="307.2" punct="pe:2">L<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="307.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="307.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="307.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="307.6" punct="pt:8">ch<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></w>.</hi></l>
										<l n="308" n_ins="12" num_ins="1.4" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="308.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="308.2">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3">en</seg>t</w> <w n="308.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="308.4" punct="pe:8">n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</hi></l>
										<l n="309" n_ins="13" num_ins="1.5" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="309.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="309.2">qu</w>’<w n="309.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="309.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="309.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="309.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="309.7">d<rhyme label="c" id="6" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></hi></l>
										<l n="310" n_ins="14" num_ins="1.6" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="310.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="310.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="310.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="310.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="310.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="310.6" punct="pe:8">s<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>r</rhyme></w> !</hi></l>
									</lg>
								</div><div id_part="5">
									<lg n="1" type="sizain" rhyme="aabccb">
										<l n="313" n_ins="17" num_ins="1.1" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="313.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>c</w> <w n="313.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="313.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="313.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="313.5" punct="pt:6">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt">ez</seg></w>. <w n="313.6" punct="vg:8">M<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</hi></l>
										<l n="314" n_ins="18" num_ins="1.2" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="314.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="314.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="314.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="314.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="314.5">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></hi></l>
										<l n="315" n_ins="19" num_ins="1.3" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="315.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="315.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="315.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="315.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="315.5" punct="pt:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</hi></l>
										<l n="316" n_ins="20" num_ins="1.4" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="316.1" punct="vg:2">C<seg phoneme="œ" type="vs" value="1" rule="345" place="1">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="316.2">c<seg phoneme="œ" type="vs" value="1" rule="345" place="3">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="316.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="316.4" punct="pv:8">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</hi></l>
										<l n="317" n_ins="21" num_ins="1.5" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="317.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="317.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="317.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="317.4" punct="vg:5">r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="317.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="317.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></hi></l>
										<l n="318" n_ins="22" num_ins="1.6" lm="8" met="8"><space unit="char" quantity="12"></space><hi rend="ital"><w n="318.1">F<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="318.2">f<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="318.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="318.4" punct="pe:8">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</hi></l>
									</lg>
								</div></div></body></text></TEI>