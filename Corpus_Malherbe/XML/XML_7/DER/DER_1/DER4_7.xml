<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER4" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="9(abab)">
				<head type="number">IV</head>
				<opener>
					<salute>A Michel Puy.</salute>
				</opener>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">s<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">qu</w>’<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="2.5">r<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="2.8" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.2">m</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="3.4" punct="vg:4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></w>, <w n="3.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.7">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="4.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="4.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="4.7" punct="vg:8">c<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>r</rhyme></w>,</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rs</w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="5.5">p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="6.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="6.3" punct="vg:4">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" punct="vg">en</seg>t</w>, <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="6.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd</w> <w n="6.6" punct="vg:8">p<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>r</rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="7.3" punct="vg:4">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>nt</w>, <w n="7.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="7.5" punct="vg:8">m<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3">t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="8.5">l</w>’<w n="8.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">v<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="9.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5">s</w>’<w n="9.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="10.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="10.5">d</w>’<w n="10.6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="10.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.8">j</w>’<w n="10.9" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cr<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="11.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="11.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="11.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.7" punct="pt:8">f<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="12.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="12.6" punct="pt:8">cr<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="13.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>gs</w> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.6" punct="vg:8"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">cl<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="14.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="14.5">fr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="15.3">cr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.5">p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="16.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="16.6" punct="pt:8">p<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="17.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="17.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="17.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.5" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="18.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="18.3">t<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="18.4" punct="vg:8">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</rhyme></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">D</w>’<w n="19.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="19.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="19.4">qu</w>’<w n="19.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="19.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="19.7" punct="vg:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>c<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg></w>, <w n="20.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.3">t</w>’<w n="20.4" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="21.3">d<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="21.4"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="21.5" punct="vg:8">st<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>x</w> <w n="22.2" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="354" place="2">e</seg>x<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.4" punct="vg:8">T<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></rhyme></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">H<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>lb<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rg</w> <w n="23.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.5" punct="vg:8">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>st<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="24.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="24.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="24.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.6" punct="pt:8">tr<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pt">ain</seg></rhyme></w>.</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="25.5" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<rhyme label="a" id="12" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="26.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="26.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="26.5" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="13" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="27.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="27.3" punct="vg:4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="27.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="27.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="27.6" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<rhyme label="a" id="12" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="28.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="28.3">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.5" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="b" id="13" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w>-<w n="29.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="29.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="29.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="29.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>il</w> <w n="29.7"><rhyme label="a" id="14" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w>-<w n="30.2">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3">e</seg></w> <w n="30.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="30.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="30.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="30.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<rhyme label="b" id="15" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></w></l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="31.2">l<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="31.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="31.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="31.5" punct="pt:8">f<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>ill<rhyme label="a" id="14" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="32.3">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="3">oi</seg></w> <w n="32.4" punct="pi:4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pi">on</seg></w> ? <w n="32.5">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="32.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="32.7">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="7">oi</seg></w> <w n="32.8" punct="pi:8">b<rhyme label="b" id="15" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pi">on</seg></rhyme></w> ?</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="8" met="8"><w n="33.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="33.2">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="415" place="4" punct="vg">ô</seg>t</w>, <w n="33.4" punct="vg:6">j<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>t</w>, <w n="33.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="33.6">fr<rhyme label="a" id="16" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="34" num="9.2" lm="8" met="8"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="34.2">s</w>’<w n="34.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="34.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="34.5">l</w>’<w n="34.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="34.8" punct="vg:8">s<rhyme label="b" id="17" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="35" num="9.3" lm="8" met="8"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">j</w>’<w n="35.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="35.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="35.5">f<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="35.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="35.7">p<rhyme label="a" id="16" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="36" num="9.4" lm="8" met="8"><w n="36.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="36.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>fl<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="36.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="36.4" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<rhyme label="b" id="17" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>