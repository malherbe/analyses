<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER85" modus="cm" lm_max="12" metProfile="6=6" form="suite de distiques" schema="9((aa))">
				<head type="number">LXXXV</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" mp6="M" mp4="P" met="8+4"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>str<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="M">u</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" caesura="1">on</seg></w><caesura></caesura> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="1.7">b<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>gr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="2.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rs<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="2.5">qu<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M/mc">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fm">e</seg></w>-<w n="2.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="2.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>gr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>f<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" mp6="F" met="4+4+4"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="3.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.4">br<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.7">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8" caesura="2">i</seg>t</w><caesura></caesura> <w n="3.8">j</w>’<w n="3.9"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="3.10" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11" mp="M">in</seg>cl<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">c</w>’<w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.6" punct="vg:6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">em</seg>ps</w>,<caesura></caesura> <w n="4.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="4.10">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10" mp="M">i</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s</w> <w n="4.11">pl<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s</rhyme></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">qu</w>’<w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="5.6">p<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>t</w> <w n="5.7">d</w>’<w n="5.8" punct="pt:12">h<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>tr<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>h<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="6.4">h<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><seg phoneme="i" type="vs" value="1" rule="477" place="10">ï</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <hi rend="ital"><w n="6.5" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">An</seg>thr<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w></hi>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="7.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="7.3">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>tch<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.5">C<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rc<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="12" mp6="Pem" met="6−6"><subst hand="RR" reason="analysis" type="phonemization"><del>49</del><add rend="hidden"><w n="8.1" punct="vg:4">qu<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.2">n<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>f</w></add></subst>, <w n="8.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem" caesura="1">e</seg></w><caesura></caesura> <w n="8.5" punct="vg:8">B<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rb<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>, <w n="8.6" punct="pt:10">P<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>s</w>. <w n="8.7">J<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="Lc">u</seg>squ</w>’<w n="8.8"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="12" mp="C">au</seg></rhyme></w></l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg></w>, <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="9.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.5">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="9.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.8" punct="pt:12">g<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="10.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <hi rend="ital"><w n="10.3">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>cl<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w></hi><caesura></caesura> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <hi rend="ital"><w n="10.5" punct="pv:12">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.6">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="10.8">F<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w></hi> ;</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="11.3">L<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rgu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>c</w> <w n="11.7"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rt</rhyme></w></l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="M">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="12.3">B<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="12.5">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="10">ean</seg></w> <w n="12.6" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">Ai</seg>c<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>rd</rhyme></w>.</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="13.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd</w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="13.5">V<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>r</w><caesura></caesura> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="13.9" punct="vg:12">r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1" punct="vg:2">Fr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.4" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rb<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="14.5">m<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="14.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="14.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.8" punct="vg:12">S<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2" punct="vg:3">D<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="M">eu</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3" punct="vg">e</seg>l</w>, <w n="15.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="15.5">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="15.6" punct="vg:12">cr<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1">P<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <hi rend="ital"><w n="16.3">B<seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w></hi><caesura></caesura> <w n="16.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <hi rend="ital"><w n="16.5" punct="vg:12">P<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="16.6">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>s</rhyme></w></hi>,</l>
					<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" mp="M">aî</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="17.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>rs</w><caesura></caesura> <w n="17.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="17.6">t<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="17.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="17.8" punct="pt:12">cu<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" type="distiques" rhyme="aa…">
					<l n="18" num="2.1" lm="12" met="6+6"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">j</w>’<w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="18.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="4" mp="M">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p</w> <w n="18.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg>s</w><caesura></caesura> <w n="18.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="18.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="18.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="18.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="18.10" punct="pt:12">l<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>