<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER105" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="6((aa))">
				<head type="number">CV</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">d</w>’<w n="1.3">h<seg phoneme="o" type="vs" value="1" rule="415" place="3" mp="M">ô</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="1.5">fl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.9" punct="vg:12">b<seg phoneme="ɛ̃" type="vs" value="1" rule="238" place="11" mp="M">en</seg>z<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="2.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rt</w><caesura></caesura> <w n="2.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.8">pl<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="3.8" punct="pt:12">n<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="4.2">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="4.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="5.4">ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>t</w><caesura></caesura> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.7">f<seg phoneme="œ" type="vs" value="1" rule="406" place="8" mp="M">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="5.9" punct="vg:12">g<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="6.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r</w> <w n="6.8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="6.9"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>s</w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="7.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="7.10" punct="pt:12">p<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="8.2" punct="pv:2">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pv">o</seg>rs</w> ; <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="8.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="8.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>t</w><caesura></caesura> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.8">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.9">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>f<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1" punct="pv:3">D<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3" punct="pv">en</seg>t</w> ; <w n="9.2">d</w>’<w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="9.4">r<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg></w><caesura></caesura> <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.8">j<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="426" place="12">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">j</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="Lc">à</seg></w>-<w n="10.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="10.7">m<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="10.9">j<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="12">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.4" punct="vg:6">gr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="11.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.8" punct="vg:12">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rm<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="12" num="1.12" lm="12" met="6+6"><hi rend="ital"><w n="12.1">Gu<seg phoneme="i" type="vs" value="1" rule="485" place="1" mp="M">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="12.2" punct="vg:4">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>ll</w>, <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.4">B<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="12.5">D<seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w></hi> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <hi rend="ital"><w n="12.7" punct="pt:12">S<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="Lc">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>-<w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11" mp="Lc">e</seg>t</w>-<w n="12.9">M<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w></hi>.</l>
				</lg>
			</div></body></text></TEI>