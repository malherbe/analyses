<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" rhyme="none" key="DER140" modus="cm" lm_max="12" metProfile="6−6">
				<head type="number">CXL</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">d</w>’<w n="1.3">h<seg phoneme="o" type="vs" value="1" rule="415" place="3" mp="M">ô</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="1.4">m<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="1.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">Un</seg></w> <w n="1.8"><seg phoneme="œ" type="vs" value="1" rule="286" place="10" mp="M">œ</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190" place="11">e</seg>t</w> <w n="1.9">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="2.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.6"><seg phoneme="u" type="vs" value="1" rule="426" place="8">où</seg></w> <w n="2.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="2.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s</w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="3.2">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3" punct="pt:4">n<seg phoneme="y" type="vs" value="1" rule="457" place="4" punct="pt">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>. <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="315" place="5">EAU</seg></w> <w n="3.5" punct="pt:6">CH<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="pt" caesura="1">AU</seg>D<seg phoneme="ə" type="ee" value="0" rule="e-23">E</seg></w>.<caesura></caesura> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="315" place="7">EAU</seg></w> <w n="3.7" punct="pt:9">FR<seg phoneme="wa" type="vs" value="1" rule="420" place="8">OI</seg>D<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="pt" mp="F">E</seg></w>. <hi rend="ital"><subst hand="RR" reason="analysis" type="phonemization"><del>MM</del><add rend="hidden"><w n="3.8">m<seg phoneme="e" type="vs" value="1" rule="353" place="10" mp="M">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="397" place="11">eu</seg>rs</w></add></subst> <w n="3.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="12">e</seg>s</w></hi></l>
					<l n="4" num="1.4" lm="12" mp6="Pem" met="6−6"><hi rend="ital"><w n="4.1">Cl<seg phoneme="i" type="vs" value="1" rule="d-1" place="1" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="271" place="2">en</seg>ts</w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="4.3">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem" caesura="1">e</seg></w><caesura></caesura> <w n="4.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>gl<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="4.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.7" punct="pt:12">d<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</hi></l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3" punct="pt:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pt" mp="F">e</seg></w>. <w n="5.4">R<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>gl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="5.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="5.9" punct="pt:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pt">œu</seg>r</w>.</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="6.2">j<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.4" punct="vg:6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>rs</w>,<caesura></caesura> <w n="6.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="6.6">f<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.7" punct="pe:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pe">o</seg>r</w> !</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1" lm="12" met="6+6"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="7.2">n</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="2">e</seg>s</w> <w n="7.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="7.5" punct="pt:4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="pt">à</seg></w>. <w n="7.6">J</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="7.8">l<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="7.9">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.10"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.11">l</w>’<hi rend="ital"><w n="7.12"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">A</seg>nn<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></hi></l>
					<l n="8" num="3.2" lm="12" mp6="P" met="6−6"><hi rend="ital"><w n="8.1" punct="vg:5">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="8.2">T<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ph<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg" mp="F">e</seg>s</w></hi>, <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P" caesura="1">ou</seg>r</w><caesura></caesura> <w n="8.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="8.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="12">e</seg>s</w></l>
					<l n="9" num="3.3" lm="12" met="6+6"><w n="9.1" punct="pv:2">S<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="2" punct="pv">o</seg>ts</w> ; <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="9.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="9.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>s</w></l>
					<l n="10" num="3.4" lm="12" met="6+6"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2" punct="vg:3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="10.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="10.5" punct="vg:6">cl<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>s</w>,<caesura></caesura> <w n="10.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="7">o</seg>p</w> <w n="10.7">h<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="10.9">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="10.10" punct="vg:12">nu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="11" num="4.1" lm="12" met="6+6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="11.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="y" type="vs" value="1" rule="d-3" place="3" mp="M">u</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.4">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6" caesura="1">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="11.6">li<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="11.8">l</w>’<w n="11.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r</w></l>
					<l n="12" num="4.2" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">â</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="12.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="12.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="12.7">fru<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="12.8">tr<seg phoneme="o" type="vs" value="1" rule="433" place="10">o</seg>p</w> <w n="12.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>r</w>.</l>
				</lg>
			</div></body></text></TEI>