<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER37" modus="cm" lm_max="12" metProfile="6−6" form="suite de distiques" schema="9((aa))">
				<head type="number">XXXVII</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="1.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366" place="9">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.6" punct="vg:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>r</w> <w n="2.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="353" place="10" mp="M">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="3.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="3.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="3.6" punct="vg:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="4.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="4.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.8">s</w>’<w n="4.9" punct="pt:12"><seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>n<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>r</rhyme></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="5.4" punct="vg:6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="5.5">l<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</w> <w n="5.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="5.8" punct="vg:12">pl<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" mp6="C" met="6−6"><w n="6.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C" caesura="1">e</seg>s</w><caesura></caesura> <w n="6.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>rs</w> <w n="6.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="6.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="6.8" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="445" place="12">û</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="7.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="7.4">p<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>tr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="7.7">tr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rf<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ̃" type="vs" value="1" rule="268" place="12">um</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.8" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>f<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="12" punct="pt">un</seg>t</rhyme></w>.</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">l</w>’<w n="9.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="9.6">j<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.9">b<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="9.10" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3" punct="vg:4">l<seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="M">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="10.5">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="6" caesura="1">eu</seg>s</w><caesura></caesura> <w n="10.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rs</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="10.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="10.9" punct="vg:12">j<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg>n<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3" punct="ps:4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="ps">ai</seg>s</w>… <w n="11.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="11.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="11.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></rhyme></w></l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="2" mp="M">ue</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="12.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.5" punct="pe:9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="pe ti" mp="F">e</seg>s</w> ! — <w n="12.6">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="12.7">j<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rd<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></rhyme></w></l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>d</w><caesura></caesura> <w n="13.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="13.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="13.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ll<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="14.3">r<seg phoneme="u" type="vs" value="1" rule="428" place="3">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="14.4">s</w>’<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="14.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="14.8" punct="vg:12">c<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>l<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe ti">a</seg>s</w> ! — <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">E</seg>t</w> <w n="15.3">j</w>’<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" caesura="1">en</seg>s</w><caesura></caesura> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="15.7" punct="vg:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="16.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="16.3" punct="vg:3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>rs</w>, <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="16.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="16.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>ts</w><caesura></caesura> <w n="16.7">qu</w>’<w n="16.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="16.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="16.10">b<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg>x</w> <w n="16.11" punct="vg:12">y<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="17.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="17.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="17.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="17.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="17.6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="17.7">m<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="18" num="1.18" lm="12" met="6+6"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="18.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</w> <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="18.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="18.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="18.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="10">um</seg></w> <w n="18.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="18.9" punct="pt:12">r<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>