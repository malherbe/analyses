<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER113" modus="cm" lm_max="12" metProfile="6−6" form="suite de distiques" schema="8((aa))">
				<head type="number">CXIII</head>
				<opener>
					<salute>Pour Distraire Mon Ami Le Poète Léon Vérane.</salute>
				</opener>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="1.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.5">pl<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>t</w><caesura></caesura> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="2.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="2.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.10" punct="vg:12">pl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2" punct="vg:3">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="3.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.9">gl<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3" punct="vg:4">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="4.4">j</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="4.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="4.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="4.8">b<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>t</w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.10">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="5.3" punct="vg:4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" punct="vg">e</seg></w>, <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="5.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="5.6" punct="vg:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>fs</w>, <w n="5.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="5.8">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="5.9" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>c<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="345" place="12">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="6.3" punct="vg:4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="6.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="6.6">gl<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.9">f<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="12">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="7" num="1.7" lm="12" mp6="P" met="6−6"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P" caesura="1">an</seg>s</w><caesura></caesura> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="7.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>g<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.3">ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="8.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="8.8">b<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>g<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="9.4">gl<seg phoneme="i" type="vs" value="1" rule="493" place="5" mp="M">y</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.7">r<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="9.8">bl<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>f<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3" punct="pt:5">t<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pt" mp="F">e</seg></w>. <w n="10.4">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ds</w><caesura></caesura> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.6" punct="pt:9">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="pt" mp="F">e</seg>s</w>. <w n="10.7">T<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="10.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>g<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="11.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.7" punct="pv:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ss<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="12.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="4" mp="C">o</seg>s</w> <w n="12.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="12.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="12.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="12.10">p<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">j</w>’<w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="13.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="13.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="13.8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="13.9" punct="pt:12">bl<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="14.2">qu</w>’<w n="14.3" punct="pe:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe" mp="F">e</seg></w> ! <w n="14.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="14.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" mp="P">e</seg>rs</w> <w n="14.7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="14.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="14.9">t<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="14.10">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="14.11" punct="vg:12">p<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="15.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="15.4">l</w>’<w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" caesura="1">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="15.8">f<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="15.9" punct="vg:12">r<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>g<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="16.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>r</w> <w n="16.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="16.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="16.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="16.7">s<seg phoneme="u" type="vs" value="1" rule="428" place="8">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="16.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="16.9" punct="pt:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>g<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>