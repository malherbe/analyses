<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU33" modus="sm" lm_max="8" metProfile="8" form="suite périodique avec alternance de type 1" schema="2{2(abab) 1(abba)}">
					<head type="number">XXXII</head>
					<head type="main">Le Vampire</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="1.2" punct="vg:2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="1.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="1.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.7" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="2.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="2.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>f</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="2.6" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="3.2" punct="vg:2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="3.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="3.6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2" punct="vg:3">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="4.3" punct="vg:4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg>s</w>, <w n="4.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="5.4">h<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="6.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.6" punct="pv:8">d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="7" num="2.3" lm="8" met="8">— <w n="7.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>f<seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="7.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="7.6">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rç<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.6" punct="vg:8">ch<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="9.3">j<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="9.6" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.5">l</w>’<w n="10.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="11.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.5" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="8" met="8">— <w n="12.1" punct="vg:3">M<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="12.2">m<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w>-<w n="12.4" punct="pe:8">t<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg></rhyme></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">J</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="13.3">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5">gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="14.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.4" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">j</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="15.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="15.6">p<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="15.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rf<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="16.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="16.4" punct="pt:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="17.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.6">gl<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">M</w>’<w n="18.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="18.3">pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="18.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="18.7">m</w>’<w n="18.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="18.9" punct="dp:8">d<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp">i</seg>t</rhyme></w> :</l>
						<l n="19" num="5.3" lm="8" met="8">« <w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="19.2">n</w>’<w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="2">e</seg>s</w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="19.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.6">qu</w>’<w n="19.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="19.8">t</w>’<w n="19.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>l<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="20.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.4" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>d<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>t</rhyme></w>,</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abba">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1" punct="pe:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">Im</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe ti">e</seg></w> ! — <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>p<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="22.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rts</w> <w n="22.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="vg">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</rhyme></w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="23.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="23.3">r<seg phoneme="e" type="vs" value="1" rule="213" place="4">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>sc<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</rhyme></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="24.5" punct="pe:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> ! »</l>
					</lg>
				</div></body></text></TEI>