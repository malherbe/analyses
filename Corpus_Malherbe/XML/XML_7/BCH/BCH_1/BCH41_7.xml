<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH41" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="4(abaab)">
					<head type="number">XL</head>
					<lg n="1" type="quintil" rhyme="abaab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.6">r<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>rc<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>s</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="2.6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</w> <w n="2.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="2.9" punct="vg:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rb<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="3.2">j</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.4">ch<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="P">ez</seg></w> <w n="3.5" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="3.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="3.8">b<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.9">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="4.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="4.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="4.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.7"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="4.8">d</w>’<w n="4.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">am</seg>br<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="5.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="5.5" punct="vg:6">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>ds</w>,<caesura></caesura> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.8"><seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="C">i</seg>ls</w> <w n="5.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>nt</w> <w n="5.10" punct="pe:12">b<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg></rhyme></w> !</l>
					</lg>
					<lg n="2" type="quintil" rhyme="abaab">
						<l n="6" num="2.1" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3">pl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="6.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="6.6">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rs</w> <w n="6.8">p<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="6.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="6.10" punct="vg:12">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="7" num="2.2" lm="12" met="6+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">m</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="2" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.9" punct="dp:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="dp">eu</seg>r</rhyme></w> :</l>
						<l n="8" num="2.3" lm="12" met="6+6"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="8.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="8.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.7">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="8.8" punct="pe:12">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="9" num="2.4" lm="12" met="6+6"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="9.3">s</w>’<w n="9.4"><seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="9.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="9.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="9.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="9.9" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="10" num="2.5" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="10.9" punct="pt:12">fl<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quintil" rhyme="abaab">
						<l n="11" num="3.1" lm="12" met="6+6"><w n="11.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="11.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="11.3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l</w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="11.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.6">j<seg phoneme="u" type="vs" value="1" rule="426" place="6" caesura="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="11.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="11.8"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="11.9">gr<seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.10">mi<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="12" num="3.2" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="12.3">n</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="12.9" punct="pv:12">v<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pv">ain</seg></rhyme></w> ;</l>
						<l n="13" num="3.3" lm="12" met="6+6"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="13.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="13.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="13.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.10" punct="vg:12">fi<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="14" num="3.4" lm="12" met="6+6"><w n="14.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="14.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>gts</w> <w n="14.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">j</w>’<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="14.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="14.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.8">p<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="14.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="14.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="14.11">l<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="15" num="3.5" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">c</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="15.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="15.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="15.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="15.8">n</w>’<w n="15.9"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="15.10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="15.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="15.12" punct="pt:12">f<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="quintil" rhyme="abaab">
						<l n="16" num="4.1" lm="12" met="6+6"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="16.2">m<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="16.3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>c<seg phoneme="e" type="vs" value="1" rule="353" place="9" mp="M">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="16.7" punct="vg:12">plu<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="17" num="4.2" lm="12" met="6+6"><w n="17.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="17.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="17.3" punct="vg:4">fl<seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="vg">o</seg>ts</w>, <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="17.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="17.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="17.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="17.9" punct="vg:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>p<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</rhyme></w>,</l>
						<l n="18" num="4.3" lm="12" met="6+6"><w n="18.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="18.3">b<seg phoneme="a" type="vs" value="1" rule="307" place="4" mp="M">â</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="18.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="18.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="18.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="18.7">s</w>’<w n="18.8" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="11" mp="M">en</seg>nu<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="19" num="4.4" lm="12" met="6+6"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="19.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="19.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="19.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="19.7">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.8"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="11" mp="M">ou</seg><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="20" num="4.5" lm="12" met="6+6"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="20.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="20.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="20.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="20.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="20.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="20.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="20.9">p<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="20.10" punct="pt:12">c<seg phoneme="œ" type="vs" value="1" rule="345" place="11" mp="M">ue</seg>ill<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>r</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>