<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH93" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="3(ababa)">
					<head type="number">XXXIX</head>
					<lg n="1" type="quintil" rhyme="ababa">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="1.6">m</w>’<w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="1.9" punct="vg:8">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="5">u</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.8" punct="vg:8">pl<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="3.4">g<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w>-<w n="4.2" punct="vg:2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>s</w>, <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.6" punct="ps:8">pl<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></w>…</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="5.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="5.6">m</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="5.9" punct="pt:8">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="quintil" rhyme="ababa">
						<l n="6" num="2.1" lm="8" met="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.4">br<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.5" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="7" num="2.2" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="7.5" punct="pv:8">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>qu<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pv">eu</seg>r</rhyme></w> ;</l>
						<l n="8" num="2.3" lm="8" met="8"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="8.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">l<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="9" num="2.4" lm="8" met="8"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="9.3" punct="vg:4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg>x</w>, <w n="9.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="9.6" punct="pe:8">c<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pe">œu</seg>r</rhyme></w> !</l>
						<l n="10" num="2.5" lm="8" met="8"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="5">u</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.8" punct="vg:8">pl<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="3" type="quintil" rhyme="ababa">
						<l n="11" num="3.1" lm="8" met="8"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="11.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l</w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.6">m</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="11.9" punct="pv:8">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg></rhyme></w> ;</l>
						<l n="12" num="3.2" lm="8" met="8"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4">p<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.5">fl<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="13" num="3.3" lm="8" met="8"><w n="13.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="13.6" punct="pe:8">t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg></rhyme></w> !</l>
						<l n="14" num="3.4" lm="8" met="8"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="14.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="14.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="14.9" punct="vg:8">cr<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="15" num="3.5" lm="8" met="8"><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="15.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="15.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="15.6">m</w>’<w n="15.7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="15.9" punct="pt:8">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>