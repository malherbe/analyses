<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH13" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(abba)">
					<head type="number">XII</head>
					<head type="main">MADRIGAL</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">r<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.4">r<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.6" punct="vg:8">gr<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>ds</rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="2.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="2.3">qu</w>’<w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>t</w> <w n="2.6">s</w>’<w n="2.7"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="2.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="2.10">l</w>’<w n="2.11" punct="vg:8"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="3.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="3.4">n</w>’<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="3.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>pl<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.5" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>ts</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="5.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="5.7">d<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="6.3" punct="vg:5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="6.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="6.6">p<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>t</rhyme></w></l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="7.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4" punct="vg:5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>d</w>, <w n="7.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.8" punct="vg:8">v<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>t</rhyme></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.4">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="8.5" punct="pv:8">t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w>-<w n="9.2" punct="vg:2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2" punct="vg">au</seg>t</w>, <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="9.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="10.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="10.3">l</w>’<w n="10.4"><seg phoneme="œ" type="vs" value="1" rule="286" place="3">œ</seg>il</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="10.6">s</w>’<w n="10.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>t</w> <w n="11.2" punct="pv:5"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pv">e</seg></w> ; <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.5">r<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="12.7" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>st<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2" punct="pe:3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe">e</seg></w> ! <w n="13.3">c</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="14.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="14.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">l</w>’<w n="14.6" punct="vg:8"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>rs</rhyme></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.5">d</w>’<w n="15.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="15.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="15.8">v<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</rhyme></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="16.4" punct="pt:8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="133" place="1">E</seg>h</w> <w n="17.2" punct="pe:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2" punct="pe">en</seg></w> ! <w n="17.3">l</w>’<w n="17.4"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="17.6">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="18.4" punct="pv:8">g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2">n</w>’<w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.5">ch<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="19.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="20.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="20.6" punct="pt:8">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>t<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>