<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL55" modus="sp" lm_max="8" metProfile="8, 6" form="strophe unique" schema="1(ababab)">
				<head type="main">INVOCATION AUX PARQUES.</head>
				<head type="tune">Air : Noté, N°. 34.</head>
				<lg n="1" type="sizain" rhyme="ababab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">E</seg></w> <w n="1.2" punct="vg:3">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="1.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6" punct="pv:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="pv">ai</seg></rhyme></w> ;</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="2.4" punct="dp:6">S<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>lv<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></w> :</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="3.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="3.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>s</w> <w n="3.6">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.5" punct="vg:6">v<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="5.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="5.5" punct="vg:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">ez</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="4"></space><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2" punct="vg:2">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2" punct="vg">en</seg></w>, <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="6.6" punct="pt:6">pr<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="469" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>