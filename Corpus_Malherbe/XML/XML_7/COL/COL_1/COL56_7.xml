<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL56" modus="sp" lm_max="8" metProfile="8, 3, (6), (7), (5)" form="suite de strophes" schema="1[abba] 1[abbaba]">
				<head type="main">L’ESCALADE.</head>
				<head type="tune">Air : Toute la terre est à moi.</head>
				<head type="tune">Noté, N°. 35.</head>
				<lg n="1" type="regexp" rhyme="abbaabbaba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>M<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">OU</seg>R</w>, <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="2.2">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="2.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>squ</w>’<w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="2.5" punct="pv:8">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="pv">ou</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2">s</w>’<w n="3.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>t</w>, <w n="3.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.6" punct="vg:8">f<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">ou</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="4.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">I</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="4.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>b<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="6"><space unit="char" quantity="4"></space><w n="5.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="5.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="5.4" punct="dp:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="dp">oi</seg></rhyme></w> :</l>
					<l n="6" num="1.6" lm="7"><space unit="char" quantity="2"></space><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">cr<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="6.6" punct="vg:7">B<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="7" num="1.7" lm="5"><space unit="char" quantity="6"></space><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.4">B<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="3" met="3"><space unit="char" quantity="10"></space><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="8.3" punct="vg:3">m<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="5"><space unit="char" quantity="6"></space><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.4">B<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
					<l n="10" num="1.10" lm="3" met="3"><space unit="char" quantity="10"></space><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="10.3" punct="pt:3">m<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pt">oi</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>