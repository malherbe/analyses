<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL12" modus="cp" lm_max="9" metProfile="8, 6, 3+6" form="suite périodique" schema="3(ababab)">
				<head type="main">CHANSON</head>
				<head type="tune">A l’encontre d’un Talon rouge qui avoit perdu <lb></lb>le respect à une Financiere.</head>
				<head type="sub_1">C’EST LA FEMME QUI PARLE.</head>
				<head type="tune">Air : Hélas ! tant la nuit que le jour; <lb></lb>ou <lb></lb>Je suis, je suis malade d’Amour.</head>
				<head type="tune">Noté, N°. 11.</head>
				<lg n="1" type="sizain" rhyme="ababab">
					<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="2"></space><w n="1.1">H<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">I</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">E</seg>R</w> <w n="1.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="1.4">m</w>’<w n="1.5" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>v<seg phoneme="e" type="vs" value="1" rule="383" place="7">e</seg>ill<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pv">an</seg>t</rhyme></w> ;</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="6"></space>(<w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="2.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="2.5" punct="pf:6">ch<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>),</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="2"></space><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="3.3" punct="vg:3">F<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>t</w>, <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7" punct="vg:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></w>,</l>
					<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="6"></space><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="4.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4" punct="dp:6">br<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>squ<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></w> :</l>
					<l n="5" num="1.5" lm="9" met="3+6"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3" punct="vg:2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" punct="vg">un</seg></w>, <w n="5.4">c</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3" caesura="1">e</seg>st</w><caesura></caesura> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="5.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="5.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="ri_1" place="9">en</seg>t</rhyme></w></l>
					<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="6"></space><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">m</w>’<w n="6.3" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="6.5">m</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.7" punct="pt:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>qu<rhyme label="b" id="3" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2" type="sizain" rhyme="ababab">
					<l n="7" num="2.1" lm="8" met="8"><space unit="char" quantity="2"></space><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>PR<seg phoneme="e" type="vs" value="1" rule="409" place="2">É</seg>S</w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="7.4" punct="vg:8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>sp<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rts</rhyme></w>,</l>
					<l n="8" num="2.2" lm="6" met="6"><space unit="char" quantity="6"></space>(<w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="8.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="8.5" punct="pf:6">ch<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<rhyme label="b" id="3" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>),</l>
					<l n="9" num="2.3" lm="8" met="8"><space unit="char" quantity="2"></space><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="9.2">d</w>’<w n="9.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="9.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ff<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rts</rhyme></w>,</l>
					<l n="10" num="2.4" lm="6" met="6"><space unit="char" quantity="6"></space><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">m</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-29">e</seg>nt</w> <w n="10.4" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gu<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="11" num="2.5" lm="9" met="3+6"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="11.4" punct="vg:3">s<seg phoneme="o" type="vs" value="1" rule="438" place="3" punct="vg" caesura="1">o</seg>t</w>,<caesura></caesura> <w n="11.5">c</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="11.8">s<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>t</w> <w n="11.9">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="11.10">c<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rps</rhyme></w></l>
					<l n="12" num="2.6" lm="6" met="6"><space unit="char" quantity="6"></space><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">m</w>’<w n="12.3" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="12.5">m</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.7" punct="pt:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>qu<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3" type="sizain" rhyme="ababab">
					<l n="13" num="3.1" lm="8" met="8"><space unit="char" quantity="2"></space><w n="13.1">D</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>B<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">O</seg>RD</w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="13.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="13.7">c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>sp<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ct</rhyme></w></l>
					<l n="14" num="3.2" lm="6" met="6"><space unit="char" quantity="6"></space><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="14.2">m</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="14.4" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>qu<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="15" num="3.3" lm="8" met="8"><space unit="char" quantity="2"></space><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="15.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="15.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="15.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ct</rhyme></w></l>
					<l n="16" num="3.4" lm="6" met="6"><space unit="char" quantity="6"></space><w n="16.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>squ<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="16.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">É</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>p<rhyme label="b" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="17" num="3.5" lm="9" met="3+6"><w n="17.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="17.2" punct="vg:2">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2" punct="vg">en</seg></w>, <w n="17.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3" caesura="1">e</seg>l</w><caesura></caesura> <w n="17.4" punct="vg:4">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="vg">en</seg></w>, <w n="17.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="17.6">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="17.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="17.8" punct="pe:9">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>sp<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" punct="pe">e</seg>ct</rhyme></w> !</l>
					<l n="18" num="3.6" lm="6" met="6"><space unit="char" quantity="6"></space><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="18.2">m</w>’<w n="18.3" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, <w n="18.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="18.5">m</w>’<w n="18.6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.7" punct="pt:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>qu<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>