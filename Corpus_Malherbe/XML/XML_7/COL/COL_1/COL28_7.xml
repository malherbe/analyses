<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL28" modus="sp" lm_max="7" metProfile="7, 4, (5)" form="suite périodique" schema="4(ababcc) 1(ababbb)">
				<head type="main">CHANSON DE PARADE,</head>
				<head type="sub_1">Chantée par Gilles le Niais.</head>
				<head type="tune">Air : Vive les Grecs.</head>
				<head type="tune">Noté, N°. 20.</head>
				<lg n="1" type="sizain" rhyme="ababcc">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg></w> <w n="1.2">j</w>’<w n="1.3">sç<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="1.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="1.6">M<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="6"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>rg<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="448" place="4" punct="vg">u</seg>s</rhyme></w>,</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="3.2">j</w>’<w n="3.3">sç<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="3.4">d</w>’<w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.7">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>p<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="4" num="1.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="4.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w>’ <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3" punct="vg:4">C<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>c<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>s</rhyme></w>,</l>
					<l n="5" num="1.5" lm="7" met="7"><w n="5.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w>-<w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</rhyme></w> <ref target="1" type="noteAnchor">(1)</ref></l>
					<l n="6" num="1.6" lm="5"><space unit="char" quantity="4"></space><w n="6.1">G<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3" punct="pi:5">N<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pi">ai</seg>s</rhyme></w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2" type="sizain" rhyme="ababcc">
					<l n="7" num="2.1" lm="7" met="7"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg></w> <w n="7.2">j</w>’<w n="7.3">sç<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.5">F<seg phoneme="ɛ" type="vs" value="1" rule="366" place="5">e</seg>mm</w>’ <w n="7.6" punct="vg:7">pr<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>d<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="8" num="2.2" lm="4" met="4"><space unit="char" quantity="6"></space><w n="8.1">J<seg phoneme="u" type="vs" value="1" rule="d-2" place="1">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>r</w> <w n="8.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="8.3" punct="pv:4">bru<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="pv">i</seg>t</rhyme></w> ;</l>
					<l n="9" num="2.3" lm="7" met="7"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg></w> <w n="9.2">j</w>’<w n="9.3">sç<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="9.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>c<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="10" num="2.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="10.2">l</w>’<w n="10.3" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>spr<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t</rhyme></w>,</l>
					<l n="11" num="2.5" lm="7" met="7"><w n="11.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w>-<w n="11.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</rhyme></w></l>
					<l n="12" num="2.6" lm="5"><space unit="char" quantity="4"></space><w n="12.1">G<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3" punct="pi:5">N<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><rhyme label="c" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pi">ai</seg>s</rhyme></w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3" type="sizain" rhyme="ababcc">
					<l n="13" num="3.1" lm="7" met="7"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">j</w>’<w n="13.3">sç<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="13.4">b<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="4">en</seg></w> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="13.7" punct="pv:7">ch<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg>s</rhyme></w> ;</l>
					<l n="14" num="3.2" lm="4" met="4"><space unit="char" quantity="6"></space><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="14.3" punct="pv:4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pv">i</seg>r</rhyme></w> ;</l>
					<l n="15" num="3.3" lm="7" met="7"><w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="15.2">j</w>’<w n="15.3">sç<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="15.4">c<seg phoneme="œ" type="vs" value="1" rule="345" place="4">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.6" punct="vg:7">R<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="16" num="3.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="16.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3" punct="pv:4">fl<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>tr<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pv">i</seg>r</rhyme></w> ;</l>
					<l n="17" num="3.5" lm="7" met="7"><w n="17.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w>-<w n="17.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</rhyme></w></l>
					<l n="18" num="3.6" lm="5"><space unit="char" quantity="4"></space><w n="18.1">G<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.3" punct="pi:5">N<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><rhyme label="c" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pi">ai</seg>s</rhyme></w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4" type="sizain" rhyme="ababcc">
					<l n="19" num="4.1" lm="7" met="7"><w n="19.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="19.2">j</w>’<w n="19.3">sç<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="19.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="19.6" punct="vg:7">F<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="193" place="7">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="20" num="4.2" lm="4" met="4"><space unit="char" quantity="6"></space><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="20.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3" punct="pv:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>xpl<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pv">oi</seg>ts</rhyme></w> ;</l>
					<l n="21" num="4.3" lm="7" met="7"><w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="21.2">j</w>’<w n="21.3">sç<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="21.4" punct="vg:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="21.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="21.6" punct="vg:7">D<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="22" num="4.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="22.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="22.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>x</w> <w n="22.4" punct="pv:4">d<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pv">oi</seg>gts</rhyme></w> ;</l>
					<l n="23" num="4.5" lm="7" met="7"><w n="23.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w>-<w n="23.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="23.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</rhyme></w></l>
					<l n="24" num="4.6" lm="5"><space unit="char" quantity="4"></space><w n="24.1">G<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="24.3" punct="pi:5">N<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><rhyme label="c" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pi">ai</seg>s</rhyme></w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="5" type="sizain" rhyme="ababbb">
					<l n="25" num="5.1" lm="7" met="7"><w n="25.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">OU</seg>T</w> <w n="25.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>s</w> <w n="25.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.7">d<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="26" num="5.2" lm="4" met="4"><space unit="char" quantity="6"></space><w n="26.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="26.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="26.3" punct="vg:4">C<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>pl<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" punct="vg">e</seg>ts</rhyme></w>,</l>
					<l n="27" num="5.3" lm="7" met="7"><w n="27.1">N</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="27.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>t</w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="27.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="27.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="27.7" punct="vg:7">r<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="28" num="5.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="28.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="28.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="28.4" punct="pv:4">f<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pv">ai</seg>ts</rhyme></w> ;</l>
					<l n="29" num="5.5" lm="7" met="7"><w n="29.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="29.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="29.3">m</w>’<w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pr<rhyme label="b" id="15" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</rhyme></w></l>
					<l n="30" num="5.6" lm="5"><space unit="char" quantity="4"></space><w n="30.1">G<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="30.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="30.3" punct="pi:5">N<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><rhyme label="b" id="15" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pi">ai</seg>s</rhyme></w> ?</l>
				</lg>
				<closer>
					<note type="footnote" id="(1)">On remarquera que ce mot est écrit <lb></lb>conformement à l’Orthographe de M. de Voltaire.</note>
				</closer>
			</div></body></text></TEI>