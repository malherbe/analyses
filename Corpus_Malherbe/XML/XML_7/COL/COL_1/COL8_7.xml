<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL8" modus="sp" lm_max="8" metProfile="8, 7, 6, 3, (4), (1)" form="suite de strophes" schema="1[abbbaccca] 2[aaa] 2[aa] 1[abbba]">
				<head type="main">L’AMOUR APOTHICAIRE,</head>
				<head type="form">PARODIE.</head>
				<head type="tune">Air : Noté, N°. 8.</head>
				<lg n="1" type="regexp" rhyme="abbbaccc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">OU</seg>R</w> <w n="1.2">fl<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">N<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>st<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="COL8_1" place="8">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="2.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">A</seg>m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</rhyme></w></l>
					<l n="3" num="1.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg></w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.4">j<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</rhyme></w></l>
					<l n="4" num="1.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="4.1">D</w>’<w n="4.2"><seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="4.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="4.6" punct="vg:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>r</rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Pr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="5.2">l</w>’<w n="5.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="5.6" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>th<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="6" num="1.6" lm="7" met="7"><space unit="char" quantity="2"></space><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="6.2" punct="vg:3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="6.4" punct="vg:7">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<rhyme label="c" id="3" gender="m" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg></rhyme></w>,</l>
					<l n="7" num="1.7" lm="6" met="6"><space unit="char" quantity="4"></space><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2" punct="vg:3">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="7.3">qu</w>’<w n="7.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="7.5" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<rhyme label="c" id="3" gender="m" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></rhyme></w>,</l>
					<l n="8" num="1.8" lm="3" met="3"><space unit="char" quantity="10"></space><w n="8.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2" punct="pt:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ge<rhyme label="c" id="3" gender="m" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="316" place="3" punct="pt">a</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2" type="regexp" rhyme="aaaaaaaa">
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2" punct="vg:2">N<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" punct="vg">o</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="9.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.6" punct="vg:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rri<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="COL8_1" place="8">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="10" num="2.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="10.1" punct="dp:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="dp">i</seg>t</w> : <w n="10.2">d<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="10.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="10.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<rhyme label="a" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7">en</seg>t</rhyme></w></l>
					<l n="11" num="2.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="11.3" punct="vg:6">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<rhyme label="a" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="vg">en</seg>t</rhyme></w>,</l>
					<l n="12" num="2.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="12.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">tr<seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6">dr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p</w> <w n="12.7" punct="pt:7">bl<rhyme label="a" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pt">an</seg>c</rhyme></w>.</l>
					<l n="13" num="2.5" lm="4"><space unit="char" quantity="8"></space><w n="13.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="13.2">qu</w>’<w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="13.4" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>tr<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></rhyme></w>,</l>
					<l n="14" num="2.6" lm="7" met="7"><space unit="char" quantity="2"></space><w n="14.1">D</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">s</w>’<w n="14.5" punct="dp:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="dp">a</seg></rhyme></w> :</l>
					<l n="15" num="2.7" lm="1"><space unit="char" quantity="14"></space><w n="15.1" punct="pe:1"><rhyme label="a" id="6" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</rhyme></w> !</l>
					<l n="16" num="2.8" lm="7" met="7"><space unit="char" quantity="2"></space><w n="16.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="16.2" punct="vg:3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="16.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="16.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="16.6" punct="pt:7">l<rhyme label="a" id="6" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" punct="pt">à</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3" type="regexp" rhyme="abbbaaaa">
					<l n="17" num="3.1" lm="8" met="8"><w n="17.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="17.2" punct="pe:2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pe">eu</seg></w> ! <w n="17.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5">sç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="17.6"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="17.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="17.8" punct="pv:8"><rhyme label="a" id="7" gender="f" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
					<l n="18" num="3.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="18.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="18.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd<rhyme label="b" id="8" gender="m" type="a" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pe">eu</seg>r</rhyme></w> !</l>
					<l n="19" num="3.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="19.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="19.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<rhyme label="b" id="8" gender="m" type="e" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</rhyme></w></l>
					<l n="20" num="3.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="20.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">p<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="20.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d</w> <w n="20.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="20.6" punct="pt:7">c<rhyme label="b" id="8" gender="m" type="a" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="249" place="7" punct="pt">œu</seg>r</rhyme></w>.</l>
					<l n="21" num="3.5" lm="8" met="8"><w n="21.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="21.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="21.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="21.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="21.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="21.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="21.7" punct="pe:8">f<rhyme label="a" id="7" gender="f" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
					<l n="22" num="3.6" lm="7" met="7"><space unit="char" quantity="2"></space><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="22.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p</w> <w n="22.5" punct="vg:7">mi<rhyme label="a" id="9" gender="m" type="a" stanza="6"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="23" num="3.7" lm="6" met="6"><space unit="char" quantity="4"></space><w n="23.1">L</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="23.4" punct="dp:6">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<rhyme label="a" id="9" gender="m" type="e" stanza="6"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="dp">eu</seg>x</rhyme></w> :</l>
					<l n="24" num="3.8" lm="3" met="3"><space unit="char" quantity="10"></space><w n="24.1">J</w>’<w n="24.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="24.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="24.4" punct="pt:3">d<rhyme label="a" id="9" gender="m" type="a" stanza="6"><seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="pt">eu</seg>x</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>