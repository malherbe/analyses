<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA10" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="11((aa))">
				<head type="number">X</head>
				<head type="main">DANS CES MURS OÙ L’ÉCHO</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="1.3">m<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rs</w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg></w><caesura></caesura> <w n="1.7">r<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="1.9">h<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11" mp="M">o</seg>qu<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12">e</seg>ts</rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.4" punct="vg:6">J<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="415" place="6" punct="vg" caesura="1">ô</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="2.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>qu<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>s</rhyme></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="C">i</seg></w> <w n="3.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="3.4" punct="dp:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="dp in" caesura="1">o</seg>r</w> :<caesura></caesura> « <w n="3.5" punct="vg:8">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, » <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="3.7" punct="vg:12">B<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>p<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="4.2" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>ts</w>, <w n="4.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="4.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="6" caesura="1">œ</seg>il</w><caesura></caesura> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>br<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="4.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="4.9" punct="pt:12">c<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="5.4">pr<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5" mp="M">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" caesura="1">en</seg>s</w><caesura></caesura> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="5.7" punct="pv:12">P<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg>s</rhyme></w> ;</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="6.2" punct="pt:3">r<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>c<seg phoneme="a" type="vs" value="1" rule="341" place="3" punct="pt">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">Un</seg></w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>xpr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>s</w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.6">qu</w>’<w n="6.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="6.9">pr<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s</rhyme></w></l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.4">br<seg phoneme="y" type="vs" value="1" rule="445" place="5" mp="M">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>x</w> <w n="7.6" punct="pv:9">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="pv">on</seg>s</w> ; <w n="7.7"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="C">i</seg>l</w> <w n="7.8" punct="pt:12">j<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>b<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1" punct="vg:1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>d</w>, <w n="8.2">v<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="8.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.4">j<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="8.7">fl<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>t</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="8.9" punct="vg:12">b<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2" punct="dp:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="dp in">i</seg>t</w> : « <w n="9.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="9.4" punct="pe:4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pe">eu</seg>x</w> ! <w n="9.5">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.6">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="9.8">tr<seg phoneme="o" type="vs" value="1" rule="433" place="9">o</seg>p</w> <w n="9.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>g<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>t</rhyme></w>. »</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="10.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg></w><caesura></caesura> <w n="10.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="10.7">r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="10.9" punct="vg:12">br<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>g<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>d</rhyme></w>,</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="pt">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>. <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">I</seg>l</w> <w n="11.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>t</w><caesura></caesura> <w n="11.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.9">h<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="12.3" punct="pt:3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="pt">an</seg>g</w>. <w n="12.4">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="12.5" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="12.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="12.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="12.10">c<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="13.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3" punct="dp:5">bl<seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="dp in" mp="F">e</seg>s</w> : « <w n="13.4" punct="pe:6">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" punct="pe" caesura="1">en</seg>s</w> !<caesura></caesura> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="13.7" punct="vg:12">l<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="12" punct="vg">à</seg></rhyme></w>,</l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="14.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="14.6">h<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="14.8" punct="vg:12">g<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg></rhyme></w>,</l>
					<l part="I" n="15" num="1.15" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="15.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="15.4" punct="vg:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">î</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg" caesura="1">ai</seg></w>,<caesura></caesura> <w n="15.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="15.6" punct="pe:10">m<seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pe">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! » </l>
					<l part="F" n="15" num="1.15" lm="12" met="6+6"><w n="15.7"><seg phoneme="o" type="vs" value="1" rule="415" place="11">Ô</seg></w> <w n="15.8">d<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1" punct="pe:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pe">é</seg></w> ! <w n="16.2">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rps</w> <w n="16.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="16.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="16.5">n</w>’<w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="8">e</seg>s</w> <w n="16.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="16.8">qu</w>’<w n="16.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="16.10" punct="vg:12">dr<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="17.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="17.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="17.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="17.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="17.7" punct="pi:12">pr<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1" place="11" mp="M">i</seg><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pi">en</seg>s</rhyme></w> ?</l>
					<l n="18" num="1.18" lm="12" met="6+6"><w n="18.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="18.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="18.4" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt" caesura="1">a</seg>s</w>.<caesura></caesura> <w n="18.5">S<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="18.6">j<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="18.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="18.8" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>vi<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="12" punct="vg">en</seg>s</rhyme></w>,</l>
					<l n="19" num="1.19" lm="12" met="6+6"><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="19.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="19.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.5" punct="dp:9">Tr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>pm<seg phoneme="a" type="vs" value="1" rule="341" place="9" punct="dp">a</seg>nn</w> : <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="19.7">m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>st<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="20" num="1.20" lm="12" met="6+6"><w n="20.1" punct="vg:2">R<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="20.3">b<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="20.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rt</w><caesura></caesura> <w n="20.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="20.7" punct="vg:9">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9" punct="vg">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="20.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="20.9">l</w>’<w n="20.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tt<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="21" num="1.21" lm="12" met="6+6"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">T<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="21.3">t</w>’<w n="21.4" punct="vg:6"><seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="21.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="21.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="21.8" punct="vg:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>rs</rhyme></w>,</l>
					<l n="22" num="1.22" lm="12" met="6+6"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="22.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="22.4">J<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="22.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="22.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="22.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="22.8" punct="pe:12">L<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>th<seg phoneme="o" type="vs" value="1" rule="GLA10_1" place="11" mp="M">au</seg>w<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="GLA10_2" place="12" punct="pe">e</seg>rs</rhyme></w> !</l>
				</lg>
				<closer>
					<dateline> 4 octobre.</dateline>
				</closer>
			</div></body></text></TEI>