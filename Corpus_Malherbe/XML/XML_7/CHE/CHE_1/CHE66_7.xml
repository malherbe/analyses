<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS</head><div type="poem" key="CHE66" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="4((aa))">
					<head type="number">I</head>
					<head type="main">Tiré d’Oppien</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l ana="incomplete" where="F" n="1" num="1.1"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="ø" type="vs" value="1" rule="405">Eu</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> .........................................</l>
						<l rhyme="none" n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="Lc">o</seg>rsqu</w>’<w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="2.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>s</w><caesura></caesura> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="2.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.9" punct="vg:12">L<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="3.4" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="3.7">r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.8" punct="vg:12">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>bl<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="4.6">Z<seg phoneme="ø" type="vs" value="1" rule="405" place="5" mp="M">eu</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="4.10" punct="pv:12">b<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pv">eau</seg></rhyme></w> ;</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.3" punct="vg:6">B<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="5.4">H<seg phoneme="i" type="vs" value="1" rule="dc-4" place="7" mp="M">y</seg><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.5" punct="vg:12">N<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>r<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="6.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="6.4">G<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="6.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="6.6">s<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="6.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="6.8" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>r<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="7.4" punct="pv:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pv" caesura="1">em</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ;<caesura></caesura> <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="7.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="7.8">y<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></w></l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3" punct="vg:4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" punct="vg">e</seg>ts</w>, <w n="8.4">h<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="8.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="8.10" punct="pv:12">ci<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg>x</rhyme></w> ;</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="9.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="9.4" punct="vg:4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>c</w>, <w n="9.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="9.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="9.9" punct="vg:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="10.3">fru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="10.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="10.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="10.9">b<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg>x</w> <w n="10.10" punct="pt:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>d<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>