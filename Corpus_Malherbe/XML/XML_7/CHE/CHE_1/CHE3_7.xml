<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES ANTIQUES ‒ ÉTUDES</head><div type="poem" key="CHE3" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="7((aa))">
					<head type="number">III</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:2"><seg phoneme="e" type="vs" value="1" rule="250" place="1" mp="M">Œ</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="4" mp="M">en</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="1.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="1.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t</w> <w n="1.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rd<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>x</w><caesura></caesura> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">im</seg>pr<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>d<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="3.6">pr<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t</w> <w n="3.7">tr<seg phoneme="o" type="vs" value="1" rule="433" place="10">o</seg>p</w> <w n="3.8" punct="vg:12">j<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="4.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" mp="M">en</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="317" place="6" caesura="1">au</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="4.7" punct="pv:12">c<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>ps</rhyme></w> ;</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="5.4" punct="vg:6">f<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6" punct="vg" caesura="1">ê</seg>ts</w>,<caesura></caesura> <w n="5.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.6">c<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="5.9" punct="vg:12">s<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="6.3">b<seg phoneme="y" type="vs" value="1" rule="445" place="3" mp="M">û</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="6.7">n<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="7.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="7.6">br<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="7.8" punct="pt:12">pl<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</rhyme></w>.</l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="8.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.5" punct="pv:6">fl<seg phoneme="a" type="vs" value="1" rule="341" place="6" punct="pv" caesura="1">a</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ;<caesura></caesura> <w n="8.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="8.7" punct="vg:9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="8.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>s</w> <w n="8.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="8.10">pi<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="241" place="12">e</seg>ds</rhyme></w></l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="9.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="9.4">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428" place="9">ou</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.7" punct="vg:12">h<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="477" place="12">ï</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="œ" type="vs" value="1" rule="286" place="2">œ</seg>il</w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="10.5" punct="vg:4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="10.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="10.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="10.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.11" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="11.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.5">l</w>’<w n="11.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="11.10" punct="pt:12">di<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg></rhyme></w>.</l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="12.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.5" punct="pt:6">m<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt" caesura="1">i</seg>t</w>.<caesura></caesura> <w n="12.6">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.7">b<seg phoneme="y" type="vs" value="1" rule="445" place="8" mp="M">û</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="12.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="12.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="12.10">f<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg></rhyme></w></l>
						<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="13.4" punct="vg:6">h<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>s</w>,<caesura></caesura> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.7">fl<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.8">r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>p<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg>x</w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="14.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg>s</w><caesura></caesura> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="14.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d</w> <w n="14.9" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">A</seg>lc<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l rhyme="none" n="15" num="1.15" lm="12" met="6+6"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="15.2">p<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="15.4">N<seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="15.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="15.6">s<seg phoneme="u" type="vs" value="1" rule="428" place="8" mp="M">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="15.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="15.8" punct="ps:12">pr<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="ps">en</seg>ts</w>…</l>
					</lg>
				</div></body></text></TEI>