<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Bestiaire</title>
				<title type="medium">Édition électronique</title>
				<author key="APO">
					<name>
						<forename>Guillaume</forename>
						<surname>APOLLINAIRE</surname>
					</name>
					<date from="1880" to="1918">1880-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>127 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">APO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bestiaire</title>
						<author>Guillaume Apollinaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>florilege.free.fr</publisher>
						<idno type="URL">http://www.florilege.free.fr/recueil/apollinaire-le_bestiaire.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="APO10" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(abbaa)">
				<head type="main">Le Dromadaire</head>
				<lg n="1" type="quintil" rhyme="abbaa">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="1.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="1.3">qu<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">dr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>dr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg></w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>lf<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg>r<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.5">l</w>’<w n="3.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="4.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="4.7">f<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">j</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="5.4">qu<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5" punct="pt:8">dr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>