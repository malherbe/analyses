<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CY GIST</head><head type="sub_part">
					OU 
					Diverses Épitaphes pour toute sorte de personnes 
					de l’un et de l’autre sexe, et pour l’Auteur même, 
					quoique vivant.
				</head><div type="poem" key="BEN126" modus="cp" lm_max="12" metProfile="8, 6+6" form="strophe unique" schema="1(ababcc)">
					<head type="main">Épitaphe d’une Femme sçavante.</head>
					<lg n="1" type="sizain" rhyme="ababcc">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg></w> <w n="1.2">g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="1.4" punct="vg:6">Sç<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="1.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>ff<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12">e</seg>t</rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="2.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6" caesura="1">en</seg>s</w><caesura></caesura> <w n="2.7">d</w>’<w n="2.8"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.9">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="3.6">f<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">D</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="4.3">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="4.7" punct="vg:8">Ph<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2" punct="vg:2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="5.3">d</w>’<w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="5.5">g<seg phoneme="u" type="vs" value="1" rule="425" place="4">oû</seg>t</w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="5.7">s<seg phoneme="ɛ" type="vs" value="1" rule="355" place="6" caesura="1">e</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="5.9" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>li<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="y" type="vs" value="1" rule="251" place="1">Eû</seg>t</w> <w n="6.2">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4">R<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="6.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>t</w> <w n="6.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="6.7">l</w>’<w n="6.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">É</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>li<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>