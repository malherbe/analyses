<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CY GIST</head><head type="sub_part">
					OU 
					Diverses Épitaphes pour toute sorte de personnes 
					de l’un et de l’autre sexe, et pour l’Auteur même, 
					quoique vivant.
				</head><div type="poem" key="BEN96" modus="cp" lm_max="12" metProfile="8, 6+6, 4+6" form="suite de strophes" schema="1[abbaccac] 1[abab] 1[abba] 1[aa] 1[abbab]">
					<head type="main">Épitaphe d’un Médecin.</head>
					<lg n="1" type="regexp" rhyme="abbaccacabababbaaaabbab">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1">C<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg></w> <w n="1.2" punct="vg:2">g<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>st</w>, <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="1.5">g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.7" punct="vg:8"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="2.2">M<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="2.5">sç<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</rhyme></w></l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">A</seg>rt</w> <w n="3.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="3.5">f<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="3.7" punct="pv:8">V<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pv">an</seg>s</rhyme></w> ;</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="4.3">lu<seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.5" punct="dp:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</rhyme></w> :</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">S</w>’<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="5.7">d</w>’<w n="5.8">H<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ti<rhyme label="c" id="3" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</rhyme></w></l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="6.3" punct="vg:2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>t</w>, <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="6.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="6.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="6.7" punct="vg:8">ti<rhyme label="c" id="3" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg>s</rhyme></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2">n</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.7" punct="pv:8">n<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
						<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ls</w> <w n="8.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="8.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="8.5" punct="pt:8">v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>ti<rhyme label="c" id="3" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg>s</rhyme></w>.</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="9.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="9.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="9.6" punct="vg:6">s<seg phoneme="ɛ" type="vs" value="1" rule="355" place="6" punct="vg" caesura="1">e</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="9.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="9.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.10" punct="vg:12">gu<rhyme label="a" id="4" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="10.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="10.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">d</w>’<w n="10.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8" punct="pt:12">b<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>ss<rhyme label="b" id="5" gender="m" type="a" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></w>.</l>
						<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="11.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="11.4">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rts</w><caesura></caesura> <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.7">tr<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="11.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="P">ou</seg>s</w> <w n="11.9" punct="vg:12">t<rhyme label="a" id="4" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">N</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="12.3">d<seg phoneme="y" type="vs" value="1" rule="445" place="2" mp="Lp">û</seg>t</w>-<w n="12.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="12.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="12.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7">c<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="12.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="12.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="12.10" punct="pi:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ç<rhyme label="b" id="5" gender="m" type="e" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pi">on</seg></rhyme></w> ?</l>
						<l n="13" num="1.13" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="13.3">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="4">u</seg>y<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="13.6" punct="vg:8">Li<rhyme label="a" id="6" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="1.14" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="14.3">lu<seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="14.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>bl<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6" punct="pv:8">p<rhyme label="b" id="7" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>s</rhyme></w> ;</l>
						<l n="15" num="1.15" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">n</w>’<w n="15.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="15.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="15.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.7">tr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<rhyme label="b" id="7" gender="m" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
						<l n="16" num="1.16" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="16.3">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="16.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.8" punct="pt:8">Fi<rhyme label="a" id="6" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="17" num="1.17" lm="8" met="8"><space unit="char" quantity="8"></space><w n="17.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="17.2"><seg phoneme="e" type="vs" value="1" rule="170" place="2">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="17.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="17.4">Qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="a" id="8" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></rhyme></w></l>
						<l n="18" num="1.18" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">d</w>’<w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">Au</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="3">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t</w> <w n="18.6" punct="pt:8">C<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>nn<rhyme label="a" id="8" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></w>.</l>
						<l n="19" num="1.19" lm="10" met="4+6"><w n="19.1">Vr<seg phoneme="ɛ" type="vs" value="1" rule="323" place="1">ay</seg></w> <w n="19.2" punct="vg:4">B<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>c</w>,<caesura></caesura> <w n="19.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="19.4">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="6" mp="M">u</seg><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="19.5">d</w>’<w n="19.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.7" punct="vg:10"><seg phoneme="œ" type="vs" value="1" rule="286" place="9" mp="M">œ</seg>ill<rhyme label="a" id="9" gender="f" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="20" num="1.20" lm="10" met="4+6"><w n="20.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="20.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="20.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="20.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="20.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="20.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="20.8" punct="pv:10">f<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>l<rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="pv">e</seg>t</rhyme></w> ;</l>
						<l n="21" num="1.21" lm="10" met="4+6"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">n</w>’<w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rgn<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="21.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="21.7" punct="vg:10">M<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>l<rhyme label="b" id="10" gender="m" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="vg">e</seg>t</rhyme></w>,</l>
						<l n="22" num="1.22" lm="10" met="4+6"><w n="22.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="22.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="22.3">M<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" caesura="1">e</seg>t</w><caesura></caesura> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="22.6" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<rhyme label="a" id="9" gender="f" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="23" num="1.23" lm="12" met="6+6"><w n="23.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="23.2">qu</w>’<w n="23.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="23.4">n</w>’<w n="23.5"><seg phoneme="y" type="vs" value="1" rule="251" place="3">eû</seg>t</w> <w n="23.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="23.7">lu<seg phoneme="i" type="vs" value="1" rule="497" place="5" mp="Lc">y</seg></w>-<w n="23.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="23.9"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="23.10">pr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="23.11"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="23.12" punct="pt:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>t</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>