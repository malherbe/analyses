<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DÉLIQUESCENCES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA" sort="1">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<author key="VIC" sort="2">
					<name>
						<forename>Gabriel</forename>
						<surname>VICAIRE</surname>
					</name>
					<date from="1848" to="1900">1848-1900</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>				
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>297 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Déliquescences</title>
						<author>Henri Beauclair et Gabriel Vicaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 - 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/deliqu01.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Déliquescences</title>
								<author>Henri Beauclair et Gabriel Vicaire</author>
								<imprint>
									<publisher>Les Editions Henri Jonquières et Cie, Paris</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEV10" modus="cm" lm_max="12" metProfile="6=6" form="suite périodique" schema="3(abab)">
				<head type="main">Pour avoir péché</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" mp6="M" mp4="C" met="8+4"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="1.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="1.5">C<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="493" place="6" mp="M">y</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>ps<seg phoneme="i" type="vs" value="1" rule="468" place="8" caesura="1">i</seg>s</w><caesura></caesura> <w n="1.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="1.7" punct="vg:11">J<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" punct="vg">on</seg></w>, <w n="1.8">r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="307" place="2" mp="M">a</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="2.5" punct="tc:6">f<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg ti" caesura="1">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> — <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r</w> <w n="2.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="2.10" punct="pt:12">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rp<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pt">en</seg>ts</rhyme></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="3.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="3.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="3.7" punct="vg:12">Chl<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>t</w>, <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">É</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>r</w> <w n="4.5" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="4.6">br<seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="4.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="272" place="10" mp="M">Æ</seg>g<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg>p<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="5.6" punct="vg:12">t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4">cu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ds</w> <w n="6.7" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>v<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">oû</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="pv">en</seg>ts</rhyme></w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="7.3" punct="vg:5">cr<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="7.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cs</w> <w n="7.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="7.8" punct="vg:12">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>b<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="8.2"><seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ts</w> <w n="8.3">f<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="8.4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="8.6" punct="pt:12">N<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>cr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="9.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">En</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.4" punct="vg:6">C<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rm<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg>s</w>,<caesura></caesura> <w n="9.5" punct="vg:8">fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" punct="vg" mp="F">e</seg>s</w>, <w n="9.6">vr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.8" punct="vg:12">c<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="10.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.7">D<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="10.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="10.9" punct="pi:12">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="11" mp="M">ou</seg><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pi">i</seg>t</rhyme></w> ?</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="11.3">P<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="4" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="11.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="11.6" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rc<seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg>ss<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1" punct="pe:1">L<seg phoneme="i" type="vs" value="1" rule="493" place="1" punct="pe">y</seg>s</w> ! <w n="12.2" punct="pe:4">D<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="12.3" punct="pe:6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">O</seg>rch<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pe" caesura="1">i</seg>s</w> !<caesura></caesura> <w n="12.4">M<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="12.6" punct="pe:12">L<seg phoneme="u" type="vs" value="1" rule="dc-2" place="11" mp="M">ou</seg><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pe">i</seg>t</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>