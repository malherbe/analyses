<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="BAN54" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
					<head type="main">CONSEIL</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="133" place="1">E</seg>h</w> <w n="1.2" punct="pe:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2" punct="pe">en</seg></w> ! <w n="1.3">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.5">v<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.9" punct="pe:12">f<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12" punct="pe">ê</seg>t</rhyme></w> !</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">E</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="2.3">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="2.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.6" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:2">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.7" punct="vg:12">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Fu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="4.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>ts</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>rs</w><caesura></caesura> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="4.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="4.8" punct="pt:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1" mp="P">è</seg>s</w> <w n="5.2">l</w>’<w n="5.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="4" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="5.8" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">aî</seg>t</rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="6.3" punct="pv:4">h<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pv">a</seg>rd</w> ; <w n="6.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg>s</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="6.9" punct="pt:12">r<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.3" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="7.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</w> <w n="7.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="7.9" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="8.3">b<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="8.5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="8.6">qu</w>’<w n="8.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="8.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="353" place="8" mp="M">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">C<seg phoneme="œ" type="vs" value="1" rule="345" place="1">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="9.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="9.6">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rd</w> <w n="9.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="9.8" punct="pt:12">pr<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>p<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="10.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.7">li<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>p<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="11.5"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="11.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="11.9" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ff<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pt">u</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="12.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="12.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="12.8">s<seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="12.9" punct="pv:12">c<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="13.5" punct="vg:6">fr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="13.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg></w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="13.8">rh<seg phoneme="i" type="vs" value="1" rule="493" place="9">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="13.9" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>f<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</rhyme></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="14.3">m<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="14.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="14.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="14.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="14.9">l</w>’<w n="14.10"><seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="14.11">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.12" punct="pt:12">s<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">juillet 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>