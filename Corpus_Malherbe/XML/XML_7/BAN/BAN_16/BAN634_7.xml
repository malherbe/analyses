<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN634" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(abab)">
				<head type="number">I</head>
				<head type="main">A Catulle Mendès</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="1.2" punct="vg:3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" punct="vg">en</seg>t</w>, <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.5">Ph<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="2.4" punct="vg:5">br<seg phoneme="u" type="vs" value="1" rule="427" place="4">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg>s</w>, <w n="2.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="2.6" punct="vg:8">C<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">Ph<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="3.6" punct="vg:8">t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="vg">ein</seg>ts</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="4.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="4.6" punct="pt:8">T<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="5.3">C<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4" punct="vg:8">M<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" punct="vg">è</seg>s</rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">qu</w>’<w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">D</w>’<w n="7.2"><seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="7.5" punct="vg:8">H<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" punct="vg">è</seg>s</rhyme></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="8.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="8.3">d</w>’<w n="8.4" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.7" punct="vg:8">C<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>rr<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="9.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="9.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.8">s<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>s</rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>z</w> <w n="10.6" punct="vg:8">m<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="11.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="11.6">fi<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="11.7" punct="tc:8">d<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg ti">in</seg>s</rhyme></w>, —</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="12.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="12.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="12.7" punct="pt:8">r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="3">u</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="13.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="13.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="13.6" punct="vg:8">l<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>ds</rhyme></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="a" type="vs" value="1" rule="145" place="5">a</seg>m</w> <w n="14.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="14.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.6" punct="vg:8">L<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="15.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="15.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="15.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7">l<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="16.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="16.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="16.4" punct="pt:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="17.5" punct="vg:8">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="18.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="18.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5">l</w>’<w n="18.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg></w> <w n="18.7" punct="vg:8">c<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="19.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="19.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="19.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="19.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="9" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="20.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="20.6" punct="pt:8">C<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>c<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888"> 19 mai 1888.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>