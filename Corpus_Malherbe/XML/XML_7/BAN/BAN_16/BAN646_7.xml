<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN646" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="9(abab)">
				<head type="number">XIII</head>
				<head type="main">Le Froid</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="1.4" punct="vg:4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>r</w>, <w n="1.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg></w> <w n="1.6">d</w>’<w n="1.7" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>cr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2">v<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="2.3" punct="vg:6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="vg">en</seg>t</w>, <w n="2.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D</w>’<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="3.3">gr<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>t</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6" punct="vg:8">f<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="4.4" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp">i</seg>s</w> : <w n="4.5">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>c</w> <w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="7">e</seg>s</w>-<w n="4.8" punct="pi:8">t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pi">u</seg></rhyme></w> ?</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ff<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="5.5" punct="vg:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="6.4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="6.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.7" punct="pt:8">y<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">b<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="7.5">t</w>’<w n="7.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>p<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>-<w n="8.3" punct="vg:2">j<seg phoneme="ə" type="ee" value="0" rule="e-15">e</seg></w>, <w n="8.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="8.5" punct="pe:8">m<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</rhyme></w> !</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="9.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="10.4" punct="vg:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>sc<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>t<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">gr<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="11.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7">l<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">gr<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="12.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>gts</w> <w n="12.5" punct="pt:8">tr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>t<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2" punct="dp:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="dp">i</seg>s</w> : <w n="13.3">T<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="13.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg></w> <w n="13.7" punct="vg:8">f<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="14.2" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="354" place="5">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>pt</w> <w n="14.4">d</w>’<w n="14.5" punct="vg:8">h<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2">en</seg>t</w> <w n="15.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">n<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="15.4">t</w>-<w n="15.5" punct="pi:6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pi ti">on</seg></w> ? — <w n="15.6" punct="vg:8">P<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="414" place="8">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w>-<w n="16.2" punct="vg:2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>l</w>, <w n="16.3" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">f<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">A</seg>m<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="17.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="17.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="17.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="17.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="17.6" punct="vg:8">b<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="18.4" punct="tc:4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg ti">oi</seg>r</w>, — <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="18.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="18.7" punct="pe:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>l</rhyme></w> !</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>d</w> <w n="19.2" punct="vg:3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="19.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="19.4">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="19.5" punct="vg:8">S<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lt<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>b<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="20.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">n<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="20.5" punct="pt:8">D<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>l</rhyme></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="21.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="21.4">di<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="21.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.6" punct="vg:8">C<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>th<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="22.2" punct="vg:2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2" punct="vg">oin</seg>s</w>, <w n="22.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.4">l</w>’<w n="22.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="22.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="22.7" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="23.2" punct="vg:3">r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>r</w>, <w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="23.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="23.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.7" punct="vg:8">t<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="24.4">b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="25.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.5" punct="vg:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>s</w>, <w n="25.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="25.7" punct="vg:8">s<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">P<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="26.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="26.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="26.5" punct="vg:8">l<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="493" place="8" punct="vg">y</seg>s</rhyme></w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="27.3">str<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ct<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="27.4" punct="vg:7">n<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg></w>, <w n="27.5">c<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="28.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="28.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="28.4">R<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="28.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="28.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.7" punct="pt:8">Ph<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="29.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="29.3">n</w>’<w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="29.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="29.6">n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="29.7" punct="vg:8">t<rhyme label="a" id="15" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="30.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="30.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="30.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="5">en</seg>s</w> <w n="30.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="30.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="30.8">vi<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></w></l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2" punct="vg:4">n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="31.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="31.4" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">A</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>th<rhyme label="a" id="15" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="32.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="32.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="32.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="32.6" punct="pt:8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="16" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></w>.</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="8" met="8"><w n="33.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2" punct="vg:2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="33.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="33.4" punct="vg:5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="33.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="33.6" punct="pv:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>gl<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="34" num="9.2" lm="8" met="8"><w n="34.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="34.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="34.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="34.5" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg">œu</seg>rs</w>, <w n="34.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="34.7">l</w>’<w n="34.8" punct="pv:8"><rhyme label="b" id="18" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="pv">ai</seg></rhyme></w> ;</l>
					<l n="35" num="9.3" lm="8" met="8"><w n="35.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="35.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="35.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="35.4" punct="vg:5">m<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5" punct="vg">en</seg>t</w>, <w n="35.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="35.6">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="36" num="9.4" lm="8" met="8"><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="36.2">j</w>’<w n="36.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="36.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="36.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="36.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="36.7">n<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="36.8" punct="pt:8">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888"> 11 août 1888.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>