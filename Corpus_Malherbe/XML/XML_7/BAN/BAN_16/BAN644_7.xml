<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN644" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="9(abab)">
				<head type="number">XI</head>
				<head type="main">Désarmement</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pi:3">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="pi">er</seg></w> ? <w n="1.2" punct="vg:4">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg></w>, <w n="1.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w>-<w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="1.6" punct="vg:8">c<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rt</rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="2.3">qu</w>’<w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="2.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.8" punct="pt:8">c<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>gl<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg></w>, <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="3.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="3.6" punct="pi:8">c<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pi">ou</seg>rt</rhyme></w> ?</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="4.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="4.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="4.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="4.6">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w> <w n="4.7" punct="pt:8">c<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4" punct="pe:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pe">en</seg>t</rhyme></w> !</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="6.4" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="7.2" punct="vg:4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>t</w>, <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="vg">en</seg>t</rhyme></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">c<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.5" punct="pt:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="9.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>gl<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="10.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="3">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="10.4" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>p<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3" punct="pt:2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pt">on</seg></w>. <w n="11.4">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="11.5" punct="vg:6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="11.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">É</seg>gl<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="12.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="12.6" punct="pi:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</rhyme></w> ?</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="13.2" punct="vg:4">D<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="13.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="13.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="14.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="14.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="14.7" punct="vg:8">ch<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="15.2">qu</w>’<w n="15.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="15.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="15.7" punct="vg:8">p<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="16.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="16.5" punct="pt:8"><rhyme label="b" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">J<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="17.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="17.5" punct="vg:8">y<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="18.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5">l</w>’<w n="18.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rc</w> <w n="18.7">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="19.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="19.4">m<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3">br<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.6" punct="pi:8">b<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="21.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="21.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="21.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="21.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t</w> <w n="21.6">l</w>’<w n="21.7"><seg phoneme="œ" type="vs" value="1" rule="286" place="6">œ</seg>il</w> <w n="21.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>bt<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</rhyme></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="22.3">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>ph<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="22.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="22.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="22.6" punct="vg:8">v<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="23.2">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="3">e</seg></w>-<w n="23.3" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="23.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w>-<w n="23.6">t</w>-<w n="23.7"><rhyme label="a" id="11" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</rhyme></w></l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="24.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="24.5" punct="vg:8">v<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w>-<w n="25.3">t</w>-<w n="25.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="25.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="25.6">fi<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="25.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cc<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rds</rhyme></w></l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="27.2">s<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="27.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="27.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="27.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="27.6" punct="vg:8">c<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rps</rhyme></w>,</l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="28.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="28.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="28.4" punct="pi:8">pl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</rhyme></w> ?</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="29.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="29.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="29.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="29.6">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="a" id="15" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</rhyme></w></l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="30.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">An</seg>ch<rhyme label="b" id="16" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="31.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="31.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>s</w> <w n="31.4">d</w>’<w n="31.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="31.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="31.7" punct="pv:8">v<rhyme label="a" id="15" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pv">ou</seg>s</rhyme></w> ;</l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="32.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="32.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="32.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="32.6" punct="vg:8">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ch<rhyme label="b" id="16" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="8" met="8"><w n="33.1" punct="vg:3">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3" punct="vg">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="33.2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">È</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="33.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="33.4" punct="vg:8">D<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<rhyme label="a" id="17" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w>,</l>
					<l n="34" num="9.2" lm="8" met="8"><w n="34.1">D<seg phoneme="y" type="vs" value="1" rule="445" place="1">û</seg>t</w> <w n="34.2">l</w>’<w n="34.3"><seg phoneme="ø" type="vs" value="1" rule="405" place="2">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="34.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<rhyme label="b" id="18" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="35" num="9.3" lm="8" met="8"><w n="35.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="35.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="35.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="35.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="35.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w>-<w n="35.7" punct="vg:8">l<rhyme label="a" id="17" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></w>,</l>
					<l n="36" num="9.4" lm="8" met="8"><w n="36.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="36.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="36.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="36.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="36.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="36.6" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<rhyme label="b" id="18" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888"> 28 juillet 1888.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>