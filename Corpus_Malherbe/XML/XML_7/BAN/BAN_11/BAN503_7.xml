<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN503" modus="sm" lm_max="8" metProfile="8" form="petite ballade" schema="3(ababbcbc) bcbc">
					<head type="number">XXIX</head>
					<head type="main">Ballade de Victor Hugo <lb></lb>père de tous les rimeurs</head>
					<lg n="1" type="huitain" rhyme="ababbcbc">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="1.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gn<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">R<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.5" punct="pt:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>li<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg>s</rhyme></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2" punct="vg:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>rs</w>, <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.7" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>pr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="4.2">ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="4.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="4.4">h<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>li<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</rhyme></w></l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2" punct="vg:3">V<seg phoneme="wa" type="vs" value="1" rule="BAN503_1" place="2">oy</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg></w>, <w n="5.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="5">y</seg>s</w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.5" punct="vg:8">t<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>li<rhyme label="b" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg>s</rhyme></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D</w>’<w n="6.2" punct="vg:1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" punct="vg">Au</seg>ch</w>, <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.4" punct="vg:3">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>ts</w>, <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.6">G<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p</w> <w n="6.7"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.9" punct="vg:8">L<rhyme label="c" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="7.6" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>lli<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg>s</rhyme></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w>-<w n="8.6" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>, <w n="8.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="8.8">l</w>’<w n="8.9" punct="pt:8"><rhyme label="c" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababbcbc">
						<l n="9" num="2.1" lm="8" met="8"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg>s</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>bl<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="10" num="2.2" lm="8" met="8"><w n="10.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ds</w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>li<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</rhyme></w></l>
						<l n="11" num="2.3" lm="8" met="8"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="11.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>squ</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.7" punct="pv:8">c<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="12" num="2.4" lm="8" met="8"><w n="12.1">C<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w>-<w n="12.2" punct="vg:2">l<seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="vg">à</seg></w>, <w n="12.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.5" punct="vg:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>li<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg>s</rhyme></w>,</l>
						<l n="13" num="2.5" lm="8" met="8"><w n="13.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="366" place="1">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="13.3">rh<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="13.4" punct="vg:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="7">u</seg>li<rhyme label="b" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg>s</rhyme></w>,</l>
						<l n="14" num="2.6" lm="8" met="8"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="14.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>j<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="14.3">l</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="14.5">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="c" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="15" num="2.7" lm="8" met="8"><w n="15.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="15.5" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>li<rhyme label="b" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg>s</rhyme></w> ;</l>
						<l n="16" num="2.8" lm="8" met="8"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w>-<w n="16.6" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>, <w n="16.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="16.8">l</w>’<w n="16.9" punct="pt:8"><rhyme label="c" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababbcbc">
						<l n="17" num="3.1" lm="8" met="8"><w n="17.1">D</w>’<w n="17.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="17.4">ti<seg phoneme="ɛ" type="vs" value="1" rule="366" place="5">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.6" punct="pv:8">l<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="18" num="3.2" lm="8" met="8"><w n="18.1">D</w>’<w n="18.2" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="18.3">s</w>’<w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="18.6" punct="vg:8">p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>li<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg>s</rhyme></w>,</l>
						<l n="19" num="3.3" lm="8" met="8"><w n="19.1">H<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="19.3">s<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="20" num="3.4" lm="8" met="8"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="20.3" punct="vg:3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>ts</w>, <w n="20.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="20.6" punct="pv:8">b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>li<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg>s</rhyme></w> ;</l>
						<l n="21" num="3.5" lm="8" met="8"><w n="21.1">D</w>’<w n="21.2" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="21.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="21.5">ge<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>li<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</rhyme></w></l>
						<l n="22" num="3.6" lm="8" met="8"><w n="22.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="22.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="22.3">cr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.5">R<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="22.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.7">l</w>’<w n="22.8" punct="vg:8"><rhyme label="c" id="12" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">I</seg>sl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="23" num="3.7" lm="8" met="8"><w n="23.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="23.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="23.3">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="23.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="23.6" punct="pv:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>lli<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg>s</rhyme></w> ;</l>
						<l n="24" num="3.8" lm="8" met="8"><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="24.5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w>-<w n="24.6" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>, <w n="24.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="24.8">l</w>’<w n="24.9" punct="pt:8"><rhyme label="c" id="12" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="bcbc">
						<head type="form">Envoi</head>
						<l n="25" num="4.1" lm="8" met="8"><w n="25.1">G<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="25.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="25.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="25.4">j<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>illi<rhyme label="b" id="13" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</rhyme></w></l>
						<l n="26" num="4.2" lm="8" met="8"><w n="26.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="26.2" punct="vg:2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="26.4">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="26.6">L<rhyme label="c" id="14" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>sl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="27" num="4.3" lm="8" met="8"><w n="27.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.2">l</w>’<w n="27.3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r</w> <w n="27.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="27.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="27.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>li<rhyme label="b" id="13" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg>s</rhyme></w> ;</l>
						<l n="28" num="4.4" lm="8" met="8"><w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="28.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="28.5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w>-<w n="28.6" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>, <w n="28.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="28.8">l</w>’<w n="28.9" punct="pt:8"><rhyme label="c" id="14" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1869">Août 1869.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>