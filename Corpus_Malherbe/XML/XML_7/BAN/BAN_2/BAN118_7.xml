<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN118" modus="sm" lm_max="7" metProfile="7" form="villanelle" schema="AbA 6(abA) abAA">
				<head type="main">VILLANELLE <lb></lb>DE BULOZ</head>
				<lg n="1" rhyme="AbA">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="1.5" punct="dp:7">L<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="6">ay</seg>r<rhyme label="A" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="dp">a</seg>c</rhyme></w> :</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p</w>-<w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="2.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5" punct="pt:7">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="3.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="3.7" punct="pt:7">s<rhyme label="A" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>c</rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abA">
					<l n="4" num="2.1" lm="7" met="7"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3" punct="vg:4">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="4.5" punct="vg:7">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rn<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg>c</rhyme></w>,</l>
					<l n="5" num="2.2" lm="7" met="7"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <hi rend="ital"><w n="5.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="5.4" punct="pt:7">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>mm<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</hi></l>
					<l n="6" num="2.3" lm="7" met="7"><w n="6.1">J</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="6.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="6.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.5" punct="pt:7">L<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="6">ay</seg>r<rhyme label="A" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>c</rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="abA">
					<l n="7" num="3.1" lm="7" met="7"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2">L<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="7.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="7.4">B<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lz<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c</rhyme></w></l>
					<l n="8" num="3.2" lm="7" met="7"><w n="8.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l</w> <w n="8.3">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="4">eu</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="8.5" punct="pt:7">v<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="9" num="3.3" lm="7" met="7"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="9.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="9.5">d</w>’<w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="9.7" punct="pt:7">s<rhyme label="A" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>c</rhyme></w>.</l>
				</lg>
				<lg n="4" rhyme="abA">
					<l n="10" num="4.1" lm="7" met="7"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="10.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="10.4">m<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>lm<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch</rhyme></w></l>
					<l n="11" num="4.2" lm="7" met="7"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="11.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.5" punct="pt:7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="12" num="4.3" lm="7" met="7"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="12.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.5" punct="pt:7">L<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="6">ay</seg>r<rhyme label="A" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>c</rhyme></w>.</l>
				</lg>
				<lg n="5" rhyme="abA">
					<l n="13" num="5.1" lm="7" met="7"><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c</w>-<w n="13.5">m<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c</rhyme></w></l>
					<l n="14" num="5.2" lm="7" met="7"><w n="14.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="14.2">B<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3" punct="pt:7">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="15" num="5.3" lm="7" met="7"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="15.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="15.5">d</w>’<w n="15.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="15.7" punct="pt:7">s<rhyme label="A" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>c</rhyme></w>.</l>
				</lg>
				<lg n="6" rhyme="abA">
					<l n="16" num="6.1" lm="7" met="7"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="16.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="16.5" punct="vg:7">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>s<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg>c</rhyme></w>,</l>
					<l n="17" num="6.2" lm="7" met="7"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">j</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="17.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="17.6">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.7" punct="pt:7">ti<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="18" num="6.3" lm="7" met="7"><w n="18.1">J</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="18.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="18.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="18.5" punct="pt:7">L<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="6">ay</seg>r<rhyme label="A" id="9" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>c</rhyme></w>.</l>
				</lg>
				<lg n="7" rhyme="abA">
					<l n="19" num="7.1" lm="7" met="7"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="19.2" punct="vg:3">f<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg></w>, <w n="19.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="19.4">n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l</w> <w n="19.5" punct="pe:7">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pe">a</seg>c</rhyme></w> !</l>
					<l n="20" num="7.2" lm="7" met="7"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="20.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="20.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">m</w>’<w n="20.6" punct="pt:7"><seg phoneme="e" type="vs" value="1" rule="354" place="6">e</seg>x<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					<l n="21" num="7.3" lm="7" met="7"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="21.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="21.5">d</w>’<w n="21.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="21.7" punct="pt:7">s<rhyme label="A" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>c</rhyme></w>.</l>
				</lg>
				<lg n="8" rhyme="abAA">
					<l n="22" num="8.1" lm="7" met="7"><w n="22.1" punct="vg:2">Pl<seg phoneme="ø" type="vs" value="1" rule="405" place="1">eu</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="22.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>gn<rhyme label="a" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c</rhyme></w></l>
					<l n="23" num="8.2" lm="7" met="7"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="23.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.3">pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="23.5" punct="pe:7">p<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
					<l n="24" num="8.3" lm="7" met="7"><w n="24.1">J</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="24.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="24.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="24.5" punct="vg:7">L<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="323" place="6">ay</seg>r<rhyme label="A" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg>c</rhyme></w>,</l>
					<l n="25" num="8.4" lm="7" met="7"><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="25.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="25.5">d</w>’<w n="25.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="25.7" punct="pe:7">s<rhyme label="A" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pe">a</seg>c</rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1845">octobre 1845</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>