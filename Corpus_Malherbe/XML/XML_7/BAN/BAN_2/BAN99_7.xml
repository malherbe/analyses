<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RONDEAUX</head><div type="poem" key="BAN99" modus="cp" lm_max="10" metProfile="2, 4+6" form="rondeau non classique" schema="ababa bbaC abaabC">
					<head type="main">ARSÈNE</head>
					<lg n="1" rhyme="ababa">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="1.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="Lp">ai</seg>t</w>-<w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="1.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="1.5">s</w>’<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="1.7" punct="vg:8">d<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>, <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rm<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">m<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="2.3" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg" caesura="1">e</seg>rts</w>,<caesura></caesura> <w n="2.4">qu</w>’<w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="2.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="2.9" punct="pi:10">S<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">S<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="3.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="P">a</seg>r</w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="3.7" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="170" place="8" mp="M">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="vg">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="4.2" punct="pe:4">h<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe" caesura="1">a</seg>s</w> !<caesura></caesura> <w n="4.3">P<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="4.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7" mp="M">i</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="4.7">s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1" punct="vg:4">L<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="5.7" punct="pt:10">g<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>m<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="bbaC">
						<l n="6" num="2.1" lm="10" met="4+6"><w n="6.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="P">a</seg>r</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="6.5">j<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="6.8" punct="vg:10">sc<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="7" num="2.2" lm="10" met="4+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="7.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="7.7" punct="vg:10">Cl<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>m<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="2.3" lm="10" met="4+6"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="8.3">T<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="8.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.7">L<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></rhyme></w>-</l>
						<l n="9" num="2.4" lm="2" met="2"><space quantity="18" unit="char"></space><w n="9.1" punct="pt:2">H<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ss<rhyme label="C" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" punct="pt">e</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="abaabC">
						<l n="10" num="3.1" lm="10" met="4+6"><w n="10.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="10.2">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="10.4" punct="vg:4">f<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="10.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="5">œ</seg>il</w> <w n="10.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="10.9" punct="vg:10">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9" mp="M">ê</seg>m<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg></rhyme></w>,</l>
						<l n="11" num="3.2" lm="10" met="4+6"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="11.2" punct="vg:2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" caesura="1">è</seg>s</w><caesura></caesura> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="11.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.7" punct="vg:10">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>t<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="3.3" lm="10" met="4+6"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="12.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ls</w> <w n="12.3">bu<seg phoneme="i" type="vs" value="1" rule="491" place="3" mp="M">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="12.5">s</w>’<w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="12.7" punct="pe:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rm<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pe">i</seg></rhyme></w> !</l>
						<l n="13" num="3.4" lm="10" met="4+6"><w n="13.1">H<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="13.3" punct="vg:4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4" punct="vg" caesura="1">ain</seg></w>,<caesura></caesura> <w n="13.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="6" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="13.6" punct="vg:10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg></rhyme></w>,</l>
						<l n="14" num="3.5" lm="10" met="4+6"><w n="14.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="14.2">d</w>’<w n="14.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" caesura="1">o</seg>r</w><caesura></caesura> <w n="14.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="14.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="14.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="14.8">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="14.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">A</seg>rs<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="15" num="3.6" lm="2" met="2"><space quantity="18" unit="char"></space><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="15.2">c</w>’<w n="15.3" punct="pt:2"><rhyme label="C" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2" punct="pt">e</seg>st</rhyme></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1849">juillet 1849</date>.
						</dateline>
					</closer>
					</div></body></text></TEI>