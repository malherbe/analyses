<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN120" modus="sm" lm_max="7" metProfile="7" form="villanelle" schema="AbA 10(abA) abAA">
				<head type="main">VILLANELLE <lb></lb>DES PAUVRES HOUSSEURS</head>
				<opener>
					<epigraph>
						<cit>
							<quote>En avant, mes amis ; <lb></lb>sus au romantisme ; <lb></lb>
							Voltaire ET l’École normale.</quote>
							<bibl><hi rend="ital">Figaro</hi> du 30 décembre 1858.</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" rhyme="AbA">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="1.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="1.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>phl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="A" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>b<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</rhyme></w></l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>il</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5" punct="pt:7">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>lt<rhyme label="A" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abA">
					<l n="4" num="2.1" lm="7" met="7"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="4.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="5" num="2.2" lm="7" met="7"><w n="5.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="5.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5" punct="vg:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>d</rhyme></w>,</l>
					<l n="6" num="2.3" lm="7" met="7"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="6.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="6.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="6.4" punct="pt:7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>phl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="A" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="abA">
					<l n="7" num="3.1" lm="7" met="7"><w n="7.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="7.3">F<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="8" num="3.2" lm="7" met="7"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="8.3">fr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="8.4">qu</w>’<w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="8.6">m<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>d</rhyme></w></l>
					<l n="9" num="3.3" lm="7" met="7"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>il</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5" punct="pt:7">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>lt<rhyme label="A" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" rhyme="abA">
					<l n="10" num="4.1" lm="7" met="7"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4">c<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<rhyme label="a" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="11" num="4.2" lm="7" met="7"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="11.2">f<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="11.3" punct="vg:4">T<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" punct="vg">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.4" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="189" place="5" punct="vg">e</seg>t</w>, <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="11.6" punct="vg:7">g<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">oû</seg>t</rhyme></w>,</l>
					<l n="12" num="4.3" lm="7" met="7"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="12.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="12.4" punct="pt:7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>phl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="A" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="5" rhyme="abA">
					<l n="13" num="5.1" lm="7" met="7"><w n="13.1">Pr<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>d</w>’<w n="13.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3" punct="vg:7"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="5.2" lm="7" met="7"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3">l</w>’<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="14.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</rhyme></w></l>
					<l n="15" num="5.3" lm="7" met="7"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>il</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5" punct="pt:7">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>lt<rhyme label="A" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="6" rhyme="abA">
					<l n="16" num="6.1" lm="7" met="7"><w n="16.1" punct="vg:2">T<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="16.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w>-<w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="5">e</seg></w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="16.5" punct="vg:7">t<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="17" num="6.2" lm="7" met="7"><w n="17.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="17.3" punct="ps:3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="ps">a</seg></w>… <w n="17.4">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="4">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</rhyme></w></l>
					<l n="18" num="6.3" lm="7" met="7"><w n="18.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>il</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5" punct="pt:7">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>lt<rhyme label="A" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="7" rhyme="abA">
					<l n="19" num="7.1" lm="7" met="7"><w n="19.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="19.2">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="19.4" punct="pe:7">l<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
					<l n="20" num="7.2" lm="7" met="7"><w n="20.1" punct="pe:1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg>h</w> ! <w n="20.2">L</w>’<w n="20.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="20.4">r<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="20.6">l</w>’<w n="20.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="20.8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bs<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</rhyme></w></l>
					<l n="21" num="7.3" lm="7" met="7"><w n="21.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="21.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="21.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="21.4" punct="pt:7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>phl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="A" id="10" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="8" rhyme="abA">
					<l n="22" num="8.1" lm="7" met="7"><w n="22.1">B<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="22.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.4" punct="pv:7">c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<rhyme label="a" id="12" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="23" num="8.2" lm="7" met="7"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="23.4" punct="ps:5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="ps">é</seg></w>… <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.6">t<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</rhyme></w></l>
					<l n="24" num="8.3" lm="7" met="7"><w n="24.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>il</w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.5" punct="pt:7">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>lt<rhyme label="A" id="12" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="9" rhyme="abA">
					<l n="25" num="9.1" lm="7" met="7"><w n="25.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">c<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="25.3">ph<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>st<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="26" num="9.2" lm="7" met="7"><w n="26.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.3">d<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="26.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="26.5">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">oû</seg>t</rhyme></w></l>
					<l n="27" num="9.3" lm="7" met="7"><w n="27.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="27.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="27.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="27.4" punct="pt:7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>phl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="A" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="10" rhyme="abA">
					<l n="28" num="10.1" lm="7" met="7"><w n="28.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>s</w> <w n="28.2" punct="vg:3">P<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rg<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg></w>, <w n="28.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="28.5" punct="pe:7">cl<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>st<rhyme label="a" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
					<l n="29" num="10.2" lm="7" met="7"><w n="29.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="29.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="29.6">b<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</rhyme></w></l>
					<l n="30" num="10.3" lm="7" met="7"><w n="30.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="30.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="30.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>il</w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="30.5" punct="pt:7">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>lt<rhyme label="A" id="15" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="11" rhyme="abA">
					<l n="31" num="11.1" lm="7" met="7"><w n="31.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="31.2" punct="vg:2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>t</w>, <w n="31.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="31.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="31.5" punct="vg:7">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>st<rhyme label="a" id="16" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="32" num="11.2" lm="7" met="7"><w n="32.1">J<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="32.2">P<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="32.4">l</w>’<w n="32.5" punct="pi:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>g<rhyme label="b" id="17" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pi">ou</seg>t</rhyme></w> ?</l>
					<l n="33" num="11.3" lm="7" met="7"><w n="33.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="33.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="33.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="33.4" punct="pt:7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>phl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="A" id="16" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="12" rhyme="abAA">
					<l n="34" num="12.1" lm="7" met="7"><w n="34.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">F<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ey</seg></w> <w n="34.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="34.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="34.5" punct="vg:7">C<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>th<rhyme label="a" id="18" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="35" num="12.2" lm="7" met="7"><w n="35.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="35.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="35.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="35.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="35.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>squ</w>’<w n="35.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="35.7">b<rhyme label="b" id="17" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</rhyme></w></l>
					<l n="36" num="12.3" lm="7" met="7"><w n="36.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="36.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="36.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="36.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>phl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="A" id="18" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="37" num="12.4" lm="7" met="7"><w n="37.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="37.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="37.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>il</w> <w n="37.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="37.5" punct="pt:7">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>lt<rhyme label="A" id="18" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1858">décembre 1858</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>