<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" insert="2" modus="xx" key="BAN84" form="suite périodique" schema="2(ababcdcd)">
									<head type="main">CHANSON.</head>
									<div type="section" n="1">
										<head type="number">I</head>
										<lg n="1" type="huitain" rhyme="ababcdcd">
											<l n="167" n_ins="1" num_ins="1.1" lm="8" met="8"><space quantity="8" unit="char"></space><w n="167.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="167.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="167.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="167.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="167.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="167.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="167.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="167.8" punct="pe:8"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
											<l n="168" n_ins="2" num_ins="1.2" lm="8" met="8"><space quantity="8" unit="char"></space><w n="168.1" punct="vg:2">Pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rr<seg phoneme="o" type="vs" value="1" rule="438" place="2" punct="vg">o</seg>t</w>, <w n="168.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="168.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="168.4">cr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="168.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</rhyme></w></l>
											<l n="169" n_ins="3" num_ins="1.3" lm="8" met="8"><space quantity="8" unit="char"></space><w n="169.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="169.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="169.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="169.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="169.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="169.6" punct="dp:8">fl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
											<l n="170" n_ins="4" num_ins="1.4" lm="8" met="8"><space quantity="8" unit="char"></space><w n="170.1">C</w>’<w n="170.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="170.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="170.4" punct="vg:4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">em</seg>ps</w>, <w n="170.5">c</w>’<w n="170.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="170.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="170.8" punct="pe:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pe">e</seg>il</rhyme></w> !</l>
											<l n="171" n_ins="5" num_ins="1.5" lm="8" met="8"><space quantity="8" unit="char"></space><w n="171.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="171.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="2">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="171.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="171.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffr<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
											<l n="172" n_ins="6" num_ins="1.6" lm="8" met="8"><space quantity="8" unit="char"></space><w n="172.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="172.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="172.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="172.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="172.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="172.6" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></w> !</l>
											<l n="173" n_ins="7" num_ins="1.7" lm="8" met="8"><space quantity="8" unit="char"></space><w n="173.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pe">i</seg>s</w> ! <w n="173.2">V<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="173.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="173.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</w> <w n="173.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="173.6">Fr<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
											<l n="174" n_ins="8" num_ins="1.8" lm="8" met="8"><space quantity="8" unit="char"></space><w n="174.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="174.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="174.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="174.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="174.5" punct="pe:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></w> !</l>
										</lg>
									</div>
									<div type="section" n="2">
										<head type="number">II</head>
										<lg n="1" type="huitain" rhyme="ababcdcd">
											<l n="175" n_ins="9" num_ins="1.1" lm="8" met="8"><space quantity="8" unit="char"></space><w n="175.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="175.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="175.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="175.4" punct="vg:8">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
											<l n="176" n_ins="10" num_ins="1.2" lm="8" met="8"><space quantity="8" unit="char"></space><w n="176.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="176.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="176.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="176.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>ts</w> <w n="176.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rpr<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</rhyme></w>,</l>
											<l n="177" n_ins="11" num_ins="1.3" lm="8" met="8"><space quantity="8" unit="char"></space><w n="177.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="177.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="177.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="177.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="177.5">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
											<l n="178" n_ins="12" num_ins="1.4" lm="8" met="8"><space quantity="8" unit="char"></space><w n="178.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="178.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="178.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="178.4">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>ts</w> <w n="178.5" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cr<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</rhyme></w>.</l>
											<l n="179" n_ins="13" num_ins="1.5" lm="8" met="8"><space quantity="8" unit="char"></space><w n="179.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="179.2">j<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="179.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="179.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="179.5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="179.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
											<l n="180" n_ins="14" num_ins="1.6" lm="8" met="8"><space quantity="8" unit="char"></space><w n="180.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="180.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="180.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="180.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="180.5" punct="pe:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></w> !</l>
											<l n="181" n_ins="15" num_ins="1.7" lm="8" met="8"><space quantity="8" unit="char"></space><w n="181.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pe">i</seg>s</w> ! <w n="181.2">V<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="181.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="181.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</w> <w n="181.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="181.6">Fr<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
											<l n="182" n_ins="16" num_ins="1.8" lm="8" met="8"><space quantity="8" unit="char"></space><w n="182.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="182.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="182.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="182.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="182.5" punct="pe:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></w> !</l>
										</lg>
									</div>
								</div></body></text></TEI>