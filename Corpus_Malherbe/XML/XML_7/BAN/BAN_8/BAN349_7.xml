<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN349" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
				<head type="main">La Nuit</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="1.6" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg" caesura="1">œu</seg>rs</w>,<caesura></caesura> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="1.9" punct="vg:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Fl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="2.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="2.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="2.8" punct="vg:12">v<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M/mp">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="Lp">en</seg>ds</w>-<w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="3.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rds</w><caesura></caesura> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c</w> <w n="3.9">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10">ein</seg></w> <w n="3.10">d</w>’<w n="3.11"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="4.3">r<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>ls</w><caesura></caesura> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="4.5">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="8" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="4.6" punct="pi:12">g<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>si<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pi">er</seg>s</rhyme></w> ?</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="5.2" punct="vg:3">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="5.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>ts</w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</rhyme></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="6.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>ff<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="6.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="6.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg>s</w> <w n="6.9" punct="vg:12">v<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="7.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>tti<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.5">dr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ps</w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg>x</w> <w n="7.7">f<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="7.8">t<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="8.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="8.3" punct="vg:4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4" punct="vg">ein</seg></w>, <w n="8.4">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="5" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="8.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="8.7">b<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.9" punct="pi:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>si<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pi">er</seg>s</rhyme></w> ?</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="Lp">oi</seg>s</w>-<w n="9.2" punct="vg:2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg></w>, <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="9.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="9.8"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="9.9">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="9.10">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="9.11" punct="vg:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Fu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>r</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="10.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="10.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>cs</w><caesura></caesura> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="10.7" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>ss<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">M<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>s</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="11.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="11.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>r</w> <w n="11.9" punct="pi:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>sp<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pi">oi</seg>r</rhyme></w> ?</l>
				</lg>
				<lg n="4" rhyme="ede">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Ou</seg></w>, <w n="12.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2" mp="Lc">eu</seg>t</w>-<w n="12.3" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" punct="vg">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="12.7" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">Pi<seg phoneme="e" type="vs" value="1" rule="241" place="1">e</seg>ds</w> <w n="13.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="13.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.5" punct="vg:6">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="13.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="13.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="13.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="13.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="13.10">m<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>r<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</rhyme></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="14.3">ru<seg phoneme="i" type="vs" value="1" rule="491" place="3" mp="M">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="14.5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="14.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="14.7">j<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="Lc">u</seg>squ</w>’<w n="14.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="14.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="14.10" punct="pt:12">b<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1847">Octobre 1847.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>