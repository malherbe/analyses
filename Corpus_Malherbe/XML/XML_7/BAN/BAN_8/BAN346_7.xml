<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN346" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
				<head type="main">La Thessalie</head>
				<head type="sub_1">A Auguste Préault</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="1.2" punct="vg:4">Th<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="4" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6" caesura="1">e</seg>st</w><caesura></caesura> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="1.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="1.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>ts</w> <w n="1.8">p<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>tt<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="2.2">n<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>rs</w> <w n="2.3" punct="vg:4">v<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="2.4">j<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="2.9" punct="vg:12">r<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>cs</rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="3.6">f<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.9">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="3.10">ch<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>cs</rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg>ts</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rpr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.6">d</w>’<w n="4.7" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="4.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="4.10">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="4.11" punct="pt:12">fr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="5.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="457" place="3">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="5.7">p<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="5.8">gr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="6.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rp<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="6.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.7" punct="vg:12">bl<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>cs</rhyme></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2" punct="vg:3">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="7.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="7.8" punct="vg:12">s<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>cs</rhyme></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="8.6">T<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="8.7" punct="pt:12">g<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="9.2" punct="vg:3">gr<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>t</w>, <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg></w> <w n="9.7">n</w>’<w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="9.9" punct="vg:12">pl<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="10.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>cs</w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="10.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="10.8">f<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.4">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="11.7"><seg phoneme="œ" type="vs" value="1" rule="286" place="9">œ</seg>il</w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.9" punct="pt:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" rhyme="ede">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="12.2" punct="pe:4">st<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="3" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pe">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="12.7">l</w>’<w n="12.8"><seg phoneme="œ" type="vs" value="1" rule="286" place="10">œ</seg>il</w> <w n="12.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="12.10" punct="vg:12">f<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></rhyme></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="13.2">pi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>ds</w> <w n="13.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>lc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg></w><caesura></caesura> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="13.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="13.9">gu<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tr<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">D</w>’<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="14.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="14.5">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="14.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>l</w> <w n="14.9" punct="pt:12">bl<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg></rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1847">Octobre 1847.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>