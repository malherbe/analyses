<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN363" modus="sm" lm_max="7" metProfile="7" form="suite de strophes" schema="1[abab] 2[aa] 2[abba]">
				<head type="main">Le Vin de l’Amour</head>
				<lg n="1" type="regexp" rhyme="ababaaabbaaaabba">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.3" punct="vg:5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>f</w>, <w n="1.4">l</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">A</seg>m<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</rhyme></w></l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2" punct="vg:3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>t</w>, <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5" punct="vg:7">r<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="3.5">d</w>’<w n="3.6" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pt">ou</seg>r</rhyme></w>.</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="4.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t</w>, <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="4.5">l</w>’<w n="4.6" punct="vg:7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="7" met="7"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="5">œ</seg>il</w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>z<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</rhyme></w></l>
					<l n="6" num="1.6" lm="7" met="7"><w n="6.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="6.5">p<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</rhyme></w></l>
					<l n="7" num="1.7" lm="7" met="7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>rs</w> <w n="7.4">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="7.6" punct="pt:7">tr<rhyme label="a" id="4" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="8" num="1.8" lm="7" met="7"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="8.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="8.4" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp">i</seg>t</w> : <w n="8.5">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w>-<w n="8.6" punct="vg:7">m<rhyme label="b" id="5" gender="m" type="a" stanza="3"><seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="7" met="7"><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="9.3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="9.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="353" place="6">e</seg>ffr<rhyme label="b" id="5" gender="m" type="e" stanza="3"><seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></rhyme></w>,</l>
					<l n="10" num="1.10" lm="7" met="7"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ts</w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="10.4" punct="pe:7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rm<rhyme label="a" id="4" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg>s</rhyme></w> !</l>
					<l n="11" num="1.11" lm="7" met="7"><w n="11.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ts</w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="11.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<rhyme label="a" id="6" gender="m" type="a" stanza="4"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</rhyme></w></l>
					<l n="12" num="1.12" lm="7" met="7"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">f<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w>-<w n="12.4" punct="vg:7">n<rhyme label="a" id="6" gender="m" type="e" stanza="4"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg>s</rhyme></w>,</l>
					<l n="13" num="1.13" lm="7" met="7"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="13.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="13.6" punct="pt:7"><rhyme label="a" id="7" gender="f" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="14" num="1.14" lm="7" met="7"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="14.3">p<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="14.7">v<rhyme label="b" id="8" gender="m" type="a" stanza="5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg></rhyme></w></l>
					<l n="15" num="1.15" lm="7" met="7"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="15.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="15.3">ru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="15.5" punct="dp:7">v<rhyme label="b" id="8" gender="m" type="e" stanza="5"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7" punct="dp">ain</seg></rhyme></w> :</l>
					<l n="16" num="1.16" lm="7" met="7"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="16.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g</w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="16.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="16.7" punct="pt:7">l<rhyme label="a" id="7" gender="f" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1847">Juin 1847.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>