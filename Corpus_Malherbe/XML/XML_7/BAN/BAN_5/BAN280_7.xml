<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN280" modus="sp" lm_max="7" metProfile="7, 4" form="suite périodique" schema="7(aabccb)">
				<head type="number">LXXXVIII</head>
				<head type="main">La Fourmi et la cigale</head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1" punct="vg:2">L<seg phoneme="ɔ" type="vs" value="1" rule="317" place="1">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="1.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="1.5" punct="vg:7">gr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="2.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5">gr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="3" num="1.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>ts</rhyme></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="vg:7">v<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="7" met="7"><w n="5.1">Tr<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.7">p<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="6" num="1.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">tr<seg phoneme="o" type="vs" value="1" rule="433" place="2">o</seg>p</w> <w n="6.3">d</w>’<w n="6.4" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="pt">an</seg>ts</rhyme></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="7" num="2.1" lm="7" met="7"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2" punct="dp:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp">i</seg>t</w> : <w n="7.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="8" num="2.2" lm="7" met="7"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="8.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">l<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5" punct="vg:7">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>d<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="9" num="2.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.4" punct="pt:4"><rhyme label="b" id="5" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pt">o</seg>rs</rhyme></w>.</l>
					<l n="10" num="2.4" lm="7" met="7"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2" punct="vg:3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>, <w n="10.3">c</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="10.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="10.6">d</w>’<w n="10.7" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ff<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="11" num="2.5" lm="7" met="7"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="11.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="11.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.7">f<rhyme label="c" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="12" num="2.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3" punct="pt:4">tr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pt">o</seg>rs</rhyme></w>.</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="13" num="3.1" lm="7" met="7"><w n="13.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.4">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="14" num="3.2" lm="7" met="7"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">g<seg phoneme="u" type="vs" value="1" rule="425" place="2">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>br<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="15" num="3.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="15.3" punct="pt:4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pt">er</seg>s</rhyme></w>.</l>
					<l n="16" num="3.4" lm="7" met="7"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="16.5" punct="vg:7">p<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="17" num="3.5" lm="7" met="7"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">l<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>s</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="18" num="3.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="18.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="18.3" punct="pt:4"><seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="pt">é</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabccb">
					<l n="19" num="4.1" lm="7" met="7"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="19.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="19.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6" punct="pt:7">phr<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="20" num="4.2" lm="7" met="7"><w n="20.1">D</w>’<w n="20.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="307" place="1">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>rs</w>, <w n="20.3">j</w>’<w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="20.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.7">t<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>p<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="21" num="4.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3" punct="pt:4">r<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>b<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pt">i</seg>s</rhyme></w>.</l>
					<l n="22" num="4.4" lm="7" met="7"><w n="22.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t</w>-<w n="22.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="22.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="22.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="22.5">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="22.7" punct="vg:7">p<rhyme label="c" id="12" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="23" num="4.5" lm="7" met="7"><w n="23.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Ou</seg></w>, <w n="23.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="23.3" punct="vg:5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>pl<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5" punct="vg">en</seg>t</w>, <w n="23.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="23.5">c<rhyme label="c" id="12" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="24" num="4.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="24.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="24.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="24.3" punct="pi:4">h<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pi">i</seg>ts</rhyme></w> ?</l>
				</lg>
				<lg n="5" type="sizain" rhyme="aabccb">
					<l n="25" num="5.1" lm="7" met="7"><w n="25.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.3" punct="vg:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="25.5" punct="vg:7">pr<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="26" num="5.2" lm="7" met="7"><w n="26.1" punct="vg:2">L<seg phoneme="ɔ" type="vs" value="1" rule="317" place="1">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="26.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="26.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="26.5">r<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="27" num="5.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="27.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="27.2" punct="pt:4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pt">i</seg>t</rhyme></w>.</l>
					<l n="28" num="5.4" lm="7" met="7"><w n="28.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="28.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="28.3">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.4">d</w>’<w n="28.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="28.6">pr<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="29" num="5.5" lm="7" met="7"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2">d</w>’<w n="29.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="29.4" punct="pt:4">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>qui<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pt">er</seg></w>. <w n="29.5">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="29.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="29.7">m<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="30" num="5.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>rm<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="30.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="30.3" punct="dp:4">d<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp">i</seg>t</rhyme></w> :</l>
				</lg>
				<lg n="6" type="sizain" rhyme="aabccb">
					<l n="31" num="6.1" lm="7" met="7"><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">n</w>’<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="31.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="31.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="31.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="31.7" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<rhyme label="a" id="16" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="32" num="6.2" lm="7" met="7"><w n="32.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="32.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="32.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="32.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="32.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="32.6">m<rhyme label="a" id="16" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="33" num="6.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="33.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="33.3" punct="vg:4">ch<rhyme label="b" id="17" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="vg">e</seg>r</rhyme></w>,</l>
					<l n="34" num="6.4" lm="7" met="7"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="34.2" punct="vg:2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="34.3">l</w>’<w n="34.4" punct="vg:4">h<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="vg">e</seg>r</w>, <w n="34.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="34.6" punct="vg:7">fr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<rhyme label="c" id="18" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="35" num="6.5" lm="7" met="7"><w n="35.1">C</w>’<w n="35.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="35.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.4">j</w>’<w n="35.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="35.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="35.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="35.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rs<rhyme label="c" id="18" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="36" num="6.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="36.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="36.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="36.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="36.4" punct="pt:4">ch<rhyme label="b" id="17" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pt">ai</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="7" type="sizain" rhyme="aabccb">
					<l n="37" num="7.1" lm="7" met="7"><w n="37.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="37.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="37.3">f<seg phoneme="œ" type="vs" value="1" rule="304" place="3">ai</seg>s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="37.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="37.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<rhyme label="a" id="19" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="38" num="7.2" lm="7" met="7"><w n="38.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="38.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="38.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="3">o</seg>p</w> <w n="38.4">d</w>’<w n="38.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="38.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="38.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="38.8">p<rhyme label="a" id="19" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="39" num="7.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="39.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="39.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="39.3" punct="vg:4">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<rhyme label="b" id="20" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg></rhyme></w>,</l>
					<l n="40" num="7.4" lm="7" met="7"><w n="40.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="40.2">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="40.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="40.4" punct="vg:5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>t</w>, <w n="40.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="40.6" punct="vg:7">b<rhyme label="c" id="21" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="41" num="7.5" lm="7" met="7"><w n="41.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="41.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="41.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="41.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="41.5" punct="vg:7">P<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>b<rhyme label="c" id="21" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="42" num="7.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="42.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="42.2">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="42.3">m</w>’<w n="42.4" punct="pe:4"><rhyme label="b" id="20" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4" punct="pe">en</seg></rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">8 mars 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>