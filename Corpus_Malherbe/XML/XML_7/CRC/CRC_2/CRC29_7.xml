<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Petits airs</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>280 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_2</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Petits airs</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/petitsairspome00carc/page/24/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies complètes</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RONALD DAVIS ET CIE</publisher>
									<date when="1920">1920</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRC29" rhyme="none" modus="cp" lm_max="12" metProfile="8, 6+6, 4+6">
		<head type="main">Laure</head>
			<opener>
				<salute><hi rend="smallcap">AU SOUVENIR DE JEAN-MARC BERNARD</hi>.</salute>
			</opener>
		<lg n="1">
			<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:2">L<seg phoneme="ɔ" type="vs" value="1" rule="317" place="1">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="1.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>ts</w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
			<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="i" type="vs" value="1" rule="dc-1" place="4">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg>s</w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w></l>
			<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="3.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="3.5">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="3.6">d</w>’<w n="3.7" punct="ps:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="ps">ou</seg>r</w>…</l>
			<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="4.4" punct="vg:6">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>rpr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="4.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="4.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>ssi<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="4.8" punct="pt:12">p<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
		</lg>
		<lg n="2">
			<l n="5" num="2.1" lm="12" met="6+6">— <w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="5.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="5.3" punct="pi:3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pi">a</seg>l</w> ? <w n="5.4">D<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M/mp">i</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="5.5" punct="ps:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="ps" caesura="1">ou</seg>s</w>…<caesura></caesura> <w n="5.6">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.8">t</w>’<w n="5.9"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg></w> <w n="5.10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="5.11" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
			<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="6.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rds</w> <w n="6.5" punct="pt:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pt">an</seg>ts</w>.</l>
			<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="7.4" punct="pi:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pi" caesura="1">a</seg>l</w> ?<caesura></caesura> <w n="7.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="7.7" punct="vg:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="vg">en</seg>t</w>,</l>
			<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="8.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="8.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="8.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.10" punct="pi:12">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
		</lg>
	</div></body></text></TEI>