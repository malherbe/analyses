<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Au vent crispé du matin</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>407 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_3</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Au vent crispé du matin</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/auventcrispdum00carc/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Au vent crispé du matin</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Nouvelle Édition Nouvelle</publisher>
									<date when="1913">1913</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1913">1913</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS AIGRES-DOUCES</head><div type="poem" key="CRC52" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="8(abab)">
			<head type="main"><hi rend="ital">L’Illusion</hi></head>
			<div n="1" type="section">
			<head type="number">I</head>
			<lg n="1" type="quatrain" rhyme="abab">
				<l n="1" num="1.1" lm="7" met="7"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">r<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.4">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>fl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
				<l n="2" num="1.2" lm="7" met="7"><w n="2.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="2.4" punct="vg:7">cl<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>r</rhyme></w>,</l>
				<l n="3" num="1.3" lm="7" met="7"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" punct="vg">e</seg>t</w>, <w n="3.3">l</w>’<w n="3.4" punct="tc:7">h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="ti">e</seg></rhyme></w> —</l>
				<l n="4" num="1.4" lm="7" met="7"><w n="4.1">Fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="4.2" punct="tc:4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="ti">e</seg>s</w> — <w n="4.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="4.4">l</w>’<w n="4.5"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r</rhyme></w></l>
			</lg>
			<lg n="2" type="quatrain" rhyme="abab">
				<l n="5" num="2.1" lm="7" met="7"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">s</w>’<w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>br<seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.6">l<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>mi<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
				<l n="6" num="2.2" lm="7" met="7"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">m<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5" punct="pt:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>fl<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="7" punct="pt">e</seg>ts</rhyme></w>.</l>
				<l n="7" num="2.3" lm="7" met="7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>mi<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
				<l n="8" num="2.4" lm="7" met="7"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="8.6" punct="pt:7">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>l<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="pt">ai</seg>s</rhyme></w>.</l>
			</lg>
			</div>
			<div n="2" type="section">
			<head type="number">II</head>
			<lg n="1" type="quatrain" rhyme="abab">
				<l n="9" num="1.1" lm="7" met="7"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3">en</seg>t</w> <w n="9.3">t</w>’<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<rhyme label="a" id="5" gender="f" type="a" part="I"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></rhyme></w>-<w n="9.5" punct="vg:7"><rhyme label="a" id="5" gender="f" type="a" part="F">j<seg phoneme="ə" type="ef" value="1" rule="e-1" place="8" punct="vg">e</seg></rhyme></w>,</l>
				<l n="10" num="1.2" lm="7" met="7"><w n="10.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="10.2" punct="vg:2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="10.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.5" punct="vg:5">br<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="10.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</rhyme></w></l>
				<l n="11" num="1.3" lm="7" met="7"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="11.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="11.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="11.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.6" punct="vg:7">n<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
				<l n="12" num="1.4" lm="7" met="7"><w n="12.1">R<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="12.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="12.4" punct="pi:7">c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>br<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pi">i</seg></rhyme></w> ?</l>
			</lg>
			<lg n="2" type="quatrain" rhyme="abab">
				<l n="13" num="2.1" lm="7" met="7"><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="13.2">n</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="13.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="13.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6" punct="vg:7">s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>du<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
				<l n="14" num="2.2" lm="7" met="7"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="14.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="14.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd</w> <w n="14.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>str<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</rhyme></w></l>
				<l n="15" num="2.3" lm="7" met="7"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="15.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="15.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
				<l n="16" num="2.4" lm="7" met="7"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4" punct="ps:5"><seg phoneme="e" type="vs" value="1" rule="189" place="5" punct="ps">e</seg>t</w>… <w n="16.5">s</w>’<w n="16.6" punct="pt:7"><seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>ffr<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="pt">ai</seg>t</rhyme></w>.</l>
			</lg>
			</div>
			<div n="3" type="section">
			<head type="number">III</head>
			<lg n="1" type="quatrain" rhyme="abab">
				<l n="17" num="1.1" lm="7" met="7"><w n="17.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="17.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
				<l n="18" num="1.2" lm="7" met="7"><w n="18.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2" punct="vg:3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="18.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="18.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="18.6" punct="vg:7">gr<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>s</rhyme></w>,</l>
				<l n="19" num="1.3" lm="7" met="7"><w n="19.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="19.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt</w> <w n="19.4">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>t<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
				<l n="20" num="1.4" lm="7" met="7"><w n="20.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="20.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="20.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="20.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="20.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.7" punct="pt:7">pr<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg>x</rhyme></w>.</l>
			</lg>
			<lg n="2" type="quatrain" rhyme="abab">
				<l n="21" num="2.1" lm="7" met="7"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="21.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="21.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="21.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.6" punct="vg:7">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
				<l n="22" num="2.2" lm="7" met="7"><w n="22.1">H<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="22.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="22.4" punct="ps:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="ps">ai</seg>s</rhyme></w>…</l>
				<l n="23" num="2.3" lm="7" met="7"><w n="23.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="23.2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w>-<w n="23.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="23.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
				<l n="24" num="2.4" lm="7" met="7"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="24.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.5">t</w>’<w n="24.6" punct="pi:7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="pi">ai</seg>s</rhyme></w> ?</l>
			</lg>
			</div>
			<div n="4" type="section">
			<head type="number">IV</head>
			<lg n="1" type="quatrain" rhyme="abab">
				<l n="25" num="1.1" lm="7" met="7"><w n="25.1" punct="pe:1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe ps">a</seg>rs</w> ! … <w n="25.2">T<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="25.3">n</w>’<w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="3">e</seg>s</w> <w n="25.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="25.6" punct="dp:7">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="7">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></rhyme></w> :</l>
				<l n="26" num="1.2" lm="7" met="7"><w n="26.1">J</w>’<w n="26.2" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" punct="vg">en</seg>ds</w>, <w n="26.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.5" punct="vg:7">v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="7" punct="vg">e</seg>t</rhyme></w>,</l>
				<l n="27" num="1.3" lm="7" met="7"><w n="27.1">Fr<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="27.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="27.4" punct="pt:7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="7">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
				<l n="28" num="1.4" lm="7" met="7"><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="28.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="28.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="28.4">t</w>’<w n="28.5" punct="ps:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<rhyme label="b" id="16" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="ps">er</seg></rhyme></w>…</l>
			</lg>
			<lg n="2" type="quatrain" rhyme="abab">
				<l n="29" num="2.1" lm="7" met="7"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="29.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="29.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="29.5" punct="tc:6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="ti">e</seg></w> — <w n="29.6">d<rhyme label="a" id="14" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
				<l n="30" num="2.2" lm="7" met="7"><w n="30.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="30.3" punct="tc:3">r<seg phoneme="o" type="vs" value="1" rule="318" place="3" punct="ti">au</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> — <w n="30.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="30.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="30.6" punct="vg:7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>tt<rhyme label="b" id="15" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>t</rhyme></w>,</l>
				<l n="31" num="2.3" lm="7" met="7"><w n="31.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="31.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="31.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="31.5" punct="vg:7">d<rhyme label="a" id="14" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
				<l n="32" num="2.4" lm="7" met="7"><w n="32.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="32.2">l</w>’<w n="32.3" punct="vg:3">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>r</w>, <w n="32.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="32.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="32.6" punct="pe:7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>t<rhyme label="b" id="15" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" punct="pe">em</seg>ps</rhyme></w> !</l>
			</lg>
			</div>
		</div></body></text></TEI>