<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR63" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
						<head type="main">UN POÈTE</head>
						<lg n="1" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg><seg phoneme="i" type="vs" value="1" rule="477" place="3">ï</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="1.4">r<seg phoneme="i" type="vs" value="1" rule="493" place="8" mp="M">y</seg>thm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="1.6" punct="vg:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>gl<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>ts</rhyme></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="2.2">s</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="2.4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="2.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="2.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="2.8" punct="vg:12">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="3.9">n<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="4.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="4.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="8" mp="M">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="403" place="9" mp="M">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="11">en</seg>t</w> <w n="4.7" punct="pt:12">cl<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pt">o</seg>s</rhyme></w>.</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">A</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="5.3">sc<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="5.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="5.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="5.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="5.8" punct="dp:12">l<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="dp">o</seg>s</rhyme></w> :</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="6.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="6.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rds</w> <w n="6.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="6.7" punct="pv:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">â</seg>t<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2" punct="vg:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>t</w>, <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>dr<seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.6" punct="vg:12">st<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3">str<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="8.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="8.9" punct="pt:12">fl<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pt">o</seg>ts</rhyme></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c</w> <w n="9.4">d</w>’<w n="9.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.8">p<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="9.9">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t</w> <w n="9.10" punct="vg:12">b<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="10.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="9">ue</seg>il</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="10.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="10.10" punct="vg:12">gl<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="11.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" mp="M">im</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.7">s</w>’<w n="11.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>t</rhyme></w>.</l>
						</lg>
						<lg n="4" rhyme="ede">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="12.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="Lc">o</seg>rsqu</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="12.4" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="12.5">l</w>’<w n="12.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="12.8">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg></w> <w n="12.9" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rt<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="Lc">o</seg>rsqu</w>’<w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="13.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="13.5">s</w>’<w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="13.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="13.9" punct="vg:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>t</rhyme></w>,</l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="14.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="14.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="14.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="14.5" punct="vg:6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="14.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="14.7">m<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>t</w> <w n="14.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="14.9">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="11">en</seg></w> <w n="14.10" punct="pt:12">d<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>