<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR51" modus="cm" lm_max="12" metProfile="6+6" form="terza rima classique" schema="5(aba) b">
						<head type="main">CONSEIL DU MAÎTRE</head>
						<opener>
							<salute>À LUCIEN ROSSELET</salute>
						</opener>
						<lg n="1" rhyme="aba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.4" punct="dp:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp in">i</seg>t</w> : « <w n="1.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="1.6" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">en</seg>ds</w>,<caesura></caesura> » <w n="1.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="1.8">j</w>’<w n="1.9"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="1.10" punct="dp:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="dp in">i</seg>t</w> : « <w n="1.11">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="1.12" punct="pt:12">v<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></w>. »</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="2.2">l<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="2.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="2.6">m<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>ts</w> <w n="2.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="2.8">l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>st<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3">M<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="3.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="3.8" punct="pt:12">v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="248" place="12" punct="pt">œu</seg>x</rhyme></w>.</l>
						</lg>
						<lg n="2" rhyme="bcb">
							<l n="4" num="2.1" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="4.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="4.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="4.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>pr<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="4.8" punct="pv:12">t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
							<l n="5" num="2.2" lm="12" met="6+6"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="5.2">cr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t</w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="5.8" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></rhyme></w>,</l>
							<l n="6" num="2.3" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="6.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="6.5">d<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>il</w><caesura></caesura> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="6.7">f<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="6.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="6.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="6.10" punct="pt:12">t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="3" rhyme="cac">
							<l n="7" num="3.1" lm="12" met="6+6"><w n="7.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="7.2" punct="pe:3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pe">i</seg>ls</w> ! <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">I</seg>l</w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="7.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="7.8">l</w>’<w n="7.9" punct="dp:12">h<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>z<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="dp">on</seg></rhyme></w> :</l>
							<l n="8" num="3.2" lm="12" met="6+6"><w n="8.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="8.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ls</w><caesura></caesura> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.7">r<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="8.8">gr<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="9" num="3.3" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="9.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt</w> <w n="9.6">r<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.9" punct="pe:12">pr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg></rhyme></w> !</l>
						</lg>
						<lg n="4" rhyme="aba">
							<l n="10" num="4.1" lm="12" met="6+6"><w n="10.1">Cr<seg phoneme="i" type="vs" value="1" rule="469" place="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="10.3">fr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="10.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="10.6" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="10.7">j<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="Lc">u</seg>squ</w>’<w n="10.8"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="10.9">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="10.10">qu</w>’<w n="10.11"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="10.12" punct="pv:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>p<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
							<l n="11" num="4.2" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9" mp="P">e</seg>rs</w> <w n="11.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</rhyme></w></l>
							<l n="12" num="4.3" lm="12" met="6+6"><w n="12.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="12.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="12.5">f<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="12.8">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="12.10" punct="pt:12">m<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="5" rhyme="bcb">
							<l n="13" num="5.1" lm="12" met="6+6"><w n="13.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="Lp">a</seg></w>-<w n="13.2">t</w>’<w n="13.3" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2" punct="vg">en</seg></w>, <w n="13.4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d</w> <w n="13.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="13.7" punct="vg:6">di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="13.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="13.9" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="13.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="13.11" punct="pt:12">v<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>r</rhyme></w>.</l>
							<l n="14" num="5.2" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="14.5">s<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>ts</w><caesura></caesura> <w n="14.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="14.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="14.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="14.9" punct="dp:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
							<l n="15" num="5.3" lm="12" met="6+6"><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="15.2" punct="pe:2">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="pe">oi</seg>t</w> ! <w n="15.3">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="15.6">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg>s</w> <w n="15.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="15.8">t</w>’<w n="15.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>r</rhyme></w>.</l>
						</lg>
						<lg n="6" rhyme="c">
							<l n="16" num="6.1" lm="12" met="6+6"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="16.3">l</w>’<w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="16.5">d</w>’<w n="16.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="16.7">f<seg phoneme="u" type="vs" value="1" rule="426" place="6" caesura="1">ou</seg></w><caesura></caesura> <w n="16.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="16.9">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="16.10">v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="16.11"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.12"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="16.13" punct="pe:12">s<rhyme label="c" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						</lg>
					</div></body></text></TEI>