<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">L’AUBE</head><div type="poem" key="HAR104" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="3(aabccb)">
						<head type="main">ROMANCE</head>
						<lg n="1" type="sizain" rhyme="aabccb">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="1.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="1.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="1.6" punct="vg:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.4" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">m<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.7" punct="vg:8">m<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>gu<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>ts</rhyme></w>,</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.5" punct="pv:8">pr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="5" num="1.5" lm="8" met="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6" punct="vg:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.4" punct="vg:3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg></w>, <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.8" punct="pt:8">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pt">e</seg>ts</rhyme></w>.</l>
						</lg>
						<lg n="2" type="sizain" rhyme="aabccb">
							<l n="7" num="2.1" lm="8" met="8"><w n="7.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="7.4">br<seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="7.6" punct="vg:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
							<l n="8" num="2.2" lm="8" met="8"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.5" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
							<l n="9" num="2.3" lm="8" met="8"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">l<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rds</w> <w n="9.3" punct="vg:4">gl<seg phoneme="a" type="vs" value="1" rule="343" place="3">a</seg>ï<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>ls</w>, <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="9.6" punct="vg:8">fl<seg phoneme="y" type="vs" value="1" rule="454" place="7">u</seg><rhyme label="b" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>ts</rhyme></w>,</l>
							<l n="10" num="2.4" lm="8" met="8"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>r</w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="10.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="10.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="10.7" punct="pe:8">s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>v<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
							<l n="11" num="2.5" lm="8" met="8"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="11.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6" punct="vg:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<rhyme label="c" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="12" num="2.6" lm="8" met="8"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.4" punct="vg:3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg></w>, <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.8" punct="pt:8">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg><rhyme label="b" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pt">e</seg>ts</rhyme></w>.</l>
						</lg>
						<lg n="3" type="sizain" rhyme="aabccb">
							<l n="13" num="3.1" lm="8" met="8"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3">bl<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="13.4" punct="vg:4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>ds</w>, <w n="13.5" punct="vg:5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>rs</w>, <w n="13.6" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>rs</w>, <w n="13.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.8" punct="pe:8">m<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
							<l n="14" num="3.2" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="14.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
							<l n="15" num="3.3" lm="8" met="8"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>ts</w> <w n="15.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="15.5" punct="pv:8">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pv">e</seg>ts</rhyme></w> ;</l>
							<l n="16" num="3.4" lm="8" met="8"><w n="16.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="16.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="16.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="17" num="3.5" lm="8" met="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">c</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="17.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="17.5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>pr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="17.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.8">j</w>’<w n="17.9"><rhyme label="c" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
							<l n="18" num="3.6" lm="8" met="8"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3" punct="vg:3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg></w>, <w n="18.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.5">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="18.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="18.7" punct="pt:8">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pt">e</seg>ts</rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>