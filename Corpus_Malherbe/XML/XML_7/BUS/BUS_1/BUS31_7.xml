<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS DE TOUS LES PAYS</head><div type="poem" key="BUS31" modus="cp" lm_max="10" metProfile="8, 5+5, (3), (5)" form="suite périodique avec alternance de type 1" schema="5{1(abab) 1(aaa)}">
					<head type="main">BARCAROLLE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Che cosa vecol ch’io peschi ? <lb></lb>
									L’anel che m’è cascà.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="1.4" punct="vg:5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg" caesura="1">ou</seg>rs</w>,<caesura></caesura> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="1.6">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7" mp="M">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="1.8" punct="vg:10">T<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="3"><space unit="char" quantity="14"></space><w n="2.1" punct="pe:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg></rhyme></w> !</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" mp="M">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w> <w n="3.7" punct="vg:10">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>f<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="3"><space unit="char" quantity="14"></space><w n="4.1" punct="pe:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg></rhyme></w> !</l>
					</lg>
					<lg n="2" type="tercet" rhyme="aaa">
						<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">s</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="5.6" punct="pt:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></rhyme></w>.</l>
						<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="6.2" punct="vg:3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="6.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="6.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="6.6">l</w>’<w n="6.7" punct="pv:8"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pv">eau</seg></rhyme></w> ;</l>
						<l n="7" num="2.3" lm="5"><space unit="char" quantity="10"></space><w n="7.1" punct="vg:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="7.2" punct="vg:4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="7.3" punct="pe:5">l<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="pe">o</seg></rhyme></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="8" num="3.1" lm="10" met="5+5"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="Lp">au</seg>t</w>-<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" mp="M">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="8.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>r</w> <w n="8.6" punct="vg:7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>s</w>, <w n="8.7"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg></w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="8.9" punct="pi:10">b<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
						<l n="9" num="3.2" lm="3"><space unit="char" quantity="14"></space><w n="9.1" punct="pe:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg></rhyme></w> !</l>
						<l n="10" num="3.3" lm="10" met="5+5">— <w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="10.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6" mp="P">à</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="10.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w> <w n="10.8" punct="pt:10">cr<seg phoneme="y" type="vs" value="1" rule="454" place="9" mp="M">u</seg><rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="11" num="3.4" lm="3"><space unit="char" quantity="14"></space><w n="11.1" punct="pe:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg></rhyme></w> !</l>
					</lg>
					<lg n="4" type="tercet" rhyme="aaa">
						<l n="12" num="4.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="12.2">s</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="12.6" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="13" num="4.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2" punct="vg:3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="13.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="13.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="13.6">l</w>’<w n="13.7" punct="pv:8"><rhyme label="a" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pv">eau</seg></rhyme></w> ;</l>
						<l n="14" num="4.3" lm="5"><space unit="char" quantity="10"></space><w n="14.1" punct="vg:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="14.2" punct="vg:4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="14.3" punct="pe:5">l<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="pe">o</seg></rhyme></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="15" num="5.1" lm="10" met="5+5"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="15.3" punct="vg:5">d<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" punct="vg" caesura="1">ai</seg></w>,<caesura></caesura> <w n="15.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>r</w> <w n="15.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="15.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="15.7" punct="vg:10">p<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="16" num="5.2" lm="3"><space unit="char" quantity="14"></space><w n="16.1" punct="pe:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg></rhyme></w> !</l>
						<l n="17" num="5.3" lm="10" met="5+5"><w n="17.1">C<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t</w> <w n="17.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="17.3">d</w>’<w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" caesura="1">en</seg>t</w><caesura></caesura> <w n="17.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="17.7">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.8" punct="vg:10">pl<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="18" num="5.4" lm="3"><space unit="char" quantity="14"></space><w n="18.1" punct="pe:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg></rhyme></w> !</l>
					</lg>
					<lg n="6" type="tercet" rhyme="aaa">
						<l n="19" num="6.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2">s</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="19.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="19.6" punct="pt:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></rhyme></w>.</l>
						<l n="20" num="6.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="20.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="20.2" punct="vg:3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="20.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="20.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="20.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="20.6" punct="pv:8">P<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pv">eau</seg></rhyme></w> ;</l>
						<l n="21" num="6.3" lm="5"><space unit="char" quantity="10"></space><w n="21.1" punct="vg:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="21.2" punct="vg:4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="21.3" punct="pe:5">l<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="pe">o</seg></rhyme></w> !</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="22" num="7.1" lm="10" met="5+5"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="22.2">n</w>’<w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="22.5">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5" caesura="1">oin</seg></w><caesura></caesura> <w n="22.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.7">l</w>’<w n="22.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="22.9">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="22.10" punct="vg:10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">em</seg>b<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="23" num="7.2" lm="3"><space unit="char" quantity="14"></space><w n="23.1" punct="pe:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg></rhyme></w> !</l>
						<l n="24" num="7.3" lm="10" met="5+5"><w n="24.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="24.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" caesura="1">en</seg>t</w><caesura></caesura> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="24.5">g<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="24.7" punct="vg:10">b<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="25" num="7.4" lm="3"><space unit="char" quantity="14"></space><w n="25.1" punct="pe:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg></rhyme></w> !</l>
					</lg>
					<lg n="8" type="tercet" rhyme="aaa">
						<l n="26" num="8.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="26.2">s</w>’<w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="26.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="26.6" punct="pt:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="12" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></rhyme></w>.</l>
						<l n="27" num="8.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="27.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="27.2" punct="vg:3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="27.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="27.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="27.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="27.6">l</w>’<w n="27.7" punct="pv:8"><rhyme label="a" id="12" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pv">eau</seg></rhyme></w> ;</l>
						<l n="28" num="8.3" lm="5"><space unit="char" quantity="10"></space><w n="28.1" punct="vg:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="28.2" punct="vg:4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="28.3" punct="pe:5">l<rhyme label="a" id="12" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="pe">o</seg></rhyme></w> !</l>
					</lg>
					<lg n="9" type="quatrain" rhyme="abab">
						<l n="29" num="9.1" lm="10" met="5+5"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="29.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="29.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="29.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="29.5">v<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>x</w><caesura></caesura> <w n="29.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="29.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="29.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9">en</seg></w> <w n="29.9" punct="vg:10">t<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="30" num="9.2" lm="3"><space unit="char" quantity="14"></space><w n="30.1" punct="pe:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg></rhyme></w> !</l>
						<l n="31" num="9.3" lm="10" met="5+5"><w n="31.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="31.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="31.3">d</w>’<w n="31.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>r</w><caesura></caesura> <w n="31.5">qu</w>’<w n="31.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="31.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="31.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>t</w> <w n="31.9" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>pr<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="32" num="9.4" lm="3"><space unit="char" quantity="14"></space><w n="32.1" punct="pe:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg></rhyme></w> !</l>
					</lg>
					<lg n="10" type="tercet" rhyme="aaa">
						<l n="33" num="10.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="33.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="33.2">s</w>’<w n="33.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="33.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="33.6" punct="pt:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="15" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></rhyme></w>.</l>
						<l n="34" num="10.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="34.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="34.2" punct="vg:3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="34.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="34.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="34.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="34.6">l</w>’<w n="34.7" punct="pv:8"><rhyme label="a" id="15" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pv">eau</seg></rhyme></w> ;</l>
						<l n="35" num="10.3" lm="5"><space unit="char" quantity="10"></space><w n="35.1" punct="vg:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="35.2" punct="vg:4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="35.3" punct="pe:5">l<rhyme label="a" id="15" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="pe">o</seg></rhyme></w> !</l>
					</lg>
					<closer>
						<placeName>Venise</placeName>.
					</closer>
				</div></body></text></TEI>