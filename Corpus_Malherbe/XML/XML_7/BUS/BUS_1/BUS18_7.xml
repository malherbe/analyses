<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS18" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="1(abba) 5(abab)">
					<head type="number">XVII</head>
					<head type="main">LA VEILLÉE DU GRAND SABBAT</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L</w>’ <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">An</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.5">C<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>g<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3" punct="dp:4">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="dp">er</seg></w> : <w n="2.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>s</w>, <w n="2.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.6" punct="pt:8">f<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.3">c<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="3.5" punct="pt:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.5" punct="pt:6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" punct="pt">en</seg>t</w>. <w n="4.6" punct="vg:8">D<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>j<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></w>,</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">D<seg phoneme="ɔ" type="vs" value="1" rule="116" place="1">o</seg>m</w> <w n="5.2" punct="vg:4">B<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>z<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="5.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.5" punct="pt:8">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>sp<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="6.5" punct="vg:8">pr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.5">p<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="8.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5" punct="pv:8">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg></rhyme></w> ;</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:2">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="370" place="2" punct="vg">e</seg>n</w>, <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="9.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>t</w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5" punct="vg:8">n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5" punct="vg:8">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>squ<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ff<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <hi rend="ital"><w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg></w></hi> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.6" punct="pt:8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1" punct="vg:1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="13.2">qu</w>’<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="13.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="13.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.7" punct="pt:8">r<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.3">l</w>’<w n="14.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="14.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="14.7" punct="pt:8">li<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></rhyme></w>.</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="15.3" punct="vg:8">D<seg phoneme="ø" type="vs" value="1" rule="405" place="5">eu</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="16.2">r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="16.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="16.5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ç<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.6" punct="pv:8">Di<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg></rhyme></w> ;</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="17.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="18.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="18.5">s</w>’<w n="18.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>t</rhyme></w>.</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="19.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="19.4">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="20.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="20.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="20.6" punct="vg:8">S<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bb<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>t</rhyme></w>,</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="21.4">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="21.5">s<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>rs</w> <w n="21.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ct<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="22.2"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="22.3">l</w>’<w n="22.4" punct="pt:8">h<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>z<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></w>.</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="23.3" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffl<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="23.4" punct="pt:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="24.2">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="24.6" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></w>.</l>
					</lg>
					<closer>
						<placeName>Cordoba</placeName>.
					</closer>
				</div></body></text></TEI>