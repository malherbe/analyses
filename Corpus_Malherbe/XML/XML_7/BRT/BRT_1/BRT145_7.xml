<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT145" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="3(abab)">
				<head type="number">XLV</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="1.4" punct="vg:4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="4" punct="vg">a</seg>il</w>, <w n="1.5">ou<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="Lc">i</seg></w>-<w n="1.6" punct="pe:6">d<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe" caesura="1">a</seg></w> !<caesura></caesura> <w n="1.7">n</w>’<w n="1.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="1.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="1.11" punct="vg:12">f<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="2.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>il</w> <w n="2.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.6" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="3.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="3.9" punct="vg:10">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="3.10">l</w>’<w n="3.11"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="3.12">cl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">qu</w>’<w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="4.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="4.8" punct="pt:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="355" place="10" mp="M">e</seg>x<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="5.3">p<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="5.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.8"><seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="5.9" punct="pv:12">l<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>g<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="6.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="6.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.9" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>l<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></w>.</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="7.2">qu</w>’<w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="7.4" punct="vg:3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>t</w>, <w n="7.5" punct="vg:6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="7.7">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.8">m<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2"><seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ppr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.8">j<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.9" punct="pt:12">v<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">in</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5" mp="M">ou</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="497" place="7" mp="C">y</seg></w> <w n="9.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="9.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.8" punct="pv:12">v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">n<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="10.4">s</w>’<w n="10.5"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="10.6">c<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>pt</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="10.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="10.9" punct="pv:12">t<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>r</rhyme></w> ;</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="11.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.3">s</w>’<w n="11.4"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="11.5" punct="pv:6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pv" caesura="1">e</seg>rd</w> ;<caesura></caesura> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="10">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="11.8"><seg phoneme="i" type="vs" value="1" rule="497" place="11" mp="C">y</seg></w> <w n="11.9">n<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.3" punct="ps:6">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="ps" caesura="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>…<caesura></caesura> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="12.7" punct="pe:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>r</rhyme></w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Tri-pot.</note>
					</closer>
			</div></body></text></TEI>