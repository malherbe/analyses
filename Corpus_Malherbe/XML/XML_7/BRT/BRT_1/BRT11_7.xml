<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT11" modus="cp" lm_max="12" metProfile="6+6, (6)" form="strophe unique" schema="1(ababcdcd)">
				<head type="number">XI</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="1.2" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t</w>, <w n="1.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="Lp">i</seg>s</w>-<w n="1.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="1.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="1.8">v<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="1.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="1.10">p<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="2.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="2.4">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="2.7" punct="ps:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>gn<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="ps">é</seg></rhyme></w>…</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>ts</w> <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5" punct="vg:6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6" punct="vg" caesura="1">en</seg>s</w>,<caesura></caesura> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.9"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>xpl<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="4.6" punct="pt:12">v<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="5.4" punct="dp:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="dp" caesura="1">u</seg>s</w> :<caesura></caesura> <w n="5.5">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.6">t</w>’<w n="5.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.8">s</w>’<w n="5.9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>xp<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>br<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="6.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="6.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="6.7">l</w>’<w n="6.8"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="345" place="9">ue</seg>il</w> <w n="6.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="6.10">l</w>’<w n="6.11" punct="ps:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tt<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="ps">en</seg>d</rhyme></w>…</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="7.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="7.5">gr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.6">d</w>’<w n="7.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="vg">e</seg>ct</w>, <w n="7.8">d</w>’<w n="7.9"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r</w> <w n="7.10"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="7.12" punct="ps:12">p<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></w>…</l>
					<l n="8" num="1.8" lm="6"><space unit="char" quantity="12"></space><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="3">e</seg>s</w>-<w n="8.4" punct="pi:4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pi ps ti">u</seg></w> ? … — <w n="8.5" punct="pt:6">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pt">an</seg></rhyme></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Dis-moi qui tu hantes, je te dirai qui tu es.</note>
					</closer>
			</div></body></text></TEI>