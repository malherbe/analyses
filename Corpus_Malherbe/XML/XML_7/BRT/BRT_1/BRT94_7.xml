<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT94" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="2[abab] 1[ababcbc] 1[ababa]">
				<head type="number">XCIV</head>
				<lg n="1" type="regexp" rhyme="ababababcbcababaabab">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="1.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="1.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rt</w><caesura></caesura> <w n="1.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.6">ch<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="1.8" punct="vg:8">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>s</w>, <w n="1.9">m<seg phoneme="a" type="vs" value="1" rule="145" place="9" mp="Lc">a</seg>m</w>’<w n="1.10" punct="vg:10">z<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="2.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">n</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="2.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="2.8" punct="ps:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="ps">en</seg>t</rhyme></w>…</l>
					<l n="3" num="1.3" lm="10" met="4+6">— <w n="3.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1" mp="M">Im</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="Lc">ou</seg>s</w>-<w n="3.3" punct="pe:7">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="pe" mp="F">e</seg></w> ! <w n="3.4">R<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M/mp">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="Lp">on</seg>d</w>-<w n="3.5" punct="pv:10"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="Lp">A</seg></w>-<w n="4.2">t</w>-<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="4.5"><seg phoneme="u" type="vs" value="1" rule="d-2" place="5" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="476" place="6">ï</seg></w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</w> <w n="4.7" punct="pe:10">m<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>n<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pe ps">an</seg>t</rhyme></w> ! …</l>
					<l n="5" num="1.5" lm="10" met="4+6">— <w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="5.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="5.7" punct="pv:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ct<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="6.3" punct="vg:4">f<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>gu<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.7" punct="dp:10">c<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>nn<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="dp">aî</seg>t</rhyme></w> :</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="7.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="7">en</seg>t</w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="7.6" punct="vg:10">cr<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="8.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="8.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="8.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="8.8" punct="pt:10">b<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nn<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="pt">e</seg>t</rhyme></w>.</l>
					<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.3" punct="pt:4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pt" caesura="1">a</seg></w>.<caesura></caesura> <w n="9.4">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="9.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="9.8">n<rhyme label="c" id="5" gender="f" type="a" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="10.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="10.3">g<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">oû</seg>t</w><caesura></caesura> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="10.6" punct="vg:10">c<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ch<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="vg">e</seg>ts</rhyme></w>,</l>
					<l n="11" num="1.11" lm="10" met="4+6"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="11.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ts</w> <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" caesura="1">in</seg>s</w><caesura></caesura> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="11.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="11.6">h<seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="M">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="11.7" punct="vg:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rn<rhyme label="c" id="5" gender="f" type="e" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="12" num="1.12" lm="10" met="4+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="12.2">d</w>’<w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="12.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rts</w><caesura></caesura> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="12.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="12.7" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ch<rhyme label="a" id="6" gender="m" type="a" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg>s</rhyme></w>.</l>
					<l n="13" num="1.13" lm="10" met="4+6"><w n="13.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pe ps">on</seg></w> ! … <w n="13.2">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="P">a</seg>r</w> <w n="13.4">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="13.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="13.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="b" id="7" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="14" num="1.14" lm="10" met="4+6"><w n="14.1">L</w>’<w n="14.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="1" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="14.3">m<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="14.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="14.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<rhyme label="a" id="6" gender="m" type="e" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></rhyme></w></l>
					<l n="15" num="1.15" lm="10" met="4+6"><w n="15.1">N</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="15.4" punct="ps:4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="ps" caesura="1">a</seg></w>…<caesura></caesura> <w n="15.5">V<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="15.6" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">ez</seg></w>, <w n="15.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="15.8" punct="pi:10">g<rhyme label="b" id="7" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi ps" mp="F">e</seg></rhyme></w> ? …</l>
					<l n="16" num="1.16" lm="10" met="4+6"><w n="16.1" punct="pi:1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1" punct="pi ps">oin</seg>t</w> ? … <w n="16.2" punct="vg:4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="16.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rs</w>, <w n="16.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="16.6" punct="dp:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>r<rhyme label="a" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="10" punct="dp">ai</seg></rhyme></w> :</l>
					<l n="17" num="1.17" lm="10" met="4+6"><w n="17.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="17.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="17.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="17.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="17.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="17.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="17.8" punct="vg:10">m<rhyme label="a" id="8" gender="f" type="a" stanza="4"><seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="18" num="1.18" lm="10" met="4+6"><w n="18.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="18.3">m</w>’<w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.6">m</w>’<w n="18.7" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="18.8" punct="pe:10">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rdi<rhyme label="b" id="9" gender="m" type="a" stanza="4"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg></rhyme></w> !</l>
					<l n="19" num="1.19" lm="10" met="4+6"><w n="19.1">C</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="19.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="19.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="19.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="19.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="19.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="19.8">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="19.9" punct="vg:10">j<rhyme label="a" id="8" gender="f" type="e" stanza="4"><seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="20" num="1.20" lm="10" met="4+6"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="20.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>ls</w><caesura></caesura> <w n="20.4">h<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w> <w n="20.5"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg></w> <w n="20.6" punct="pe:10">m<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>li<rhyme label="b" id="9" gender="m" type="e" stanza="4"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg></rhyme></w> ! »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Ce qui est différé n’est pas perdu.</note>
					</closer>
			</div></body></text></TEI>