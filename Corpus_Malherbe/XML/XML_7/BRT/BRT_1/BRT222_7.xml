<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT222" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="5[abab]">
				<head type="number">XLVII</head>
				<lg n="1" type="regexp" rhyme="abababababababababab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2" punct="vg:4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <subst reason="analysis" hand="ML" type="completion"><del>....</del><add rend="hidden"><w n="1.4" punct="vg:6">Ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg" caesura="1">e</seg>r</w></add><caesura></caesura></subst>, <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="1.6">m<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.7">v<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">Ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3">en</seg>t</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="2.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <subst reason="analysis" hand="ML" type="completion"><del>....</del><add rend="hidden"><w n="2.7" punct="pv:12">Ch<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pv">e</seg>r</rhyme></w></add></subst> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="3.5">n</w>’<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="3.8">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="3.9">tr<seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="3.10">n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="3.11" punct="dp:12">n<seg phoneme="e" type="vs" value="1" rule="353" place="10" mp="M">e</seg>tt<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2">pi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>ds</w> <w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="4.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="4.5" punct="pv:6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pv" caesura="1">eu</seg>ls</w> ;<caesura></caesura> <w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="4.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l</w> <w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="4.9">tr<seg phoneme="o" type="vs" value="1" rule="433" place="11">o</seg>p</w> <subst reason="analysis" hand="ML" type="completion"><del>....</del><add rend="hidden"><w n="4.10">ch<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12">e</seg>r</rhyme></w></add></subst></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="5.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="5.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg>t</w><caesura></caesura> <w n="5.6">qu</w>’<w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="5.9">pr<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">J</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="6.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <subst reason="analysis" hand="ML" type="completion"><del>.....</del><add rend="hidden"><w n="6.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w></add></subst> <w n="6.5">fr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.8">su<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>s</w> <w n="6.9" punct="dp:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="dp">é</seg></rhyme></w> :</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <subst reason="analysis" hand="ML" type="completion"><del>......</del><add rend="hidden"><w n="7.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w></add></subst> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="7.6">v<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.8">l</w>’<w n="7.9"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.10" punct="pe:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="8.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="8.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">c</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="8.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="9.5">Cr<seg phoneme="ø" type="vs" value="1" rule="403" place="6" caesura="1">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.8">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rd<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">A</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rds</w> <w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="10.9" punct="pv:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pv">an</seg>t</rhyme></w> ;</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="11.2">l</w>’<w n="11.3" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">In</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="11.4" punct="vg:6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="11.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="11.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.9" punct="pv:12">d<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <subst reason="analysis" hand="ML" type="completion"><del>....</del><add rend="hidden"><w n="12.2">Ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w></add></subst> <w n="12.3">m</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="12.6" punct="vg:6">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="12.7">j</w>’<w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="12.9">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s</w> <w n="12.10" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>ss<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>t</rhyme></w>.</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="Lc">ou</seg>rd</w>’<w n="13.4">hu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="13.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="13.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.7" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>dr<rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="14.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="14.3">qu</w>’<w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="14.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="14.8">n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>fs</w> <w n="14.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="14.10" punct="vg:12">j<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</rhyme></w>,</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="15.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>tr<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="15.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" mp="M">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="15.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="15.7" punct="vg:12">d<rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <subst reason="analysis" hand="ML" type="completion"><del>.....</del><add rend="hidden"><w n="16.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w></add></subst> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="16.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="16.5" punct="vg:6">p<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg>s</w>,<caesura></caesura> <w n="16.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="16.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="16.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="16.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="16.10" punct="pt:12">t<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>r</rhyme></w>.</l>
					<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="17.3">m</w>’<w n="17.4" punct="pe:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="pe" caesura="1">i</seg>t</w> !<caesura></caesura> <w n="17.5">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="17.6" punct="vg:9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" punct="vg">en</seg>ds</w>, <w n="17.7">p<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-27" place="11" mp="F">e</seg></w> <w n="17.8" punct="vg:12">h<rhyme label="a" id="9" gender="f" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="18" num="1.18" lm="12" met="6+6"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="18.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="18.3">qu</w>’<w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="18.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="18.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="18.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="18.8">t</w>’<w n="18.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="18.10" punct="pe:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>mn<rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></rhyme></w> !</l>
					<l n="19" num="1.19" lm="12" met="6+6"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="19.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="19.3">c<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="19.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="19.5">j</w>’<w n="19.6"><seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.7"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <subst reason="analysis" hand="ML" type="completion"><del>.....</del><add rend="hidden"><w n="19.8">ch<rhyme label="a" id="9" gender="f" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></add></subst></l>
					<l n="20" num="1.20" lm="12" met="6+6"><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></w> <w n="20.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="20.4" punct="pt:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt" caesura="1">ou</seg>rs</w>.<caesura></caesura> <w n="20.5">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="20.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rt</w> <w n="20.7"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="20.8" punct="pe:12">s<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<rhyme label="b" id="10" gender="m" type="e" stanza="5"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></rhyme></w> ! »</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Cher, Cher, cher, chère, chaire, Cher, chaire.</note>
					</closer>
			</div></body></text></TEI>