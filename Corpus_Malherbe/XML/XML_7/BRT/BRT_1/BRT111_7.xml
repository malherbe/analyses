<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT111" modus="cp" lm_max="12" metProfile="6, 6+6" form="suite de strophes" schema="2[abba]">
				<head type="number">XI</head>
				<lg n="1" type="regexp" rhyme="ab">
					<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="12"></space><w n="1.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="1.2" punct="vg:3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>r</w>, <w n="1.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="1.4" punct="pe:6">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pe">i</seg>r</rhyme></w> ! »</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="2.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="2.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="2.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="2.9" punct="pt:12">phr<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="regexp" rhyme="ba">
					<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ffl<seg phoneme="y" type="vs" value="1" rule="454" place="2">u</seg><seg phoneme="?" type="va" value="1" rule="162" place="3">en</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="3.4">Cr<seg phoneme="ø" type="vs" value="1" rule="403" place="6" caesura="1">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="3.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="354" place="9" mp="M">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>pt</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="3.9" punct="vg:12">v<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="2.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="4.3">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="4.5" punct="pt:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt">i</seg>r</rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="regexp" rhyme="abba">
					<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="5.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="5.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>t</w> <w n="5.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="5.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="5.6">n<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="5.7" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>v<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="6" num="3.2" lm="12" met="6+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.4"><seg phoneme="ø" type="vs" value="1" rule="247" place="6" caesura="1">œu</seg>fs</w><caesura></caesura> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="6.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">am</seg>ps</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="6.9">bl<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg></w> <w n="6.10" punct="pv:12">m<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="445" place="12" punct="pv">û</seg>r</rhyme></w> ;</l>
					<l n="7" num="3.3" lm="12" met="6+6"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2" punct="vg:3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>ts</w>, <w n="7.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>t</w> <w n="7.4" punct="vg:6">n<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="7.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="366" place="7">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg></w> <w n="7.8" punct="ps:12">s<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="y" type="vs" value="1" rule="445" place="12" punct="ps">û</seg>r</rhyme></w>…</l>
					<l n="8" num="3.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">n<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="307" place="5" mp="M">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="8.7"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.9" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Ou-tarde.</note>
					</closer>
			</div></body></text></TEI>