<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS-PORTRAITS</head><div type="poem" key="BRT292" modus="cm" lm_max="12" metProfile="6+6" form="sonnet peu classique" schema="abba baab ccd eed">
				<head type="number">XVII</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" caesura="1">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.7" punct="pv:12">g<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="2.2">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="2.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.5">tr<seg phoneme="o" type="vs" value="1" rule="415" place="6" caesura="1">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="2.8">r<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="2.9">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="2.10"><seg phoneme="a" type="vs" value="1" rule="343" place="11" mp="M">a</seg>ï<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">r<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="3.5" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pe" caesura="1">an</seg>ts</w> !<caesura></caesura> <w n="3.6">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="3.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="3.9" punct="pt:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="11" mp="M">ue</seg>ill<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg>s</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="P">a</seg>r</w> <w n="4.7">d</w>’<w n="4.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="4.9" punct="pt:12">h<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="baab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="5.2">l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="5.4" punct="dp:6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="dp" caesura="1">in</seg></w> :<caesura></caesura> <w n="5.5">l</w>’<w n="5.6"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="5.7" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="6.2" punct="vg:3">pr<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg></w>, <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.8" punct="pv:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.6">b<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>n<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="8.3">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="8.5">l</w>’<w n="8.6" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="8.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="8.9">nu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t</w> <w n="8.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="8.11" punct="pe:12">d<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</rhyme></w> !</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="9.2">d</w>’<w n="9.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">É</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ts</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">É</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>ts</w><caesura></caesura> <w n="9.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.7">pu<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>d<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="10.3">Cl<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="10.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="10.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="10.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="10.7" punct="vg:12">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>d<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">L</w>’<w n="11.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>t</w>, <w n="11.3">gl<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="11.4">d</w>’<w n="11.5">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.7">m<seg phoneme="wa" type="vs" value="1" rule="192" place="8">oe</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="11.10" punct="pe:12"><rhyme label="d" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="432" place="12" punct="pe">o</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="12.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="12.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="12.4">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="12.6">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>s</w> <w n="12.7">d</w>’<w n="12.8"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.9">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="13.3">l</w>’<w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="13.6" punct="vg:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="13.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="13.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.9" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rn<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="14.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="14.5" punct="pe:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="pe ps in" caesura="1">o</seg>rt</w> !<caesura></caesura> … « <w n="14.6">D<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="14.7" punct="pe:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rb<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pe">o</seg>s</rhyme></w> ! »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Brunehaut.</note>
					</closer>
			</div></body></text></TEI>