<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉNIGMES</head><head type="sub_1">BOTANIQUE<lb></lb>(<hi rend="smallcap">EMBLÈMES</hi>.)</head><div type="poem" key="BRT151" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2[abab] 1[aa] 1[abba]">
			<head type="number">I</head>
				<lg n="1" type="regexp" rhyme="ababaaabbaabab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">f<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="1.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="1.5">g<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="2.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>p</w> <w n="2.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.7">m</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.9" punct="vg:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="3.5">h<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.3" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>ppr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="5.3">j</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.7" punct="pv:8">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="6.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="6.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.5">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="7.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="7.7" punct="pt:8">m<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pt">an</seg></rhyme></w>.</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>s</w> <w n="8.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5" punct="vg:8">t<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<rhyme label="b" id="5" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="192" place="3">oe</seg>ll<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="9.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="9.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>d<rhyme label="b" id="5" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">j</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l</w> <w n="10.6" punct="pt:8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>d</rhyme></w>.</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">m</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.8" punct="pv:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<rhyme label="a" id="6" gender="f" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="2">ue</seg>il</w> <w n="12.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">m</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>t</w> <w n="12.7" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="7" gender="m" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
					<l n="13" num="1.13" lm="8" met="8"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="a" id="6" gender="f" type="e" stanza="4"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="14" num="1.14" lm="8" met="8"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="14.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</w> <w n="14.4" punct="dp:4">m<seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="dp">o</seg>t</w> : <hi rend="ital"><w n="14.5" punct="pt:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="7" gender="m" type="e" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w></hi>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Lin.</note>
					</closer>
			</div></body></text></TEI>