<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><head type="sub_part">A FLORENCE</head><div type="poem" key="BRI90" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="8((aa))">
					<head type="main">Palinodie</head>
					<head type="sub_1">(Après un voyage)</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">A</seg>Z</w>, <w n="1.2" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="3" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg>s</w>, <w n="1.3" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="1.7" punct="vg:12">Fr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">m<seg phoneme="i" type="vs" value="1" rule="493" place="2" mp="M">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="2.4">s</w>’<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.8" punct="pt:12">Fl<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="3.4" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="vg" caesura="1">e</seg>il</w>,<caesura></caesura> <w n="3.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="3.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d</w> <w n="3.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s</w> <w n="3.9">n<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="4.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="4.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.8" punct="vg:12">s<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</rhyme></w>,</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="5.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="5.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="5.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="5.9" punct="vg:12">d<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.3">f<seg phoneme="œ" type="vs" value="1" rule="304" place="5" mp="M">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="6.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.7" punct="vg:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>d<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="7.4">M<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="7.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="7.7">d</w>’<w n="7.8">hu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.9" punct="vg:12">br<seg phoneme="y" type="vs" value="1" rule="445" place="11" mp="M">û</seg>l<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>t</rhyme></w>,</l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">F<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="8.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="8.7" punct="pv:12">v<seg phoneme="e" type="vs" value="1" rule="383" place="11" mp="M">e</seg>ill<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pv">ai</seg>t</rhyme></w> ;</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="9.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.3">br<seg phoneme="y" type="vs" value="1" rule="454" place="5" mp="M">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="9.6">li<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="9.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="9.8" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>d<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="10.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.4">s</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="10.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="10.8" punct="dp:12">j<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="11.2" punct="vg:4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2" mp="M">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="4" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="11.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">E</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ts</w> <w n="11.5" punct="vg:12">s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="12.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="12.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>rb<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="12.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="12.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="12.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" mp="P">e</seg>rs</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="12.8" punct="pe:12">ci<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</rhyme></w> !</l>
						<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="13.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg" mp="F">e</seg>s</w>, <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="9" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="13.7" punct="vg:12">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="14.3">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="14.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="14.7" punct="vg:12">p<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>s<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="15.2" punct="vg:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="15.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="15.4"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="9" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="15.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="15.8" punct="dp:12">N<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="dp">o</seg>rd</rhyme></w> :</l>
						<l n="16" num="1.16" lm="12" met="6+6">« <w n="16.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="16.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="16.4" punct="vg:6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">oin</seg></w>,<caesura></caesura> <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="16.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="16.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="16.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="16.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="16.10" punct="pe:12">m<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pe">o</seg>rt</rhyme></w> ! »</l>
					</lg>
				</div></body></text></TEI>