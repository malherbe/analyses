<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><head type="sub_part">A FLORENCE</head><div type="poem" key="BRI84" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="7(abaab)">
					<head type="main">La Fleur qui m’est douce</head>
					<lg n="1" type="quintil" rhyme="abaab">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>CC<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">O</seg>RD</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="1.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="1.7">l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">M<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.4" punct="dp:7">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>mm<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7" punct="dp">e</seg>il</rhyme></w> :</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.5" punct="vg:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3" punct="vg:5">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="4.4" punct="vg:7">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rph<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="5.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="5.4" punct="pt:7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rm<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7" punct="pt">e</seg>il</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quintil" rhyme="abaab">
						<l n="6" num="2.1" lm="7" met="7"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="6.3">s</w>’<w n="6.4" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ch<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="7" num="2.2" lm="7" met="7"><w n="7.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.5" punct="pv:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>g<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pv">a</seg>rd</rhyme></w> ;</l>
						<l n="8" num="2.3" lm="7" met="7"><w n="8.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="8.2">l<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w>-<w n="8.4" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" punct="vg">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="8.6">ch<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="9" num="2.4" lm="7" met="7"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="10" num="2.5" lm="7" met="7"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="3">ym</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5">l</w>’<w n="10.6" punct="pt:7"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>rt</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quintil" rhyme="abaab">
						<l n="11" num="3.1" lm="7" met="7"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.5">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rs<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="12" num="3.2" lm="7" met="7"><w n="12.1" punct="vg:2">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>t</w>, <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.6" punct="vg:7">m<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7" punct="vg">ain</seg></rhyme></w>,</l>
						<l n="13" num="3.3" lm="7" met="7"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pt</w> <w n="13.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.5" punct="vg:7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>s<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="3.4" lm="7" met="7"><w n="14.1" punct="vg:2">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="vg">è</seg>s</w>, <w n="14.2">di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="14.4" punct="pt:7">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>c<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
						<l n="15" num="3.5" lm="7" met="7"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="15.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="15.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="15.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.5" punct="pt:7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="pt">in</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="quintil" rhyme="abaab">
						<l n="16" num="4.1" lm="7" met="7"><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3">di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">É</seg>tr<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="17" num="4.2" lm="7" met="7"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="17.3">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ts</w> <w n="17.4">s<seg phoneme="wa" type="vs" value="1" rule="440" place="6">o</seg>y<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</rhyme></w></l>
						<l n="18" num="4.3" lm="7" met="7"><w n="18.1" punct="pv:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="pv">e</seg>nt</w> ; <w n="18.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.4">th<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="19" num="4.4" lm="7" met="7"><w n="19.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="19.3">s<seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>tr<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="469" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="20" num="4.5" lm="7" met="7"><w n="20.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="20.3" punct="pt:7">m<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg>x</rhyme></w>.</l>
					</lg>
					<lg n="5" type="quintil" rhyme="abaab">
						<l n="21" num="5.1" lm="7" met="7"><w n="21.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="21.2" punct="vg:4">C<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>bu<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="21.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="21.5" punct="vg:7">c<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="22" num="5.2" lm="7" met="7"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="22.5" punct="dp:7">c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="dp">é</seg></rhyme></w> :</l>
						<l n="23" num="5.3" lm="7" met="7"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="23.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.5" punct="vg:7">p<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="24" num="5.4" lm="7" met="7"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="24.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.3">d</w>’<w n="24.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="24.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="24.6">c<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="25" num="5.5" lm="7" met="7"><w n="25.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="25.2">pr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.3" punct="pt:7">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>t<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
					<lg n="6" type="quintil" rhyme="abaab">
						<l n="26" num="6.1" lm="7" met="7"><w n="26.1" punct="vg:1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1" punct="vg">eu</seg>r</w>, <w n="26.2">d</w>’<w n="26.3"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="26.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="26.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="27" num="6.2" lm="7" met="7"><w n="27.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="27.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg></w> <w n="27.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></w>,</l>
						<l n="28" num="6.3" lm="7" met="7"><w n="28.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="28.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="28.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rc<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="29" num="6.4" lm="7" met="7"><w n="29.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="29.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="29.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="29.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="29.6" punct="vg:7">T<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>sc<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="30" num="6.5" lm="7" met="7"><w n="30.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="30.3">s</w>’<w n="30.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="30.5" punct="dp:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="5">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>vr<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="dp">é</seg></rhyme></w> :</l>
					</lg>
					<lg n="7" type="quintil" rhyme="abaab">
						<l n="31" num="7.1" lm="7" met="7"><w n="31.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="31.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="31.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="31.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="31.5">m</w>’<w n="31.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="31.7">d<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="32" num="7.2" lm="7" met="7"><w n="32.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oî</seg>t</w> <w n="32.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="32.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="32.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ps</w> <w n="32.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="32.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="32.7" punct="pv:7">m<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pv">e</seg>r</rhyme></w> ;</l>
						<l n="33" num="7.3" lm="7" met="7"><w n="33.1">S<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="33.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="33.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="33.4" punct="vg:7">m<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="34" num="7.4" lm="7" met="7"><w n="34.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="34.2">l</w>’<w n="34.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt</w> <w n="34.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="34.5">l</w>’<w n="34.6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="34.8" punct="vg:7">p<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="35" num="7.5" lm="7" met="7"><w n="35.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="35.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd</w> <w n="35.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="35.5">g<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.6" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pt">e</seg>r</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>