<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><head type="sub_part">A ROME</head><div type="poem" key="BRI97" modus="cp" lm_max="10" metProfile="5, 5+5" form="suite périodique" schema="2(abaab)">
					<head type="main">Les Fleurs sombres</head>
					<opener>
						<salute>À Xavier Marmier</salute>
					</opener>
					<lg n="1" type="quintil" rhyme="abaab">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>CR<seg phoneme="a" type="vs" value="1" rule="340" place="2">A</seg>S<seg phoneme="ə" type="ee" value="0" rule="e-23">E</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="1.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="1.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5" caesura="1">e</seg>ds</w><caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="1.6" punct="vg:10">m<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="2.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="2.4">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" caesura="1">o</seg>rd</w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="2.6">d</w>’<w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="2.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="2.9" punct="vg:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ffr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>t</rhyme></w>,</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d</w> <w n="3.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="3.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.7">plu<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>xh<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="4.4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="M">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>s</w><caesura></caesura> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">O</seg>ph<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="5" num="1.5" lm="5" met="5"><space unit="char" quantity="10"></space><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g</w> <w n="5.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="5.4" punct="pt:5">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" punct="pt">en</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quintil" rhyme="abaab">
						<l n="6" num="2.1" lm="10" met="5+5"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg>x</w> <w n="6.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rds</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" caesura="1">in</seg>s</w><caesura></caesura> <w n="6.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="6.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="6.7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="6.8" punct="vg:10">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>sc<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="7" num="2.2" lm="10" met="5+5"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2">tr<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="497" place="4" mp="C">y</seg></w> <w n="7.4" punct="vg:5">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg" caesura="1">oî</seg>t</w>,<caesura></caesura> <w n="7.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="7.6">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg></w> <w n="7.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="7.9" punct="vg:10">f<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="vg">o</seg>rt</rhyme></w>,</l>
						<l n="8" num="2.3" lm="10" met="5+5"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="8.3">l</w>’<w n="8.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>x</w><caesura></caesura> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="8.8">f<seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.9" punct="vg:10">t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="9" num="2.4" lm="10" met="5+5"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>t</w><caesura></caesura> <w n="9.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="9.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="9.7">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t</w> <w n="9.8">s</w>’<w n="9.9"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="2.5" lm="5" met="5"><space unit="char" quantity="10"></space><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="10.2">dr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ps</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.5" punct="pt:5">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="pt">o</seg>rt</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>