<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Enluminures</title>
				<title type="medium">Édition électronique</title>
				<author key="ELS">
					<name>
						<forename>Max</forename>
						<surname>ELSKAMP</surname>
					</name>
					<date from="1862" to="1931">1862-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>718 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">ELS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Enluminures</title>
						<author>Max Elskamp</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/maxelskampenluminures.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Enluminures / paysages, heures, vies, chansons, grotesques</title>
						<author>Max Elskamp</author>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k10575703.r=%22max%20elskamp%22?rk=42918;4</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>Paul Lacomblez, Éditeur</publisher>
							<date when="1898">1898</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1898">1898</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">VIES</head><div type="poem" key="ELS18" modus="cm" lm_max="10" metProfile="5÷5" form="suite de strophes" schema="3(abba) 2(abab)">
					<head type="number">V</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>r</w> <w n="1.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.4" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="1.6">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rs</w> <w n="1.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="1.9" punct="vg:10">v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">où</seg></w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="2.4" punct="vg:5">j<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" punct="vg" caesura="1">in</seg>s</w>,<caesura></caesura> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="2.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="7">en</seg>s</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="2.9" punct="vg:10">v<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">e</seg>t</w>, <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="3.4" punct="vg:5">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="3.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</w> <w n="3.7" punct="vg:10"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>ss<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="4.5" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" punct="vg" caesura="1">e</seg>r</w>,<caesura></caesura> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.7">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rs</w> <w n="4.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="4.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="4.10" punct="vg:10">v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="10" met="5−5" mp5="C"><w n="5.1">v<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="5.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C" caesura="1">e</seg>s</w><caesura></caesura> <w n="5.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="5.7">f<rhyme label="a" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="10" met="5−5" mp5="C"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">au</seg>x</w> <w n="6.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C" caesura="1">e</seg>s</w><caesura></caesura> <w n="6.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>rs</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="6.7" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<rhyme label="b" id="3" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg>s</rhyme></w>,</l>
						<l n="7" num="2.3" lm="10" met="5−5" mp5="P"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="7.2">s</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P" caesura="1">an</seg>s</w><caesura></caesura> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="7.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="7.9" punct="vg:10">l<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="8.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="8.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="8.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg></w> <w n="8.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.10" punct="pt:10"><rhyme label="a" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="9.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>rs</w> <w n="9.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>gts</w><caesura></caesura> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg>x</w> <w n="9.7" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>g<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɥi" type="vs" value="1" rule="274" place="10">ui</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="10.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="10.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5" caesura="1">en</seg>t</w><caesura></caesura> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="10.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="10.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>r</w> <w n="10.8" punct="vg:10">cr<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>x</rhyme></w>,</l>
						<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>r</w> <w n="11.3" punct="vg:5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="11.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="11.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="11.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>rs</w> <w n="11.7">v<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>x</rhyme></w></l>
						<l n="12" num="3.4" lm="10" met="5+5"><w n="12.1">qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="12.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>r</w> <w n="12.4">p<seg phoneme="o" type="vs" value="1" rule="318" place="5" caesura="1">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7" punct="pv:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>g<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="10" met="5+5"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="13.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="13.8" punct="vg:10">v<rhyme label="a" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="14" num="4.2" lm="10" mp5="F" met="10"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="14.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="14.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="14.7" punct="vg:10">v<rhyme label="b" id="7" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg></rhyme></w>,</l>
						<l n="15" num="4.3" lm="10" met="5+5"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">au</seg>x</w> <w n="15.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="15.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="15.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>s</w><caesura></caesura> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg>x</w> <w n="15.6">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="15.7" punct="vg:10">h<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>b<rhyme label="a" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="16" num="4.4" lm="10" met="5+5"><w n="16.1">d<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="16.3">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>x</w><caesura></caesura> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="16.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="16.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>f<rhyme label="b" id="7" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg></rhyme></w>,</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="10" met="5+5"><w n="17.1">c<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="17.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="17.4" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="17.5">c</w>’<w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="17.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="17.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.9" punct="vg:10">f<rhyme label="a" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="18" num="5.2" lm="10" met="5+5"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="18.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>rs</w> <w n="18.3">p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="18.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" caesura="1">o</seg>rps</w><caesura></caesura> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w> <w n="18.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>s</w> <w n="18.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="18.8" punct="vg:10">t<rhyme label="b" id="9" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>ts</rhyme></w>,</l>
						<l n="19" num="5.3" lm="10" met="5+5"><w n="19.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1" mp="M">im</seg>pl<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="19.2" punct="vg:5">s<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" punct="vg" caesura="1">e</seg>il</w>,<caesura></caesura> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="19.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="19.5" punct="vg:10">n<rhyme label="a" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="20" num="5.4" lm="10" met="5+5"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>ts</w><caesura></caesura> <w n="20.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w> <w n="20.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="20.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>rs</w> <w n="20.7" punct="pt:10">br<rhyme label="b" id="9" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>