<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CONTES</head><div type="poem" key="CHF16" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="2[abab] 1[abba] 1[aa] 1[abbaa]">
					<head type="main">PROLOGUE D’UN AUTRE CONTE</head>
					<lg n="1" type="regexp" rhyme="abababababbaaaabbaa">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="1.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="1.6" punct="pv:10">r<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>c<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="pv">ain</seg></rhyme></w> ;</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="2.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" caesura="1">e</seg>rs</w><caesura></caesura> <w n="2.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="2.7" punct="pt:10">m<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rch<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="3.2" punct="vg:4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>cl<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg" caesura="1">ez</seg></w>,<caesura></caesura> <w n="3.3" punct="vg:6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg" mp="F">e</seg>s</w>, <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.6" punct="vg:10">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>st<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="4.2">L<seg phoneme="u" type="vs" value="1" rule="d-2" place="2" mp="M/mc">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="3" mp="Lc">i</seg>s</w>-<w n="4.3">Qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" caesura="1">in</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="4.6">pl<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="4.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="4.8" punct="pt:10">v<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="5.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="Lc">on</seg>g</w>-<w n="5.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" caesura="1">em</seg>ps</w><caesura></caesura> <w n="5.4">j</w>’<w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="5.7">g<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="5.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="5.9" punct="pt:10">b<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</rhyme></w>.</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="dp" caesura="1">ai</seg>t</w> :<caesura></caesura> <w n="6.4">L<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="6.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="6.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.8" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>tr<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="469" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="7.3">l</w>’<w n="7.4" punct="pv:4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pv" caesura="1">er</seg></w> ;<caesura></caesura> <w n="7.5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.7">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="7.8" punct="pt:10">p<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</rhyme></w>.</l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">Tr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="8.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="8.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>l<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="9.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.5" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="8" mp="M">en</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>bl<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="469" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="10.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" mp="P">e</seg>rs</w> <w n="10.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="10.6">cl<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ts</w> <w n="10.7" punct="vg:10">h<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="M">eu</seg>r<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="11" num="1.11" lm="10" met="4+6"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" mp="M">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="11.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="11.5" punct="vg:7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7" punct="vg">œu</seg>r</w>, <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="11.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="11.8" punct="vg:10">v<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="248" place="10" punct="vg">œu</seg>x</rhyme></w>,</l>
						<l n="12" num="1.12" lm="10" met="4+6"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="12.2" punct="vg:4">l<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="12.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" mp="M">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.5" punct="pt:10">ch<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="13" num="1.13" lm="10" met="4+6"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">m</w>’<w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="13.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">An</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>s</w>, <w n="13.6">f<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="13.8" punct="pt:10">mi<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg>x</rhyme></w>.</l>
						<l n="14" num="1.14" lm="10" met="4+6"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="14.2" punct="vg:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2" punct="vg">en</seg></w>, <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="P">a</seg>r</w> <w n="14.4" punct="vg:4">f<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="14.5" punct="vg:6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>r</w>, <w n="14.6" punct="vg:10">s<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="15" num="1.15" lm="10" met="4+6"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="d-3" place="3" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="15.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>ts</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="15.6">l</w>’<w n="15.7" punct="vg:10">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>lv<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<rhyme label="a" id="8" gender="f" type="a" stanza="5"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="16" num="1.16" lm="10" met="4+6"><w n="16.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="16.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="16.3" punct="vg:4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="16.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="16.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">A</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>z<rhyme label="b" id="9" gender="m" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10" punct="vg">e</seg>l</rhyme></w>,</l>
						<l n="17" num="1.17" lm="10" met="4+6"><w n="17.1">L</w>’<w n="17.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="17.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="17.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="17.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="17.6" punct="vg:10">ci<rhyme label="b" id="9" gender="m" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10" punct="vg">e</seg>l</rhyme></w>,</l>
						<l n="18" num="1.18" lm="10" met="4+6"><w n="18.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="18.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>r</w> <w n="18.3">l</w>’<w n="18.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4" caesura="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="P">a</seg>r</w> <w n="18.7">l</w>’<w n="18.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-27" place="8" mp="F">e</seg></w> <w n="18.9" punct="dp:10">h<seg phoneme="a" type="vs" value="1" rule="343" place="9" mp="M">a</seg><rhyme label="a" id="8" gender="f" type="e" stanza="5"><seg phoneme="i" type="vs" value="1" rule="476" place="10">ï</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="19" num="1.19" lm="10" met="4+6"><w n="19.1">P<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="19.2">d</w>’<w n="19.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="4" caesura="1">ue</seg>il</w><caesura></caesura> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="19.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="19.7" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>xp<rhyme label="a" id="8" gender="f" type="a" stanza="5"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>

					</lg>
				</div></body></text></TEI>