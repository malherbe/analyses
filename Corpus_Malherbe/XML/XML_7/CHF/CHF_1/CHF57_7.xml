<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF57" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="1[abbacca] 5[aa]">
					<head type="main">A CELLE QUI N’EST PLUS</head>
					<lg n="1" type="regexp" rhyme="abbaccaaaaaaaaaaa">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">m<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4">en</seg>t</w> <w n="1.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="3">en</seg>s</w> <w n="2.4" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>gu<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="2.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rg<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.7" punct="vg:12">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>p<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="3.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="3.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="3.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.7" punct="vg:12">t<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ss<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu</w>’<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="4.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t</w> <w n="4.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">L</w>’<w n="5.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="5.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ff<rhyme label="c" id="3" gender="m" type="a" stanza="1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rt</rhyme></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>x</w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>sp<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="6.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="6.9">m<rhyme label="c" id="3" gender="m" type="e" stanza="1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt</rhyme></w></l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="7.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rps</w> <w n="7.3">qu</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="7.8" punct="pv:12">p<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ss<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="8.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3">m<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4">en</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="8.6">l</w>’<w n="8.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="8.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="8.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ppu<rhyme label="a" id="4" gender="m" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg></rhyme></w>,</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="9.4">fu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="9.5">l</w>’<w n="9.6" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="9.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="9.8">l</w>’<w n="9.9"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="9.10">fu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t</w> <w n="9.11">l</w>’<w n="9.12" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="a" id="4" gender="m" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></rhyme></w>,</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="10.2" punct="vg:2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>l</w>, <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="10.4" punct="vg:6">fr<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="10.5">j</w>’<w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="10.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="10.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="10.9">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="11.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="11.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="11.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="353" place="8" mp="M">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="9" mp="M">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.7" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="12.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="12.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>ts</w><caesura></caesura> <w n="12.5">j</w>’<w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="12.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="12.8">l</w>’<w n="12.9" punct="pv:12">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rr<rhyme label="a" id="6" gender="m" type="a" stanza="4"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>r</rhyme></w> ;</l>
						<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>t</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="13.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="13.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="13.8" punct="pv:12">c<rhyme label="a" id="6" gender="m" type="e" stanza="4"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pv">œu</seg>r</rhyme></w> ;</l>
						<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="14.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="14.3" punct="vg:4">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg" mp="F">e</seg>s</w>, <w n="14.4" punct="vg:6">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg" caesura="1">e</seg>ts</w>,<caesura></caesura> <w n="14.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="14.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt</w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="14.9" punct="vg:12">p<rhyme label="a" id="7" gender="f" type="a" stanza="5"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1">D</w>’<w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="15.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4">en</seg>t</w> <w n="15.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="15.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>x</w><caesura></caesura> <w n="15.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="15.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="15.8">l</w>’<w n="15.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>pr<rhyme label="a" id="7" gender="f" type="e" stanza="5"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="16.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="16.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">j</w>’<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="16.6">v<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="16.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="16.8">l</w>’<w n="16.9"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="16.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<rhyme label="a" id="8" gender="m" type="a" stanza="6"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</rhyme></w>,</l>
						<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="17.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">j</w>’<w n="17.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="17.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="17.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></w> <w n="17.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="17.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="17.10" punct="pe:12">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<rhyme label="a" id="8" gender="m" type="e" stanza="6"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg>s</rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>