<div align="center">
<center><h3>Fichiers HTML des textes analysés</h3></center>
</div>

Un fichier pour chaque poème ou pièce de théâtre et pour chacune des étapes du traitement.
L'étape du traitement est identifiée par la terminaison du nom du fichier : ex. **MAL2_6PCMF.html** = visualisation des
propriétés PCMF+L du poème MAL2, **MAL2_7.html** = visualisation des rimes, des schémas de rimes et de la forme globale du poème MAL2. 

▪ Les étapes du traitement automatique 

BAU3 = Charles Baudleaire albatros

- **.html** : texte brut avant analyse
- **_6.html** : noyaux syllabiques, strophes, profil métrique et mètre des vers
- **_6b.html** : diérèses, "e" élidés, strophes, profil(s) métrique(s) et mètre des vers
- **_6PCMF.html** : propriétés PCMF+L, strophes, profil(s) métrique(s) et mètre des vers
- **_6P.html** : propriétés P, strophes, profil(s) métrique(s) et mètre des vers
- **_6C.html** : propriétés C, strophes, profil(s) métrique(s) et mètre des vers
- **_6M.html** : propriétés M, strophes, profil(s) métrique(s) et mètre des vers
- **_6F.html** : propriétés F, strophes, profil(s) métrique(s) et mètre des vers
- **_6L.html** : propriétés L, strophes, profil(s) métrique(s) et mètre des vers
- **_7.html** : rimes, strophes et forme globale
- **_8.html** : extension des rimes et PGTC (données incomplètes)
- **_9.html** : qualité des rimes (données incomplètes)
