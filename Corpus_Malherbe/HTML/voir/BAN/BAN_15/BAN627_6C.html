<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:tei="http://www.tei-c.org/ns/1.0">
  <head>
    <title>MÉTRIQUE EN LIGNE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="traitement automatique de textes versifiés" />
    <meta name="keywords" content="métrique pésie versification" />
    <meta name="author" content="CRISCO" />
    <link type="text/css" rel="stylesheet" href="../../../outils/css/style.css" />
    <link type="text/css" rel="stylesheet" href="../../../outils/css/analyse.css" />
    <script type="text/javascript" src="../../../outils/js/infos.js"></script>
  </head>
  <body>
    <div id="curseur" class="infobulle"></div>
<div id="textes">
<div class="div_code_poeme">BAN_15/BAN627</div>
<div class="div_auteur">Théodore de BANVILLE</div>
<div class="div_recueil_titre_main">ROSES DE NOËL</div>
<div class="div_date">1878</div>
<table class="table_body">
				<tr><td class="td_titre_main">Vers le ciel</td></tr>
				<tr><td><table class="table_lg">
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_1">Élevons n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 438&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 4&#60;br/&#62;propriété = C')" onmouseout="cache()">o</a>s regards<span class="caesura"> |</span> vers l<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> ciel adouci.</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_2">Mère, c’est dans <a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 452&#60;br/&#62;phonème = œ̃&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = C')" onmouseout="cache()">un</a> jour,<span class="caesura"> |</span> pareil à celui-ci</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_3">Que t<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> mère, éperdue,<span class="caesura"> |</span> en s<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 161&#60;br/&#62;phonème = ɛ&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a>s ferveurs étranges,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_4">T<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> voyait, en dormant,<span class="caesura"> |</span> sourire pour l<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 161&#60;br/&#62;phonème = ɛ&#60;br/&#62;valeur = 1&#60;br/&#62;place = 11&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a>s Anges !</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num">5</span></td>
<td class="td_vers" id="vers_5">Ah ! par c<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 161&#60;br/&#62;phonème = ɛ&#60;br/&#62;valeur = 1&#60;br/&#62;place = 3&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a>s premiers jours<span class="caesura"> |</span> de printemps clairs et doux,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_6">L<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> souffle de n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 438&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = C')" onmouseout="cache()">o</a>s morts<span class="caesura"> |</span> chéris est avec nous.</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_7"><a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 468&#60;br/&#62;phonème = i&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">I</a>l caresse n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 438&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = C')" onmouseout="cache()">o</a>s fronts<span class="caesura"> |</span> et n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 425&#60;br/&#62;phonème = u&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">ou</a>s dit à l’oreille :</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_8">Voici que tout renaît<span class="caesura"> |</span> et que tout s<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> réveille ;</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_9">Qu’après l’hiver jaloux<span class="caesura"> |</span> qui dépouillait l<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 407&#60;br/&#62;phonème = œ&#60;br/&#62;valeur = 1&#60;br/&#62;place = 11&#60;br/&#62;propriété = C')" onmouseout="cache()">eu</a>r front</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num">10</span></td>
<td class="td_vers" id="vers_10">L<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 161&#60;br/&#62;phonème = ɛ&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a>s bois luxuriants<span class="caesura"> |</span> bientôt reverdiront,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_11">Et que renouvelant<span class="caesura"> |</span> s<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 7&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> riche broderie</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_12">L<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> terre <a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 318&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 3&#60;br/&#62;propriété = C')" onmouseout="cache()">au</a> flanc vermeil<span class="caesura"> |</span> sera toute fleurie !</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_13">Mère, <a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 468&#60;br/&#62;phonème = i&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">i</a>ls parlent ainsi,<span class="caesura"> |</span> car <a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 468&#60;br/&#62;phonème = i&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">i</a>ls suivent n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 438&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 11&#60;br/&#62;propriété = C')" onmouseout="cache()">o</a>s pas.</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_14"><a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 468&#60;br/&#62;phonème = i&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">I</a>ls n<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 425&#60;br/&#62;phonème = u&#60;br/&#62;valeur = 1&#60;br/&#62;place = 3&#60;br/&#62;propriété = C')" onmouseout="cache()">ou</a>s laissent pas,<span class="caesura"> |</span> <a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 468&#60;br/&#62;phonème = i&#60;br/&#62;valeur = 1&#60;br/&#62;place = 7&#60;br/&#62;propriété = C')" onmouseout="cache()">i</a>ls n<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 425&#60;br/&#62;phonème = u&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = C')" onmouseout="cache()">ou</a>s quittent pas,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num">15</span></td>
<td class="td_vers" id="vers_15">Mais attentifs, voyant<span class="caesura"> |</span> n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 438&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 7&#60;br/&#62;propriété = C')" onmouseout="cache()">o</a>s peines amassées,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_16">A suivre dans n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 438&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = C')" onmouseout="cache()">o</a>s yeux<span class="caesura"> |</span> l’ombre de n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 438&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = C')" onmouseout="cache()">o</a>s pensées,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_17"><a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 468&#60;br/&#62;phonème = i&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">I</a>ls n<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> sont malheureux<span class="caesura"> |</span> que de notre douleur,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_18">Puisqu’<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 468&#60;br/&#62;phonème = i&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">i</a>ls ont déjà pu<span class="caesura"> |</span> sentir l<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 407&#60;br/&#62;phonème = œ&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = C')" onmouseout="cache()">eu</a>r vie en fleur</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_19">Naître et s’éveiller, comme<span class="caesura"> |</span> <a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 452&#60;br/&#62;phonème = œ̃&#60;br/&#62;valeur = 1&#60;br/&#62;place = 7&#60;br/&#62;propriété = C')" onmouseout="cache()">un</a> renouveau splendide.</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num">20</span></td>
<td class="vers_indent_2" id="vers_20">L<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> vérité n’est pas<span class="caesura"> |</span> notre front qui s<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 11&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> ride :</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_21">C’est l<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> bonté de Dieu,<span class="caesura"> |</span> qui n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 425&#60;br/&#62;phonème = u&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">ou</a>s laisse entrevoir</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_22"><a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 318&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">Au</a> lointain l<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 4&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> lueur<span class="caesura"> |</span> sereine de l’espoir,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_23">Et qui n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 425&#60;br/&#62;phonème = u&#60;br/&#62;valeur = 1&#60;br/&#62;place = 3&#60;br/&#62;propriété = C')" onmouseout="cache()">ou</a>s versera<span class="caesura"> |</span> l<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 7&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> bonheur sans mesure</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_24">Dans l<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 161&#60;br/&#62;phonème = ɛ&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a>s cieux frémissants<span class="caesura"> |</span> que s<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> prunelle azure.</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num">25</span></td>
<td class="td_vers" id="vers_25"><a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 468&#60;br/&#62;phonème = i&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">I</a>ls n<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 425&#60;br/&#62;phonème = u&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">ou</a>s rendra m<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 418&#60;br/&#62;phonème = ɔ̃&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = C')" onmouseout="cache()">on</a> père<span class="caesura"> |</span> et s<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> grave douceur</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_26">Et l<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> rire ingénu<span class="caesura"> |</span> de m<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> petite sœur ;</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_27">Car l<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> Seigneur n’emplit<span class="caesura"> |</span> d’ombre l<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> forêt verte</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_28">Et n<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> sème d<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 161&#60;br/&#62;phonème = ɛ&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a>s fleurs<span class="caesura"> |</span> sur l<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 340&#60;br/&#62;phonème = a&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">a</a> plaine déserte</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_29">Et n<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> fait rayonner<span class="caesura"> |</span> sur nous l<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> soleil d’or</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num">30</span></td>
<td class="td_vers" id="vers_30">Que pour nous dire : Enfants,<span class="caesura"> |</span> patientez encor ;</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_31">V<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 438&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 1&#60;br/&#62;propriété = C')" onmouseout="cache()">o</a>s ennuis sont amers<span class="caesura"> |</span> et v<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 438&#60;br/&#62;phonème = o&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = C')" onmouseout="cache()">o</a>s jours difficiles,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_32">Mais j<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> v<a class="pm_C" onmouseover="montre('type = vs&#60;br/&#62;règle = 425&#60;br/&#62;phonème = u&#60;br/&#62;valeur = 1&#60;br/&#62;place = 3&#60;br/&#62;propriété = C')" onmouseout="cache()">ou</a>s vois, j<a class="pm_C" onmouseover="montre('type = em&#60;br/&#62;règle = e-12&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = C')" onmouseout="cache()">e</a> songe<span class="caesura"> |</span> à vous. Soyez tranquilles.</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
				</table></td></tr>
				<tr><td class="td_closer">
					<div class="div_dateline">
						 16 février 1872.
					</div>
				</td></tr>
			<tr><td><table class="table_infos_analyse"><tr>
<td class="td_infos_met"><span class="span_infos_analyse_titre">mètre</span></td>
<td class="td_profil">
<span class="span_infos_analyse_titre_1">profil métrique : </span><span class="span_infos_analyse_contenu">6+6</span>
</td>
</tr></table></td></tr>
</table>
</div>
  </body>
</html>
