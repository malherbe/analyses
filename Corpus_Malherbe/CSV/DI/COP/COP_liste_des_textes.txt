
	▪ François COPPÉE [COP]

 		Liste des recueils de poésies
 		─────────────────────────────
		▫ PROMENADES ET INTÉRIEURS, 1872 [COP_1]
		▫ POÈMES MODERNES, 1867-1869 [COP_2]
		▫ LE CAHIER ROUGE, 1874 [COP_3]
		▫ LES HUMBLES, 1872 [COP_4]
		▫ Sonnets intimes et Poèmes inédits, 1925 [COP_5]
		▫ DES VERS FRANÇAIS, 1906 [COP_6]
		▫ LE RELIQUAIRE, 1866 [COP_7]
		▫ POÈMES DIVERS, 1869 [COP_8]
