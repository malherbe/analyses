
	▪ Alexandre DUCROS [DUC]

 		Liste des recueils de poésies
 		─────────────────────────────
		▫ Les Capricieuses, 1854 [DUC_1]
		▫ Les Caresses d’Antan, 1896 [DUC_2]
		▫ Les Étrivières, 1867-1885 [DUC_3]
