
	▪ Marceline DESBORDES-VALMORE [DES]

 		Liste des recueils de poésies
 		─────────────────────────────
		▫ POÉSIES, 1830 [DES_1]
		▫ LES PLEURS, 1830 [DES_2]
		▫ BOUQUETS ET PRIÈRES, 1843 [DES_3]
		▫ POÉSIES INÉDITES, 1860 [DES_4]
