
	▪ Théodore de BANVILLE [BAN]

 		Liste des recueils de poésies
 		─────────────────────────────
		▫ IDYLLES PRUSSIENNES, 1871 [BAN_10]
		▫ TRENTE-SIX BALLADES JOYEUSES, 1873 [BAN_11]
		▫ LES PRINCESSES, 1874 [BAN_12]
		▫ OCCIDENTALES, 1875 [BAN_13]
		▫ RIMES DORÉES, 1875 [BAN_14]
		▫ ROSES DE NOËL, 1878 [BAN_15]
		▫ SONNAILLES ET CLOCHETTES, 1888 [BAN_16]
		▫ DANS LA FOURNAISE - Dernières Poésies, 1892 [BAN_17]
		▫ PETIT TRAITÉ DE POÉSIE FRANÇAISE, 1881 [BAN_18]
		▫ Les Cariatides, 1842 [BAN_1]
		▫ Odes funambulesques, 1857 [BAN_2]
		▫ Les Exilés, 1867 [BAN_3]
		▫ AMÉTHYSTES, 1860-1861 [BAN_4]
		▫ NOUS TOUS, 1883-1884 [BAN_5]
		▫ ODELETTES, 1856 [BAN_6]
		▫ RONDELS, 1874 [BAN_7]
		▫ LE SANG DE LA COUPE, 1857 [BAN_8]
		▫ LES STALACTITES, 1843-1846 [BAN_9]
