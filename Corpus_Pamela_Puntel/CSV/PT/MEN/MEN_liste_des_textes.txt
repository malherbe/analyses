
 ▪	Catulle MENDÈS [MEN]

 	Liste des recueils de poésies
 	─────────────────────────────
	▫	ODELETTE GUERRIÈRE - (poème extrait du recueil : Poésies, tome II, 1892)	1870 [MEN_5]
	▫	LA COLÈRE D’UN FRANC-TIREUR	1870 [MEN_6]
