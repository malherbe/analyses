
<div align="center">
<center><h3>Visualisation du corpus analysé (fichiers PDF)</h3></center>
</div>


Fichiers au format PDF. un fichier pour chaque poème et pour chaque pièce de théâtre

Les fichiers de visualisation de l'analyse métrique sont regroupés par auteur et
par recueil ou pièce de théatre.
Ces fichiers PDF ont été générés automatiquement à partir des pages HTML des
différentes étapes de l'analyse.
Dans le répertoire HTML, il y a un fichier HTML pour chacune des étapes, alors
que dans ce répertoire, le contenu de
ces fichiers est regroupé dans un seul fichier par poème ou pièce de théâtre.

Chaque fichier contient une visualisation des principales étapes de l'analyse :

- visualisation du texte brut (= ….html)

- visualisation des noyaux syllabiques avec le mètre des vers + profil(s) métrique(s)
du poème ou de la pièce (= …_6.html)

- visualisation des propriétés PCMF+L (=_6PCMF.html)

- visualisation des rimes et du schéma de rimes (= …_7.html)

- visualisation de l'extension des rimes + forme globale, valeurs calculées
de l'extension des rimes et de la qualité des rimes (= …_9.html)



▪ **couleurs_html_pdf.pdf** : codes des couleurs utilisés dans les fichiers de visualisation.



