
 ▪	Émile BERGERAT [BRG]

 	Liste des recueils de poésies
 	─────────────────────────────
	▫	A CHÂTEAUDUN	1871 [BRG_1]
	▫	HYMNE A LA FRANCE	1870 [BRG_2]
	▫	LE MAÎTRE D'ÉCOLE	1870 [BRG_3]
	▫	LE PETIT ALSACIEN	1870 [BRG_4]
	▫	LES CUIRASSIERS DE REICHSHOFFEN	1870 [BRG_5]
	▫	STRASBOURG	1870 [BRG_6]
	▫	POËMES DE LA GUERRE - 1870-1871Édition partielle	1871 [BRG_7]
