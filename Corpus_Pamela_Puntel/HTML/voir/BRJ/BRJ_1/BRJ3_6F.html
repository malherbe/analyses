<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:tei="http://www.tei-c.org/ns/1.0">
  <head>
    <title>MÉTRIQUE EN LIGNE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="traitement automatique de textes versifiés" />
    <meta name="keywords" content="métrique pésie versification" />
    <meta name="author" content="CRISCO" />
    <link type="text/css" rel="stylesheet" href="../../../outils/css/style.css" />
    <link type="text/css" rel="stylesheet" href="../../../outils/css/analyse.css" />
    <script type="text/javascript" src="../../../outils/js/infos.js"></script>
  </head>
  <body>
    <div id="curseur" class="infobulle"></div>
<div id="textes">
<div class="div_code_poeme">BRJ_1/BRJ3</div>
<div class="div_recueil_titre_corpus">corpus Pamela Puntel</div>
<div class="div_auteur">Jules BARBIER</div>
<div class="div_recueil_titre_main">LE FRANC-TIREUR</div>
<div class="div_date">1871</div>
<table class="table_body">
<tr><td class="td_titre_part_main">LE FRANC-TIREUR</td></tr>
					<tr><td class="td_titre_num">II</td></tr>
					<tr><td class="td_titre_main">TOAST<span class="appel_note">1</span>
</td></tr>
					<tr><td><table class="table_lg">
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_1">Tandis que, souriant<span class="caesura">⎟</span> aux bouteill<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s nouvell<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_2">Un peu de sens encor<span class="caesura">⎟</span> demeure en vos cervell<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s.</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_3">Et que les noirs flacons,<span class="caesura">⎟</span> dans la glace endormis.</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_4">Gard<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>nt en paix l'ivresse,<span class="caesura">⎟</span> un mot, ô mes amis !</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					</table></td></tr>
					<tr><td><table class="table_lg">
						<tr>
<td class="td_num"><span class="vers_num">5</span></td>
<td class="td_vers" id="vers_5">Et toi, Muse indolente,<span class="caesura">⎟</span> amoureus<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> des fêt<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_6">Qui te plais à tresser<span class="caesura">⎟</span> des ros<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s pour nos têt<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_7">Et, guidant au plaisir<span class="caesura">⎟</span> tes pâl<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s nourrissons,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_8">Les allait<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 4&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s de vin<span class="caesura">⎟</span> et de moll<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s chansons,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_9">Toi qui n'a pas au cœur<span class="caesura">⎟</span> un<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = Fc')" onmouseout="cache()">e</a> mâl<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> pensé<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num">10</span></td>
<td class="td_vers" id="vers_10">Et de la voix du peuple<span class="caesura">⎟</span> as l'oreill<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> blessé<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_11">N'arrêt<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 3&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> pas ici<span class="caesura">⎟</span> tes regards incertains ;</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_12">Baiss<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 2&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> ton voile et passe,<span class="caesura">⎟</span> ô Mus<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> des festins !</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					</table></td></tr>
					<tr><td><table class="table_lg">
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_13">La mus<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 3&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> que j'appelle<span class="caesura">⎟</span> et la Mus<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> que j'aim<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_14">C'est celle à qui la France<span class="caesura">⎟</span> a donné le baptêm<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> ;</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num">15</span></td>
<td class="td_vers" id="vers_15">C'est cell<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 3&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> qui s'émeut<span class="caesura">⎟</span> des cris de ses enfants,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_16">Qui nous porte, orgueilleuse,<span class="caesura">⎟</span> en ses bras triomphants,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_17">Et nous fait écouter<span class="caesura">⎟</span> au loin, par intervall<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_18">Les sonor<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 4&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s chansons<span class="caesura">⎟</span> de la poudre et des ball<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s.</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
					</table></td></tr>
					<tr><td><table class="table_lg">
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_19">Mes amis, on a dit<span class="caesura">⎟</span> que nos cœurs abattus</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num">20</span></td>
<td class="td_vers" id="vers_20">Étaient déshérités<span class="caesura">⎟</span> de tout<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s les vertus,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_21">Que nous avions perdu<span class="caesura">⎟</span> le souvenir antiqu<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a></td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_22">Des austèr<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 4&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s devoirs<span class="caesura">⎟</span> du foyer domestiqu<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_23">Et que, se renfermant<span class="caesura">⎟</span> dans son ciel oublié,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_24">Dieu retirait à lui<span class="caesura">⎟</span> l'amour et l'amitié.</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					</table></td></tr>
					<tr><td><table class="table_lg">
						<tr>
<td class="td_num"><span class="vers_num">25</span></td>
<td class="td_vers" id="vers_25">Et moi, qui sens encore<span class="caesura">⎟</span> aux bell<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s destiné<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_26">Se diriger l'ardeur<span class="caesura">⎟</span> de mes jeun<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s anné<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_27">Moi, qui suis riche encor<span class="caesura">⎟</span> d'espérance et de foi,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_28">Amis, qui crois en vous,<span class="caesura">⎟</span> amour, qui crois en toi,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_29">J'ai jeté vers les cieux<span class="caesura">⎟</span> mes hymn<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s plus fervent<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num">30</span></td>
<td class="td_vers" id="vers_30">Et j'ai dit : Non, Seigneur !<span class="caesura">⎟</span> nos âm<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s sont vivant<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_31">Ta paternell<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> main<span class="caesura">⎟</span> n'a pas déshérité</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_32">Ceux où réside encor<span class="caesura">⎟</span> l'ardent<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> charité ;</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_33">Et tu ne frapperas,<span class="caesura">⎟</span> au jour de tes colèr<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_34">Que ceux qui te chantaient<span class="caesura">⎟</span> en maudissant leurs frèr<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s ;</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num">35</span></td>
<td class="td_vers" id="vers_35">L'avenir est à nous,<span class="caesura">⎟</span> et nos pas sont vainqueurs ;</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_36">Ton culte est dans leur bouche,<span class="caesura">⎟</span> il sera dans nos cœurs !</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					</table></td></tr>
					<tr><td><table class="table_lg">
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_37">Oui, nous serons encor<span class="caesura">⎟</span> dign<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 8&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s fils de nos pèr<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_38">Mais non pas de ceux-là<span class="caesura">⎟</span> dont les vain<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s prièr<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_39">Résonnaient tout le jour<span class="caesura">⎟</span> sans fruit pour les humains,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num">40</span></td>
<td class="td_vers" id="vers_40">Non, Seigneur, mais de ceux<span class="caesura">⎟</span> qui priaient de leurs mains,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_41">Des géants qui passaient<span class="caesura">⎟</span> les mers et les montagn<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_42">Et, comme un larg<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> fleuve<span class="caesura">⎟</span> aux arid<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 10&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s campagn<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_43">Laisse après lui les fleurs<span class="caesura">⎟</span> et la fécondité,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_44">Laissaient chez. les vaincus<span class="caesura">⎟</span> la jeûn<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> liberté !</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num">45</span></td>
<td class="td_vers" id="vers_45">Nous saurons entonner<span class="caesura">⎟</span> avec idolâtri<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a></td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_46">Le chant sublime : « Allons,<span class="caesura">⎟</span> enfants de la patri<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a> !… »</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_47">Partir, les yeux au ciel<span class="caesura">⎟</span> et le sein haletant,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_48">Vaincre avec un<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = Fc')" onmouseout="cache()">e</a> fourche,<span class="caesura">⎟</span> et mourir en chantant !</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					</table></td></tr>
					<tr><td><table class="table_lg">
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_49">Et j'ai voulu, ce soir,<span class="caesura">⎟</span> entrechoquant nos verr<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num">50</span></td>
<td class="td_vers" id="vers_50">Boire un vin généreux<span class="caesura">⎟</span> aux mân<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-22&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 9&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s de nos pèr<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-5&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 13&#60;br/&#62;propriété = F')" onmouseout="cache()">e</a>s,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_a">a</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_51">Et, mêlant un<a class="pm_F" onmouseover="montre('type = ef&#60;br/&#62;règle = e-24&#60;br/&#62;phonème = ə&#60;br/&#62;valeur = 1&#60;br/&#62;place = 5&#60;br/&#62;propriété = Fc')" onmouseout="cache()">e</a> larme<span class="caesura">⎟</span> au plaisir insensé,</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
						<tr>
<td class="td_num"><span class="vers_num"></span></td>
<td class="td_vers" id="vers_52">Que le jeune avenir<span class="caesura">⎟</span> saluât le passé !</td>
<td class="td_met_66"><span class="met">6+6</span></td>
<td class="td_rime_b">b</td>
</tr>
					</table></td></tr>
					<tr><td class="td_closer">
						<span class="span_dateline">
							Juin 1846.
						</span>
						<div class="td_footnote">
<span class="footnote_id">1&nbsp;</span>
							Ces vers datent de mes vingt ans ; Peut-être leur accent détonne ;<br />
							C'est un rayon de mon printemps Dans les brumes de mon automne.
						</div>
					</td></tr>
				<tr><td><table class="table_infos_analyse"><tr>
<td class="td_infos_met"><span class="span_infos_analyse_titre">mètre</span></td>
<td class="td_profil">
<span class="span_infos_analyse_titre_1">profil métrique : </span><span class="span_infos_analyse_contenu">6+6</span>
</td>
</tr></table></td></tr>
</table>
</div>
  </body>
</html>
