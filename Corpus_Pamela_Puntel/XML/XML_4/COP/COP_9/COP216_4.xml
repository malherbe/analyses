<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">ÉCRIT PENDANT LE SIÈGE</title>
				<title type="sub">édition partielle du recueil : LES HUMBLES (1891)</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>126 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">COP_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES HUMBLES</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1020437</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES HUMBLES</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>édition partielle de la partie : ECRITS PENDANT LE SIÈGE</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP216">
				<head type="main">A L'AMBULANCE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="1.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="?" type="va" value="1" rule="161">en</seg>t</w> <w n="1.3">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.4">bru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="2.5">pr<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">d</w>'<w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="4.5">bl<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="5.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="5.6">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="6.2">b<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="6.5">l</w>'<w n="6.6"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>.</l>
					<l n="7" num="2.3"><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.2">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="7.3">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.4">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">l</w>'<w n="8.5"><seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mn<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.4">vi<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="9.6">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>st<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.7">r<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="10" num="3.2"><w n="10.1">G<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="10.2">d</w>'<w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.4">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="11.2">h<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.4">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.7">pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="12.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.5">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.4">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>p<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="13.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w> <w n="14.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="15.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="15.3">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="15.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.6">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="16" num="4.4"><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.2">n<seg phoneme="o" type="vs" value="1" rule="443">o</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>-<w n="16.5">hu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Cr<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="17.2">qu</w>'<w n="17.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="17.4">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="17.6">qu</w>'<w n="17.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.8"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.9">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="5.2"><w n="18.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.3">s<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="18.4">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="18.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.7">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="19.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.5">br<seg phoneme="y" type="vs" value="1" rule="444">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="19.6">gu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="20.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="20.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="358">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="21.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.5">v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1">L</w>'<w n="22.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t</w> <w n="22.3">qu</w>'<w n="22.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.5">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="22.6">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="22.7">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ?</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="23.2">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="23.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.5">j<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="23.7">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="24" num="6.4"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="24.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">l</w>'<w n="24.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="24.5">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.7">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rc<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="25.3">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>. — <w n="25.4">L<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="25.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="26" num="7.2"><w n="26.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="26.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="26.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="26.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ;</l>
					<l n="27" num="7.3"><w n="27.1">Pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="27.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="27.3"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="27.4">r<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tm<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>sph<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="28" num="7.4"><w n="28.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="28.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="28.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="28.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">L</w>'<w n="29.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>fl<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="29.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="29.5">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="29.6">s<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="30" num="8.2"><w n="30.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="30.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="30.6">v<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1">D<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="31.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="31.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.5">bl<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="32" num="8.4"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="32.2">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="32.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="32.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="32.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.6">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">— <w n="33.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="33.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="33.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="33.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="34" num="9.2"><w n="34.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.3">p<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="34.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="34.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w>,</l>
					<l n="35" num="9.3"><w n="35.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">gr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>gn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w>, <w n="35.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="35.4">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.5">pr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="36" num="9.4"><w n="36.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="36.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> : « <w n="36.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="36.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>-<w n="36.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> ! »</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Novembre 1870</date>
					</dateline>
				</closer>
			</div></body></text></TEI>