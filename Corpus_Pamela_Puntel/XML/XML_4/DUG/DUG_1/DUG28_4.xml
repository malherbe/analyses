<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG28">
				<head type="main">LES LIÈVRES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.5">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="1.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.8">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">L</w>'<w n="2.2"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="2.5">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.6">s</w>'<w n="2.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.3">n</w>'<w n="3.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.7">j<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.9">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w>, <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.3">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">scr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="5.3">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.5">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !…</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="6.2">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ct</w>, <w n="6.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>-<w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.7">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.9">l</w>'<w n="6.10"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="7" num="2.2"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="7.2">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="7.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">fl<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>lti<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
					<l n="8" num="2.3"><w n="8.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.2">v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="8.4">c</w>'<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.7">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="8.8">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,'</l>
					<l n="9" num="2.4"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.4">qu</w>'<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="9.6">B<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rk</w> <w n="9.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.8">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.5"><space unit="char" quantity="8"></space><w n="10.1">C</w>'<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.4">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="10.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="10.6">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !…</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">—<w n="11.1">S</w>'<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t</w>, <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="11.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>, <w n="11.7">d</w>'<w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.9">l<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.10">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="11.11">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="3.2"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w>,</l>
					<l n="13" num="3.3"><w n="13.1">N</w>'<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>-<w n="13.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="13.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.6">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bs<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>, <w n="13.8">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="13.9">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="14" num="3.4"><w n="14.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>pp<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.5">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.7">bl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>sph<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="3.5"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="15.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="15.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> !…</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w>, <w n="16.3">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="16.4">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg>s</w> ! <w n="16.5">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> ! <w n="16.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="17" num="4.2"><w n="17.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="17.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.5">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>, <w n="17.6">c</w>'<w n="17.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.8">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="17.9">qu</w>'<w n="17.10"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="17.11">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="17.12">h<seg phoneme="a" type="vs" value="1" rule="342">a</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg>r</w>,</l>
					<l n="18" num="4.3"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="18.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="18.3">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="18.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="18.6">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="18.7">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="19" num="4.4"><w n="19.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>… <w n="19.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="19.7">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="19.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.9">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="20" num="4.5"><space unit="char" quantity="8"></space><w n="20.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="20.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="20.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !…</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="21.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="21.5">d</w>'<w n="21.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xc<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="21.7">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.8">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="22" num="5.2"><w n="22.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="22.2">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.4">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="22.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.6">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="22.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="22.8">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> !</l>
					<l n="23" num="5.3"><w n="23.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="23.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w>, <w n="23.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rtr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="23.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.9">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="5.4"><w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="24.3">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="24.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>qu<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="24.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>-<w n="24.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="24.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.8">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="25" num="5.5"><space unit="char" quantity="8"></space><w n="25.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="25.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="25.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> !</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="26.5">hu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="26.6">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.8">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="27" num="6.2"><w n="27.1">F<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="27.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="27.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="27.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.9">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.10">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>,</l>
					<l n="28" num="6.3"><w n="28.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="28.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.3">m<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="28.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="28.5">l</w>'<w n="28.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="29" num="6.4"><w n="29.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="29.2">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="29.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="29.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="29.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.7">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="29.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.10">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="30" num="6.5"><space unit="char" quantity="8"></space>— <w n="30.1"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> ! <w n="30.2">ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="30.3">c</w>'<w n="30.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="30.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="30.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="30.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> !…</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><w n="31.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="31.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="31.4">l</w>'<w n="31.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w>, <w n="31.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="31.8">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="31.9">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="31.10">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="32" num="7.2"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="32.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="32.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="32.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="32.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="32.7">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>,</l>
					<l n="33" num="7.3"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="33.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="33.4">p<seg phoneme="o" type="vs" value="1" rule="443">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="413">ë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="33.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> <w n="33.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>s</w> <w n="33.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.9">fi<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="34" num="7.4"><w n="34.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="34.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="34.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="34.5">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="34.7">li<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="35" num="7.5"><space unit="char" quantity="8"></space><w n="35.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="35.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="35.5">l<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>