<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR21">
				<head type="main">18 MARS 1871</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>L<seg phoneme="ɔ" type="vs" value="1" rule="438">O</seg>RS</w> <w n="1.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="1.3">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>q</w> <w n="1.4">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="1.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="1.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> ;</l>
					<l n="2" num="1.2"><w n="2.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>q</w> <w n="2.3">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w>, <w n="2.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="2.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.2">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="3.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> ; <w n="3.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="3.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="3.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="3.8">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="4.4">t<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="4.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="5" num="1.5"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="5.5">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="6.4">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>squ<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="6.5">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="6.6">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
					<l n="7" num="1.7"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.3">l</w>'<w n="7.4">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="7.6">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="8" num="1.8"><w n="8.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xp<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="8.4">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="8.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.7">v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="9" num="1.9"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="302">aim</seg></w>, <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">fr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>d</w>, <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.9">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> ;</l>
					<l n="10" num="1.10"><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="10.3">v<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>,</l>
					<l n="11" num="1.11"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="11.5">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.8">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="11.9">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="12" num="1.12"><w n="12.1">Pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w>, <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="12.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.4">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>q</w> <w n="12.5">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="13" num="1.13"><w n="13.1">C<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>q</w> <w n="13.2">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="13.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ls</w> <w n="13.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
					<l n="14" num="1.14"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="14.2">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w>, <w n="14.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="14.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
					<l n="15" num="1.15"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="15.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="15.4">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>, — <w n="15.5">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.7">ch<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="15.9">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, —</l>
					<l n="16" num="1.16"><w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="16.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="16.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="16.6">cr<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="17" num="1.17"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ds</w> <w n="17.5">p<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="17.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="17.7">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="17.8">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rs</w>,</l>
					<l n="18" num="1.18"><w n="18.1">S<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="18.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="18.3">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ts</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="18.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cts</w> <w n="18.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rs</w>,</l>
					<l n="19" num="1.19"><w n="19.1">D</w>’<w n="19.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="19.3"><seg phoneme="œ" type="vs" value="1" rule="285">œ</seg>il</w> <w n="19.4">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="19.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="19.6">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="19.7">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="20" num="1.20"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>tt<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="20.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="20.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="20.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.6">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>ps</w>-<w n="20.7"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>l<seg phoneme="i" type="vs" value="1" rule="492">y</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
					<l part="I" n="21" num="1.21"><w n="21.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="21.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="21.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="21.4">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="21.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ? — </l>
					<l part="F" n="21" num="1.21"><w n="21.6">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="21.7">qu</w>’<w n="21.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="21.9"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="21.10">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w></l>
					<l n="22" num="1.22"><w n="22.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.2">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>, <w n="22.4">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="22.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="22.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.7">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w>,</l>
					<l n="23" num="1.23"><w n="23.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="23.2">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w>, — <w n="23.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="23.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="23.6">s<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="23.8">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! —</l>
					<l n="24" num="1.24"><w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="338">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="24.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="24.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.4">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="24.6">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>lt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="24.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.8">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>