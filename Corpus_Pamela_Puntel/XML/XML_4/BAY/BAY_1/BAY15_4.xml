<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA FRONTIÈRE</title>
				<title type="sub">ESSAIS DE POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BAY">
					<name>
						<forename>Hippolyte</forename>
						<surname>BAYE</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>864 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BAY_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FRONTIÈRE. ESSAIS DE POÉSIES</title>
						<author>BAYE, HIPPOLYTE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54640834.r=BAYE%2C%20HIPPOLYTE%20ESSAIS%20DE%20PO%C3%89SIES%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA FRONTIÈRE. ESSAIS DE POÉSIES</title>
								<author>BAYE, HIPPOLYTE</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE LACROIX, VERBOECKHOVËN ET CIE ÉDITEURS</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAY15">
				<head type="main">POUR DES OUBLIÉS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="wa" type="vs" value="1" rule="419">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.4">fl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="1.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="2.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="2.3">j<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>,</l>
					<l n="3" num="1.3"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="3.6">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w></l>
					<l n="4" num="1.4"><w n="4.1">S</w>'<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="4.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.5">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="5.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">f<seg phoneme="ɥi" type="vs" value="1" rule="461">u</seg>y<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="6.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w>, <w n="6.4">l</w>'<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg>y<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="2.2"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.3">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.6">bu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w></l>
					<l n="8" num="2.3"><w n="8.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">d</w>'<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> :</l>
					<l n="9" num="2.4"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">n</w>'<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.4">qu</w>'<w n="9.5"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.7">br<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="2.5"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.3">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="10.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.6">c<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="11.2">c</w>'<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">t<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.8">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="12" num="3.2"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="12.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>rs</w> <w n="12.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w></l>
					<l n="13" num="3.3"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">br<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="13.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.7">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>cs</w>,</l>
					<l n="14" num="3.4"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>li<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="14.4">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="3.5"><w n="15.1">V<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="15.2">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="15.3">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="15.4">j<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="15.6">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> ?</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="16.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d</w> <w n="16.4">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="17" num="4.2"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rtr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.5"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="17.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="17.7">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>,</l>
					<l n="18" num="4.3"><w n="18.1">N<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="18.3">n</w>'<w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="18.5">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.7">b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd</w>,</l>
					<l n="19" num="4.4"><w n="19.1">N<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l</w> <w n="19.2">pi<seg phoneme="e" type="vs" value="1" rule="240">e</seg>d</w> <w n="19.3">n</w>'<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="19.6">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="4.5"><w n="20.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="20.4"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="20.5">v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.7">M<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> !</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="21.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="21.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="21.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="5.2"><w n="22.1">Tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="22.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="22.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w>, <w n="22.4">tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>.</l>
					<l n="23" num="5.3"><w n="23.1">N<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l</w> <w n="23.2">n</w>'<w n="23.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="23.4">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="23.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="23.6">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> :</l>
					<l n="24" num="5.4"><w n="24.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="24.2">f<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="24.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="25" num="5.5"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="25.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="25.3">d</w>'<w n="25.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="25.6"><seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> !</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.2">fl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="26.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="26.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="26.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="26.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="27" num="6.2"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.3">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="27.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>.</l>
					<l n="28" num="6.3"><w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="28.2"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="28.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="28.4">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="28.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="28.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> ?</l>
					<l n="29" num="6.4"><w n="29.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="29.2">bu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w>, <w n="29.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="29.4">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.6">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="30" num="6.5"><w n="30.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="30.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="30.3">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="30.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="30.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="30.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> ?</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="31.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="31.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="31.4">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="31.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="31.6">l</w>'<w n="31.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="32" num="7.2"><w n="32.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg></w> <w n="32.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="32.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rt</w> <w n="32.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="32.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>,</l>
					<l n="33" num="7.3"><w n="33.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="33.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="33.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="33.4">b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rds</w> <w n="33.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="33.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>,</l>
					<l n="34" num="7.4"><w n="34.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="34.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="35" num="7.5"><w n="35.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">n<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l</w> <w n="35.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.4">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="35.6">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="36" num="8.1"><w n="36.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="36.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="36.3">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="36.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="36.5">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="36.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.7">h<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>…</l>
					<l n="37" num="8.2"><w n="37.1">C</w>'<w n="37.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="37.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="37.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="37.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="37.6"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w>,</l>
					<l n="38" num="8.3"><w n="38.1">D</w>'<w n="38.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="38.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.4">f<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w>.</l>
					<l n="39" num="8.4"><w n="39.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="39.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="39.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="39.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="39.5">h<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="40" num="8.5"><w n="40.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="40.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="40.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.4">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="40.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="40.6">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rs</w> <w n="40.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w>.</l>
				</lg>
				<lg n="9">
					<l n="41" num="9.1"><w n="41.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="41.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="41.3">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="42" num="9.2"><w n="42.1"><seg phoneme="wa" type="vs" value="1" rule="419">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="42.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="42.3">g<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="42.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> ;</l>
					<l n="43" num="9.3"><w n="43.1">R<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="43.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="43.3">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="43.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> ;</l>
					<l n="44" num="9.4"><w n="44.1">Fl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="44.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="44.3">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="44.4">bru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="44.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.6">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="44.7"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="45" num="9.5"><w n="45.1">C<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="45.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="45.3">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="45.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="45.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="45.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Avril 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>