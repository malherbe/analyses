<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="LON">
					<name>
						<forename>Eugène</forename>
						<nameLink>de</nameLink>
						<surname>LONLAY</surname>
					</name>
					<date from="1815" to="1886">1815-1886</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>52 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">LON_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>>POÉSIES, ÉDITION ELZÉVIRIENNE</title>
						<author>EUGÈNE DE LONLAY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k65208385/f11.image.r=EUG%C3%88NE%20DE%20LONLAY</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES, ÉDITION ELZÉVIRIENNE</title>
								<author>EUGÈNE DE LONLAY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALCAN-LEVY</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="LON2">
				<head type="main">PRIÈRE</head>
				<head type="main">AVANT LA BATAILLE</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="1.2">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="1.3">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.3">gu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="2.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>.</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.7">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="4.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gs</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="5" num="1.1"><w n="5.1">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="5.2">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w>, <w n="5.5">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="5.7">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="5.8">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="6" num="1.2"><w n="6.1">T<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="6.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.4">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="6.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.6">cr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rts</w>,</l>
						<l n="7" num="1.3"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="7.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="7.3">v<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>x</w>, <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="o" type="vs" value="1" rule="317">au</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="7.6">pr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="8" num="1.4"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="8.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="8.4">v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="9" num="1.1"><w n="9.1">C</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.4">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="9.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="9.6">qu</w>'<w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.8">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="9.9">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="9.10">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.11">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="1.2"><w n="10.1">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="10.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="10.3">l</w>'<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ctr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.7">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w> ;</l>
						<l n="11" num="1.3"><w n="11.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="11.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="11.4">y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="11.5">r<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg>y<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.7">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="1.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="12.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="12.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>