<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP19">
				<head type="number">XIX</head>
				<head type="main">DEVANT UN BERCEAU</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.3">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="1.4">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.7">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="1.8">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="2" num="2.1"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">l</w>'<w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.7">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">l</w>'<w n="2.10"><seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w></l>
					<l n="3" num="2.2"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.3">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.5">l</w>'<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.8">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.9">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.10">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.11">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="2.3"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg>t</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.3">r<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg>y<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="4.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.6">l<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="5" num="2.4"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="5.4">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="5.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.8">c<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> :</l>
					<l n="6" num="2.5"><w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">g<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
					<l n="7" num="2.6"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="7.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.6">j<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.9">v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="8" num="2.7"><w n="8.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="8.2">j</w>'<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="8.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="8.6">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.9"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="9" num="2.8"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">bru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="9.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="9.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="9.7">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>-<w n="9.8">V<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>…</l>
					<l n="10" num="2.9"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="492">y</seg></w> <w n="10.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="10.4">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="10.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="11.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.5">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="11.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.9">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.10"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="3.2"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="12.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="12.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="13" num="3.3"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>, <w n="13.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="13.6"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="13.7">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>,</l>
					<l n="14" num="3.4"><w n="14.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="14.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.6">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="14.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.8">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="14.9">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ;</l>
					<l n="15" num="3.5"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.2">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>, <w n="15.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="15.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.5">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="15.6">qu</w>'<w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="15.8">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="15.9">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="3.6"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="16.2">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="16.3">j<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="16.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="16.6">s</w>'<w n="16.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="17" num="3.7"><w n="17.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="17.2">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="17.3">d</w>'<w n="17.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="17.5">v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="17.6">m<seg phoneme="i" type="vs" value="1" rule="492">y</seg>st<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,…</l>
				</lg>
				<lg n="4">
					<l n="18" num="4.1"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="18.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="18.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="18.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.9">y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> !</l>
				</lg>
				<lg n="5">
					<l n="19" num="5.1"><w n="19.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="19.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>, <w n="19.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="19.9">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.10">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="5.2"><w n="20.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.6">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="20.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.10">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="21" num="5.3"><w n="21.1">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="21.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="21.5">l</w>'<w n="21.6">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.7">d</w>'<w n="21.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>-<w n="21.9">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>,</l>
					<l n="22" num="5.4"><w n="22.1">D</w>'<w n="22.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="22.3">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="22.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="22.6">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="22.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="22.8">fr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>,</l>
					<l n="23" num="5.5"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w>, <w n="23.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="23.7">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.8">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="5.6"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="24.2">t</w>-<w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.4">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="24.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="24.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="24.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="24.10"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="25" num="5.7"><w n="25.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.2">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="25.4">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="25.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="25.6">n<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l</w> <w n="25.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.8">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="25.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.10">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w>,</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">M<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w>, <w n="26.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.4">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="26.6">l</w>'<w n="26.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> <w n="26.8">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="26.9">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> ?</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 28 octobre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>