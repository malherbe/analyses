<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ47">
					<head type="number">XLVII</head>
					<head type="main">ON SE BAT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="1.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> !… <w n="1.4">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">m<seg phoneme="o" type="vs" value="1" rule="437">o</seg>t</w> <w n="1.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="1.7">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><space unit="char" quantity="6"></space><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.3">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="2.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> ;</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="3.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="3.4">l</w>'<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="3.9">fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><space unit="char" quantity="6"></space><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> !</l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="5.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> !… <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="5.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> ? <w n="5.6">n<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l</w> <w n="5.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="5.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.10">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="6" num="1.6"><space unit="char" quantity="6"></space><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>-<w n="6.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="6.5">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>-<w n="7.2">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="7.3">v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.5">L<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="7.7">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>»<w n="7.9">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="8" num="1.8"><space unit="char" quantity="6"></space><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !…</l>
						<l n="9" num="1.9"><w n="9.1">L</w>'<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.6"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.7"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="1.10"><space unit="char" quantity="6"></space><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="10.2">bru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="10.3">d</w>'<w n="10.4"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="10.7">l</w>'<w n="10.8"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> ;</l>
						<l n="11" num="1.11"><w n="11.1">P<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.5">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="12" num="1.12"><space unit="char" quantity="6"></space><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.2">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.4">l</w>'<w n="12.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> !</l>
						<l n="13" num="1.13"><w n="13.1">S<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="13.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="13.4">hu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> ? <w n="13.5">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="13.6">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="13.7">t</w>-<w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="14" num="1.14"><space unit="char" quantity="6"></space><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>fs</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>.</l>
						<l n="15" num="1.15"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="15.2">l</w>'<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="339">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ! <w n="15.5">l</w>'<w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.7">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="16" num="1.16"><space unit="char" quantity="6"></space><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="16.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="16.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> !…</l>
						<l n="17" num="1.17"><w n="17.1">N<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="17.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="17.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="17.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="17.6">l</w>'<w n="17.7"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="1.18"><space unit="char" quantity="6"></space><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.2">m<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="18.6">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w> ;</l>
						<l n="19" num="1.19"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="19.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="19.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="19.4">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="19.5">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="19.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.7">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.8">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="1.20"><space unit="char" quantity="6"></space><w n="20.1">N</w>'<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="20.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="20.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="20.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.7">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w> ?…</l>
						<l n="21" num="1.21">— <w n="21.1">N<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="21.2">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="1.22"><space unit="char" quantity="6"></space><w n="22.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>-<w n="22.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="22.5">c<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
						<l n="23" num="1.23"><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="23.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">t<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.6">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="1.24"><space unit="char" quantity="6"></space><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="425">Où</seg></w> <w n="24.2">l</w>'<w n="24.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="24.4">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rt</w> <w n="24.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="24.6">s</w>'<w n="24.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ?…</l>
						<l n="25" num="1.25"><w n="25.1">C</w>'<w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="25.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.5">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !… <w n="25.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="25.7">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="25.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="25.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="26" num="1.26"><space unit="char" quantity="6"></space><w n="26.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="26.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="26.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="26.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="26.6">si<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> ;</l>
						<l n="27" num="1.27"><w n="27.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="27.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="27.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="27.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="27.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.7">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>… <w n="27.8"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="27.9">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="28" num="1.28"><space unit="char" quantity="6"></space><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="28.2"><seg phoneme="y" type="vs" value="1" rule="390">eu</seg></w> <w n="28.3">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="28.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.5">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> !…</l>
						<l n="29" num="1.29"><w n="29.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="29.3">bru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="29.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>-<w n="29.5">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w>. — <w n="29.6"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> <w n="29.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> ?…— <w n="29.8">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="29.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
						<l n="30" num="1.30"><space unit="char" quantity="6"></space><w n="30.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="30.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> ;</l>
						<l n="31" num="1.31"><w n="31.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">br<seg phoneme="y" type="vs" value="1" rule="444">û</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="31.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="32" num="1.32"><space unit="char" quantity="6"></space><w n="32.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="32.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="32.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="32.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> !</l>
						<l n="33" num="1.33"><w n="33.1">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="33.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>-<w n="33.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="33.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w>, <w n="33.5">h<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="34" num="1.34"><space unit="char" quantity="6"></space><w n="34.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="34.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t</w> ?…</l>
						<l n="35" num="1.35"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.2">nu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> ! <w n="35.3">nu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="35.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="35.6">gl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
						<l n="36" num="1.36"><space unit="char" quantity="6"></space><w n="36.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="36.3">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> !… <w n="36.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="36.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.6">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> !…</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">10 novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>