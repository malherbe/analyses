<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ92">
					<head type="main">ÉPILOGUE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> <w n="1.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>, <w n="1.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> !… <w n="1.4">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>, <w n="1.6">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="1.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> !… <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.9">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.2">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="2.6"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.8">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="3" num="1.3"><w n="3.1">L</w>'<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… — <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="3.5">d</w>'<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w>,</l>
						<l n="4" num="1.4"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">br<seg phoneme="y" type="vs" value="1" rule="444">û</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.6">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">j</w>'<w n="4.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> !</l>
						<l n="5" num="1.5"><w n="5.1">J</w>'<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">l<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="5.6">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="5.8"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w>,</l>
						<l n="6" num="1.6"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="6.2">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>, <w n="6.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="6.6">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.8">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w>,</l>
						<l n="7" num="1.7"><w n="7.1">S</w>'<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.5">fl<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w> <w n="7.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="7.7">c<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.8"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="7.9">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>,</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.5">y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="8.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.8">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="8.9">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="8.10">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ds</w> !</l>
						<l n="9" num="1.9"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="9.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, <w n="9.6">s<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.9">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="1.10"><w n="10.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>, <w n="10.5">l</w>'<w n="10.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="1.11"><w n="11.1">Qu</w>'<w n="11.2"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="11.6">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>sh<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ;</l>
						<l n="12" num="1.12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="12.5">j</w>'<w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.8">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="12.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.10">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w>-<w n="13.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="13.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> ?… <w n="13.7">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.10">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="2.2"><w n="14.1">Pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="14.3">l</w>'<w n="14.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="15" num="2.3"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="15.4">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="15.5">s</w>'<w n="15.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="15.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="15.8">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.9">l<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
						<l n="16" num="2.4"><w n="16.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="16.2">qu</w>'<w n="16.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.8">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="16.9"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.10">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="17.4">qu</w>'<w n="17.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="17.6">f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="17.8">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.9">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="17.10">C<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="3.2"><w n="18.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>, <w n="18.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="18.7">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="18.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="18.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.10">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="19" num="3.3"><w n="19.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.3">g<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="19.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="19.6">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="19.7">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ;</l>
						<l n="20" num="3.4"><w n="20.1">L</w>'<w n="20.2"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> ; <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w>… <w n="20.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="20.7">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> ?… <w n="20.8">l</w>'<w n="20.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">Em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ! —</l>
						<l n="21" num="3.5"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="21.2">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="49">e</seg>s</w>-<w n="21.5">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="21.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="21.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.8">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="22" num="3.6"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.2">f<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ss<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="22.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>… — <w n="22.5">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="22.7">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>, <w n="22.8">qu</w>'<w n="22.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="23" num="3.7"><w n="23.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="23.2">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="23.4">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="23.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> ;</l>
						<l n="24" num="3.8"><w n="24.1">L</w>'<w n="24.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="24.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="24.5">s<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r</w>, <w n="24.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.8">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w>.</l>
						<l n="25" num="3.9"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="25.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="25.3">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>, <w n="25.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">n</w>'<w n="25.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="25.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="25.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.9">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="25.10">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="25.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="26" num="3.10"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="26.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="26.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="26.4">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.6">L<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="26.8">l</w>'<w n="26.9"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ls<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="27" num="3.11"><w n="27.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="27.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="27.3">t</w>-<w n="27.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="27.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="27.6">gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ? <w n="27.7">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.8">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="27.9">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="27.10">vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> !</l>
						<l n="28" num="3.12"><w n="28.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.2">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="28.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="28.4"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="28.5">l</w>'<w n="28.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="28.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w>… —</l>
						<l n="29" num="3.13"><w n="29.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> ! <w n="29.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="29.3">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="29.4"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="29.5">l</w>'<w n="29.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.9">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="3.14"><w n="30.1">V<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="30.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="30.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="30.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.5">n<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="30.6">j<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="30.8">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="30.9">d</w>'<w n="30.10"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
						<l n="31" num="3.15"><w n="31.1">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="31.2">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="31.3">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="31.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ! <w n="31.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="31.6">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="31.7">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="31.8">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ps</w> !</l>
						<l n="32" num="3.16"><w n="32.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="32.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="32.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="32.6">d</w>'<w n="32.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="32.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="32.9">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="32.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.11">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> !</l>
					</lg>
					<lg n="4">
						<l n="33" num="4.1"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ! <w n="33.2">c</w>'<w n="33.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="33.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> ! <w n="33.5">D<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="33.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="33.7">B<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rk</w> <w n="33.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="33.9">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="34" num="4.2"><w n="34.1">S<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="34.2">g<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="34.3">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="34.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="34.5">t<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="34.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="34.7">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="35" num="4.3"><w n="35.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg>z<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="35.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="35.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.5">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="35.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>sc<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</l>
						<l n="36" num="4.4"><w n="36.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>, <w n="36.2">f<seg phoneme="œ" type="vs" value="1" rule="303">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="36.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="36.4">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> !… — <w n="36.5">Qu</w>'<w n="36.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="36.7">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>-<w n="36.8">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="36.9">G<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="e" type="vs" value="1" rule="352">e</seg>tt<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ?</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">29 janvier 1871.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>