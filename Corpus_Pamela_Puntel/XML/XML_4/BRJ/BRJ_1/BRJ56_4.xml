<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ56">
					<head type="number">LVI</head>
					<head type="main">PRIÈRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>'<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">d</w>'<w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.5">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="1.7">s</w>'<w n="1.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.10">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="1.11">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="1.12">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="2.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="2.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.8">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.9">l<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.4"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="3.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.6">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="3.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>g<seg phoneme="y" type="vs" value="1" rule="447">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.5">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w> <w n="4.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.8">v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="4.9">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.7">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">f<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="6" num="3.1"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">n</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.6">pr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.9">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="3.2"><w n="7.1">D</w>'<w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.3">r<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.5">t</w>'<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.8">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>,</l>
						<l n="8" num="3.3"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.2">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="8.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.6">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.7">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="9" num="3.4"><w n="9.1">L</w>'<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>, <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="9.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.7">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ;</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.2">pr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> !</l>
						<l n="11" num="4.2"><w n="11.1">C</w>'<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">d</w>'<w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="11.6">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="11.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="11.8">l</w>'<w n="11.9"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343">ue</seg>il</w> <w n="11.10">s</w>'<w n="11.11">h<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="4.3"><w n="12.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ts</w> <w n="12.4">n</w>'<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="12.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="12.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.9">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ts</w>,</l>
						<l n="13" num="4.4"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="13.5">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>, <w n="13.7">s<seg phoneme="ɛ" type="vs" value="1" rule="383">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="13.9">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.10">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ppl<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.5"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ps</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="14.6">d</w>'<w n="14.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="14.8">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.9">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> !</l>
					</lg>
					<lg n="5">
						<l n="15" num="5.1"><w n="15.1">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="15.3">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="15.4">ch<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="15.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.6">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="15.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.9">r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> !…</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1"><w n="16.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="16.2">r<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="16.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.8">v<seg phoneme="u" type="vs" value="1" rule="424">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="17" num="6.2"><w n="17.1">D</w>'<w n="17.2"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.3">cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="17.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="17.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">ch<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> ;</l>
						<l n="18" num="6.3"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.2">br<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">l</w>'<w n="18.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="18.7">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="19" num="6.4"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.3">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="19.5">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="19.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="19.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="19.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.9">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> !…</l>
					</lg>
					<lg n="7">
						<l n="20" num="7.1"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="20.2">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="20.3">h<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="20.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="20.5">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="20.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">22 novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>