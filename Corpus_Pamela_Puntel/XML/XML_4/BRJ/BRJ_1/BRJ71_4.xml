<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ71">
					<head type="number">LXXI</head>
					<head type="main">LE VIEILLARD TARENTIN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> : <w n="1.5">c</w>'<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.7"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">vi<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.9">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">Qu</w>'<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.3">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">P<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="2.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.9">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.2">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="3.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>,</l>
						<l n="4" num="2.2"><w n="4.1">Pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.3">l</w>'<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.6">v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="4.7">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>.</l>
						<l n="5" num="2.3"><space unit="char" quantity="8"></space><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.7">j<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="6" num="2.4"><space unit="char" quantity="8"></space><w n="6.1">N<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="6.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="6.5">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w></l>
						<l n="7" num="2.5"><space unit="char" quantity="8"></space><w n="7.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="7.3">l</w>'<w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.7">ci<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>.</l>
						<l n="8" num="2.6"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="8.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">pr<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="9" num="2.7"><w n="9.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.6">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.9"><seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w></l>
						<l n="10" num="2.8"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">gl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="10.7"><seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> !…</l>
						<l n="11" num="2.9"><space unit="char" quantity="8"></space><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="11.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w>, <w n="11.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="11.4">l</w>'<w n="11.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="2.10"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w> <w n="12.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="12.7">l</w>'<w n="12.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w>.</l>
						<l n="13" num="2.11"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="13.2">s</w>'<w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>qui<seg phoneme="ɛ" type="vs" value="1" rule="32">e</seg>rt</w> <w n="13.4">d</w>'<w n="13.5"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="13.6">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="13.7">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="13.8">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.9">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="14" num="2.12"><space unit="char" quantity="8"></space><w n="14.1">Lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="14.4">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gt</w> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.6">vi<seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> :</l>
						<l n="15" num="2.13">« <w n="15.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="15.3">d<seg phoneme="y" type="vs" value="1" rule="444">û</seg></w> <w n="15.4">j<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="15.5">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.6">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="15.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="15.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.10">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="2.14">» <w n="16.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>-<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w>, <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.5">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="16.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="16.7">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>. <w n="16.8">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.9">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w></l>
						<l n="17" num="2.15">» <w n="17.1">Qu</w>'<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="17.3">s</w> <w n="17.4"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="17.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="17.7">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.8">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.10">b<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>.</l>
						<l n="18" num="2.16">» <w n="18.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="18.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.4">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> ? <w n="18.5">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="18.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>b<seg phoneme="y" type="vs" value="1" rule="444">û</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.9">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !»</l>
						<l n="19" num="2.17"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w>, <w n="19.4">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">m<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>qu<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> : « <w n="19.7"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> ! <w n="19.8">qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ?</l>
						<l n="20" num="2.18"><space unit="char" quantity="8"></space>» <w n="20.1">Cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="20.3">vi<seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> ? <w n="20.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="21" num="2.19"><space unit="char" quantity="8"></space>» <w n="21.1">Qu</w>'<w n="21.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="21.3">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">nu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ? <w n="21.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="21.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="21.8">c<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> !</l>
						<l n="22" num="2.20">» <w n="22.1">F<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>-<w n="22.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="22.4">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="22.5">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>fl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="22.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.8">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="23" num="2.21">» <w n="23.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.2">f<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="23.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="23.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="23.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="23.7">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ?</l>
						<l n="24" num="2.22"><space unit="char" quantity="8"></space>» <w n="24.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="24.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="24.3">j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="24.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.6">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="25" num="2.23"><space unit="char" quantity="8"></space>» <w n="25.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">n</w>'<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="25.5">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="25.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.7">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="25.8">cr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> !</l>
						<l n="26" num="2.24">» <w n="26.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="26.2">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">n</w>'<w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="26.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="26.6">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="26.7">d</w>'<w n="26.8"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.9">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>. » —</l>
						<l n="27" num="2.25"><w n="27.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="27.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="27.5">v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="27.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="27.7">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w></l>
						<l n="28" num="2.26"><space unit="char" quantity="8"></space><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">T<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>. <w n="28.3">L</w>'<w n="28.4">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="28.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="28.6">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="29" num="2.27"><space unit="char" quantity="8"></space><w n="29.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">In</seg>cl<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="29.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.</l>
						<l n="30" num="2.28"><w n="30.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="30.4">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="30.5">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="30.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.7">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="31" num="2.29"><w n="31.1">Pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="31.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.3">v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="31.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.5">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="31.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="31.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="31.8">l</w>'<w n="31.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="31.10">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="31.11"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w></l>
						<l n="32" num="2.30"><w n="32.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="32.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="32.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="32.6">g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="306">a</seg>il</w> <w n="32.7">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="33" num="2.31"><space unit="char" quantity="8"></space><w n="33.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="33.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="33.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="33.4">fl<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w>, <w n="33.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="33.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="33.8">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>,</l>
						<l n="34" num="2.32"><w n="34.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="34.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="34.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="34.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="34.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="34.6">vi<seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> <w n="34.7"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
						<l n="35" num="2.33"><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="35.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> <w n="35.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="35.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="35.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="35.6">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="35.7">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="36" num="3.1"><w n="36.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="36.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="36.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="36.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="36.5">Vi<seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> <w n="36.6">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>