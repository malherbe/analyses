<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ87">
					<head type="number">LXXXVII</head>
					<head type="main">NOTRE FRITZ</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">J</w>'<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rgn<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>-<w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ;</l>
						<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">cr<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w>, <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="2.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="2.8">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>tt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="4.4">m<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="4.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ! <w n="5.2">C<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="5.4">l</w>'<w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w></l>
						<l n="6" num="1.6"><w n="6.1">L</w>'<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="6.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.5">d</w>'<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.8">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.11">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.5">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>g<seg phoneme="y" type="vs" value="1" rule="447">u</seg>st<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w></l>
						<l n="8" num="1.8"><w n="8.1">F<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.3">l</w>'<w n="8.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.7">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="8.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.10">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, <w n="9.3">d</w>'<w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.6">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w></l>
						<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="10.3">l</w>'<w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="10.5">d</w>'<w n="10.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="11.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="11.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.6">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w></l>
						<l n="12" num="1.12"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="12.2">l</w>'<w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.7">r<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.9">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.3">Fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tz</w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>b<seg phoneme="wa" type="vs" value="1" rule="419">oî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ;</l>
						<l n="14" num="1.14"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="14.2">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="14.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="14.9">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space>« <w n="15.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.2">Fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tz</w> ! »… <w n="15.3">N<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">Fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tz</w> <w n="15.5">n</w>'<w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="15.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w></l>
						<l n="16" num="1.16"><w n="16.1">D</w>'<w n="16.2">h<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="16.6">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="16.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.9">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="17.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="17.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>,</l>
						<l n="18" num="1.18"><w n="18.1">B<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ll</w>-<w n="18.2">d<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>g</w> <w n="18.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="18.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="18.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.6">cr<seg phoneme="o" type="vs" value="1" rule="436">o</seg>cs</w> <w n="18.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="18.9">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ;</l>
						<l n="20" num="1.20"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="20.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="20.4">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="20.6">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="20.7">fl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="20.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.9">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space>(<w n="21.1">C</w>'<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="21.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="21.4">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="21.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>.) — <w n="21.8">Br<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>f</w>,</l>
						<l n="22" num="1.22"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="22.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="22.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="22.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="22.7">d</w>'<w n="22.8"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="22.9">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.10">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"></space><w n="23.1">Fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tz</w> <w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="23.3">f<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="23.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>f</w> ;</l>
						<l n="24" num="1.24"><w n="24.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="24.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="24.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.4">Fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tz</w> <w n="24.5">n</w>'<w n="24.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="24.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.8">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">Janvier 1871.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>