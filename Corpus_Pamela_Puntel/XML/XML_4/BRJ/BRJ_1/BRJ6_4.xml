<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ6">
					<head type="number">V</head>
					<head type="main">LA GUERRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w>, <w n="1.4">l</w>'<w n="1.5">h<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1">L</w>'<w n="2.2"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> <w n="2.3">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> :</l>
						<l n="3" num="1.3"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="4.5">t<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ;</l>
						<l n="5" num="1.5"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.2">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w> <w n="5.3">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.6">fl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">fl<seg phoneme="o" type="vs" value="1" rule="437">o</seg>t</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
						<l n="7" num="1.7"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.2">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.5">v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !…</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="8.2">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="8.3">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="8.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.4">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>gn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="9.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="10" num="2.2"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>' <w n="10.3">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">gr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="10.7">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>,</l>
						<l n="11" num="2.3"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.2">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.4">d</w>'<w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="2.4"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.3">r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="12.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="12.6">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> ;</l>
						<l n="13" num="2.5"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.2">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg><seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="14" num="2.6"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="14.2">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.5">r<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
						<l n="15" num="2.7"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="434">O</seg>ffr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.5">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !…</l>
						<l n="16" num="2.8"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="16.2">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="16.3">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="16.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.2"><seg phoneme="y" type="vs" value="1" rule="449">u</seg>hl<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>, <w n="17.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="18" num="3.2"><w n="18.1">S</w>'<w n="18.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="18.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="18.4">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="18.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="18.6">gu<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.3">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="19.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="20" num="3.4"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="20.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="20.3">Gr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tch<seg phoneme="ɛ" type="vs" value="1" rule="24">e</seg>n</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> ;</l>
						<l n="21" num="3.5"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">v<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="21.3">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.5">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="22" num="3.6"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="22.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.5">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> :</l>
						<l n="23" num="3.7"><w n="23.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.2">s</w>'<w n="23.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.5">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
						<l n="24" num="3.8"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="24.2">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="24.3">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="24.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="25.2">r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="25.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="25.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="26" num="4.2"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="26.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">gr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="26.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> :</l>
						<l n="27" num="4.3"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="27.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="27.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.4">d<seg phoneme="i" type="vs" value="1" rule="496">y</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="4.4"><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="28.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="28.3">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="28.6">h<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> !…</l>
						<l n="29" num="4.5"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="29.2">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="29.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="29.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="29.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="4.6"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="30.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="30.3">d</w>'<w n="30.4">h<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
						<l n="31" num="4.7"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="31.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="31.3">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.6">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
						<l n="32" num="4.8"><w n="32.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="32.2">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="32.3">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="32.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="33.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> ! <w n="33.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.4">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !… <w n="33.6">P<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="33.7"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="34" num="5.2"><w n="34.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="34.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="34.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="34.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="34.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>,</l>
						<l n="35" num="5.3"><w n="35.1">V<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>-<w n="35.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>, <w n="35.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="35.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.5">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="35.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="35.7">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="36" num="5.4"><w n="36.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="36.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="36.3">ci<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="36.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="36.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="36.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> !</l>
						<l n="37" num="5.5"><w n="37.1">P<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="37.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.3">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="37.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> <w n="37.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="37.6">gl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="38" num="5.6"><w n="38.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="38.2">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="38.3">nu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="38.4">j<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="38.6">cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
						<l n="39" num="5.7"><w n="39.1">P<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="39.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="39.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="39.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.7">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
						<l n="40" num="5.8"><w n="40.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>, <w n="40.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> ! <w n="40.3"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="40.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ! —</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Juillet 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>