<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ69">
					<head type="number">LXIX</head>
					<head type="main">LA RÉACTION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> ? <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="1.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="1.7">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> ?… —</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.2">B<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?…</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="425">Où</seg></w> <w n="3.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> ?… <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.4">l</w>'<w n="3.5">h<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.7">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> ?…</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>-<w n="4.4">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> ; <w n="4.5">v<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> ! <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="5.6">qu</w>'<w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="5.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="5.10">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> <w n="5.11">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>…</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="6.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="6.4">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.6">s</w>'<w n="6.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Qu</w>'<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="7.3">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.3">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">B<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="9.4">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="9.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="9.6">l</w>'<w n="9.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="9.8">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> !… <w n="9.9">j</w>'<w n="9.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>dm<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w>.</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">L<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="10.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="10.3">qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.2">S<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.6">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="12.2">d</w>'<w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.4">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="12.5">fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ! <w n="13.2">r<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="13.3">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="13.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.6">l</w>'<w n="13.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="14.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="14.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rri<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="16.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="16.4">l</w>'<w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">P<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>, <w n="17.2"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w>, <w n="17.3">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="17.4">n</w>'<w n="17.5"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="17.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">fl<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="19.2">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="19.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.4"><seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="19.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="19.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.7">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="20.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="20.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="20.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.6">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! —</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="21.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="21.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.8">m</w>'<w n="21.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="21.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.11"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.4">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space><w n="23.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="23.3">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.5">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="23.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="24.6">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="25.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="25.6">m<seg phoneme="a" type="vs" value="1" rule="339">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.7">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>,</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="26.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.4">y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="26.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="26.6">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="27" num="7.3"><space unit="char" quantity="8"></space><w n="27.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="27.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="27.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="27.5"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="27.7">Br<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">n</w>'<w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="28.4">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="28.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="28.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="28.7">h<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="28.8">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="29.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="29.4">Di<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="29.5">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> ! <w n="29.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="29.7">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="29.8">d</w>'<w n="29.9"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="29.10">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="29.11">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.12">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w></l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="30.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="30.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="30.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="30.6">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="31" num="8.3"><space unit="char" quantity="8"></space><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.2">m</w>'<w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>-<w n="32.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="32.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="32.4">pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="32.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="32.6">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="32.7">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> ! <w n="33.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> ! <w n="33.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="33.4"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="33.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="33.6">hu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> !</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">m</w>'<w n="34.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="34.6">f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="35" num="9.3"><space unit="char" quantity="8"></space><w n="35.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="35.2">n</w>'<w n="35.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="35.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="35.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.7">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> !…</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">P<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="36.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.3">B<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rk</w> <w n="36.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="36.5">Gu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="37.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="37.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> ?… <w n="37.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="37.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.7">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="37.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="37.9">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ;</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><w n="38.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.4">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="38.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="38.7">v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…—</l>
						<l n="39" num="10.3"><space unit="char" quantity="8"></space><w n="39.1">S</w>'<w n="39.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="39.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="39.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="39.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>, <w n="39.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.7">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>-<w n="39.8">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w></l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><w n="40.1">L</w>'<w n="40.2"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> !… <w n="40.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.5">l</w>'<w n="40.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="40.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>