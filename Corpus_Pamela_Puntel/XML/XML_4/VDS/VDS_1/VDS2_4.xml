<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES GRAINS DE POUDRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VDS">
					<name>
						<forename>Ali-Joseph-Augustin</forename>
						<surname>VIAL DE SABLIGNY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>604 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VDS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES GRAINS DE POUDRE</title>
						<author>ALI VIAL DE SABLIGNY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5612402j.r=VIAL%20DE%20SABLIGNY%20ALI-JOSEPH-AUGUSTIN%20LES%20GRAINS%20DE%20POUDRE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES GRAINS DE POUDRE</title>
								<author>ALI VIAL DE SABLIGNY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DESCHAMPS, PAPETIER-LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VDS2">
				<head type="main">SENTINELLES, VEILLEZ !</head>
				<head type="sub_2">Poésie récitée par Mme Eugénie PETIT</head>
				<head type="sub_2">HOMMAGE A LA GARDE NATIONALE DE PARIS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="1.2">v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="1.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">h<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rts</w> !</l>
					<l n="2" num="1.2"><w n="2.1">L</w>'<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="2.4">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>fs</w>, <w n="2.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="3.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>ts</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="3.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.7">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.8">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="3.9">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w></l>
					<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="4.2">n</w>'<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.5">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">l</w>'<w n="4.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.10">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.11">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">V<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">nu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w>,</l>
					<l n="6" num="2.2"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.5">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="7" num="2.3"><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.3">l</w>'<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.9">bru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w>,</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="8.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="8.5">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="8.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="8.8">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.9">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="8.10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="9.5">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="9.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="9.7">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.9">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ;</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.3">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="10.4">d</w>'<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.6">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ssi<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310">an</seg>ts</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="11.6">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w> <w n="11.8">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w></l>
					<l n="12" num="3.4"><w n="12.1">V<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-29">e</seg>nt</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="12.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.8">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">V<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.5">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>, <w n="13.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="13.7">l</w>'<w n="13.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t</w> <w n="13.9">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> ;</l>
					<l n="14" num="4.2"><w n="14.1">L</w>'<w n="14.2">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">n</w>'<w n="14.4"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rs</w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="14.9">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.10">f<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>rs</w> <w n="15.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="15.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="15.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="16.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.3">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="16.4">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="17.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">p<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.7">pr<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> !</l>
					<l n="18" num="5.2"><w n="18.1">V<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="18.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="18.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="18.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>ps</w> <w n="18.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.9">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="19" num="5.3"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="19.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="19.4">fr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="19.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.6">l</w>'<w n="19.7"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.8">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
					<l n="20" num="5.4"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">l</w>'<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="20.4">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="20.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="20.6">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="20.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.8">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> ! <w n="21.2">v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="21.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="21.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">h<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="21.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rts</w> !</l>
					<l n="22" num="6.2"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.3">l</w>'<w n="22.4"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="22.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="22.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="23" num="6.3"><w n="23.1">V<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="23.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="23.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="23.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.6">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="23.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.8">vi<seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w> :</l>
					<l n="24" num="6.4"><w n="24.1">D<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="24.2">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>lli<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="24.3">d</w>'<w n="24.4">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="24.5">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="24.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="24.7">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.8">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.2">t<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="25.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="25.5"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="25.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.7">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg>s</w> !</l>
					<l n="26" num="7.2"><w n="26.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="26.2">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="26.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="26.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="26.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="26.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rg<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="27" num="7.3"><w n="27.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.2">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w>, <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="27.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="27.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.7">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="27.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="27.9">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.10">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>s</w>,</l>
					<l n="28" num="7.4"><w n="28.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="28.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.6">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="29.2">v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> ! <w n="29.3">l</w>’<w n="29.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="29.5">gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w></l>
					<l n="30" num="8.2"><w n="30.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="30.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">br<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.4">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="30.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg>s</w>, <w n="30.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="30.7">l</w>'<w n="30.8">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.9"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="30.10">j<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="31" num="8.3"><w n="31.1"><seg phoneme="u" type="vs" value="1" rule="425">Où</seg></w> <w n="31.2">Str<seg phoneme="a" type="vs" value="1" rule="339">a</seg>sb<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w> <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.4">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="31.5">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="31.6">tr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>ph<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> :</l>
					<l n="32" num="8.4"><w n="32.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">si<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="32.5">v<seg phoneme="o" type="vs" value="1" rule="317">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.6">qu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="32.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w>-<w n="32.8">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">20 octobre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>