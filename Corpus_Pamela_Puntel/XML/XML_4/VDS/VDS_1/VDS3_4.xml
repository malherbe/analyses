<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES GRAINS DE POUDRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VDS">
					<name>
						<forename>Ali-Joseph-Augustin</forename>
						<surname>VIAL DE SABLIGNY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>604 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VDS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES GRAINS DE POUDRE</title>
						<author>ALI VIAL DE SABLIGNY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5612402j.r=VIAL%20DE%20SABLIGNY%20ALI-JOSEPH-AUGUSTIN%20LES%20GRAINS%20DE%20POUDRE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES GRAINS DE POUDRE</title>
								<author>ALI VIAL DE SABLIGNY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DESCHAMPS, PAPETIER-LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VDS3">
				<head type="main">AUX MESSAGERS DE LA PATRIE</head>
				<head type="sub_2">Poésie récitée par Mme Eugénie PETIT</head>
				<head type="sub_2">HOMMAGE A M. LE BARON LARREY</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">V<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="1.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="1.4">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="1.5">m<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="1.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="1.7">Ci<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="1.8">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="424">Ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="2.2"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="2.3">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.3">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="3.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">l</w>'<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>z<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="5.2">n</w>'<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>, <w n="5.5">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="5.6">qu</w>'<w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="5.9">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> !</l>
					<l n="6" num="2.2"><space unit="char" quantity="12"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="7.2">n</w>'<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">qu</w>'<w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="7.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> ! <w n="7.7">V<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.10">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="8" num="2.4"><space unit="char" quantity="12"></space><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="9.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>, <w n="9.5">lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> ! <w n="9.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="9.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>-<w n="9.10">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>,</l>
					<l n="10" num="3.2"><space unit="char" quantity="12"></space><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="10.3">v<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="10.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="10.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
					<l n="11" num="3.3"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.2">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="11.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="11.7">n</w>'<w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>-<w n="11.9">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="11.10">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>,</l>
					<l n="12" num="3.4"><space unit="char" quantity="12"></space><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="12.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>d<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.8">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>,</l>
					<l n="14" num="4.2"><space unit="char" quantity="12"></space><w n="14.1">P<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="14.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="15" num="4.3"><w n="15.1">T<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="15.2">qu</w>'<w n="15.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.4">n</w>'<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="15.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="15.7">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="15.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="15.9">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.10">si<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>,</l>
					<l n="16" num="4.4"><space unit="char" quantity="12"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="16.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">l</w>'<w n="16.4"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343">ue</seg>il</w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="17.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="17.3">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="17.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="17.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="17.6">v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="17.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rri<seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w></l>
					<l n="18" num="5.2"><space unit="char" quantity="12"></space><w n="18.1">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="18.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rts</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="19.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="19.4">d</w>'<w n="19.5"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.6">l<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w>,</l>
					<l n="20" num="5.4"><space unit="char" quantity="12"></space><w n="20.1">D<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="20.2">fru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.5">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="21.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="21.4">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="21.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="21.7">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="21.8">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ds</w> !</l>
					<l n="22" num="6.2"><space unit="char" quantity="12"></space><w n="22.1">V<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="22.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="22.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="22.4">d</w>'<w n="22.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="23.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="23.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="23.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.6">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="23.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.9">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w></l>
					<l n="24" num="6.4"><space unit="char" quantity="12"></space><w n="24.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="25.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="25.4">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="25.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="25.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="25.7">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="25.8">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="25.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.10">s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w></l>
					<l n="26" num="7.2"><space unit="char" quantity="12"></space><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="26.2">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="26.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="26.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.5">n<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="27" num="7.3"><w n="27.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.3">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>, <w n="27.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="27.5">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="27.6">l</w>'<w n="27.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> <w n="27.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.9">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.10">v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w>,</l>
					<l n="28" num="7.4"><space unit="char" quantity="12"></space><w n="28.1">Gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.3">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">C</w>'<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="29.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="29.4">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="29.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="29.6">qu</w>'<w n="29.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="29.8">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="29.9">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="29.10">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.11"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rvi<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
					<l n="30" num="8.2"><space unit="char" quantity="12"></space><w n="30.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="31" num="8.3"><w n="31.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.2">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="31.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="31.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="31.5">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rs</w> <w n="31.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="31.7">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="31.8">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
					<l n="32" num="8.4"><space unit="char" quantity="12"></space><w n="32.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="32.3">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="33.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="33.3">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="33.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="33.5">l</w>'<w n="33.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="33.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rts</w>,</l>
					<l n="34" num="9.2"><space unit="char" quantity="12"></space><w n="34.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rm<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="34.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="34.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="35" num="9.3"><w n="35.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="35.2">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="35.3">t</w>-<w n="35.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="35.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="35.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="35.7">p<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>ts</w> <w n="35.8">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="35.9">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="35.10"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w>,</l>
					<l n="36" num="9.4"><space unit="char" quantity="12"></space><w n="36.1">S<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="36.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="37.2">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="37.3">v<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="37.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="37.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="37.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w></l>
					<l n="38" num="10.2"><space unit="char" quantity="12"></space><w n="38.1">V<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.2">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="38.3">d</w>'<w n="38.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="39" num="10.3"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="39.2">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w>, <w n="39.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="39.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="39.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="39.6">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.7">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="39.8">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>,</l>
					<l n="40" num="10.4"><space unit="char" quantity="12"></space><w n="40.1">Gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.2">ph<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="41.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="41.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, <w n="41.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="41.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="41.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
					<l n="42" num="11.2"><space unit="char" quantity="12"></space><w n="42.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="42.2">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="42.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="42.4">T<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="43" num="11.3"><w n="43.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> , <w n="43.2">R<seg phoneme="ɔ" type="vs" value="1" rule="440">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="43.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="43.4">l</w>'<w n="43.5">H<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="43.7">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="43.9">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="43.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
					<l n="44" num="11.4"><space unit="char" quantity="12"></space><w n="44.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="44.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="44.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.4"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="45.2">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="45.3">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> ! <w n="45.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.5">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="45.6">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="45.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="45.8">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> !</l>
					<l n="46" num="12.2"><space unit="char" quantity="12"></space><w n="46.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="46.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="46.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="46.4">l</w>'<w n="46.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="46.6">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="47" num="12.3"><w n="47.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="47.2">l</w>'<w n="47.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> <w n="47.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="47.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> <w n="47.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="47.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="47.8">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="47.9">l</w>'<w n="47.10"><seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w></l>
					<l n="48" num="12.4"><space unit="char" quantity="12"></space><w n="48.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="48.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="48.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="48.4">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 4 décembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>