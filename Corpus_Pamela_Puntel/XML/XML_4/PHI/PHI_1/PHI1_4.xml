<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">VIVE GAMBETTA !</title>
				<title type="medium">Édition électronique</title>
				<author key="PHI">
					<name>
						<forename>Gustave</forename>
						<surname>PHILIPPON</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">PHI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>VIVE GAMBETTA !</title>
						<author>GUSTAVE PHILIPPON</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LACROIX VERBOECKHOVEN ET CIE</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="PHI1">
				<head type="main">DÉDICACE ET ENVOI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg></w> <w n="1.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>-<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="2.2"><seg phoneme="y" type="vs" value="1" rule="250">eû</seg>t</w>, <w n="2.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="2.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="2.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="2.8">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">Â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="3.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>tr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.6">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w></l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="339">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="4.8">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.9">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w>,</l>
					<l n="5" num="1.5"><w n="5.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>, <w n="5.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.3">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.7">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="1.7"><w n="7.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.4">M<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lg<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w>.</l>
					<l n="8" num="1.8"><w n="8.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="8.5">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.7">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="8.8">d</w>'<w n="8.9"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343">ue</seg>il</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">qu</w>'<w n="9.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="9.5">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>, <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.8">h<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="10.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="10.3">d</w>'<w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.5">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="10.6">qu</w>'<w n="10.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="10.8">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.11">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="11" num="1.11"><w n="11.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="11.3">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="11.4">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="11.5">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="11.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="11.8">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w>, <w n="11.10">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.11">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
					<l n="12" num="1.12"><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="12.2">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="12.7">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="12.8">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="12.10">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>.</l>
					<l n="13" num="1.13"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.2">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w>, <w n="13.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.7">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="14" num="1.14"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.4">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>s</w> <w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="14.7">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ff<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="14.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.9"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="15" num="1.15"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="15.3">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="15.5">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="15.6">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="15.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.9">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w>,</l>
					<l n="16" num="1.16"><w n="16.1">S<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="16.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.8">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="17.2">j<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="17.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.4">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w>, <w n="17.5">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="17.6">d</w>'<w n="17.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="17.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="17.9"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="1.18"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> : <w n="18.2">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">G<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="e" type="vs" value="1" rule="352">e</seg>tt<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! <w n="18.4">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.6">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="19" num="1.19"><w n="19.1">Pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="19.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="19.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>, <w n="19.6">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="19.7">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.8"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ls<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>,</l>
					<l n="20" num="1.20"><w n="20.1">V<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="20.2">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="20.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="20.4">d</w>'<w n="20.5"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="20.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.7"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rph<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w> <w n="20.8">pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>,</l>
					<l n="21" num="1.21"><w n="21.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>t<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="21.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.5">Vi<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="21.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="21.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="22" num="2.1"><w n="22.1">Fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="22.2">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w>, <w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="22.5">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="377">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<closer>
					<signed>GUSTAVE PHILIPPON.</signed>
				</closer>
			</div></body></text></TEI>