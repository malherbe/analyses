<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’EMPIRE</head><div type="poem" key="FRK11">
					<head type="main">JACQUES BONHOMME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">E</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">J<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> ! <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">B<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="2.2">qu</w>’<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.4">fou<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.8">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> !</l>
						<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.8">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">n<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="4.2">l</w>’<w n="4.3">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>… <w n="4.4">C</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1">B<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> ! <w n="5.2">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> ! <w n="5.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="5.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="6.2">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">dr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">n<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ls</w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>p<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.3">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.5">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.3">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.6">l<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="9.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">N<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.3">n</w>’<w n="11.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="12.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="12.6">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.8">gl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« <w n="13.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="13.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">j<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.6">P<seg phoneme="a" type="vs" value="1" rule="339">â</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w>,</l>
						<l n="15" num="4.3"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">J<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cqu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
						<l n="16" num="4.4"><w n="16.1">V<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="16.3">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="16.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.5">r<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>s</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« <w n="17.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="17.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> ! <w n="17.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">t<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="18" num="5.2"><w n="18.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>’<w n="18.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="18.3">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="18.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="18.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="18.6">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w>,</l>
						<l n="19" num="5.3"><w n="19.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.5">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.6">N<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="20.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« <w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="21.2">qu</w>’<w n="21.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="21.4">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="21.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="21.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.8">fr<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="22" num="6.2"><w n="22.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="22.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="22.5">pi<seg phoneme="e" type="vs" value="1" rule="240">e</seg>d</w> <w n="22.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="22.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w>,</l>
						<l n="23" num="6.3"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.2">fru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ts</w> <w n="23.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="23.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="6.4"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="24.2">J<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="24.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="24.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="24.6">c<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> !»</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="25.2">P<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="7.2"><w n="26.1">S<seg phoneme="u" type="vs" value="1" rule="424">oû</seg>ls</w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.4">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="26.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="26.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="26.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>,</l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="434">O</seg>pp<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t</w> <w n="27.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="28" num="7.4"><w n="28.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.3">l<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ps</w> <w n="28.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>…</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="29.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> : <w n="29.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="29.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="29.7"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>mm<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="30" num="8.2"><w n="30.1">Qu</w>’<w n="30.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="30.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="30.4">l</w>’<w n="30.5"><seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="30.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="30.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.8">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
						<l n="31" num="8.3"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="31.2">qu</w>’<w n="31.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="31.4">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="31.5">l</w>’<w n="31.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="31.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="8.4"><w n="32.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="32.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="33.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.3">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="33.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="33.5">vi<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="33.6">M<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="34" num="9.2"><w n="34.1">R<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="34.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="34.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>sc<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
						<l n="35" num="9.3"><w n="35.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="35.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="35.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="35.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.6">n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="36" num="9.4"><w n="36.1">Tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="36.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="36.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.4">cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="37.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="37.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="37.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="37.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.6">B<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="38" num="10.2"><w n="38.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="38.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ci<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="38.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="38.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="38.5">h<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>,</l>
						<l n="39" num="10.3"><w n="39.1">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="39.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="39.3">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> <w n="39.4">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.6">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="40" num="10.4"><w n="40.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="40.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="40.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> !</l>
					</lg>
				</div></body></text></TEI>