<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’EMPIRE</head><div type="poem" key="FRK10">
					<head type="main">LE SPECTRE DE BAUDIN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">UN</seg></w> <w n="1.2">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> — <w n="1.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">P<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="1.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.8">l</w>’<w n="1.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="2.4">s</w>’<w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɥi" type="vs" value="1" rule="461">u</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.7">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="2.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>,—</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="3.2">l</w>’<w n="3.3">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.4"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="3.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">pi<seg phoneme="e" type="vs" value="1" rule="240">e</seg>d</w> <w n="3.8">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="3.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.10">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.11"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="3.12">cr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">T<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="4.4">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.7">vi<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.8">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rgs</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>-<w n="5.2">hu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.7">d</w>’<w n="5.8"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.9">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="5.10">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.11">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>-<w n="6.2">hu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="6.6">gr<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.10">d</w>’<w n="6.11">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="492">y</seg>r</w> <w n="7.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="7.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">dr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>… <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="7.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">P<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="8.6">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> :</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">— <w n="9.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="9.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.9">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">l</w>’<w n="10.3">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.8">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="11.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="11.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>h</w> ! <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="11.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.9">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="12" num="3.4"><w n="12.1">S<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="12.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="12.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.7">P<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>st<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="13.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="13.6">g<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="13.7">d</w>’<w n="13.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="14" num="4.2"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">Dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="14.4">n</w>’<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="14.7">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="14.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.9">qu</w>’<w n="14.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.11">l</w>’<w n="14.12"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="15.2">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.5">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="15.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="15.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="15.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="16" num="4.4"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="16.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="16.4">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="16.6">t<seg phoneme="i" type="vs" value="1" rule="492">y</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> : « <w n="16.7">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.8">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> !»</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ou<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="17.2">l</w>’<w n="17.3">h<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="17.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="17.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="17.8"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.9">f<seg phoneme="o" type="vs" value="1" rule="433">o</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.10"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="18" num="5.2"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="18.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="18.6">r<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="18.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.9">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> :</l>
						<l n="19" num="5.3"><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="19.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.5">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="19.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.8">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rtr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.9">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">S</w>’<w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="20.4">g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.5"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w> <w n="20.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="20.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.8">Dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="20.9"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">C</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="21.3">Lu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="21.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="21.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="21.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="21.7">s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w>, <w n="21.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="21.9">c</w>’<w n="21.10"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="21.11">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.12">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="21.13">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="21.14">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="22" num="6.2"><w n="22.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.2">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="22.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="22.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.7">tr<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.8"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="22.9">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> :</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="23.4">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="23.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="23.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="23.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.9">nu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="23.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="24" num="6.4"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">tr<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="24.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="24.5">fl<seg phoneme="o" type="vs" value="1" rule="437">o</seg>t</w> <w n="24.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="24.8">s</w>’<w n="24.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>gl<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.3">P<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="25.5">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="25.6">l<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>, <w n="25.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="25.8">C<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="25.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="25.10">g<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="26" num="7.2"><w n="26.1">R<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="26.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.4">si<seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>, — <w n="26.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>, <w n="26.6">j<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, — <w n="26.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w></l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">s<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="27.4"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="27.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="27.6">l</w>’<w n="27.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.9">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="7.4"><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="28.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="28.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="28.4">Sp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.6">B<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> !</l>
					</lg>
				</div></body></text></TEI>