<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">AMERTUMES ET PAIN NOIR</title>
				<title type="sub">SIÈGE DE PARIS 1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="FRS">
					<name>
						<forename>Émile</forename>
						<surname>FRANÇOIS</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>487 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
						<author>ÉMILE FRANÇOIS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61012844.r=FRAN%C3%87OIS%20E.%2C%20AMERTUMES%20ET%20PAIN%20NOIR.?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
								<author>ÉMILE FRANÇOIS</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE A. LACROIX, VERBOECKHOVEN ET Cie</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-24" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRS7">
				<head type="main">CAUSES</head>
				<div type="section" n="1">
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="1.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="1.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="1.5">n<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?…</l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>fs</w> : <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">Em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.6">R<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">L</w>'<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>, <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="3.7">t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.3">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>dg<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> ;</l>
						<l n="5" num="1.5"><w n="5.1">L</w>'<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="5.3"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="5.4">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="5.6">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.7">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.3">m<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.6">v<seg phoneme="ɛ" type="vs" value="1" rule="354">e</seg>x<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">Tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="7.2">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w>, <w n="7.4">qu</w>'<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.6">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.7">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="354">e</seg>x<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">D<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="9.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>fs</w>, <w n="9.3">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="9.4">t<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !…</l>
						<l n="10" num="2.2"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.3">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="10.4">st<seg phoneme="y" type="vs" value="1" rule="449">u</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>,</l>
						<l n="11" num="2.3"><w n="11.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="11.3">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="12" num="2.4"><w n="12.1">L<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.3">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> !</l>
						<l n="13" num="2.5"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="13.2">r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="13.3">qu</w>'<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>-<w n="13.5">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="13.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="13.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.9"><seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
						<l n="14" num="2.6"><w n="14.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="14.2">c<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="14.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="14.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="14.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="14.7">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.8">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ts</w> ?…</l>
						<l n="15" num="2.7"><w n="15.1">P<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> ! <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="15.6">b<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="16" num="2.8"><w n="16.1">D</w>'<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="16.4">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="16.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.6">r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> ?…</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">L</w>'<w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="354">e</seg>x<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>… <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="17.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="18" num="3.2"><w n="18.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="18.2">l</w>'<w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="18.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="18.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
						<l n="19" num="3.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.4">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.5">l</w>'<w n="19.6">h<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="19.7">fi<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="3.4"><w n="20.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="20.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="20.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="wa" type="vs" value="1" rule="439">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
						<l n="21" num="3.5"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="21.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ; <w n="21.4">pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="21.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="21.6">bru<seg phoneme="i" type="vs" value="1" rule="490">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="3.6"><w n="22.1">S<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="22.2">l</w>'<w n="22.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="22.5">l</w>'<w n="22.6"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w>.</l>
						<l n="23" num="3.7"><w n="23.1">L</w>'<w n="23.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="23.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> ; <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="23.5">l</w>'<w n="23.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">Em</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="3.8"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="24.2">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="24.3">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="24.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">f<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="25.2">c</w>'<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="25.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="25.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="25.6">l</w>'<w n="25.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="25.8">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="4.2"><w n="26.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ?… <w n="26.3">H<seg phoneme="i" type="vs" value="1" rule="492">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="26.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>st<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
						<l n="27" num="4.3"><w n="27.1">Vi<seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> <w n="27.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="27.3">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.4">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="28" num="4.4"><w n="28.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="28.2">l</w>'<w n="28.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="28.6">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w></l>
						<l n="29" num="4.5"><w n="29.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="29.2">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="29.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>pt<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
						<l n="30" num="4.6"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="30.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="30.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="30.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="30.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ts</w>,</l>
						<l n="31" num="4.7"><w n="31.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="31.2">l</w>'<w n="31.3"><seg phoneme="ø" type="vs" value="1" rule="404">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="31.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="32" num="4.8"><w n="32.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>-<w n="32.2">l<seg phoneme="ə" type="ee" value="0" rule="e-8">e</seg></w>, <w n="32.3"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="32.4">r<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="32.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.6">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="32.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="32.9">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> ?…</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">9 décembre.</date>
						</dateline>
					</closer>
				</div>
				<div type="section" n="2">
					<lg n="1">
						<l n="33" num="1.1"><w n="33.1"><seg phoneme="ø" type="vs" value="1" rule="404">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="33.2"><seg phoneme="ø" type="vs" value="1" rule="404">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.3">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="33.5">l<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="34" num="1.2"><w n="34.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="34.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="34.3">d</w>'<w n="34.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="34.5"><seg phoneme="œ" type="vs" value="1" rule="285">œ</seg>il</w> <w n="34.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w></l>
						<l n="35" num="1.3"><w n="35.1">L</w>'<w n="35.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="35.4">su<seg phoneme="i" type="vs" value="1" rule="490">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="35.6">t<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="36" num="1.4"><w n="36.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.2">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="36.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="36.4">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="36.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> !…</l>
						<l n="37" num="1.5"><w n="37.1">C</w>'<w n="37.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="37.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="37.4">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="37.5">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="37.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="37.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="38" num="1.6"><w n="38.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="38.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="38.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.4">p<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="38.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="38.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="38.7">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l>
						<l n="39" num="1.7"><w n="39.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="39.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.3">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> ?… <w n="39.4">C</w>'<w n="39.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="39.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="39.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="40" num="1.8"><w n="40.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="40.2">D<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="40.3">B<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w>. <w n="40.4">Pu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w>, <w n="40.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="40.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="40.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> !…</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">12 décembre.</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>