<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ86" modus="sm" lm_max="8" metProfile="8">
				<head type="number">LXXXVI</head>
					<head type="main">LETTRE DE GRETCHEN</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="1">e</seg>r</w> <w n="1.2" punct="vg:2">Schw<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>rtz</w>, <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="1.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t</w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="1.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="7">e</seg>c</w> <w n="2.5" punct="pt:8">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>s</w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">m<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3" punct="vg:5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="5" punct="vg">en</seg></w>, <w n="3.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>d</w> <w n="3.5">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="3.6" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="4.2">c<seg phoneme="œ" type="vs" value="1" rule="248" place="2">œu</seg>r</w> <w n="4.3">n</w>'<w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="4.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="4.7" punct="pt:8">j<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>x</w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="vg">a</seg>s</w>, <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">t</w>'<w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="5.8" punct="vg:8">pr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="6.4" punct="vg:5">m<seg phoneme="wa" type="vs" value="1" rule="422" place="5" punct="vg">oi</seg></w>, <w n="6.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="6">e</seg>r</w> <w n="6.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>r</w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="7.3">p<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="7.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.5">j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r</w></l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4" punct="pt:8">j<seg phoneme="wa" type="vs" value="1" rule="" place="6">oa</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="9" num="1.9" lm="8" met="8"><w n="9.1">D</w>'<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</w> <w n="9.6" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg></w>.</l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">d</w>'<w n="10.3" punct="vg:3"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="3" punct="vg">o</seg>r</w>, <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="10.5">br<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="11" num="1.11" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="11.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>t</w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="11.4">b<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="11.5">d</w>'<w n="11.6" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></w> !…</l>
						<l n="12" num="1.12" lm="8" met="8"><w n="12.1">T<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="12.2" punct="pt:3">Gr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tch<seg phoneme="ɛ" type="vs" value="1" rule="24" place="3" punct="pt ti">e</seg>n</w>. — <w n="12.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pe">A</seg>h</w> ! <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">Janvier 1871</date>
						</dateline>
					</closer>
				</div></body></text></TEI>