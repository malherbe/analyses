<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ47" modus="cp" lm_max="10" metProfile="7, 5+5">
					<head type="number">XLVII</head>
					<head type="main">ON SE BAT</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="1.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3" punct="pe:3">b<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe ps">a</seg>t</w> !… <w n="1.4">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="1.5">m<seg phoneme="o" type="vs" value="1" rule="437" place="5" caesura="1">o</seg>t</w><caesura></caesura> <w n="1.6">s<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="8">en</seg>t</w> <w n="1.7">r<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="2" num="1.2" lm="7" met="7"><space unit="char" quantity="6"></space><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="2.3">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="5">en</seg>t</w> <w n="2.4" punct="pv:7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" punct="pv">ain</seg></w> ;</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="3.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" mp="P">an</seg>s</w> <w n="3.4">l</w>'<w n="3.5" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.8">c<seg phoneme="œ" type="vs" value="1" rule="248" place="8">œu</seg>r</w> <w n="3.9">fr<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="4" num="1.4" lm="7" met="7"><space unit="char" quantity="6"></space><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="4.5" punct="pe:7">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" punct="pe">in</seg></w> !</l>
						<l n="5" num="1.5" lm="10" met="5+5"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="5.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3" punct="pe:3">b<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe ps">a</seg>t</w> !… <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4">où</seg></w> <w n="5.5" punct="pi:5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" punct="pi" caesura="1">on</seg>c</w> ?<caesura></caesura> <w n="5.6">n<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>l</w> <w n="5.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.8">p<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>t</w> <w n="5.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.10" punct="pv:10">d<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="6" num="1.6" lm="7" met="7"><space unit="char" quantity="6"></space><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>g</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w>-<w n="6.4">b<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="6.5">v<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="6.6" punct="vg:7">c<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="vg">er</seg></w>,</l>
						<l n="7" num="1.7" lm="10" met="5+5"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="Lc">à</seg></w>-<w n="7.2" punct="vg:2">b<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>s</w>, <w n="7.3">v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="3" mp="P">e</seg>rs</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="C">a</seg></w> <w n="7.5" punct="pe:5">L<seg phoneme="wa" type="vs" value="1" rule="419" place="5" punct="pe ps" caesura="1">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !<caesura></caesura>… <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">A</seg></w> <w n="7.7">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w>»<w n="7.9" punct="pv:10">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="8" num="1.8" lm="7" met="7"><space unit="char" quantity="6"></space><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="4">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5" punct="pe:7">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="pe ps">er</seg></w> !…</l>
						<l n="9" num="1.9" lm="10" met="5+5"><w n="9.1">L</w>'<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="9.5" punct="vg:5">m<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="vg" caesura="1">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="9.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="466" place="6" mp="M">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.7"><seg phoneme="i" type="vs" value="1" rule="466" place="9" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="10" num="1.10" lm="7" met="7"><space unit="char" quantity="6"></space><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="10.2">bru<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>t</w> <w n="10.3">d</w>'<w n="10.4"><seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="10.7">l</w>'<w n="10.8" punct="pv:7"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="7" punct="pv">ai</seg>r</w> ;</l>
						<l n="11" num="1.11" lm="10" met="5+5"><w n="11.1">P<seg phoneme="ø" type="vs" value="1" rule="397" place="1" mp="Lc">eu</seg>t</w>-<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="5" caesura="1">à</seg></w><caesura></caesura> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="11.5">l<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.6" punct="pv:10">c<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="12" num="1.12" lm="7" met="7"><space unit="char" quantity="6"></space><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="12.2">f<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">su<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="12.4">l</w>'<w n="12.5" punct="pe:7"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7" punct="pe">ai</seg>r</w> !</l>
						<l n="13" num="1.13" lm="10" met="5+5"><w n="13.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem/mp">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w>-<w n="13.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="Lc">ou</seg>rd</w>'<w n="13.4" punct="pi:5">hu<seg phoneme="i" type="vs" value="1" rule="490" place="5" punct="pi" caesura="1">i</seg></w> ?<caesura></caesura> <w n="13.5">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="13.6">fr<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M/mp">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem/mp">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="Lp">a</seg></w>-<w n="13.7">t</w>-<w n="13.8" punct="pi:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></w> ?</l>
						<l n="14" num="1.14" lm="7" met="7"><space unit="char" quantity="6"></space><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="14.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>fs</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="14.5" punct="pt:7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="7" punct="pt">oi</seg>r</w>.</l>
						<l n="15" num="1.15" lm="10" met="5+5"><w n="15.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" punct="pe">O</seg>h</w> ! <w n="15.2">l</w>'<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.4" punct="pe:5">s<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="pe" caesura="1">i</seg></w> !<caesura></caesura> <w n="15.5">l</w>'<w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="M">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="15.7" punct="pe:10">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
						<l n="16" num="1.16" lm="7" met="7"><space unit="char" quantity="6"></space><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="16.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="5">en</seg></w> <w n="16.5" punct="pe:7">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="7" punct="pe ps">oi</seg>r</w> !…</l>
						<l n="17" num="1.17" lm="10" met="5+5"><w n="17.1">N<seg phoneme="o" type="vs" value="1" rule="437" place="1" mp="C">o</seg>s</w> <w n="17.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg>s</w> <w n="17.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="5" caesura="1">e</seg>rs</w><caesura></caesura> <w n="17.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="M">an</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg></w> <w n="17.6">l</w>'<w n="17.7"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="18" num="1.18" lm="7" met="7"><space unit="char" quantity="6"></space><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="18.2">m<seg phoneme="ɛ" type="vs" value="1" rule="381" place="2">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>rs</w> <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="18.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s</w> <w n="18.6" punct="pv:7">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" punct="pv">o</seg>rts</w> ;</l>
						<l n="19" num="1.19" lm="10" met="5+5"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="19.2" punct="vg:3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="19.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="19.4">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5" caesura="1">ai</seg>t</w><caesura></caesura> <w n="19.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="19.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="19.7">t<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="19.8">fl<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="20" num="1.20" lm="7" met="7"><space unit="char" quantity="6"></space><w n="20.1">N</w>'<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="20.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="20.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>c</w> <w n="20.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="20.7" punct="pi:7">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" punct="pi ps">o</seg>rts</w> ?…</l>
						<l n="21" num="1.21" lm="10" met="5+5">— <w n="21.1">N<seg phoneme="o" type="vs" value="1" rule="437" place="1" mp="C">o</seg>s</w> <w n="21.2">j<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="21.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="5" caesura="1">a</seg>ts</w><caesura></caesura> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="P">à</seg></w> <w n="21.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="22" num="1.22" lm="7" met="7"><space unit="char" quantity="6"></space><w n="22.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">p<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w>-<w n="22.3"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ls</w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="22.5" punct="vg:7">c<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="vg">er</seg></w>,</l>
						<l n="23" num="1.23" lm="10" met="5+5"><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="23.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="23.3">t<seg phoneme="y" type="vs" value="1" rule="444" place="4" mp="M">û</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="5" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="P">à</seg></w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="23.6" punct="vg:10">m<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="24" num="1.24" lm="7" met="7"><space unit="char" quantity="6"></space><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Où</seg></w> <w n="24.2">l</w>'<w n="24.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="24.4">m<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>rt</w> <w n="24.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="24.6">s</w>'<w n="24.7" punct="pi:7"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="7" punct="pi ps">er</seg></w> ?…</l>
						<l n="25" num="1.25" lm="10" met="5+5"><w n="25.1">C</w>'<w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="25.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="25.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="25.5" punct="pe:5">cr<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="pe ps" caesura="1">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !<caesura></caesura>… <w n="25.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="C">On</seg></w> <w n="25.7" punct="pv:7">d<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="pv">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="25.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="25.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="25.10" punct="pv:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="26" num="1.26" lm="7" met="7"><space unit="char" quantity="6"></space><w n="26.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="26.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t</w> <w n="26.4">b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="26.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="26.6" punct="pv:7">si<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7" punct="pv">en</seg>s</w> ;</l>
						<l n="27" num="1.27" lm="10" met="5+5"><w n="27.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="27.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r</w> <w n="27.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="27.4" punct="vg:5">d<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="vg" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="27.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="C">on</seg></w> <w n="27.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="27.7" punct="ps:8">p<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="ps">eu</seg>r</w>… <w n="27.8"><seg phoneme="o" type="vs" value="1" rule="414" place="9">ô</seg></w> <w n="27.9" punct="pe:10">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
						<l n="28" num="1.28" lm="7" met="7"><space unit="char" quantity="6"></space><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>r</w> <w n="28.2"><seg phoneme="y" type="vs" value="1" rule="390" place="3">eu</seg></w> <w n="28.3">p<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="28.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="28.5" punct="pe:7">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7" punct="pe ps">en</seg>s</w> !…</l>
						<l n="29" num="1.29" lm="10" met="5+5"><w n="29.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="29.3">bru<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>t</w> <w n="29.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Lc">e</seg></w>-<w n="29.5" punct="pt:5">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5" punct="pt ti" caesura="1">oi</seg>x</w>.<caesura></caesura> — <w n="29.6"><seg phoneme="e" type="vs" value="1" rule="132" place="6">E</seg>h</w> <w n="29.7" punct="pi:7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="7" punct="pi ps ti">en</seg></w> ?…— <w n="29.8">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8">en</seg></w> <w n="29.9" punct="pe:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe ps" mp="F">e</seg></w> !…</l>
						<l n="30" num="1.30" lm="7" met="7"><space unit="char" quantity="6"></space><w n="30.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="30.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg></w> <w n="30.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="30.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="30.5" punct="pv:7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" punct="pv">ain</seg></w> ;</l>
						<l n="31" num="1.31" lm="10" met="5+5"><w n="31.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="31.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="31.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="31.4" punct="vg:5">br<seg phoneme="y" type="vs" value="1" rule="444" place="5" punct="vg" caesura="1">û</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="31.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="31.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="31.7" punct="pe:10">d<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
						<l n="32" num="1.32" lm="7" met="7"><space unit="char" quantity="6"></space><w n="32.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="32.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="32.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="32.5" punct="pe:7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" punct="pe">ain</seg></w> !</l>
						<l n="33" num="1.33" lm="10" met="5+5"><w n="33.1">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg></w> <w n="33.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M/mp">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" mp="Lp">ai</seg>t</w>-<w n="33.3"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l</w> <w n="33.4" punct="vg:5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" punct="vg" caesura="1">on</seg>c</w>,<caesura></caesura> <w n="33.5">h<seg phoneme="o" type="vs" value="1" rule="434" place="6" mp="M">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="33.6" punct="pe:10">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
						<l n="34" num="1.34" lm="7" met="7"><space unit="char" quantity="6"></space><w n="34.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="34.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="34.4" punct="pi:7">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pi ps">â</seg>t</w> ?…</l>
						<l n="35" num="1.35" lm="10" met="5+5"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="35.2" punct="pe:5">nu<seg phoneme="i" type="vs" value="1" rule="490" place="5" punct="pe" caesura="1">i</seg>t</w> !<caesura></caesura> <w n="35.3">nu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg>t</w> <w n="35.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.5"><seg phoneme="e" type="vs" value="1" rule="188" place="8">e</seg>t</w> <w n="35.6" punct="pe:10">gl<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe ps" mp="F">e</seg></w> !…</l>
						<l n="36" num="1.36" lm="7" met="7"><space unit="char" quantity="6"></space><w n="36.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="36.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2">en</seg>t</w> <w n="36.3" punct="pe:4">m<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="pe ps">i</seg>t</w> !… <w n="36.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">On</seg></w> <w n="36.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="36.6" punct="pe:7">b<seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pe ps">a</seg>t</w> !…</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">10 novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>