<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ15" modus="cp" lm_max="12" metProfile="8, 6+6">
					<head type="number">XV</head>
					<head type="main">LE NUAGE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="280" place="1" punct="pe">oi</seg></w> ! <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">n</w>'<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.8">v<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="1.9">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10" mp="C">e</seg>s</w> <w n="1.10" punct="pi:12">v<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg>s</w> ?</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="2.2" punct="vg:3">br<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>ds</w>, <w n="2.3">qu</w>'<w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="2.5">r<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="2.6">d</w>'<w n="2.7"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r</w> <w n="2.8">p<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="P">ou</seg>r</w> <w n="2.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="169" place="10" mp="M">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="vg">i</seg>s</w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">F<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="3.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="4" mp="C">o</seg>s</w> <w n="3.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>ts</w><caesura></caesura> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="3.6">b<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>cs</w> <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>t<seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="4.2">cr<seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3">qu</w>'<w n="4.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ls</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="4.6" punct="pe:8">c<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>mm<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>s</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>ls</w> <w n="5.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="2" mp="M">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="5.5">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="8" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="9" mp="M">y</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10">an</seg>s</w> <w n="5.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="11" mp="P">an</seg>s</w> <w n="5.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="6.2" punct="vg:3">f<seg phoneme="a" type="vs" value="1" rule="192" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="6.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg>ts</w>,<caesura></caesura> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="6.6">vi<seg phoneme="e" type="vs" value="1" rule="382" place="8" mp="M">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>rds</w> <w n="6.7" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10" mp="M">en</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg>s</w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">cr<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">î</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="P">à</seg></w> <w n="7.4">l</w>'<w n="7.5" punct="vg:6"><seg phoneme="ø" type="vs" value="1" rule="404" place="5" mp="M">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6" punct="vg" caesura="1">o</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="7.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.8">n<seg phoneme="wa" type="vs" value="1" rule="439" place="9" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="7.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="7.10" punct="vg:12">l<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="8.3">t<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r</w> <w n="8.5">l<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</w> <w n="8.6" punct="pe:8">bl<seg phoneme="e" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pe">é</seg>s</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="pe:3">M<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="9.2" punct="vg:4">T<seg phoneme="wa" type="vs" value="1" rule="422" place="4" punct="vg">oi</seg></w>, <w n="9.3" punct="vg:6">B<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg" caesura="1">a</seg>rk</w>,<caesura></caesura> <w n="9.4">t<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="C">u</seg></w> <w n="9.5" punct="pe:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="8" punct="pe">en</seg>s</w> ! <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="188" place="9">e</seg>t</w> <w n="9.7">t<seg phoneme="wa" type="vs" value="1" rule="422" place="10">oi</seg></w> <w n="9.8" punct="vg:12">Gu<seg phoneme="i" type="vs" value="1" rule="484" place="11" mp="M">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317" place="12">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="10.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="2">en</seg>s</w> <w n="10.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.4" punct="pe:6">B<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe" caesura="1">a</seg>rk</w> !<caesura></caesura> <w n="10.5">v<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="10.6">j<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>rn<seg phoneme="o" type="vs" value="1" rule="317" place="9">au</seg>x</w> <w n="10.7" punct="vg:12">fr<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg>s</w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="11.2" punct="vg:3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>ts</w>, <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="11.4" punct="vg:6">s<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" punct="vg" caesura="1">e</seg>ts</w>,<caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="11.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>t</w> <w n="11.7">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="11.8">r<seg phoneme="wa" type="vs" value="1" rule="439" place="11" mp="M">o</seg>y<seg phoneme="o" type="vs" value="1" rule="317" place="12">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1" punct="pe:2">M<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="1">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="pe ps">e</seg>nt</w> !… <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="12.3" punct="pe:5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="5" punct="pe">ez</seg></w> ! <w n="12.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="12.5" punct="pe:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="7">en</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pe pe">ez</seg></w> ! !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="13.2">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="2" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" mp="M">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.5">s<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="9">e</seg>il</w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="13.7">s<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="13.8" punct="pv:12">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="14.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="14.3">l</w>'<w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="14.5">s<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>s</w> <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="14.7">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>l</w> <w n="14.8" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="12" punct="pv">ai</seg>s</w> ;</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="15.3" punct="pe:3">c<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe ps">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4" mp="C">un</seg></w> <w n="15.5">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="5" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="15.6">d<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="15.7">l</w>'<w n="15.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="15.9" punct="vg:12">n<seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">s<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="4">e</seg>il</w> <w n="16.4">s</w>'<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="16.7" punct="pe:8">p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pe">ai</seg>x</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Août 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>