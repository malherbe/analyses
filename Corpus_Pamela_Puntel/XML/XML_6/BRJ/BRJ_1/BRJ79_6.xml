<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ79" modus="cm" lm_max="12" metProfile="6+6">
					<head type="number">LXXIX</head>
					<head type="main">COUR ET JARDIN</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="2" mp="M">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="1.3">sc<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" caesura="1">in</seg>s</w><caesura></caesura> <w n="1.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="1.6">r<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="1.8" punct="vg:12">gr<seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="2.2">gr<seg phoneme="o" type="vs" value="1" rule="434" place="2" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="2.3">l<seg phoneme="œ" type="vs" value="1" rule="406" place="5" mp="C">eu</seg>r</w> <w n="2.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg>x</w> <w n="2.6">y<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="2.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>s</w> <w n="2.9">b<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="M">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>s</w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="3.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="3.5">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>q</w> <w n="3.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="8">en</seg>ts</w> <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="P">a</seg>r</w> <w n="3.8"><seg phoneme="y" type="vs" value="1" rule="452" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="3.9" punct="pv:12">r<seg phoneme="y" type="vs" value="1" rule="456" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="4.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="P">a</seg>r</w> <w n="4.4">l</w>'<w n="4.5" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="317" place="6" punct="vg" caesura="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="4.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="C">i</seg>ls</w> <w n="4.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>t</w> <w n="4.8" punct="ps:10">m<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="ps ti" mp="F">e</seg></w>… — <w n="4.9" punct="pe:12">s<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="M">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="419" place="12" punct="pe">oi</seg>s</w> !</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">C</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="5.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">ch<seg phoneme="e" type="vs" value="1" rule="346" place="5" mp="P">ez</seg></w> <w n="5.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.8" punct="vg:8">c<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>rqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.9"><seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="M">u</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="5.10">d</w>'<w n="5.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">É</seg>bl<seg phoneme="u" type="vs" value="1" rule="426" place="2" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>t</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" mp="C">on</seg></w> <w n="6.3" punct="vg:6">p<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>c</w>,<caesura></caesura> <w n="6.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="188" place="7" punct="vg">e</seg>t</w>, <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="P">a</seg>r</w> <w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9" mp="C">un</seg></w> <w n="6.7">t<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>r</w> <w n="6.8" punct="vg:12">b<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="12" punct="vg">in</seg></w>,</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>t</w> <w n="7.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="M">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r</w> <w n="7.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="7.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>ts</w><caesura></caesura> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="7.6">qu<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.7">s<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>s</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="7.9">pi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">C<seg phoneme="o" type="vs" value="1" rule="414" place="1" mp="M">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="8.2" punct="vg:3">c<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>r</w>, <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="8.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="8.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="8.7">c<seg phoneme="o" type="vs" value="1" rule="414" place="9" mp="M">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg></w> <w n="8.8" punct="pt:12">j<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="12" punct="pt">in</seg></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>