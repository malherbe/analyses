<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉPAVES</head><head type="main_subpart">PATRIE</head><div type="poem" n="1" key="FRK18" modus="cm" lm_max="12" metProfile="6+6">
							<head type="number">I</head>
							<lg n="1">
								<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">QU<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">AN</seg>D</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="1.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="M">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="1.5">v<seg phoneme="y" type="vs" value="1" rule="449" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="1.6">fu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>r</w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg></w> <w n="1.9" punct="vg:12">r<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
								<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="C">on</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5" mp="M">in</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg" caesura="1">a</seg>ct<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="8">e</seg>c</w> <w n="2.6">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="2.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="2.8">v<seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>x</w></l>
								<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="3.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5" mp="M">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>t</w> <w n="3.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="317" place="10" mp="M">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419" place="12" punct="vg">oi</seg>s</w>,</l>
								<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>rt</w><caesura></caesura> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.6">s<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="9">e</seg>il</w> <w n="4.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" mp="P">an</seg>s</w> <w n="4.8">l</w>’<w n="4.9" punct="pe:12"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
							</lg>
							<lg n="2">
								<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="5.2" punct="vg:3">P<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="5.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="5" mp="C">o</seg>s</w> <w n="5.5">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6" caesura="1">œu</seg>rs</w><caesura></caesura> <w n="5.6">n<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>l</w> <w n="5.7">m<seg phoneme="o" type="vs" value="1" rule="437" place="8">o</seg>t</w> <w n="5.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.9">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="5.10" punct="pe:12">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
								<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>g</w> <w n="6.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="6.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="6.6">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="6.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.9">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="6.10" punct="vg:12">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="12" punct="vg">oi</seg>ts</w>,</l>
								<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="7.3">l<seg phoneme="wa" type="vs" value="1" rule="422" place="4">oi</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="7.5">f<seg phoneme="ɛ" type="vs" value="1" rule="63" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="7.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="8" mp="M">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>rs</w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="7.9">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="7.10" punct="pv:12">r<seg phoneme="wa" type="vs" value="1" rule="419" place="12" punct="pv">oi</seg>s</w> ;</l>
								<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="8.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="P">ou</seg>r</w> <w n="8.3"><seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="8.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="C">a</seg></w> <w n="8.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="8.7">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="8.8">br<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>s</w> <w n="8.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>t</w> <w n="8.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="8.11" punct="pe:12">gl<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
							</lg>
							<lg n="3">
								<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="9.2">P<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="9.4" punct="pe:6">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pe" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> !<caesura></caesura> <w n="9.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="9">en</seg></w> <w n="9.8">s<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="9.9">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">on</seg></w></l>
								<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="10.2">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>c<seg phoneme="œ" type="vs" value="1" rule="344" place="6" caesura="1">ue</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg></w> <w n="10.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="8">ein</seg></w> <w n="10.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="C">u</seg></w> <w n="10.8">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rcl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.9" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11" mp="M">in</seg>t<seg phoneme="i" type="vs" value="1" rule="466" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
								<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="11.2">p<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>t</w> <w n="11.4">g<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="11.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="11.6">f<seg phoneme="wa" type="vs" value="1" rule="439" place="8" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="11.7" punct="vg:12">l<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>t<seg phoneme="i" type="vs" value="1" rule="466" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							</lg>
							<lg n="4">
								<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="12.2">qu</w>’<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" mp="M">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="12.5">P<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>x</w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="188" place="9">e</seg>t</w> <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="12.8">R<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">on</seg></w></l>
								<l n="13" num="4.2" lm="12" met="6+6">— <w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="P">u</seg>r</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">s<seg phoneme="œ" type="vs" value="1" rule="405" place="3">eu</seg>il</w> <w n="13.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4">où</seg></w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>d</w> <w n="13.8">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="13.9">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="13.10" punct="tc:12">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ti" mp="F">e</seg></w> —</l>
								<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="14.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="3" mp="C">eu</seg>rs</w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="14.5">tr<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="14.7">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.8" punct="pe:12"><seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
							</lg>
						</div></body></text></TEI>