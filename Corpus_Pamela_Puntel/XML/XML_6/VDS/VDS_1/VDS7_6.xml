<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES GRAINS DE POUDRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VDS">
					<name>
						<forename>Ali-Joseph-Augustin</forename>
						<surname>VIAL DE SABLIGNY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>604 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VDS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES GRAINS DE POUDRE</title>
						<author>ALI VIAL DE SABLIGNY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5612402j.r=VIAL%20DE%20SABLIGNY%20ALI-JOSEPH-AUGUSTIN%20LES%20GRAINS%20DE%20POUDRE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES GRAINS DE POUDRE</title>
								<author>ALI VIAL DE SABLIGNY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DESCHAMPS, PAPETIER-LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VDS7" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main">RETOURNONS AU TRAVAIL<ref type="noteAnchor">*</ref></head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="3">en</seg>ts</w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="C">u</seg></w> <w n="1.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="1.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>t</w> <w n="1.8">pl<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>s</w> <w n="1.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="2.3">f<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t</w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="Lc">ou</seg>rd</w>'<w n="2.5">hu<seg phoneme="i" type="vs" value="1" rule="490" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="2.6">d<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>p<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="2.8" punct="vg:12">f<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="vg">i</seg>l</w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="3.2">l<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="3.4" punct="pv:6">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" mp="M">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="466" place="5" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pv" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ;<caesura></caesura> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="132" place="7">e</seg>h</w> <w n="3.6" punct="pe:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8" punct="pe">en</seg></w> ! <w n="3.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9" mp="P">an</seg>s</w> <w n="3.8">pl<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>s</w> <w n="3.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="P">à</seg></w> <w n="4.3">l</w>'<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>li<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="4.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>s</w> <w n="4.7">l</w>'<w n="4.8" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="M">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="pt">i</seg>l</w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" mp="M">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="5.4">j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>rs</w> <w n="5.5">p<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>rs</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="5.7" punct="vg:12">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="11" mp="M">o</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.4">tr<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="4">a</seg>il</w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="6.6">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>l</w><caesura></caesura> <w n="6.7">p<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>t</w> <w n="6.8">n<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="6.9" punct="dp:12">v<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="12" punct="dp">er</seg></w> :</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>rn<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg></w> <w n="7.3" punct="vg:6">tr<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="6" punct="vg" caesura="1">a</seg>il</w>,<caesura></caesura> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M/mp">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem/mp">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="Lp">on</seg>s</w>-<w n="7.5" punct="vg:10">n<seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="vg">ou</seg>s</w>, <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="7.7" punct="vg:12">fr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">Qu</w>'<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="8.3">d</w>'<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.5" punct="vg:6">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg" caesura="1">a</seg>t</w>,<caesura></caesura> <w n="8.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>t</w> <w n="8.8" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="12" punct="pt">er</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg></w> <w n="9.2" punct="vg:3">tr<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="3" punct="vg">a</seg>il</w>, <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg></w> <w n="9.4" punct="pe:6">tr<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="6" punct="pe" caesura="1">a</seg>il</w> !<caesura></caesura> <w n="9.5" punct="vg:9">F<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" mp="M">o</seg>rg<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" punct="vg">on</seg></w>, <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="9.7">l</w>'<w n="9.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>cl<seg phoneme="y" type="vs" value="1" rule="452" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="2.2" lm="12" met="6+6"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>c</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.4" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="10.5">c</w>'<w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r</w> <w n="10.9" punct="pe:12">s<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="pe">é</seg></w> !</l>
					<l n="11" num="2.3" lm="12" met="6+6"><w n="11.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="P">à</seg></w> <w n="11.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="11.4" punct="pv:6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5" mp="M">in</seg>c<seg phoneme="o" type="vs" value="1" rule="314" place="6" punct="pv" caesura="1">eau</seg>x</w> ;<caesura></caesura> <w n="11.5" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9" punct="vg">ain</seg></w>, <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="11.8" punct="vg:12">pl<seg phoneme="y" type="vs" value="1" rule="452" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="12" num="2.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">gl<seg phoneme="ɔ" type="vs" value="1" rule="442" place="3">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="12.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="12.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="12.7" punct="pt:12">r<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="pt">é</seg></w>.</l>
					<l n="13" num="2.5" lm="12" met="6+6"><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="13.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" mp="P">an</seg>s</w> <w n="13.4">v<seg phoneme="o" type="vs" value="1" rule="437" place="5" mp="C">o</seg>s</w> <w n="13.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.7">s<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>t</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="13.10" punct="vg:12">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="14" num="2.6" lm="12" met="6+6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="14.2">l</w>'<w n="14.3" punct="vg:4"><seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="14.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="C">ou</seg>s</w> <w n="14.5" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="M">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="8">ez</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="14.8">l</w>'<w n="14.9" punct="vg:12"><seg phoneme="y" type="vs" value="1" rule="452" place="10" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="12" punct="vg">e</seg>rs</w>,</l>
					<l n="15" num="2.7" lm="12" met="6+6"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg>x</w> <w n="15.2">m<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="15.3">fr<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="15.5" punct="vg:9">st<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="M">u</seg>p<seg phoneme="œ" type="vs" value="1" rule="406" place="9" punct="vg">eu</seg>r</w>, <w n="15.6">d</w>'<w n="15.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="10" mp="M">im</seg>pu<seg phoneme="i" type="vs" value="1" rule="490" place="11" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="16" num="2.8" lm="12" met="6+6"><w n="16.1">Qu</w>'<w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="16.3">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="16.5">su<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>t</w> <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="16.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>s</w> <w n="16.8">n<seg phoneme="o" type="vs" value="1" rule="437" place="10" mp="C">o</seg>s</w> <w n="16.9" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="12" punct="pt">e</seg>rs</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="12" met="6+6"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="17.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">am</seg>p</w> <w n="17.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="17.4">tr<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="5" mp="M">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="17.5">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="17.7">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.8"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="18" num="3.2" lm="12" met="6+6"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="18.2">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="18.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="P">ou</seg>r</w> <w n="18.4" punct="dp:6">dr<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="6" punct="dp" caesura="1">eau</seg></w> :<caesura></caesura> <w n="18.5" punct="vg:8">C<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.6" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="vg">oi</seg>r</w>, <w n="18.7" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="12" punct="vg">eu</seg>r</w>,</l>
					<l n="19" num="3.3" lm="12" met="6+6"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="19.2">d<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="19.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg></w> <w n="19.5">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="5" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="6" caesura="1">y</seg>s</w><caesura></caesura> <w n="19.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="19.7">s<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="19.8" punct="pe:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>n<seg phoneme="o" type="vs" value="1" rule="434" place="11" mp="M">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
					<l n="20" num="3.4" lm="12" met="6+6"><w n="20.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>t</w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="P">à</seg></w> <w n="20.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="20.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg>ts</w>,<caesura></caesura> <w n="20.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>t</w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="20.7">l<seg phoneme="œ" type="vs" value="1" rule="406" place="10" mp="C">eu</seg>r</w> <w n="20.8" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406" place="12" punct="pe">eu</seg>r</w> !</l>
					<l n="21" num="3.5" lm="12" met="6+6"><w n="21.1">C</w>'<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="21.3"><seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="21.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="21.5">r<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">an</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="21.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="21.8">f<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="22" num="3.6" lm="12" met="6+6"><w n="22.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="22.2">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="M">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>s</w> <w n="22.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="22.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="22.6" punct="vg:12">f<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg></w>,</l>
					<l n="23" num="3.7" lm="12" met="6+6"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="23.2">l</w>'<w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="23.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="23.5" punct="vg:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="5" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="6" punct="vg" caesura="1">ô</seg>t</w>,<caesura></caesura> <w n="23.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="434" place="8" mp="M">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>t</w> <w n="23.7">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="23.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="24" num="3.8" lm="12" met="6+6"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="24.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="24.5" punct="vg:6">f<seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="24.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="8">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="24.8" punct="pe:12">L<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="pe">é</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Paris, 10 mars 1871.</date>
					</dateline>
					<note type="footnote" id="*">Ces paroles, mises en musique par M. Le Corbeiller, sont en vente chez Colombier, éditeur</note>
				</closer>
			</div></body></text></TEI>