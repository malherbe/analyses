<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES GRAINS DE POUDRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VDS">
					<name>
						<forename>Ali-Joseph-Augustin</forename>
						<surname>VIAL DE SABLIGNY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>604 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VDS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES GRAINS DE POUDRE</title>
						<author>ALI VIAL DE SABLIGNY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5612402j.r=VIAL%20DE%20SABLIGNY%20ALI-JOSEPH-AUGUSTIN%20LES%20GRAINS%20DE%20POUDRE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES GRAINS DE POUDRE</title>
								<author>ALI VIAL DE SABLIGNY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DESCHAMPS, PAPETIER-LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VDS12" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main">AIMONS-NOUS</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>p<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="1.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.5" punct="vg:12">c<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="2.2" punct="vg:3">fr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="4" mp="M">ai</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="2.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="2.5">d</w>'<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="2.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="8">ain</seg>t</w> <w n="2.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="12" punct="pv">en</seg>t</w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2" mp="C">o</seg>s</w> <w n="3.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="3">œu</seg>rs</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="P">à</seg></w> <w n="3.5">ch<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="6" caesura="1">un</seg></w><caesura></caesura> <w n="3.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w> <w n="3.7">l</w>'<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>s</w> <w n="3.9">f<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">qu</w>'<w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="4.4">c<seg phoneme="o" type="vs" value="1" rule="434" place="3" mp="M">o</seg>mm<seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>rd</w><caesura></caesura> <w n="4.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="4.7">l<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.8" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="419" place="10" mp="M">oi</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="12" punct="pe">en</seg>t</w> !</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="5.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4">en</seg></w> <w n="5.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">on</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="P">a</seg>r</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>g</w> <w n="5.9"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="5.10">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="5.11" punct="dp:12">l<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg>s</w> :</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1" mp="M">U</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="6.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="4" mp="C">o</seg>s</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="352" place="5" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>rts</w><caesura></caesura> <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>r</w> <w n="6.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="10">en</seg></w> <w n="6.7" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pv">an</seg>t</w> ;</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2" punct="vg:3">tr<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="3" punct="vg">a</seg>il</w>, <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="7.4" punct="vg:6">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">è</seg>s</w>,<caesura></caesura> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="7.7">m<seg phoneme="ɛ" type="vs" value="1" rule="381" place="9" mp="M">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="7.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="3">ein</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.4">b<seg phoneme="y" type="vs" value="1" rule="449" place="6" caesura="1">u</seg>t</w><caesura></caesura> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6">l</w>'<w n="8.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w> <w n="8.8">v<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="8.9" punct="pt:12">p<seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="M">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="490" place="11" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg>t</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="12" met="6+6"><w n="9.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="M">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="M">on</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="9.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>gs</w><caesura></caesura> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" mp="P">an</seg>s</w> <w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="9.7">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.8" punct="vg:12">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11" mp="M">in</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="2.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="10.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="M">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="351" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="10.4">l</w>'<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="8">e</seg>c</w> <w n="10.7">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="C">u</seg></w> <w n="10.9" punct="pv:12">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="12" punct="pv">om</seg></w> ;</l>
					<l n="11" num="2.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="11.2">l</w>'<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>t</w> <w n="11.6">fi<seg phoneme="ɛ" type="vs" value="1" rule="345" place="9">e</seg>l</w> <w n="11.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.8" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="12" num="2.4" lm="12" met="6+6"><w n="12.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="12.2">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3">en</seg></w> <w n="12.3">fr<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="12.4">s<seg phoneme="wa" type="vs" value="1" rule="439" place="7" mp="M">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w> <w n="12.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>s</w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="10" mp="C">un</seg></w> <w n="12.7" punct="pt:12">ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="11" mp="M">aî</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="pt">on</seg></w>.</l>
					<l n="13" num="2.5" lm="12" met="6+6"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">am</seg>b<seg phoneme="o" type="vs" value="1" rule="314" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="13.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="13.6">gu<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="188" place="9">e</seg>t</w> <w n="13.8">n<seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="C">ou</seg>s</w> <w n="13.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="14" num="2.6" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" mp="M">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="14.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="14.6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>x</w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="14.9" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="vg">on</seg></w>,</l>
					<l n="15" num="2.7" lm="12" met="6+6"><w n="15.1" punct="vg:2">Su<seg phoneme="i" type="vs" value="1" rule="490" place="1" mp="M">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="15.2">su<seg phoneme="i" type="vs" value="1" rule="490" place="3" mp="M">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="15.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="15.4">c<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="15.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="C">u</seg></w> <w n="15.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="Lc">on</seg></w>-<w n="15.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="9">en</seg>s</w> <w n="15.8" punct="pt:12">p<seg phoneme="o" type="vs" value="1" rule="443" place="10" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="16" num="2.8" lm="12" met="6+6"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="16.2">l</w>'<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="C">u</seg></w> <w n="16.5">b<seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="16.6">h<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="16.8" punct="pt:12">fl<seg phoneme="o" type="vs" value="1" rule="443" place="10" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="pt">on</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="12" met="6+6"><w n="17.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="17.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="17.5">qu</w>'<w n="17.6"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="17.7">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="17.8" punct="pv:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="18" num="3.2" lm="12" met="6+6"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg></w> <w n="18.2">f<seg phoneme="wa" type="vs" value="1" rule="439" place="2" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="18.4">l</w>'<w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="18.6">r<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M/mp">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="M/mp">au</seg>ff<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="Lp">on</seg>s</w>-<w n="18.7">n<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>s</w> <w n="18.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="11" mp="C">un</seg></w> <w n="18.9" punct="dp:12">p<seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="dp">eu</seg></w> :</l>
					<l n="19" num="3.3" lm="12" met="6+6"><w n="19.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="19.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="P">ou</seg>s</w> <w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4" mp="C">un</seg></w> <w n="19.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>l</w> <w n="19.5">p<seg phoneme="y" type="vs" value="1" rule="449" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="19.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7">où</seg></w> <w n="19.7">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t</w> <w n="19.8"><seg phoneme="e" type="vs" value="1" rule="188" place="11">e</seg>t</w> <w n="19.9">br<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="20" num="3.4" lm="12" met="6+6"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="20.2">s<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3">e</seg>il</w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="20.5" punct="vg:6">p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="vg" caesura="1">ai</seg>x</w>,<caesura></caesura> <w n="20.6">d<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>t</w> <w n="20.7"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="20.8">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="20.9" punct="pt:12">v<seg phoneme="ø" type="vs" value="1" rule="247" place="12" punct="pt">œu</seg></w>.</l>
					<l n="21" num="3.5" lm="12" met="6+6"><w n="21.1">Pr<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="C">a</seg></w> <w n="21.3" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="419" place="5" mp="M">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="21.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ls</w> <w n="21.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="21.7" punct="vg:12">R<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="22" num="3.6" lm="12" met="6+6"><w n="22.1">F<seg phoneme="œ" type="vs" value="1" rule="303" place="1" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="22.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>gn<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="22.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="22.5">j<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="M">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.6"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="22.7">l</w>'<w n="22.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="vg">ou</seg>r</w>,</l>
					<l n="23" num="3.7" lm="12" met="6+6"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="23.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="23.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="C">ou</seg>s</w> <w n="23.5">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="23.6">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" caesura="1">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="23.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7" mp="C">un</seg></w> <w n="23.8">s<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg></w> <w n="23.9" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="466" place="11" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="24" num="3.8" lm="12" met="6+6"><w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="24.2">l</w>'<w n="24.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="M">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="24.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="24.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="24.6" punct="vg:9">fr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg" mp="F">e</seg>s</w>, <w n="24.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="10">en</seg></w> <w n="24.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="11" mp="C">un</seg></w> <w n="24.9" punct="pt:12">j<seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="pt">ou</seg>r</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Paris, 30 juin 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>