<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR5" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main"> 31 OCTOBRE 1870</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">EN</seg>D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">AN</seg>T</w> <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="1.4">nu<seg phoneme="i" type="vs" value="1" rule="490" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="1.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="1.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="P">u</seg>r</w> <w n="1.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="1.9">qu<seg phoneme="i" type="vs" value="1" rule="490" place="11" mp="Lc">i</seg></w>-<w n="1.10" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="2.2">pl<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="P">à</seg></w> <w n="2.4" punct="pt:4">fl<seg phoneme="o" type="vs" value="1" rule="437" place="4" punct="pt">o</seg>ts</w>. <w n="2.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.7">j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r</w> <w n="2.8">t<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>f</w> <w n="2.9" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>l</w> <w n="3.3">s</w>'<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="3.5" punct="dp:6">d<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" punct="dp" caesura="1">e</seg>rt</w> :<caesura></caesura> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.7">s<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="9">e</seg>il</w> <w n="3.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="10" mp="C">u</seg></w> <w n="3.9">m<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="12">in</seg></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="P">u</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="4.3">s<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="4.4">m<seg phoneme="u" type="vs" value="1" rule="427" place="5" mp="M">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="4.5">j<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="4.7">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="9" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg></w> <w n="4.8" punct="pt:12">ch<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="12" punct="pt">in</seg></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="5.3" punct="vg:6">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="M">an</seg>cm<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>sn<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>l</w>,<caesura></caesura> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="5.5">pu<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg>s</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="5.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="304" place="10">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.8" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="466" place="11" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>n<seg phoneme="o" type="vs" value="1" rule="434" place="5" mp="M">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.4" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="6.5" punct="vg:10">v<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="vg">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="6.6" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="P">a</seg>r</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="7.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="7.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="7.5">pu<seg phoneme="i" type="vs" value="1" rule="490" place="7" mp="M">i</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="7.6" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>pp<seg phoneme="ɥi" type="vs" value="1" rule="461" place="11" mp="M">u</seg>y<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg>s</w>,</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="8.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>g<seg phoneme="i" type="vs" value="1" rule="466" place="3" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>ts</w> <w n="8.3">pr<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6" caesura="1">en</seg>s</w><caesura></caesura> <w n="8.4">s</w>‘<w n="8.5" punct="vg:9"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg" mp="F">e</seg>nt</w>, <w n="8.6">d<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="439" place="11" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg>s</w></l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="9.2">t<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="306" place="3" mp="M">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>rs</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.5" punct="vg:9">B<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="189" place="9" punct="vg">e</seg>t</w>, <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>ls</w> <w n="9.8" punct="pt:12">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>nt</w>.</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1" punct="vg:2">D<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="M">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="10.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>gs</w> <w n="10.4" punct="vg:6">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="10.6">r<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="10.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="10.8" punct="pt:12">m<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>nt</w>.</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>ls</w> <w n="11.2">s</w>’<w n="11.3" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="pt" mp="F">e</seg>nt</w>. <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="132" place="5">E</seg>h</w> <w n="11.5" punct="pe:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6" punct="pe" caesura="1">en</seg></w> !<caesura></caesura> <w n="11.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="11.7">v<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="11.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="11.9" punct="pe:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="12" punct="pe">oi</seg>r</w> !</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1" punct="pe:3">C<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="12.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="12.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>t</w> <w n="12.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="9" mp="M">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>rs</w> <w n="12.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="12.7" punct="pe:12">s<seg phoneme="wa" type="vs" value="1" rule="419" place="12" punct="pe">oi</seg>r</w> !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="2">
					<l n="13" num="2.1" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">B<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="13.4" punct="dp:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="dp" caesura="1">i</seg>s</w> :<caesura></caesura> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="13.6">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="9">e</seg>st</w> <w n="13.8" punct="ps:12">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="M">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></w>…</l>
					<l n="14" num="2.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="14.2" punct="pe:3">r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="14.3">tr<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>s</w> <w n="14.4" punct="pe:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pe" caesura="1">on</seg>s</w> !<caesura></caesura> <w n="14.5"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="C">I</seg>ls</w> <w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.8" punct="pe:12">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11" mp="M">in</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>