<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR14" modus="cp" lm_max="12" metProfile="6, 6+6">
				<head type="main">REFRAIN PRUSSIEN</head>
				<opener>
					<dateline>
						<date when="1871">Janvier 1871.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">HU<seg phoneme="i" type="vs" value="1" rule="490" place="1">I</seg>T</w> <w n="1.2">h<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="1.4" punct="pt:6">s<seg phoneme="o" type="vs" value="1" rule="434" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="pt" caesura="1">é</seg></w>.<caesura></caesura> <w n="1.5">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>j<seg phoneme="ɔ" type="vs" value="1" rule="442" place="9">o</seg>r</w> <w n="1.7">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>rt</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="1.9" punct="vg:12">t<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="3">en</seg>s</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="2.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg>s</w> <w n="2.8" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="317" place="10" mp="M">au</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="pt">on</seg>s</w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg>x</w> <w n="3.2" punct="pe:3">pi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pe" mp="F">e</seg>s</w> ! <w n="3.3">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="3.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="3.5">v<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.6" punct="dp:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="10" mp="M">im</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></w> :</l>
					<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="12"></space><space quantity="12" unit="char"></space><w n="4.1" punct="vg:3">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg>s</w>, <w n="4.2" punct="pe:6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pe">on</seg>s</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="P">a</seg>r</w> <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">fr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>d</w> <w n="5.4">gl<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="5.5">s</w>’<w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" mp="M">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>t</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="5.8">l<seg phoneme="œ" type="vs" value="1" rule="406" place="11" mp="C">eu</seg>r</w> <w n="5.9" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="6.2">P<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>si<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4">en</seg>s</w> <w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="6.4">ch<seg phoneme="o" type="vs" value="1" rule="317" place="6" caesura="1">au</seg>d</w><caesura></caesura> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w> <w n="6.6">l<seg phoneme="œ" type="vs" value="1" rule="406" place="9" mp="C">eu</seg>rs</w> <w n="6.7" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="pv">on</seg>s</w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">R<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M/mp">é</seg>v<seg phoneme="e" type="vs" value="1" rule="382" place="2" mp="M/mp">e</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" caesura="1">e</seg>c</w><caesura></caesura> <w n="7.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="7.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>s</w> <w n="7.6">b<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189" place="10">e</seg>ts</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="7.8" punct="dp:12">s<seg phoneme="ɛ" type="vs" value="1" rule="383" place="12">ei</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></w> :</l>
					<l n="8" num="2.4" lm="6" met="6"><space unit="char" quantity="12"></space><space quantity="12" unit="char"></space><w n="8.1" punct="vg:3">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg>s</w>, <w n="8.2" punct="pe:6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pe">on</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="9.2" punct="vg:2">nu<seg phoneme="i" type="vs" value="1" rule="490" place="2" punct="vg">i</seg>t</w>, <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="9.4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ts</w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="9.6" punct="vg:6">gr<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="9.7">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.8">l</w>’<w n="9.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="9.10">d<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t</w> <w n="9.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="11">en</seg></w> <w n="9.12" punct="vg:12">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="10.4">h<seg phoneme="o" type="vs" value="1" rule="414" place="4" mp="M">ô</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>t<seg phoneme="o" type="vs" value="1" rule="317" place="6" caesura="1">au</seg>x</w><caesura></caesura> <w n="10.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>t</w> <w n="10.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10" mp="C">e</seg>s</w> <w n="10.9" punct="pv:12">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="pv">on</seg>s</w> ;</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">s<seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="11.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="5" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="11.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="11.7" punct="dp:12">d<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></w> :</l>
					<l n="12" num="3.4" lm="6" met="6"><space unit="char" quantity="12"></space><space quantity="12" unit="char"></space><w n="12.1" punct="vg:3">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg>s</w>, <w n="12.2" punct="pe:6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pe">on</seg>s</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">V<seg phoneme="wa" type="vs" value="1" rule="419" place="1" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="13.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="4">ô</seg>t</w> <w n="13.3">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>q</w> <w n="13.4">m<seg phoneme="wa" type="vs" value="1" rule="419" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="13.7">n</w>’<w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg></w> <w n="13.9">b<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg></w> <w n="13.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="13.11" punct="pe:12">bi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="14.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>c</w> <w n="14.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="14.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem/mp">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M/mp">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" punct="vg" caesura="1">ai</seg></w>-<w n="14.5" punct="vg:6">j<seg phoneme="ə" type="ee" value="0" rule="e-15">e</seg></w>,<caesura></caesura> <w n="14.6"><seg phoneme="o" type="vs" value="1" rule="414" place="7">ô</seg></w> <w n="14.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>ds</w> <w n="14.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">am</seg>ps</w> <w n="14.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="14.10" punct="vg:12">h<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="M">ou</seg>bl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="vg">on</seg>s</w> ,</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="15.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="15.3">j<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="M">am</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="15.4" punct="vg:6">f<seg phoneme="y" type="vs" value="1" rule="452" place="5" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="15.6">ch<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>cr<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="15.7" punct="pi:12">l<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
					<l n="16" num="4.4" lm="6" met="6"><space unit="char" quantity="12"></space><space quantity="12" unit="char"></space><w n="16.1" punct="vg:3">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg>s</w>, <w n="16.2" punct="pe:6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pe">on</seg>s</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1" punct="vg:1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" punct="vg">an</seg>tz</w>, <w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="C">u</seg></w> <w n="17.3">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="17.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5">Gr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>tch<seg phoneme="ɛ" type="vs" value="1" rule="24" place="6" caesura="1">e</seg>n</w><caesura></caesura> <w n="17.6">m</w>’<w n="17.7"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="17.8">d<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t</w> <w n="17.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9" mp="P">an</seg>s</w> <w n="17.10"><seg phoneme="y" type="vs" value="1" rule="452" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="17.11">l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">Qu</w>’<w n="18.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="18.3">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="18.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="18.5">br<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="18.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>r</w> <w n="18.7"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" mp="M">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="18.8">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10" mp="C">e</seg>s</w> <w n="18.9">br<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>s</w> <w n="18.10" punct="pi:12">r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="pi">on</seg>ds</w> ?</l>
					<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="19.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3" mp="C">i</seg></w> <w n="19.4" punct="vg:6">d<seg phoneme="o" type="vs" value="1" rule="443" place="4" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" punct="vg" caesura="1">ai</seg></w>,<caesura></caesura> <w n="19.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="19.6">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg></w> <w n="19.7">v<seg phoneme="ø" type="vs" value="1" rule="397" place="9">eu</seg>t</w> <w n="19.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="19.9" punct="ps:12">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></w>…</l>
					<l n="20" num="5.4" lm="6" met="6"><space unit="char" quantity="12"></space><space quantity="12" unit="char"></space><w n="20.1" punct="vg:3">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg>s</w>, <w n="20.2" punct="pe:6">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pe">on</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>