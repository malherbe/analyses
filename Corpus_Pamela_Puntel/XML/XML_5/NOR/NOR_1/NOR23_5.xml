<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR23" modus="sm" lm_max="8">
				<head type="main">UN SPECTACLE DRÔLE</head>
				<opener>
					<dateline>
						<date when="1871">Mars 1871.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">EN</seg></w> <w n="1.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="2">ein</seg></w> <w n="1.3" punct="vg:5">b<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="vg">a</seg>rd</w>, <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rd</w>’<w n="1.5" punct="vg:8">hu<seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="vg">i</seg></w>,</l>
					<l n="2" num="1.2" lm="8"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="2.3">v<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="2.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.5">ch<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7" punct="vg:8">dr<seg phoneme="o" type="vs" value="1" rule="414" place="8">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="8"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="3.2">dr<seg phoneme="o" type="vs" value="1" rule="414" place="2">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">j</w>’<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="3.7">r<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w></l>
					<l n="4" num="1.4" lm="8"><w n="4.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="4.2">j</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="4.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="4.5">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="4.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="4.7" punct="pt:8">r<seg phoneme="o" type="vs" value="1" rule="414" place="8">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w>-<w n="5.4" punct="pi:5">v<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="pi">ou</seg>s</w> ? <w n="5.5">J</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="5.7">pl<seg phoneme="ø" type="vs" value="1" rule="404" place="7">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w></l>
					<l n="6" num="2.2" lm="8"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="7" num="2.3" lm="8"><w n="7.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="vg">E</seg>t</w>, <w n="7.2">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="7.4" punct="vg:5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="5" punct="vg">en</seg>ts</w>, <w n="7.5">j</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w></l>
					<l n="8" num="2.4" lm="8"><w n="8.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="8.2" punct="vg:3">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="8.5">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="8.6" punct="dp:8">s<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>x</w> <w n="9.4">g<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="9.5">c<seg phoneme="o" type="vs" value="1" rule="434" place="6">o</seg>mm<seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w></l>
					<l n="10" num="3.2" lm="8"><w n="10.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="10.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>t</w> <w n="10.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="10.5" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
					<l n="11" num="3.3" lm="8"><w n="11.1">C</w>’<w n="11.2" punct="tc:2"><seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2" punct="vg ti">ai</seg>t</w>, — <w n="11.3">sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.4" punct="pe:8">c<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pe ti">eu</seg>x</w> ! —</l>
					<l n="12" num="3.4" lm="8"><w n="12.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>x</w> <w n="12.2">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ts</w> <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="12.5" punct="pe:8">g<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>