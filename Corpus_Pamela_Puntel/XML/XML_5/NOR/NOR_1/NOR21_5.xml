<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR21" modus="cm" lm_max="12">
				<head type="main">18 MARS 1871</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">O</seg>RS</w> <w n="1.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="1.3">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>q</w> <w n="1.4">m<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s</w> <w n="1.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w> <w n="1.7" punct="pv:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="pv">u</seg></w> ;</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="2.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>q</w> <w n="2.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg">oi</seg>s</w>, <w n="2.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6" punct="vg">en</seg>ts</w>, <w n="2.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w> <w n="2.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="411" place="11">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="12">u</seg></w></l>
					<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="3.2">h<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ts</w> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="3.4" punct="pv:6">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pv">a</seg>t</w> ; <w n="3.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="3.6" punct="vg:10">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="351" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg">e</seg></w>, <w n="3.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="11">an</seg>s</w> <w n="3.8" punct="vg:12">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="12">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="4.4">t<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s</w> <w n="4.5">s<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="8">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="12">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="5" num="1.5" lm="12"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="169" place="2">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="5.4" punct="vg:6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg">eu</seg>r</w>, <w n="5.5" punct="vg:10">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg">e</seg></w>, <w n="5.6" punct="pv:12">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="pv">eu</seg>x</w> ;</l>
					<l n="6" num="1.6" lm="12"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t</w> <w n="6.4" punct="vg:6">r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>squ<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg">é</seg></w>, <w n="6.5">c<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439" place="8">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="9">en</seg>s</w> <w n="6.6" punct="vg:12">g<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="vg">eu</seg>x</w>,</l>
					<l n="7" num="1.7" lm="12"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="7.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="7.3">l</w>'<w n="7.4">h<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="7.6">s<seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg></w> <w n="7.8" punct="pv:12">p<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="8" num="1.8" lm="12"><w n="8.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>xp<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="8.4">m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="8.5">f<seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>s</w> <w n="8.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="8.7" punct="pv:12">v<seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="9" num="1.9" lm="12"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="9.5" punct="vg:8">f<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">aim</seg></w>, <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="9.7" punct="vg:10">fr<seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="vg">oi</seg>d</w>, <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg></w> <w n="9.9" punct="pv:12">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="12" punct="pv">o</seg>rt</w> ;</l>
					<l n="10" num="1.10" lm="12"><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="10.3">v<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="10.4" punct="vg:6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg>b<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg">er</seg></w>, <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8">un</seg></w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg></w> <w n="10.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="352" place="11">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>rt</w>,</l>
					<l n="11" num="1.11" lm="12"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">c<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="11.5">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="8">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="11.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg></w> <w n="11.9" punct="pv:12">g<seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg>n<seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="12" num="1.12" lm="12"><w n="12.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg>s</w>, <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>s</w> <w n="12.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="12.4">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>q</w> <w n="12.5">m<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7">s<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">in</seg>f<seg phoneme="i" type="vs" value="1" rule="466" place="11">i</seg>n<seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="13" num="1.13" lm="12"><w n="13.1">C<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">in</seg>q</w> <w n="13.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>s</w>, <w n="13.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ls</w> <w n="13.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w> <w n="13.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg></w>,</l>
					<l n="14" num="1.14" lm="12"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="14.2">pr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>x</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.5" punct="vg:6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>g</w>, <w n="14.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.7">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>t</w> <w n="14.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11">e</seg>st<seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg></w></l>
					<l n="15" num="1.15" lm="12"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>b<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="15.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="15.4" punct="tc:6">r<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg ti">i</seg>r</w>, — <w n="15.5">c<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r</w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="15.7">ch<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.8"><seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11">ai</seg>t</w> <w n="15.9" punct="tc:12">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg ti">e</seg></w>, —</l>
					<l n="16" num="1.16" lm="12"><w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="16.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="16.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="16.5">d<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>r</w> <w n="16.6">cr<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="17" num="1.17" lm="12"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>r</w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>ds</w> <w n="17.5">p<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="17.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10">an</seg>s</w> <w n="17.7">n<seg phoneme="o" type="vs" value="1" rule="437" place="11">o</seg>s</w> <w n="17.8" punct="vg:12">m<seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="vg">u</seg>rs</w>,</l>
					<l n="18" num="1.18" lm="12"><w n="18.1">S<seg phoneme="u" type="vs" value="1" rule="427" place="1">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="18.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="18.3">m<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>n<seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>ts</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.5">l<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>rs</w> <w n="18.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>cts</w> <w n="18.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="11">im</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="vg">u</seg>rs</w>,</l>
					<l n="19" num="1.19" lm="12"><w n="19.1">D</w>’<w n="19.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="19.3"><seg phoneme="œ" type="vs" value="1" rule="285" place="2">œ</seg>il</w> <w n="19.4">v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="19.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="19.6">n<seg phoneme="o" type="vs" value="1" rule="437" place="10">o</seg>s</w> <w n="19.7" punct="vg:12">m<seg phoneme="y" type="vs" value="1" rule="449" place="11">u</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="20" num="1.20" lm="12"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>tt<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="20.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>rs</w> <w n="20.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="20.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8">e</seg>s</w> <w n="20.6">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">am</seg>ps</w>-<w n="20.7" punct="dp:12"><seg phoneme="e" type="vs" value="1" rule="408" place="10">É</seg>l<seg phoneme="i" type="vs" value="1" rule="492" place="11">y</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg>s</w> :</l>
					<l part="I" n="21" num="1.21" lm="12"><w n="21.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="21.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="21.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="21.4" punct="vg:6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg">a</seg></w>, <w n="21.5" punct="pi:8">p<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280" place="8" punct="pi ti">oi</seg></w> ? — </l>
					<l part="F" n="21" num="1.21" lm="12"><w n="21.6">P<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>r</w> <w n="21.7">qu</w>’<w n="21.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="10">en</seg></w> <w n="21.9"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="11">un</seg></w> <w n="21.10">j<seg phoneme="u" type="vs" value="1" rule="424" place="12">ou</seg>r</w></l>
					<l n="22" num="1.22" lm="12"><w n="22.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="22.3" punct="vg:6">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>ts</w>, <w n="22.4">v<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>s</w> <w n="22.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>r</w> <w n="22.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="22.7" punct="vg:12">f<seg phoneme="o" type="vs" value="1" rule="317" place="11">au</seg>b<seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="vg">ou</seg>rg</w>,</l>
					<l n="23" num="1.23" lm="12"><w n="23.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="23.2">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="23.3" punct="tc:6"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="6" punct="vg ti">ô</seg>t</w>, — <w n="23.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="23.5">l<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r</w> <w n="23.6">s<seg phoneme="u" type="vs" value="1" rule="427" place="9">ou</seg>ill<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="11">e</seg>st</w> <w n="23.8" punct="pe:12">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ti">e</seg></w> ! —</l>
					<l n="24" num="1.24" lm="12"><w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="338" place="1">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="24.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="24.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="24.4" punct="vg:6">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="vg">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">à</seg></w> <w n="24.6">r<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>lt<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg></w> <w n="24.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg></w> <w n="24.8" punct="pe:12">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>