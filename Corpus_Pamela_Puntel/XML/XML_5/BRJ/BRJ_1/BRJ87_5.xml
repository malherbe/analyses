<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ87" modus="cp" lm_max="12">
					<head type="number">LXXXVII</head>
					<head type="main">NOTRE FRITZ</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><space unit="char" quantity="8"></space><w n="1.1">J</w>'<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rgn<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w>-<w n="1.5" punct="pv:8">l<seg phoneme="a" type="vs" value="1" rule="341" place="8" punct="pv">à</seg></w> ;</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">cr<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>ls</w>, <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="248" place="8">œu</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg></w> <w n="2.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg></w> <w n="2.8" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="8"><space unit="char" quantity="8"></space><w n="3.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">A</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="4.4">m<seg phoneme="œ" type="vs" value="1" rule="248" place="4">œu</seg>rs</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>d<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg></w> <w n="4.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.8" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="317" place="11">au</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="5" num="1.5" lm="8"><space unit="char" quantity="8"></space><w n="5.1" punct="pe:2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406" place="2" punct="pe">eu</seg>r</w> ! <w n="5.2">C<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="5.4">l</w>'<w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w></l>
						<l n="6" num="1.6" lm="12"><w n="6.1">L</w>'<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="6.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="6.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">d</w>'<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="6.8">d<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11">on</seg></w> <w n="6.11" punct="pv:12">p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="7" num="1.7" lm="8"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="7.5">d<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">Au</seg>g<seg phoneme="y" type="vs" value="1" rule="447" place="7">u</seg>st<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w></l>
						<l n="8" num="1.8" lm="12"><w n="8.1">F<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>t</w> <w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="8.3">l</w>'<w n="8.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="8.7" punct="vg:8">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8" punct="vg">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="9">en</seg></w> <w n="8.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg></w> <w n="8.10" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="9" num="1.9" lm="8"><space unit="char" quantity="8"></space><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="9.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="vg">oi</seg>r</w>, <w n="9.3">d</w>'<w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="9.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="9.6">h<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rd<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w></l>
						<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>s</w> <w n="10.3">l</w>'<w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6">en</seg>t</w> <w n="10.5">d</w>'<w n="10.6"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.8" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="11" num="1.11" lm="8"><space unit="char" quantity="8"></space><w n="11.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="11.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ls</w> <w n="11.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rts</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="11.6">d<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w></l>
						<l n="12" num="1.12" lm="12"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="12.2">l</w>'<w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="2">im</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6">œu</seg>r</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6">s<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="12.7">r<seg phoneme="ɛ" type="vs" value="1" rule="384" place="9">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="12.9" punct="pe:12">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="13" num="1.13" lm="8"><space unit="char" quantity="8"></space><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="13.3">Fr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>tz</w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">em</seg>b<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.6" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>s</w> ;</l>
						<l n="14" num="1.14" lm="12"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="14.2">f<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="14.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="6">en</seg>s</w> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="14.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="14.7">r<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="341" place="11">à</seg></w> <w n="14.9" punct="dp:12">d<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="15" num="1.15" lm="8"><space unit="char" quantity="8"></space>« <w n="15.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2" punct="pe:3">Fr<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="pe ps">i</seg>tz</w> ! »… <w n="15.3">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4">Fr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>tz</w> <w n="15.5">n</w>'<w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="15.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>s</w></l>
						<l n="16" num="1.16" lm="12"><w n="16.1">D</w>'<w n="16.2">h<seg phoneme="y" type="vs" value="1" rule="452" place="1">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="16.4" punct="vg:6">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg">er</seg></w>, <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="16.6">l<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r</w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>d</w> <w n="16.8"><seg phoneme="a" type="vs" value="1" rule="341" place="11">à</seg></w> <w n="16.9" punct="pe:12">r<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="17" num="1.17" lm="8"><space unit="char" quantity="8"></space><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="17.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="17.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg></w>,</l>
						<l n="18" num="1.18" lm="12"><w n="18.1">B<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>ll</w>-<w n="18.2">d<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>g</w> <w n="18.3" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6" punct="vg">en</seg>t</w>, <w n="18.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t</w> <w n="18.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8">e</seg>s</w> <w n="18.6">cr<seg phoneme="o" type="vs" value="1" rule="436" place="9">o</seg>cs</w> <w n="18.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="18.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11">on</seg>t</w> <w n="18.9">f<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="19" num="1.19" lm="8"><space unit="char" quantity="8"></space><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="19.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>v<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="19.5" punct="pv:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pv">i</seg></w> ;</l>
						<l n="20" num="1.20" lm="12"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="20.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="20.4">d<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="20.6">d<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>t</w> <w n="20.7">fl<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>tt<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg></w> <w n="20.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg></w> <w n="20.9" punct="pv:12">b<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="21" num="1.21" lm="8"><space unit="char" quantity="8"></space>(<w n="21.1">C</w>'<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="21.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="21.4">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3">en</seg></w> <w n="21.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.7" punct="pt:7">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="pt ti">e</seg></w>.) — <w n="21.8" punct="vg:8">Br<seg phoneme="ɛ" type="vs" value="1" rule="345" place="8" punct="vg">e</seg>f</w>,</l>
						<l n="22" num="1.22" lm="12"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>g</w> <w n="22.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="22.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="22.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8">un</seg></w> <w n="22.7">d</w>'<w n="22.8"><seg phoneme="ø" type="vs" value="1" rule="397" place="9">eu</seg>x</w> <w n="22.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="22.10" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>c<seg phoneme="y" type="vs" value="1" rule="449" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="23" num="1.23" lm="8"><space unit="char" quantity="8"></space><w n="23.1">Fr<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>tz</w> <w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="23.3">f<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="23.6" punct="pv:8">ch<seg phoneme="ɛ" type="vs" value="1" rule="345" place="8" punct="pv">e</seg>f</w> ;</l>
						<l n="24" num="1.24" lm="12"><w n="24.1">J<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>squ</w>'<w n="24.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="24.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.4">Fr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>tz</w> <w n="24.5">n</w>'<w n="24.6"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>t</w> <w n="24.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="24.8" punct="pe:12">r<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>c<seg phoneme="y" type="vs" value="1" rule="449" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">Janvier 1871.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>