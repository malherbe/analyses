<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ77" modus="cp" lm_max="12">
					<head type="number">LXXVII</head>
					<head type="main">LE PAYSAN</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="2">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="3">y</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg></w> <w n="1.3">t<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="1.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.5">v<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>t</w> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>s</w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="10">en</seg></w> <w n="1.8" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="12">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="2.2">R<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.3">d</w>'<w n="2.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rd</w>'<w n="2.5">hu<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg></w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">Lu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="3.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="2">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">l</w>'<w n="3.5" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6" punct="pv">en</seg>t</w> ; <w n="3.6">s<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.8">r<seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="3.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.10">l</w>'<w n="3.11" punct="pt:12"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="12">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="4.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="4.5">gr<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7" punct="pe:8">lu<seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="pe">i</seg></w> !</l>
						<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="5.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="5.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="5.4" punct="vg:6">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rc<seg phoneme="o" type="vs" value="1" rule="314" place="6" punct="vg">eau</seg>x</w>, <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="5.6" punct="vg:9">fr<seg phoneme="o" type="vs" value="1" rule="443" place="8">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9" punct="vg">en</seg>t</w>, <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg></w> <w n="5.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="6.2">h<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="6.3">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>x</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="6.6" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">em</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>s</w> ;</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="7.3">ch<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="7.4" punct="vg:6">lu<seg phoneme="i" type="vs" value="1" rule="490" place="6" punct="vg">i</seg></w>, <w n="7.5">b<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="8">a</seg>il</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="188" place="9">e</seg>t</w> <w n="7.7" punct="pv:12">p<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="466" place="11">i</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="8" num="1.8" lm="8"><space unit="char" quantity="8"></space><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="8.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w>-<w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="8.7" punct="pe:8">gr<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe ps">a</seg>s</w> !…</l>
						<l n="9" num="1.9" lm="12"><w n="9.1" punct="pe:1">S<seg phoneme="wa" type="vs" value="1" rule="419" place="1" punct="pe">oi</seg>t</w> ! <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="9.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="vg">ai</seg>t</w>, <w n="9.5">j</w>'<w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="9.7" punct="pv:9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="9" punct="pv">en</seg>s</w> ; <w n="9.8">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>s</w> <w n="9.9">v<seg phoneme="u" type="vs" value="1" rule="424" place="11">ou</seg>s</w> <w n="9.10"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="10" num="1.10" lm="8"><space unit="char" quantity="8"></space><w n="10.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">en</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="10.6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r</w></l>
						<l n="11" num="1.11" lm="12"><w n="11.1">Qu</w>'<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="11.3"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg></w> <w n="11.4" punct="vg:6">b<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg">er</seg></w>, <w n="11.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11">e</seg>s</w> <w n="11.8" punct="pt:12">b<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="12" num="1.12" lm="8"><space unit="char" quantity="8"></space><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="12.5">l</w>'<w n="12.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>tt<seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="pe">oi</seg>r</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>