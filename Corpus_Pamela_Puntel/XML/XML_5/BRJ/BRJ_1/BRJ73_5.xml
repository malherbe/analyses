<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ73" modus="cm" lm_max="12">
					<head type="number">LXXIII</head>
					<head type="main">L’AUMÔNIER DU ROI</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">J</w>'<w n="1.2"><seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="1.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="1.6">r<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="1.7">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.8">l<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>g<seg phoneme="y" type="vs" value="1" rule="447" place="10">u</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.9">h<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3">j<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="2.6" punct="pt:6">nu<seg phoneme="i" type="vs" value="1" rule="490" place="6" punct="pt">i</seg>ts</w>. <w n="2.7">Pr<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="2.8">l<seg phoneme="œ" type="vs" value="1" rule="406" place="11">eu</seg>r</w> <w n="2.9" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="vg">ou</seg>rs</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="3.2">cr<seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">v<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg></w> <w n="3.8">m<seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="4.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="4.3">v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="5">en</seg>t</w> <w n="4.5" punct="pt:6">f<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pt">oi</seg>s</w>. <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8">e</seg>s</w> <w n="4.8">nu<seg phoneme="i" type="vs" value="1" rule="490" place="9">i</seg>ts</w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="4.10">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11">e</seg>s</w> <w n="4.11" punct="pe:12">j<seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="pe">ou</seg>rs</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="5.2">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ts</w> <w n="5.3">s</w>'<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">à</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>r</w> <w n="5.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11">e</seg>s</w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="6.3" punct="tc:6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="vg ti">ai</seg>s</w>, — <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l</w> <w n="6.5">n</w>'<w n="6.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="8">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="6.7" punct="dp:12">p<seg phoneme="u" type="vs" value="1" rule="424" place="11">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280" place="12" punct="dp">oi</seg></w> :</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="7.3">m<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>t</w> <w n="7.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>l</w> <w n="7.5" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>t</w>, <w n="7.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>t</w>-<w n="7.7" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8" punct="vg">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.8"><seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg></w> <w n="7.9">p<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>r</w> <w n="7.10">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11">e</seg>s</w> <w n="7.11" punct="pe:12">l<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ti">e</seg>s</w> ! —</l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>st<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="8.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="8.5">ch<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="8.6">l</w>'<w n="8.7"><seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>m<seg phoneme="o" type="vs" value="1" rule="414" place="9">ô</seg>ni<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg></w> <w n="8.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="11">u</seg></w> <w n="8.9" punct="vg:12">r<seg phoneme="wa" type="vs" value="1" rule="422" place="12" punct="vg">oi</seg></w>,</l>
						<l n="9" num="2.5" lm="12"><w n="9.1">L</w>'<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>dj<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="9.3">d</w>'<w n="9.4"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>bt<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="9.6" punct="pt:9">gr<seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="pt">e</seg></w>. <w n="9.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg></w> <w n="9.8" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="424" place="11">ou</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="10" num="2.6" lm="12"><w n="10.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="10.2" punct="vg:4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>fr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="10.3">l</w>'<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="338" place="5">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="10.5">fr<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="9">en</seg>t</w> <w n="10.6" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="11">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg></w>,</l>
						<l n="11" num="2.7" lm="12"><w n="11.1">Lu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="11.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t</w> <w n="11.3">d</w>'<w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="11.5">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6">œu</seg>r</w> <w n="11.6" punct="vg:8">l<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></w>, <w n="11.7" punct="vg:9">c<seg phoneme="a" type="vs" value="1" rule="339" place="9" punct="vg">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="11.8" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="10">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="12" num="2.8" lm="12">« <w n="12.1">S</w>'<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="12.4">p<seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" punct="vg">o</seg>rt</w>, <w n="12.7">c</w>'<w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="12.9">qu</w>'<w n="12.10"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>l</w> <w n="12.11">l</w>'<w n="12.12"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="12.13" punct="pt:12">m<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="pt">é</seg></w>.</l>
						<l n="13" num="2.9" lm="12">» <w n="13.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3" punct="pe:4">cl<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" punct="pe">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">Au</seg></w> <w n="13.5">r<seg phoneme="wa" type="vs" value="1" rule="422" place="6">oi</seg></w> <w n="13.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="13.8">d<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>s</w> <w n="13.9" punct="dp:12">n<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="14" num="2.10" lm="12">» <w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="14.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="14.5" punct="vg:6">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg">ou</seg>s</w>, <w n="14.6" punct="vg:8">M<seg phoneme="œ" type="vs" value="1" rule="150" place="7">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="8" punct="vg">eu</seg>r</w>, <w n="14.7">c</w>'<w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="9">e</seg>st</w> <w n="14.9"><seg phoneme="y" type="vs" value="1" rule="452" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="14.10">gu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="15" num="2.11" lm="12">» <w n="15.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="15.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="15.3" punct="pe:4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="pe ti in">an</seg>g</w> ! » — « <w n="15.4" punct="vg:6">M<seg phoneme="œ" type="vs" value="1" rule="150" place="5">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="6" punct="vg">eu</seg>r</w>, <w n="15.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t</w> <w n="15.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="15.7" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>st<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="vg">eu</seg>r</w>, <w n="15.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="15.9">v<seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg></w></l>
						<l n="16" num="2.12" lm="12">» <w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="16.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="16.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.7">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="16.8">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="10">eu</seg></w> <w n="16.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="16.10" punct="pe:12">m<seg phoneme="wa" type="vs" value="1" rule="422" place="12" punct="pe ps">oi</seg></w> !… »</l>
						<l n="17" num="2.13" lm="12"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="17.3" punct="vg:3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" punct="vg">ai</seg></w>, <w n="17.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5">n</w>'<w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="17.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="17.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.9">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg></w> <w n="17.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="17.11">l</w>’<w n="17.12" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="408" place="10">É</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="18" num="2.14" lm="12"><w n="18.1">C</w>'<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.4">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.6" punct="pv:6">Gu<seg phoneme="i" type="vs" value="1" rule="484" place="5">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317" place="6" punct="pv">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="18.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="18.8">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg></w> <w n="18.9">p<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>r</w> <w n="18.10">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10">e</seg>s</w> <w n="18.11" punct="pe:12"><seg phoneme="y" type="vs" value="1" rule="449" place="11">u</seg>hl<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="12" punct="pe">an</seg>s</w> !</l>
						<l n="19" num="2.15" lm="12"><w n="19.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="19.4">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="19.5">p<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</w> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="19.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="19.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>g</w> <w n="19.9"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="19.10">d</w>'<w n="19.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="20" num="2.16" lm="12"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="20.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2">ain</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="365" place="3">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="20.4">r<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s</w> <w n="20.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>r</w> <w n="20.6">l<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>rs</w> <w n="20.7">tr<seg phoneme="o" type="vs" value="1" rule="414" place="9">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="20.8" punct="pe:12">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pe">an</seg>ts</w> !</l>
						<l n="21" num="2.17" lm="12"><w n="21.1">C</w>'<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.4">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="21.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="21.6" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg">a</seg>ts</w>, <w n="21.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="21.8">m<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>rtr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.9"><seg phoneme="e" type="vs" value="1" rule="188" place="9">e</seg>t</w> <w n="21.10">d<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg></w> <w n="21.11" punct="vg:12">p<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="22" num="2.18" lm="12"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="22.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="22.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.5"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="22.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9">e</seg>s</w> <w n="22.7" punct="pv:12">pr<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>s<seg phoneme="o" type="vs" value="1" rule="434" place="11">o</seg>nni<seg phoneme="e" type="vs" value="1" rule="346" place="12" punct="pv">er</seg>s</w> ;</l>
						<l n="23" num="2.19" lm="12"><w n="23.1">C</w>'<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.4">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="23.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.6">Gu<seg phoneme="i" type="vs" value="1" rule="484" place="5">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="23.8">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>t</w> <w n="23.9"><seg phoneme="a" type="vs" value="1" rule="341" place="9">à</seg></w> <w n="23.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg></w> <w n="23.11" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="466" place="11">i</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="24" num="2.20" lm="12"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="24.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="24.3">m<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="24.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6">en</seg></w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="9">e</seg>ils</w> <w n="24.7" punct="pe:12"><seg phoneme="o" type="vs" value="1" rule="317" place="10">au</seg>m<seg phoneme="o" type="vs" value="1" rule="414" place="11">ô</seg>ni<seg phoneme="e" type="vs" value="1" rule="346" place="12" punct="pe">er</seg>s</w> !</l>
					</lg>
					<lg n="3">
						<l n="25" num="3.1" lm="12"><w n="25.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="25.3">s</w>'<w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="25.5">r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="25.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t</w> <w n="25.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="25.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10">an</seg>s</w> <w n="25.9">l</w>'<w n="25.10" punct="dp:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>p<seg phoneme="o" type="vs" value="1" rule="414" place="12">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="26" num="3.2" lm="12"><w n="26.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="26.2" punct="vg:2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" punct="vg">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="26.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="26.5" punct="pe:6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pe ps">an</seg>g</w> !… <w n="26.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="26.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="26.8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg></w> <w n="26.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="26.10" punct="pe:12">v<seg phoneme="o" type="vs" value="1" rule="414" place="12">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>