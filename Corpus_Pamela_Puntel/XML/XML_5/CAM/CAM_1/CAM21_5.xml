<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM21" modus="cp" lm_max="13">
				<head type="main">LA MORT POUR LA PATRIE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Murioï osa te phulla kaï anthea gignetaï ôrê</quote>
							<bibl>
								<name>(HOMÈRE)</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342" place="1">a</seg><seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="1.7">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>r</w> <w n="1.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.9" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="13"><w n="2.1">H<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="y" type="vs" value="1" rule="390" place="3">eu</seg>t</w> <w n="2.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5" punct="vg:7">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="vg">ou</seg>s</w>, <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="414" place="8">ô</seg></w> <w n="2.7">j<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="2.8" punct="vg:13">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="13" punct="vg">an</seg>ts</w>,</l>
						<l n="3" num="1.3" lm="12">« <w n="3.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="3.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">fl<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="3.6">f<seg phoneme="œ" type="vs" value="1" rule="405" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="3.7"><seg phoneme="o" type="vs" value="1" rule="317" place="10">au</seg></w> <w n="3.8" punct="pt:12">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12" punct="pt">em</seg>ps</w>. »</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="4.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="4.3">cr<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>l</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg></w> <w n="4.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8">en</seg></w> <w n="4.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>t</w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg></w> <w n="4.8" punct="pe:12">pr<seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="5.2">j<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="351" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="5.4" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg">é</seg></w>, <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418" place="9">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg></w> <w n="5.8" punct="vg:12">j<seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="6.2">v<seg phoneme="a" type="vs" value="1" rule="306" place="2">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ts</w> <w n="6.3" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4" punct="vg">œu</seg>rs</w>, <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="6.7">d</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>r</w> <w n="6.9" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg>ts</w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="4">ez</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="7.4" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="188" place="7" punct="vg">e</seg>t</w>, <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>s</w> <w n="7.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="7.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg>ts</w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">f<seg phoneme="ɛ" type="vs" value="1" rule="63" place="2">e</seg>r</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="8.5">v<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="8.6" punct="vg:6">ch<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="vg">ai</seg>rs</w>, <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="8.8">m<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="306" place="9">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="8.9">v<seg phoneme="u" type="vs" value="1" rule="424" place="11">ou</seg>s</w> <w n="8.10" punct="pt:12">br<seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg>ts</w>, <w n="9.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="9.4">l<seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="9.6">r<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>v<seg phoneme="i" type="vs" value="1" rule="481" place="9">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="341" place="10">à</seg></w> <w n="9.8">v<seg phoneme="o" type="vs" value="1" rule="437" place="11">o</seg>s</w> <w n="9.9" punct="vg:12">y<seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="vg">eu</seg>x</w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w>-<w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t</w> <w n="10.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>ts</w> <w n="10.5">h<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="10.6">d</w>’<w n="10.7"><seg phoneme="o" type="vs" value="1" rule="317" place="10">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="10.8" punct="pi:12">ci<seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="pi">eu</seg>x</w> ?</l>
						<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>bs<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rb<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w>-<w n="11.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="11.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="11.7">n<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.8" punct="pi:12"><seg phoneme="i" type="vs" value="1" rule="466" place="11">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1">N</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="12.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="12.7">s<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="8">ein</seg>t</w> <w n="12.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9">an</seg>s</w> <w n="12.10">l</w>’<w n="12.11" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10" punct="vg">ai</seg>r</w>, <w n="12.12"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="11">un</seg></w> <w n="12.13">fl<seg phoneme="o" type="vs" value="1" rule="437" place="12">o</seg>t</w></l>
						<l n="13" num="4.2" lm="12"><w n="13.1">P<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="13.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="13.3">l</w>’<w n="13.4" punct="pi:6"><seg phoneme="o" type="vs" value="1" rule="443" place="4">O</seg>c<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6" punct="pi">an</seg></w> ? <w n="13.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7">Ou</seg></w> <w n="13.6">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w>-<w n="13.7">n<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>s</w> <w n="13.8">pl<seg phoneme="y" type="vs" value="1" rule="449" place="11">u</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="12">ô</seg>t</w></l>
						<l n="14" num="4.3" lm="12"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="y" type="vs" value="1" rule="452" place="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.3"><seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4">v<seg phoneme="i" type="vs" value="1" rule="481" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg></w> <w n="14.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="8">ein</seg></w> <w n="14.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg></w> <w n="14.8">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="10">eu</seg></w> <w n="14.9" punct="pi:12">c<seg phoneme="o" type="vs" value="1" rule="443" place="11">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="15" num="1.1" lm="11"><w n="15.1">Pl<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="15.4" punct="vg:6">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg></w>, <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.6">Chr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>st</w> <w n="15.7" punct="dp:11">r<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="11" punct="dp">é</seg></w> :</l>
						<l n="16" num="1.2" lm="12"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="16.2">v<seg phoneme="i" type="vs" value="1" rule="481" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pt">a</seg>t</w>. <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="16.8">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>rt</w> <w n="16.9">l<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg></w> <w n="16.10" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="424" place="11">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="17" num="1.3" lm="12"><w n="17.1">L</w>’<w n="17.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="17.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="17.5" punct="vg:6">ci<seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="vg">eu</seg>x</w>, <w n="17.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="17.8">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>rps</w> <w n="17.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="18" num="1.4" lm="12"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="18.3">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="18.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg></w> <w n="18.5">d</w>’<w n="18.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="18.7">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="8">u</seg><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="18.8" punct="pt:12">v<seg phoneme="wa" type="vs" value="1" rule="419" place="11">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="19" num="2.1" lm="12"><w n="19.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="19.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>x</w> <w n="19.4">d</w>’<w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="19.6" punct="vg:6">h<seg phoneme="o" type="vs" value="1" rule="317" place="6" punct="vg">au</seg>t</w>, <w n="19.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>r</w> <w n="19.9"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg></w> <w n="19.10" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg></w>,</l>
						<l n="20" num="2.2" lm="12"><w n="20.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="20.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="20.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="20.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg></w> <w n="20.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="20.8">bru<seg phoneme="i" type="vs" value="1" rule="490" place="10">i</seg>t</w> <w n="20.9" punct="pv:12">r<seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="21" num="2.3" lm="12"><w n="21.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="21.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg></w> <w n="21.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="21.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="4">en</seg>s</w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="21.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="21.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>r</w> <w n="21.8">l</w>’<w n="21.9" punct="dp:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">em</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="22" num="2.4" lm="12"><w n="22.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="22.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd</w> <w n="22.3">v<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="22.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="22.5">h<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>t</w> <w n="22.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="22.8">d<seg phoneme="o" type="vs" value="1" rule="414" place="9">ô</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="419" place="11">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="23" num="3.1" lm="12"><w n="23.1">D<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w>-<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="23.4">j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="23.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="23.6"><seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="23.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="23.8" punct="pi:12">f<seg phoneme="o" type="vs" value="1" rule="433" place="12">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
						<l n="24" num="3.2" lm="12"><w n="24.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="24.2">t<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="24.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.4">d</w>’<w n="24.5" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="6" punct="vg">o</seg>r</w>, <w n="24.6"><seg phoneme="o" type="vs" value="1" rule="414" place="7">ô</seg></w> <w n="24.7" punct="vg:9">Pl<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" punct="vg">on</seg></w>, <w n="24.8">n</w>’<w n="24.9"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="10">e</seg>st</w> <w n="24.10">p<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>s</w> <w n="24.11" punct="pv:12">f<seg phoneme="o" type="vs" value="1" rule="317" place="12">au</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="25" num="3.3" lm="12"><w n="25.1">T<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="25.2">pr<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="351" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="25.3" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="414" place="7">ô</seg></w> <w n="25.5">Chr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>st</w> <w n="25.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="25.7">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="25.8" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="pt">a</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="26" num="4.1" lm="12"><w n="26.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="26.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="26.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ts</w> <w n="26.4">fl<seg phoneme="ø" type="vs" value="1" rule="404" place="5">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="26.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="26.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg></w> <w n="26.8" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="492" place="12">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="27" num="4.2" lm="12"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="27.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="27.3"><seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="27.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="27.6">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg></w> <w n="27.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="27.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10">e</seg>s</w> <w n="27.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="28" num="4.3" lm="12"><w n="28.1">L<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>r</w> <w n="28.2">h<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>r<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg><seg phoneme="i" type="vs" value="1" rule="476" place="4">ï</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="6">ain</seg>t</w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="28.5">s<seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="28.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg></w> <w n="28.7" punct="pt:12">tr<seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="pt">a</seg>s</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>