<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES DOULEURS DE LA GUERRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VRM">
					<name>
						<forename>Louis-Lucien</forename>
						<surname>VERMEIL</surname>
					</name>
					<date from="1833" to="1901">1833-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>620 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VRM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES DOULEURS DE LA GUERRE</title>
						<author>LOUIS-LUCIEN VERMEIL</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=l7xDAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES DOULEURS DE LA GUERRE</title>
								<author>LOUIS-LUCIEN VERMEIL</author>
								<imprint>
									<pubPlace>LAUSANNE</pubPlace>
									<publisher>LIBRAIRIE BLANC IMER et LEBET</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VRM3" modus="cm" lm_max="12">
				<head type="number">III</head>
				<head type="main">LE BIVAC</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="1.3">l</w>'<w n="1.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="1.6" punct="pv:6">b<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pv">a</seg>c</w> ; <w n="1.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.8">p<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>mm<seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">R<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="2.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ts</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="3.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="3.3">f<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="3.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="3.6" punct="pt:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="6" punct="pt">en</seg></w>. <w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">On</seg></w> <w n="3.8">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="3.9">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10">e</seg>s</w> <w n="3.10" punct="pv:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="pv">a</seg>ts</w> ;</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>s</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="4.3">m<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>t</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="4.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6" punct="vg">en</seg>ts</w>, <w n="4.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l</w> <w n="4.7">f<seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>t</w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346" place="11">er</seg></w> <w n="4.9" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="pt">a</seg>s</w>.</l>
					<l n="5" num="1.5" lm="12"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="5.2">h<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>ts</w> <w n="5.3" punct="vg:3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3" punct="vg">ai</seg>ts</w>, <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="5.5" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>xpl<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="vg">oi</seg>ts</w>, <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.9" punct="dp:12">br<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>v<seg phoneme="u" type="vs" value="1" rule="424" place="12">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
					<l n="6" num="1.6" lm="12"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4" punct="ps:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg ps">er</seg></w>,… <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="6.6">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rcl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.7">v<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>s</w> <w n="6.8" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11">en</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="12">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="7" num="1.7" lm="12"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="7.2">s</w>'<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>n<seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="7.5">r<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="7.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg></w> <w n="7.9">v<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="12">an</seg></w></l>
					<l n="8" num="1.8" lm="12"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="8.3">l</w>'<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.8">s<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="o" type="vs" value="1" rule="317" place="11">au</seg></w> <w n="8.10" punct="pt:12">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg>g</w>.</l>
					<l n="9" num="1.9" lm="12"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="9.2">l</w>'<w n="9.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="9.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="9.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="9.6">d</w>'<w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8">un</seg></w> <w n="9.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>scr<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t</w> <w n="9.9">s</w>'<w n="9.10" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="10" num="1.10" lm="12"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="10.2">V<seg phoneme="a" type="vs" value="1" rule="306" place="2">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="10.4">l</w>'<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>r</w> <w n="10.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="10.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10">an</seg>s</w> <w n="10.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11">on</seg></w> <w n="10.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="11" num="1.11" lm="12">« <w n="11.1">C<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>x</w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="11.3">t<seg phoneme="y" type="vs" value="1" rule="444" place="3">û</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">mi<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="11.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="11.8">br<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11">on</seg>t</w> <w n="11.9">p<seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>s</w></l>
					<l n="12" num="1.12" lm="12">« <w n="12.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="12.3">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>x</w> <w n="12.4">d</w>'<w n="12.5" punct="pe:6">h<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="pe ps">eu</seg>r</w> !… <w n="12.6" punct="vg:8">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>scr<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>ts</w>, <w n="12.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="346" place="10">ez</seg></w> <w n="12.8"><seg phoneme="o" type="vs" value="1" rule="317" place="11">au</seg></w> <w n="12.9" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="pe">a</seg>s</w> ! »</l>
					<l n="13" num="1.13" lm="12"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="13.2" punct="vg:2">b<seg phoneme="wa" type="vs" value="1" rule="419" place="2" punct="vg">oi</seg>t</w>, <w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="13.4">s</w>'<w n="13.5"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="13.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg></w> <w n="13.9">j<seg phoneme="wa" type="vs" value="1" rule="439" place="11">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="402" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="14" num="1.14" lm="12"><w n="14.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="1">en</seg>t</w> <w n="14.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="14.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>ts</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="14.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>d</w> <w n="14.7">l</w>'<w n="14.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.9" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="402" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="15" num="1.15" lm="12"><w n="15.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="15.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="15.4">l</w>'<w n="15.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="15.6" punct="pv:6">r<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pv">i</seg>t</w> ; <w n="15.7">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="15.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="15.9">m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.10"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="15.11">l</w>'<w n="15.12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>t</w></l>
					<l n="16" num="1.16" lm="12"><w n="16.1">Pl<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="16.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="16.3">l<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="16.4">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="16.5" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="vg">oi</seg>t</w>, <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="437" place="9">o</seg>ts</w> <w n="16.8" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="11">ou</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pe">an</seg>t</w> !</l>
					<l n="17" num="1.17" lm="12"><w n="17.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="17.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="17.4">l</w>'<w n="17.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="17.6" punct="pv:6">r<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pv">i</seg>t</w> ; <w n="17.7">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="17.8">l<seg phoneme="a" type="vs" value="1" rule="341" place="8">à</seg></w> <w n="17.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9">an</seg>s</w> <w n="17.10">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10">e</seg>s</w> <w n="17.11">c<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="11">oin</seg>s</w> <w n="17.12">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="18" num="1.18" lm="12"><w n="18.1">G<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="18.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="18.3">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rps</w> <w n="18.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>ts</w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg></w> <w n="18.6">m<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="397" place="9">eu</seg></w> <w n="18.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10">e</seg>s</w> <w n="18.8" punct="pe:12">d<seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg>s</w> !</l>
					<l n="19" num="1.19" lm="12"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="19.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="19.4">l</w>'<w n="19.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="19.6" punct="tc:6">r<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pv ti">i</seg>t</w> ; — <w n="19.7">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="19.8">p<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>t</w> <w n="19.9"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="19.10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12">ain</seg></w></l>
					<l n="20" num="1.20" lm="12"><w n="20.1">D</w>'<w n="20.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="20.3">r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="20.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="20.5" punct="pe:6">v<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pe">ou</seg>s</w> !<w n="20.6">S<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt</w> <w n="20.7" punct="vg:9">b<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="339" place="9" punct="vg">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="20.8" punct="pe:12"><seg phoneme="i" type="vs" value="1" rule="466" place="10">i</seg>nh<seg phoneme="y" type="vs" value="1" rule="452" place="11">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12" punct="pe">ain</seg></w> !</l>
				</lg>
			</div></body></text></TEI>