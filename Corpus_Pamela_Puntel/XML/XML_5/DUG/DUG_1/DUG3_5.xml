<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG3" modus="sp" lm_max="8">
				<head type="main">EN AVANT</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="1.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="1.6" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>t<seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="vg">eau</seg></w>,</l>
					<l n="2" num="1.2" lm="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="2.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="304" place="3">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="2.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="2.6" punct="vg:8">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="8"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.3">ch<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="3.6" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="339" place="7">â</seg>t<seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="vg">eau</seg></w>,</l>
					<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5">dr<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="8">eau</seg></w></l>
					<l n="5" num="1.5" lm="4"><space unit="char" quantity="8"></space><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="5.3" punct="pe:4">M<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe ps">e</seg></w> !…</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1" lm="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rm<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="6.3" punct="pe:6">M<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="pe">e</seg></w> ! <w n="6.4">d<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="6.5">s<seg phoneme="œ" type="vs" value="1" rule="248" place="8">œu</seg>rs</w></l>
					<l n="7" num="2.2" lm="8"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="7.3">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="7.4">m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t</w> <w n="7.5"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6">s<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="8" num="2.3" lm="8"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="8.2">M<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rs<seg phoneme="e" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.3">qu</w>'<w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ccl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="9" num="2.4" lm="8"><w n="9.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="2">en</seg>t</w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r</w> <w n="9.3">v<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="9.4">br<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="9.5" punct="pe:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>g<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pe ps">eu</seg>rs</w> !…</l>
					<l n="10" num="2.5" lm="8"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="10.3">p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>x</w> <w n="10.4"><seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="10.6" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="11" num="2.6" lm="8"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="11.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>s</w> <w n="11.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="11.5" punct="ps:8">l<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</w>…</l>
					<l n="12" num="2.7" lm="8"><w n="12.1">L</w>'<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="12.3">p<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="12.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="12.6">b<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="12.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="13" num="2.8" lm="8"><w n="13.1">L</w>'<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="13.4">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="4">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="5">y</seg>s</w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="13.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>ds</w> <w n="13.7" punct="pe:8">c<seg phoneme="œ" type="vs" value="1" rule="248" place="8" punct="pe ps">œu</seg>rs</w> !…</l>
				</lg>
				<lg n="3">
					<l n="14" num="3.1" lm="8"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2">en</seg>t</w> <w n="14.2">tr<seg phoneme="o" type="vs" value="1" rule="432" place="3">o</seg>p</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t</w></l>
					<l n="15" num="3.2" lm="8"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="15.2">n<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="15.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="15.5" punct="vg:8">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="16" num="3.3" lm="8"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="16.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="16.4">l</w>'<w n="16.5"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="442" place="5">o</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.6"><seg phoneme="u" type="vs" value="1" rule="425" place="6">où</seg></w> <w n="16.7">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="16.8">s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="17" num="3.4" lm="8"><w n="17.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">br<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="17.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.7" punct="pe:8">gr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>t</w> !</l>
					<l n="18" num="3.5" lm="8"><w n="18.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="18.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>ds</w> <w n="18.3">ch<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>cs</w> <w n="18.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>h<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="18.5">l</w>'<w n="18.6" punct="vg:8">h<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="19" num="3.6" lm="8"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="19.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l</w> <w n="19.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d</w> <w n="19.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="19.6">v<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t</w> <w n="19.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="19.8" punct="ps:8">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
					<l n="20" num="3.7" lm="8"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="20.2" punct="vg:3">b<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>vou<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>c</w>, <w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="20.4">j<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.6" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="21" num="3.8" lm="8"><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="21.2">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="21.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="21.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="21.7" punct="pe:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe ps">i</seg>t</w> !…</l>
				</lg>
				<lg n="4">
					<l n="22" num="4.1" lm="8"><w n="22.1" punct="pe:2">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="pe">ou</seg>t</w> ! <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="22.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="22.4" punct="dp:6">cr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> : <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">à</seg></w> <w n="22.6" punct="pe:8">m<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pe">oi</seg></w> !</l>
					<l n="23" num="4.2" lm="8"><w n="23.1">J<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="351" place="2">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="23.3">m<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="23.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="24" num="4.3" lm="8"><w n="24.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">c<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="24.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="24.4">l</w>'<w n="24.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="24.6"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="25" num="4.4" lm="8"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="25.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">cr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w>-<w n="25.4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="25.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="351" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="25.6" punct="pe:8">t<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pe ps">oi</seg></w> !…</l>
					<l n="26" num="4.5" lm="8"><w n="26.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="26.3">f<seg phoneme="wa" type="vs" value="1" rule="422" place="3">oi</seg></w> <w n="26.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">a</seg>ï<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="26.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="27" num="4.6" lm="8"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="27.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="27.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="3">œu</seg>r</w> <w n="27.4" punct="vg:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg">é</seg></w>, <w n="27.5" punct="pe:8">j<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="28" num="4.7" lm="8"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="28.2">qu</w>'<w n="28.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="28.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="28.5"><seg phoneme="œ" type="vs" value="1" rule="248" place="4">œu</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="28.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="28.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="29" num="4.8" lm="8"><w n="29.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="29.2">vr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="29.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>ts</w> <w n="29.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="29.5">p<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="29.6" punct="pe:8">r<seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pe ps">oi</seg></w> !…</l>
				</lg>
				<lg n="5">
					<l n="30" num="5.1" lm="8"><w n="30.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="30.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="30.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="30.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="30.6" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>t<seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="vg">eau</seg></w>,</l>
					<l n="31" num="5.2" lm="8"><w n="31.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="31.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="304" place="3">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="31.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="31.6" punct="vg:8">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="32" num="5.3" lm="8"><w n="32.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="32.3">ch<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="32.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="32.6" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="339" place="7">â</seg>t<seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="vg">eau</seg></w>,</l>
					<l n="33" num="5.4" lm="8"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="33.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="33.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="33.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="33.5">dr<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="8">eau</seg></w></l>
					<l n="34" num="5.5" lm="4"><space unit="char" quantity="8"></space><w n="34.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="34.3" punct="pe:4">M<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe ps">e</seg></w> !…</l>
				</lg>
			</div></body></text></TEI>