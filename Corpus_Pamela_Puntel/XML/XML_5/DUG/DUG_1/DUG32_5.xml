<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG32" modus="sm" lm_max="8">
				<head type="main">AMOUR !</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="1.2">r<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="1.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>d</w> <w n="1.5">d</w>'<w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="1.7">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6">œu</seg>r</w> <w n="1.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8"><w n="2.1">L</w>'<w n="2.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>r</w>, <w n="2.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="466" place="5">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="2.5" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="vg">in</seg></w>,</l>
					<l n="3" num="1.3" lm="8"><w n="3.1" punct="vg:2"><seg phoneme="e" type="vs" value="1" rule="353" place="1">E</seg>x<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>l</w>, <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>d<seg phoneme="i" type="vs" value="1" rule="481" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="3.3"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg></w> <w n="3.4" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="4.2">l</w>'<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="4.7" punct="pv:8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pv">ain</seg></w> ;</l>
					<l n="5" num="1.5" lm="8"><w n="5.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">m<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="5.4">cr<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="5.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>s</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.8">li<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="6" num="1.6" lm="8"><w n="6.1">F<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="6.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>br<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s</w> <w n="6.4">qu</w>'<w n="6.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="7" num="1.7" lm="8"><w n="7.1">S</w>'<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="496" place="1">y</seg></w> <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>c</w> <w n="7.5">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.8">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="8" num="1.8" lm="8"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6">en</seg>t</w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="8.5" punct="pe:8">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="pe">in</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="8"><w n="9.1">B<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>lz<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>c</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="9.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="4">ein</seg>t</w> <w n="9.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="9.6">b<seg phoneme="o" type="vs" value="1" rule="314" place="7">eau</seg></w> <w n="9.7">l<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="10" num="2.2" lm="8"><w n="10.1">C<seg phoneme="ɛ" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="10.3"><seg phoneme="i" type="vs" value="1" rule="466" place="4">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="10.5">f<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l</w></l>
					<l n="11" num="2.3" lm="8"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="11.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="11.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="466" place="4">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="12" num="2.4" lm="8"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>x</w> <w n="12.2">v<seg phoneme="ɛ" type="vs" value="1" rule="304" place="2">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="5">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="12.5" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>l</w>,</l>
					<l n="13" num="2.5" lm="8"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="vg">E</seg>t</w>, <w n="13.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="13.5">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="14" num="2.6" lm="8"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="14.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="14.4">bl<seg phoneme="ɛ" type="vs" value="1" rule="351" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="14.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.7">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="14.8" punct="vg:8">t<seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="15" num="2.7" lm="8"><w n="15.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pl<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="15.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.4">st<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="16" num="2.8" lm="8"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="16.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>l</w> <w n="16.4" punct="pe:8">pi<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>st<seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe ps">a</seg>l</w> !…</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="8">— <w n="17.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg>d</w>, <w n="17.2"><seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w>-<w n="17.3" punct="pe:6">m<seg phoneme="wa" type="vs" value="1" rule="422" place="6" punct="pe">oi</seg></w> ! <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="17.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="18" num="3.2" lm="8"><w n="18.1">M</w>'<w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>tr<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="18.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>cs</w> <w n="18.5" punct="pe:8">t<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pe">eu</seg>x</w> !</l>
					<l n="19" num="3.3" lm="8"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">c<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r</w> <w n="19.4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="6">oi</seg></w> <w n="19.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="20" num="3.4" lm="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="20.2">j</w>'<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg></w> <w n="20.4">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="20.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="20.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>gs</w> <w n="20.7" punct="pe:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pe ps">eu</seg>x</w> !…</l>
					<l n="21" num="3.5" lm="8"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">su<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>s</w> <w n="21.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="21.5">j<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.6">n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="21.7" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="22" num="3.6" lm="8"><w n="22.1">L</w>'<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>r</w> <w n="22.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="22.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="22.6">p<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="22.7" punct="vg:8">fr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="23" num="3.7" lm="8"><w n="23.1">C</w>'<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="23.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt</w> <w n="23.5">s<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="23.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.7">j</w>'<w n="23.8" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></w> !…</l>
					<l n="24" num="3.8" lm="8"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="24.2" punct="tc:5">r<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="dp ti">i</seg>t</w> : — <w n="24.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.5" punct="pe:8">v<seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pe ps">eu</seg>x</w> !…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1" lm="8"><w n="25.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="25.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">v<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="25.5">fl<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="25.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="25.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="26" num="4.2" lm="8"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="26.2" punct="vg:4">m<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="26.3">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="26.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r</w></l>
					<l n="27" num="4.3" lm="8"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="27.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="27.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ds</w> <w n="27.4">y<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="27.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rcl<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>s</w> <w n="27.6">d</w>'<w n="27.7"><seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="28" num="4.4" lm="8"><w n="28.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="28.2">p<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>r</w> <w n="28.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="28.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="28.6" punct="ps:8">j<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="ps">ou</seg>r</w>…</l>
					<l n="29" num="4.5" lm="8"><w n="29.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="29.2">r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>squ<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="29.3">r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.4"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="29.5" punct="vg:8">bl<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>sph<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="30" num="4.6" lm="8"><w n="30.1">S</w>'<w n="30.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="30.3">f<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="30.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.5">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="30.6" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="31" num="4.7" lm="8"><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">t</w>'<w n="31.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s</w> <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="31.5">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg></w> <w n="31.6">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="32" num="4.8" lm="8"><w n="32.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="32.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="32.3">c<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="32.5" punct="pe:8">v<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe ti">ou</seg>r</w> ! —</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1" lm="8"><w n="33.1" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg></w>, <w n="33.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="33.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>d<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="34" num="5.2" lm="8"><w n="34.1">C<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rps</w> <w n="34.2">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="34.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="34.4">c<seg phoneme="œ" type="vs" value="1" rule="248" place="5">œu</seg>r</w> <w n="34.5" punct="vg:8"><seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>lc<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></w>,</l>
					<l n="35" num="5.3" lm="8"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1">É</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="383" place="2">ei</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="35.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r</w> <w n="35.3">t<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="35.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>r</w> <w n="35.5">m<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="36" num="5.4" lm="8"><w n="36.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="36.2">pl<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="36.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="36.4">dr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg></w> <w n="36.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></w>,</l>
					<l n="37" num="5.5" lm="8"><w n="37.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="37.2">t</w>'<w n="37.3"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="2">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="37.5" punct="pe:8">d<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="38" num="5.6" lm="8"><w n="38.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="38.2" punct="pe:5">fr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="481" place="5" punct="pe">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ! <w n="38.3"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="7">e</seg>c</w> <w n="38.4" punct="pe:8">r<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="39" num="5.7" lm="8"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="39.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="39.3">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4">en</seg></w> <w n="39.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="39.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="39.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="40" num="5.8" lm="8"><w n="40.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="40.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="40.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>rs</w> <w n="40.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="6">en</seg>t</w> <w n="40.5" punct="pe:8">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pe ps">é</seg></w> !…</l>
				</lg>
			</div></body></text></TEI>