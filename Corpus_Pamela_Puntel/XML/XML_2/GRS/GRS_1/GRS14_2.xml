<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'ANNÉE MAUDITE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="GRS">
					<name>
						<forename>Charles</forename>
						<surname>GRANDSARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2146 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'ANNÉE MAUDITE 1870-1871</title>
						<author>Charles Grandsard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61285325.r=GRANDSARD%20L%27ANN%C3%89E%20MAUDITE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'ANNÉE MAUDITE 1870-1871</title>
								<author>Charles Grandsard</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE DU PETIT JOURNAL</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRS14">
				<head type="main">LES FAUX FRANÇAIS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="1.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.7">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.8"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="2.7">f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">P<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w>, <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="3.5">m<seg phoneme="wa" type="vs" value="1" rule="191">oe</seg>ll<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="3.9">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">S</w>'<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="4.4">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.6">v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.8">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.9">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="5.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="5.5">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.6">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4">fl<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="6.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.7">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="7.4">fr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t</w> <w n="7.7">v<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lg<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>dr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.5">tr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="8.6">d</w>'<w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, : « <w n="9.5">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.7">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="9.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="9.9">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> !</l>
					<l n="10" num="3.2"><w n="10.1">Pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="10.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ! <w n="10.5">Pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.7">l</w>'<w n="10.8">H<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ! »</l>
					<l n="11" num="3.3"><w n="11.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="11.3">qu</w>'<w n="11.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="11.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="11.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="11.8">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="11.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
					<l n="12" num="3.4"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="12.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.5">l<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="12.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.8">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="13.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="13.5">d</w>'<w n="13.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="13.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rb<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.3">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="14.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w></l>
					<l n="15" num="4.3"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="15.2"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="15.5">l</w>'<w n="15.6">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.7"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="15.8">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.9">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="16.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="16.5">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="16.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="16.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.8">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.9">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">C<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="17.3">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="17.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.6">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="18" num="5.2"><w n="18.1">S<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="18.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="18.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.8">l</w>'<w n="18.9"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="19.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="19.3">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="19.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="19.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="19.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.8"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="19.9">t<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="20" num="5.4"><w n="20.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="20.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="20.5">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="20.6">qu</w>'<w n="20.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.8">v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="20.9">d</w>'<w n="20.10"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">C<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>-<w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="21.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="21.7"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="22.2">n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="22.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="22.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w>, <w n="22.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="22.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="22.7">d</w>'<w n="22.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>,</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="23.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="23.5">l</w>'<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.7">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="23.9">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>…</l>
					<l n="24" num="6.4"><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="24.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="24.3">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="24.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> : <w n="24.7">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.8">n</w>'<w n="24.9"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="24.10">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="24.11"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="24.12">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>