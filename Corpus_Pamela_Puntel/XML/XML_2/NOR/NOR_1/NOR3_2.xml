<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR3">
				<head type="main">L’EXILÉE</head>
				<opener>
					<dateline>
						<date when="1870">Septembre 1870.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424">OU</seg>RQU<seg phoneme="wa" type="vs" value="1" rule="280">OI</seg></w> <w n="1.2">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>-<w n="1.3">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>, <w n="1.4">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.5">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="2.3">b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd</w> <w n="2.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.6">ch<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ?</l>
					<l n="3" num="1.3"><w n="3.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="3.3"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="3.4">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.5">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="3.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.5">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ;</l>
					<l n="5" num="1.5"><w n="5.1">T<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="5.3"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="5.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="5.5">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="5.6">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="5.7">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.3">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="6.4">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="6.6">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w>-<w n="7.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.6">t</w>’<w n="7.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.8">bl<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
					<l n="8" num="2.2"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>s</w>-<w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.5">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w></l>
					<l n="9" num="2.3"><w n="9.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.2">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="9.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="9.5">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="10" num="2.4"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.3">g<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.4">fr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>d</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="10.6">m<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ?</l>
					<l n="11" num="2.5"><w n="11.1">T</w>’<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="11.3">t</w>-<w n="11.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="11.6">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="12" num="2.6"><w n="12.1">C<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>-<w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="12.4">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.5">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="12.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.7">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> ?</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w>-<w n="13.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.5">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.6">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="13.7">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> ?</l>
					<l n="14" num="3.2"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w>-<w n="14.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="14.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ?</l>
					<l n="15" num="3.3"><w n="15.1">S<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="15.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="15.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="15.5">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
					<l n="16" num="3.4"><w n="16.1">Gr<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="o" type="vs" value="1" rule="434">o</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="16.3">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.5">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>,</l>
					<l n="17" num="3.5"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="17.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="17.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>gu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="17.5">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
					<l n="18" num="3.6"><w n="18.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="18.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="18.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="18.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>br<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ?</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="19.3">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="49">e</seg>s</w>-<w n="19.5">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="19.6">v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
					<l n="20" num="4.2"><w n="20.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="20.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.4">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="20.5">t</w>’<w n="20.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ?</l>
					<l n="21" num="4.3"><w n="21.1">V<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="21.2">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>-<w n="21.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.4">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> : <w n="21.5">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="21.6">qu</w>’<w n="21.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.8">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.9">v<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="22" num="4.4"><w n="22.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.3">s</w>’<w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="22.5">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="22.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
					<l n="23" num="4.5"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="23.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="23.3">t</w>’<w n="23.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="23.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="23.6">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="24" num="4.6"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="24.2">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="24.5">t</w>’<w n="24.6"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">V<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> : <w n="25.2">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>’<w n="25.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="25.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.5">f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="26" num="5.2"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>pt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="26.3">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="26.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="26.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> ;</l>
					<l n="27" num="5.3"><w n="27.1">R<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="27.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="27.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.5">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
					<l n="28" num="5.4"><w n="28.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="28.2">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="28.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="28.4">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>dr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.5">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>,</l>
					<l n="29" num="5.5"><w n="29.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="29.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="29.4">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="29.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.6">t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="30" num="5.6"><w n="30.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="30.2">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="30.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="30.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="30.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="30.6">t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">— <w n="31.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="31.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="31.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>gu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="31.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="32" num="6.2"><w n="32.1">Qu</w>’<w n="32.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="32.3">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="32.4">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="32.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="32.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w></l>
					<l n="33" num="6.3"><w n="33.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="33.2">j</w>’<w n="33.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="33.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="33.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="33.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="34" num="6.4"><w n="34.1">D</w>’<w n="34.2"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="34.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="34.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="34.5">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> :</l>
					<l n="35" num="6.5"><w n="35.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="35.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="35.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="35.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="35.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="35.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="36" num="6.6"><w n="36.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="36.2">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="36.3">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="36.4">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="36.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="36.6">gu<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="37.2">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.3">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="37.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="37.5">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.6">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
					<l n="38" num="7.2"><w n="38.1">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="38.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="38.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="38.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="38.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
					<l n="39" num="7.3"><w n="39.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="39.2">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>-<w n="39.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="39.4">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="39.6">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="39.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ffl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="40" num="7.4"><w n="40.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg></w> <w n="40.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="40.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="40.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="40.5">v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="40.6">d</w>’<w n="40.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
					<l n="41" num="7.5"><w n="41.1">F<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="41.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="41.3">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="41.4">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="41.5">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="42" num="7.6"><w n="42.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="42.2">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="42.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="42.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>gm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="43.2">f<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="43.3">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="43.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="43.5"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="44" num="8.2"><w n="44.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="44.2">Gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="44.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="44.4">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="44.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> :</l>
					<l n="45" num="8.3"><w n="45.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="45.2">n</w>’<w n="45.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="45.4">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="45.5">f<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="45.6">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="45.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
					<l n="46" num="8.4"><w n="46.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="46.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="46.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="46.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="46.5">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>j<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
					<l n="47" num="8.5"><w n="47.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="47.2">c<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="47.3">d</w>’<w n="47.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="47.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
					<l n="48" num="8.6"><w n="48.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="48.2">m</w>’<w n="48.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="48.4">l</w>’<w n="48.5">H<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
				</lg>
			</div></body></text></TEI>