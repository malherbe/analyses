<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR29">
				<head type="main">A L’ARMÉE DE FRANCE</head>
				<opener>
					<dateline>
						<date when="1871">30 mai 1871.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">ON</seg></w> <w n="1.2">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.3">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="1.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="1.6">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.7">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="2.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="2.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.7">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.8">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="2.9">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="3.2">c</w>'<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="3.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="3.7">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.8">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="3.9">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.10">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="4.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="4.7">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> <w n="4.8">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="4.10">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="5.2">c<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.4">l<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="5.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.7">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="5.8">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.9">c<seg phoneme="i" type="vs" value="1" rule="492">y</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w></l>
					<l n="6" num="2.2"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>’<w n="6.5">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> ; <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.9">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="339">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="7.5">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.7">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.3">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="8.5">fl<seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="9.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.3">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> <w n="9.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.5">t<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.8">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="9.9">cr<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.3">v<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="10.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> ; <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="10.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="10.9">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
					<l n="11" num="3.3"><w n="11.1">S<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.2">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="11.3">c</w>’<w n="11.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="11.5">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.8"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="12.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="12.6">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.9">f<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="13" num="4.2"><w n="13.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="13.7">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="13.8">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.10">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="14" num="4.3"><w n="14.1">V<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="14.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="14.3">n<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="14.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="14.7">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="14.8">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> : <w n="14.9">M<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> !</l>
				</lg>
			</div></body></text></TEI>