<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR11">
				<head type="main">BATAILLE DE CHAMPIGNY</head>
				<opener>
					<dateline>
						<date when="1870">Plateau d’Avron, 2 décembre.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">ON</seg></w> <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="1.3">qu</w>’<w n="1.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="1.7">f<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.9">qu</w>’<w n="1.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.11">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.12">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.13">l</w>'<w n="1.14"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> !</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>gs</w> <w n="2.3">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ts</w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rds</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w></l>
					<l n="3" num="1.3"><w n="3.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="3.2">tr<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="3.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="3.4">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w> <w n="3.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.7">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="3.8">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.9">j<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w>-<w n="4.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="4.6">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.7">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.8">c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="4.9">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
					<l n="5" num="1.5"><w n="5.1">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="5.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.4">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="5.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="5.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="339">â</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
					<l n="6" num="1.6"><w n="6.1">L<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>-<w n="6.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="6.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>, <w n="6.5">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="6.6">s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="6.7">d</w>’<w n="6.8"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tz</w> ?</l>
				</lg>
			</div></body></text></TEI>