<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR20">
				<head type="main">SAINT-CLOUD</head>
				<opener>
					<dateline>
						<date when="1871">Mars 1871.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="vi" value="1" rule="347">E</seg></w> <w n="1.2">s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>f</w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.5">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d</w> <w n="1.6">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.9">r<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.3">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.5">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rs</w> <w n="2.6"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.8">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.10">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.4">n<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.6">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.7">c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="3.8">bl<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="3.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="4.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="4.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="4.5">f<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="5.2">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>, <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.5">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="5.8">l</w>’<w n="5.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> :</l>
					<l n="6" num="2.2"><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="6.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="6.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>, <w n="6.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.9">f<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rc<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="7.6">l</w>'<w n="7.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="8" num="2.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="8.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.2">p<seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="8.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.4">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.6">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.8">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="9.9">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.10">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="10.2">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.6">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="10.7">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.8">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
					<l n="11" num="3.3"><w n="11.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="11.2">S<seg phoneme="o" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="440">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="11.3">G<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rrh<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="11.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.6">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="11.7">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w></l>
					<l n="12" num="3.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="12.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="12.2"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.4">c<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.3">s</w>’<w n="13.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="13.6">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.7">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="13.8">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
					<l n="14" num="4.2"><w n="14.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="14.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="14.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.5">br<seg phoneme="ɥ" type="sc" value="0" rule="454">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="14.6">d</w>’<w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.8">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="14.9">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="14.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg></w> <w n="15.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.3"><seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="15.4">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="15.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="15.6">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.7">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="16" num="4.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="16.3">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.4">g<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="17.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="17.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> : <w n="17.6">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="17.8">M<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="18.2">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="18.3">d</w>’<w n="18.4"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="18.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="18.6">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.7">n<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="18.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="19.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="19.3">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="19.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="19.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.7">V<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lm<seg phoneme="i" type="vs" value="1" rule="492">y</seg></w>,</l>
					<l n="20" num="5.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="20.1">C<seg phoneme="y" type="vs" value="1" rule="449">u</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="20.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="20.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="20.5"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.6">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>