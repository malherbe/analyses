<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ34">
					<head type="number">XXXIV</head>
					<head type="main">AVENTURE DU ROI GUILLAUME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> <w n="1.2">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> ! <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="1.6">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="1.8">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="1.9"><seg phoneme="y" type="vs" value="1" rule="390">eu</seg></w> <w n="1.10">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ?</l>
						<l n="2" num="1.2"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="2.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.5">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.7">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> ?</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ffl<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="3.6">r<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> ?</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> !… <w n="4.4">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">st<seg phoneme="y" type="vs" value="1" rule="449">u</seg>p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">S<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="5.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>, <w n="5.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="5.5">r<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> !… <w n="5.6">T<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.7">ch<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w>, <w n="5.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>cs</w> <w n="5.9">d</w>'<w n="5.10"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">S</w>'<w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5">f<seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> ; <w n="6.6">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.7">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="6.8">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> !</l>
						<l n="7" num="2.3"><w n="7.1">C<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="7.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> ; <w n="7.5">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="7.6">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="7.7">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="7.8">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.9">qu</w>'<w n="7.10"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> !</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.2">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.3">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.5"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.6">pl<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.3">t</w>'<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>qu<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !… <w n="9.6">C</w>'<w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.8">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="9.9"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.10"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.11">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> !</l>
						<l n="10" num="3.2"><w n="10.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.2">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="10.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>cs</w>-<w n="10.4">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="10.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="10.6">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="10.7">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> !</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="11.2">n</w>'<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="11.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="11.7">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.8">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.9">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.10">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="11.11">bl<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> !</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.2">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b</w> <w n="12.3">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.4">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t</w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.7">r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="13.2">v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="13.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>g<seg phoneme="y" type="vs" value="1" rule="447">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="13.7">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="14" num="4.2"><w n="14.1">J</w>'<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="14.4">l</w>'<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.7">v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="14.8">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>br<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.9"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="14.10">m<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">P<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>-<w n="15.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>, <w n="15.3">d</w>'<w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="15.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="15.6">j<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w>, <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="15.8">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="15.9"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="15.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.11">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="16.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="16.3">v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w>, <w n="16.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Octobre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>