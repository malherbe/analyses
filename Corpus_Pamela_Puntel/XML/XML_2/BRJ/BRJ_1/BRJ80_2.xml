<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ80">
					<head type="number">LXXX</head>
					<head type="main">LE SOLSTICE D'HIVER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="1.2">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="1.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.2">T<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="2.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xtr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="3.2">b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>il</w>, <w n="4.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.3">l</w>'<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1">D</w>'<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.3">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="6.4">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5">r<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.2">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>, <w n="7.4">l</w>'<w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6">s</w>'<w n="7.7"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">l</w>'<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>str<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="8.6">l</w>'<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="9" num="1.9"><w n="9.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.3">n<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="9.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> !</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1"><w n="10.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.4">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="11" num="2.2"><w n="11.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="11.2">t</w>'<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="11.6">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>lst<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="12" num="2.3"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="12.2">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.3">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.4">d</w>'<w n="12.5">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="12.6">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>.</l>
						<l n="13" num="2.4"><w n="13.1">Pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.3">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> ! <w n="13.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="13.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.6">ch<seg phoneme="y" type="vs" value="1" rule="444">û</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> !</l>
						<l n="14" num="2.5"><w n="14.1">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="14.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.4">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
						<l n="15" num="2.6"><w n="15.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="15.4">r<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="15.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>,</l>
						<l n="16" num="2.7"><w n="16.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="16.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="16.6">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="17" num="2.8"><w n="17.1">L</w> '<w n="17.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.3"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="17.4">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.6">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.7">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
						<l n="18" num="2.9"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.2">n<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="18.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="18.4">p<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="18.6">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.7">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>