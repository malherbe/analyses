<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ67">
					<head type="number">LXVII</head>
					<head type="main">LA SAINTE-VEHME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>'<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> ?… <w n="1.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> ! — <w n="1.4">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.6">S<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="1.7">V<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>hm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.2">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="2.3">s<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w>, <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>,</l>
						<l n="3" num="1.3"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.2">Gu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317">au</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="3.4">B<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rk</w> <w n="3.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="3.6">j<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.7">l</w>'<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="4.3">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="4.4">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.6">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> !… <w n="5.2">L</w>'<w n="5.3">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.9">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>lt<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2">qu</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.5">n<seg phoneme="ɛ" type="vs" value="1" rule="383">ei</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.6">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.8">d</w>'<w n="6.9"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="6.10">s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.2">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="7.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="7.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w>, <w n="7.7">t<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="7.9">pl<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.10">l<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="12"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="8.3">l</w>'<w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="8.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Qu</w>'<w n="9.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ts</w> <w n="9.5">d</w>'<w n="9.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.7">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
						<l n="10" num="3.2"><w n="10.1">F<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w>-<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="10.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.5">p<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.6">p<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="10.7">l</w>'<w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ?</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="11.2">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.3">m<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.6">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="11.8">l</w>'<w n="11.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="12"></space><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>pr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.5">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">N</w>'<w n="13.2"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="13.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>, <w n="13.5">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.6">l</w>'<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="13.8">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="14" num="4.2"><w n="14.1">C<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.2">qu</w>'<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="14.4">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="14.6">fl<seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.9">t<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="14.10">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> ;</l>
						<l n="15" num="4.3"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="15.2">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.6">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="15.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.9">t<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
						<l n="16" num="4.4"><space unit="char" quantity="12"></space><w n="16.1">L</w>'<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="16.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="16.4">l</w>'<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Qu</w>'<w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="17.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="17.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.8">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.9"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="17.10">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.11">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="18.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="18.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="18.6">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="18.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="18.8">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.9">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.3">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>-<w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="19.5">s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="19.7">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.8"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="19.9">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="19.10">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.11">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="20" num="5.4"><space unit="char" quantity="12"></space><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="20.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="20.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.5">p<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.2">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="21.3">f<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="21.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.7">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="22" num="6.2"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="22.2">s<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="22.6"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="22.7">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="22.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>.</l>
						<l n="23" num="6.3"><w n="23.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="23.3">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="23.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.5">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w>, <w n="23.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="23.7">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="23.8">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.9">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.10">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="24" num="6.4"><space unit="char" quantity="12"></space><w n="24.1">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="24.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="24.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>