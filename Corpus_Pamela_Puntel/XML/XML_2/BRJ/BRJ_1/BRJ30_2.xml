<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ30">
					<head type="number">XXX</head>
					<head type="main">LES MARTYRS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.2">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="1.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="1.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ff<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="2.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ; <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.6">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>.</l>
						<l n="3" num="1.3"><w n="3.1">C</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.4">n<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w>. <w n="3.5">L</w>'<w n="3.6"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.9">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="3.10">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.11">s<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="4.3">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w>, <w n="4.6">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.7">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="4.9">l</w>'<w n="4.10"><seg phoneme="a" type="vs" value="1" rule="339">â</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="5.2">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w>, <w n="5.4">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="5.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>-<w n="5.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w>, <w n="5.7">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.8">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.9">d</w>'<w n="5.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>…</l>
						<l n="6" num="2.2">» <w n="6.1">N<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.3">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.6">s<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="6.7">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> ! »</l>
					</lg>
					<lg n="3">
						<l part="I" n="7" num="3.1">— « <w n="7.1"><seg phoneme="u" type="vs" value="1" rule="425">Où</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.3">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> ? » <w n="7.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="7.5">l</w>'<w n="7.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>. </l>
						<l part="F" n="7" num="3.1">— » <w n="7.7"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="7.8">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.9">m</w>'<w n="7.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.11">gu<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
						<l part="I" n="8" num="3.2">» <w n="8.1"><seg phoneme="u" type="vs" value="1" rule="425">Où</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> !… » </l>
					</lg>
					<lg n="4">
						<l part="F" n="8">— » <w n="8.4">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="8.5">qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ! <w n="8.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="8.8">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.9">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w>,</l>
						<l n="9" num="4.1">» <w n="9.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w> ; <w n="9.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.6">s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="9.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="9.8">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w></l>
						<l n="10" num="4.2">» <w n="10.1">Qu</w>'<w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.3">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="10.4">s</w>'<w n="10.5"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="10.6">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t</w>, <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rgn<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="10.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="10.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.10">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! »</l>
						<l n="11" num="4.3">— » <w n="11.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="11.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="11.3">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="11.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>-<w n="11.7">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="11.9">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.10">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.11">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !</l>
						<l part="I" n="12" num="4.4">» <w n="12.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg></w> <w n="12.2">s<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>… </l>
					</lg>
					<lg n="5">
						<l part="F" n="12">— » <w n="12.3">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="12.6">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.7">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.8">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.9">l</w>'<w n="12.10"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="12.11">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! »</l>
					</lg>
					<lg n="6">
						<l n="13" num="6.1"><w n="13.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.2">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="13.3">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="13.5">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="13.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l part="I" n="14" num="6.2"><w n="14.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !… </l>
					</lg>
					<lg n="7">
						<l part="M" n="14">— » <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="14.4">g<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> !… <w n="14.5">qu</w>'<w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.7">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.8">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !… » </l>
					</lg>
					<lg n="8">
						<l part="F" n="14"><w n="14.9">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> !…</l>
					</lg>
					<lg n="9">
						<l n="15" num="9.1"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="15.2">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="15.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="15.5">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="15.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.7">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="16" num="9.2"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.3">t<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="16.5">tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="16.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> : <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="16.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="16.9">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="16.10">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> !…</l>
					</lg>
					<lg n="10">
						<l n="17" num="10.1"><w n="17.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.2">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.4">g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>. <w n="17.5">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="17.8">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.9">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="18" num="10.2"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt</w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="18.3">br<seg phoneme="ɥ" type="sc" value="0" rule="454">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w>, <w n="18.4">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="18.6">cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="18.7">d</w>'<w n="18.8"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="18.9">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w></l>
						<l n="19" num="10.3"><w n="19.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="19.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="19.6">c<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="19.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.8">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="19.9">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
					</lg>
					<lg n="11">
						<l n="20" num="11.1"><w n="20.1">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="20.2">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="20.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="492">y</seg>rs</w>, <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="20.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="20.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="20.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> ?… —</l>
					</lg>
					<lg n="12">
						<l n="21" num="12.1"><w n="21.1">J</w>'<w n="21.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="21.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="21.6">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.7"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="21.8">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.9">n<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="22" num="12.2"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="22.2">c</w>'<w n="22.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="22.4">m</w>'<w n="22.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="22.6">t</w>-<w n="22.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.8">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>, <w n="22.9"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="22.10"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="22.11">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.12">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="23" num="12.3"><w n="23.1">P<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.4">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>-<w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="23.6">qu</w>'<w n="23.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="23.8"><seg phoneme="o" type="vs" value="1" rule="314">Eau</seg>x</w> <w n="23.9">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.10">l</w>'<w n="23.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">Em</seg>p<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>,</l>
						<l n="24" num="12.4"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="24.2">H<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w>, <w n="24.3">l</w>'<w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> <w n="24.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="24.6">j</w>'<w n="24.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="24.8">v<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="24.9">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.10"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="24.11">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
						<l n="25" num="12.5"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="25.2">j<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="25.4">fl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="25.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="25.6">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="25.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.8">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
						<l part="I" n="26" num="12.6"><w n="26.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>br<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.3">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="26.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="26.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.6">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> !… </l>
					</lg>
					<lg n="13">
						<l part="F" n="26"><w n="26.7"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="26.8">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
						<l n="27" num="13.1"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="27.2">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.3"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.4"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="27.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.7">l</w>'<w n="27.8"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
						<l n="28" num="13.2"><w n="28.1">T<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="28.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.3">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>… <w n="28.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="28.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, <w n="28.6">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.7">g<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="28.8">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="28.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="28.10">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w>,</l>
						<l n="29" num="13.3"><w n="29.1">F<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="29.3">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.4">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w></l>
						<l n="30" num="13.4"><w n="30.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="30.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> !… <w n="30.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w>-<w n="30.4">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> ?… <w n="30.5">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="30.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="30.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Octobre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>