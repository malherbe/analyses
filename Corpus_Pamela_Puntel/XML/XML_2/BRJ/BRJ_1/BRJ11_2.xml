<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ11">
					<head type="number">XI</head>
					<head type="main">LES BULLETINS DE VICTOIRE</head>
					<div type="section" n="1">
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="1.2">B<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rtr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.4">R<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w> <w n="1.5">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="2" num="1.2"><w n="2.1">N<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.2">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="2.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> ;</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="3.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w>, <w n="3.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.4">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="3.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.6">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1">G<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="4.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> :</l>
							<l n="5" num="1.5"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.2">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ck</w>-<w n="5.3">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ck<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="6" num="1.6"><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="6.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.3">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ll<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w>,</l>
							<l n="7" num="1.7"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.2">br<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.4">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.6">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
							<l n="8" num="1.8"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.3">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.5">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w>.</l>
							<l n="9" num="1.9"><w n="9.1">Br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ps</w> <w n="9.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.6">cr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="10" num="1.10"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.2">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.4">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> ;</l>
							<l n="11" num="1.11"><w n="11.1">C</w>'<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>rs</w> <w n="11.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.7">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="12" num="1.12"><w n="12.1">Qu</w>'<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="12.4">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>v<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w>.</l>
							<l n="13" num="1.13"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w> <w n="13.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.7">j<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="14" num="1.14"><w n="14.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="14.4">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="14.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>,</l>
							<l n="15" num="1.15"><w n="15.1">Tr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="15.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="15.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.5">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="16" num="1.16"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="16.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ts</w> <w n="16.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="16.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="16.6">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> !</l>
							<l n="17" num="1.17"><w n="17.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="17.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="17.6">r<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>gl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="18" num="1.18"><w n="18.1">F<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w>-<w n="18.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="18.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="18.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>dr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?… <w n="18.6">F<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="18.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> !</l>
							<l n="19" num="1.19"><w n="19.1">L</w>'<w n="19.2"><seg phoneme="ø" type="vs" value="1" rule="404">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>p<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.3">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="19.4">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="19.5">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.6">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="19.7"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
							<l n="20" num="1.20"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="20.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.5">d<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</l>
							<l n="21" num="1.21"><w n="21.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="21.2">br<seg phoneme="ɥ" type="sc" value="0" rule="454">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="21.3">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="21.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="21.6">t<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="22" num="1.22"><w n="22.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="22.2">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.3">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.4">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>,</l>
							<l n="23" num="1.23"><w n="23.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="23.2">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.3">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="23.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="23.5">d</w>'<w n="23.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
							<l n="24" num="1.24"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="24.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="24.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="24.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.5">B<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rl<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1870">22 août 1870</date>
							</dateline>
						</closer>
					</div>
					<div type="section" n="2">
						<lg n="1">
							<l n="25" num="1.1"><w n="25.1">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.3">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="25.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="25.5">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
							<l n="26" num="1.2"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="26.5">ch<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>,</l>
							<l n="27" num="1.3"><w n="27.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg>t</w> <w n="27.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="27.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="27.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.5">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="27.6">r<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> ;</l>
							<l n="28" num="1.4"><w n="28.1">S<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> !… <w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="28.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="28.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="28.5">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1870">8 novembre 1 870.</date>
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>