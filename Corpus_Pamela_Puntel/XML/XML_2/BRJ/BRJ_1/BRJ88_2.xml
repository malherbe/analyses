<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ88">
					<head type="number">LXXXVIII</head>
					<head type="main">LE ROI TROPPMANN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">R<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="1.2">fr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="1.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.4">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.7">phr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>pl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">v<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="2.4">n</w>'<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="2.7">b<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="2.8">d</w>'<w n="2.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ph<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="3.5">j</w>'<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="3.7">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.4"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.8">l</w>'<w n="4.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Tr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ppm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="5.3">n</w>'<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="5.8">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
						<l n="6" num="2.2"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.3">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="6.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.5">t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="6.6">qu</w>'<w n="6.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>-<w n="6.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="6.9">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.11">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="7.3">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.5">l<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="7.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.7">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ts</w>,</l>
						<l n="8" num="2.4"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>pt</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="8.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.6">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ts</w>,</l>
						<l n="9" num="2.5"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ppr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="9.3">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.5"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.7">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="10" num="2.6"><w n="10.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="10.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="10.5">qu</w>'<w n="10.6"><seg phoneme="y" type="vs" value="1" rule="250">eû</seg>t</w> <w n="10.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.9">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
						<l n="11" num="2.7"><w n="11.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.2">l</w>'<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.4">h<seg phoneme="œ̃" type="vs" value="1" rule="260">um</seg>bl<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w>, <w n="11.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.6">n</w>'<w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="11.9">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>.</l>
						<l n="12" num="2.8"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="12.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="12.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="12.8">p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> ?</l>
						<l n="13" num="2.9"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="353">E</seg>x<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="13.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="13.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.5">d<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ss<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="13.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.7">Gu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317">au</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
						<l n="14" num="2.10"><w n="14.1">C<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="14.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="14.4">s</w>'<w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="14.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.8">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.9">r<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="o" type="vs" value="1" rule="317">au</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="15" num="2.11"><w n="15.1">V<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="15.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.3">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="15.4">l</w>'<w n="15.5"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ls<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.8">L<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ; <w n="15.9">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> !</l>
						<l n="16" num="2.12"><w n="16.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.2">Tr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ppm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn</w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="16.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.6">m<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg></w>,</l>
						<l n="17" num="2.13"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="17.2">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="17.4">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="17.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="17.6">l</w>'<w n="17.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="17.8">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="17.9">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="18" num="2.14"><w n="18.1">L</w>'<w n="18.2">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>sp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="18.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.5">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="18.7">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w> <w n="18.9">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.10">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="19" num="2.15"><w n="19.1">J<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="19.2">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="19.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.6"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.8">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>,</l>
						<l n="20" num="2.16"><w n="20.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.2">cr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="20.4">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>… <w n="20.5">l</w>'<w n="20.6"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="20.7">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.8"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="20.9">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> !</l>
						<l n="21" num="2.17"><w n="21.1">Tr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>pm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn</w> <w n="21.2"><seg phoneme="y" type="vs" value="1" rule="250">eû</seg>t</w>-<w n="21.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="21.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="21.5">m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="21.6">s</w>'<w n="21.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="21.8"><seg phoneme="y" type="vs" value="1" rule="250">eû</seg>t</w> <w n="21.9"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="21.10">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
					</lg>
					<lg n="3">
						<l n="22" num="3.1"><w n="22.1">L</w>'<w n="22.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="22.4">cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="22.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.8">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
						<l n="23" num="3.2"><w n="23.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="23.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.3">gu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="23.4">Tr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ppm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn</w> ; <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="23.6">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="23.7">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.8">r<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
						<l n="24" num="3.3"><w n="24.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="24.2">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.3">l</w>'<w n="24.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="24.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="24.7">gu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !… <w n="24.8">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ?</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">Janvier 1871.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>