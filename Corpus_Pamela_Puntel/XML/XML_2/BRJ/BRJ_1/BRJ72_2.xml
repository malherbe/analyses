<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ72">
					<head type="number">LXXI</head>
					<head type="main">QUI VIVE !</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>'<w n="1.2">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="1.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.6">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.7"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.8">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="2.2">m<seg phoneme="o" type="vs" value="1" rule="437">o</seg>t</w> <w n="2.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.3">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>pt<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">S<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.2"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.4"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w>.</l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="5.2">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="5.3">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.4">c</w>'<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.6">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>. <w n="5.7"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="5.8">tr<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="5.9"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="5.10">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.11">g<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="6.2">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>, <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.5">fr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> ;</l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="7.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">s</w>'<w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="7.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.7">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.8">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rg<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="12"></space><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg></w> <w n="8.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.4">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="8.5">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> !</l>
						<l n="9" num="1.9"><w n="9.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">l<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>, <w n="9.8">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="10" num="1.10"><space unit="char" quantity="12"></space><w n="10.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.3">f<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
						<l n="11" num="1.11"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="11.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="11.4">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="11.8">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="12" num="1.12"><space unit="char" quantity="12"></space><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="12.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="12.4">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
						<l n="13" num="1.13"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="13.2">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.4">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="13.5">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.7">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
						<l n="14" num="1.14"><space unit="char" quantity="12"></space><w n="14.1">V<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.3">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="14.6">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> ;</l>
						<l n="15" num="1.15"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="15.2">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="15.4">B<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lf<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>, <w n="15.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="15.6">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="15.8">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
						<l n="16" num="1.16"><space unit="char" quantity="12"></space><w n="16.1">Qu</w>'<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.4">br<seg phoneme="y" type="vs" value="1" rule="444">û</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="16.5">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> !</l>
						<l n="17" num="1.17"><w n="17.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="17.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.3">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="17.4">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.5">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="17.7">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="17.8"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
						<l n="18" num="1.18"><space unit="char" quantity="12"></space><w n="18.1">C<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.2">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
						<l n="19" num="1.19"><w n="19.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.2">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.3">m<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="19.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="19.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.6">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="19.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.8">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
						<l n="20" num="1.20"><space unit="char" quantity="12"></space><w n="20.1">D<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="20.3">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="20.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c</w> !</l>
						<l n="21" num="1.21"><w n="21.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="348">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="21.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.3">l</w>'<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="21.6">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="21.7">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>sf<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="22" num="1.22"><space unit="char" quantity="12"></space><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="22.2">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="22.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.5">b<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t</w></l>
						<l n="23" num="1.23"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="23.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.3">p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="23.4">pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="23.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="23.7">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.8"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="24" num="1.24"><space unit="char" quantity="12"></space><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="24.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="24.3">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> !</l>
						<l n="25" num="1.25"><w n="25.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="25.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="25.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.4">n<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w>, <w n="25.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="25.6">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="25.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="26" num="1.26"><space unit="char" quantity="12"></space><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="26.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>,</l>
						<l n="27" num="1.27"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="27.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="27.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="27.4">l</w>'<w n="27.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="27.7">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="27.8">cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> : <w n="27.9">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="27.10">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
						<l n="28" num="1.28"><space unit="char" quantity="12"></space><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="28.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> : <w n="28.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> !… —</l>
						<l n="29" num="1.29"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> <w n="29.2">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>, <w n="29.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="29.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w>, <w n="29.5"><seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="29.6">r<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="30" num="1.30"><space unit="char" quantity="12"></space><w n="30.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg></w> <w n="30.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="30.3">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>,</l>
						<l n="31" num="1.31"><w n="31.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="31.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="31.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="31.4">qu</w>'<w n="31.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="31.6">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>, <w n="31.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="31.8">l</w>'<w n="31.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>dv<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="31.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="32" num="1.32"><space unit="char" quantity="12"></space><w n="32.1">D</w>'<w n="32.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="32.3">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="32.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="32.5">c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> !</l>
						<l n="33" num="1.33"><w n="33.1">V<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! <w n="33.2">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>, <w n="33.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="33.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ! <w n="33.5">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="33.6">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="33.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="33.8">qu</w>'<w n="33.9"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="33.10"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="34" num="1.34"><space unit="char" quantity="12"></space><w n="34.1">L</w>'<w n="34.2">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="34.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="34.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="34.5">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> ;</l>
						<l n="35" num="1.35"><w n="35.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="35.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="35.3">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="35.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="35.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="35.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>cr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="35.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="35.8">s</w>'<w n="35.9">h<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="36" num="1.36"><space unit="char" quantity="12"></space><w n="36.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="36.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="36.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="36.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="36.5">p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> !</l>
						<l n="37" num="1.37"><w n="37.1">V<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> ! <w n="37.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="37.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="37.4">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="37.5">cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> : <w n="37.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="37.7">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !… <w n="37.8">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.9">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.10">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="38" num="1.38"><space unit="char" quantity="12"></space><w n="38.1">Qu</w>'<w n="38.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="38.3">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="38.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="38.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="38.6">ch<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>,</l>
						<l n="39" num="1.39"><w n="39.1">Pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="39.2">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="39.5">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.7">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="40" num="1.40"><space unit="char" quantity="12"></space><w n="40.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="40.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>dr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> : <w n="40.3">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>