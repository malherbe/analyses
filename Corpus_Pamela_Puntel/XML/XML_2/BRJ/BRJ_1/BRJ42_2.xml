<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ42">
					<head type="number">XLII</head>
					<head type="main">PROCLAMATION</head>
					<lg n="1">
						<l n="1" num="1.1">» <w n="1.1">S<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="1.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>f<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="1.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="2" num="1.2">» <w n="2.1">J</w>'<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xpr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="2.3">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.8">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> ! »</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>— (<w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="3.6">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="4.5">l</w>'<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.)</l>
						<l n="5" num="1.5">» — <w n="5.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2">c<seg phoneme="o" type="vs" value="1" rule="317">au</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="5.4">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.6">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="5.7">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="5.8">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w></l>
						<l n="6" num="1.6">» <w n="6.1">N</w>'<w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ç<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="6.5">l</w>'<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="6.7">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.8">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.10">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="6.11"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> ! »</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space>— (<w n="7.1">C<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>q</w> <w n="7.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w>, <w n="7.4">s</w>'<w n="7.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="7.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t</w>, <w n="7.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.9">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.2">l</w>'<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="8.4"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="8.5">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?)</l>
						<l n="9" num="1.9">» — <w n="9.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.3">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.4">W<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rth</w>, <w n="9.5">S<seg phoneme="a" type="vs" value="1" rule="296">aa</seg>rbr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ck</w>, <w n="9.6">B<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>, <w n="9.7">Str<seg phoneme="a" type="vs" value="1" rule="339">a</seg>sb<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>— (<w n="10.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> ! <w n="10.2">Str<seg phoneme="a" type="vs" value="1" rule="339">a</seg>sb<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="10.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?)</l>
						<l n="11" num="1.11">» — <w n="11.1">S<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w>, <w n="11.2">M<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tz</w>, <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="11.4">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="11.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.7">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.8">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w> ! »</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space>— (<w n="12.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.3">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.6">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !)</l>
						<l n="13" num="1.13">» — <w n="13.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="13.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.6">pr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="13.7">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="13.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w></l>
						<l n="14" num="1.14">» <w n="14.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="14.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.9">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="15" num="1.15">» <w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="15.4">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="15.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="15.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.7">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.8">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! »</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space>— (<w n="16.1">C</w>'<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="16.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> ! <w n="16.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.5">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="16.7">c<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.)</l>
						<l n="17" num="1.17">» — <w n="17.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="17.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="49">e</seg>s</w> <w n="17.5">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="17.6">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
						<l n="18" num="1.18">» <w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="18.2">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>v<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="18.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gu<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> !… (<w n="18.6">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.7">n<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> !…</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="19.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="19.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.5">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w>.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">L</w>'<w n="20.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="20.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.5">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="21.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="21.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.5">v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w>,</l>
						<l n="22" num="1.22"><w n="22.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.2">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rtr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.4">bl<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="22.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.6">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w>, <w n="22.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.8">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> ;</l>
						<l n="23" num="1.23"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="23.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="23.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="23.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>,</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="24.2">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="24.3">t<seg phoneme="ɥ" type="sc" value="0" rule="457">u</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="24.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="25.2">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="25.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="25.4">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="25.5">gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="26" num="1.26"><w n="26.1">Br<seg phoneme="y" type="vs" value="1" rule="444">û</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="26.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="26.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>fs</w> !… <w n="26.4">Br<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>f</w> ! <w n="26.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="26.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.7">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> !)</l>
						<l n="27" num="1.27">» — <w n="27.1">M<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tz</w>, <w n="27.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="27.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="27.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="27.5">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.6">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="28" num="1.28">» <w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="28.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>, <w n="28.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! »</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space>— (<w n="29.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="29.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="29.4">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> ;</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">T<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.2">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !… ) — « <w n="30.3">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="30.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="30.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="31" num="1.31"><w n="31.1">D<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="31.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="31.3">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>… » — (<w n="31.4">V<seg phoneme="œ" type="vs" value="1" rule="405">eu</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="31.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xc<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="31.6">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="31.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="32" num="1.32"><w n="32.1">L</w>'<w n="32.2"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ll<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="32.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="32.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="32.5">m<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w> <w n="32.6">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="32.7">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="32.8">r<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.)</l>
						<l n="33" num="1.33">» — <w n="33.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="33.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="33.3">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="33.4">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="33.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="33.6">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="34" num="1.34">» <w n="34.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="34.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="34.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>pl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="34.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>. <w n="34.5">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="34.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="34.7">l</w>'<w n="34.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="34.9">pr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
						<l n="35" num="1.35">» <w n="35.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="35.2">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="35.3">l</w>'<w n="35.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="35.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="35.6">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="35.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="35.8">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
						<l n="36" num="1.36">» <w n="36.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="36.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="36.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="36.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="36.5">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="36.6">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="36.7"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="36.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! »</l>
					</lg>
					<lg n="2">
						<l n="37" num="2.1">— <w n="37.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.2">ch<seg phoneme="y" type="vs" value="1" rule="444">û</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="37.4">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="37.6">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="37.8">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>.</l>
						<l n="38" num="2.2"><space unit="char" quantity="8"></space><w n="38.1">T<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="38.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>, <w n="38.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>l<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="38.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="39" num="2.3"><w n="39.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="39.2">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="39.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.4">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.6">h<seg phoneme="i" type="vs" value="1" rule="492">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
						<l n="40" num="2.4"><w n="40.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="40.2">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="40.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="40.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="40.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="40.6">cr<seg phoneme="y" type="vs" value="1" rule="444">û</seg></w> ; — <w n="40.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="40.8">n<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="40.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="40.10">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="40.11">m<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>