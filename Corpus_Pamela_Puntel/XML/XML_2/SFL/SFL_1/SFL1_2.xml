<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DU SIÈGE DE PARIS</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="SFL">
					<name>
						<forename>Théobald</forename>
						<surname>SAINT-FÉLIX</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>161 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">SFL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>CHANTS DU SIÈGE DE PARIS. 1870-1871</title>
						<author>THÉOBALD SAINT-FÉLIX</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Imprimerie Ch. Schiller</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="SFL1">
				<head type="main">LE BRANLE-BAS</head>
				<head type="sub_1">CHANT NAVAL</head>
				<opener>
					<salute>AUX MARINS DE LA FRANCE</salute>
				</opener>
				<div type="section" n="1">
					<head type="number">1</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w>, <w n="1.2">n</w>’<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="2.2">br<seg phoneme="ɥ" type="sc" value="0" rule="454">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="2.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="2.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
						<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.4">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="3.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.7">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="3.8">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="4.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="4.6">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>…</l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.3">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="5.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1">C<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="6.2">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
						<l n="7" num="1.7"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="7.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.6">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>, <w n="8.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="8.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.6">t<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
						<l n="9" num="2.2"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="9.2">c</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.4">l</w>’<w n="9.5">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w>.</l>
						<l n="10" num="2.3"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="10.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="10.4">c</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.6">l</w>’<w n="10.7">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.8">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="10.9">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="11" num="2.4"><w n="11.1">J<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="11.3">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w>, <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="11.7">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">2</head>
					<lg n="1">
						<l n="12" num="1.1"><w n="12.1">N</w>’<w n="12.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="12.3">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>,</l>
						<l n="13" num="1.2"><w n="13.1">S<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="13.2">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>…</l>
						<l n="14" num="1.3"><w n="14.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="14.2">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="14.3">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="14.4">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>, <w n="14.5">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
						<l n="15" num="1.4"><w n="15.1">R<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="15.2">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.3">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="15.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="15.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ps</w>.</l>
						<l n="16" num="1.5"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="16.2">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="16.4">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="16.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="17" num="1.6"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="17.4">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="17.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="18" num="1.7"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.2">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="18.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="19" num="2.1"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>, <w n="19.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="19.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="19.6">t<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
						<l n="20" num="2.2"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="20.2">c</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="20.4">l</w>’<w n="20.5">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w>.</l>
						<l n="21" num="2.3"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="21.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="21.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="21.4">c</w>’<w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="21.6">l</w>’<w n="21.7">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.8">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="21.9">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="22" num="2.4"><w n="22.1">J<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="22.3">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="22.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="22.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w>, <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="22.7">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">3</head>
					<lg n="1">
						<l n="23" num="1.1"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="23.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="23.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="23.4">Pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>, <w n="23.5">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="23.6">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>,</l>
						<l n="24" num="1.2"><w n="24.1">S<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="24.2">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.5">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="25" num="1.3"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>cr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="25.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.3">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>lt<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="26" num="1.4"><w n="26.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343">ue</seg>il</w> <w n="26.4">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.5"><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.6">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> !</l>
						<l n="27" num="1.5"><w n="27.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="27.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="27.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
						<l n="28" num="1.6"><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.2">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="28.4">l</w>’<w n="28.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="29" num="1.7"><w n="29.1">S</w>’<w n="29.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="29.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="29.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.5">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="30" num="2.1"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="30.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>, <w n="30.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="30.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="30.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="30.6">t<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
						<l n="31" num="2.2"><w n="31.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="31.2">c</w>’<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="31.4">l</w>’<w n="31.5">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="31.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="31.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w>.</l>
						<l n="32" num="2.3"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="32.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="32.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="32.4">c</w>’<w n="32.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="32.6">l</w>’<w n="32.7">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="32.8">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="32.9">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="33" num="2.4"><w n="33.1">J<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="33.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="33.3">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="33.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="33.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w>, <w n="33.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="33.7">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> !</l>
					</lg>
				</div>
				<closer>
					<note type="footnote" id="">N.B. — La musique, chant et piano, composé par M. Henri Cohen, est en vente chez M. Sylvain Saint-Étienne, n° 31 bis, Faubourg-Montmartre, à Paris.</note>
				</closer>
			</div></body></text></TEI>