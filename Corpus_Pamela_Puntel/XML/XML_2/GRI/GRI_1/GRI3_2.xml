<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">REVUE DE BRETAGNE ET DE VENDÉE</title>
				<title type="sub_2">QUATORZIÈME ANNÉE</title>
				<title type="sub_1">TROISIÈME SÉRIE — TOME VIII</title>
				<title type="medium">Édition électronique</title>
				<author key="GRI">
					<name>
						<forename>Émile</forename>
						<surname>GRIMAUD</surname>
					</name>
					<date from="1831" to="1901">1831-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>413 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">STROPHES PATRIOTIQUES</title>
						<author>ÉMILE GRIMAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k453762t.r=Revue%20de%20Bretagne%20et%20de%20Vend%C3%A9e%201870?rk=85837;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>REVUE DE BRETAGNE ET DE VENDÉE</title>
								<imprint>
									<pubPlace>NANTES</pubPlace>
									<publisher>REVUE DE BRETAGNE ET DE VENDÉE — ANNÉE 1870 — DEUXIÈME SEMESTRE</publisher>
									<date when="1870">OCTOBRE 1870</date>
								</imprint>
								<biblScope unit="tome">T. 28</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-21" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Octobre 1870</head><div type="poem" key="GRI3">
					<head type="main">UN SOLDAT DU PAPE</head>
					<opener>
						<salute>A LA MÉMOIRE D'AUGUSTE DE LA BROSSE</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.6">t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="2.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, <w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> : « <w n="2.6">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> !</l>
						<l n="3" num="1.3">» <w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
						<l n="4" num="1.4">» <w n="4.1">T<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>-<w n="4.2">t</w>-<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.5">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="4.6">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> ? »</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="5.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w> <w n="5.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> : « <w n="5.5">F<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! »</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="6.2">f<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.4">l</w>'<w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>…</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="7.2">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>, <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="7.4">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t</w> <w n="7.5">d</w>'<w n="7.6"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.3">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="8.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.5">M<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="9.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="10.3">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="10.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="11.2">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="11.3">f<seg phoneme="œ" type="vs" value="1" rule="303">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="11.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="12" num="3.4"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="12.2">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="12.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="12.5">g<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="13.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="14" num="4.2"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.3"><seg phoneme="œ" type="vs" value="1" rule="285">œ</seg>il</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="14.5">l</w>'<w n="14.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="15.2">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="15.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> : « <w n="15.4">R<seg phoneme="ɔ" type="vs" value="1" rule="440">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="15.6">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! »</l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="16.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt</w> <w n="16.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.6">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="17.2">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rs</w> <w n="17.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.5">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.7"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
						<l n="18" num="5.2"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="18.2">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="18.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="18.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.7">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>…</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="19.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="19.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>f</w> <w n="19.4">d</w>'<w n="19.5">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="19.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
						<l n="20" num="5.4"><w n="20.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.2">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.4">J<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>-<w n="20.5">Chr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">M<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="21.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="21.3">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="21.5">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>.</l>
						<l n="22" num="6.2"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w>-<w n="22.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="22.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> <w n="22.7">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> ?</l>
						<l n="23" num="6.3"><w n="23.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="23.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.3">fl<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w> <w n="23.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.5">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="23.6">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
						<l n="24" num="6.4"><w n="24.1">C<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="24.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="24.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">N<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.2">l</w>'<w n="25.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="25.5">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.6">r<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
						<l n="26" num="7.2"><w n="26.1">F<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="26.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="26.3">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.4">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w>.</l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="27.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="27.3">l</w>'<w n="27.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="27.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.6">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
						<l n="28" num="7.4"><w n="28.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> ! <w n="28.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="28.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>g</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">L</w>'<w n="29.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="29.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="29.4">Tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="29.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w>, <w n="29.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="29.7">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! <w n="29.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="29.9">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
						<l n="30" num="8.2"><w n="30.1">P<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w>, <w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="30.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="30.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>p</w> <w n="30.5">d</w>'<w n="30.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> :</l>
						<l n="31" num="8.3">« <w n="31.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w>, <w n="31.2">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>-<w n="31.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w>, <w n="31.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="31.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="31.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="31.7">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
						<l n="32" num="8.4">» <w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="32.2">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="32.5"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="32.6">S<seg phoneme="ɛ" type="vs" value="1" rule="383">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ! »</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">24 octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>