<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP20">
				<head type="number">XX</head>
				<head type="main">LE SERGENT</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.4">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="1.5">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="1.6">d<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="1.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="1.8">d</w>'<w n="1.9"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="2.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.3">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.4">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.6">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="2.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.10"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="3.4">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="3.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.6">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.7">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>.</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3">l</w>'<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="4.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="4.8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g</w>,</l>
					<l n="5" num="1.5"><w n="5.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="5.3">d</w>'<w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="5.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="5.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="5.8">cr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="5.10">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.11">m<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
					<l n="6" num="1.6"><w n="6.1">L<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.3">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="6.5">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.7">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>, <w n="6.8">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.10">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					<l n="7" num="1.7"><w n="7.1">S<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="7.3">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w>, <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="7.6">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="7.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
					<l part="I" n="8" num="1.8">— <w n="8.1"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> <w n="8.2">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> ! <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>-<w n="8.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w>, <w n="8.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>-<w n="8.7">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.8"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="8.9">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ? </l>
					<l part="F" n="8" num="1.8">— <w n="8.10">Tr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>-<w n="8.11">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>.</l>
					<l n="9" num="1.9"><w n="9.1">P<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.2">d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! <w n="9.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="9.4">n</w>'<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="9.7">c<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>q</w> <w n="9.8">m<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.10">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="10" num="2.1"><w n="10.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.2">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> : <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.4"><seg phoneme="œ" type="vs" value="1" rule="285">œ</seg>il</w> <w n="10.5">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="10.7">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.8">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
					<l n="11" num="2.2"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="11.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="11.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.5"><seg phoneme="y" type="vs" value="1" rule="250">eû</seg>t</w> <w n="11.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="11.7">qu</w>'<w n="11.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.9">m</w>'<w n="11.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="11.11">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>.</l>
					<l n="12" num="2.3"><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="12.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w>, <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="12.6">br<seg phoneme="ɥ" type="sc" value="0" rule="454">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="12.7">d</w>'<w n="12.8"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="12.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w></l>
					<l n="13" num="2.4"><w n="13.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="13.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.4"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="13.5">m<seg phoneme="ɥ" type="sc" value="0" rule="457">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="13.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="13.7">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.8">g<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="13.9">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.10">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> :</l>
					<l n="14" num="2.5"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="14.2">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.3">dr<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="14.4">d</w>'<w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="14.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="14.8">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.9">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>, <w n="14.10"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="14.11">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="14.12">d</w>'<w n="14.13"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
					<l n="15" num="2.6"><w n="15.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.2">s</w>'<w n="15.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="15.5">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.6">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="15.7">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ttr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w>…</l>
					<l part="I" n="16" num="2.7"><w n="16.1">D</w>'<w n="16.2"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="16.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="16.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="16.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> : </l>
					<l part="M" n="16" num="2.7">— <w n="16.7">Pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> ! </l>
					<l part="F" n="16" num="2.7"><w n="16.8"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="16.9"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="16.10">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Auxonne, 8 septembre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>