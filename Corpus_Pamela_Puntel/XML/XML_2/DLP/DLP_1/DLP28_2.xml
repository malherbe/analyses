<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP28">
				<head type="number">XXVIII</head>
				<head type="main">L'ORPHELIN</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.7">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.8">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.9">ch<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ;</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.3">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="240">e</seg>ds</w>, <w n="2.4">l</w>'<w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.6">t<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="2.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="2.9">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.10">l<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="3.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="3.5">d</w>'<w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="3.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.8">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>.</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="4.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>squ<seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="4.4">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>-<w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.7">g<seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> :</l>
					<l n="5" num="1.5"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.4">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.8">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="5.9">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="5.10">r<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.3"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="6.5">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>, <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.7">l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.8">l</w>'<w n="6.9"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.10"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.11">b<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.3">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="7.5">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="7.6">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.8">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="7.9">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.10">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>,</l>
					<l n="8" num="2.2"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.3">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="8.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> <w n="8.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="8.6">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.8">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> !</l>
					<l n="9" num="2.3"><w n="9.1">H<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="9.6">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.7"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.9">l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ttr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="10" num="2.4"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.3">s</w>'<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> : <w n="10.6"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="10.7">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.8">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w>-<w n="10.9"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
					<l n="11" num="2.5"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="11.2">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="11.3">c</w>'<w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.5">s<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r</w>, <w n="11.6">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="11.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.8">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="11.9">n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.10">l</w>'<w n="11.11"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.12">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> !…</l>
				</lg>
				<lg n="3">
					<l n="12" num="3.1"><w n="12.1">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="12.2">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="12.3">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.4">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="12.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="12.6">v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w>, <w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.8">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="12.9">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="13.2">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.5">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>… <w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.9"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>…</l>
					<l n="14" num="4.2"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="14.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.3">d<seg phoneme="œ" type="vs" value="1" rule="405">eu</seg>il</w> ! <w n="14.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="14.5">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.6">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="14.7">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.9">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.2">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="15.4">l</w>'<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="15.6">qu</w>'<w n="15.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.8">t<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="15.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="15.10">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.11">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> :</l>
					<l n="16" num="4.4">« <w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="16.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="16.3">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="16.4">s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="16.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w>, <w n="16.6">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="16.7">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.8">t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.9">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="16.10">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! »</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="17.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="17.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.6">t<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.7">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="17.8">t</w>'<w n="17.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
					<l n="18" num="5.2"><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="18.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="18.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="18.5">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="18.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="18.7">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="18.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					<l n="19" num="5.3"><w n="19.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="19.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="19.3">qu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rz<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="19.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="19.6">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.7">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="19.8">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w>,</l>
					<l n="20" num="5.4"><w n="20.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="20.2">c<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="20.4">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="20.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="20.6">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.7">qu</w>'<w n="20.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.9">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="20.10">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="20.11">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
					<l n="21" num="5.5"><w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="21.2">qu</w>'<w n="21.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="21.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="21.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="21.8"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.9">h<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.10">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="22" num="5.6"><w n="22.1">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.2">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="22.3">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="22.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="22.6">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					<l n="23" num="5.7"><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="23.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.4">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.5">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="23.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="23.7">n<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.8">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="23.9"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
					<l n="24" num="5.8"><space unit="char" quantity="4"></space><w n="24.1">C</w>'<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="24.3">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.4">h<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="24.6"><seg phoneme="ø" type="vs" value="1" rule="397">EU</seg>X</w> <w n="24.7">qu</w>'<w n="24.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="24.9">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="24.10">g<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
					<l n="25" num="5.9"><w n="25.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="25.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="25.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="25.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="25.6">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="25.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.9">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="26" num="5.10"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="26.2">qu</w>'<w n="26.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="26.4">v<seg phoneme="œ" type="vs" value="1" rule="405">eu</seg>ill<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.5"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="26.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="26.7">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.8">qu</w>'<w n="26.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="26.10">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="26.11">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="27" num="5.11"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="27.2">qu</w>'<w n="27.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="27.4">n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.5">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.6">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="27.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.8">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="27.9">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="27.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>,</l>
					<l n="28" num="5.12"><w n="28.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="28.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="28.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="28.5">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="28.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="28.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="28.8">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="28.10">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.11">s<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> !</l>
					<l n="29" num="5.13"><w n="29.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.2">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.3">d</w>'<w n="29.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="29.5">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="29.6">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="29.7">s</w>'<w n="29.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="29.9">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.10">R<seg phoneme="ɔ" type="vs" value="1" rule="440">o</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
					<l n="30" num="5.14"><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="30.2">l</w>'<w n="30.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="30.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="30.5"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rph<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ! <w n="30.6"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="30.7">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="30.8">d</w>'<w n="30.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="30.10">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="30.11"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="30.12">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">1er Janvier 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>