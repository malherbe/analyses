<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM7">
				<head type="main">LAOCOON</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.2">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="1.3">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.4">dr<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.7">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="2.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="2.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.4">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="2.5">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.8">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="3.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.4">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.6">pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.9">r<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>str<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> <w n="4.4">qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.6">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.2">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>cs</w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="5.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="5.7">t<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
					<l n="6" num="2.2"><w n="6.1">P<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !… <w n="6.2">L<seg phoneme="a" type="vs" value="1" rule="342">a</seg><seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="o" type="vs" value="1" rule="443">o</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="6.4">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5">t<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rdr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.7">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w>,</l>
					<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.3">b<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="7.5">l</w>’<w n="7.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="7.8">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.9">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="8.2">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="8.4">c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> ! <w n="8.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="8.6">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w>-<w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="8.9">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">D<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="9.2">c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rbr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.6">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="9.7">P<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="9.8">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w>,</l>
					<l n="10" num="3.2"><w n="10.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.4">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="10.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="10.7">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="10.9">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.10"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.11">fl<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w>.</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.3">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="11.5">s</w>’<w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.8">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>pt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>…</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>cr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>… <w n="12.3">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="12.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>, <w n="12.5">v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="12.6">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="12.7">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.8">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>,</l>
					<l n="13" num="4.2"><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="13.2">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="13.3">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="13.4">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w>, <w n="13.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="13.6">l</w>’<w n="13.7"><seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="13.8">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="13.9">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="13.10">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>.</l>
					<l n="14" num="4.3"><w n="14.1">C<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> ! <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="14.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="14.5">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.6">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="14.7">s<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="14.8"><seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>