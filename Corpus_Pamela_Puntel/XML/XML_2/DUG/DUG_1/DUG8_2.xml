<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG8">
				<head type="main">LE HULAN</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.4">r<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="1.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="2.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.4">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="2.6">b<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w></l>
					<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.2">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.3">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.6">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="425">Où</seg></w> <w n="4.2">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.3">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.5">p<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="4.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>…</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="5.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rm<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="6.2">d</w>'<w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>br<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.6">d</w>'<w n="6.7"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="6.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>…</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">J</w>'<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="7.3">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="7.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="7.5">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="7.6">cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>… <w n="7.7">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					<l n="8" num="2.2"><w n="8.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="8.2">l</w>'<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.4">d</w>'<w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.7">br<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>…</l>
					<l n="9" num="2.3"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="9.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="9.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.4">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>.</l>
					<l n="10" num="2.4"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="10.2">j<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.5">c<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="10.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w>,</l>
					<l n="11" num="2.5"><w n="11.1">C<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt</w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="11.3">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>, <w n="11.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="11.5">gr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w></l>
					<l n="12" num="2.6"><w n="12.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="379">e</seg>nt</w> <w n="12.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.4"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>…</l>
					<l n="13" num="2.7">— <w n="13.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="13.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.3">K<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl</w> <w n="13.4">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.6">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="14" num="2.8"><w n="14.1">M<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>-<w n="14.2">t</w>-<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w>, <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.6">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="14.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
					<l n="15" num="2.9"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="15.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.3">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="15.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="15.5">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="15.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="16" num="2.10"><w n="16.1">Qu<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.2">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.3">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="16.6">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w>…</l>
					<l n="17" num="2.11"><w n="17.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="17.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="17.3">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w>-<w n="17.4">j<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="17.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> ?</l>
				</lg>
				<lg n="3">
					<l n="18" num="3.1"><w n="18.1">P<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="18.3">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="18.5">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="18.6">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.7">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="19" num="3.2"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">r<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>pr<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> <w n="19.5">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> :</l>
					<l n="20" num="3.3"><w n="20.1">J</w>'<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="20.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="302">aim</seg></w> ! <w n="20.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.5">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg>s</w> ! —<w n="20.6"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="20.7">l<seg phoneme="o" type="vs" value="1" rule="443">o</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
					<l n="21" num="3.4"><w n="21.1">M<seg phoneme="œ" type="vs" value="1" rule="150">on</seg>s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="396">eu</seg>r</w>, <w n="21.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="21.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="21.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="21.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="21.6">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
					<l n="22" num="3.5"><w n="22.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="22.2">n</w>'<w n="22.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="22.4">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> ! — <w n="22.5">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="22.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w>, <w n="22.7">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
					<l n="23" num="3.6"><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="23.2">v<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="23.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="23.4">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="23.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.6">ch<seg phoneme="ə" type="vi" value="1" rule="356">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="24" num="3.7"><w n="24.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="24.2">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="24.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="24.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="24.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="24.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="24.7">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">V<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.3">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.4">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="25.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="25.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>,</l>
					<l n="26" num="4.2"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="26.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.4">pr<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="26.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="26.6">l</w>'<w n="26.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="27" num="4.3"><w n="27.1">M<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="27.3">r<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="27.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.5">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>…</l>
					<l n="28" num="4.4"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="28.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="28.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="28.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="28.5">gl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="28.6">d</w>'<w n="28.7"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
					<l n="29" num="4.5"><w n="29.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="29.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="29.4">g<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="29.5">qu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="29.6">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="30" num="4.6"><w n="30.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="30.2">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="30.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="30.4">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="30.5">v<seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="30.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="30.7">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> !…</l>
					<l n="31" num="4.7"><w n="31.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="31.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="31.3">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d</w> <w n="31.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt</w> <w n="31.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="31.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.7">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="32" num="4.8"><w n="32.1">J<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="32.2">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="32.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="32.4"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>… <w n="32.5">c</w>'<w n="32.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="32.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>vr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !</l>
					<l n="33" num="4.9"><w n="33.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="33.2">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="33.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="33.4">m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="33.5">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
					<l n="34" num="4.10"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="34.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="34.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ch<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="34.4">r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>b<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="34.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="34.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="34.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w>…</l>
					<l n="35" num="4.11"><w n="35.1">J</w>'<w n="35.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="35.3">t<seg phoneme="ɥ" type="sc" value="0" rule="457">u</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="35.4">l</w>'<w n="35.5">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>, <w n="35.6">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="35.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="35.8">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w></l>
					<l n="36" num="4.12"><w n="36.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="36.2">p<seg phoneme="a" type="vs" value="1" rule="339">â</seg>l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="36.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="36.4">d<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="36.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="36.6">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="36.7">s<seg phoneme="œ" type="vs" value="1" rule="405">eu</seg>il</w> :</l>
					<l n="37" num="4.13"><w n="37.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="37.2">s<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="37.4">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="37.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w></l>
					<l n="38" num="4.14"><w n="38.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="38.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="38.3">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="38.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="38.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="38.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>…</l>
					<l n="39" num="4.15"><w n="39.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="39.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="39.3">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> <w n="39.4">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="39.6">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> !</l>
					<l n="40" num="4.16"><w n="40.1">C</w>'<w n="40.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="40.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>, <w n="40.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="40.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="40.6">l<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="40.7">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w></l>
					<l n="41" num="4.17"><w n="41.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="41.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="41.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="41.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="41.5">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>…</l>
				</lg>
				<lg n="5">
					<l n="42" num="5.1"><w n="42.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="42.2">j</w>'<w n="42.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="42.4">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="42.5">d</w>'<w n="42.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="42.7">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="42.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>… ,</l>
				</lg>
				<lg n="6">
					<l n="43" num="6.1"><w n="43.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="43.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="43.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="43.4">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="43.5"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rph<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w></l>
					<l n="44" num="6.2"><w n="44.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="44.2">r<seg phoneme="ə" type="vi" value="1" rule="350">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="44.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="44.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="44.5">p<seg phoneme="ə" type="vi" value="1" rule="349">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="44.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w>,</l>
					<l n="45" num="6.3"><w n="45.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="45.2">d<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="45.3">l</w>'<w n="45.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="45.5">c<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="45.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="45.7">Rh<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w></l>
					<l n="46" num="6.4"><w n="46.1">V<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="46.2">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> <w n="46.3">d<seg phoneme="œ" type="vs" value="1" rule="405">eu</seg>il</w> <w n="46.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="46.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="vi" value="1" rule="347">e</seg></w> !…</l>
				</lg>
			</div></body></text></TEI>