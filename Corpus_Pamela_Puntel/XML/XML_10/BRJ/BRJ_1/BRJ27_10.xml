<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ27" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="7(abab)">
					<head type="number">XXVII</head>
					<head type="main">L'AURORE BORÉALE</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.3">r<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" mp="P">an</seg>s</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="1.6">ci<seg phoneme="ø" type="vs" value="1" rule="397" place="9">eu</seg>x</w> <w n="1.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="M">om</seg>br<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">Fl<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>ts</w> <w n="2.2">n<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>rs</w> <w n="2.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="2.6" punct="dp:8">v<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</rhyme></w> :</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="3.2"><seg phoneme="y" type="vs" value="1" rule="250" place="2">eû</seg>t</w> <w n="3.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4" mp="C">un</seg></w> <w n="3.5">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5" mp="M">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>l</w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg></w> <w n="3.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="10" mp="P">u</seg>r</w> <w n="3.8" punct="dp:12">P<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="dp">i</seg>s</rhyme></w> :</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3">d</w>'<w n="4.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="5" punct="vg">en</seg>t</w>, <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="4.6" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="5.2">d</w>'<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="5.4">v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="5.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>s</w> <w n="5.8">l</w>'<w n="5.9" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="443" place="10" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>z<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="vg">on</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">M<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="188" place="2">e</seg>t</w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="6.6" punct="pv:8"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="7.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="439" place="5" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="7.5" punct="vg:8">j<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="vg">in</seg></w>, <w n="7.6" punct="vg:10">f<seg phoneme="wa" type="vs" value="1" rule="439" place="9" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="10" punct="vg">er</seg></w>, <w n="7.7" punct="pe:12">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11" mp="M">ai</seg>s<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="pe ps">on</seg></rhyme></w> !…</l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg></w> <w n="8.3">j<seg phoneme="a" type="vs" value="1" rule="306" place="4">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="8.4"><seg phoneme="y" type="vs" value="1" rule="452" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.5" punct="vg:8">fl<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="9.2">d</w>'<w n="9.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4" punct="vg">o</seg>rd</w>, <w n="9.4">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="9.6">d<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>t<seg phoneme="ø" type="vs" value="1" rule="402" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="9.7">cl<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>rt<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg>s</rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="132" place="1">E</seg>h</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="10.3">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="4">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="10.4" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>r<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="11.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="11.4">l</w>'<w n="11.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="11.7">r<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="M">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9">an</seg>s</w> <w n="11.8"><seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="11" mp="M">en</seg>t<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg>s</rhyme></w></l>
						<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">Br<seg phoneme="o" type="vs" value="1" rule="443" place="1">o</seg>d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>s</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>rs</w> <w n="12.4" punct="pv:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1" punct="vg:3">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>t</w>, <w n="13.2">s</w>'<w n="13.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="352" place="4" mp="M">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="13.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.5">r<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="13.6" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>c<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12" punct="vg">o</seg>r</rhyme></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1">V<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="14.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="14.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.4" punct="vg:8">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>mi<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">En</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="15.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="15.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="352" place="9" mp="M">e</seg>ffl<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="15.7">d</w>'<w n="15.8" punct="vg:12"><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12" punct="vg">o</seg>r</rhyme></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="5">a</seg>il</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5" punct="pe:8">l<seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>mi<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1" punct="vg:2">M<seg phoneme="y" type="vs" value="1" rule="d-3" place="1" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="189" place="2" punct="vg">e</seg>t</w>, <w n="17.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="17.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">em</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="17.4">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="7" mp="C">e</seg>t</w> <w n="17.5"><seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">O</seg>c<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10">an</seg></w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="17.7" punct="vg:12">f<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="vg">eu</seg></rhyme></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1">M<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>s<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="18.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="18.6" punct="vg:8">s<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">R<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="19.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg></w> <w n="19.5">sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="19.7" punct="vg:12">Di<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="vg">eu</seg></rhyme></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="20.3">sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="20.5" punct="pe:8">h<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg>s</rhyme></w> !…</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="12" met="6+6"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="21.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="21.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="21.5" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="21.6">n<seg phoneme="wa" type="vs" value="1" rule="439" place="7" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w> <w n="21.7">d</w>'<w n="21.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9">un</seg></w> <w n="21.9">r<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>rd<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="12" punct="pt">en</seg>t</rhyme></w>.</l>
						<l n="22" num="6.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="22.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.4">t<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="5">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="22.5" punct="pv:8">f<seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
						<l n="23" num="6.3" lm="12" met="6+6"><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="23.2">l</w>'<w n="23.3"><seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">O</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4">en</seg>t</w> <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg></w> <w n="23.5" punct="vg:6">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>rd</w>,<caesura></caesura> <w n="23.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="C">u</seg></w> <w n="23.7">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>rd</w> <w n="23.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="23.9">l</w>'<w n="23.10" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="434" place="10" mp="M">O</seg>cc<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>d<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="12" punct="vg">en</seg>t</rhyme></w>,</l>
						<l n="24" num="6.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>g</w> <w n="24.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="24.6" punct="pe:8">t<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>n<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="12" met="6+6"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="25.2" punct="pe:3">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="pe">eu</seg>r</w> ! <w n="25.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="25.4">l</w>'<w n="25.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="25.6" punct="pe:6">v<seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="pe ps" caesura="1">u</seg></w> !<caesura></caesura>… <w n="25.7">J</w>'<w n="25.8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg></w> <w n="25.9">v<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg></w> <w n="25.10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="25.11">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="25.12">y<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="12">eu</seg>x</rhyme></w></l>
						<l n="26" num="7.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="26.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="26.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="352" place="4">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="439" place="5">o</seg>y<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-27" place="7">e</seg></w> <w n="26.4" punct="pe:8">h<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="27" num="7.3" lm="12" met="6+6"><w n="27.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="27.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>g</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="27.4">ru<seg phoneme="i" type="vs" value="1" rule="490" place="4" mp="M">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="27.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" mp="P">an</seg>s</w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="27.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>rg<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>r</w> <w n="27.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="27.9" punct="pe:12">ci<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="pe ps">eu</seg>x</rhyme></w> !…</l>
						<l n="28" num="7.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="28.2" punct="pe:2">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="pe">eu</seg></w> ! <w n="28.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>l</w> <w n="28.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="28.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>g</w> <w n="28.7">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="28.8" punct="pi:8">c<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">24 septembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>