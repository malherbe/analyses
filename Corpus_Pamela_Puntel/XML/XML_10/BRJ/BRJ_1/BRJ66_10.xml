<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ66" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="1[abaab] 1[abbacca]">
					<head type="number">LXVI</head>
					<head type="main">UNE FRANÇAISE</head>
					<lg n="1" type="regexp" rhyme="abaababbacca">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">m<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>t</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="1.4">l</w>'<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="1.6">r<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="1.7">t<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="y" type="vs" value="1" rule="452" place="11" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9" punct="pt:12"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt ti" mp="F">e</seg></rhyme></w>. —</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="2.2">g<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>l</w> <w n="2.3">pr<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="2.5" punct="pv:9">bl<seg phoneme="e" type="vs" value="1" rule="352" place="8" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="9" punct="pv">é</seg></w> ; <w n="2.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>d</w> <w n="2.7" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="422" place="12" punct="pe">oi</seg></rhyme></w> !</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">on</seg>f<seg phoneme="i" type="vs" value="1" rule="481" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg>x</w> <w n="3.5">s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6" caesura="1">oin</seg>s</w><caesura></caesura> <w n="3.6">d</w>'<w n="3.7"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>s</w> <w n="3.9">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="3.10" punct="pt:12">d<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="4.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="M">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.4">v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="6" caesura="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="4.6" punct="vg:8">s<seg phoneme="œ" type="vs" value="1" rule="248" place="8" punct="vg">œu</seg>r</w>, <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="9">en</seg></w> <w n="4.8" punct="vg:10">m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="10" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="11">en</seg></w> <w n="4.10">f<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="192" place="12">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">S<seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="M">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="P">a</seg>r</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="5.5">v<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="5.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="C">on</seg></w> <w n="5.10" punct="pt:12">R<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="422" place="12" punct="pt">oi</seg></rhyme></w>.</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">Gu<seg phoneme="i" type="vs" value="1" rule="484" place="1" mp="M">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="6.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>t</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="6.4" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="6.5">lu<seg phoneme="i" type="vs" value="1" rule="490" place="7" mp="C">i</seg></w> <w n="6.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>d</w> <w n="6.7" punct="vg:10">gr<seg phoneme="a" type="vs" value="1" rule="339" place="9">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="6.8">l</w>'<w n="6.9">h<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>n<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">D</w>'<w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="M">au</seg>g<seg phoneme="y" type="vs" value="1" rule="447" place="3">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.6">lu<seg phoneme="i" type="vs" value="1" rule="490" place="9" mp="C">i</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="M">an</seg>d<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="346" place="12">er</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">S</w>'<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="8.4"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="8.5">f<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="8.6">qu</w>'<w n="8.7"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l</w> <w n="8.8">lu<seg phoneme="i" type="vs" value="1" rule="490" place="8" mp="C">i</seg></w> <w n="8.9">pu<seg phoneme="i" type="vs" value="1" rule="490" place="9">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="438" place="11" mp="M">o</seg>rd<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="346" place="12" punct="pt">er</seg></rhyme></w>.</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="9.2" punct="dp:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>s<seg phoneme="o" type="vs" value="1" rule="443" place="4" mp="M">o</seg>l<seg phoneme="y" type="vs" value="1" rule="452" place="5" mp="M">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="dp in" caesura="1">en</seg>t</w> :<caesura></caesura> « <w n="9.3" punct="vg:8">S<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="9.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="9.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="C">ou</seg>s</w> <w n="9.6" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>bh<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="12">o</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg></rhyme></w> !…</l>
						<l n="10" num="1.10" lm="12" met="6+6">» <w n="10.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="10.5">f<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="10.8">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg></w> <w n="10.9">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="10.10">p<rhyme label="c" id="5" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>s</rhyme></w></l>
						<l n="11" num="1.11" lm="12" met="6+6">» <w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="11.3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="3">oin</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="11.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="11.7"><seg phoneme="u" type="vs" value="1" rule="425" place="7">où</seg></w> <w n="11.8">v<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="C">ou</seg>s</w> <w n="11.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="11.10">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="11">ez</seg></w> <w n="11.11" punct="pe:12">p<rhyme label="c" id="5" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="pe">a</seg>s</rhyme></w> ! »</l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="12.2" punct="pt:3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="pt">i</seg>t</w>. <w n="12.3">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="12.4">R<seg phoneme="wa" type="vs" value="1" rule="422" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="12.5">s</w>'<w n="12.6" punct="ps:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="490" place="8" punct="ps">i</seg>t</w>… <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="188" place="9">e</seg>t</w> <w n="12.8">c<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>rt</w> <w n="12.9" punct="pe:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>c<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>