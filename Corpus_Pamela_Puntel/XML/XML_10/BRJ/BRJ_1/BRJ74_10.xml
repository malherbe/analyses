<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ74" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="5(abab)">
					<head type="number">LXXIV</head>
					<head type="main">SAUTE MARQUIS !</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:2">Vr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="1" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="2" punct="vg">en</seg>t</w>, <w n="1.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rqu<seg phoneme="i" type="vs" value="1" rule="490" place="4" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="1.3">t<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="1.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="C">ou</seg>s</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="1.6">d<seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="1.7" punct="pe:10">b<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" mp="Lp">en</seg>ds</w>-<w n="2.3">t<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.6">d<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="2.7">p<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>r</w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.9" punct="pi:10">b<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">â</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pi ti">on</seg></rhyme></w> ? —</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="3.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="3.5">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="3.6" punct="vg:10">cr<seg phoneme="i" type="vs" value="1" rule="466" place="8" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="466" place="9" mp="M">i</seg>n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">l</w>'<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" mp="M">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6" mp="C">un</seg></w> <w n="4.6">p<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.7" punct="pi:10">m<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pi">on</seg></rhyme></w> ?</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="5.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">l<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="5.5">d<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="5.7" punct="vg:10">gu<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="6.3">pr<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="6.5">f<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">â</seg>ch<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>x</w> <w n="6.6" punct="pv:10">l<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" mp="M">en</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="10" punct="pv">ain</seg>s</rhyme></w> ;</l>
						<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="7.2">l</w>'<w n="7.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" mp="M">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="7.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.6">s</w>'<w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="7.8">d<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>t</w> <w n="7.9" punct="vg:10">gu<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="vg">E</seg>t</w>, <w n="8.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="8.4" punct="vg:4">j<seg phoneme="y" type="vs" value="1" rule="449" place="4" punct="vg" caesura="1">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="C">i</seg>l</w> <w n="8.6">s</w>'<w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.9">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="8.10" punct="pe:10">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="10" punct="pe">ain</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="9.2">s</w>'<w n="9.3" punct="pe:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="3" mp="M">im</seg>pr<seg phoneme="i" type="vs" value="1" rule="466" place="4" punct="pe ps ti" caesura="1">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !<caesura></caesura>… — <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="414" place="5">ô</seg></w> <w n="9.5">pl<seg phoneme="y" type="vs" value="1" rule="452" place="6">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="9.6" punct="vg:10">j<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="10.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="10.5">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt</w> <w n="10.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8">en</seg></w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="9">en</seg></w> <w n="10.8" punct="pv:10">c<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pv">ou</seg>r</rhyme></w> ;</l>
						<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="11.4">l</w>'<w n="11.5"><seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="6">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="7" mp="M">im</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>s</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="12.3">p<seg phoneme="o" type="vs" value="1" rule="314" place="4" caesura="1">eau</seg></w><caesura></caesura> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="12.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>rqu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>s</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="12.7" punct="pe:10">Gr<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>c<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="10" punct="pe">ou</seg>rt</rhyme></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1" punct="pe:1">V<seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">a</seg></w> ! <w n="13.2">c</w>'<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="13.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" caesura="1">ain</seg></w><caesura></caesura> <w n="13.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.7">t<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="C">u</seg></w> <w n="13.8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="452" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="13.9">d</w>'<w n="13.10"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="14.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2" mp="M">ai</seg>d<seg phoneme="wa" type="vs" value="1" rule="439" place="3" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="14.3">mi<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>ll<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="14.5" punct="pv:10">c<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="352" place="9" mp="M">e</seg>ss<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pv">an</seg>t</rhyme></w> ;</l>
						<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="15.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434" place="3" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" caesura="1">aî</seg>t</w><caesura></caesura> <w n="15.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rs</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="15.5">D<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w> <w n="15.6">D<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>c<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="16.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="C">on</seg></w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>rj<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="188" place="8">e</seg>t</w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="16.8" punct="pe:10">s<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pe">an</seg>g</rhyme></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="10" met="4+6"><w n="17.1" punct="vg:2">S<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="17.2" punct="pe:4">m<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rqu<seg phoneme="i" type="vs" value="1" rule="490" place="4" punct="pe" caesura="1">i</seg>s</w> !<caesura></caesura> <w n="17.3" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="188" place="5" punct="vg">e</seg>t</w>, <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="17.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="17.6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>x</w> <w n="17.7" punct="vg:10">t<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="18" num="5.2" lm="10" met="4+6"><w n="18.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" mp="Lp">ai</seg>s</w>-<w n="18.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="18.3">v<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>l<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="18.5">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>ts</w> <w n="18.6">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="18.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="8">en</seg></w> <w n="18.8" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>cqu<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="490" place="10" punct="pv">i</seg>s</rhyme></w> ;</l>
						<l n="19" num="5.3" lm="10" met="4+6"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="19.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="19.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="19.5">d<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="19.6">f<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s</w> <w n="19.7">s</w>'<w n="19.8"><seg phoneme="i" type="vs" value="1" rule="496" place="7">y</seg></w> <w n="19.9">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="19.10" punct="pv:10">pr<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="20" num="5.4" lm="10" met="4+6"><w n="20.1">C</w>'<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="20.4">d</w>'<w n="20.5" punct="pe:4"><seg phoneme="y" type="vs" value="1" rule="452" place="4" punct="pe ps" caesura="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !<caesura></caesura>… <w n="20.6" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="pe">on</seg>s</w> ! <w n="20.7" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="20.8" punct="pe:10">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>rqu<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="490" place="10" punct="pe">i</seg>s</rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>