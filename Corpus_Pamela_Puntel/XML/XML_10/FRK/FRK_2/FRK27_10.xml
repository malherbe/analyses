<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉPAVES</head><div type="poem" key="FRK27" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="5(abab)">
					<head type="main">LA REVANCHE</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" punct="pe">O</seg>H</w> ! <w n="1.2" punct="vg:2">ou<seg phoneme="i" type="vs" value="1" rule="490" place="2" punct="vg">i</seg></w>, <w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="C">a</seg></w> <w n="1.5" punct="vg:6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="1.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="456" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="2.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="2.3">g<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="481" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="2.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="10">en</seg>ts</w> <w n="2.8" punct="vg:12">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" mp="M">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.4" punct="vg:6">r<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="3.7">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="490" place="11">i</seg></w> <w n="3.9" punct="vg:12">t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="456" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6" punct="pe:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="M">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="M">an</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="pe">é</seg></rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="5.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2" mp="P">an</seg>s</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="5.4">f<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.5" punct="tc:6">h<seg phoneme="y" type="vs" value="1" rule="452" place="5" mp="M">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="304" place="6" punct="ti" caesura="1">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> —<caesura></caesura> <w n="5.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7">où</seg></w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="5.8">r<seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>s</w> <w n="5.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>t</w> <w n="5.10">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="5.11" punct="tc:12">v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ti" mp="F">e</seg>s</rhyme></w> —</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" mp="M">in</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" mp="M">in</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="6.4">s</w>’<w n="6.5"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="M">an</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="6.6" punct="pv:12">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>rg<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="12" punct="pv">en</seg>t</rhyme></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="7.5">fl<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="7.6">n<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rds</w> <w n="7.8" punct="vg:12">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>d<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="8.3">s<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>l</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.5">j<seg phoneme="wa" type="vs" value="1" rule="422" place="6" caesura="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="8.7">d</w>’<w n="8.8" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="M">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="12" punct="pe">en</seg>t</rhyme></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="9.3">p<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="9.4" punct="vg:6">m<seg phoneme="œ" type="vs" value="1" rule="406" place="5" mp="M">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="9.6" punct="dp:9">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" punct="dp in">an</seg>t</w> : « <w n="9.7" punct="pe:12">D<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>vr<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !»</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.3" punct="vg:6">Ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="10.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="10.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10">en</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="10.8" punct="vg:12">p<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="vg">a</seg>s</rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="C">eu</seg>rs</w> <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="443" place="8">o</seg></w> <w n="11.7" punct="dp:11">r<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" mp="M">on</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="11" punct="dp in">a</seg></w> : « <w n="11.8" punct="pe:12">Fr<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !»</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="12.3" punct="tc:4">b<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="314" place="4" punct="ti">eau</seg>x</w> — <w n="12.4" punct="tc:6">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5" mp="M">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="ti" caesura="1">i</seg>fs</w> —<caesura></caesura> <w n="12.5" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg>t</w>, <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="12.7">t<seg phoneme="u" type="vs" value="1" rule="424" place="11">ou</seg>t</w> <w n="12.8" punct="dp:12">b<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="dp">a</seg>s</rhyme></w> :</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6">« <w n="13.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" punct="pe">O</seg>h</w> ! <w n="13.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3">en</seg>t</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="13.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.5" punct="vg:8">cr<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg></w>, <w n="13.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="13.7">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="10">om</seg></w> <w n="13.8">qu<seg phoneme="i" type="vs" value="1" rule="490" place="11">i</seg></w> <w n="13.9" punct="pi:12">p<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi ps" mp="F">e</seg></rhyme></w> ?…»</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="vg">E</seg>t</w>, <w n="14.2">s<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="14.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="14.4">qu</w>’<w n="14.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="14.6" punct="vg:6">v<seg phoneme="o" type="vs" value="1" rule="317" place="6" punct="vg" caesura="1">au</seg>t</w>,<caesura></caesura> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="14.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">om</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>t</w> <w n="14.9">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10" mp="C">e</seg>s</w> <w n="14.10" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>l<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12" punct="vg">ai</seg>s</rhyme></w>,</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>ls</w> <w n="15.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="15.3">gl<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="15.5">l<seg phoneme="œ" type="vs" value="1" rule="406" place="8" mp="C">eu</seg>r</w> <w n="15.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg></w> <w n="15.7">qu<seg phoneme="i" type="vs" value="1" rule="490" place="10">i</seg></w> <w n="15.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="15.9">l<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="16.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="2">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="C">au</seg>x</w> <w n="16.4">l<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rds</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="314" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="16.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="16.8">l<seg phoneme="œ" type="vs" value="1" rule="406" place="10" mp="C">eu</seg>rs</w> <w n="16.9" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>l<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12" punct="pe">ai</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="12" met="6+6">—« <w n="17.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" punct="pe">O</seg>h</w> ! <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="17.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="17.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="M">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="17.5">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" caesura="1">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="17.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7" mp="C">un</seg></w> <w n="17.7">c<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rc<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9">an</seg></w> <w n="17.8"><seg phoneme="o" type="vs" value="1" rule="317" place="10" mp="C">au</seg>x</w> <w n="17.9" punct="pi:12">D<seg phoneme="ɔ" type="vs" value="1" rule="438" place="11" mp="M">o</seg>ctr<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="466" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg>s</rhyme></w> ?»</l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1" punct="pe:3">P<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="M">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="18.2">Pl<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg></w> <w n="18.4" punct="pe:6">Dr<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="pe" caesura="1">oi</seg>t</w> !<caesura></caesura> <w n="18.5">c<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r</w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="18.7">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">in</seg>s</w> <w n="18.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11">on</seg>t</w> <w n="18.9" punct="vg:12">m<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="444" place="12" punct="vg">û</seg>rs</rhyme></w>,</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="19.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="2">en</seg></w> <w n="19.3">qu</w>’<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>c</w> <w n="19.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="19.6">cr<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="19.7">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" mp="M">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w> <w n="19.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="19.9">n<seg phoneme="o" type="vs" value="1" rule="437" place="10" mp="C">o</seg>s</w> <w n="19.10" punct="vg:12">p<seg phoneme="wa" type="vs" value="1" rule="419" place="11" mp="M">oi</seg>tr<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="466" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="20.2">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="20.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="20.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="20.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ttr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>s</w> <w n="20.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="20.8" punct="pe:12">m<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="pe">u</seg>rs</rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>