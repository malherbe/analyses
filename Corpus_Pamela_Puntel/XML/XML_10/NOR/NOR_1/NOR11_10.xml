<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR11" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(aabbcc)">
				<head type="main">BATAILLE DE CHAMPIGNY</head>
				<opener>
					<dateline>
						<date when="1870">Plateau d’Avron, 2 décembre.</date>
					</dateline>
				</opener>
				<lg n="1" type="sizain" rhyme="aabbcc">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">ON</seg></w> <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="1.3">qu</w>’<w n="1.4"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ls</w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="1.7">fu<seg phoneme="i" type="vs" value="1" rule="490" place="6" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="1.9">qu</w>’<w n="1.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w> <w n="1.11">v<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="1.12">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.13">l</w>'<w n="1.14" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>v<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pe">an</seg>t</rhyme></w> !</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="2.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>gs</w> <w n="2.3">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">on</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>ts</w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>rds</w><caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>s</w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="2.7">v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="12">en</seg>t</rhyme></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="3.2">tr<seg phoneme="e" type="vs" value="1" rule="352" place="2" mp="M">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="306" place="3" mp="M">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="3.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="5" mp="C">o</seg>s</w> <w n="3.4">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6" caesura="1">œu</seg>rs</w><caesura></caesura> <w n="3.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="3.7">cr<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>s</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="3.9" punct="pt:12">j<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="4.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4" mp="C">un</seg></w> <w n="4.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6" caesura="1">è</seg>s</w><caesura></caesura> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="9">e</seg>l</w> <w n="4.9">n<seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="C">ou</seg>s</w> <w n="4.10" punct="pi:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>v<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">R<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="M">au</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="5.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="5.4">f<seg phoneme="ø" type="vs" value="1" rule="397" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="5.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="5.6">h<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>s</w> <w n="5.7" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">â</seg>l<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">Lu<seg phoneme="i" type="vs" value="1" rule="490" place="1" mp="M/mp">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="Lp">a</seg>s</w>-<w n="6.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="6.4" punct="vg:6">n<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="314" place="6" punct="vg" caesura="1">eau</seg></w>,<caesura></caesura> <w n="6.5">b<seg phoneme="o" type="vs" value="1" rule="314" place="7">eau</seg></w> <w n="6.6">s<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="9">e</seg>il</w> <w n="6.7">d</w>’<w n="6.8" punct="pi:12"><seg phoneme="o" type="vs" value="1" rule="317" place="10" mp="M">Au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>rl<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="pi">i</seg>tz</rhyme></w> ?</l>
				</lg>
			</div></body></text></TEI>