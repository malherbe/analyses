<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR8" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="6((aa))">
				<opener>
					<dateline>
						<date when="1870">Bobigny. novembre 1870.</date>
					</dateline>
				</opener>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" punct="pe">O</seg>h</w> ! <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">c</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="1.5">tr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="1.7">fr<seg phoneme="wa" type="vs" value="1" rule="419" place="6" caesura="1">oi</seg>d</w><caesura></caesura> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="1.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="Lc">an</seg>d</w>’<w n="1.10" punct="vg:10">g<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="1.11">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="1.12" punct="pe:12">nu<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="490" place="12" punct="pe">i</seg>t</rhyme></w> !</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>ps</w> <w n="2.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.5" punct="vg:6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">on</seg>g</w>,<caesura></caesura> <w n="2.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d</w> <w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="9">e</seg>st</w> <w n="2.9" punct="vg:10">s<seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="vg">eu</seg>l</w>, <w n="2.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="11" mp="P">an</seg>s</w> <w n="2.11" punct="vg:12">bru<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="490" place="12" punct="vg">i</seg>t</rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="3.3" punct="vg:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">am</seg>p</w>, <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg></w> <w n="3.5">m<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="397" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="i" type="vs" value="1" rule="466" place="8" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="11">é</seg></w> <w n="3.9" punct="pe:12">s<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="4.3">r<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="4">e</seg>ils</w> <w n="4.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="6" punct="vg" caesura="1">ain</seg>s</w>,<caesura></caesura> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="4.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="11" mp="P">an</seg>s</w> <w n="4.8" punct="pe:12">n<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="5" num="1.5" lm="12" met="6+6">« <w n="5.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg>x</w> <w n="5.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pe" mp="F">e</seg>s</w> ! <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg>x</w> <w n="5.4" punct="pe:6">cr<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>n<seg phoneme="o" type="vs" value="1" rule="314" place="6" punct="pe" caesura="1">eau</seg>x</w> !<caesura></caesura> » <w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">On</seg></w> <w n="5.6" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="9" punct="vg">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" mp="C">on</seg></w> <w n="5.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>tt<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12" punct="vg">en</seg>d</rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">pu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="6.3" punct="pt:3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3" punct="pt ti">en</seg></w>. — <w n="6.4" punct="vg:6">F<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>gu<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="314" place="9">eau</seg></w> <w n="6.7">l</w>’<w n="6.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg></w> <w n="6.9">s</w>’<w n="6.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>t<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12" punct="vg">en</seg>d</rhyme></w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="7.2">t<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="P">u</seg>r</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>c</w><caesura></caesura> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="7.7">m<seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="M">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="7.9" punct="pt:12">gu<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">S</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M/mp">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="Lp">o</seg>rt</w>-<w n="8.3" punct="dp:3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="dp">on</seg></w> : <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="M">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="6" caesura="1">ô</seg>t</w><caesura></caesura> <w n="8.5"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="8.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg></w> <w n="8.7">p<seg phoneme="ø" type="vs" value="1" rule="397" place="10">eu</seg></w> <w n="8.8">l<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>g<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="9.2" punct="vg:3">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="C">ou</seg>s</w> <w n="9.4" punct="pv:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pv" caesura="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ;<caesura></caesura> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="9">ô</seg>t</w> <w n="9.6"><seg phoneme="y" type="vs" value="1" rule="452" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="9.7">v<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>x</rhyme></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="10.2" punct="dp:2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="dp in">i</seg>t</w> : « <w n="10.3">C</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="10.5">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="10.6" punct="pv:6">t<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="pv" caesura="1">ou</seg>r</w> ;<caesura></caesura> <w n="10.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg>s</w>, <w n="10.8">n<seg phoneme="y" type="vs" value="1" rule="452" place="9" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="443" place="11">o</seg></w> <w n="10.9">tr<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>s</rhyme></w> »</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">p<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.3">n<seg phoneme="y" type="vs" value="1" rule="452" place="4" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="443" place="6" caesura="1">o</seg></w><caesura></caesura> <w n="11.4" punct="vg:7">b<seg phoneme="a" type="vs" value="1" rule="306" place="7" punct="vg">â</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="11.5"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="11.7" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">â</seg>ch<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="12.4" punct="dp:6">m<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="M">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="dp in" caesura="1">an</seg>t</w> :<caesura></caesura> « <w n="12.5" punct="vg:7">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="7" punct="vg">eu</seg></w>, <w n="12.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="12.7">c</w>’<w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="9">e</seg>st</w> <w n="12.9" punct="vg:10">b<seg phoneme="o" type="vs" value="1" rule="314" place="10" punct="vg">eau</seg></w>, <w n="12.10">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="12.11" punct="pe:12">gl<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> ! »</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabbcc">
					<l n="13" num="2.1" lm="12" met="6+6"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="13.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4">e</seg>l</w> <w n="13.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">â</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="13.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d</w> <w n="13.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="P">u</seg>r</w> <w n="13.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="13.9">pr<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg>s</w> <w n="13.10">d<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>s<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rts</rhyme></w></l>
					<l n="14" num="2.2" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">br<seg phoneme="u" type="vs" value="1" rule="426" place="2" mp="M">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd</w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="C">u</seg></w> <w n="14.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="14.5" punct="vg:7">gl<seg phoneme="i" type="vs" value="1" rule="467" place="7" punct="vg">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="188" place="8">e</seg>t</w> <w n="14.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="14.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" mp="P">an</seg>s</w> <w n="14.9">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="14.10"><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">ai</seg>rs</rhyme></w></l>
					<l n="15" num="2.3" lm="12" met="6+6"><w n="15.1">Sc<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1" mp="M">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="15.3">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="5" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="M">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="9">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.7" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="443" place="10" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="M">an</seg>g<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="16" num="2.4" lm="12" met="6+6"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="16.2" punct="vg:4">f<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="16.3" punct="dp:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="358" place="5" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="490" place="6" punct="dp" caesura="1">i</seg></w> :<caesura></caesura> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="16.7">s<seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="M">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>g<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="17" num="2.5" lm="12" met="6+6"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="17.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="17.3">s</w>’<w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" mp="P">an</seg>s</w> <w n="17.6" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="vg">ou</seg>r</rhyme></w>,</l>
					<l n="18" num="2.6" lm="12" met="6+6"><w n="18.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="18.3">nu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>t</w> <w n="18.4">s</w>’<w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6" caesura="1">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="18.6"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg>x</w> <w n="18.7">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg>s</w> <w n="18.8">f<seg phoneme="ø" type="vs" value="1" rule="397" place="10">eu</seg>x</w> <w n="18.9">d<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="C">u</seg></w> <w n="18.10" punct="pt:12">j<rhyme label="c" id="9" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="pt">ou</seg>r</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>