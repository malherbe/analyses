<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM18" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
					<head type="main">THÉODORE KŒRNER</head>
					<head type="number">I</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="1.2">h<seg phoneme="i" type="vs" value="1" rule="492" place="2">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="1.4">l</w>’<w n="1.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>,<caesura></caesura> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="414" place="7">ô</seg></w> <w n="1.7">K<seg phoneme="e" type="vs" value="1" rule="249" place="8" mp="M">œ</seg>rn<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="1.8" punct="vg:12">Th<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg><seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">En</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="2" mp="M">aî</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg></w> <w n="2.3">p<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>l</w><caesura></caesura> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="2.5">j<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.6" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg>ds</rhyme></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="3.2" punct="vg:3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>s</w>, <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="3.5">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="3.6">sc<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7" mp="M">e</seg>ll<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="3.7">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="3.8" punct="dp:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="10" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="466" place="11" mp="M">i</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12" punct="dp">en</seg>ts</rhyme></w> :</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">B<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fm">e</seg></w>-<w n="4.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M/mc">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="492" place="4" punct="vg">y</seg>r</w>, <w n="4.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="4.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="6" caesura="1">om</seg></w><caesura></caesura> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.6">d<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</w> <w n="4.7">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="9" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>s</w> <w n="4.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.9" punct="pt:12">d<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="5.2" punct="vg:2">v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="2" punct="vg">e</seg>rs</w>, <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="5.4">fr<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="5.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg></w> <w n="5.8" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>n<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1" mp="M">In</seg>sp<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="6.2">l</w>’<w n="6.3">h<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg><seg phoneme="i" type="vs" value="1" rule="476" place="6" caesura="1">ï</seg>sm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="6.6">p<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>rs</w> <w n="6.7" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>v<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="M">oû</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12" punct="pv">en</seg>ts</rhyme></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="7.2">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="M">o</seg>rs<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="63" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="7.6">br<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>t</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10" mp="C">e</seg>s</w> <w n="7.8" punct="pv:12">t<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="M">ou</seg>rm<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12" punct="pv">en</seg>ts</rhyme></w> ;</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.5">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="6" caesura="1">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="d-3" place="9" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>t</w> <w n="8.8">l</w>’<w n="8.9" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="317" place="11" mp="M">au</seg>r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">T<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="9.2" punct="vg:3">l<seg phoneme="i" type="vs" value="1" rule="492" place="2">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="9.3">m<seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="M">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="9.5">j<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>g</w> <w n="9.6" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>st<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="P">ou</seg>r</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="C">a</seg></w> <w n="10.4">j<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="10.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="P">ou</seg>r</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="10.8" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>rt<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="11.2">p<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="11.3">r<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="11.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="11.5">cr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="11.7" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>vr<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:3">P<seg phoneme="o" type="vs" value="1" rule="443" place="1" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="413" place="2">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.6">d</w>’<w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="11">un</seg></w> <w n="12.8" punct="pt:12">r<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="12" punct="pt">oi</seg></rhyme></w>.</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="13.2">c<seg phoneme="œ" type="vs" value="1" rule="248" place="2">œu</seg>rs</w> <w n="13.3">g<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" mp="M">e</seg>rm<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4">ain</seg>s</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="13.5">fu<seg phoneme="i" type="vs" value="1" rule="490" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="13.6">l</w>’<w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.8">b<seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="13.9" punct="vg:12">f<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="422" place="12" punct="vg">oi</seg></rhyme></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="14.4"><seg phoneme="i" type="vs" value="1" rule="466" place="4" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="14.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>t</w> <w n="14.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="P">ou</seg>r</w> <w n="14.7">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="14.8" punct="pt:12">Fr<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>