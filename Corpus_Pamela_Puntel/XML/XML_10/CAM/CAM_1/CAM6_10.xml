<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM6" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
				<head type="main">LE SOLDAT MOURANT</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">dr<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg></w> <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="1.6">c<seg phoneme="œ" type="vs" value="1" rule="248" place="8">œu</seg>r</w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="10">o</seg>r</w> <w n="1.8" punct="pt:12">tr<seg phoneme="e" type="vs" value="1" rule="352" place="11" mp="M">e</seg>ss<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.4">su<seg phoneme="i" type="vs" value="1" rule="490" place="4" mp="M">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg></w> <w n="2.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="2.6">s<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>s</w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="2.8">f<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.9" punct="ps:12">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="M">on</seg>d<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="ps">an</seg>ts</rhyme></w>…</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:4">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="3.3">m<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" caesura="1">e</seg>ts</w><caesura></caesura> <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="3.5">g<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.7">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="3.8" punct="vg:12">d<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="12" punct="vg">en</seg>ts</rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="pe:2">M<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="M">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="pe">i</seg></w> ! <w n="4.2">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.4">t<seg phoneme="wa" type="vs" value="1" rule="422" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="4.5">s</w>’<w n="4.6"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="4.8" punct="pt:12">m<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>tr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ts</w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="5.5" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>b<seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="P">a</seg>r</w> <w n="5.7"><seg phoneme="y" type="vs" value="1" rule="452" place="8" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="402" place="10">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg>t</w> <w n="6.2">tr<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="C">a</seg></w> <w n="6.4">p<seg phoneme="wa" type="vs" value="1" rule="419" place="5" mp="M">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="466" place="6" caesura="1">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="6.6">p<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>tr<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg></w> <w n="6.7" punct="pt:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>d<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="12" punct="pt">an</seg>s</rhyme></w>.</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2" punct="ps:3">br<seg phoneme="y" type="vs" value="1" rule="444" place="2">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="ps" mp="F">e</seg></w>… <w n="7.3">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" mp="P">an</seg>s</w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="7.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>rs</w><caesura></caesura> <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t</w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="7.8">ch<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>rb<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>s</w> <w n="7.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>rd<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="12" punct="pt">en</seg>ts</rhyme></w>.</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t</w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.5">m<seg phoneme="wa" type="vs" value="1" rule="422" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="8.6">s<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="P">u</seg>r</w> <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">am</seg>p</w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.10" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="pt:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="pt">eu</seg></w>. <w n="9.2">C<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>rs</w> <w n="9.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.4" punct="pt:6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="pt" caesura="1">er</seg></w>.<caesura></caesura> <w n="9.5">V<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s</w> <w n="9.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="9.7">m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="o" type="vs" value="1" rule="317" place="10" mp="C">au</seg></w> <w n="9.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="12">ou</seg>r</rhyme></w></l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="Lp">en</seg>ds</w>-<w n="10.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.4">m<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>d<seg phoneme="a" type="vs" value="1" rule="306" place="5" mp="M">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="10.5">b<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="P">a</seg>r</w> <w n="10.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" mp="C">on</seg></w> <w n="10.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>m<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="pt">ou</seg>r</rhyme></w>.</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="Lp">i</seg>s</w>-<w n="11.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="11.3">qu</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" mp="M">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="11.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.8">s<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11">ai</seg>s</w> <w n="11.9">d</w>’<w n="11.10" punct="pv:12"><rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="Lp">i</seg>s</w>-<w n="12.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="12.3" punct="vg:3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" punct="vg">e</seg></w>, <w n="12.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="P">ou</seg>r</w> <w n="12.5">s<seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="M">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="10">en</seg></w> <w n="12.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>m<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="12" punct="vg">oi</seg></rhyme></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>t</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="13.3">j<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="13.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="13.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t</w> <w n="13.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">om</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>s</w> <w n="13.7">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="13.8" punct="vg:12">m<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="422" place="12" punct="vg">oi</seg></rhyme></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="14.2" punct="vg:2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" punct="vg">e</seg></w>, <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="P">a</seg>r</w> <w n="14.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="14.5" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>rt</w>,<caesura></caesura> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="14.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="9">e</seg>st</w> <w n="14.9" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="466" place="10" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="438" place="11" mp="M">o</seg>rt<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>