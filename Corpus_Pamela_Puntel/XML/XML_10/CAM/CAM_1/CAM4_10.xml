<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM4" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
				<head type="main">TOMBEAU DE DAGOBERT ET DE DUGOMMIER</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">H<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="1.2" punct="vg:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="6" punct="vg" caesura="1">ain</seg>s</w>,<caesura></caesura> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg></w> <w n="1.4">pi<seg phoneme="e" type="vs" value="1" rule="240" place="8">e</seg>d</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="1.6" punct="vg:12">P<seg phoneme="i" type="vs" value="1" rule="492" place="10" mp="M">y</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="2.2">R<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>v<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="M">u</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" mp="P">an</seg>s</w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="e" type="vs" value="1" rule="352" place="8" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>rt</w> <w n="2.6">c<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>s</w> <w n="2.7" punct="pt:12"><seg phoneme="y" type="vs" value="1" rule="452" place="11" mp="M">u</seg>n<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="pt">i</seg>t</rhyme></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="3.2">d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="M">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="3.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>s</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.7" punct="pt:12">gr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>n<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="pt">i</seg>t</rhyme></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="4.3">v<seg phoneme="i" type="vs" value="1" rule="481" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="4.6">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="4.8" punct="pt:12">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10" mp="M">e</seg>st<seg phoneme="i" type="vs" value="1" rule="466" place="11" mp="M">i</seg>n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="5.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">în</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="5.4">d<seg phoneme="ø" type="vs" value="1" rule="397" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="5.6">l<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="5.7" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="10" mp="M">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="466" place="11" mp="M">i</seg>n<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>t</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="6.5">v<seg phoneme="o" type="vs" value="1" rule="437" place="5" mp="C">o</seg>s</w> <w n="6.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="6" caesura="1">om</seg>s</w><caesura></caesura> <w n="6.7">j<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s</w> <w n="6.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="6.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.10" punct="pt:12">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>rn<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="pt">i</seg>t</rhyme></w>.</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">R<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="7.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rn<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="7.7">b<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>n<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>t</rhyme></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="8.2">ph<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="P">a</seg>r</w> <w n="8.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="8.7">gl<seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="10" mp="M">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="11" mp="M">aî</seg>n<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:3">D<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>g<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3" punct="vg">e</seg>rt</w>, <w n="9.2" punct="vg:6">D<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="M">u</seg>g<seg phoneme="o" type="vs" value="1" rule="434" place="5" mp="M">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="9.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="8">ez</seg></w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9">un</seg></w> <w n="9.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>g</w> <w n="9.7" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>mm<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="381" place="12" punct="pt">e</seg>il</rhyme></w>.</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>s</w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="10.3">b<seg phoneme="o" type="vs" value="1" rule="314" place="3">eau</seg>x</w> <w n="10.4">ci<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="425" place="5">où</seg></w> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="10.9">l</w>’<w n="10.10"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>r</w> <w n="10.11">d<seg phoneme="y" type="vs" value="1" rule="449" place="10" mp="C">u</seg></w> <w n="10.12" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>l<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="381" place="12" punct="vg">e</seg>il</rhyme></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M">ou</seg>ffl<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="11.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="11.3">h<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg><seg phoneme="i" type="vs" value="1" rule="476" place="6" caesura="1">ï</seg>sm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w> <w n="11.6">n<seg phoneme="o" type="vs" value="1" rule="437" place="9" mp="C">o</seg>s</w> <w n="11.7">j<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="11.8" punct="pt:12">br<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg></w> <w n="12.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>t</w><caesura></caesura> <w n="12.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="12.7">l<seg phoneme="œ" type="vs" value="1" rule="406" place="10" mp="C">eu</seg>rs</w> <w n="12.8" punct="dp:12"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="M">a</seg>ï<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="dp">eu</seg>x</rhyme></w> :</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>ls</w> <w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.4">br<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="13.5" punct="vg:6">v<seg phoneme="a" type="vs" value="1" rule="306" place="5" mp="M">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.7">c<seg phoneme="œ" type="vs" value="1" rule="248" place="8">œu</seg>r</w> <w n="13.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="317" place="9" mp="M">au</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="e" id="7" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="14.2">pr<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="14.3">l<seg phoneme="œ" type="vs" value="1" rule="406" place="5" mp="C">eu</seg>r</w> <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>g</w><caesura></caesura> <w n="14.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>r</w> <w n="14.6">n</w>’<w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.8">p<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>s</w> <w n="14.9" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>scl<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>