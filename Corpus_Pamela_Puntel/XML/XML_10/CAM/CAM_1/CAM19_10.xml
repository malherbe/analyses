<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM19" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
					<head type="main">THÉODORE KŒRNER</head>
					<head type="number">II</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="C">u</seg></w> <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s</w> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5" mp="M">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="1.6" punct="vg:9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="1.7">t<seg phoneme="y" type="vs" value="1" rule="449" place="10" mp="C">u</seg></w> <w n="1.8" punct="dp:12">d<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12" punct="dp">ai</seg>s</rhyme></w> :</l>
						<l n="2" num="1.2" lm="12" met="6+6">« <w n="2.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ls</w> <w n="2.2">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="2.3" punct="vg:6">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">on</seg>t<seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.6" punct="pe:12"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="443" place="10" mp="M">o</seg>m<seg phoneme="i" type="vs" value="1" rule="466" place="11" mp="M">i</seg>n<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1" mp="M">In</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="3" punct="vg">é</seg>s</w>, <w n="3.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="3.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="3.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="3.7" punct="vg:12">t<seg phoneme="i" type="vs" value="1" rule="492" place="10" mp="M">y</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>nn<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="4.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="C">ou</seg>s</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="6" caesura="1">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="4.7">v<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>ls</w> <w n="4.8" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12" punct="pt">ê</seg>ts</rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6">« <w n="5.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="P">à</seg></w> <w n="5.3" punct="pe:4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="pe">ou</seg>s</w> ! <w n="5.4">v<seg phoneme="o" type="vs" value="1" rule="437" place="5" mp="C">o</seg>s</w> <w n="5.5" punct="vg:6">ci<seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="5.6">v<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="5.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">am</seg>ps</w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="188" place="9">e</seg>t</w> <w n="5.9">v<seg phoneme="o" type="vs" value="1" rule="437" place="10" mp="C">o</seg>s</w> <w n="5.10">f<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>r<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">ê</seg>ts</rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Pl<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="6.3">l<seg phoneme="wa" type="vs" value="1" rule="439" place="4" mp="M">o</seg>y<seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="M">au</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.5">v<seg phoneme="o" type="vs" value="1" rule="437" place="8" mp="C">o</seg>s</w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="6.7" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>nn<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" mp="M/mp">O</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mp">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="3" mp="Lp">ez</seg></w>-<w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="7.3" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="7.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ls</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="7.7" punct="vg:12">G<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10" mp="M">e</seg>rm<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>n<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="8.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="8.3">l<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>s</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="8.5">b<seg phoneme="o" type="vs" value="1" rule="314" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="8.6">p<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10" mp="C">e</seg>s</w> <w n="8.8" punct="pi:12">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>cr<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="12" punct="pi">e</seg>ts</rhyme></w> ?</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6">« <w n="9.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="9.2">pr<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="9.4">l</w>’<w n="9.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>rt</w>, <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="9.8" punct="pe:12">sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="c" id="5" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1" mp="Lp">E</seg>st</w>-<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>c</w> <w n="10.4">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" caesura="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="10.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="10.8" punct="pi:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" mp="M">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="c" id="5" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="11.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="11.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="11.6">n</w>’<w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="11.8">p<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>s</w> <w n="11.9">n<seg phoneme="o" type="vs" value="1" rule="437" place="11" mp="C">o</seg>s</w> <w n="11.10" punct="pt:12">f<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="pt">i</seg>ls</rhyme></w>.</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="12" met="6+6">« <w n="12.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M/mp">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w>-<w n="12.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3" mp="F">e</seg></w> <w n="12.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="P">ou</seg>r</w> <w n="12.4">l<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>gu<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="12.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t</w> <w n="12.6">d</w>’<w n="12.7"><seg phoneme="o" type="vs" value="1" rule="434" place="8" mp="M">o</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="12.9">v<seg phoneme="o" type="vs" value="1" rule="437" place="11" mp="C">o</seg>s</w> <w n="12.10" punct="vg:12">t<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="C">u</seg></w> <w n="13.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="402" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="13.7" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">em</seg>p<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="4" mp="M">I</seg><seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="14.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="14.7">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w> <w n="14.8">n<seg phoneme="o" type="vs" value="1" rule="437" place="10" mp="C">o</seg>s</w> <w n="14.9" punct="pi:12">d<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>f<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="pi">i</seg>s</rhyme></w> ? »</l>
					</lg>
				</div></body></text></TEI>