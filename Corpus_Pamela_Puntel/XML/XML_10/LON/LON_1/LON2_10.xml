<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="LON">
					<name>
						<forename>Eugène</forename>
						<nameLink>de</nameLink>
						<surname>LONLAY</surname>
					</name>
					<date from="1815" to="1886">1815-1886</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>52 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">LON_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>>POÉSIES, ÉDITION ELZÉVIRIENNE</title>
						<author>EUGÈNE DE LONLAY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k65208385/f11.image.r=EUG%C3%88NE%20DE%20LONLAY</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES, ÉDITION ELZÉVIRIENNE</title>
								<author>EUGÈNE DE LONLAY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALCAN-LEVY</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="LON2" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="3(abab)">
				<head type="main">PRIÈRE</head>
				<head type="main">AVANT LA BATAILLE</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:1">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="1" punct="vg">eu</seg></w>, <w n="1.2">s<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="1.3">pr<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="M">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="1.7">n<seg phoneme="o" type="vs" value="1" rule="437" place="9" mp="C">o</seg>s</w> <w n="1.8" punct="vg:10"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">É</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="2.3">gu<seg phoneme="i" type="vs" value="1" rule="490" place="4" caesura="1">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6">o</seg>r</w> <w n="2.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>s</w> <w n="2.7" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>rr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pt">an</seg>ts</rhyme></w>.</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="3.6" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">Pl<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="P">u</seg>r</w> <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>ds</w> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" mp="P">an</seg>s</w> <w n="4.7">n<seg phoneme="o" type="vs" value="1" rule="437" place="9" mp="C">o</seg>s</w> <w n="4.8" punct="pt:10">r<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pt">an</seg>gs</rhyme></w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="5" num="1.1" lm="10" met="4+6"><w n="5.1">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg></w> <w n="5.2">j<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="5.4" punct="vg:4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg" caesura="1">an</seg>d</w>,<caesura></caesura> <w n="5.5">r<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="P">a</seg>r</w> <w n="5.7">n<seg phoneme="o" type="vs" value="1" rule="437" place="9" mp="C">o</seg>s</w> <w n="5.8" punct="vg:10">p<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="6" num="1.2" lm="10" met="4+6"><w n="6.1" punct="vg:1">T<seg phoneme="wa" type="vs" value="1" rule="422" place="1" punct="vg">oi</seg></w>, <w n="6.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="6.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4" caesura="1">oi</seg>x</w><caesura></caesura> <w n="6.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="6.6">cr<seg phoneme="u" type="vs" value="1" rule="424" place="6" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="6.8" punct="vg:10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">em</seg>p<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="vg">a</seg>rts</rhyme></w>,</l>
						<l n="7" num="1.3" lm="10" met="4+6"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>ds</w> <w n="7.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="3" mp="C">o</seg>s</w> <w n="7.3" punct="vg:4">v<seg phoneme="ø" type="vs" value="1" rule="247" place="4" punct="vg" caesura="1">œu</seg>x</w>,<caesura></caesura> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>x<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="7.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="8" mp="C">o</seg>s</w> <w n="7.6" punct="pv:10">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
						<l n="8" num="1.4" lm="10" met="4+6"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.2">b<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="8.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="5" mp="C">o</seg>s</w> <w n="8.4">v<seg phoneme="a" type="vs" value="1" rule="306" place="6" mp="M">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>ts</w> <w n="8.5" punct="pt:10"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">en</seg>d<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pt">a</seg>rds</rhyme></w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="9" num="1.1" lm="10" met="4+6"><w n="9.1">C</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="9.4">t<seg phoneme="wa" type="vs" value="1" rule="422" place="3">oi</seg></w> <w n="9.5">s<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>l</w><caesura></caesura> <w n="9.6">qu</w>'<w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="9.8">h<seg phoneme="e" type="vs" value="1" rule="408" place="6" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="9.9">d<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>t</w> <w n="9.10">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="9.11" punct="vg:10">gl<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="1.2" lm="10" met="4+6"><w n="10.1">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg></w> <w n="10.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="10.3">l</w>'<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" mp="M">e</seg>ctr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="10.7" punct="pv:10">c<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="248" place="10" punct="pv">œu</seg>rs</rhyme></w> ;</l>
						<l n="11" num="1.3" lm="10" met="4+6"><w n="11.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="11.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="3" mp="C">o</seg>s</w> <w n="11.4">y<seg phoneme="ø" type="vs" value="1" rule="397" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="11.5">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="5" mp="M">a</seg>y<seg phoneme="o" type="vs" value="1" rule="443" place="6" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="11.7">v<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ct<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="12" num="1.4" lm="10" met="4+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="12.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="4" caesura="1">a</seg>ts</w><caesura></caesura> <w n="12.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M/mp">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fm">e</seg></w>-<w n="12.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w> <w n="12.6" punct="pt:10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="9" mp="M">ain</seg>qu<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="10" punct="pt">eu</seg>rs</rhyme></w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>