<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG33" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="2(ababccb)">
				<head type="main">JUSTICE :</head>
				<lg n="1" type="septain" rhyme="ababccb">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="1.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="377" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="1.5">J<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Où</seg></w> <w n="2.2">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="2.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>l</w> <w n="2.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="2.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>g<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>rds</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S</w>'<w n="3.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="3.4">tr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>b<seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>l</w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>g<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="447" place="8">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Où</seg></w> <w n="4.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="2">oin</seg></w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="4.4" punct="vg:5">f<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="5" punct="vg">eu</seg>rs</w>, <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="4.6">h<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rds</rhyme></w></l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="3">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="5.4" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg>d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>c<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">s<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="a" type="vs" value="1" rule="193" place="4">e</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339" place="7">A</seg>ss<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="7.2">j<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="7.5" punct="pe:8">C<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>rs</rhyme></w> !</l>
				</lg>
				<lg n="2" type="septain" rhyme="ababccb">
					<l n="8" num="2.1" lm="8" met="8"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="8.2">l</w>'<w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7">m<seg phoneme="i" type="vs" value="1" rule="492" place="7">y</seg>st<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="9" num="2.2" lm="8" met="8"><w n="9.1">J</w>'<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="9.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="9.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="9.7">f<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>s</rhyme></w></l>
					<l n="10" num="2.3" lm="8" met="8"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="10.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="10.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="10.6">t<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="11" num="2.4" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="11.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="11.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>l</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>fr<seg phoneme="ɛ" type="vs" value="1" rule="383" place="5">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="11.6" punct="vg:8">l<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="vg">oi</seg>s</rhyme></w>,</l>
					<l n="12" num="2.5" lm="8" met="8"><w n="12.1" punct="vg:2">M<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>ltk<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="12.2" punct="vg:4">B<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>rk</w>, <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.4">R<seg phoneme="wa" type="vs" value="1" rule="422" place="6">oi</seg></w> <w n="12.5" punct="pe:8">B<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rb<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="13" num="2.6" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="13.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>s</w> <w n="13.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="345" place="4" punct="vg">e</seg>l</w>, <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="13.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.6" punct="vg:8">b<rhyme label="c" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="2.7" lm="8" met="8"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="14.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">A</seg></w> <w n="14.5">M<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">O</seg>RT</w> <w n="14.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="14.7" punct="pe:8">tr<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="pe ps">oi</seg>s</rhyme></w> !…</l>
				</lg>
			</div></body></text></TEI>