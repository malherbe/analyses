<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DU SIÈGE DE PARIS</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="SFL">
					<name>
						<forename>Théobald</forename>
						<surname>SAINT-FÉLIX</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>161 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">SFL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>CHANTS DU SIÈGE DE PARIS. 1870-1871</title>
						<author>THÉOBALD SAINT-FÉLIX</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Imprimerie Ch. Schiller</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="SFL4" modus="cp" lm_max="12" metProfile="8, 6, 6+6, (9)" form="suite périodique avec alternance de type 1" schema="3{1(ababcdcd) 1(aabb)}">
				<head type="main">CHASSONS LES ALLEMANDS</head>
				<head type="form">CHANT PATRIOTIQUE</head>
				<opener>
					<salute>A LA GARDE NATIONALE</salute>
				</opener>
				<div type="section" n="1">
					<head type="number">1</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="1.2">r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>gu<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg></w> <w n="1.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6">l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="2.3" punct="vg:5">t<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="2.4" punct="ps:8">P<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>si<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="ps">en</seg>s</rhyme></w>…</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="3.3" punct="vg:5">t<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="3.4">l</w>’<w n="3.5"><seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>b<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="3.6" punct="vg:8">p<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>l<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.3">f<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="4.5" punct="pe:8">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>ssi<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="pe">en</seg>s</rhyme></w> !</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="5">en</seg>t</w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="5.4">c<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>mm<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="6.5" punct="pv:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>p<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>rts</rhyme></w> ;</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">J<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="7.3" punct="vg:4">vi<seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="vg">eu</seg>x</w>, <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="7.6">s</w>’<w n="7.7" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>l<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1" punct="vg:2">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="8.2">v<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="8.5" punct="pe:8">p<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>rts</rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="aabb">
						<l n="9" num="2.1" lm="12" met="6+6"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="9.3">l</w>’<w n="9.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="C">u</seg></w> <w n="9.6" punct="vg:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="6" punct="vg" caesura="1">e</seg>il</w>,<caesura></caesura> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.8">j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r</w> <w n="9.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.10">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="9.11">b<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>t<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="2.2" lm="12" met="6+6"><w n="10.1">D<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="439" place="2" mp="M">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="10.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="4" mp="C">o</seg>s</w> <w n="10.3">dr<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="10.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="188" place="7" punct="vg">e</seg>t</w>, <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="10.7">m<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>tr<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="2.3" lm="6" met="6"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="11.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>t</w>, <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ls</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="11.5" punct="vg:6">Fr<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>cs</rhyme></w>,</l>
						<l n="12" num="2.4" lm="6" met="6"><w n="12.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="12.3" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pe">an</seg>ds</rhyme></w> !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">2</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="13" num="1.1" lm="8" met="8"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="13.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="3">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.4" punct="vg:8">f<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="1.2" lm="8" met="8"><w n="14.1" punct="vg:2">B<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>rck</w>, <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4">œu</seg>r</w> <w n="14.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.5">d</w>’<w n="14.6" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rg<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="343" place="8" punct="vg">ue</seg>il</rhyme></w>,</l>
						<l n="15" num="1.3" lm="8" met="8"><w n="15.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="15.2">b<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg></w> <w n="15.3">s<seg phoneme="ɔ" type="vs" value="1" rule="442" place="3">o</seg>l</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>tr<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="16" num="1.4" lm="8" met="8"><w n="16.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="16.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="16.4">v<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.5" punct="ps:8">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rc<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="344" place="8" punct="ps">ue</seg>il</rhyme></w>…</l>
						<l n="17" num="1.5" lm="8" met="8"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="17.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">em</seg>br<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="17.5" punct="pv:8">Fr<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="18" num="1.6" lm="8" met="8"><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rs</w> <w n="18.3">s</w>’<w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="18.5" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>lt<rhyme label="d" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="19" num="1.7" lm="8" met="8"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="19.2">P<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s</w> <w n="19.3">v<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>t</w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="19.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>vr<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="20" num="1.8" lm="8" met="8"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="20.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="20.5" punct="pe:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rt<rhyme label="d" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pe ps">é</seg></rhyme></w> !…</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="aabb">
						<l n="21" num="2.1" lm="12" met="6+6"><w n="21.1">C</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="21.3">l</w>’<w n="21.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="21.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="C">u</seg></w> <w n="21.6" punct="vg:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="6" punct="vg" caesura="1">e</seg>il</w>,<caesura></caesura> <w n="21.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="21.8">j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r</w> <w n="21.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="21.10">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="21.11">b<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>t<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="22" num="2.2" lm="12" met="6+6"><w n="22.1">D<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="439" place="2" mp="M">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="22.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="4" mp="C">o</seg>s</w> <w n="22.3">dr<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="22.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="188" place="7" punct="vg">e</seg>t</w>, <w n="22.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg></w> <w n="22.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="22.7">m<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>tr<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="23" num="2.3" lm="6" met="6"><w n="23.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="23.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>t</w>, <w n="23.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ls</w> <w n="23.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="23.5" punct="vg:6">Fr<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>cs</rhyme></w>,</l>
						<l n="24" num="2.4" lm="6" met="6"><w n="24.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="24.3" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pe">an</seg>ds</rhyme></w> !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">3</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="25" num="1.1" lm="9"><w n="25.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>s</w> <w n="25.2"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="25.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="25.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="C">u</seg></w> <w n="25.5" punct="vg:9">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7" mp="M">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>t<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="26" num="1.2" lm="8" met="8"><w n="26.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>s</w> <w n="26.2"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">pi<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="26.6" punct="pe:8">f<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" punct="pe ps">o</seg>rts</rhyme></w> !…</l>
						<l n="27" num="1.3" lm="8" met="8"><w n="27.1">N</w>’<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w>-<w n="27.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="27.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="3">oin</seg>t</w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="27.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.7">cr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="27.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="27.9">gl<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="28" num="1.4" lm="8" met="8"><w n="28.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="28.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r</w> <w n="28.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="28.6" punct="pi:8">m<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" punct="pi ps">o</seg>rts</rhyme></w> ?…</l>
						<l n="29" num="1.5" lm="8" met="8"><w n="29.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="29.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="29.3">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="29.4">h<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="30" num="1.6" lm="8" met="8"><w n="30.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">p<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="30.6" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>b<rhyme label="d" id="16" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>t</rhyme></w>,</l>
						<l n="31" num="1.7" lm="8" met="8"><w n="31.1">F<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="31.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="31.3" punct="vg:4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" punct="vg">ain</seg></w>, <w n="31.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg></w> <w n="31.5">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6">œu</seg>r</w> <w n="31.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="31.7" punct="vg:8">r<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="32" num="1.8" lm="8" met="8"><w n="32.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="32.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="32.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="32.4">cr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="32.5" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rt<rhyme label="d" id="16" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="aabb">
						<l n="33" num="2.1" lm="12" met="6+6"><w n="33.1">C</w>’<w n="33.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="33.3">l</w>’<w n="33.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="33.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="C">u</seg></w> <w n="33.6" punct="vg:6">r<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381" place="6" punct="vg" caesura="1">e</seg>il</w>,<caesura></caesura> <w n="33.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="33.8">j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>r</w> <w n="33.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="33.10">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="33.11">b<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>t<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="34" num="2.2" lm="12" met="6+6"><w n="34.1">D<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="439" place="2" mp="M">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="34.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="4" mp="C">o</seg>s</w> <w n="34.3">dr<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="34.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="188" place="7" punct="vg">e</seg>t</w>, <w n="34.5">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg></w> <w n="34.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="34.7">m<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>tr<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="35" num="2.3" lm="6" met="6"><w n="35.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="35.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>t</w>, <w n="35.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ls</w> <w n="35.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="35.5" punct="vg:6">Fr<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>cs</rhyme></w>,</l>
						<l n="36" num="2.4" lm="6" met="6"><w n="36.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="36.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="36.3" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pe">an</seg>ds</rhyme></w> !</l>
					</lg>
				</div>
				<closer>
					<note type="footnote" id="">N.B. — La musique, chant et piano, composé par M. Jules de Langlade, est en vente chez M. Sylvain Saint-Étienne, éditeur, n° 31 bis, Faubourg-Montmartre, à Paris.</note>
				</closer>
			</div></body></text></TEI>