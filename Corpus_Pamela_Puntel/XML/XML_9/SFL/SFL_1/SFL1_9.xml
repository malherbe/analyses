<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DU SIÈGE DE PARIS</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="SFL">
					<name>
						<forename>Théobald</forename>
						<surname>SAINT-FÉLIX</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>161 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">SFL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>CHANTS DU SIÈGE DE PARIS. 1870-1871</title>
						<author>THÉOBALD SAINT-FÉLIX</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Imprimerie Ch. Schiller</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="SFL1" modus="cp" lm_max="10" metProfile="8, 4+6, (7)" form="suite périodique avec alternance de type 1" schema="3{1(abbacca) 1(abab)}" er_moy="0.22" er_max="2" er_min="0" er_mode="0(16/18)" er_moy_et="0.63" qr_moy="0.0" qr_max="C0" qr_mode="0(18/18)" qr_moy_et="0.0">
				<head type="main">LE BRANLE-BAS</head>
				<head type="sub_1">CHANT NAVAL</head>
				<opener>
					<salute>AUX MARINS DE LA FRANCE</salute>
				</opener>
				<div type="section" n="1">
					<head type="number">1</head>
					<lg n="1" type="septain" rhyme="abbacca">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:3">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="o" type="vs" value="1" rule="437" place="3" punct="vg">o</seg>ts</w>, <w n="1.2">n</w>’<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="6">ez</seg></w> <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="1.5">p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>s</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="2.2">bru<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>t</w> <w n="2.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>f<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="2.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">l</w>’<w n="2.7" punct="pi:8"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="3.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>x</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="3.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="3.7">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="3.8" punct="pv:8">gr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="4.3">b<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="4.6" punct="ps:8">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="ps">a</seg>s</rhyme></pgtc></w>…</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="5.3" punct="vg:5">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" punct="vg">on</seg>s</w>, <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="5.5" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1" punct="vg:2">C<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="6.2">v<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="6.4">l</w>’<w n="6.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rd<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>s</w> <w n="7.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>d</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6" punct="pt:8">br<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="8" num="2.1" lm="8" met="8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>x</w> <w n="8.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="8.5">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="8.6" punct="pe:8">t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></rhyme></pgtc></w> !…</l>
						<l n="9" num="2.2" lm="8" met="8"><w n="9.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg>ts</w>, <w n="9.2">c</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="9.4">l</w>’<w n="9.5">h<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="9.7" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>ts</rhyme></pgtc></w>.</l>
						<l n="10" num="2.3" lm="10" met="4+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M/mp">É</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="M/mp">an</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="10.2" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="10.3">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="10.4">c</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="10.6">l</w>’<w n="10.7">h<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.8">qu<seg phoneme="i" type="vs" value="1" rule="490" place="9">i</seg></w> <w n="10.9">d<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="11" num="2.4" lm="10" met="4+6"><w n="11.1">J<seg phoneme="wa" type="vs" value="1" rule="422" place="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="188" place="2">e</seg>t</w> <w n="11.3">b<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg>x</w> <w n="11.5" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" punct="vg">in</seg>s</w>, <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="C">au</seg>x</w> <w n="11.7" punct="pe:10">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>ld<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pe">a</seg>ts</rhyme></pgtc></w> !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">2</head>
					<lg n="1" type="septain" rhyme="abbacca">
						<l n="12" num="1.1" lm="8" met="8"><w n="12.1">N</w>’<w n="12.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="12.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.5" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rr<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>x</rhyme></pgtc></w>,</l>
						<l n="13" num="1.2" lm="8" met="8"><w n="13.1" punct="vg:3">S<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg>s</w>, <w n="13.2">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="13.4" punct="ps:8"><pgtc id="7" weight="0" schema="[R"><rhyme label="b" id="7" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</rhyme></pgtc></w>…</l>
						<l n="14" num="1.3" lm="8" met="8"><w n="14.1">N<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="14.2" punct="vg:2">cr<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>s</w>, <w n="14.3">n<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="14.4" punct="vg:6">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg">e</seg>s</w>, <w n="14.5">n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="14.6" punct="vg:8">l<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="15" num="1.4" lm="8" met="8"><w n="15.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="1">en</seg></w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">d<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>t</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="15.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="15.6" punct="pt:8">c<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>ps</rhyme></pgtc></w>.</l>
						<l n="16" num="1.5" lm="8" met="8"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>ls</w> <w n="16.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="16.4">r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="16.7" punct="vg:8">pl<pgtc id="8" weight="0" schema="R"><rhyme label="c" id="8" gender="f" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="8">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="17" num="1.6" lm="8" met="8"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="17.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="17.4">l<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="17.6" punct="vg:8">s<pgtc id="8" weight="0" schema="R"><rhyme label="c" id="8" gender="f" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="8">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="1.7" lm="8" met="8"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="18.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="7">e</seg>c</w> <w n="18.5" punct="pt:8">n<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="19" num="2.1" lm="8" met="8"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>x</w> <w n="19.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="19.5">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="19.6" punct="pe:8">t<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></rhyme></pgtc></w> !…</l>
						<l n="20" num="2.2" lm="8" met="8"><w n="20.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg>ts</w>, <w n="20.2">c</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="20.4">l</w>’<w n="20.5">h<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="20.7" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>b<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>ts</rhyme></pgtc></w>.</l>
						<l n="21" num="2.3" lm="10" met="4+6"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M/mp">É</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="M/mp">an</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="21.2" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="21.3">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="21.4">c</w>’<w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="21.6">l</w>’<w n="21.7">h<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="21.8">qu<seg phoneme="i" type="vs" value="1" rule="490" place="9">i</seg></w> <w n="21.9">d<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="22" num="2.4" lm="10" met="4+6"><w n="22.1">J<seg phoneme="wa" type="vs" value="1" rule="422" place="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="188" place="2">e</seg>t</w> <w n="22.3">b<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="22.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg>x</w> <w n="22.5" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" punct="vg">in</seg>s</w>, <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="C">au</seg>x</w> <w n="22.7" punct="pe:10">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>ld<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pe">a</seg>ts</rhyme></pgtc></w> !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">3</head>
					<lg n="1" type="septain" rhyme="abbacca">
						<l n="23" num="1.1" lm="8" met="8"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="23.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="2">om</seg></w> <w n="23.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="23.4" punct="vg:5">Pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5" punct="vg">è</seg>s</w>, <w n="23.5">dr<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>t</w> <w n="23.6" punct="vg:8">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg><pgtc id="11" weight="2" schema="CR">v<rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
						<l n="24" num="1.2" lm="8" met="8"><w n="24.1">S<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>l</w> <w n="24.2">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="24.5">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg><pgtc id="12" weight="2" schema="CR">ti<rhyme label="b" id="12" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="25" num="1.3" lm="8" met="8"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1">É</seg>cr<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="25.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.3">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<pgtc id="12" weight="2" schema="CR">ti<rhyme label="b" id="12" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="26" num="1.4" lm="7"><w n="26.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343" place="3">ue</seg>il</w> <w n="26.4">tr<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.6" punct="pe:7">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>r<pgtc id="11" weight="2" schema="CR">v<rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="314" place="7" punct="pe">eau</seg></rhyme></pgtc></w> !</l>
						<l n="27" num="1.5" lm="8" met="8"><w n="27.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="27.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>t</w>, <w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>ts</w> <w n="27.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="27.6" punct="pe:8">Fr<pgtc id="13" weight="0" schema="R"><rhyme label="c" id="13" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="28" num="1.6" lm="8" met="8"><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="28.2">j<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="28.4">l</w>’<w n="28.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d<pgtc id="13" weight="0" schema="R"><rhyme label="c" id="13" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="29" num="1.7" lm="8" met="8"><w n="29.1">S</w>’<w n="29.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>br<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="29.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="29.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="29.5" punct="pt:8">dr<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>p<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="pt">eau</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="30" num="2.1" lm="8" met="8"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>x</w> <w n="30.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="30.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="30.5">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s</w> <w n="30.6" punct="pe:8">t<pgtc id="14" weight="0" schema="R"><rhyme label="a" id="14" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></rhyme></pgtc></w> !…</l>
						<l n="31" num="2.2" lm="8" met="8"><w n="31.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg>ts</w>, <w n="31.2">c</w>’<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="31.4">l</w>’<w n="31.5">h<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="31.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="31.7" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>b<pgtc id="15" weight="0" schema="R"><rhyme label="b" id="15" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>ts</rhyme></pgtc></w>.</l>
						<l n="32" num="2.3" lm="10" met="4+6"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M/mp">É</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="M/mp">an</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="Lp">on</seg>s</w>-<w n="32.2" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="32.3">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>r</w> <w n="32.4">c</w>’<w n="32.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="32.6">l</w>’<w n="32.7">h<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="32.8">qu<seg phoneme="i" type="vs" value="1" rule="490" place="9">i</seg></w> <w n="32.9">d<pgtc id="14" weight="0" schema="R"><rhyme label="a" id="14" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="33" num="2.4" lm="10" met="4+6"><w n="33.1">J<seg phoneme="wa" type="vs" value="1" rule="422" place="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="33.2"><seg phoneme="e" type="vs" value="1" rule="188" place="2">e</seg>t</w> <w n="33.3">b<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="33.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg>x</w> <w n="33.5" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="M">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" punct="vg">in</seg>s</w>, <w n="33.6"><seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="C">au</seg>x</w> <w n="33.7" punct="pe:10">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>ld<pgtc id="15" weight="0" schema="R"><rhyme label="b" id="15" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="pe">a</seg>ts</rhyme></pgtc></w> !</l>
					</lg>
				</div>
				<closer>
					<note type="footnote" id="">N.B. — La musique, chant et piano, composé par M. Henri Cohen, est en vente chez M. Sylvain Saint-Étienne, n° 31 bis, Faubourg-Montmartre, à Paris.</note>
				</closer>
			</div></body></text></TEI>