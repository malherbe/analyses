<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ89" modus="sp" lm_max="8" metProfile="8, (6)" form="suite périodique" schema="3(aabccb)" er_moy="0.78" er_max="3" er_min="0" er_mode="0(6/9)" er_moy_et="1.13" qr_moy="0.0" qr_max="C0" qr_mode="0(9/9)" qr_moy_et="0.0">
					<head type="number">LXXXIX</head>
					<head type="main">LES ENFANTS</head>
					<lg n="1" type="sizain" rhyme="aabccb">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="1.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg>ts</w>, <w n="1.3">d</w>'<w n="1.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5">où</seg></w> <w n="1.5">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w>-<w n="1.6" punct="pi:8">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pi">ou</seg>s</rhyme></pgtc></w> ?</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>t</w> <w n="2.2" punct="vg:3">J<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>s<seg phoneme="y" type="vs" value="1" rule="449" place="3" punct="vg">u</seg>s</w>, <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>l</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="2.5" punct="pv:8">d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pv">ou</seg>x</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">En</seg>tr<seg phoneme="e" type="vs" value="1" rule="346" place="2">ez</seg></w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="3.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="3.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg></w> <w n="3.5" punct="pe:8">r<seg phoneme="wa" type="vs" value="1" rule="439" place="7">o</seg><pgtc id="2" weight="3" schema="GR">y<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</w> ! <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="4.5" punct="pe:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>gl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg>ts</rhyme></pgtc></w> !</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="5.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>c</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="5.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="5.5">v<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="5.6" punct="pi:8">fl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pi ps">an</seg>cs</rhyme></pgtc></w> ?…</l>
						<l n="6" num="1.6" lm="6"><space unit="char" quantity="4"></space>— <w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3" punct="pe:3">B<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe">a</seg>rk</w> ! <w n="6.4">c</w>'<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="6.6" punct="pe:6">Gu<seg phoneme="i" type="vs" value="1" rule="484" place="5">i</seg><pgtc id="2" weight="3" schema="GR">ll<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="sizain" rhyme="aabccb">
						<l n="7" num="2.1" lm="8" met="8"><w n="7.1">P<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="7.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg>ts</w>, <w n="7.3">v<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rps</w> <w n="7.5" punct="vg:8">fl<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>tr<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
						<l n="8" num="2.2" lm="8" met="8"><w n="8.1">V<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>s</w> <w n="8.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ts</w> <w n="8.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>gr<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>s</rhyme></pgtc></w></l>
						<l n="9" num="2.3" lm="8" met="8"><w n="9.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="9.2" punct="vg:3">t<seg phoneme="i" type="vs" value="1" rule="492" place="2">y</seg>ph<seg phoneme="y" type="vs" value="1" rule="449" place="3" punct="vg">u</seg>s</w>, <w n="9.3">l<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.4" punct="vg:8">f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="414" place="8">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="2.4" lm="8" met="8"><w n="10.1">P<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="434" place="7">o</seg><pgtc id="6" weight="2" schema="CR">nn<rhyme label="c" id="6" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="dp">é</seg></rhyme></pgtc></w> :</l>
						<l n="11" num="2.5" lm="8" met="8"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="11.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>c</w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="11.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="11.5">l</w>'<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="11.7" punct="pi:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="7">aî</seg><pgtc id="6" weight="2" schema="CR">n<rhyme label="c" id="6" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pi ps">é</seg></rhyme></pgtc></w> ?…</l>
						<l n="12" num="2.6" lm="6"><space unit="char" quantity="4"></space>— <w n="12.1">C</w>'<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="12.3" punct="pe:3">B<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe">a</seg>rk</w> ! <w n="12.4">c</w>'<w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="12.6" punct="pe:6">Gu<seg phoneme="i" type="vs" value="1" rule="484" place="5">i</seg>ll<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="sizain" rhyme="aabccb">
						<l n="13" num="3.1" lm="8" met="8"><w n="13.1">P<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="13.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg>ts</w>, <w n="13.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="13.4">v<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">â</seg><pgtc id="7" weight="2" schema="CR">l<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>rs</rhyme></pgtc></w></l>
						<l n="14" num="3.2" lm="8" met="8"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r</w> <w n="14.4">d</w>'<w n="14.5"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="14.6" punct="pv:8">d<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg><pgtc id="7" weight="2" schema="CR">l<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pv">eu</seg>rs</rhyme></pgtc></w> ;</l>
						<l n="15" num="3.3" lm="8" met="8"><w n="15.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345" place="1">e</seg>l</w> <w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="15.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg></w> <w n="15.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7" punct="pi:8">ch<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
						<l n="16" num="3.4" lm="8" met="8"><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="16.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="16.4" punct="pi:4">s<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pi">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ? <w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w>-<w n="16.6">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="6">e</seg></w> <w n="16.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="16.8" punct="pi:8">p<pgtc id="9" weight="0" schema="R"><rhyme label="c" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pi">ain</seg></rhyme></pgtc></w> ?</l>
						<l n="17" num="3.5" lm="8" met="8"><w n="17.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="17.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>c</w> <w n="17.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="17.4">l<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t</w> <w n="17.5">m<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r</w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.7" punct="pi:8">f<pgtc id="9" weight="0" schema="R"><rhyme label="c" id="9" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pi">aim</seg></rhyme></pgtc></w> ?</l>
						<l n="18" num="3.6" lm="6"><space unit="char" quantity="4"></space>— <w n="18.1">C</w>'<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="18.3" punct="pe:3">B<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe">a</seg>rk</w> ! <w n="18.4">c</w>'<w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="18.6" punct="pe:6">Gu<seg phoneme="i" type="vs" value="1" rule="484" place="5">i</seg>ll<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">Janvier 1871.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>