<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ12" modus="sp" lm_max="8" metProfile="8, 6, (4)" form="suite périodique avec alternance de type 1" schema="3{1(abbacc) 1(abab)}" er_moy="0.73" er_max="2" er_min="0" er_mode="0(9/15)" er_moy_et="0.93" qr_moy="0.0" qr_max="C0" qr_mode="0(15/15)" qr_moy_et="0.0">
					<head type="number">XII</head>
					<head type="main">LA CHASSE AUX UHLANS</head>
					<lg n="1" type="sizain" rhyme="abbacc">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>gl<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="1.3">fu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>t</w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="1.5" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">A</seg>rd<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="365" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2"><seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>hl<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="2.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="2.6" punct="pe:8"><pgtc id="2" weight="2" schema="[CR">b<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="pe">oi</seg>s</rhyme></pgtc></w> !</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>ls</w> <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="3.3">m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="3.5">b<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="3.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="2" weight="2" schema="CR">b<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="pt">oi</seg>s</rhyme></pgtc></w>.</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="4.2"><seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>hl<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="4.3">n</w>'<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="4.7" punct="pt:8">pl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">H<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg></w> <w n="5.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="5.3">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5" punct="pe:8">g<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>bi<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pe">er</seg></rhyme></pgtc></w> !</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3">en</seg></w> <w n="6.3">v<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5" punct="pe:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>gl<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pe ps ti">er</seg></rhyme></pgtc></w> !… —</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="7" num="2.1" lm="8" met="8"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="7.2" punct="pe:3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe">e</seg></w> ! <w n="7.3">f<seg phoneme="œ" type="vs" value="1" rule="303" place="4">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="7.5" punct="pe:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="4" weight="2" schema="CR">tt<rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="8" num="2.2" lm="8" met="8"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="œ" type="vs" value="1" rule="285" place="1">œ</seg>il</w> <w n="8.3" punct="vg:2">s<seg phoneme="y" type="vs" value="1" rule="444" place="2" punct="vg">û</seg>r</w>, <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.5">br<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="8.6" punct="vg:5">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" punct="vg">om</seg>pt</w>, <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.8">c<seg phoneme="œ" type="vs" value="1" rule="248" place="7">œu</seg>r</w> <w n="8.9" punct="pe:8">ch<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="8" punct="pe">au</seg>d</rhyme></pgtc></w> !</l>
						<l n="9" num="2.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="9.2" punct="pe:3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe">e</seg></w> ! <w n="9.3" punct="pe:5">p<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pe">e</seg></w> ! <w n="9.4" punct="pe:6"><pgtc id="4" weight="2" schema="[CR">t<rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="456" place="6">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="10" num="2.4" lm="4"><space unit="char" quantity="8"></space><w n="10.1" punct="pe:2">T<seg phoneme="ɛ" type="vs" value="1" rule="338" place="1">a</seg>y<seg phoneme="o" type="vs" value="1" rule="317" place="2" punct="pe">au</seg>t</w> ! <w n="10.2" punct="pe:4">t<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3">a</seg>y<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="4" punct="pe">au</seg>t</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="sizain" rhyme="abbacc">
						<l n="11" num="3.1" lm="8" met="8"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>ls</w> <w n="11.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="11.5">l<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</w> <w n="11.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>n<pgtc id="6" weight="1" schema="GR">i<rhyme label="a" id="6" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="12" num="3.2" lm="8" met="8"><w n="12.1">L<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>rs</w> <w n="12.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="12.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</w> <w n="12.5" punct="pv:8">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="7" weight="2" schema="CR">t<rhyme label="b" id="7" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pv">i</seg>ts</rhyme></pgtc></w> ;</l>
						<l n="13" num="3.3" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="13.2">l<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>rs</w> <w n="13.3">s<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><pgtc id="7" weight="2" schema="CR">t<rhyme label="b" id="7" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ts</rhyme></pgtc></w></l>
						<l n="14" num="3.4" lm="8" met="8"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="14.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="14.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="14.4">pr<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="14.5" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>t<pgtc id="6" weight="1" schema="GR">i<rhyme label="a" id="6" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="15" num="3.5" lm="8" met="8"><w n="15.1" punct="vg:2">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="406" place="2" punct="vg">eu</seg>r</w>, <w n="15.2">pr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="15.4" punct="pv:8">c<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rni<pgtc id="8" weight="0" schema="R"><rhyme label="c" id="8" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
						<l n="16" num="3.6" lm="8" met="8"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3">en</seg></w> <w n="16.3">v<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5" punct="pe:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>gl<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="8" weight="0" schema="R"><rhyme label="c" id="8" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pe ps ti">er</seg></rhyme></pgtc></w> !… —</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="17" num="4.1" lm="8" met="8"><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="17.2" punct="pe:3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe">e</seg></w> ! <w n="17.3">f<seg phoneme="œ" type="vs" value="1" rule="303" place="4">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="17.5" punct="pe:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="9" weight="2" schema="CR">tt<rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="18" num="4.2" lm="8" met="8"><w n="18.1">L</w>’<w n="18.2"><seg phoneme="œ" type="vs" value="1" rule="285" place="1">œ</seg>il</w> <w n="18.3" punct="vg:2">s<seg phoneme="y" type="vs" value="1" rule="444" place="2" punct="vg">û</seg>r</w>, <w n="18.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.5">br<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="18.6" punct="vg:5">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" punct="vg">om</seg>pt</w>, <w n="18.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.8">c<seg phoneme="œ" type="vs" value="1" rule="248" place="7">œu</seg>r</w> <w n="18.9" punct="pe:8">ch<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="8" punct="pe">au</seg>d</rhyme></pgtc></w> !</l>
						<l n="19" num="4.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="19.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="19.2" punct="pe:3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe">e</seg></w> ! <w n="19.3" punct="pe:5">p<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pe">e</seg></w> ! <w n="19.4" punct="pe:6"><pgtc id="9" weight="2" schema="[CR">t<rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="456" place="6">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe ti">e</seg></rhyme></pgtc></w> !—</l>
						<l n="20" num="4.4" lm="4"><space unit="char" quantity="8"></space><w n="20.1" punct="pe:2">T<seg phoneme="ɛ" type="vs" value="1" rule="338" place="1">a</seg>y<seg phoneme="o" type="vs" value="1" rule="317" place="2" punct="pe">au</seg>t</w> ! <w n="20.2" punct="pe:4">t<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3">a</seg>y<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="4" punct="pe">au</seg>t</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="sizain" rhyme="abbacc">
						<l n="21" num="5.1" lm="8" met="8"><w n="21.1">Tr<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>qu<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w>-<w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="21.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="21.4">m<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="21.5">n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="21.6" punct="vg:8">tr<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="5.2" lm="8" met="8"><w n="22.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="169" place="2">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s</w> <w n="22.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="22.4">f<seg phoneme="wa" type="vs" value="1" rule="422" place="6">oi</seg></w> <w n="22.5">n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="22.6" punct="pe:8">l<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
						<l n="23" num="5.3" lm="8" met="8"><w n="23.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">n</w>'<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="23.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="23.6">ch<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.7">d</w>'<w n="23.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="23.9" punct="vg:8">r<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="24" num="5.4" lm="8" met="8"><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="24.2">d</w>'<w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="24.4">p<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="24.6">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="24.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.8" punct="pe:8">l<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="25" num="5.5" lm="8" met="8"><w n="25.1">Qu</w>'<w n="25.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ls</w> <w n="25.3">m<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="25.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="25.5">j<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>squ</w>'<w n="25.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="25.7" punct="pv:8">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rni<pgtc id="13" weight="0" schema="R"><rhyme label="c" id="13" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
						<l n="26" num="5.6" lm="8" met="8"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3">en</seg></w> <w n="26.3">v<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w> <w n="26.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.5" punct="pe:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>gl<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="13" weight="0" schema="R"><rhyme label="c" id="13" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pe ps">er</seg></rhyme></pgtc></w> !…</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="27" num="6.1" lm="8" met="8"><w n="27.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="27.2" punct="pe:3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe">e</seg></w> ! <w n="27.3">f<seg phoneme="œ" type="vs" value="1" rule="303" place="4">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="27.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="27.5" punct="pe:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="14" weight="2" schema="CR">tt<rhyme label="a" id="14" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="28" num="6.2" lm="8" met="8"><w n="28.1">L</w>’<w n="28.2"><seg phoneme="œ" type="vs" value="1" rule="285" place="1">œ</seg>il</w> <w n="28.3" punct="vg:2">s<seg phoneme="y" type="vs" value="1" rule="444" place="2" punct="vg">û</seg>r</w>, <w n="28.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="28.5">br<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="28.6" punct="vg:5">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" punct="vg">om</seg>pt</w>, <w n="28.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.8">c<seg phoneme="œ" type="vs" value="1" rule="248" place="7">œu</seg>r</w> <w n="28.9" punct="pe:8">ch<pgtc id="15" weight="0" schema="R"><rhyme label="b" id="15" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="8" punct="pe">au</seg>d</rhyme></pgtc></w> !</l>
						<l n="29" num="6.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="29.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="29.2" punct="pe:3">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe">e</seg></w> ! <w n="29.3" punct="pe:5">p<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pe">e</seg></w> ! <w n="29.4" punct="pe:6"><pgtc id="14" weight="2" schema="[CR">t<rhyme label="a" id="14" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="456" place="6">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="30" num="6.4" lm="4"><space unit="char" quantity="8"></space><w n="30.1" punct="pe:2">T<seg phoneme="ɛ" type="vs" value="1" rule="338" place="1">a</seg>y<seg phoneme="o" type="vs" value="1" rule="317" place="2" punct="pe">au</seg>t</w> ! <w n="30.2" punct="pe:4">t<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3">a</seg>y<pgtc id="15" weight="0" schema="R"><rhyme label="b" id="15" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="317" place="4" punct="pe">au</seg>t</rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Août 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>