<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG22" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="6[abab]" er_moy="1.67" er_max="8" er_min="0" er_mode="2(6/12)" er_moy_et="2.13" qr_moy="0.0" qr_max="C0" qr_mode="0(12/12)" qr_moy_et="0.0">
				<lg n="1" type="regexp" rhyme="abababababababababababab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="466" place="3" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="466" place="4" mp="M">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="1.3" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="1.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="1.5">f<seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>t</w> <w n="1.7" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="466" place="11" mp="M">i</seg><pgtc id="1" weight="2" schema="CR">mm<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="3">â</seg>t<seg phoneme="i" type="vs" value="1" rule="466" place="4">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="2.3" punct="pe:8">m<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg><pgtc id="2" weight="8" schema="CVCR">r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pe">é</seg></rhyme></pgtc></w> !</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="vg">E</seg>t</w>, <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="3.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>v<seg phoneme="o" type="vs" value="1" rule="437" place="6" punct="vg" caesura="1">o</seg>ts</w>,<caesura></caesura> <w n="3.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="3.6" punct="vg:9">v<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg" mp="F">e</seg>s</w>, <w n="3.7">n<seg phoneme="o" type="vs" value="1" rule="437" place="10" mp="C">o</seg>s</w> <w n="3.8">d<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg>t</w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>l</w> <w n="4.5" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg><pgtc id="2" weight="8" schema="CVCR">rr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt in ps">é</seg></rhyme></pgtc></w>.'…</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1" punct="pe:1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="pe ps">on</seg></w> !… <w n="5.2" punct="vg:2">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2" punct="vg">ai</seg>s</w>, <w n="5.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ls</w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="5.5">h<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437" place="6" caesura="1">o</seg>s</w><caesura></caesura> <w n="5.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t</w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="5.8">s<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="5.9">t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w> <w n="6.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="6.5">l<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>s</rhyme></pgtc></w></l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="7.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="7.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg></w> <w n="7.5" punct="vg:6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="7.7">n<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rgu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>t</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="7.9" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">em</seg>p<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="8.3">fl<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="8.5" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rqu<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="419" place="8" punct="vg">oi</seg>s</rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="M">e</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="9.2" punct="pe:4">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="pe" mp="F">e</seg>s</w> ! <w n="9.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="9.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="7">en</seg></w> <w n="9.5">v<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.6">n<seg phoneme="o" type="vs" value="1" rule="437" place="10" mp="C">o</seg>s</w> <w n="9.7" punct="vg:12">bl<seg phoneme="e" type="vs" value="1" rule="352" place="11" mp="M">e</seg><pgtc id="5" weight="2" schema="CR">ss<rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="y" type="vs" value="1" rule="449" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="10" num="1.10" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">Fi<seg phoneme="ɛ" type="vs" value="1" rule="63" place="1">e</seg>rs</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>thl<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6">en</seg></w> <w n="10.5">n</w>'<w n="10.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="6" weight="2" schema="CR">b<rhyme label="b" id="6" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>t</rhyme></pgtc></w> ;</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1" punct="vg:3">Pr<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" punct="vg">on</seg>s</w>, <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="P">à</seg></w> <w n="11.3">tr<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="6" caesura="1">e</seg>rs</w><caesura></caesura> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="11.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="11.6" punct="vg:12">fl<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="5" weight="2" schema="CR">ss<rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="y" type="vs" value="1" rule="449" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="12.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5" punct="pv:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg><pgtc id="6" weight="2" schema="CR">b<rhyme label="b" id="6" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>t</rhyme></pgtc></w> ;</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="vg">E</seg>t</w>, <w n="13.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="13.3">n<seg phoneme="o" type="vs" value="1" rule="437" place="4" mp="C">o</seg>s</w> <w n="13.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="6" punct="vg" caesura="1">e</seg>rs</w>,<caesura></caesura> <w n="13.5">n<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="13.6" punct="vg:9">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="9" punct="vg">eu</seg>rs</w>, <w n="13.7">n<seg phoneme="o" type="vs" value="1" rule="437" place="10" mp="C">o</seg>s</w> <w n="13.8">d<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg><pgtc id="7" weight="2" schema="CR">f<rhyme label="a" id="7" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="14" num="1.14" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="14.2">n</w>'<w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="14.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="5">oin</seg>t</w> <w n="14.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="14.6" punct="vg:8">h<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nn<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="C">ou</seg>s</w> <w n="15.2">s<seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="M">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="15.3">d</w>'<w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6" caesura="1">an</seg></w><caesura></caesura> <w n="15.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>r</w> <w n="15.7">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="15.8"><pgtc id="7" weight="2" schema="[CR">f<rhyme label="a" id="7" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">aî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="16" num="1.16" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="16.3"><seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>rp<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5" punct="pe:8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>qu<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pe ps">eu</seg>r</rhyme></pgtc></w> !…</l>
					<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="17.2">R<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="M">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="17.4">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="17.6">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="10" mp="C">e</seg>t</w> <w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">Em</seg><pgtc id="9" weight="2" schema="CR">p<rhyme label="a" id="9" gender="f" type="a" stanza="5" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="18" num="1.18" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="18.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>g</w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="18.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.7" punct="vg:8">d<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" stanza="5" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="405" place="8" punct="vg">eu</seg>il</rhyme></pgtc></w>,</l>
					<l n="19" num="1.19" lm="12" met="6+6"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="19.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="C">i</seg>l</w> <w n="19.3">f<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t</w> <w n="19.4">qu</w>'<w n="19.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="19.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="19.7">l<seg phoneme="œ" type="vs" value="1" rule="406" place="7" mp="C">eu</seg>r</w> <w n="19.8">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.9"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>x<pgtc id="9" weight="2" schema="CR">p<rhyme label="a" id="9" gender="f" type="e" stanza="5" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="20" num="1.20" lm="8" met="8"><space unit="char" quantity="8"></space><w n="20.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg></w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.6" punct="pv:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rg<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" stanza="5" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="343" place="8" punct="pv">ue</seg>il</rhyme></pgtc></w> ;</l>
					<l n="21" num="1.21" lm="12" met="6+6"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="21.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="21.3" punct="vg:3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" punct="vg">e</seg></w>, <w n="21.4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="M">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="21.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="21.6" punct="vg:10">r<seg phoneme="y" type="vs" value="1" rule="d-3" place="8" mp="M">u</seg><seg phoneme="i" type="vs" value="1" rule="466" place="9">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="21.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="21.8" punct="vg:12">t<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" stanza="6" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="22" num="1.22" lm="8" met="8"><space unit="char" quantity="8"></space><w n="22.1">L</w>'<w n="22.2">H<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="22.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="22.5">b<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg><pgtc id="12" weight="2" schema="CR">r<rhyme label="b" id="12" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">in</seg></rhyme></pgtc></w></l>
					<l n="23" num="1.23" lm="12" met="6+6"><w n="23.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1" mp="M">In</seg>scr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3" mp="C">au</seg></w> <w n="23.3" punct="vg:6">P<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="M">an</seg>th<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="23.4"><seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg></w> <w n="23.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="P">a</seg>r</w> <w n="23.6">l<seg phoneme="œ" type="vs" value="1" rule="406" place="11" mp="C">eu</seg>rs</w> <w n="23.7" punct="vg:12">b<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e" stanza="6" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="24" num="1.24" lm="8" met="8"><space unit="char" quantity="8"></space><w n="24.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="24.2">d<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="24.3" punct="dp:3">m<seg phoneme="o" type="vs" value="1" rule="437" place="3" punct="dp">o</seg>ts</w> : <w n="24.4">P<seg phoneme="œ" type="vs" value="1" rule="406" place="4">EU</seg>PL<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">E</seg></w> <w n="24.5" punct="pe:8">S<seg phoneme="u" type="vs" value="1" rule="424" place="6">OU</seg>V<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">E</seg><pgtc id="12" weight="2" schema="CR">R<rhyme label="b" id="12" gender="m" type="e" stanza="6" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pe">AIN</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>