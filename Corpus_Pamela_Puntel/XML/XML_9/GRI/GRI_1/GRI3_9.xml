<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">REVUE DE BRETAGNE ET DE VENDÉE</title>
				<title type="sub_2">QUATORZIÈME ANNÉE</title>
				<title type="sub_1">TROISIÈME SÉRIE — TOME VIII</title>
				<title type="medium">Édition électronique</title>
				<author key="GRI">
					<name>
						<forename>Émile</forename>
						<surname>GRIMAUD</surname>
					</name>
					<date from="1831" to="1901">1831-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>413 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">STROPHES PATRIOTIQUES</title>
						<author>ÉMILE GRIMAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k453762t.r=Revue%20de%20Bretagne%20et%20de%20Vend%C3%A9e%201870?rk=85837;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>REVUE DE BRETAGNE ET DE VENDÉE</title>
								<imprint>
									<pubPlace>NANTES</pubPlace>
									<publisher>REVUE DE BRETAGNE ET DE VENDÉE — ANNÉE 1870 — DEUXIÈME SEMESTRE</publisher>
									<date when="1870">OCTOBRE 1870</date>
								</imprint>
								<biblScope unit="tome">T. 28</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-21" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Octobre 1870</head><div type="poem" key="GRI3" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="8(abab)" er_moy="1.75" er_max="6" er_min="0" er_mode="2(8/16)" er_moy_et="1.85" qr_moy="0.31" qr_max="N5" qr_mode="0(15/16)" qr_moy_et="1.21">
					<head type="main">UN SOLDAT DU PAPE</head>
					<opener>
						<salute>A LA MÉMOIRE D'AUGUSTE DE LA BROSSE</salute>
					</opener>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="1.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="1.5">s<pgtc id="1" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></pgtc></w> <w n="1.6" punct="pt:8"><pgtc id="1" weight="6" schema="V[CR" part="2">t<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="306" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="vg">oi</seg>r</w>, <w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="2.5" punct="dp:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="dp in">ai</seg>t</w> : « <w n="2.6" punct="pe:8">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg><pgtc id="2" weight="2" schema="CR" part="1">mm<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pe">en</seg>t</rhyme></pgtc></w> !</l>
						<l n="3" num="1.3" lm="8" met="8">» <w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.5" punct="pe:8">b<pgtc id="1" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>t<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="306" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="4" num="1.4" lm="8" met="8">» <w n="4.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="1">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w>-<w n="4.2">t</w>-<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="4.5">m<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s</w> <w n="4.6" punct="pi:8">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="2" weight="2" schema="CR" part="1">m<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="8" punct="pi">en</seg>t</rhyme></pgtc></w> ? »</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ts</w> <w n="5.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rts</w> <w n="5.4" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" punct="dp in">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> : « <w n="5.5" punct="pe:8">F<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg><pgtc id="3" weight="2" schema="CR" part="1">l<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> ! »</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="6.2" punct="pe:3">f<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="481" place="3" punct="pe">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ! <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4">l</w>'<w n="6.5" punct="ps:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="304" place="7">aî</seg><pgtc id="4" weight="2" schema="CR" part="1">n<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="ps">a</seg></rhyme></pgtc></w>…</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="7.2" punct="vg:2">j<seg phoneme="u" type="vs" value="1" rule="424" place="2" punct="vg">ou</seg>r</w>, <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="7.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>t</w> <w n="7.5">d</w>'<w n="7.6"><seg phoneme="i" type="vs" value="1" rule="467" place="6">I</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="3" weight="2" schema="CR" part="1">l<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="8.3">cr<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>x</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5" punct="pt:8">M<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="4" weight="2" schema="CR" part="1">n<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="9.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="9.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="5" weight="2" schema="CR" part="1">t<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="10.3">m<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="10.5" punct="pv:8"><pgtc id="6" weight="2" schema="[CR" part="1">n<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pv">ou</seg>s</rhyme></pgtc></w> ;</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="11.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2">en</seg></w> <w n="11.3">f<seg phoneme="œ" type="vs" value="1" rule="303" place="3">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="11.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="11.5">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><pgtc id="5" weight="2" schema="CR" part="1">t<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="12.2">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="12.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="5">en</seg>t</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="12.5" punct="pt:8">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="6" weight="2" schema="CR" part="1">n<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381" place="4">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="13.5" punct="pv:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg><pgtc id="7" weight="2" schema="CR" part="1">d<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="14.3"><seg phoneme="œ" type="vs" value="1" rule="285" place="3">œ</seg>il</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="14.5">l</w>'<w n="14.6" punct="pt:8">h<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>r<pgtc id="8" weight="6" schema="VCR" part="1"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>z<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="15.2">cr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="15.3" punct="dp:4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="dp in">e</seg></w> : « <w n="15.4">R<seg phoneme="ɔ" type="vs" value="1" rule="440" place="5">o</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="15.6" punct="pe:8">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>r<pgtc id="7" weight="2" schema="CR" part="1">d<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="456" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> ! »</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="16.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rt</w> <w n="16.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="16.6" punct="vg:8">tr<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>h<pgtc id="8" weight="6" schema="VCR" part="1"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>x</w> <w n="17.2">m<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>rs</w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="17.5">V<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="17.7"><pgtc id="9" weight="0" schema="[R" part="1"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="18.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4" punct="vg">ai</seg>t</w>, <w n="18.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>d</w> <w n="18.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="18.7" punct="ps:8">pr<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="ps">i</seg>t</rhyme></pgtc></w>…</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="19.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w> <w n="19.3">c<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>f</w> <w n="19.4">d</w>'<w n="19.5">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="19.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>f<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">V<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.4">J<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w>-<w n="20.5" punct="pe:8">Chr<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>st</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1" punct="vg:1">M<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1" punct="vg">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="21.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="21.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="21.5" punct="pt:8">r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="11" weight="2" schema="CR" part="1">v<rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="22.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="22.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3">en</seg></w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="22.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="22.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="63" place="6">e</seg>r</w> <w n="22.7" punct="pi:8">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="7">a</seg><pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="320" place="8" punct="pi">y</seg>s</rhyme></pgtc></w> ?</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="23.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="23.3">fl<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>ts</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.5">h<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="23.6">s<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg><pgtc id="11" weight="2" schema="CR" part="1">v<rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1" punct="vg:2">C<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="2" punct="vg">é</seg>s</w>, <w n="24.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg>x</w> <w n="24.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="24.4" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>h<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">l</w>'<w n="25.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>ds</w> <w n="25.4" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>s</w>, <w n="25.5">cl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="25.6" punct="pv:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>tr<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1" punct="vg:2">F<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="26.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="26.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="6">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="26.5" punct="pt:8"><pgtc id="14" weight="2" schema="[CR" part="1">p<rhyme label="b" id="14" gender="m" type="a" qr="N5"><seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8" punct="pt">oin</seg>t</rhyme></pgtc></w>.</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="27.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>d</w> <w n="27.3">l</w>'<w n="27.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>l</w> <w n="27.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="27.6" punct="dp:8">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2" punct="pe:3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3" punct="pe">à</seg></w> ! <w n="28.3">c<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>b<seg phoneme="i" type="vs" value="1" rule="466" place="6">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg></w> <w n="28.5" punct="pt:8"><pgtc id="14" weight="2" schema="[CR" part="1">p<rhyme label="b" id="14" gender="m" type="e" qr="N5"><seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8" punct="pt">oin</seg>g</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="8" type="quatrain" rhyme="abab">
						<l n="29" num="8.1" lm="8" met="8"><w n="29.1">L</w>'<w n="29.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="29.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="29.4">Tr<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>s</w> <w n="29.5" punct="vg:4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" punct="vg">en</seg>ts</w>, <w n="29.6"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="29.7" punct="pe:6">fr<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="29.8"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l</w> <w n="29.9" punct="pe:8">fr<pgtc id="15" weight="0" schema="R" part="1"><rhyme label="a" id="15" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></rhyme></pgtc></w> !…</l>
						<l n="30" num="8.2" lm="8" met="8"><w n="30.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg>s</w>, <w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="30.3"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg></w> <w n="30.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">am</seg>p</w> <w n="30.5">d</w>'<w n="30.6" punct="dp:8">h<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>nn<pgtc id="16" weight="0" schema="R" part="1"><rhyme label="b" id="16" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="dp">eu</seg>r</rhyme></pgtc></w> :</l>
						<l n="31" num="8.3" lm="8" met="8">« <w n="31.1" punct="vg:1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" punct="vg">en</seg>ds</w>, <w n="31.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t</w>-<w n="31.3" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>l</w>, <w n="31.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="31.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>t</w> <w n="31.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="31.7" punct="vg:8">P<pgtc id="15" weight="0" schema="R" part="1"><rhyme label="a" id="15" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="32" num="8.4" lm="8" met="8">» <w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="32.2">s<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="32.4" punct="vg:5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" punct="vg">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="32.5"><seg phoneme="o" type="vs" value="1" rule="414" place="6">ô</seg></w> <w n="32.6" punct="pe:8">S<seg phoneme="ɛ" type="vs" value="1" rule="383" place="7">ei</seg>gn<pgtc id="16" weight="0" schema="R" part="1"><rhyme label="b" id="16" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pe">eu</seg>r</rhyme></pgtc></w> ! »</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">24 octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>