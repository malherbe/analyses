<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM11" modus="sm" lm_max="8" metProfile="8" form="sonnet classique, prototype 1" schema="abba abba ccd eed" er_moy="0.86" er_max="2" er_min="0" er_mode="0(4/7)" er_moy_et="0.99" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="main">LES BORDS DE LA TET</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="1.2">br<seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="1.5">l</w>’<w n="1.6">h<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>z<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="2.2">C<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="2.5" punct="pv:8">bl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="3.2" punct="vg:4">m<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="3.6" punct="vg:8">br<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="vg:3">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>nt</w>, <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="4.4">v<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>f</w> <w n="4.5" punct="vg:8">fr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ss<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="5.2">br<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">fr<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="5.5" punct="pt:8">bu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg><pgtc id="3" weight="2" schema="CR">ss<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="6.4" punct="pe:8">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rv<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="7.2">s<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">n<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>r</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="7.6">fl<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>ts</w> <w n="7.7" punct="pv:8">p<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="8.2">T<seg phoneme="ɛ" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="8.3">m<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="8.5">tr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.6" punct="pt:8"><pgtc id="3" weight="2" schema="[CR">s<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="9.2" punct="vg:2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="9.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.5">h<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.6" punct="vg:8">n<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>f<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="10.4">b<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rds</w> <w n="10.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.6">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>v<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="11.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="11.5" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pt">an</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="8" met="8"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="12.2">c<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>s</w> <w n="12.3">tr<seg phoneme="o" type="vs" value="1" rule="432" place="6">o</seg>p</w> <w n="12.4" punct="pe:8">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>r<pgtc id="7" weight="2" schema="CR">t<rhyme label="e" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
					<l n="13" num="4.2" lm="8" met="8"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="13.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">A</seg><pgtc id="7" weight="2" schema="CR">th<rhyme label="e" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</rhyme></pgtc></w> :</l>
					<l n="14" num="4.3" lm="8" met="8"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="14.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="14.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="14.6" punct="pt:8">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" punct="pt">em</seg>ps</rhyme></pgtc></w>.<ref target="1" type="noteAnchor">1</ref></l>
				</lg>
				<ab type="dot">──────────────────────────────────────────────────</ab>
				<closer>
					<note type="footnote" id="1">C’est le mot de Périclès.</note>
				</closer>
			</div></body></text></TEI>