<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA FRONTIÈRE</title>
				<title type="sub">ESSAIS DE POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BAY">
					<name>
						<forename>Hippolyte</forename>
						<surname>BAYE</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>864 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BAY_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FRONTIÈRE. ESSAIS DE POÉSIES</title>
						<author>BAYE, HIPPOLYTE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54640834.r=BAYE%2C%20HIPPOLYTE%20ESSAIS%20DE%20PO%C3%89SIES%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA FRONTIÈRE. ESSAIS DE POÉSIES</title>
								<author>BAYE, HIPPOLYTE</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE LACROIX, VERBOECKHOVËN ET CIE ÉDITEURS</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAY14">
				<head type="main">LE GUÉ</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">n</w>'<w n="1.3">est</w> <w n="1.4">plus</w> <w n="1.5">doux</w> <w n="1.6">abri</w> <w n="1.7">dans</w> <w n="1.8">la</w> <w n="1.9">calme</w> <w n="1.10">vallée</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Il</w> <w n="2.2">n</w>'<w n="2.3">est</w> <w n="2.4">gazon</w> <w n="2.5">plus</w> <w n="2.6">mol</w> <w n="2.7">au</w> <w n="2.8">loisir</w> <w n="2.9">nonchalant</w></l>
					<l n="3" num="1.3"><w n="3.1">Que</w> <w n="3.2">la</w> <w n="3.3">rive</w> <w n="3.4">penchante</w> <w n="3.5">et</w> <w n="3.6">de</w> <w n="3.7">saules</w> <w n="3.8">voilée</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">ces</w> <w n="4.3">bords</w> <w n="4.4">sinueux</w> <w n="4.5">du</w> <w n="4.6">fleuve</w>, <w n="4.7">déroulant</w></l>
					<l n="5" num="1.5"><w n="5.1">Son</w> <w n="5.2">écharpe</w> <w n="5.3">bleuâtre</w> <w n="5.4">aux</w> <w n="5.5">joncs</w> <w n="5.6">souples</w> <w n="5.7">mêlée</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Là</w> <w n="6.2">croissent</w>, <w n="6.3">en</w> <w n="6.4">flottant</w>, <w n="6.5">les</w> <w n="6.6">parfums</w> <w n="6.7">des</w> <w n="6.8">cressons</w>,</l>
					<l n="7" num="2.2"><w n="7.1">Autour</w> <w n="7.2">du</w> <w n="7.3">sable</w> <w n="7.4">où</w> <w n="7.5">fuit</w> <w n="7.6">l</w>'<w n="7.7">ablette</w>, <w n="7.8">sémillante</w>.</l>
					<l n="8" num="2.3"><w n="8.1">Le</w> <w n="8.2">frais</w> <w n="8.3">myosotis</w> <w n="8.4">y</w> <w n="8.5">trempe</w> <w n="8.6">ses</w> <w n="8.7">buissons</w> ;</l>
					<l n="9" num="2.4"><w n="9.1">La</w> <w n="9.2">forêt</w> <w n="9.3">de</w> <w n="9.4">roseaux</w>, <w n="9.5">légère</w> <w n="9.6">et</w> <w n="9.7">vacillante</w>,</l>
					<l n="10" num="2.5"><w n="10.1">A</w> <w n="10.2">tout</w> <w n="10.3">vent</w> <w n="10.4">qui</w> <w n="10.5">frémit</w> <w n="10.6">exhale</w> <w n="10.7">de</w> <w n="10.8">vains</w> <w n="10.9">sons</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">Comme</w> <w n="11.2">tout</w> <w n="11.3">souriait</w> <w n="11.4">jadis</w> <w n="11.5">sur</w> <w n="11.6">cette</w> <w n="11.7">rive</w> !</l>
					<l n="12" num="3.2"><w n="12.1">L</w>'<w n="12.2">écolier</w> <w n="12.3">vagabond</w>, <w n="12.4">inhabile</w> <w n="12.5">lutteur</w>,</l>
					<l n="13" num="3.3"><w n="13.1">Y</w> <w n="13.2">venait</w> <w n="13.3">exercer</w> <w n="13.4">son</w> <w n="13.5">adresse</w> <w n="13.6">craintive</w>,</l>
					<l n="14" num="3.4"><w n="14.1">Sondait</w> <w n="14.2">de</w> <w n="14.3">son</w> <w n="14.4">pied</w> <w n="14.5">nu</w> <w n="14.6">le</w> <w n="14.7">gouffre</w> <w n="14.8">tentateur</w>,</l>
					<l n="15" num="3.5"><w n="15.1">Et</w>, <w n="15.2">les</w> <w n="15.3">bras</w> <w n="15.4">arrondis</w>, <w n="15.5">domptait</w> <w n="15.6">l</w>'<w n="15.7">onde</w> <w n="15.8">rétive</w>.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">C</w>'<w n="16.2">est</w> <w n="16.3">là</w> <w n="16.4">qu</w>'<w n="16.5">au</w> <w n="16.6">soir</w> <w n="16.7">tombant</w> <w n="16.8">les</w> <w n="16.9">troupeaux</w> <w n="16.10">égarés</w>,</l>
					<l n="17" num="4.2"><w n="17.1">Oubliés</w> <w n="17.2">des</w> <w n="17.3">abois</w> <w n="17.4">du</w> <w n="17.5">molosse</w> <w n="17.6">fidèle</w>,</l>
					<l n="18" num="4.3"><w n="18.1">Descendaient</w> <w n="18.2">lentement</w>, <w n="18.3">tachant</w> <w n="18.4">le</w> <w n="18.5">vert</w> <w n="18.6">des</w> <w n="18.7">prés</w>.</l>
					<l n="19" num="4.4"><w n="19.1">L</w>'<w n="19.2">épervier</w> <w n="19.3">et</w> <w n="19.4">l</w>'<w n="19.5">agneau</w>, <w n="19.6">le</w> <w n="19.7">bœuf</w> <w n="19.8">et</w> <w n="19.9">l</w>'<w n="19.10">hirondelle</w>,</l>
					<l n="20" num="4.5"><w n="20.1">Sur</w> <w n="20.2">le</w> <w n="20.3">même</w> <w n="20.4">abreuvoir</w> <w n="20.5">se</w> <w n="20.6">penchaient</w> <w n="20.7">altérés</w>.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">Mais</w> <w n="21.2">un</w> <w n="21.3">jour</w>, <w n="21.4">effrayant</w> <w n="21.5">les</w> <w n="21.6">tremblantes</w> <w n="21.7">colombes</w>,</l>
					<l n="22" num="5.2"><w n="22.1">Une</w> <w n="22.2">ardente</w> <w n="22.3">bataille</w> <w n="22.4">ici</w> <w n="22.5">se</w> <w n="22.6">déchaîna</w>.</l>
					<l n="23" num="5.3"><w n="23.1">Le</w> <w n="23.2">duel</w> <w n="23.3">aérien</w> <w n="23.4">des</w> <w n="23.5">obus</w> <w n="23.6">et</w> <w n="23.7">des</w> <w n="23.8">bombes</w>,</l>
					<l n="24" num="5.4"><w n="24.1">En</w> <w n="24.2">longs</w> <w n="24.3">sillons</w> <w n="24.4">de</w> <w n="24.5">feu</w> <w n="24.6">dans</w> <w n="24.7">l</w>'<w n="24.8">azur</w> <w n="24.9">rayonna</w> ;</l>
					<l n="25" num="5.5"><w n="25.1">Et</w> <w n="25.2">des</w> <w n="25.3">humains</w> <w n="25.4">tombaient</w>, <w n="25.5">tombaient</w> <w n="25.6">par</w> <w n="25.7">hécatombes</w> !</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">Au</w> <w n="26.2">souffle</w> <w n="26.3">mugissant</w> <w n="26.4">de</w> <w n="26.5">l</w>'<w n="26.6">ouragan</w> <w n="26.7">fatal</w>,</l>
					<l n="27" num="6.2"><w n="27.1">Que</w> <w n="27.2">d</w>'<w n="27.3">âmes</w> <w n="27.4">en</w> <w n="27.5">leur</w> <w n="27.6">fleur</w>, <w n="27.7">là</w>, <w n="27.8">se</w> <w n="27.9">sont</w> <w n="27.10">envolées</w> !</l>
					<l n="28" num="6.3"><w n="28.1">Que</w> <w n="28.2">de</w> <w n="28.3">cris</w> <w n="28.4">ont</w> <w n="28.5">monté</w> <w n="28.6">vers</w> <w n="28.7">le</w> <w n="28.8">grand</w> <w n="28.9">tribunal</w> !</l>
					<l n="29" num="6.4"><w n="29.1">Et</w>, <w n="29.2">quand</w> <w n="29.3">le</w> <w n="29.4">plomb</w> <w n="29.5">rompait</w> <w n="29.6">les</w> <w n="29.7">phalanges</w> <w n="29.8">troublées</w>,</l>
					<l n="30" num="6.5"><w n="30.1">Que</w> <w n="30.2">de</w> <w n="30.3">sang</w> <w n="30.4">a</w> <w n="30.5">rougi</w> <w n="30.6">ce</w> <w n="30.7">fluide</w> <w n="30.8">cristal</w> !</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><w n="31.1">Car</w> <w n="31.2">la</w> <w n="31.3">guerre</w> <w n="31.4">n</w>'<w n="31.5">est</w> <w n="31.6">pas</w> <w n="31.7">aisément</w> <w n="31.8">assouvie</w>.</l>
					<l n="32" num="7.2"><w n="32.1">Et</w> <w n="32.2">l</w>'<w n="32.3">obus</w> <w n="32.4">poursuivait</w> <w n="32.5">de</w> <w n="32.6">ses</w> <w n="32.7">éclats</w> <w n="32.8">d</w>'<w n="32.9">acier</w></l>
					<l n="33" num="7.3"><w n="33.1">Le</w> <w n="33.2">blessé</w> <w n="33.3">qui</w> <w n="33.4">traînait</w> <w n="33.5">le</w> <w n="33.6">reste</w> <w n="33.7">de</w> <w n="33.8">sa</w> <w n="33.9">vie</w>,</l>
					<l n="34" num="7.4"><w n="34.1">L</w>'<w n="34.2">artilleur</w> <w n="34.3">qui</w> <w n="34.4">poussait</w>, <w n="34.5">son</w> <w n="34.6">robuste</w> <w n="34.7">coursier</w>,</l>
					<l n="35" num="7.5"><w n="35.1">A</w> <w n="35.2">travers</w> <w n="35.3">le</w> <w n="35.4">courant</w>, <w n="35.5">vers</w> <w n="35.6">l</w>'<w n="35.7">autre</w> <w n="35.8">berge</w> <w n="35.9">amie</w>.</l>
				</lg>
				<lg n="8">
					<l n="36" num="8.1"><w n="36.1">Que</w> <w n="36.2">d</w>'<w n="36.3">hommes</w>, <w n="36.4">enlacés</w> <w n="36.5">par</w> <w n="36.6">le</w> <w n="36.7">bras</w> <w n="36.8">de</w> <w n="36.9">la</w> <w n="36.10">Mort</w>,</l>
					<l n="37" num="8.2"><w n="37.1">Ont</w> <w n="37.2">vu</w> <w n="37.3">fuir</w> <w n="37.4">à</w> <w n="37.5">la</w> <w n="37.6">fois</w> <w n="37.7">le</w> <w n="37.8">rivage</w> <w n="37.9">et</w> <w n="37.10">le</w> <w n="37.11">monde</w> !</l>
					<l n="38" num="8.3"><w n="38.1">Que</w> <w n="38.2">de</w> <w n="38.3">corps</w> <w n="38.4">non</w> <w n="38.5">pleurés</w> <w n="38.6">ont</w>, <w n="38.7">sur</w> <w n="38.8">ce</w> <w n="38.9">même</w> <w n="38.10">bord</w>,</l>
					<l n="39" num="8.4"><w n="39.1">Tournoyé</w> <w n="39.2">lentement</w>, <w n="39.3">pâles</w>, <w n="39.4">au</w> <w n="39.5">fil</w> <w n="39.6">de</w> <w n="39.7">l</w>'<w n="39.8">onde</w> !…</l>
					<l n="40" num="8.5">— <w n="40.1">Et</w> <w n="40.2">pourtant</w> (<w n="40.3">ô</w> <w n="40.4">Nature</w> !) <w n="40.5">aujourd</w>'<w n="40.6">hui</w> <w n="40.7">même</w> <w n="40.8">encor</w>,</l>
				</lg>
				<lg n="9">
					<l n="41" num="9.1"><w n="41.1">Il</w> <w n="41.2">n</w>'<w n="41.3">est</w> <w n="41.4">plus</w> <w n="41.5">doux</w> <w n="41.6">abri</w> <w n="41.7">dans</w> <w n="41.8">la</w> <w n="41.9">calme</w> <w n="41.10">vallée</w>,</l>
					<l n="42" num="9.2"><w n="42.1">Il</w> <w n="42.2">n</w>'<w n="42.3">est</w> <w n="42.4">gazon</w> <w n="42.5">plus</w> <w n="42.6">mol</w> <w n="42.7">au</w> <w n="42.8">loisir</w> <w n="42.9">nonchalant</w>,</l>
					<l n="43" num="9.3"><w n="43.1">Que</w> <w n="43.2">la</w> <w n="43.3">rive</w> <w n="43.4">penchante</w> <w n="43.5">et</w> <w n="43.6">de</w> <w n="43.7">saules</w> <w n="43.8">voilée</w>,</l>
					<l n="44" num="9.4"><w n="44.1">Que</w> <w n="44.2">ces</w> <w n="44.3">bords</w> <w n="44.4">sinueux</w> <w n="44.5">du</w> <w n="44.6">fleuve</w>, <w n="44.7">déroulant</w></l>
					<l n="45" num="9.5"><w n="45.1">Son</w> <w n="45.2">écharpe</w> <w n="45.3">bleuâtre</w> <w n="45.4">aux</w> <w n="45.5">joncs</w> <w n="45.6">souples</w> <w n="45.7">mêlée</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Mai 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>