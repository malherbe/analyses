<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA FRONTIÈRE</title>
				<title type="sub">ESSAIS DE POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BAY">
					<name>
						<forename>Hippolyte</forename>
						<surname>BAYE</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>864 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BAY_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FRONTIÈRE. ESSAIS DE POÉSIES</title>
						<author>BAYE, HIPPOLYTE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54640834.r=BAYE%2C%20HIPPOLYTE%20ESSAIS%20DE%20PO%C3%89SIES%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA FRONTIÈRE. ESSAIS DE POÉSIES</title>
								<author>BAYE, HIPPOLYTE</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE LACROIX, VERBOECKHOVËN ET CIE ÉDITEURS</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAY12">
				<head type="main">LA SUISSE</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">Voici</w> <w n="1.2">la</w> <w n="1.3">saison</w> <w n="1.4">de</w> <w n="1.5">clémence</w> !</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Laboureur</w>, <w n="2.2">prends</w> <w n="2.3">le</w> <w n="2.4">soc</w> <w n="2.5">en</w> <w n="2.6">main</w> ;</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">A</w> <w n="3.2">la</w> <w n="3.3">glèbe</w> <w n="3.4">unis</w> <w n="3.5">la</w> <w n="3.6">semence</w>.</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Toi</w>, <w n="4.2">soleil</w>, <w n="4.3">bénis</w> <w n="4.4">leur</w> <w n="4.5">hymen</w>.</l>
					<l n="5" num="1.5"><w n="5.1">La</w> <w n="5.2">terre</w> <w n="5.3">a</w> <w n="5.4">craint</w> <w n="5.5">un</w> <w n="5.6">éternel</w> <w n="5.7">veuvage</w>,</l>
					<l n="6" num="1.6"><w n="6.1">En</w> <w n="6.2">fossoyeurs</w> <w n="6.3">se</w> <w n="6.4">changeaient</w> <w n="6.5">les</w> <w n="6.6">colons</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Mais</w>, <w n="7.2">pour</w> <w n="7.3">fermer</w> <w n="7.4">cette</w> <w n="7.5">ère</w> <w n="7.6">de</w> <w n="7.7">ravage</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Un</w> <w n="8.2">peuple</w> <w n="8.3">ami</w> <w n="8.4">dépouille</w> <w n="8.5">ses</w> <w n="8.6">vallons</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="4"></space><w n="9.1">La</w> <w n="9.2">France</w> <w n="9.3">eut</w> <w n="9.4">des</w> <w n="9.5">jours</w> <w n="9.6">de</w> <w n="9.7">richesse</w>.</l>
					<l n="10" num="2.2"><space unit="char" quantity="4"></space><w n="10.1">Des</w> <w n="10.2">États</w> <w n="10.3">oublieux</w> <w n="10.4">l</w>'<w n="10.5">ont</w> <w n="10.6">su</w>.</l>
					<l n="11" num="2.3"><space unit="char" quantity="4"></space><w n="11.1">L</w>'<w n="11.2">ennemi</w> <w n="11.3">même</w> <w n="11.4">qui</w> <w n="11.5">l</w>'<w n="11.6">oppresse</w></l>
					<l n="12" num="2.4"><space unit="char" quantity="4"></space><w n="12.1">Pourrait</w> <w n="12.2">dire</w> <w n="12.3">le</w> <w n="12.4">pain</w> <w n="12.5">reçu</w>.</l>
					<l n="13" num="2.5"><w n="13.1">Booz</w> <w n="13.2">connaît</w> <w n="13.3">la</w> <w n="13.4">disette</w> <w n="13.5">cruelle</w></l>
					<l n="14" num="2.6"><w n="14.1">Dans</w> <w n="14.2">ses</w> <w n="14.3">champs</w>, <w n="14.4">où</w> <w n="14.5">le</w> <w n="14.6">pauvre</w> <w n="14.7">a</w> <w n="14.8">tant</w> <w n="14.9">glané</w>.</l>
					<l n="15" num="2.7"><w n="15.1">Honneur</w> <w n="15.2">à</w> <w n="15.3">Ruth</w> ! <w n="15.4">elle</w> <w n="15.5">offre</w> <w n="15.6">sa</w> <w n="15.7">javelle</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Elle</w> <w n="16.2">sourit</w> <w n="16.3">au</w> <w n="16.4">maître</w> <w n="16.5">infortuné</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space unit="char" quantity="4"></space><w n="17.1">O</w> <w n="17.2">Suisse</w> ! <w n="17.3">ô</w> <w n="17.4">terre</w> <w n="17.5">généreuse</w> !</l>
					<l n="18" num="3.2"><space unit="char" quantity="4"></space><w n="18.1">Ton</w> <w n="18.2">cœur</w> <w n="18.3">ici</w> <w n="18.4">s</w>'<w n="18.5">est</w> <w n="18.6">révélé</w></l>
					<l n="19" num="3.3"><space unit="char" quantity="4"></space><w n="19.1">Insensible</w> <w n="19.2">à</w> <w n="19.3">la</w> <w n="19.4">force</w> <w n="19.5">heureuse</w>,</l>
					<l n="20" num="3.4"><space unit="char" quantity="4"></space><w n="20.1">Épris</w> <w n="20.2">du</w> <w n="20.3">courage</w> <w n="20.4">accablé</w> !</l>
					<l n="21" num="3.5"><w n="21.1">Sans</w> <w n="21.2">écouter</w> <w n="21.3">siffler</w> <w n="21.4">la</w> <w n="21.5">calomnie</w>,</l>
					<l n="22" num="3.6"><w n="22.1">Pour</w> <w n="22.2">nos</w> <w n="22.3">vaincus</w> <w n="22.4">tu</w> <w n="22.5">parais</w> <w n="22.6">ton</w> <w n="22.7">berceau</w>.</l>
					<l n="23" num="3.7"><w n="23.1">Les</w> <w n="23.2">fils</w> <w n="23.3">errants</w> <w n="23.4">du</w> <w n="23.5">chantre</w> <w n="23.6">d</w>'<w n="23.7">Athalie</w></l>
					<l n="24" num="3.8"><w n="24.1">Ont</w> <w n="24.2">dû</w> <w n="24.3">la</w> <w n="24.4">vie</w> <w n="24.5">aux</w> <w n="24.6">neveux</w> <w n="24.7">de</w> <w n="24.8">Rousseau</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space unit="char" quantity="4"></space><w n="25.1">Qu</w>'<w n="25.2">on</w> <w n="25.3">aime</w> <w n="25.4">à</w> <w n="25.5">deviner</w> <w n="25.6">en</w> <w n="25.7">rêve</w></l>
					<l n="26" num="4.2"><space unit="char" quantity="4"></space><w n="26.1">Tes</w> <w n="26.2">lacs</w>, <w n="26.3">tes</w> <w n="26.4">monts</w> <w n="26.5">et</w> <w n="26.6">tes</w> <w n="26.7">torrents</w> !</l>
					<l n="27" num="4.3"><space unit="char" quantity="4"></space><w n="27.1">Là</w>-<w n="27.2">bas</w> <w n="27.3">les</w> <w n="27.4">ennemis</w> <w n="27.5">font</w> <w n="27.6">trêve</w></l>
					<l n="28" num="4.4"><space unit="char" quantity="4"></space><w n="28.1">Et</w> <w n="28.2">mêlent</w> <w n="28.3">leurs</w> <w n="28.4">flots</w> <w n="28.5">émigrants</w>.</l>
					<l n="29" num="4.5"><w n="29.1">Là</w>-<w n="29.2">bas</w>, <w n="29.3">surtout</w>, <w n="29.4">aucune</w> <w n="29.5">ombre</w> <w n="29.6">de</w> <w n="29.7">maître</w></l>
					<l n="30" num="4.6"><w n="30.1">N</w>'<w n="30.2">ose</w> <w n="30.3">infecter</w> <w n="30.4">l</w>'<w n="30.5">âme</w> <w n="30.6">du</w> <w n="30.7">pèlerin</w>.</l>
					<l n="31" num="4.7"><w n="31.1">Suisses</w>, <w n="31.2">jamais</w> <w n="31.3">nul</w> <w n="31.4">n</w>'<w n="31.5">a</w> <w n="31.6">pu</w> <w n="31.7">vous</w> <w n="31.8">soumettre</w> :</l>
					<l n="32" num="4.8"><w n="32.1">Le</w> <w n="32.2">Devoir</w> <w n="32.3">seul</w> <w n="32.4">est</w> <w n="32.5">votre</w> <w n="32.6">souverain</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><space unit="char" quantity="4"></space><w n="33.1">Vos</w> <w n="33.2">dons</w> <w n="33.3">consolent</w> <w n="33.4">la</w> <w n="33.5">nature</w>.</l>
					<l n="34" num="5.2"><space unit="char" quantity="4"></space><w n="34.1">Elle</w> <w n="34.2">a</w> <w n="34.3">trop</w> <w n="34.4">gémi</w> <w n="34.5">sous</w> <w n="34.6">le</w> <w n="34.7">poids</w></l>
					<l n="35" num="5.3"><space unit="char" quantity="4"></space><w n="35.1">Des</w> <w n="35.2">guerres</w> <w n="35.3">qui</w>, <w n="35.4">pour</w> <w n="35.5">leur</w> <w n="35.6">pâture</w>,</l>
					<l n="36" num="5.4"><space unit="char" quantity="4"></space><w n="36.1">Épuisaient</w> <w n="36.2">bourgs</w>, <w n="36.3">prés</w>, <w n="36.4">champs</w> <w n="36.5">et</w> <w n="36.6">bois</w>.</l>
					<l n="37" num="5.5"><w n="37.1">Le</w> <w n="37.2">blé</w> <w n="37.3">tombant</w> <w n="37.4">de</w> <w n="37.5">vos</w> <w n="37.6">mains</w> <w n="37.7">fraternelles</w></l>
					<l n="38" num="5.6"><w n="38.1">En</w> <w n="38.2">verts</w> <w n="38.3">parfums</w> <w n="38.4">bientôt</w> <w n="38.5">aura</w> <w n="38.6">germé</w>.</l>
					<l n="39" num="5.7"><w n="39.1">Le</w> <w n="39.2">vent</w> <w n="39.3">de</w> <w n="39.4">France</w> <w n="39.5">en</w> <w n="39.6">chargera</w> <w n="39.7">ses</w> <w n="39.8">ailes</w>,</l>
					<l n="40" num="5.8"><w n="40.1">Et</w> <w n="40.2">vous</w> <w n="40.3">prépare</w> <w n="40.4">un</w> <w n="40.5">message</w> <w n="40.6">embaumé</w>.</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><space unit="char" quantity="4"></space><w n="41.1">Chaque</w> <w n="41.2">grain</w> <w n="41.3">d</w>'<w n="41.4">une</w> <w n="41.5">gerbe</w> <w n="41.6">est</w> <w n="41.7">père</w> ;</l>
					<l n="42" num="6.2"><space unit="char" quantity="4"></space><w n="42.1">La</w> <w n="42.2">gerbe</w> <w n="42.3">aura</w> <w n="42.4">mille</w> <w n="42.5">épis</w> <w n="42.6">mûrs</w>.</l>
					<l n="43" num="6.3"><space unit="char" quantity="4"></space><w n="43.1">On</w> <w n="43.2">verra</w> <w n="43.3">l</w>'<w n="43.4">aisance</w> <w n="43.5">prospère</w></l>
					<l n="44" num="6.4"><space unit="char" quantity="4"></space><w n="44.1">Des</w> <w n="44.2">ruines</w> <w n="44.3">relever</w> <w n="44.4">les</w> <w n="44.5">murs</w>.</l>
					<l n="45" num="6.5"><w n="45.1">Et</w> <w n="45.2">quand</w> <w n="45.3">viendront</w> <w n="45.4">les</w> <w n="45.5">tempêtes</w> <w n="45.6">neigeuses</w>,</l>
					<l n="46" num="6.6"><w n="46.1">A</w> <w n="46.2">leur</w> <w n="46.3">foyer</w>, <w n="46.4">tranquilles</w>, <w n="46.5">les</w> <w n="46.6">époux</w></l>
					<l n="47" num="6.7"><w n="47.1">Réveilleront</w>, <w n="47.2">des</w> <w n="47.3">luttes</w> <w n="47.4">orageuses</w>,</l>
					<l n="48" num="6.8"><w n="48.1">Deux</w> <w n="48.2">souvenirs</w>… <w n="48.3">Le</w> <w n="48.4">vôtre</w> <w n="48.5">sera</w> <w n="48.6">doux</w> !</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1"><space unit="char" quantity="4"></space><w n="49.1">Dans</w> <w n="49.2">le</w> <w n="49.3">destin</w> <w n="49.4">qui</w> <w n="49.5">nous</w> <w n="49.6">afflige</w></l>
					<l n="50" num="7.2"><space unit="char" quantity="4"></space><w n="50.1">Renaissent</w> <w n="50.2">d</w>'<w n="50.3">antiques</w> <w n="50.4">vertus</w></l>
					<l n="51" num="7.3"><space unit="char" quantity="4"></space><w n="51.1">Par</w> <w n="51.2">qui</w> <w n="51.3">reprendront</w> <w n="51.4">leur</w> <w n="51.5">prestige</w></l>
					<l n="52" num="7.4"><space unit="char" quantity="4"></space><w n="52.1">Nos</w> <w n="52.2">drapeaux</w>, <w n="52.3">hélas</w> ! <w n="52.4">abattus</w>.</l>
					<l n="53" num="7.5"><w n="53.1">Sur</w> <w n="53.2">le</w> <w n="53.3">Jura</w> <w n="53.4">les</w> <w n="53.5">feux</w> <w n="53.6">de</w> <w n="53.7">délivrance</w></l>
					<l n="54" num="7.6"><w n="54.1">Luiront</w> <w n="54.2">un</w> <w n="54.3">jour</w> ; <w n="54.4">mais</w> <w n="54.5">alors</w> <w n="54.6">point</w> <w n="54.7">d</w>'<w n="54.8">oubli</w> !</l>
					<l n="55" num="7.7"><w n="55.1">Ils</w> <w n="55.2">vous</w> <w n="55.3">peindront</w> <w n="55.4">l</w>'<w n="55.5">amitié</w> <w n="55.6">de</w> <w n="55.7">la</w> <w n="55.8">France</w>,</l>
					<l n="56" num="7.8"><w n="56.1">Et</w> <w n="56.2">leur</w> <w n="56.3">reflet</w> <w n="56.4">salûra</w> <w n="56.5">le</w> <w n="56.6">Grûtli</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Mars 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>