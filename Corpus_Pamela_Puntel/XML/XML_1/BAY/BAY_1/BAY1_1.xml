<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA FRONTIÈRE</title>
				<title type="sub">ESSAIS DE POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BAY">
					<name>
						<forename>Hippolyte</forename>
						<surname>BAYE</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>864 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BAY_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FRONTIÈRE. ESSAIS DE POÉSIES</title>
						<author>BAYE, HIPPOLYTE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54640834.r=BAYE%2C%20HIPPOLYTE%20ESSAIS%20DE%20PO%C3%89SIES%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA FRONTIÈRE. ESSAIS DE POÉSIES</title>
								<author>BAYE, HIPPOLYTE</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE LACROIX, VERBOECKHOVËN ET CIE ÉDITEURS</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAY1">
				<head type="main">AU LECTEUR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Lecteur</w>, <w n="1.2">si</w> <w n="1.3">quelque</w> <w n="1.4">accent</w> <w n="1.5">sauvage</w></l>
					<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">ces</w> <w n="2.3">chants</w> <w n="2.4">te</w> <w n="2.5">laisse</w> <w n="2.6">irrité</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Souviens</w>-<w n="3.2">toi</w> <w n="3.3">qu</w>'<w n="3.4">ils</w> <w n="3.5">ont</w> <w n="3.6">éclaté</w></l>
					<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">la</w> <w n="4.3">guerre</w> <w n="4.4">et</w> <w n="4.5">dans</w> <w n="4.6">l</w>'<w n="4.7">esclavage</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">notre</w> <w n="5.3">seuil</w> <w n="5.4">ensanglanté</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Mais</w> <w n="6.2">le</w> <w n="6.3">sang</w> <w n="6.4">n</w>'<w n="6.5">était</w> <w n="6.6">rien</w> <w n="6.7">encore</w>…</l>
					<l n="7" num="2.2"><w n="7.1">J</w>'<w n="7.2">ai</w> <w n="7.3">vu</w> <w n="7.4">parmi</w> <w n="7.5">nos</w> <w n="7.6">frais</w> <w n="7.7">enclos</w>,</l>
					<l n="8" num="2.3"><w n="8.1">A</w> <w n="8.2">la</w> <w n="8.3">honte</w> <w n="8.4">des</w> <w n="8.5">vieux</w> <w n="8.6">héros</w>,</l>
					<l n="9" num="2.4"><w n="9.1">Sous</w> <w n="9.2">un</w> <w n="9.3">monarque</w> <w n="9.4">au</w> <w n="9.5">nom</w> <w n="9.6">sonore</w>,</l>
					<l n="10" num="2.5"><w n="10.1">L</w>'<w n="10.2">honneur</w> <w n="10.3">français</w> <w n="10.4">couler</w> <w n="10.5">à</w> <w n="10.6">flots</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">Lecteur</w>, <w n="11.2">garde</w>-<w n="11.3">toi</w> <w n="11.4">donc</w> <w n="11.5">de</w> <w n="11.6">croire</w></l>
					<l n="12" num="3.2"><w n="12.1">Que</w> <w n="12.2">j</w>'<w n="12.3">aime</w> <w n="12.4">à</w> <w n="12.5">grossir</w> <w n="12.6">nos</w> <w n="12.7">douleurs</w>.</l>
					<l n="13" num="3.3"><w n="13.1">J</w>'<w n="13.2">ai</w> <w n="13.3">pesé</w> <w n="13.4">ce</w> <w n="13.5">que</w> <w n="13.6">vaut</w> <w n="13.7">la</w> <w n="13.8">gloire</w>,</l>
					<l n="14" num="3.4"><w n="14.1">Ce</w> <w n="14.2">que</w>, <w n="14.3">pour</w> <w n="14.4">étonner</w> <w n="14.5">l</w>'<w n="14.6">histoire</w>,</l>
					<l n="15" num="3.5"><w n="15.1">Certains</w> <w n="15.2">hommes</w> <w n="15.3">coûtent</w> <w n="15.4">de</w> <w n="15.5">pleurs</w>…</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">En</w> <w n="16.2">exprimant</w> <w n="16.3">l</w>'<w n="16.4">horreur</w> <w n="16.5">qu</w>'<w n="16.6">inspire</w></l>
					<l n="17" num="4.2"><w n="17.1">Des</w> <w n="17.2">rois</w> <w n="17.3">le</w> <w n="17.4">caprice</w> <w n="17.5">fatal</w>,</l>
					<l n="18" num="4.3"><w n="18.1">Je</w> <w n="18.2">n</w>'<w n="18.3">ai</w> <w n="18.4">fait</w> <w n="18.5">ici</w> <w n="18.6">que</w> <w n="18.7">traduire</w></l>
					<l n="19" num="4.4"><w n="19.1">Le</w> <w n="19.2">silence</w> <w n="19.3">des</w> <w n="19.4">morts</w> <w n="19.5">et</w> <w n="19.6">dire</w></l>
					<l n="20" num="4.5"><w n="20.1">Les</w> <w n="20.2">cris</w> <w n="20.3">sortant</w> <w n="20.4">de</w> <w n="20.5">l</w>'<w n="20.6">hôpital</w>.</l>
				</lg>
			</div></body></text></TEI>