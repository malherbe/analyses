<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP19">
				<head type="number">XIX</head>
				<head type="main">DEVANT UN BERCEAU</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">la</w> <w n="1.3">voyais</w> <w n="1.4">dormir</w> <w n="1.5">dans</w> <w n="1.6">son</w> <w n="1.7">petit</w> <w n="1.8">berceau</w>.</l>
				</lg>
				<lg n="2">
					<l n="2" num="2.1"><w n="2.1">Le</w> <w n="2.2">sommeil</w> <w n="2.3">de</w> <w n="2.4">l</w>'<w n="2.5">enfant</w> <w n="2.6">et</w> <w n="2.7">celui</w> <w n="2.8">de</w> <w n="2.9">l</w>'<w n="2.10">oiseau</w></l>
					<l n="3" num="2.2"><w n="3.1">Qui</w>, <w n="3.2">la</w> <w n="3.3">tête</w> <w n="3.4">sous</w> <w n="3.5">l</w>'<w n="3.6">aile</w>, <w n="3.7">est</w> <w n="3.8">perché</w> <w n="3.9">sur</w> <w n="3.10">la</w> <w n="3.11">branche</w>,</l>
					<l n="4" num="2.3"><w n="4.1">Ont</w> <w n="4.2">un</w> <w n="4.3">rayonnement</w> <w n="4.4">dont</w> <w n="4.5">la</w> <w n="4.6">lueur</w> <w n="4.7">est</w> <w n="4.8">blanche</w>.</l>
					<l n="5" num="2.4"><w n="5.1">Je</w> <w n="5.2">la</w> <w n="5.3">voyais</w> <w n="5.4">dormir</w> <w n="5.5">tranquille</w> <w n="5.6">à</w> <w n="5.7">mon</w> <w n="5.8">côté</w> :</l>
					<l n="6" num="2.5"><w n="6.1">Sa</w> <w n="6.2">lèvre</w> <w n="6.3">avait</w> <w n="6.4">encore</w> <w n="6.5">un</w> <w n="6.6">reflet</w> <w n="6.7">de</w> <w n="6.8">gaieté</w> ;</l>
					<l n="7" num="2.6"><w n="7.1">Elle</w> <w n="7.2">devait</w> <w n="7.3">penser</w> <w n="7.4">à</w> <w n="7.5">ses</w> <w n="7.6">jeux</w> <w n="7.7">de</w> <w n="7.8">la</w> <w n="7.9">veille</w>.</l>
					<l n="8" num="2.7"><w n="8.1">Moi</w>, <w n="8.2">j</w>'<w n="8.3">entendais</w> <w n="8.4">au</w> <w n="8.5">loin</w> <w n="8.6">gronder</w> <w n="8.7">à</w> <w n="8.8">mon</w> <w n="8.9">oreille</w></l>
					<l n="9" num="2.8"><w n="9.1">Le</w> <w n="9.2">bruit</w> <w n="9.3">sourd</w> <w n="9.4">du</w> <w n="9.5">canon</w> <w n="9.6">au</w> <w n="9.7">Mont</w>-<w n="9.8">Valérien</w>…</l>
					<l n="10" num="2.9"><w n="10.1">Le</w> <w n="10.2">baby</w> <w n="10.3">qui</w> <w n="10.4">dormait</w> <w n="10.5">ne</w> <w n="10.6">se</w> <w n="10.7">doutait</w> <w n="10.8">de</w> <w n="10.9">rien</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">Oh</w> ! <w n="11.2">comme</w> <w n="11.3">elle</w> <w n="11.4">est</w> <w n="11.5">heureuse</w> ! <w n="11.6">Oh</w> ! <w n="11.7">que</w> <w n="11.8">je</w> <w n="11.9">voudrais</w> <w n="11.10">être</w>,</l>
					<l n="12" num="3.2"><w n="12.1">Endormi</w>, <w n="12.2">souriant</w> <w n="12.3">comme</w> <w n="12.4">ce</w> <w n="12.5">petit</w> <w n="12.6">être</w> !</l>
					<l n="13" num="3.3"><w n="13.1">Elle</w> <w n="13.2">dort</w>, <w n="13.3">ignorant</w> <w n="13.4">les</w> <w n="13.5">temps</w> <w n="13.6">où</w> <w n="13.7">nous</w> <w n="13.8">passons</w>,</l>
					<l n="14" num="3.4"><w n="14.1">Ne</w> <w n="14.2">sachant</w> <w n="14.3">rien</w> <w n="14.4">encor</w> <w n="14.5">des</w> <w n="14.6">pleurs</w> <w n="14.7">que</w> <w n="14.8">nous</w> <w n="14.9">versons</w> ;</l>
					<l n="15" num="3.5"><w n="15.1">Elle</w> <w n="15.2">dort</w>, <w n="15.3">inclinant</w> <w n="15.4">sa</w> <w n="15.5">tête</w>, <w n="15.6">qu</w>'<w n="15.7">un</w> <w n="15.8">beau</w> <w n="15.9">rêve</w></l>
					<l n="16" num="3.6"><w n="16.1">Qui</w> <w n="16.2">commencé</w> <w n="16.3">joyeux</w>, <w n="16.4">en</w> <w n="16.5">souriant</w> <w n="16.6">s</w>'<w n="16.7">achève</w>,</l>
					<l n="17" num="3.7"><w n="17.1">Caressé</w> <w n="17.2">doucement</w> <w n="17.3">d</w>'<w n="17.4">un</w> <w n="17.5">vol</w> <w n="17.6">mystérieux</w>,…</l>
				</lg>
				<lg n="4">
					<l n="18" num="4.1"><w n="18.1">Et</w> <w n="18.2">je</w> <w n="18.3">la</w> <w n="18.4">regardais</w> <w n="18.5">des</w> <w n="18.6">larmes</w> <w n="18.7">dans</w> <w n="18.8">les</w> <w n="18.9">yeux</w> !</l>
				</lg>
				<lg n="5">
					<l n="19" num="5.1"><w n="19.1">Dans</w> <w n="19.2">vingt</w> <w n="19.3">ans</w>, <w n="19.4">quand</w> <w n="19.5">la</w> <w n="19.6">France</w> <w n="19.7">aura</w> <w n="19.8">repris</w> <w n="19.9">sa</w> <w n="19.10">place</w>,</l>
					<l n="20" num="5.2"><w n="20.1">Quand</w> <w n="20.2">le</w> <w n="20.3">sang</w> <w n="20.4">de</w> <w n="20.5">la</w> <w n="20.6">honte</w> <w n="20.7">aura</w> <w n="20.8">lavé</w> <w n="20.9">la</w> <w n="20.10">trace</w>,</l>
					<l n="21" num="5.3"><w n="21.1">Lisant</w> <w n="21.2">dans</w> <w n="21.3">le</w> <w n="21.4">passé</w> <w n="21.5">l</w>'<w n="21.6">histoire</w> <w n="21.7">d</w>'<w n="21.8">à</w>-<w n="21.9">présent</w>,</l>
					<l n="22" num="5.4"><w n="22.1">D</w>'<w n="22.2">un</w> <w n="22.3">peuple</w> <w n="22.4">tout</w> <w n="22.5">entier</w> <w n="22.6">debout</w> <w n="22.7">et</w> <w n="22.8">frémissant</w>,</l>
					<l n="23" num="5.5"><w n="23.1">Oh</w> ! <w n="23.2">la</w> <w n="23.3">petite</w> <w n="23.4">fille</w>, <w n="23.5">alors</w>, <w n="23.6">qui</w> <w n="23.7">sera</w> <w n="23.8">femme</w>,</l>
					<l n="24" num="5.6"><w n="24.1">Aura</w>-<w n="24.2">t</w>-<w n="24.3">elle</w> <w n="24.4">gardé</w> <w n="24.5">dans</w> <w n="24.6">le</w> <w n="24.7">fond</w> <w n="24.8">de</w> <w n="24.9">son</w> <w n="24.10">âme</w></l>
					<l n="25" num="5.7"><w n="25.1">Une</w> <w n="25.2">place</w> <w n="25.3">aux</w> <w n="25.4">héros</w> <w n="25.5">dont</w> <w n="25.6">nul</w> <w n="25.7">ne</w> <w n="25.8">sait</w> <w n="25.9">le</w> <w n="25.10">nom</w>,</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">Morts</w>, <w n="26.2">quand</w> <w n="26.3">elle</w> <w n="26.4">dormait</w> <w n="26.5">à</w> <w n="26.6">l</w>'<w n="26.7">écho</w> <w n="26.8">du</w> <w n="26.9">canon</w> ?</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 28 octobre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>