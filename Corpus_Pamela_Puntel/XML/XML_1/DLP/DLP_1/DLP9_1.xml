<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP9">
				<head type="number">IX</head>
				<head type="main">LE SERMENT D'ANNIBAL</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ce</w> <w n="1.2">sont</w> <w n="1.3">dos</w> <w n="1.4">assassins</w> <w n="1.5">et</w> <w n="1.6">non</w> <w n="1.7">pas</w> <w n="1.8">des</w> <w n="1.9">soldats</w>.</l>
				</lg>
				<lg n="2">
					<l n="2" num="2.1"><w n="2.1">Voyez</w> <w n="2.2">ce</w> <w n="2.3">qu</w>'<w n="2.4">ils</w> <w n="2.5">ont</w> <w n="2.6">fait</w> : <w n="2.7">un</w> <w n="2.8">crime</w> <w n="2.9">à</w> <w n="2.10">chaque</w> <w n="2.11">pas</w>,</l>
					<l n="3" num="2.2"><w n="3.1">A</w> <w n="3.2">Saint</w>-<w n="3.3">Cloud</w>, <w n="3.4">à</w> <w n="3.5">Villiers</w>, <w n="3.6">à</w> <w n="3.7">Versaille</w>, <w n="3.8">à</w> <w n="3.9">Neuville</w>,</l>
					<l n="4" num="2.3"><w n="4.1">Dans</w> <w n="4.2">les</w> <w n="4.3">champs</w>, <w n="4.4">dans</w> <w n="4.5">les</w> <w n="4.6">bois</w>, <w n="4.7">dans</w> <w n="4.8">le</w> <w n="4.9">bourg</w>, <w n="4.10">dans</w> <w n="4.11">la</w> <w n="4.12">ville</w>,</l>
					<l n="5" num="2.4"><w n="5.1">Partout</w> <w n="5.2">l</w>'<w n="5.3">assassinat</w> <w n="5.4">infâme</w> <w n="5.5">du</w> <w n="5.6">bandit</w></l>
					<l n="6" num="2.5"><w n="6.1">Auquel</w> <w n="6.2">chaque</w> <w n="6.3">matin</w> <w n="6.4">leur</w> <w n="6.5">monarque</w> <w n="6.6">applaudit</w> !</l>
					<l n="7" num="2.6"><w n="7.1">Non</w> ! <w n="7.2">ce</w> <w n="7.3">n</w>'<w n="7.4">est</w> <w n="7.5">pas</w> <w n="7.6">assez</w> <w n="7.7">pour</w> <w n="7.8">nous</w>, <w n="7.9">ô</w> <w n="7.10">roi</w> <w n="7.11">Guillaume</w>,</l>
					<l n="8" num="2.7"><w n="8.1">Qu</w>'<w n="8.2">un</w> <w n="8.3">jour</w> <w n="8.4">l</w>'<w n="8.5">histoire</w> <w n="8.6">vienne</w> <w n="8.7">et</w> <w n="8.8">marque</w> <w n="8.9">ton</w> <w n="8.10">royaume</w></l>
					<l n="9" num="2.8"><w n="9.1">Du</w> <w n="9.2">stigmate</w> <w n="9.3">honteux</w> <w n="9.4">chauffé</w> <w n="9.5">pour</w> <w n="9.6">le</w> <w n="9.7">punir</w> ;</l>
					<l n="10" num="2.9"><w n="10.1">Non</w> ! <w n="10.2">ce</w> <w n="10.3">n</w>'<w n="10.4">est</w> <w n="10.5">pas</w> <w n="10.6">assez</w> <w n="10.7">pour</w> <w n="10.8">nous</w> <w n="10.9">de</w> <w n="10.10">l</w>'<w n="10.11">avenir</w> !</l>
					<l n="11" num="2.10"><w n="11.1">Quoi</w> ! <w n="11.2">nous</w> <w n="11.3">attendrions</w> <w n="11.4">cinquante</w> <w n="11.5">ou</w> <w n="11.6">cent</w> <w n="11.7">années</w>,</l>
					<l n="12" num="2.11"><w n="12.1">Les</w> <w n="12.2">générations</w> <w n="12.3">s</w>'<w n="12.4">en</w> <w n="12.5">iraient</w> <w n="12.6">entraînées</w></l>
					<l n="13" num="2.12"><w n="13.1">Vers</w> <w n="13.2">la</w> <w n="13.3">tombe</w> <w n="13.4">éternelle</w> <w n="13.5">où</w> <w n="13.6">dorment</w> <w n="13.7">leurs</w> <w n="13.8">aïeux</w>,</l>
					<l n="14" num="2.13"><w n="14.1">Et</w> <w n="14.2">le</w> <w n="14.3">Temps</w> <w n="14.4">poursuivrait</w> <w n="14.5">son</w> <w n="14.6">vol</w> <w n="14.7">silencieux</w>,</l>
					<l n="15" num="2.14"><w n="15.1">Sur</w> <w n="15.2">les</w> <w n="15.3">jours</w> <w n="15.4">écoulés</w> <w n="15.5">jetant</w> <w n="15.6">son</w> <w n="15.7">aile</w> <w n="15.8">immense</w>,</l>
					<l n="16" num="2.15"><w n="16.1">Sans</w> <w n="16.2">qu</w>'<w n="16.3">ait</w> <w n="16.4">sonné</w> <w n="16.5">pour</w> <w n="16.6">nous</w> <w n="16.7">l</w>'<w n="16.8">heure</w> <w n="16.9">de</w> <w n="16.10">la</w> <w n="16.11">vengeance</w> !</l>
					<l n="17" num="2.16"><w n="17.1">Des</w> <w n="17.2">mots</w> <w n="17.3">que</w> <w n="17.4">tout</w> <w n="17.5">cela</w> ! <w n="17.6">Nous</w>, <w n="17.7">nous</w> <w n="17.8">voulons</w> <w n="17.9">des</w> <w n="17.10">faits</w>,</l>
					<l n="18" num="2.17"><w n="18.1">Car</w> <w n="18.2">il</w> <w n="18.3">nous</w> <w n="18.4">faut</w> <w n="18.5">bien</w> <w n="18.6">plus</w> <w n="18.7">pour</w> <w n="18.8">être</w> <w n="18.9">satisfaits</w> !</l>
					<l n="19" num="2.18"><w n="19.1">Il</w> <w n="19.2">ne</w> <w n="19.3">nous</w> <w n="19.4">suffit</w> <w n="19.5">pas</w> <w n="19.6">de</w> <w n="19.7">compter</w> <w n="19.8">sur</w> <w n="19.9">l</w>'<w n="19.10">histoire</w> :</l>
					<l n="20" num="2.19"><w n="20.1">Une</w> <w n="20.2">telle</w> <w n="20.3">vengeance</w> <w n="20.4">est</w> <w n="20.5">trop</w> <w n="20.6">déclamatoire</w>,</l>
					<l n="21" num="2.20"><w n="21.1">Et</w> <w n="21.2">le</w> <w n="21.3">procès</w>-<w n="21.4">verbal</w> <w n="21.5">d</w>'<w n="21.6">un</w> <w n="21.7">froid</w> <w n="21.8">historien</w></l>
					<l n="22" num="2.21"><w n="22.1">Pour</w> <w n="22.2">l</w>'<w n="22.3">oubli</w> <w n="22.4">du</w> <w n="22.5">passé</w> <w n="22.6">ne</w> <w n="22.7">servirait</w> <w n="22.8">à</w> <w n="22.9">rien</w> !</l>
				</lg>
				<lg n="3">
					<l n="23" num="3.1"><w n="23.1">Sais</w>-<w n="23.2">tu</w> <w n="23.3">ce</w> <w n="23.4">qu</w>'<w n="23.5">il</w> <w n="23.6">nous</w> <w n="23.7">faut</w> <w n="23.8">à</w> <w n="23.9">nous</w>, <w n="23.10">ô</w> <w n="23.11">roi</w> <w n="23.12">Guillaume</w> ?</l>
					<l n="24" num="3.2"><w n="24.1">C</w>'<w n="24.2">est</w> <w n="24.3">le</w> <w n="24.4">drapeau</w> <w n="24.5">français</w> <w n="24.6">flottant</w> <w n="24.7">sur</w> <w n="24.8">ton</w> <w n="24.9">royaume</w>,</l>
					<l n="25" num="3.3"><w n="25.1">Et</w> <w n="25.2">pour</w> <w n="25.3">vaincre</w>, <w n="25.4">il</w> <w n="25.5">nous</w> <w n="25.6">faut</w> <w n="25.7">quelques</w> <w n="25.8">jours</w> <w n="25.9">seulement</w>,</l>
					<l n="26" num="3.4"><w n="26.1">Car</w> <w n="26.2">la</w> <w n="26.3">haine</w> <w n="26.4">d</w>'<w n="26.5">un</w> <w n="26.6">peuple</w> <w n="26.7">est</w> <w n="26.8">forte</w> <w n="26.9">immensément</w> !</l>
					<l n="27" num="3.5"><w n="27.1">Chaque</w> <w n="27.2">homme</w> <w n="27.3">fera</w> <w n="27.4">lire</w> <w n="27.5">à</w> <w n="27.6">son</w> <w n="27.7">fils</w> <w n="27.8">notre</w> <w n="27.9">histoire</w>,</l>
					<l n="28" num="3.6"><w n="28.1">Et</w> <w n="28.2">lui</w> <w n="28.3">dira</w> : <w n="28.4">Choisis</w> ! <w n="28.5">l</w>'<w n="28.6">infamie</w> <w n="28.7">ou</w> <w n="28.8">la</w> <w n="28.9">gloire</w> !</l>
					<l n="29" num="3.7"><w n="29.1">La</w> <w n="29.2">femme</w> <w n="29.3">n</w>'<w n="29.4">aimera</w> <w n="29.5">qu</w>'<w n="29.6">un</w> <w n="29.7">époux</w> <w n="29.8">libre</w> <w n="29.9">et</w> <w n="29.10">fier</w>,</l>
					<l n="30" num="3.8"><w n="30.1">Et</w> <w n="30.2">les</w> <w n="30.3">enfants</w> <w n="30.4">conçus</w> <w n="30.5">dans</w> <w n="30.6">ces</w> <w n="30.7">unions</w> <w n="30.8">d</w>'<w n="30.9">hier</w>,</l>
					<l n="31" num="3.9"><w n="31.1">Naîtront</w> <w n="31.2">le</w> <w n="31.3">sang</w> <w n="31.4">au</w> <w n="31.5">cœur</w> <w n="31.6">et</w> <w n="31.7">la</w> <w n="31.8">haine</w> <w n="31.9">dans</w> <w n="31.10">l</w>'<w n="31.11">âme</w></l>
					<l n="32" num="3.10"><w n="32.1">Pour</w> <w n="32.2">ton</w> <w n="32.3">règne</w> <w n="32.4">maudit</w> <w n="32.5">et</w> <w n="32.6">pour</w> <w n="32.7">ton</w> <w n="32.8">peuple</w> <w n="32.9">infâme</w> !</l>
				</lg>
				<lg n="4">
					<l n="33" num="4.1"><w n="33.1">Plus</w> <w n="33.2">de</w> <w n="33.3">futilités</w> ! <w n="33.4">plus</w> <w n="33.5">de</w> <w n="33.6">plaisirs</w> <w n="33.7">mesquins</w> !</l>
					<l n="34" num="4.2"><w n="34.1">Ces</w> <w n="34.2">choses</w> <w n="34.3">ne</w> <w n="34.4">vont</w> <w n="34.5">pas</w> <w n="34.6">aux</w> <w n="34.7">cœurs</w> <w n="34.8">républicains</w> !</l>
					<l n="35" num="4.3"><w n="35.1">Le</w> <w n="35.2">fer</w> <w n="35.3">ne</w> <w n="35.4">servira</w> <w n="35.5">qu</w>'<w n="35.6">à</w> <w n="35.7">forger</w> <w n="35.8">des</w> <w n="35.9">épées</w></l>
					<l n="36" num="4.4"><w n="36.1">Que</w> <w n="36.2">les</w> <w n="36.3">larmes</w> <w n="36.4">d</w>'<w n="36.5">un</w> <w n="36.6">peuple</w> <w n="36.7">auront</w> <w n="36.8">bientôt</w> <w n="36.9">trempées</w> ;</l>
					<l n="37" num="4.5"><w n="37.1">Le</w> <w n="37.2">bronze</w>, <w n="37.3">qui</w> <w n="37.4">couvrait</w> <w n="37.5">les</w> <w n="37.6">murs</w> <w n="37.7">que</w> <w n="37.8">nous</w> <w n="37.9">ornons</w>,</l>
					<l n="38" num="4.6"><w n="38.1">Le</w> <w n="38.2">bronze</w> <w n="38.3">enfantera</w> <w n="38.4">des</w> <w n="38.5">sujets</w> <w n="38.6">de</w> <w n="38.7">canons</w> !</l>
					<l n="39" num="4.7"><w n="39.1">Et</w> <w n="39.2">fallût</w>-<w n="39.3">il</w> <w n="39.4">briser</w> <w n="39.5">la</w> <w n="39.6">colonne</w> <w n="39.7">Vendôme</w>,</l>
					<l n="40" num="4.8"><w n="40.1">Pour</w> <w n="40.2">toi</w>, <w n="40.3">nous</w> <w n="40.4">en</w> <w n="40.5">aurons</w> <w n="40.6">assez</w>, <w n="40.7">ô</w> <w n="40.8">roi</w> <w n="40.9">Guillaume</w> !</l>
					<l n="41" num="4.9"><w n="41.1">Nous</w> <w n="41.2">voulons</w> <w n="41.3">étouffer</w> <w n="41.4">l</w>'<w n="41.5">écho</w> <w n="41.6">de</w> <w n="41.7">Wissembourg</w></l>
					<l n="42" num="4.10"><w n="42.1">Par</w> <w n="42.2">le</w> <w n="42.3">bruit</w> <w n="42.4">du</w> <w n="42.5">fusil</w> <w n="42.6">et</w> <w n="42.7">le</w> <w n="42.8">son</w> <w n="42.9">du</w> <w n="42.10">tambour</w> ;</l>
					<l n="43" num="4.11"><w n="43.1">Nous</w> <w n="43.2">voulons</w> <w n="43.3">effacer</w> <w n="43.4">la</w> <w n="43.5">trace</w> <w n="43.6">du</w> <w n="43.7">passage</w></l>
					<l n="44" num="4.12"><w n="44.1">Imprimé</w> <w n="44.2">dans</w> <w n="44.3">nos</w> <w n="44.4">champs</w> <w n="44.5">par</w> <w n="44.6">ta</w> <w n="44.7">horde</w> <w n="44.8">sauvage</w>,</l>
					<l n="45" num="4.13"><w n="45.1">Et</w> <w n="45.2">pour</w> <w n="45.3">n</w>'<w n="45.4">y</w> <w n="45.5">rien</w> <w n="45.6">laisser</w>, <w n="45.7">nous</w> <w n="45.8">joindrons</w> <w n="45.9">sur</w> <w n="45.10">nos</w> <w n="45.11">pas</w></l>
					<l n="46" num="4.14"><w n="46.1">Les</w> <w n="46.2">pleurs</w> <w n="46.3">de</w> <w n="46.4">leur</w> <w n="46.5">famille</w> <w n="46.6">au</w> <w n="46.7">sang</w> <w n="46.8">de</w> <w n="46.9">tes</w> <w n="46.10">soldats</w> !</l>
				</lg>
				<lg n="5">
					<l n="47" num="5.1"><w n="47.1">Mais</w> <w n="47.2">tu</w> <w n="47.3">verras</w> <w n="47.4">alors</w> <w n="47.5">quelle</w> <w n="47.6">est</w> <w n="47.7">la</w> <w n="47.8">différence</w></l>
					<l n="48" num="5.2"><w n="48.1">Du</w> <w n="48.2">bandit</w> <w n="48.3">de</w> <w n="48.4">la</w> <w n="48.5">Prusse</w> <w n="48.6">au</w> <w n="48.7">soldat</w> <w n="48.8">de</w> <w n="48.9">la</w> <w n="48.10">France</w> !</l>
					<l n="49" num="5.3"><w n="49.1">Nous</w> <w n="49.2">n</w>'<w n="49.3">irons</w> <w n="49.4">pas</w> <w n="49.5">brûler</w> <w n="49.6">tes</w> <w n="49.7">champs</w> <w n="49.8">et</w> <w n="49.9">tes</w> <w n="49.10">maisons</w>,</l>
					<l n="50" num="5.4"><w n="50.1">Ni</w> <w n="50.2">prendre</w> <w n="50.3">au</w> <w n="50.4">laboureur</w> <w n="50.5">le</w> <w n="50.6">pain</w> <w n="50.7">de</w> <w n="50.8">ses</w> <w n="50.9">moissons</w> ;</l>
					<l n="51" num="5.5"><w n="51.1">Nos</w> <w n="51.2">aïeux</w> <w n="51.3">chevaliers</w> <w n="51.4">nous</w> <w n="51.5">ont</w> <w n="51.6">légué</w> <w n="51.7">leurs</w> <w n="51.8">âmes</w> :</l>
					<l n="52" num="5.6"><w n="52.1">Chez</w> <w n="52.2">nous</w>, <w n="52.3">on</w> <w n="52.4">tient</w> <w n="52.5">sacrés</w> <w n="52.6">les</w> <w n="52.7">enfants</w> <w n="52.8">et</w> <w n="52.9">les</w> <w n="52.10">femmes</w> !</l>
					<l n="53" num="5.7"><w n="53.1">Chez</w> <w n="53.2">nous</w> <w n="53.3">qui</w>, <w n="53.4">chevaliers</w>, <w n="53.5">avons</w> <w n="53.6">toujours</w> <w n="53.7">vécu</w>,</l>
					<l n="54" num="5.8"><w n="54.1">On</w> <w n="54.2">n</w>'<w n="54.3">assassine</w> <w n="54.4">pas</w> <w n="54.5">après</w> <w n="54.6">qu</w>'<w n="54.7">on</w> <w n="54.8">a</w> <w n="54.9">vaincu</w> !</l>
				</lg>
				<lg n="6">
					<l n="55" num="6.1"><w n="55.1">Ceux</w> <w n="55.2">que</w> <w n="55.3">le</w> <w n="55.4">Panthéon</w> <w n="55.5">voit</w> <w n="55.6">couchés</w> <w n="55.7">sous</w> <w n="55.8">son</w> <w n="55.9">dôme</w>,</l>
					<l n="56" num="6.2"><w n="56.1">Ceux</w>-<w n="56.2">là</w> <w n="56.3">nous</w> <w n="56.4">montreront</w> <w n="56.5">la</w> <w n="56.6">route</w>, <w n="56.7">ô</w> <w n="56.8">roi</w> <w n="56.9">Guillaume</w> !</l>
					<l n="57" num="6.3"><w n="57.1">Et</w> <w n="57.2">quand</w> <w n="57.3">par</w> <w n="57.4">les</w> <w n="57.5">leçons</w> <w n="57.6">venant</w> <w n="57.7">de</w> <w n="57.8">ces</w> <w n="57.9">tombeaux</w>,</l>
					<l n="58" num="6.4"><w n="58.1">Nous</w> <w n="58.2">serons</w> <w n="58.3">assez</w> <w n="58.4">forts</w> <w n="58.5">pour</w> <w n="58.6">lever</w> <w n="58.7">nos</w> <w n="58.8">drapeaux</w>,</l>
					<l n="59" num="6.5"><w n="59.1">Du</w> <w n="59.2">club</w> <w n="59.3">à</w> <w n="59.4">l</w>'<w n="59.5">atelier</w>, <w n="59.6">du</w> <w n="59.7">manoir</w> <w n="59.8">à</w>' <w n="59.9">la</w> <w n="59.10">grange</w>,</l>
					<l n="60" num="6.6"><w n="60.1">Tu</w> <w n="60.2">verras</w> <w n="60.3">ce</w> <w n="60.4">que</w> <w n="60.5">c</w>'<w n="60.6">est</w> <w n="60.7">qu</w>'<w n="60.8">un</w> <w n="60.9">peuple</w> <w n="60.10">qui</w> <w n="60.11">se</w> <w n="60.12">venge</w> !</l>
				</lg>
				<lg n="7">
					<l n="61" num="7.1"><w n="61.1">Mais</w> <w n="61.2">alors</w>, <w n="61.3">triomphants</w>, <w n="61.4">nous</w> <w n="61.5">étendrons</w> <w n="61.6">la</w> <w n="61.7">main</w>,</l>
					<l n="62" num="7.2"><w n="62.1">Et</w> <w n="62.2">nous</w> <w n="62.3">dirons</w> <w n="62.4">au</w> <w n="62.5">monde</w> : <w n="62.6">Assez</w> <w n="62.7">de</w> <w n="62.8">sang</w> <w n="62.9">humain</w></l>
					<l n="63" num="7.3"><w n="63.1">Et</w> <w n="63.2">les</w> <w n="63.3">rois</w> <w n="63.4">n</w>'<w n="63.5">auront</w> <w n="63.6">plus</w> <w n="63.7">de</w> <w n="63.8">vastes</w> <w n="63.9">hécatombes</w></l>
					<l n="64" num="7.4"><w n="64.1">Pour</w> <w n="64.2">jeter</w> <w n="64.3">un</w> <w n="64.4">reflet</w> <w n="64.5">de</w> <w n="64.6">gloire</w> <w n="64.7">sur</w> <w n="64.8">leurs</w> <w n="64.9">tombes</w> !</l>
					<l n="65" num="7.5"><w n="65.1">L</w>'<w n="65.2">homme</w> <w n="65.3">connaîtra</w> <w n="65.4">l</w>'<w n="65.5">homme</w> <w n="65.6">au</w> <w n="65.7">lieu</w> <w n="65.8">de</w> <w n="65.9">le</w> <w n="65.10">briser</w>,</l>
					<l n="66" num="7.6"><w n="66.1">Et</w> <w n="66.2">dans</w> <w n="66.3">un</w> <w n="66.4">gigantesque</w> <w n="66.5">et</w> <w n="66.6">superbe</w> <w n="66.7">baiser</w>,</l>
					<l n="67" num="7.7"><w n="67.1">Sur</w> <w n="67.2">le</w> <w n="67.3">lit</w> <w n="67.4">nuptial</w> <w n="67.5">du</w> <w n="67.6">passé</w> <w n="67.7">qui</w> <w n="67.8">chancelle</w>,</l>
					<l n="68" num="7.8"><w n="68.1">Le</w> <w n="68.2">monde</w> <w n="68.3">enfantera</w> <w n="68.4">la</w> <w n="68.5">paix</w> <w n="68.6">universelle</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 17 octobre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>