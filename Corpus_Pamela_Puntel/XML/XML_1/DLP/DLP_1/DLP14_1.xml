<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP14">
				<head type="number">XIV</head>
				<head type="main">HISTOIRE QUOTIDIENNE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">Prussiens</w> <w n="1.3">maudits</w> <w n="1.4">ont</w> <w n="1.5">pillé</w> <w n="1.6">cette</w> <w n="1.7">ferme</w>,</l>
					<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">brûlant</w>, <w n="2.3">et</w> <w n="2.4">prenant</w> <w n="2.5">tout</w> <w n="2.6">ce</w> <w n="2.7">qu</w>'<w n="2.8">elle</w> <w n="2.9">renferme</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Volant</w> <w n="3.2">les</w> <w n="3.3">bœufs</w>, <w n="3.4">laissant</w> <w n="3.5">comme</w> <w n="3.6">un</w> <w n="3.7">spectre</w> <w n="3.8">debout</w></l>
					<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">misère</w> <w n="4.3">toujours</w>, <w n="4.4">et</w> <w n="4.5">la</w> <w n="4.6">honte</w> <w n="4.7">partout</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Ensuite</w>, <w n="5.2">pour</w> <w n="5.3">finir</w> <w n="5.4">ainsi</w> <w n="5.5">qu</w>'<w n="5.6">à</w> <w n="5.7">l</w>'<w n="5.8">ordinaire</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Ils</w> <w n="6.2">ont</w> <w n="6.3">tué</w> <w n="6.4">l</w>'<w n="6.5">enfant</w> <w n="6.6">et</w> <w n="6.7">violé</w> <w n="6.8">la</w> <w n="6.9">mère</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">puis</w> <w n="7.3">ils</w> <w n="7.4">sont</w> <w n="7.5">partis</w>, <w n="7.6">en</w>' <w n="7.7">laissant</w> <w n="7.8">derrière</w> <w n="7.9">eux</w></l>
					<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">mort</w> <w n="8.3">dans</w> <w n="8.4">ce</w> <w n="8.5">vallon</w> <w n="8.6">si</w> <w n="8.7">doux</w> <w n="8.8">et</w> <w n="8.9">si</w> <w n="8.10">joyeux</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Cependant</w>, <w n="9.2">le</w> <w n="9.3">fermier</w> <w n="9.4">s</w>'<w n="9.5">en</w> <w n="9.6">revient</w> <w n="9.7">de</w> <w n="9.8">la</w> <w n="9.9">ville</w>.</l>
					<l n="10" num="2.2"><w n="10.1">Dès</w> <w n="10.2">l</w>'<w n="10.3">aube</w>, <w n="10.4">appelé</w> <w n="10.5">là</w> <w n="10.6">pour</w> <w n="10.7">une</w> <w n="10.8">affaire</w> <w n="10.9">utile</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Il</w> <w n="11.2">prit</w> <w n="11.3">entre</w> <w n="11.4">ses</w> <w n="11.5">bras</w> <w n="11.6">la</w> <w n="11.7">mère</w> <w n="11.8">et</w> <w n="11.9">le</w> <w n="11.10">petit</w>,</l>
					<l n="12" num="2.4"><w n="12.1">Les</w> <w n="12.2">embrassa</w> <w n="12.3">tous</w> <w n="12.4">deux</w> <w n="12.5">sur</w> <w n="12.6">le</w> <w n="12.7">front</w>, <w n="12.8">et</w> <w n="12.9">partit</w>.</l>
					<l n="13" num="2.5"><w n="13.1">C</w>'<w n="13.2">est</w> <w n="13.3">un</w> <w n="13.4">brave</w> <w n="13.5">homme</w> : <w n="13.6">il</w> <w n="13.7">n</w>'<w n="13.8">a</w> <w n="13.9">que</w> <w n="13.10">doux</w> <w n="13.11">amours</w> <w n="13.12">dans</w> <w n="13.13">l</w>'<w n="13.14">âme</w>,</l>
					<l n="14" num="2.6"><w n="14.1">Deux</w> <w n="14.2">amours</w> <w n="14.3">saints</w> <w n="14.4">et</w> <w n="14.5">forts</w> : <w n="14.6">son</w> <w n="14.7">enfant</w> <w n="14.8">et</w> <w n="14.9">sa</w> <w n="14.10">femme</w>.</l>
					<l n="15" num="2.7"><w n="15.1">Aussi</w>, <w n="15.2">pour</w> <w n="15.3">arriver</w> <w n="15.4">plus</w> <w n="15.5">tôt</w> <w n="15.6">à</w> <w n="15.7">la</w> <w n="15.8">maison</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Il</w> <w n="16.2">va</w> <w n="16.3">vite</w>, <w n="16.4">malgré</w> <w n="16.5">le</w> <w n="16.6">chaud</w> <w n="16.7">de</w> <w n="16.8">la</w> <w n="16.9">saison</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">— <w n="17.1">Bonne</w> <w n="17.2">Jeanne</w> ! <w n="17.3">dit</w>-<w n="17.4">il</w>, <w n="17.5">va</w>-<w n="17.6">t</w>-<w n="17.7">elle</w> <w n="17.8">être</w> <w n="17.9">contente</w></l>
					<l n="18" num="3.2"><w n="18.1">De</w> <w n="18.2">me</w> <w n="18.3">voir</w> <w n="18.4">revenir</w> <w n="18.5">si</w> <w n="18.6">tôt</w> <w n="18.7">avant</w> <w n="18.8">l</w>'<w n="18.9">attente</w> !</l>
					<l n="19" num="3.3"><w n="19.1">Et</w>-<w n="19.2">le</w> <w n="19.3">bébé</w> ! <w n="19.4">Je</w> <w n="19.5">vois</w> <w n="19.6">son</w> <w n="19.7">gai</w> <w n="19.8">bonheur</w> <w n="19.9">d</w>'<w n="19.10">enfant</w> :</l>
					<l n="20" num="3.4"><w n="20.1">Comme</w> <w n="20.2">il</w> <w n="20.3">va</w> <w n="20.4">m</w>'<w n="20.5">embrasser</w>, <w n="20.6">le</w> <w n="20.7">diable</w> ! <w n="20.8">en</w> <w n="20.9">m</w>'<w n="20.10">étouffant</w>,</l>
					<l n="21" num="3.5"><w n="21.1">Le</w> <w n="21.2">teint</w> <w n="21.3">chaud</w>, <w n="21.4">et</w> <w n="21.5">les</w> <w n="21.6">yeux</w> <w n="21.7">brûlants</w> <w n="21.8">de</w> <w n="21.9">convoitise</w>,</l>
					<l n="22" num="3.6"><w n="22.1">Afin</w> <w n="22.2">de</w> <w n="22.3">s</w>'<w n="22.4">emparer</w> <w n="22.5">plus</w> <w n="22.6">tôt</w> <w n="22.7">de</w> <w n="22.8">la</w> <w n="22.9">surprise</w> :</l>
					<l n="23" num="3.7"><w n="23.1">C</w>'<w n="23.2">est</w> <w n="23.3">plus</w> <w n="23.4">beau</w> <w n="23.5">que</w> <w n="23.6">jamais</w> <w n="23.7">il</w> <w n="23.8">ne</w> <w n="23.9">l</w>'<w n="23.10">aurait</w> <w n="23.11">rêvé</w>…</l>
					<l n="24" num="3.8"><w n="24.1">Encore</w> <w n="24.2">trois</w> <w n="24.3">quarts</w> <w n="24.4">d</w>'<w n="24.5">heure</w> <w n="24.6">et</w> <w n="24.7">je</w> <w n="24.8">suis</w> <w n="24.9">arrivé</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">La</w> <w n="25.2">surprise</w>, <w n="25.3">c</w>'<w n="25.4">était</w> <w n="25.5">un</w> <w n="25.6">grand</w> <w n="25.7">polichinelle</w></l>
					<l n="26" num="4.2"><w n="26.1">Que</w> <w n="26.2">l</w>'<w n="26.3">on</w> <w n="26.4">faisait</w> <w n="26.5">sauter</w> <w n="26.6">en</w> <w n="26.7">tirant</w> <w n="26.8">la</w> <w n="26.9">ficelle</w>.</l>
					<l n="27" num="4.3"><w n="27.1">Il</w> <w n="27.2">arrive</w> <w n="27.3">au</w> <w n="27.4">chemin</w> <w n="27.5">qui</w> <w n="27.6">mène</w> <w n="27.7">à</w> <w n="27.8">la</w> <w n="27.9">maison</w> :</l>
					<l n="28" num="4.4">— <w n="28.1">C</w>'<w n="28.2">est</w> <w n="28.3">étrange</w>, <w n="28.4">on</w> <w n="28.5">dirait</w> <w n="28.6">que</w> <w n="28.7">je</w> <w n="28.8">perds</w> <w n="28.9">la</w> <w n="28.10">raison</w>,</l>
					<l n="29" num="4.5"><w n="29.1">Se</w> <w n="29.2">dit</w>-<w n="29.3">il</w> ; <w n="29.4">mais</w> <w n="29.5">vraiment</w> <w n="29.6">je</w> <w n="29.7">sens</w> <w n="29.8">mon</w> <w n="29.9">cœur</w> <w n="29.10">qui</w> <w n="29.11">tremble</w>.</l>
					<l n="30" num="4.6"><w n="30.1">Je</w> <w n="30.2">suis</w> <w n="30.3">fou</w> ! <w n="30.4">Je</w> <w n="30.5">n</w>'<w n="30.6">ai</w> <w n="30.7">rien</w> <w n="30.8">à</w> <w n="30.9">craindre</w>, <w n="30.10">ce</w> <w n="30.11">me</w> <w n="30.12">semble</w> ;</l>
					<l n="31" num="4.7"><w n="31.1">A</w> <w n="31.2">la</w> <w n="31.3">ville</w>, <w n="31.4">on</w> <w n="31.5">disait</w> <w n="31.6">qu</w>'<w n="31.7">ils</w> <w n="31.8">étaient</w> <w n="31.9">loin</w> : <w n="31.10">ainsi</w>,</l>
					<l n="32" num="4.8"><w n="32.1">On</w> <w n="32.2">ne</w> <w n="32.3">doit</w> <w n="32.4">pas</w> <w n="32.5">s</w>'<w n="32.6">attendre</w> <w n="32.7">à</w> <w n="32.8">les</w> <w n="32.9">voir</w> <w n="32.10">par</w>-<w n="32.11">ici</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">Il</w> <w n="33.2">arrive</w>. <w n="33.3">Grand</w> <w n="33.4">Dieu</w> ! <w n="33.5">plus</w> <w n="33.6">rien</w> <w n="33.7">que</w> <w n="33.8">la</w> <w n="33.9">ruine</w> !</l>
					<l n="34" num="5.2"><w n="34.1">La</w> <w n="34.2">ferme</w> <w n="34.3">incendiée</w> <w n="34.4">et</w> <w n="34.5">pillée</w> ! <w n="34.6">Il</w> <w n="34.7">devine</w> !</l>
					<l n="35" num="5.3"><w n="35.1">Il</w> <w n="35.2">devine</w> <w n="35.3">que</w> <w n="35.4">là</w> <w n="35.5">les</w> <w n="35.6">Maudits</w> <w n="35.7">ont</w> <w n="35.8">passé</w>,</l>
					<l n="36" num="5.4"><w n="36.1">Qu</w>'<w n="36.2">ils</w> <w n="36.3">ont</w> <w n="36.4">semé</w> <w n="36.5">la</w> <w n="36.6">mort</w>, <w n="36.7">et</w> <w n="36.8">qu</w>'<w n="36.9">ils</w> <w n="36.10">n</w>'<w n="36.11">ont</w> <w n="36.12">rien</w> <w n="36.13">laissé</w>.</l>
					<l n="37" num="5.5"><w n="37.1">Presque</w> <w n="37.2">fou</w>, <w n="37.3">l</w>’<w n="37.4">œil</w> <w n="37.5">hagard</w>, <w n="37.6">il</w> <w n="37.7">court</w> <w n="37.8">dans</w> <w n="37.9">les</w> <w n="37.10">décombres</w> :</l>
					<l n="38" num="5.6"><w n="38.1">Jeanne</w> ! <w n="38.2">Paul</w> ! —<w n="38.3">Rien</w>. — <w n="38.4">Là</w> <w n="38.5">bas</w>, <w n="38.6">il</w> <w n="38.7">aperçoit</w> <w n="38.8">doux</w> <w n="38.9">ombres</w>…</l>
					<l n="39" num="5.7"><w n="39.1">Ce</w> <w n="39.2">sont</w> <w n="39.3">eux</w>… <w n="39.4">Non</w> ! <w n="39.5">Il</w> <w n="39.6">court</w>, <w n="39.7">appelant</w> <w n="39.8">son</w> <w n="39.9">enfant</w>,</l>
					<l n="40" num="5.8"><w n="40.1">Sa</w> <w n="40.2">femme</w>… — <w n="40.3">Rien</w> <w n="40.4">encor</w> ! <w n="40.5">rien</w> <w n="40.6">qu</w>'<w n="40.7">un</w> <w n="40.8">air</w> <w n="40.9">étouffant</w></l>
					<l n="41" num="5.9"><w n="41.1">Qui</w> <w n="41.2">monte</w> <w n="41.3">en</w> <w n="41.4">s</w>'<w n="41.5">échappant</w> <w n="41.6">de</w> <w n="41.7">ce</w> <w n="41.8">carnage</w> <w n="41.9">immense</w> :</l>
					<l n="42" num="5.10"><w n="42.1">Rien</w> <w n="42.2">que</w> <w n="42.3">le</w> <w n="42.4">désespoir</w>, <w n="42.5">et</w> <w n="42.6">rien</w> <w n="42.7">que</w> <w n="42.8">le</w> <w n="42.9">silence</w> !</l>
					<l n="43" num="5.11"><w n="43.1">Où</w> <w n="43.2">sont</w>-<w n="43.3">ils</w> ? <w n="43.4">Juste</w> <w n="43.5">ciel</w> ! <w n="43.6">Comprenez</w>-<w n="43.7">vous</w> <w n="43.8">cela</w> ?</l>
					<l n="44" num="5.12"><w n="44.1">Chercher</w> <w n="44.2">ses</w> <w n="44.3">deux</w> <w n="44.4">amours</w> <w n="44.5">qu</w>'<w n="44.6">on</w> <w n="44.7">avait</w> <w n="44.8">laissés</w> <w n="44.9">là</w>,</l>
					<l n="45" num="5.13"><w n="45.1">Et</w> <w n="45.2">ne</w> <w n="45.3">plus</w> <w n="45.4">rien</w> <w n="45.5">trouver</w> ! <w n="45.6">Où</w> <w n="45.7">sont</w>-<w n="45.8">ils</w> ? <w n="45.9">Il</w> <w n="45.10">appelle</w></l>
					<l n="46" num="5.14"><w n="46.1">Rien</w> <w n="46.2">encor</w> <w n="46.3">ne</w> <w n="46.4">répond</w> <w n="46.5">à</w> <w n="46.6">sa</w> <w n="46.7">voix</w> ! <w n="46.8">Il</w> <w n="46.9">chancelle</w></l>
					<l n="47" num="5.15"><w n="47.1">Où</w> <w n="47.2">sont</w>-<w n="47.3">ils</w> ? <w n="47.4">Dans</w> <w n="47.5">la</w> <w n="47.6">cour</w> ? <w n="47.7">Vide</w> ! <w n="47.8">Au</w> <w n="47.9">bois</w>, <w n="47.10">près</w> <w n="47.11">d</w>'<w n="47.12">ici</w> ?</l>
					<l n="48" num="5.16"><w n="48.1">Vide</w> ! <w n="48.2">Dans</w> <w n="48.3">le</w> <w n="48.4">jardin</w> <w n="48.5">alors</w> ? <w n="48.6">Non</w>, <w n="48.7">vide</w> <w n="48.8">aussi</w> !</l>
					<l n="49" num="5.17">— <w n="49.1">Voyons</w> ! <w n="49.2">voyons</w> ! <w n="49.3">dit</w>-<w n="49.4">il</w>, <w n="49.5">ils</w> <w n="49.6">sont</w> <w n="49.7">chez</w> <w n="49.8">des</w> <w n="49.9">voisines</w></l>
					<l n="50" num="5.18"><w n="50.1">Ils</w> <w n="50.2">n</w>'<w n="50.3">auront</w> <w n="50.4">pas</w> <w n="50.5">voulu</w> <w n="50.6">rester</w> <w n="50.7">dans</w> <w n="50.8">ces</w> <w n="50.9">ruines</w> ;</l>
					<l n="51" num="5.19"><w n="51.1">Seuls</w>, <w n="51.2">ils</w> <w n="51.3">auront</w> <w n="51.4">eu</w> <w n="51.5">peur</w> : <w n="51.6">ce</w> <w n="51.7">n</w>'<w n="51.8">est</w> <w n="51.9">pas</w> <w n="51.10">étonnant</w>…</l>
					<l n="52" num="5.20"><w n="52.1">Eh</w> <w n="52.2">bien</w> ! <w n="52.3">voilà</w>-<w n="52.4">t</w>-<w n="52.5">il</w> <w n="52.6">pas</w> <w n="52.7">que</w> <w n="52.8">je</w> <w n="52.9">ris</w> <w n="52.10">maintenant</w> ?</l>
					<l n="53" num="5.21"><w n="53.1">C</w>'<w n="53.2">est</w> <w n="53.3">que</w> <w n="53.4">l</w>'<w n="53.5">émotion</w> <w n="53.6">était</w> <w n="53.7">bien</w> <w n="53.8">naturelle</w> !</l>
					<l n="54" num="5.22">— <w n="54.1">Que</w> <w n="54.2">diable</w> <w n="54.3">ai</w>-<w n="54.4">je</w> <w n="54.5">donc</w> <w n="54.6">là</w> ? <w n="54.7">C</w>'<w n="54.8">est</w> <w n="54.9">le</w> <w n="54.10">polichinelle</w> !</l>
					<l n="55" num="5.23"><w n="55.1">Pauvre</w> <w n="55.2">petit</w> ! <w n="55.3">va</w>-<w n="55.4">t</w>-<w n="55.5">il</w> <w n="55.6">être</w> <w n="55.7">content</w> <w n="55.8">demain</w> !…</l>
				</lg>
				<lg n="6">
					<l n="56" num="6.1"><w n="56.1">Tout</w> <w n="56.2">à</w> <w n="56.3">coup</w> <w n="56.4">il</w> <w n="56.5">s</w>'<w n="56.6">arrête</w> <w n="56.7">au</w> <w n="56.8">milieu</w> <w n="56.9">du</w> <w n="56.10">chemin</w>,</l>
					<l n="57" num="6.2"><w n="57.1">Et</w> <w n="57.2">pousse</w> <w n="57.3">un</w> <w n="57.4">cri</w>, <w n="57.5">ce</w> <w n="57.6">cri</w> <w n="57.7">que</w> <w n="57.8">jette</w> <w n="57.9">dans</w> <w n="57.10">sa</w> <w n="57.11">haine</w></l>
					<l n="58" num="6.3"><w n="58.1">L</w>'<w n="58.2">homme</w> <w n="58.3">que</w> <w n="58.4">la</w> <w n="58.5">douleur</w> <w n="58.6">terrasse</w> <w n="58.7">comme</w> <w n="58.8">un</w> <w n="58.9">chêne</w>…</l>
					<l n="59" num="6.4"><w n="59.1">Devant</w> <w n="59.2">lui</w>, <w n="59.3">dans</w> <w n="59.4">le</w> <w n="59.5">sang</w> <w n="59.6">où</w> <w n="59.7">s</w>'<w n="59.8">impriment</w> <w n="59.9">ses</w> <w n="59.10">pas</w>,</l>
					<l n="60" num="6.5"><w n="60.1">La</w> <w n="60.2">mère</w> <w n="60.3">morte</w>, <w n="60.4">ayant</w> <w n="60.5">l</w>'<w n="60.6">enfant</w> <w n="60.7">mort</w> <w n="60.8">dans</w> <w n="60.9">ses</w> <w n="60.10">bras</w> !</l>
				</lg>
				<lg n="7">
					<l n="61" num="7.1"><w n="61.1">Il</w> <w n="61.2">tourna</w> <w n="61.3">sur</w> <w n="61.4">lui</w>-<w n="61.5">même</w>, <w n="61.6">et</w> <w n="61.7">roula</w> <w n="61.8">sur</w> <w n="61.9">la</w> <w n="61.10">pierre</w>.</l>
				</lg>
				<lg n="8">
					<l n="62" num="8.1"><w n="62.1">Quand</w> <w n="62.2">il</w> <w n="62.3">revint</w> <w n="62.4">à</w> <w n="62.5">lui</w>, <w n="62.6">dans</w> <w n="62.7">la</w> <w n="62.8">nuit</w>, <w n="62.9">sans</w> <w n="62.10">lumière</w>,</l>
					<l n="63" num="8.2"><w n="63.1">Il</w> <w n="63.2">prit</w> <w n="63.3">ses</w> <w n="63.4">deux</w> <w n="63.5">amours</w> <w n="63.6">qui</w> <w n="63.7">dormaient</w> <w n="63.8">toujours</w> <w n="63.9">là</w></l>
					<l n="64" num="8.3"><w n="64.1">L</w>'<w n="64.2">un</w> <w n="64.3">sur</w> <w n="64.4">l</w>'<w n="64.5">autre</w>, <w n="64.6">creusa</w> <w n="64.7">leur</w> <w n="64.8">tombe</w>, <w n="64.9">et</w> <w n="64.10">s</w>'<w n="64.11">en</w> <w n="64.12">alla</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 2 octobre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>