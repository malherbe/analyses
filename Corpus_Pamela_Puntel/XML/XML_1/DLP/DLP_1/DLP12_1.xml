<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP12">
				<head type="number">XII</head>
				<head type="main">SOUVENIR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">nous</w> <w n="1.3">étions</w> <w n="1.4">enfants</w> <w n="1.5">tous</w> <w n="1.6">doux</w>, <w n="1.7">mon</w> <w n="1.8">frère</w> <w n="1.9">et</w> <w n="1.10">moi</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Au</w> <w n="2.2">mois</w> <w n="2.3">d</w>'<w n="2.4">août</w>, <w n="2.5">nous</w> <w n="2.6">allions</w>, <w n="2.7">plus</w> <w n="2.8">triomphants</w> <w n="2.9">qu</w>'<w n="2.10">un</w> <w n="2.11">roi</w></l>
					<l n="3" num="1.3"><w n="3.1">Au</w> <w n="3.2">château</w> <w n="3.3">du</w> <w n="3.4">grand</w>'<w n="3.5">père</w>, <w n="3.6">au</w> <w n="3.7">fond</w> <w n="3.8">de</w> <w n="3.9">la</w> <w n="3.10">montagne</w>,</l>
					<l n="4" num="1.4"><w n="4.1">En</w> <w n="4.2">Bourgogne</w>, <w n="4.3">au</w> <w n="4.4">milieu</w> <w n="4.5">de</w> <w n="4.6">la</w> <w n="4.7">belle</w> <w n="4.8">campagne</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Toute</w>, <w n="5.2">chaude</w>, <w n="5.3">et</w> <w n="5.4">dorée</w> <w n="5.5">aux</w> <w n="5.6">feux</w> <w n="5.7">de</w> <w n="5.8">ce</w> <w n="5.9">soleil</w></l>
					<l n="6" num="1.6"><w n="6.1">Qui</w> <w n="6.2">fait</w> <w n="6.3">le</w> <w n="6.4">blé</w> <w n="6.5">plus</w> <w n="6.6">jaune</w> <w n="6.7">et</w> <w n="6.8">le</w> <w n="6.9">vin</w> <w n="6.10">plus</w> <w n="6.11">vermeil</w>.</l>
					<l n="7" num="1.7"><w n="7.1">C</w>'<w n="7.2">est</w> <w n="7.3">là</w> <w n="7.4">que</w> <w n="7.5">se</w> <w n="7.6">passaient</w> <w n="7.7">nos</w> <w n="7.8">joyeuses</w> <w n="7.9">vacances</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Il</w> <w n="8.2">me</w> <w n="8.3">semble</w> <w n="8.4">avoir</w> <w n="8.5">eu</w> <w n="8.6">souvent</w> <w n="8.7">plusieurs</w> <w n="8.8">enfances</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Tant</w> <w n="9.2">les</w> <w n="9.3">ressouvenirs</w> <w n="9.4">du</w> <w n="9.5">passé</w> <w n="9.6">sont</w> <w n="9.7">nombreux</w>.</l>
				</lg>
				<lg n="2">
					<l n="10" num="2.1"><w n="10.1">J</w>'<w n="10.2">ai</w> <w n="10.3">ces</w> <w n="10.4">premiers</w> <w n="10.5">jours</w>-<w n="10.6">là</w> <w n="10.7">toujours</w> <w n="10.8">devant</w> <w n="10.9">les</w> <w n="10.10">yeux</w>.</l>
					<l n="11" num="2.2"><w n="11.1">Figurez</w>-<w n="11.2">vous</w> <w n="11.3">la</w> <w n="11.4">route</w> <w n="11.5">aride</w> <w n="11.6">à</w> <w n="11.7">la</w> <w n="11.8">montée</w></l>
					<l n="12" num="2.3"><w n="12.1">Avec</w> <w n="12.2">la</w> <w n="12.3">haie</w> <w n="12.4">en</w> <w n="12.5">fleur</w> <w n="12.6">au</w> <w n="12.7">bord</w> <w n="12.8">d</w>'<w n="12.9">un</w> <w n="12.10">champ</w> <w n="12.11">plantée</w>,</l>
					<l n="13" num="2.4"><w n="13.1">Et</w> <w n="13.2">tout</w> <w n="13.3">en</w> <w n="13.4">haut</w>, <w n="13.5">après</w> <w n="13.6">un</w> <w n="13.7">talus</w> <w n="13.8">fort</w> <w n="13.9">glissant</w>,</l>
					<l n="14" num="2.5"><w n="14.1">La</w> <w n="14.2">croix</w>, <w n="14.3">blanche</w> <w n="14.4">jadis</w>, <w n="14.5">qu</w>'<w n="14.6">on</w> <w n="14.7">salue</w> <w n="14.8">en</w> <w n="14.9">passant</w>,</l>
					<l n="15" num="2.6"><w n="15.1">A</w> <w n="15.2">côté</w>, <w n="15.3">le</w> <w n="15.4">grand</w> <w n="15.5">bois</w> <w n="15.6">qui</w> <w n="15.7">se</w> <w n="15.8">perd</w> <w n="15.9">dans</w> <w n="15.10">l</w>'<w n="15.11">espace</w>,</l>
					<l n="16" num="2.7"><w n="16.1">En</w> <w n="16.2">bas</w>, <w n="16.3">dans</w> <w n="16.4">le</w> <w n="16.5">vallon</w>, <w n="16.6">la</w> <w n="16.7">rivière</w> <w n="16.8">qui</w> <w n="16.9">passe</w>,</l>
					<l n="17" num="2.8"><w n="17.1">Et</w> <w n="17.2">tout</w> <w n="17.3">près</w>, <w n="17.4">le</w> <w n="17.5">château</w> <w n="17.6">dans</w> <w n="17.7">son</w> <w n="17.8">beau</w> <w n="17.9">parc</w> <w n="17.10">ombreux</w>…</l>
				</lg>
				<lg n="3">
					<l n="18" num="3.1"><w n="18.1">Lorsque</w> <w n="18.2">j</w>'<w n="18.3">arrivais</w> <w n="18.4">là</w>, <w n="18.5">Dieu</w> ! <w n="18.6">que</w> <w n="18.7">j</w>'<w n="18.8">étais</w> <w n="18.9">heureux</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Sans</w> <w n="19.2">doute</w> <w n="19.3">vous</w> <w n="19.4">trouvez</w> <w n="19.5">cela</w> <w n="19.6">très</w>-<w n="19.7">ordinaire</w> ?</l>
					<l n="20" num="4.2"><w n="20.1">Eh</w> <w n="20.2">bien</w>, <w n="20.3">hier</w> <w n="20.4">encor</w> <w n="20.5">j</w>'<w n="20.6">allais</w> <w n="20.7">chez</w> <w n="20.8">mon</w> <w n="20.9">grand</w>'<w n="20.10">père</w>,</l>
					<l n="21" num="4.3"><w n="21.1">Et</w> <w n="21.2">dès</w> <w n="21.3">que</w> <w n="21.4">j</w>'<w n="21.5">arrivais</w> <w n="21.6">je</w> <w n="21.7">me</w> <w n="21.8">sentais</w> <w n="21.9">ému</w></l>
					<l n="22" num="4.4"><w n="22.1">Dans</w> <w n="22.2">ce</w> <w n="22.3">pays</w> <w n="22.4">aimé</w> <w n="22.5">que</w> <w n="22.6">j</w>'<w n="22.7">avais</w> <w n="22.8">tant</w> <w n="22.9">connu</w>.</l>
					<l n="23" num="4.5"><w n="23.1">Le</w> <w n="23.2">lendemain</w>, <w n="23.3">dès</w> <w n="23.4">l</w>'<w n="23.5">aube</w>, <w n="23.6">oh</w> ! <w n="23.7">quelle</w> <w n="23.8">promenade</w> !</l>
					<l n="24" num="4.6"><w n="24.1">Je</w> <w n="24.2">courais</w> <w n="24.3">dans</w> <w n="24.4">les</w> <w n="24.5">champs</w>, <w n="24.6">sur</w> <w n="24.7">la</w> <w n="24.8">belle</w> <w n="24.9">esplanade</w>,</l>
					<l n="25" num="4.7"><w n="25.1">Dans</w> <w n="25.2">les</w> <w n="25.3">bois</w> <w n="25.4">où</w> <w n="25.5">si</w> <w n="25.6">fier</w> <w n="25.7">jadis</w> <w n="25.8">j</w>'<w n="25.9">avais</w> <w n="25.10">passé</w>,</l>
					<l n="26" num="4.8"><w n="26.1">Pour</w> <w n="26.2">bien</w> <w n="26.3">voir</w> <w n="26.4">si</w> <w n="26.5">le</w> <w n="26.6">temps</w> <w n="26.7">n</w>'<w n="26.8">avait</w> <w n="26.9">rien</w> <w n="26.10">effacé</w> :</l>
					<l n="27" num="4.9"><w n="27.1">Le</w> <w n="27.2">bonheur</w> <w n="27.3">que</w> <w n="27.4">j</w>'<w n="27.5">avais</w> <w n="27.6">ne</w> <w n="27.7">peut</w> <w n="27.8">pas</w> <w n="27.9">se</w> <w n="27.10">décrire</w>,</l>
					<l n="28" num="4.10"><w n="28.1">Ces</w> <w n="28.2">choses</w>-<w n="28.3">là</w>, <w n="28.4">vraiment</w>, <w n="28.5">il</w> <w n="28.6">ne</w> <w n="28.7">faut</w> <w n="28.8">pas</w> <w n="28.9">en</w> <w n="28.10">rire</w>…</l>
				</lg>
				<lg n="5">
					<l n="29" num="5.1"><w n="29.1">Chacun</w> <w n="29.2">a</w> <w n="29.3">dans</w> <w n="29.4">son</w> <w n="29.5">cœur</w> <w n="29.6">un</w> <w n="29.7">endroit</w> <w n="29.8">préféré</w>,</l>
					<l n="30" num="5.2"><w n="30.1">Et</w> <w n="30.2">s</w>'<w n="30.3">est</w> <w n="30.4">dit</w> : <w n="30.5">Si</w> <w n="30.6">je</w> <w n="30.7">peux</w>, <w n="30.8">c</w>'<w n="30.9">est</w> <w n="30.10">là</w> <w n="30.11">que</w> <w n="30.12">je</w> <w n="30.13">vivrai</w> :</l>
					<l n="31" num="5.3"><w n="31.1">Eh</w> <w n="31.2">bien</w>, <w n="31.3">pour</w> <w n="31.4">moi</w>, <w n="31.5">c</w>'<w n="31.6">est</w> <w n="31.7">là</w> <w n="31.8">que</w> <w n="31.9">j</w>'<w n="31.10">aurais</w> <w n="31.11">voulu</w> <w n="31.12">vivre</w> !</l>
					<l n="32" num="5.4"><w n="32.1">Vous</w> <w n="32.2">savez</w>, <w n="32.3">on</w> <w n="32.4">connaît</w> <w n="32.5">le</w> <w n="32.6">chemin</w> <w n="32.7">qu</w>'<w n="32.8">on</w> <w n="32.9">va</w> <w n="32.10">suivre</w>,</l>
					<l n="33" num="5.5"><w n="33.1">On</w> <w n="33.2">connaît</w> <w n="33.3">les</w> <w n="33.4">buissons</w> <w n="33.5">sur</w> <w n="33.6">le</w> <w n="33.7">sol</w> <w n="33.8">endormis</w>,</l>
					<l n="34" num="5.6"><w n="34.1">Et</w> <w n="34.2">même</w> <w n="34.3">des</w> <w n="34.4">brins</w> <w n="34.5">d</w>'<w n="34.6">herbe</w> <w n="34.7">on</w> <w n="34.8">s</w>'<w n="34.9">est</w> <w n="34.10">fait</w> <w n="34.11">des</w> <w n="34.12">amis</w> ;</l>
					<l n="35" num="5.7"><w n="35.1">On</w> <w n="35.2">sait</w> <w n="35.3">juste</w> <w n="35.4">l</w>'<w n="35.5">endroit</w> <w n="35.6">où</w> <w n="35.7">l</w>'<w n="35.8">aubépine</w> <w n="35.9">pousse</w>,</l>
					<l n="36" num="5.8"><w n="36.1">L</w>'<w n="36.2">endroit</w> <w n="36.3">où</w> <w n="36.4">l</w>'<w n="36.5">on</w> <w n="36.6">aura</w> <w n="36.7">les</w> <w n="36.8">meilleurs</w> <w n="36.9">lits</w> <w n="36.10">de</w> <w n="36.11">mousse</w>,</l>
					<l n="37" num="5.9"><w n="37.1">Pour</w> <w n="37.2">rêver</w> <w n="37.3">doucement</w> <w n="37.4">sans</w> <w n="37.5">craindre</w> <w n="37.6">la</w> <w n="37.7">chaleur</w>,</l>
					<l n="38" num="5.10"><w n="38.1">Et</w> <w n="38.2">pouvoir</w> <w n="38.3">aux</w> <w n="38.4">oiseaux</w> <w n="38.5">raconter</w> <w n="38.6">sa</w> <w n="38.7">douleur</w>.</l>
					<l n="39" num="5.11"><w n="39.1">Tenez</w> ! <w n="39.2">je</w> <w n="39.3">gagerais</w> <w n="39.4">m</w>'<w n="39.5">en</w> <w n="39.6">aller</w>, <w n="39.7">sans</w> <w n="39.8">lumière</w>,</l>
					<l n="40" num="5.12"><w n="40.1">La</w> <w n="40.2">nuit</w>, <w n="40.3">les</w> <w n="40.4">yeux</w> <w n="40.5">fermés</w>, <w n="40.6">m</w>'<w n="40.7">asseoir</w> <w n="40.8">sur</w> <w n="40.9">une</w> <w n="40.10">pierre</w></l>
					<l n="41" num="5.13"><w n="41.1">Que</w> <w n="41.2">je</w> <w n="41.3">revois</w> <w n="41.4">d</w>'<w n="41.5">ici</w> <w n="41.6">derrière</w> <w n="41.7">le</w> <w n="41.8">parvis</w></l>
					<l n="42" num="5.14"><w n="42.1">De</w> <w n="42.2">l</w>'<w n="42.3">église</w>, <w n="42.4">à</w> <w n="42.5">côté</w> <w n="42.6">d</w>'<w n="42.7">un</w> <w n="42.8">champ</w> <w n="42.9">de</w> <w n="42.10">chenevis</w>,</l>
					<l n="43" num="5.15"><w n="43.1">Ou</w> <w n="43.2">j</w>'<w n="43.3">ai</w> <w n="43.4">conté</w> <w n="43.5">jadis</w> <w n="43.6">bien</w> <w n="43.7">des</w> <w n="43.8">vers</w> <w n="43.9">à</w> <w n="43.10">la</w> <w n="43.11">lune</w></l>
					<l n="44" num="5.16"><w n="44.1">Qui</w> <w n="44.2">ne</w> <w n="44.3">trouvait</w> <w n="44.4">jamais</w> <w n="44.5">ma</w> <w n="44.6">visite</w> <w n="44.7">importune</w> !</l>
				</lg>
				<lg n="6">
					<l n="45" num="6.1"><w n="45.1">Eh</w> <w n="45.2">bien</w>, <w n="45.3">quand</w> <w n="45.4">on</w> <w n="45.5">m</w>'<w n="45.6">a</w> <w n="45.7">dit</w> : <w n="45.8">Les</w> <w n="45.9">Prussiens</w> <w n="45.10">sont</w> <w n="45.11">là</w> !</l>
					<l part="I" n="46" num="6.2"><w n="46.1">J</w>'<w n="46.2">ai</w> <w n="46.3">pleuré</w>. </l>
					<l part="F" n="46" num="6.2"><w n="46.4">Je</w> <w n="46.5">voyais</w> <w n="46.6">à</w> <w n="46.7">travers</w> <w n="46.8">tout</w> <w n="46.9">cela</w></l>
					<l n="47" num="6.3"><w n="47.1">Passer</w> <w n="47.2">le</w> <w n="47.3">soudard</w> <w n="47.4">ivre</w> <w n="47.5">et</w> <w n="47.6">demandant</w> <w n="47.7">à</w> <w n="47.8">boire</w> ;</l>
					<l n="48" num="6.4"><w n="48.1">Tout</w> <w n="48.2">ce</w> <w n="48.3">que</w> <w n="48.4">je</w> <w n="48.5">gardais</w> <w n="48.6">au</w> <w n="48.7">fond</w> <w n="48.8">de</w> <w n="48.9">ma</w> <w n="48.10">mémoire</w>,</l>
					<l n="49" num="6.5"><w n="49.1">Souvenirs</w> <w n="49.2">d</w>'<w n="49.3">un</w> <w n="49.4">passé</w> <w n="49.5">qui</w> <w n="49.6">fait</w> <w n="49.7">battre</w> <w n="49.8">mon</w> <w n="49.9">cœur</w>,</l>
					<l n="50" num="6.6"><w n="50.1">Je</w> <w n="50.2">voyais</w> <w n="50.3">tout</w> <w n="50.4">cela</w> <w n="50.5">foulé</w> <w n="50.6">par</w> <w n="50.7">le</w> <w n="50.8">vainqueur</w> !</l>
					<l n="51" num="6.7"><w n="51.1">Tout</w> <w n="51.2">ce</w> <w n="51.3">que</w> <w n="51.4">j</w>'<w n="51.5">aimais</w> <w n="51.6">tant</w>, <w n="51.7">ma</w> <w n="51.8">seconde</w> <w n="51.9">patrie</w></l>
					<l n="52" num="6.8"><w n="52.1">Par</w> <w n="52.2">les</w> <w n="52.3">soldats</w> <w n="52.4">maudits</w> <w n="52.5">désolée</w> <w n="52.6">et</w> <w n="52.7">flétrie</w> !…</l>
				</lg>
				<lg n="7">
					<l n="53" num="7.1"><w n="53.1">Quand</w> <w n="53.2">les</w> <w n="53.3">reverrons</w>-<w n="53.4">nous</w> <w n="53.5">ces</w> <w n="53.6">beaux</w> <w n="53.7">jours</w> <w n="53.8">d</w>'<w n="53.9">autrefois</w>,</l>
					<l n="54" num="7.2"><w n="54.1">O</w> <w n="54.2">ma</w> <w n="54.3">chère</w> <w n="54.4">campagne</w>, <w n="54.5">o</w> <w n="54.6">mes</w> <w n="54.7">champs</w>, <w n="54.8">ô</w> <w n="54.9">mes</w> <w n="54.10">bois</w> !…</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Chaumont, 8 septembre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>