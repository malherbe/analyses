<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP31">
				<head type="number">XXXI</head>
				<head type="main">LE BOSSU</head>
				<opener>
					<salute> A ALFRED STEVEN</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Oh</w> ! <w n="1.2">comme</w> <w n="1.3">on</w> <w n="1.4">le</w> <w n="1.5">raillait</w> <w n="1.6">ce</w> <w n="1.7">pauvre</w> <w n="1.8">petit</w> <w n="1.9">être</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">tout</w> <w n="2.3">disgracieux</w> <w n="2.4">le</w> <w n="2.5">ciel</w> <w n="2.6">avait</w> <w n="2.7">fait</w> <w n="2.8">naître</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Quand</w> <w n="3.2">il</w> <w n="3.3">était</w> <w n="3.4">dehors</w> <w n="3.5">tes</w> <w n="3.6">méchants</w> <w n="3.7">en</w> <w n="3.8">riaient</w>…</l>
					<l n="4" num="1.4"><w n="4.1">Lui</w>, <w n="4.2">sentant</w> <w n="4.3">dans</w> <w n="4.4">ses</w> <w n="4.5">yeux</w> <w n="4.6">dos</w> <w n="4.7">larmes</w> <w n="4.8">qui</w> <w n="4.9">brillaient</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Savait</w> <w n="5.2">bien</w> — <w n="5.3">Dieu</w> <w n="5.4">souvent</w> <w n="5.5">est</w> <w n="5.6">trop</w> <w n="5.7">injuste</w>, <w n="5.8">en</w> <w n="5.9">somme</w> —</l>
					<l n="6" num="1.6"><w n="6.1">Que</w> <w n="6.2">ce</w> <w n="6.3">corps</w> <w n="6.4">e</w> <w n="6.5">bouffon</w> <w n="6.6">cachait</w> <w n="6.7">l</w>'<w n="6.8">âme</w> <w n="6.9">d</w>'<w n="6.10">un</w> <w n="6.11">homme</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Or</w>, <w n="7.2">le</w> <w n="7.3">dix</w>-<w n="7.4">neuf</w>, — <w n="7.5">voyant</w> <w n="7.6">les</w> <w n="7.7">soldats</w> <w n="7.8">revenir</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Après</w> <w n="8.2">avoir</w> <w n="8.3">vaincu</w> <w n="8.4">sans</w> <w n="8.5">avoir</w> <w n="8.6">pu</w> <w n="8.7">tenir</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Je</w> <w n="9.2">regardais</w> <w n="9.3">pensif</w> <w n="9.4">tous</w> <w n="9.5">ces</w> <w n="9.6">visages</w> <w n="9.7">sombres</w> :</l>
					<l n="10" num="1.10"><w n="10.1">Déjà</w> <w n="10.2">la</w> <w n="10.3">nuit</w> <w n="10.4">partout</w> <w n="10.5">avait</w> <w n="10.6">jeté</w> <w n="10.7">ses</w> <w n="10.8">ombres</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">comme</w> <w n="11.3">on</w> <w n="11.4">est</w> <w n="11.5">plus</w> <w n="11.6">triste</w> <w n="11.7">à</w> <w n="11.8">la</w> <w n="11.9">nuit</w> <w n="11.10">qu</w>'<w n="11.11">en</w> <w n="11.12">plein</w> <w n="11.13">jour</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Je</w> <w n="12.2">pleurais</w> <w n="12.3">en</w> <w n="12.4">dedans</w> <w n="12.5">ce</w> <w n="12.6">glorieux</w> <w n="12.7">retour</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">les</w> <w n="13.3">pauvres</w> <w n="13.4">héros</w> <w n="13.5">tombés</w> <w n="13.6">dans</w> <w n="13.7">la</w> <w n="13.8">bataille</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Lorsque</w> <w n="14.2">je</w> <w n="14.3">vis</w> <w n="14.4">l</w>'<w n="14.5">un</w> <w n="14.6">d</w>'<w n="14.7">eux</w>, <w n="14.8">haussant</w> <w n="14.9">sa</w> <w n="14.10">maigre</w> <w n="14.11">taille</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Par</w> <w n="15.2">la</w> <w n="15.3">foule</w>, <w n="15.4">hors</w> <w n="15.5">des</w> <w n="15.6">rang</w>, <w n="15.7">voulant</w> <w n="15.8">être</w> <w n="15.9">aperçu</w> ;</l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">la</w> <w n="16.3">foule</w> <w n="16.4">disait</w> : <w n="16.5">Oh</w> ! <w n="16.6">le</w> <w n="16.7">petit</w> <w n="16.8">bossu</w> !</l>
					<l n="17" num="1.17"><w n="17.1">Il</w> <w n="17.2">était</w> <w n="17.3">là</w>, <w n="17.4">portant</w> <w n="17.5">fièrement</w> <w n="17.6">sur</w> <w n="17.7">l</w>'<w n="17.8">épaule</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Comme</w> <w n="18.2">un</w> <w n="18.3">comédien</w> <w n="18.4">qui</w> <w n="18.5">va</w> <w n="18.6">jouer</w> <w n="18.7">son</w> <w n="18.8">rôle</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Le</w> <w n="19.2">gros</w> <w n="19.3">sac</w> <w n="19.4">et</w> <w n="19.5">la</w> <w n="19.6">tente</w>, <w n="19.7">ornements</w> <w n="19.8">d</w>'<w n="19.9">aujourd</w>'<w n="19.10">hui</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">le</w> <w n="20.3">grand</w> <w n="20.4">chassepot</w> <w n="20.5">deux</w> <w n="20.6">fois</w> <w n="20.7">plus</w> <w n="20.8">long</w> <w n="20.9">que</w> <w n="20.10">lui</w> !</l>
					<l n="21" num="1.21"><w n="21.1">Il</w> <w n="21.2">paraissait</w>, <w n="21.3">hélas</w> ! <w n="21.4">brisé</w> <w n="21.5">le</w> <w n="21.6">pauvre</w> <w n="21.7">diable</w>,</l>
					<l n="22" num="1.22"><w n="22.1">D</w>'<w n="22.2">un</w> <w n="22.3">effort</w> <w n="22.4">dont</w> <w n="22.5">jamais</w> <w n="22.6">on</w> <w n="22.7">ne</w> <w n="22.8">l</w>'<w n="22.9">eût</w> <w n="22.10">cru</w> <w n="22.11">capable</w> ;</l>
					<l n="23" num="1.23"><w n="23.1">Mais</w> <w n="23.2">grandi</w> <w n="23.3">par</w> <w n="23.4">l</w>'<w n="23.5">approche</w> <w n="23.6">énorme</w> <w n="23.7">du</w> <w n="23.8">tombeau</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Ce</w> <w n="24.2">petit</w> <w n="24.3">bossu</w>-<w n="24.4">là</w> <w n="24.5">me</w> <w n="24.6">parut</w> <w n="24.7">le</w> <w n="24.8">plus</w> <w n="24.9">beau</w> !</l>
					<l n="25" num="1.25"><w n="25.1">Au</w> <w n="25.2">moins</w> <w n="25.3">son</w> <w n="25.4">camarade</w> <w n="25.5">avait</w> <w n="25.6">une</w> <w n="25.7">maîtresse</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Une</w> <w n="26.2">femme</w>, <w n="26.3">quelqu</w>'<w n="26.4">un</w> <w n="26.5">qui</w> <w n="26.6">pour</w> <w n="26.7">vous</w> <w n="26.8">s</w>'<w n="26.9">intéresse</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Et</w> <w n="27.2">qui</w> <w n="27.3">paye</w> <w n="27.4">en</w> <w n="27.5">baisers</w> <w n="27.6">son</w> <w n="27.7">glorieux</w> <w n="27.8">labeur</w> ;</l>
					<l n="28" num="1.28"><w n="28.1">Car</w> <w n="28.2">la</w> <w n="28.3">gloire</w> <w n="28.4">a</w> <w n="28.5">toujours</w> <w n="28.6">sa</w> <w n="28.7">monnaie</w> <w n="28.8">en</w> <w n="28.9">bonheur</w>…</l>
					<l n="29" num="1.29"><w n="29.1">Mais</w> <w n="29.2">lui</w>, <w n="29.3">qui</w> <w n="29.4">l</w>'<w n="29.5">attendra</w> ? <w n="29.6">mais</w> <w n="29.7">lui</w>, <w n="29.8">pauvre</w> <w n="29.9">grotesque</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Dont</w> <w n="30.2">la</w> <w n="30.3">taille</w> <w n="30.4">et</w> <w n="30.5">le</w> <w n="30.6">cœur</w> <w n="30.7">n</w>'<w n="30.8">ont</w> <w n="30.9">rien</w> <w n="30.10">de</w> <w n="30.11">romanesque</w>,</l>
					<l n="31" num="1.31"><w n="31.1">Qui</w> <w n="31.2">lui</w> <w n="31.3">dira</w> : <w n="31.4">merci</w> ! <w n="31.5">le</w> <w n="31.6">soir</w>, <w n="31.7">à</w> <w n="31.8">son</w> <w n="31.9">retour</w> ?</l>
					<l n="32" num="1.32"><w n="32.1">Sa</w> <w n="32.2">gloire</w>, <w n="32.3">à</w> <w n="32.4">lui</w>, <w n="32.5">n</w>'<w n="32.6">a</w> <w n="32.7">pas</w> <w n="32.8">sa</w> <w n="32.9">monnaie</w> <w n="32.10">en</w> <w n="32.11">amour</w> !</l>
				</lg>
				<lg n="2">
					<l n="33" num="2.1"><w n="33.1">C</w>'<w n="33.2">est</w> <w n="33.3">que</w> <w n="33.4">pour</w> <w n="33.5">lui</w> <w n="33.6">la</w> <w n="33.7">France</w> <w n="33.8">est</w> <w n="33.9">la</w> <w n="33.10">seule</w> <w n="33.11">chérie</w>,</l>
					<l n="34" num="2.2"><w n="34.1">Et</w> <w n="34.2">qu</w>'<w n="34.3">il</w> <w n="34.4">atout</w> <w n="34.5">gardé</w> <w n="34.6">d</w>'<w n="34.7">amour</w> <w n="34.8">pour</w> <w n="34.9">la</w> <w n="34.10">Patrie</w> !</l>
					<l n="35" num="2.3"><w n="35.1">Car</w>, <w n="35.2">au</w> <w n="35.3">fusil</w>, <w n="35.4">au</w> <w n="35.5">sac</w>, <w n="35.6">qui</w> <w n="35.7">lui</w> <w n="35.8">brisaient</w> <w n="35.9">les</w> <w n="35.10">bras</w>,</l>
					<l n="36" num="2.4"><w n="36.1">Eh</w> <w n="36.2">bien</w> ! <w n="36.3">c</w>'<w n="36.4">était</w> <w n="36.5">le</w> <w n="36.6">seul</w> <w n="36.7">qui</w> <w n="36.8">ne</w> <w n="36.9">se</w> <w n="36.10">plaignit</w> <w n="36.11">pas</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Paris, 22 Janvier 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>