<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP26">
				<head type="number">XXVI</head>
				<head type="main">ORLÉANS</head>
				<head type="sub">LA PREMIÈRE VICTOIRE</head>
				<lg n="1">
					<l part="I" n="1" num="1.1"><w n="1.1">On</w> <w n="1.2">nous</w> <w n="1.3">disait</w> : </l>
					<l part="F" n="1" num="1.1">— <w n="1.4">Pourquoi</w> <w n="1.5">vouloir</w> <w n="1.6">lutter</w> <w n="1.7">encor</w> ?</l>
					<l n="2" num="1.2"><w n="2.1">Pourquoi</w> ? <w n="2.2">Pour</w> <w n="2.3">y</w> <w n="2.4">gagner</w> <w n="2.5">la</w> <w n="2.6">victoire</w> <w n="2.7">ou</w> <w n="2.8">la</w> <w n="2.9">mort</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Eh</w> <w n="3.2">bien</w> ! <w n="3.3">cette</w> <w n="3.4">fois</w>-<w n="3.5">ci</w> <w n="3.6">notre</w> <w n="3.7">heure</w> <w n="3.8">est</w> <w n="3.9">arrivée</w> !</l>
					<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">vieille</w> <w n="4.3">âme</w> <w n="4.4">française</w> <w n="4.5">est</w> <w n="4.6">enfin</w> <w n="4.7">retrouvée</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Oh</w> ! <w n="5.2">non</w> ; <w n="5.3">quand</w> <w n="5.4">cinq</w> <w n="5.5">cents</w> <w n="5.6">ans</w> <w n="5.7">notre</w> <w n="5.8">front</w> <w n="5.9">s</w>'<w n="5.10">éleva</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Nous</w> <w n="6.2">ne</w> <w n="6.3">pouvions</w> <w n="6.4">pas</w> <w n="6.5">être</w> <w n="6.6">un</w> <w n="6.7">peuple</w> <w n="6.8">qui</w> <w n="6.9">s</w>'<w n="6.10">en</w> <w n="6.11">va</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Comment</w> ! <w n="7.2">on</w> <w n="7.3">aurait</w> <w n="7.4">vu</w> <w n="7.5">d</w>'<w n="7.6">un</w> <w n="7.7">coup</w> <w n="7.8">tomber</w> <w n="7.9">la</w> <w n="7.10">France</w> !</l>
					<l n="8" num="2.4"><w n="8.1">Comment</w> ! <w n="8.2">plus</w> <w n="8.3">de</w> <w n="8.4">courage</w> <w n="8.5">au</w> <w n="8.6">cœur</w>, <w n="8.7">plus</w> <w n="8.8">d</w>'<w n="8.9">espérance</w>,</l>
					<l n="9" num="2.5"><w n="9.1">Plus</w> <w n="9.2">de</w> <w n="9.3">foi</w> <w n="9.4">dans</w> <w n="9.5">le</w> <w n="9.6">ciel</w>, <w n="9.7">et</w> <w n="9.8">plus</w> <w n="9.9">de</w> <w n="9.10">force</w> <w n="9.11">en</w> <w n="9.12">nous</w> !</l>
					<l n="10" num="2.6"><w n="10.1">Allons</w> ! <w n="10.2">dresse</w> <w n="10.3">ton</w> <w n="10.4">front</w> <w n="10.5">meurtri</w>, <w n="10.6">France</w> <w n="10.7">à</w> <w n="10.8">genoux</w> !</l>
					<l n="11" num="2.7"><w n="11.1">Ta</w> <w n="11.2">coupe</w> <w n="11.3">d</w>'<w n="11.4">amertume</w> <w n="11.5">est</w> <w n="11.6">maintenant</w> <w n="11.7">finie</w> :</l>
					<l n="12" num="2.8"><w n="12.1">Toi</w> <w n="12.2">qui</w> <w n="12.3">jetais</w> <w n="12.4">si</w> <w n="12.5">loin</w> <w n="12.6">l</w>'<w n="12.7">éclat</w> <w n="12.8">de</w> <w n="12.9">ton</w> <w n="12.10">génie</w>,</l>
					<l n="13" num="2.9"><w n="13.1">Suis</w> <w n="13.2">toujours</w>, <w n="13.3">à</w> <w n="13.4">travers</w> <w n="13.5">ton</w> <w n="13.6">sol</w> <w n="13.7">ensanglanté</w>,</l>
					<l n="14" num="2.10"><w n="14.1">Ce</w> <w n="14.2">chemin</w> <w n="14.3">qui</w> <w n="14.4">tout</w> <w n="14.5">droit</w> <w n="14.6">mine</w> <w n="14.7">à</w> <w n="14.8">la</w> <w n="14.9">liberté</w> !</l>
					<l n="15" num="2.11"><w n="15.1">Songe</w> <w n="15.2">qu</w>'<w n="15.3">il</w> <w n="15.4">faut</w> <w n="15.5">lutter</w> <w n="15.6">cinq</w> <w n="15.7">mois</w> <w n="15.8">encore</w>, <w n="15.9">peut</w>-<w n="15.10">être</w>.</l>
					<l n="16" num="2.12"><w n="16.1">Avant</w> <w n="16.2">de</w> <w n="16.3">voir</w> <w n="16.4">enfin</w> <w n="16.5">le</w> <w n="16.6">grand</w> <w n="16.7">passé</w> <w n="16.8">renaître</w>,</l>
					<l n="17" num="2.13"><w n="17.1">Et</w> <w n="17.2">que</w> <w n="17.3">c</w>'<w n="17.4">est</w> <w n="17.5">aujourd</w>'<w n="17.6">hui</w> <w n="17.7">pour</w> <w n="17.8">ceux</w> <w n="17.9">qui</w> <w n="17.10">vont</w> <w n="17.11">mourir</w></l>
					<l n="18" num="2.14"><w n="18.1">Un</w> <w n="18.2">pays</w> <w n="18.3">tout</w> <w n="18.4">entier</w> <w n="18.5">qu</w>'<w n="18.6">il</w> <w n="18.7">faut</w> <w n="18.8">reconquérir</w> !</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1"><w n="19.1">Orléans</w> <w n="19.2">est</w> <w n="19.3">repris</w> ! <w n="19.4">C</w>'<w n="19.5">est</w> <w n="19.6">la</w> <w n="19.7">première</w> <w n="19.8">étape</w> !</l>
				</lg>
				<lg n="4">
					<l part="I" n="20" num="4.1"><w n="20.1">Marche</w> ! </l>
					<l part="F" n="20" num="4.1"><w n="20.2">Encor</w> <w n="20.3">quelques</w> <w n="20.4">jours</w>, <w n="20.5">et</w> <w n="20.6">Paris</w> <w n="20.7">leur</w> <w n="20.8">échappe</w> !</l>
					<l n="21" num="4.2"><w n="21.1">Et</w> <w n="21.2">les</w> <w n="21.3">Maudits</w> <w n="21.4">verront</w>, <w n="21.5">quand</w> <w n="21.6">l</w>'<w n="21.7">épée</w> <w n="21.8">aura</w> <w n="21.9">lui</w>,</l>
					<l n="22" num="4.3"><w n="22.1">Tous</w> <w n="22.2">les</w> <w n="22.3">conscrits</w> <w n="22.4">d</w>'<w n="22.5">hier</w>, <w n="22.6">vétérans</w> <w n="22.7">d</w>'<w n="22.8">aujourd</w>'<w n="22.9">hui</w> !</l>
				</lg>
				<lg n="5">
					<l part="I" n="23" num="5.1"><w n="23.1">Marche</w> ! </l>
					<l part="F" n="23" num="5.1">— <w n="23.2">La</w> <w n="23.3">route</w> <w n="23.4">est</w> <w n="23.5">longue</w> <w n="23.6">et</w> <w n="23.7">la</w> <w n="23.8">lutte</w> <w n="23.9">est</w> <w n="23.10">pénible</w>,</l>
					<l n="24" num="5.2"><w n="24.1">Mais</w> <w n="24.2">nous</w> <w n="24.3">avons</w> <w n="24.4">au</w> <w n="24.5">cœur</w> <w n="24.6">une</w> <w n="24.7">joie</w> <w n="24.8">indicible</w>.</l>
					<l n="25" num="5.3"><w n="25.1">Et</w> <w n="25.2">ce</w> <w n="25.3">premier</w> <w n="25.4">succès</w> <w n="25.5">qui</w> <w n="25.6">nous</w> <w n="25.7">enfièvre</w> <w n="25.8">tant</w></l>
					<l n="26" num="5.4"><w n="26.1">Fera</w> <w n="26.2">que</w> <w n="26.3">tes</w> <w n="26.4">soldats</w> <w n="26.5">mourront</w> <w n="26.6">tous</w> <w n="26.7">en</w> <w n="26.8">chantant</w> !</l>
				</lg>
				<lg n="6">
					<l part="I" n="27" num="6.1"><w n="27.1">Marche</w> ! </l>
					<l part="F" n="27" num="6.1">— <w n="27.2">Ils</w> <w n="27.3">sont</w> <w n="27.4">refoulés</w> <w n="27.5">au</w> <w n="27.6">delà</w> <w n="27.7">de</w> <w n="27.8">la</w> <w n="27.9">Saône</w> ;</l>
					<l n="28" num="6.2"><w n="28.1">Sois</w> <w n="28.2">fière</w> ! <w n="28.3">Parmi</w> <w n="28.4">nous</w> <w n="28.5">n</w>'<w n="28.6">a</w> <w n="28.7">reculé</w> <w n="28.8">personne</w> ;</l>
					<l n="29" num="6.3"><w n="29.1">Aucun</w> <w n="29.2">de</w> <w n="29.3">nous</w> <w n="29.4">n</w>'<w n="29.5">a</w> <w n="29.6">fui</w> <w n="29.7">les</w> <w n="29.8">coups</w> <w n="29.9">à</w> <w n="29.10">recevoir</w>,</l>
					<l n="30" num="6.4"><w n="30.1">Et</w> <w n="30.2">chacun</w> <w n="30.3">de</w> <w n="30.4">tes</w> <w n="30.5">fils</w> <w n="30.6">a</w> <w n="30.7">bien</w> <w n="30.8">fait</w> <w n="30.9">son</w> <w n="30.10">devoir</w> !</l>
					<l n="31" num="6.5"><w n="31.1">Les</w> <w n="31.2">dangers</w> ? <w n="31.3">à</w> <w n="31.4">quoi</w> <w n="31.5">bon</w> ! <w n="31.6">la</w> <w n="31.7">mort</w> ? <w n="31.8">que</w> <w n="31.9">nous</w> <w n="31.10">importe</w> !</l>
					<l n="32" num="6.6"><w n="32.1">Si</w> <w n="32.2">nos</w> <w n="32.3">corps</w> <w n="32.4">sont</w> <w n="32.5">meurtris</w>, <w n="32.6">notre</w> <w n="32.7">âme</w> <w n="32.8">est</w> <w n="32.9">toujours</w> <w n="32.10">forte</w> !</l>
					<l n="33" num="6.7"><w n="33.1">Et</w> <w n="33.2">pensant</w> <w n="33.3">aux</w> <w n="33.4">amis</w> <w n="33.5">tombés</w> <w n="33.6">sur</w> <w n="33.7">le</w> <w n="33.8">chemin</w>,</l>
					<l n="34" num="6.8"><w n="34.1">Nous</w> <w n="34.2">envierons</w> <w n="34.3">leur</w> <w n="34.4">sort</w>, <w n="34.5">prêts</w> <w n="34.6">à</w> <w n="34.7">tomber</w> <w n="34.8">demain</w> !</l>
				</lg>
				<lg n="7">
					<l part="I" n="35" num="7.1"><w n="35.1">Marche</w> ! </l>
					<l part="F" n="35" num="7.1">— <w n="35.2">Va</w> <w n="35.3">d</w>'<w n="35.4">un</w> <w n="35.5">coup</w> <w n="35.6">d</w>'<w n="35.7">aile</w> <w n="35.8">à</w> <w n="35.9">ta</w> <w n="35.10">sainte</w> <w n="35.11">frontière</w> :</l>
					<l n="36" num="7.2"><w n="36.1">Quand</w> <w n="36.2">tu</w> <w n="36.3">seras</w> <w n="36.4">debout</w> <w n="36.5">en</w> <w n="36.6">armes</w>, <w n="36.7">tout</w> <w n="36.8">entière</w>,</l>
					<l n="37" num="7.3"><w n="37.1">Nous</w> <w n="37.2">verrons</w> <w n="37.3">qui</w> <w n="37.4">des</w> <w n="37.5">deux</w> <w n="37.6">fera</w> <w n="37.7">ce</w> <w n="37.8">qu</w>'<w n="37.9">il</w> <w n="37.10">a</w> <w n="37.11">dit</w>,</l>
					<l n="38" num="7.4"><w n="38.1">Du</w> <w n="38.2">peuple</w> <w n="38.3">qu</w>'<w n="38.4">on</w> <w n="38.5">admire</w>, <w n="38.6">ou</w> <w n="38.7">du</w> <w n="38.8">roi</w> <w n="38.9">qu</w>'<w n="38.10">on</w> <w n="38.11">maudit</w> !</l>
				</lg>
				<lg n="8">
					<l n="39" num="8.1"><w n="39.1">Enfin</w>, <w n="39.2">marche</w> <w n="39.3">toujours</w>, <w n="39.4">France</w>, <w n="39.5">marche</w> <w n="39.6">sans</w> <w n="39.7">cesse</w> !</l>
					<l n="40" num="8.2"><w n="40.1">Jusqu</w>'<w n="40.2">à</w> <w n="40.3">ce</w> <w n="40.4">qu</w>'<w n="40.5">ayant</w> <w n="40.6">fait</w> <w n="40.7">ton</w> <w n="40.8">œuvre</w> <w n="40.9">vengeresse</w>,</l>
					<l n="41" num="8.3"><w n="41.1">Après</w> <w n="41.2">avoir</w> <w n="41.3">lutté</w> <w n="41.4">deux</w> <w n="41.5">cents</w> <w n="41.6">jours</w> <w n="41.7">pour</w> <w n="41.8">cela</w>,</l>
					<l n="42" num="8.4"><w n="42.1">Tu</w> <w n="42.2">puisses</w> <w n="42.3">étancher</w> <w n="42.4">tout</w> <w n="42.5">le</w> <w n="42.6">sang</w> <w n="42.7">qui</w> <w n="42.8">coula</w> !</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<l n="43" num="8.5"><w n="43.1">Et</w> <w n="43.2">maintenant</w> <w n="43.3">pensons</w> <w n="43.4">que</w> <w n="43.5">l</w>’<w n="43.6">œuvre</w> <w n="43.7">est</w> <w n="43.8">commencée</w>,</l>
					<l n="44" num="8.6"><w n="44.1">Pensons</w> <w n="44.2">que</w> <w n="44.3">vers</w> <w n="44.4">le</w> <w n="44.5">ciel</w> <w n="44.6">la</w> <w n="44.7">France</w> <w n="44.8">était</w> <w n="44.9">dressée</w></l>
					<l n="45" num="8.7"><w n="45.1">Pour</w> <w n="45.2">lui</w> <w n="45.3">montrer</w> <w n="45.4">ses</w> <w n="45.5">champs</w> <w n="45.6">sillonnés</w> <w n="45.7">par</w> <w n="45.8">le</w> <w n="45.9">feu</w>,</l>
					<l n="46" num="8.8"><w n="46.1">Et</w> <w n="46.2">qu</w>'<w n="46.3">à</w> <w n="46.4">son</w> <w n="46.5">cri</w> <w n="46.6">d</w>'<w n="46.7">appel</w> <w n="46.8">vient</w> <w n="46.9">de</w> <w n="46.10">répondre</w> <w n="46.11">Dieu</w> !</l>
				</lg>
				<lg n="9">
					<l n="47" num="9.1"><w n="47.1">Ne</w> <w n="47.2">songeons</w> <w n="47.3">au</w> <w n="47.4">succès</w> <w n="47.5">qu</w>'<w n="47.6">ont</w> <w n="47.7">remporté</w> <w n="47.8">les</w> <w n="47.9">nôtres</w></l>
					<l n="48" num="9.2"><w n="48.1">Que</w> <w n="48.2">pour</w> <w n="48.3">sentir</w> <w n="48.4">qu</w>'<w n="48.5">il</w> <w n="48.6">doit</w> <w n="48.7">être</w> <w n="48.8">suivi</w> <w n="48.9">par</w> <w n="48.10">d</w>'<w n="48.11">autres</w> !</l>
					<l n="49" num="9.3"><w n="49.1">Paris</w> <w n="49.2">doit</w> <w n="49.3">imiter</w> <w n="49.4">Orléans</w> : <w n="49.5">il</w> <w n="49.6">le</w> <w n="49.7">faut</w> !</l>
					<l n="50" num="9.4"><w n="50.1">Le</w> <w n="50.2">premier</w> <w n="50.3">pas</w> <w n="50.4">est</w> <w n="50.5">fait</w> ; <w n="50.6">eh</w> <w n="50.7">bien</w> ! <w n="50.8">montons</w> <w n="50.9">plus</w> <w n="50.10">haut</w> !</l>
					<l n="51" num="9.5"><w n="51.1">Orléans</w>, <w n="51.2">Tours</w>, <w n="51.3">Paris</w> : <w n="51.4">de</w> <w n="51.5">la</w> <w n="51.6">Seine</w> <w n="51.7">à</w> <w n="51.8">la</w> <w n="51.9">Loire</w>,</l>
					<l part="I" n="52" num="9.6"><w n="52.1">France</w> ! </l>
					<l part="F" n="52" num="9.6"><w n="52.2">Nous</w> <w n="52.3">te</w> <w n="52.4">ferons</w> <w n="52.5">des</w> <w n="52.6">étapes</w> <w n="52.7">de</w> <w n="52.8">gloire</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">15 novembre 1870.</date>
					</dateline>
					<note id="" type="footnote">
						Nous n'avons pas voulu retrancher les pièces qui attestent les enthousiasmes
						et les saintes croyances de tout un peuple. L'histoire dira si là où il y avait
						tant de foi, il ne devait pas y avoir héroïsme.
						(NOTE DE L'ÉDITEUR.)
					</note>
				</closer>
			</div></body></text></TEI>