<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP1">
				<head type="main">L'INVASION</head>
				<head type="sub">1870-1871</head>
				<head type="number">I</head>
					<head type="main">PRÉLUDE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">France</w> <w n="1.3">immortelle</w> <w n="1.4">et</w> <w n="1.5">féconde</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">le</w> <w n="2.3">peuple</w> <w n="2.4">est</w> <w n="2.5">le</w> <w n="2.6">peuple</w>-<w n="2.7">roi</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">qui</w> <w n="3.3">fais</w> <w n="3.4">frissonner</w> <w n="3.5">le</w> <w n="3.6">monde</w></l>
					<l n="4" num="1.4"><w n="4.1">D</w>'<w n="4.2">admiration</w> <w n="4.3">devant</w> <w n="4.4">toi</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Mère</w> <w n="5.2">de</w> <w n="5.3">luttes</w> <w n="5.4">grandioses</w></l>
					<l n="6" num="2.2"><w n="6.1">Comme</w> <w n="6.2">les</w> <w n="6.3">bardes</w> <w n="6.4">en</w> <w n="6.5">chantaient</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">résument</w> <w n="7.3">toutes</w> <w n="7.4">les</w> <w n="7.5">choses</w></l>
					<l n="8" num="2.4"><w n="8.1">Dont</w> <w n="8.2">Rome</w> <w n="8.3">et</w> <w n="8.4">Sparte</w> <w n="8.5">se</w> <w n="8.6">vantaient</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">O</w> <w n="9.2">France</w> ! <w n="9.3">j</w>'<w n="9.4">ai</w> <w n="9.5">pris</w> <w n="9.6">ton</w> <w n="9.7">histoire</w></l>
					<l n="10" num="3.2"><w n="10.1">Dans</w> <w n="10.2">ces</w> <w n="10.3">deux</w> <w n="10.4">cents</w> <w n="10.5">jours</w> <w n="10.6">écoulés</w>,</l>
					<l n="11" num="3.3"><w n="11.1">J</w>'<w n="11.2">ai</w> <w n="11.3">pris</w> <w n="11.4">tes</w> <w n="11.5">souffrances</w>, <w n="11.6">ta</w> <w n="11.7">gloire</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">tes</w> <w n="12.3">souvenirs</w> <w n="12.4">écroulés</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">de</w> <w n="13.3">tout</w> <w n="13.4">cela</w>, <w n="13.5">de</w> <w n="13.6">ces</w> <w n="13.7">crimes</w></l>
					<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">les</w> <w n="14.3">deux</w> <w n="14.4">tyrans</w> <w n="14.5">ont</w> <w n="14.6">commis</w></l>
					<l n="15" num="4.3"><w n="15.1">De</w> <w n="15.2">tes</w> <w n="15.3">soldats</w>, <w n="15.4">saintes</w> <w n="15.5">victimes</w></l>
					<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">tes</w> <w n="16.3">infâmes</w> <w n="16.4">ennemis</w> ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">De</w> <w n="17.2">tout</w>, <w n="17.3">des</w> <w n="17.4">larges</w> <w n="17.5">coups</w> <w n="17.6">d</w>'<w n="17.7">épée</w></l>
					<l n="18" num="5.2"><w n="18.1">Avec</w> <w n="18.2">lesquels</w> <w n="18.3">tu</w> <w n="18.4">te</w> <w n="18.5">défends</w>,</l>
					<l n="19" num="5.3"><w n="19.1">J</w>'<w n="19.2">ai</w> <w n="19.3">voulu</w> <w n="19.4">faire</w> <w n="19.5">une</w> <w n="19.6">épopée</w></l>
					<l n="20" num="5.4"><w n="20.1">Pour</w> <w n="20.2">la</w> <w n="20.3">léguer</w> <w n="20.4">à</w> <w n="20.5">tes</w> <w n="20.6">enfants</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Août-Octobre 1870.</date>
						<date when="1871">Octobre-Février 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>