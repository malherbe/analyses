<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP30">
				<head type="number">XXX</head>
				<head type="main">ÉPISODE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Oui</w>, <w n="1.2">leur</w> <w n="1.3">œuvre</w> <w n="1.4">est</w> <w n="1.5">superbe</w> <w n="1.6">il</w> <w n="1.7">vaut</w> <w n="1.8">bien</w> <w n="1.9">qu</w>'<w n="1.10">on</w> <w n="1.11">la</w> <w n="1.12">chante</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>'<w n="2.2">importe</w> <w n="2.3">à</w> <w n="2.4">ces</w> <w n="2.5">gens</w>-<w n="2.6">là</w> <w n="2.7">la</w> <w n="2.8">rage</w> <w n="2.9">ou</w> <w n="2.10">l</w>'<w n="2.11">épouvante</w> ?</l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>'<w n="3.2">importe</w> <w n="3.3">un</w> <w n="3.4">monument</w> <w n="3.5">qui</w> <w n="3.6">s</w>'<w n="3.7">écroule</w> <w n="3.8">à</w> <w n="3.9">moitié</w> ?</l>
					<l n="4" num="1.4"><w n="4.1">Cela</w> <w n="4.2">vaut</w> <w n="4.3">tout</w> <w n="4.4">au</w> <w n="4.5">plus</w> <w n="4.6">un</w> <w n="4.7">geste</w> <w n="4.8">de</w> <w n="4.9">pitié</w> !</l>
					<l n="5" num="1.5"><w n="5.1">Quoi</w> ! <w n="5.2">des</w> <w n="5.3">soldats</w> <w n="5.4">tués</w> ! <w n="5.5">des</w> <w n="5.6">hommes</w> <w n="5.7">qui</w> <w n="5.8">succombent</w> ?</l>
					<l n="6" num="1.6"><w n="6.1">Ce</w> <w n="6.2">n</w>'<w n="6.3">est</w> <w n="6.4">pas</w> <w n="6.5">pour</w> <w n="6.6">si</w> <w n="6.7">peu</w> <w n="6.8">que</w> <w n="6.9">tous</w> <w n="6.10">ces</w> <w n="6.11">obus</w> <w n="6.12">tombent</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Ah</w> ! <w n="7.2">Paris</w> <w n="7.3">veut</w> <w n="7.4">lutter</w> ? <w n="7.5">ah</w> ! <w n="7.6">Paris</w> <w n="7.7">se</w> <w n="7.8">défend</w> ?</l>
					<l n="8" num="1.8"><w n="8.1">Bien</w> ! <w n="8.2">nous</w> <w n="8.3">tuerons</w> <w n="8.4">la</w> <w n="8.5">femme</w> <w n="8.6">et</w> <w n="8.7">nous</w> <w n="8.8">tuerons</w> <w n="8.9">l</w>'<w n="8.10">enfant</w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Oh</w> ! <w n="9.2">je</w> <w n="9.3">vivrai</w> <w n="9.4">cent</w> <w n="9.5">ans</w> <w n="9.6">sans</w> <w n="9.7">que</w> <w n="9.8">l</w>'<w n="9.9">oubli</w> <w n="9.10">commence</w> !</l>
					<l n="10" num="2.2"><w n="10.1">Écoutez</w> : <w n="10.2">mon</w> <w n="10.3">cœur</w> <w n="10.4">saigne</w> <w n="10.5">et</w> <w n="10.6">bondit</w> <w n="10.7">quand</w> <w n="10.8">j</w>'<w n="10.9">y</w> <w n="10.10">pense</w> :</l>
					<l n="11" num="2.3"><w n="11.1">La</w> <w n="11.2">mère</w> <w n="11.3">avait</w> <w n="11.4">trente</w> <w n="11.5">ans</w> : <w n="11.6">son</w> <w n="11.7">fils</w> <w n="11.8">en</w> <w n="11.9">avait</w> <w n="11.10">dix</w>.</l>
					<l n="12" num="2.4"><w n="12.1">Vous</w> <w n="12.2">savez</w> ? <w n="12.3">ces</w> <w n="12.4">enfants</w> <w n="12.5">éveillés</w> <w n="12.6">et</w> <w n="12.7">hardis</w>,</l>
					<l n="13" num="2.5"><w n="13.1">Dont</w> <w n="13.2">on</w> <w n="13.3">dit</w> : « <w n="13.4">ce</w> <w n="13.5">garçon</w> <w n="13.6">arrivera</w> <w n="13.7">sans</w> <w n="13.8">faute</w> ! »</l>
					<l n="14" num="2.6"><w n="14.1">Eh</w> <w n="14.2">bien</w> ! <w n="14.3">je</w> <w n="14.4">les</w> <w n="14.5">ai</w> <w n="14.6">vus</w> <w n="14.7">étendus</w> <w n="14.8">côte</w> <w n="14.9">à</w> <w n="14.10">côte</w>,</l>
					<l n="15" num="2.7"><w n="15.1">Ayant</w> <w n="15.2">encor</w> <w n="15.3">gardé</w> <w n="15.4">ce</w> <w n="15.5">sourire</w> <w n="15.6">attristé</w></l>
					<l n="16" num="2.8"><w n="16.1">De</w> <w n="16.2">l</w>'<w n="16.3">Être</w> <w n="16.4">humain</w> <w n="16.5">qui</w> <w n="16.6">meurt</w> <w n="16.7">et</w> <w n="16.8">voit</w> <w n="16.9">l</w>'<w n="16.10">éternité</w> ;</l>
					<l n="17" num="2.9"><w n="17.1">Je</w> <w n="17.2">les</w> <w n="17.3">ai</w> <w n="17.4">vus</w>, <w n="17.5">auprès</w> <w n="17.6">d</w>'<w n="17.7">un</w> <w n="17.8">vieux</w> <w n="17.9">mur</w> <w n="17.10">en</w> <w n="17.11">ruine</w>,</l>
					<l n="18" num="2.10"><w n="18.1">Elle</w> <w n="18.2">frappée</w> <w n="18.3">au</w> <w n="18.4">front</w> <w n="18.5">et</w> <w n="18.6">lui</w> <w n="18.7">dans</w> <w n="18.8">la</w> <w n="18.9">poitrine</w> !</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1"><w n="19.1">Dieu</w> <w n="19.2">juste</w> ! <w n="19.3">Dieu</w> <w n="19.4">puissant</w> <w n="19.5">crucifié</w> <w n="19.6">pour</w> <w n="19.7">nous</w> !</l>
					<l n="20" num="3.2"><w n="20.1">Toi</w> <w n="20.2">l</w>'<w n="20.3">être</w> <w n="20.4">doux</w> <w n="20.5">et</w> <w n="20.6">bon</w> <w n="20.7">qu</w>'<w n="20.8">on</w> <w n="20.9">adore</w> <w n="20.10">à</w> <w n="20.11">genoux</w>,</l>
					<l n="21" num="3.3"><w n="21.1">Toi</w> <w n="21.2">qui</w> <w n="21.3">nous</w> <w n="21.4">dis</w> <w n="21.5">jadis</w> <w n="21.6">par</w> <w n="21.7">la</w> <w n="21.8">voix</w> <w n="21.9">des</w> <w n="21.10">apôtres</w> :</l>
					<l n="22" num="3.4">« <w n="22.1">Mes</w> <w n="22.2">enfants</w>, <w n="22.3">aimez</w>-<w n="22.4">vous</w> <w n="22.5">toujours</w> <w n="22.6">les</w> <w n="22.7">uns</w> <w n="22.8">les</w> <w n="22.9">autres</w>… »</l>
					<l n="23" num="3.5"><w n="23.1">En</w> <w n="23.2">plein</w> <w n="23.3">jour</w>, <w n="23.4">en</w> <w n="23.5">ce</w> <w n="23.6">siècle</w>, <w n="23.7">et</w> <w n="23.8">les</w> <w n="23.9">pieds</w> <w n="23.10">dans</w> <w n="23.11">le</w> <w n="23.12">sang</w>,</l>
					<l n="24" num="3.6"><w n="24.1">Voilà</w>, <w n="24.2">ce</w> <w n="24.3">que</w> <w n="24.4">j</w>'<w n="24.5">ai</w> <w n="24.6">vu</w>, <w n="24.7">Dieu</w> <w n="24.8">juste</w>, <w n="24.9">Dieu</w> <w n="24.10">puissant</w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">Quoi</w> <w n="25.2">qu</w>'<w n="25.3">il</w> <w n="25.4">puisse</w> <w n="25.5">advenir</w> <w n="25.6">de</w> <w n="25.7">nous</w>, <w n="25.8">ô</w> <w n="25.9">pauvre</w> <w n="25.10">France</w>,</l>
					<l n="26" num="4.2"><w n="26.1">Va</w> ! <w n="26.2">tu</w> <w n="26.3">peux</w> <w n="26.4">à</w> <w n="26.5">tes</w> <w n="26.6">pleurs</w> <w n="26.7">mêler</w> <w n="26.8">de</w> <w n="26.9">l</w>'<w n="26.10">espérance</w>,</l>
					<l n="27" num="4.3"><w n="27.1">Car</w> <w n="27.2">nous</w> <w n="27.3">allons</w> <w n="27.4">tous</w> <w n="27.5">vivre</w>, <w n="27.6">ardents</w> <w n="27.7">à</w> <w n="27.8">nous</w> <w n="27.9">venger</w></l>
					<l n="28" num="4.4"><w n="28.1">Du</w> <w n="28.2">sang</w> <w n="28.3">parisien</w> <w n="28.4">dans</w> <w n="28.5">le</w> <w n="28.6">sang</w> <w n="28.7">étranger</w> !</l>
				</lg>
				<lg n="5">
					<l n="29" num="5.1"><w n="29.1">Si</w> <w n="29.2">l</w>'<w n="29.3">heure</w> <w n="29.4">doit</w> <w n="29.5">venir</w> <w n="29.6">demain</w> <w n="29.7">ou</w> <w n="29.8">l</w>'<w n="29.9">autre</w> <w n="29.10">année</w> ;</l>
					<l n="30" num="5.2"><w n="30.1">Si</w> <w n="30.2">tu</w> <w n="30.3">restes</w> <w n="30.4">encor</w> <w n="30.5">plus</w> <w n="30.6">longtemps</w> <w n="30.7">condamnée</w>,</l>
					<l n="31" num="5.3"><w n="31.1">Ou</w> <w n="31.2">si</w> <w n="31.3">nous</w> <w n="31.4">ne</w> <w n="31.5">touchons</w> <w n="31.6">au</w> <w n="31.7">but</w> <w n="31.8">que</w> <w n="31.9">dans</w> <w n="31.10">cinq</w> <w n="31.11">ans</w>,</l>
					<l n="32" num="5.4"><w n="32.1">Nous</w> <w n="32.2">inculquerons</w> <w n="32.3">tous</w> <w n="32.4">la</w> <w n="32.5">haine</w> <w n="32.6">à</w> <w n="32.7">nos</w> <w n="32.8">enfants</w> !</l>
					<l n="33" num="5.5"><w n="33.1">Ils</w> <w n="33.2">apprendront</w> <w n="33.3">à</w> <w n="33.4">lire</w> <w n="33.5">en</w> <w n="33.6">lisant</w> <w n="33.7">tes</w> <w n="33.8">désastres</w> !</l>
					<l n="34" num="5.6"><w n="34.1">Et</w> <w n="34.2">tout</w>, <w n="34.3">l</w>'<w n="34.4">homme</w>, <w n="34.5">les</w> <w n="34.6">fleurs</w>, <w n="34.7">l</w>'<w n="34.8">Océan</w> <w n="34.9">et</w> <w n="34.10">les</w> <w n="34.11">astres</w>,</l>
					<l n="35" num="5.7"><w n="35.1">Tout</w> <w n="35.2">depuis</w> <w n="35.3">l</w>'<w n="35.4">être</w> <w n="35.5">humain</w> <w n="35.6">qui</w> <w n="35.7">respire</w> <w n="35.8">et</w> <w n="35.9">qui</w> <w n="35.10">sent</w>,</l>
					<l n="36" num="5.8"><w n="36.1">Jusqu</w>'<w n="36.2">à</w> <w n="36.3">la</w> <w n="36.4">chose</w> <w n="36.5">brute</w> <w n="36.6">et</w> <w n="36.7">l</w>'<w n="36.8">objet</w> <w n="36.9">impuissant</w>,</l>
					<l n="37" num="5.9"><w n="37.1">Afin</w> <w n="37.2">de</w> <w n="37.3">concourir</w> <w n="37.4">à</w> <w n="37.5">la</w> <w n="37.6">tâche</w> <w n="37.7">inhumaine</w>,</l>
					<l n="38" num="5.10"><w n="38.1">Tout</w> <w n="38.2">payera</w> <w n="38.3">son</w> <w n="38.4">tribut</w> <w n="38.5">à</w> <w n="38.6">notre</w> <w n="38.7">œuvre</w> <w n="38.8">de</w> <w n="38.9">haine</w> !</l>
				</lg>
				<lg n="6">
					<l n="39" num="6.1"><w n="39.1">Mais</w> <w n="39.2">lorsque</w> <w n="39.3">nous</w> <w n="39.4">aurons</w> <w n="39.5">assez</w> <w n="39.6">longtemps</w> <w n="39.7">vécu</w></l>
					<l n="40" num="6.2"><w n="40.1">Pour</w> <w n="40.2">rendre</w> <w n="40.3">sa</w> <w n="40.4">vigueur</w> <w n="40.5">à</w> <w n="40.6">ton</w> <w n="40.7">peuple</w> <w n="40.8">vaincu</w>,</l>
					<l n="41" num="6.3">— <w n="41.1">France</w>, <w n="41.2">pardonne</w>-<w n="41.3">moi</w> ! — <w n="41.4">pour</w> <w n="41.5">châtier</w> <w n="41.6">leurs</w> <w n="41.7">crimes</w>,</l>
					<l n="42" num="6.4"><w n="42.1">Je</w> <w n="42.2">n</w>'<w n="42.3">aurai</w> <w n="42.4">qu</w>'<w n="42.5">à</w> <w n="42.6">songer</w> <w n="42.7">aux</w> <w n="42.8">deux</w> <w n="42.9">pauvres</w> <w n="42.10">victimes</w>,</l>
					<l n="43" num="6.5"><w n="43.1">Car</w> <w n="43.2">ces</w> <w n="43.3">deux</w> <w n="43.4">innocents</w> <w n="43.5">que</w> <w n="43.6">j</w>'<w n="43.7">ai</w> <w n="43.8">vus</w> <w n="43.9">massacrer</w></l>
					<l n="44" num="6.6"><w n="44.1">M</w>'<w n="44.2">apprendront</w> <w n="44.3">à</w> <w n="44.4">haïr</w> <w n="44.5">pour</w> <w n="44.6">m</w>'<w n="44.7">avoir</w> <w n="44.8">fait</w> <w n="44.9">pleurer</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Paris, 19 Janvier 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>