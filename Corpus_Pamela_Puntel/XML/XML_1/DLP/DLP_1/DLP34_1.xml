<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP34">
				<head type="number">XXXIV</head>
				<head type="main">LA RECONNAISSANCE D'UN PEUPLE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">courant</w> <w n="1.3">est</w> <w n="1.4">venu</w> : <w n="1.5">tout</w> <w n="1.6">un</w> <w n="1.7">peuple</w> <w n="1.8">est</w> <w n="1.9">debout</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">langage</w> <w n="2.3">nouveau</w> <w n="2.4">fait</w> <w n="2.5">résonner</w> <w n="2.6">partout</w></l>
					<l n="3" num="1.3"><w n="3.1">Ces</w> <w n="3.2">vieux</w> <w n="3.3">mots</w> <w n="3.4">de</w> <w n="3.5">croisade</w> <w n="3.6">et</w> <w n="3.7">de</w> <w n="3.8">guerre</w> <w n="3.9">sacrée</w></l>
					<l n="4" num="1.4"><w n="4.1">Par</w> <w n="4.2">qui</w> <w n="4.3">Jérusalem</w> <w n="4.4">un</w> <w n="4.5">jour</w> <w n="4.6">fut</w> <w n="4.7">délivrée</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">tous</w> <w n="5.3">les</w> <w n="5.4">chevaliers</w> <w n="5.5">français</w> <w n="5.6">levant</w> <w n="5.7">les</w> <w n="5.8">yeux</w></l>
					<l n="6" num="1.6"><w n="6.1">Voient</w> <w n="6.2">frissonner</w> <w n="6.3">la</w> <w n="6.4">vieille</w> <w n="6.5">armure</w> <w n="6.6">des</w> <w n="6.7">aïeux</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Louis</w> <w n="7.2">XVI</w> <w n="7.3">est</w> <w n="7.4">le</w> <w n="7.5">roi</w> ? <w n="7.6">qu</w>'<w n="7.7">importe</w> <w n="7.8">un</w> <w n="7.9">roi</w> <w n="7.10">de</w> <w n="7.11">France</w> !</l>
					<l n="8" num="2.2"><w n="8.1">La</w> <w n="8.2">croisade</w> <w n="8.3">moderne</w> <w n="8.4">est</w> <w n="8.5">là</w>-<w n="8.6">bas</w>, <w n="8.7">et</w> <w n="8.8">commence</w> !</l>
					<l n="9" num="2.3"><w n="9.1">Ah</w> ! <w n="9.2">ce</w> <w n="9.3">n</w>'<w n="9.4">est</w> <w n="9.5">plus</w> <w n="9.6">le</w> <w n="9.7">Christ</w> <w n="9.8">dont</w> <w n="9.9">ils</w> <w n="9.10">portent</w> <w n="9.11">la</w> <w n="9.12">croix</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Ce</w> <w n="10.2">n</w>'<w n="10.3">est</w> <w n="10.4">plus</w> <w n="10.5">le</w> <w n="10.6">tombeau</w> <w n="10.7">de</w> <w n="10.8">leur</w> <w n="10.9">Dieu</w>, <w n="10.10">cette</w> <w n="10.11">fois</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Qui</w> <w n="11.2">fait</w> <w n="11.3">bondir</w> <w n="11.4">ces</w> <w n="11.5">cœurs</w> <w n="11.6">et</w> <w n="11.7">prendre</w> <w n="11.8">ces</w> <w n="11.9">épées</w>.</l>
					<l n="12" num="2.6"><w n="12.1">Plume</w> <w n="12.2">dont</w> <w n="12.3">le</w> <w n="12.4">héros</w> <w n="12.5">écrit</w> <w n="12.6">ses</w> <w n="12.7">épopées</w> !</l>
					<l n="13" num="2.7"><w n="13.1">Non</w> ! <w n="13.2">la</w> <w n="13.3">voix</w> <w n="13.4">qui</w> <w n="13.5">leur</w> <w n="13.6">parle</w> <w n="13.7">est</w> <w n="13.8">la</w> <w n="13.9">voix</w> <w n="13.10">du</w> <w n="13.11">canon</w></l>
					<l n="14" num="2.8"><w n="14.1">Et</w> <w n="14.2">d</w>'<w n="14.3">un</w> <w n="14.4">peuple</w>, <w n="14.5">dont</w> <w n="14.6">hier</w> <w n="14.7">ils</w> <w n="14.8">ignoraient</w> <w n="14.9">le</w> <w n="14.10">nom</w> :</l>
					<l n="15" num="2.9"><w n="15.1">Mais</w> <w n="15.2">elle</w> <w n="15.3">parle</w> <w n="15.4">haut</w> <w n="15.5">une</w> <w n="15.6">langue</w> <w n="15.7">inconnue</w></l>
					<l n="16" num="2.10"><w n="16.1">Qui</w> <w n="16.2">fendant</w> <w n="16.3">l</w>'<w n="16.4">Océan</w> <w n="16.5">d</w>'<w n="16.6">un</w> <w n="16.7">vol</w> <w n="16.8">leur</w> <w n="16.9">est</w> <w n="16.10">venue</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Ils</w> <w n="17.2">se</w> <w n="17.3">lèvent</w>, <w n="17.4">manants</w> <w n="17.5">et</w> <w n="17.6">gentilhommes</w>, <w n="17.7">tous</w> !</l>
					<l n="18" num="3.2"><w n="18.1">Celui</w>-<w n="18.2">ci</w> <w n="18.3">prend</w> <w n="18.4">son</w> <w n="18.5">fils</w> <w n="18.6">assis</w> <w n="18.7">sur</w> <w n="18.8">ses</w> <w n="18.9">genoux</w>.</l>
					<l n="19" num="3.3"><w n="19.1">Et</w> <w n="19.2">lui</w> <w n="19.3">dit</w>, <w n="19.4">en</w> <w n="19.5">s</w>'<w n="19.6">offrant</w> <w n="19.7">lui</w>-<w n="19.8">même</w> <w n="19.9">en</w> <w n="19.10">sacrifice</w>.</l>
					<l n="20" num="3.4">— <w n="20.1">Moi</w>, <w n="20.2">je</w> <w n="20.3">suis</w> <w n="20.4">le</w> <w n="20.5">soldat</w> <w n="20.6">croisé</w> <w n="20.7">pour</w> <w n="20.8">la</w> <w n="20.9">justice</w> !</l>
					<l n="21" num="3.5"><w n="21.1">Cet</w> <w n="21.2">autre</w> <w n="21.3">est</w> <w n="21.4">marié</w> <w n="21.5">de</w> <w n="21.6">huit</w> <w n="21.7">jours</w> <w n="21.8">seulement</w>…</l>
					<l n="22" num="3.6">— <w n="22.1">Oh</w> ! <w n="22.2">si</w> <w n="22.3">tôt</w> ! <w n="22.4">quand</w> <w n="22.5">l</w>'<w n="22.6">époux</w> <w n="22.7">est</w> <w n="22.8">encore</w> <w n="22.9">un</w> <w n="22.10">amant</w> ! —</l>
					<l n="23" num="3.7"><w n="23.1">Mais</w> <w n="23.2">l</w>'<w n="23.3">amour</w> <w n="23.4">vient</w> <w n="23.5">après</w> <w n="23.6">cette</w> <w n="23.7">voix</w> <w n="23.8">éternelle</w> :</l>
					<l n="24" num="3.8">— <w n="24.1">Je</w> <w n="24.2">suis</w> <w n="24.3">le</w> <w n="24.4">paladin</w> <w n="24.5">que</w> <w n="24.6">tout</w> <w n="24.7">un</w> <w n="24.8">peuple</w> <w n="24.9">appelle</w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">El</w> <w n="25.2">tous</w> <w n="25.3">se</w> <w n="25.4">sont</w> <w n="25.5">levés</w>, <w n="25.6">et</w> <w n="25.7">tous</w> <w n="25.8">ils</w> <w n="25.9">sont</w> <w n="25.10">partis</w> !</l>
				</lg>
				<lg n="5">
					<l n="26" num="5.1"><w n="26.1">Ils</w> <w n="26.2">arrivent</w>… <w n="26.3">grand</w> <w n="26.4">Dieu</w> ! <w n="26.5">quels</w> <w n="26.6">tourments</w> <w n="26.7">ressentis</w>,</l>
					<l n="27" num="5.2"><w n="27.1">Pendant</w> <w n="27.2">cette</w> <w n="27.3">effrayante</w> <w n="27.4">et</w> <w n="27.5">rude</w> <w n="27.6">traversée</w> !</l>
				</lg>
				<lg n="6">
					<l n="28" num="6.1"><w n="28.1">S</w>'<w n="28.2">ils</w> <w n="28.3">avaient</w> <w n="28.4">pu</w> <w n="28.5">combattre</w> <w n="28.6">au</w> <w n="28.7">moins</w> <w n="28.8">par</w> <w n="28.9">la</w> <w n="28.10">pensée</w> !</l>
					<l n="29" num="6.2"><w n="29.1">Ils</w> <w n="29.2">arrivent</w>… <w n="29.3">Enfin</w> ! <w n="29.4">Pourquoi</w> ? <w n="29.5">pour</w> <w n="29.6">conquérir</w> ?</l>
					<l n="30" num="6.3"><w n="30.1">Non</w> ! <w n="30.2">pour</w> <w n="30.3">sauver</w> <w n="30.4">un</w> <w n="30.5">peuple</w>, <w n="30.6">ou</w> <w n="30.7">sinon</w> <w n="30.8">pour</w> <w n="30.9">mourir</w> !</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><w n="31.1">Dites</w> <w n="31.2">à</w> <w n="31.3">Washington</w> <w n="31.4">de</w> <w n="31.5">relever</w> <w n="31.6">la</w> <w n="31.7">tête</w> :</l>
					<l n="32" num="7.2"><w n="32.1">C</w>'<w n="32.2">est</w> <w n="32.3">l</w>'<w n="32.4">âme</w> <w n="32.5">de</w> <w n="32.6">la</w> <w n="32.7">France</w> <w n="32.8">aux</w> <w n="32.9">mains</w> <w n="32.10">de</w> <w n="32.11">Lafayette</w> !</l>
				</lg>
				<lg n="8">
					<l n="33" num="8.1"><w n="33.1">Sabre</w> <w n="33.2">au</w> <w n="33.3">vent</w> ! <w n="33.4">déployez</w> <w n="33.5">l</w>'<w n="33.6">oriflamme</w> <w n="33.7">au</w> <w n="33.8">lys</w> <w n="33.9">blanc</w> !</l>
					<l n="34" num="8.2"><w n="34.1">Montjoie</w>-<w n="34.2">Sainl</w>-<w n="34.3">Denis</w> <w n="34.4">s</w>'<w n="34.5">unit</w> <w n="34.6">à</w> <w n="34.7">Fatherland</w> !</l>
					<l n="35" num="8.3"><w n="35.1">Au</w> <w n="35.2">galop</w> ! <w n="35.3">au</w> <w n="35.4">galop</w> <w n="35.5">à</w> <w n="35.6">travers</w> <w n="35.7">la</w> <w n="35.8">savane</w></l>
					<l n="36" num="8.4"><w n="36.1">Que</w> <w n="36.2">suit</w>, <w n="36.3">l</w>'<w n="36.4">orgueil</w> <w n="36.5">au</w> <w n="36.6">cœur</w>, <w n="36.7">la</w> <w n="36.8">grande</w> <w n="36.9">caravane</w> !</l>
					<l n="37" num="8.5"><w n="37.1">Elle</w> <w n="37.2">va</w> <w n="37.3">dans</w> <w n="37.4">son</w> <w n="37.5">sang</w> <w n="37.6">qui</w> <w n="37.7">ruisselle</w> <w n="37.8">partout</w> ;</l>
					<l n="38" num="8.6"><w n="38.1">Sachant</w> <w n="38.2">quel</w> <w n="38.3">est</w> <w n="38.4">le</w> <w n="38.5">prix</w> <w n="38.6">que</w> <w n="38.7">Dieu</w> <w n="38.8">lui</w> <w n="38.9">garde</w> <w n="38.10">au</w> <w n="38.11">bout</w>,</l>
					<l n="39" num="8.7"><w n="39.1">Car</w> <w n="39.2">elle</w> <w n="39.3">voit</w> <w n="39.4">au</w> <w n="39.5">loin</w>, <w n="39.6">chaque</w> <w n="39.7">fois</w> <w n="39.8">qu</w>'<w n="39.9">elle</w> <w n="39.10">avance</w>.</l>
					<l n="40" num="8.8"><w n="40.1">Le</w> <w n="40.2">jour</w> <w n="40.3">étincelant</w> <w n="40.4">d</w>'<w n="40.5">un</w> <w n="40.6">peuple</w> <w n="40.7">qui</w> <w n="40.8">commence</w> !</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<l n="41" num="8.9"><w n="41.1">Près</w> <w n="41.2">d</w>'<w n="41.3">un</w> <w n="41.4">siècle</w> <w n="41.5">a</w> <w n="41.6">passé</w> : <w n="41.7">ce</w> <w n="41.8">peuple</w> <w n="41.9">est</w> <w n="41.10">grand</w> <w n="41.11">et</w> <w n="41.12">fort</w>.</l>
				</lg>
				<lg n="9">
					<l n="42" num="9.1"><w n="42.1">Oh</w> ! <w n="42.2">combien</w> <w n="42.3">de</w> <w n="42.4">Français</w> <w n="42.5">ont</w> <w n="42.6">rencontré</w> <w n="42.7">la</w> <w n="42.8">mort</w> !</l>
					<l n="43" num="9.2"><w n="43.1">Dans</w> <w n="43.2">la</w> <w n="43.3">campagne</w>, <w n="43.4">au</w> <w n="43.5">fond</w> <w n="43.6">du</w> <w n="43.7">bois</w> <w n="43.8">sonore</w> <w n="43.9">et</w> <w n="43.10">sombre</w>,</l>
					<l n="44" num="9.3"><w n="44.1">J</w>'<w n="44.2">imagine</w> <w n="44.3">qu</w>'<w n="44.4">on</w> <w n="44.5">voit</w> <w n="44.6">se</w> <w n="44.7">promener</w> <w n="44.8">leur</w> <w n="44.9">ombre</w>,</l>
					<l n="45" num="9.4"><w n="45.1">Qui</w> <w n="45.2">pensive</w>, <w n="45.3">accoudée</w> <w n="45.4">au</w> <w n="45.5">bord</w> <w n="45.6">de</w> <w n="45.7">son</w> <w n="45.8">tombeau</w>,</l>
					<l n="46" num="9.5"><w n="46.1">Et</w> <w n="46.2">songeant</w> <w n="46.3">au</w> <w n="46.4">passé</w>, <w n="46.5">songeant</w> <w n="46.6">à</w> <w n="46.7">Rochambeau</w>,</l>
					<l n="47" num="9.6"><w n="47.1">Songeant</w> <w n="47.2">à</w> <w n="47.3">tous</w> <w n="47.4">enfin</w>, <w n="47.5">dit</w> <w n="47.6">en</w> <w n="47.7">levant</w> <w n="47.8">la</w> <w n="47.9">tête</w> :</l>
					<l n="48" num="9.7">— <w n="48.1">Voilà</w> <w n="48.2">l</w>’<w n="48.3">œuvre</w> <w n="48.4">pourtant</w> <w n="48.5">que</w> <w n="48.6">notre</w> <w n="48.7">sang</w> <w n="48.8">a</w> <w n="48.9">faite</w> !</l>
				</lg>
				<lg n="10">
					<l n="49" num="10.1"><w n="49.1">Puis</w> <w n="49.2">elle</w> <w n="49.3">entend</w> <w n="49.4">au</w> <w n="49.5">loin</w> <w n="49.6">une</w> <w n="49.7">voix</w> <w n="49.8">dans</w> <w n="49.9">le</w> <w n="49.10">ciel</w>…</l>
					<l n="50" num="10.2"><w n="50.1">C</w>'<w n="50.2">est</w> <w n="50.3">la</w> <w n="50.4">France</w> <w n="50.5">qui</w> <w n="50.6">meurt</w> <w n="50.7">et</w> <w n="50.8">qui</w> <w n="50.9">bat</w> <w n="50.10">le</w> <w n="50.11">rappel</w>,</l>
					<l n="51" num="10.3"><w n="51.1">De</w> <w n="51.2">ses</w> <w n="51.3">enfants</w>, <w n="51.4">de</w> <w n="51.5">ceux</w> <w n="51.6">qu</w>'<w n="51.7">elle</w> <w n="51.8">a</w> <w n="51.9">voulu</w> <w n="51.10">défendre</w>…</l>
					<l n="52" num="10.4"><w n="52.1">Alors</w>, <w n="52.2">le</w> <w n="52.3">vieux</w> <w n="52.4">héros</w> <w n="52.5">se</w> <w n="52.6">penche</w> <w n="52.7">pour</w> <w n="52.8">entendre</w></l>
					<l n="53" num="10.5"><w n="53.1">Si</w> <w n="53.2">rien</w> <w n="53.3">ne</w> <w n="53.4">va</w> <w n="53.5">répondre</w> <w n="53.6">à</w> <w n="53.7">ce</w> <w n="53.8">sanglot</w> <w n="53.9">profond</w>…</l>
					<l n="54" num="10.6"><w n="54.1">Allons</w> <w n="54.2">donc</w> ! <w n="54.3">Washington</w> <w n="54.4">est</w> <w n="54.5">mort</w>, <w n="54.6">rien</w> <w n="54.7">ne</w> <w n="54.8">répond</w> !</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<l n="55" num="10.7"><w n="55.1">A</w> <w n="55.2">genoux</w> <w n="55.3">sur</w> <w n="55.4">ton</w> <w n="55.5">sol</w> <w n="55.6">où</w> <w n="55.7">toujours</w> <w n="55.8">le</w> <w n="55.9">sang</w> <w n="55.10">monte</w>,</l>
					<l n="56" num="10.8"><w n="56.1">Comme</w> <w n="56.2">un</w> <w n="56.3">maudit</w> <w n="56.4">qui</w> <w n="56.5">pleure</w> <w n="56.6">en</w> <w n="56.7">regardant</w> <w n="56.8">sa</w> <w n="56.9">honte</w>,</l>
					<l n="57" num="10.9"><w n="57.1">Moi</w>, <w n="57.2">libre</w> <w n="57.3">citoyen</w> <w n="57.4">du</w> <w n="57.5">peuple</w> <w n="57.6">américain</w>,</l>
					<l n="58" num="10.10"><w n="58.1">Qui</w> <w n="58.2">ne</w> <w n="58.3">s</w>'<w n="58.4">est</w> <w n="58.5">pas</w> <w n="58.6">penché</w> <w n="58.7">pour</w> <w n="58.8">te</w> <w n="58.9">tendre</w> <w n="58.10">la</w> <w n="58.11">main</w>,</l>
					<l n="59" num="10.11"><w n="59.1">Et</w> <w n="59.2">reste</w> <w n="59.3">à</w> <w n="59.4">contempler</w> <w n="59.5">tes</w> <w n="59.6">malheurs</w> <w n="59.7">qui</w> <w n="59.8">grandissent</w>,</l>
					<l n="60" num="10.12"><w n="60.1">Moi</w>, <w n="60.2">certain</w> <w n="60.3">que</w> <w n="60.4">là</w>-<w n="60.5">bas</w> <w n="60.6">les</w> <w n="60.7">cœurs</w> <w n="60.8">fiers</w> <w n="60.9">m</w>'<w n="60.10">applaudissent</w>,</l>
					<l n="61" num="10.13"><w n="61.1">France</w>, <w n="61.2">du</w> <w n="61.3">sang</w> <w n="61.4">de</w> <w n="61.5">ceux</w> <w n="61.6">dont</w> <w n="61.7">tu</w> <w n="61.8">nous</w> <w n="61.9">as</w> <w n="61.10">fait</w> <w n="61.11">don</w>,</l>
				</lg>
				<lg n="11">
					<l n="62" num="11.1"><w n="62.1">Je</w> <w n="62.2">viens</w> <w n="62.3">très</w>-<w n="62.4">humblement</w> <w n="62.5">te</w> <w n="62.6">demander</w> <w n="62.7">pardon</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Paris, 22 Février 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>