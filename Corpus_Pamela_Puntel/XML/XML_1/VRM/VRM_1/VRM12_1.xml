<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES DOULEURS DE LA GUERRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VRM">
					<name>
						<forename>Louis-Lucien</forename>
						<surname>VERMEIL</surname>
					</name>
					<date from="1833" to="1901">1833-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>620 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VRM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES DOULEURS DE LA GUERRE</title>
						<author>LOUIS-LUCIEN VERMEIL</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=l7xDAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES DOULEURS DE LA GUERRE</title>
								<author>LOUIS-LUCIEN VERMEIL</author>
								<imprint>
									<pubPlace>LAUSANNE</pubPlace>
									<publisher>LIBRAIRIE BLANC IMER et LEBET</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VRM12">
				<head type="number">XII</head>
				<head type="main">LA GRANDEUR DE JÉSUS<lb></lb>ET LES GRANDEURS HUMAINES<ref target="1" type="noteAnchor">1</ref></head>
				<opener>
					<epigraph>
						<cit>
							<quote>Il sera grand…</quote>
							<bibl>
								<name>— Luc., 1, 15.</name>.
							</bibl>
						</cit>
						<cit>
							<quote>Il a reçu un nom qui est au dessus de tout autre nom…</quote>
							<bibl>
								<name>Philip., II, 9.</name>.
							</bibl>
						</cit>
						<cit>
							<quote>Le nom de Jésus…</quote>
							<bibl>
								<name>Math., 1, 21.</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Comparez</w> <w n="1.2">à</w> <w n="1.3">ce</w> <w n="1.4">nom</w> <w n="1.5">tous</w> <w n="1.6">les</w> <w n="1.7">noms</w> <w n="1.8">de</w> <w n="1.9">la</w> <w n="1.10">terre</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Tous</w> <w n="2.2">les</w> <w n="2.3">titres</w> <w n="2.4">pompeux</w> <w n="2.5">de</w> <w n="2.6">notre</w> <w n="2.7">vanité</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>'<w n="3.2">ils</w> <w n="3.3">sont</w> <w n="3.4">petits</w> <w n="3.5">ces</w> <w n="3.6">noms</w> <w n="3.7">d</w>'<w n="3.8">un</w> <w n="3.9">éclat</w> <w n="3.10">éphémère</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Seul</w> <w n="4.2">il</w> <w n="4.3">est</w> <w n="4.4">grand</w> <w n="4.5">le</w> <w n="4.6">Fils</w> <w n="4.7">de</w> <w n="4.8">l</w>'<w n="4.9">immortalité</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Noms</w> <w n="5.2">fameux</w> <w n="5.3">des</w> <w n="5.4">héros</w> <w n="5.5">que</w> <w n="5.6">célèbre</w> <w n="5.7">l</w>'<w n="5.8">histoire</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Comme</w> <w n="6.2">vous</w> <w n="6.3">pâlissez</w> <w n="6.4">auprès</w> <w n="6.5">de</w> <w n="6.6">ce</w> <w n="6.7">doux</w> <w n="6.8">nom</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Jésus</w> <w n="7.2">grandit</w> <w n="7.3">encore</w> <w n="7.4">et</w> <w n="7.5">va</w> <w n="7.6">de</w> <w n="7.7">gloire</w> <w n="7.8">en</w> <w n="7.9">gloire</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">Mais</w> <w n="8.2">vous</w> <w n="8.3">disparaissez</w>, <w n="8.4">comme</w> <w n="8.5">votre</w> <w n="8.6">renom</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Monarques</w>, <w n="9.2">conquérants</w>, <w n="9.3">puissants</w> <w n="9.4">hommes</w> <w n="9.5">de</w> <w n="9.6">guerre</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Vous</w> <w n="10.2">qui</w> <w n="10.3">fîtes</w> <w n="10.4">trembler</w> <w n="10.5">maintes</w> <w n="10.6">fois</w> <w n="10.7">l</w>'<w n="10.8">univers</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Vous</w> <w n="11.2">qui</w> <w n="11.3">traciez</w> <w n="11.4">vos</w> <w n="11.5">noms</w> <w n="11.6">sur</w> <w n="11.7">le</w> <w n="11.8">bronze</w> <w n="11.9">ou</w> <w n="11.10">la</w> <w n="11.11">pierre</w>,</l>
					<l n="12" num="3.4"><w n="12.1">A</w> <w n="12.2">peine</w> <w n="12.3">on</w> <w n="12.4">se</w> <w n="12.5">souvient</w> <w n="12.6">même</w> <w n="12.7">de</w> <w n="12.8">vos</w> <w n="12.9">revers</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Vos</w> <w n="13.2">noms</w> <w n="13.3">sont</w> <w n="13.4">effacés</w>, <w n="13.5">vos</w> <w n="13.6">grandeurs</w> <w n="13.7">abattues</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Alexandre</w> <w n="14.2">ou</w> <w n="14.3">César</w>, <w n="14.4">votre</w> <w n="14.5">règne</w> <w n="14.6">n</w>'<w n="14.7">est</w> <w n="14.8">rien</w> !</l>
					<l n="15" num="4.3"><w n="15.1">Le</w> <w n="15.2">temps</w> <w n="15.3">a</w> <w n="15.4">tout</w> <w n="15.5">détruit</w>, <w n="15.6">vos</w> <w n="15.7">palais</w>, <w n="15.8">Vos</w> <w n="15.9">statues</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Vos</w> <w n="16.2">cités</w> <w n="16.3">ne</w> <w n="16.4">sont</w> <w n="16.5">plus</w>, <w n="16.6">comme</w> <w n="16.7">au</w> <w n="16.8">sol</w> <w n="16.9">assyrien</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Il</w> <w n="17.2">faut</w> <w n="17.3">fouiller</w> <w n="17.4">longtemps</w> <w n="17.5">pour</w> <w n="17.6">trouver</w> <w n="17.7">Babylone</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Ou</w> <w n="18.2">Ninive</w>, <w n="18.3">ou</w> <w n="18.4">Carthage</w>, <w n="18.5">ou</w> <w n="18.6">les</w> <w n="18.7">vieilles</w> <w n="18.8">cités</w> ;</l>
					<l n="19" num="5.3"><w n="19.1">En</w> <w n="19.2">vain</w> <w n="19.3">fouillerait</w>-<w n="19.4">on</w> <w n="19.5">pour</w> <w n="19.6">retrouver</w> <w n="19.7">un</w> <w n="19.8">trône</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Car</w> <w n="20.2">ils</w> <w n="20.3">sont</w> <w n="20.4">vermoulus</w> <w n="20.5">avant</w> <w n="20.6">d</w>'<w n="20.7">être</w> <w n="20.8">quittés</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Bien</w> <w n="21.2">fragile</w> <w n="21.3">est</w>, <w n="21.4">hélas</w> ! <w n="21.5">la</w> <w n="21.6">splendeur</w> <w n="21.7">d</w>'<w n="21.8">un</w> <w n="21.9">empire</w> !</l>
					<l n="22" num="6.2"><w n="22.1">Que</w> <w n="22.2">de</w> <w n="22.3">sceptres</w> <w n="22.4">brisés</w> ! — <w n="22.5">Leur</w> <w n="22.6">prestige</w> <w n="22.7">est</w> <w n="22.8">détruit</w> !</l>
					<l n="23" num="6.3"><w n="23.1">Un</w> <w n="23.2">craquement</w> <w n="23.3">lugubre</w>… <w n="23.4">et</w> <w n="23.5">ta</w> <w n="23.6">puissance</w> <w n="23.7">expire</w>,</l>
					<l n="24" num="6.4"><w n="24.1">O</w> <w n="24.2">monarque</w> <w n="24.3">orgueilleux</w> <w n="24.4">qui</w> <w n="24.5">faisais</w> <w n="24.6">tant</w> <w n="24.7">de</w> <w n="24.8">bruit</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Nous</w> <w n="25.2">en</w> <w n="25.3">sommes</w> <w n="25.4">témoins</w> : <w n="25.5">un</w> <w n="25.6">empire</w> <w n="25.7">s</w>'<w n="25.8">écroule</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Et</w> <w n="26.2">le</w> <w n="26.3">peuple</w> <w n="26.4">applaudit</w>, <w n="26.5">il</w> <w n="26.6">voit</w> <w n="26.7">tomber</w> <w n="26.8">ses</w> <w n="26.9">fers</w> !</l>
					<l n="27" num="7.3"><w n="27.1">Le</w> <w n="27.2">trône</w> <w n="27.3">est</w> <w n="27.4">emporté</w> <w n="27.5">sur</w> <w n="27.6">la</w> <w n="27.7">vague</w> <w n="27.8">qui</w> <w n="27.9">roule</w>…</l>
					<l n="28" num="7.4"><w n="28.1">C</w>'<w n="28.2">est</w> <w n="28.3">l</w>'<w n="28.4">Océan</w> <w n="28.5">humain</w>. <w n="28.6">Que</w> <w n="28.7">ces</w> <w n="28.8">flots</w> <w n="28.9">sont</w> <w n="28.10">amers</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Ses</w> <w n="29.2">flots</w> <w n="29.3">en</w> <w n="29.4">écumant</w> <w n="29.5">rejaillissent</w> <w n="29.6">sur</w> <w n="29.7">Rome</w> !</l>
					<l n="30" num="8.2"><w n="30.1">Au</w> <w n="30.2">milieu</w> <w n="30.3">des</w> <w n="30.4">débris</w> <w n="30.5">de</w> <w n="30.6">l</w>'<w n="30.7">antique</w> <w n="30.8">cité</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Là</w> <w n="31.2">se</w> <w n="31.3">trouve</w> <w n="31.4">un</w> <w n="31.5">vieillard</w>, <w n="31.6">un</w> <w n="31.7">grand</w> <w n="31.8">pontife</w>, <w n="31.9">un</w> <w n="31.10">homme</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Qui</w> <w n="32.2">veut</w> <w n="32.3">passer</w> <w n="32.4">pour</w> <w n="32.5">Dieu</w>. <w n="32.6">Quelle</w> <w n="32.7">caducité</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Devant</w> <w n="33.2">ce</w> <w n="33.3">flot</w> <w n="33.4">vengeur</w> <w n="33.5">qui</w> <w n="33.6">mugit</w> <w n="33.7">et</w> <w n="33.8">qui</w> <w n="33.9">monte</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Tremblez</w>, <w n="34.2">tremblez</w>, <w n="34.3">tyrans</w> ; — <w n="34.4">car</w> <w n="34.5">votre</w> <w n="34.6">tour</w> <w n="34.7">viendra</w>.</l>
					<l n="35" num="9.3"><w n="35.1">Les</w> <w n="35.2">peuples</w> <w n="35.3">en</w> <w n="35.4">courroux</w> <w n="35.5">voudront</w> <w n="35.6">noyer</w> <w n="35.7">leur</w> <w n="35.8">honte</w>…</l>
					<l n="36" num="9.4"><w n="36.1">Bientôt</w> <w n="36.2">la</w> <w n="36.3">Liberté</w> <w n="36.4">partout</w> <w n="36.5">triomphera</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Mais</w> <w n="37.2">il</w> <w n="37.3">est</w>, <w n="37.4">je</w> <w n="37.5">le</w> <w n="37.6">sais</w>, <w n="37.7">un</w> <w n="37.8">trône</w> <w n="37.9">de</w> <w n="37.10">Justice</w> ;</l>
					<l n="38" num="10.2"><w n="38.1">Il</w> <w n="38.2">est</w> <w n="38.3">une</w> <w n="38.4">grandeur</w> <w n="38.5">qui</w> <w n="38.6">ne</w> <w n="38.7">doit</w> <w n="38.8">point</w> <w n="38.9">passer</w>,</l>
					<l n="39" num="10.3"><w n="39.1">C</w>'<w n="39.2">est</w> <w n="39.3">ton</w> <w n="39.4">trône</w>, <w n="39.5">ô</w> <w n="39.6">Jésus</w> ! <w n="39.7">celui</w> <w n="39.8">du</w> <w n="39.9">sacrifice</w> ;</l>
					<l n="40" num="10.4"><w n="40.1">Ta</w> <w n="40.2">croix</w>, <w n="40.3">divin</w> <w n="40.4">Sauveur</w>, <w n="40.5">ne</w> <w n="40.6">saurait</w> <w n="40.7">s</w>'<w n="40.8">effacer</w> !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">La</w> <w n="41.2">charité</w> <w n="41.3">du</w> <w n="41.4">Christ</w> ! <w n="41.5">mais</w> <w n="41.6">elle</w> <w n="41.7">est</w> <w n="41.8">éternelle</w> ;</l>
					<l n="42" num="11.2"><w n="42.1">C</w>'<w n="42.2">est</w> <w n="42.3">là</w> <w n="42.4">sa</w> <w n="42.5">gloire</w> <w n="42.6">à</w> <w n="42.7">lui</w> <w n="42.8">que</w> <w n="42.9">rien</w> <w n="42.10">ne</w> <w n="42.11">peut</w> <w n="42.12">ternir</w>.</l>
					<l n="43" num="11.3"><w n="43.1">Sur</w> <w n="43.2">cette</w> <w n="43.3">croix</w> <w n="43.4">maudite</w>, <w n="43.5">ô</w> <w n="43.6">que</w> <w n="43.7">sa</w> <w n="43.8">mort</w> <w n="43.9">est</w> <w n="43.10">belle</w> !</l>
					<l n="44" num="11.4"><w n="44.1">Aussi</w> <w n="44.2">son</w> <w n="44.3">pur</w> <w n="44.4">triomphe</w> <w n="44.5">est</w> <w n="44.6">bien</w> <w n="44.7">loin</w> <w n="44.8">de</w> <w n="44.9">finir</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Tu</w> <w n="45.2">règnes</w>, <w n="45.3">ô</w> <w n="45.4">Jésus</w> ! <w n="45.5">comme</w> <w n="45.6">un</w> <w n="45.7">roi</w> <w n="45.8">débonnaire</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Tu</w> <w n="46.2">règnes</w> <w n="46.3">sans</w> <w n="46.4">verser</w> <w n="46.5">le</w> <w n="46.6">sang</w> <w n="46.7">de</w> <w n="46.8">tes</w> <w n="46.9">sujets</w>.</l>
					<l n="47" num="12.3"><w n="47.1">Le</w> <w n="47.2">sang</w> <w n="47.3">que</w> <w n="47.4">tu</w> <w n="47.5">versas</w>, <w n="47.6">c</w>'<w n="47.7">est</w> <w n="47.8">le</w> <w n="47.9">tien</w> <w n="47.10">sur</w> <w n="47.11">la</w> <w n="47.12">terre</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Le</w> <w n="48.2">tien</w> <w n="48.3">pour</w> <w n="48.4">nous</w> <w n="48.5">sauver</w> ; <w n="48.6">ne</w> <w n="48.7">l</w>'<w n="48.8">oublions</w> <w n="48.9">jamais</w> !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Tu</w> <w n="49.2">règnes</w> <w n="49.3">dans</w> <w n="49.4">le</w> <w n="49.5">ciel</w>, <w n="49.6">d</w>'<w n="49.7">où</w> <w n="49.8">tu</w> <w n="49.9">vois</w> <w n="49.10">à</w> <w n="49.11">cette</w> <w n="49.12">heure</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Ce</w> <w n="50.2">monde</w> <w n="50.3">tout</w> <w n="50.4">couvert</w> <w n="50.5">de</w> <w n="50.6">carnage</w> <w n="50.7">et</w> <w n="50.8">de</w> <w n="50.9">sang</w> !</l>
					<l n="51" num="13.3"><w n="51.1">Tu</w> <w n="51.2">règnes</w>, <w n="51.3">ô</w> <w n="51.4">Seigneur</w> ! — <w n="51.5">Du</w> <w n="51.6">lieu</w> <w n="51.7">de</w> <w n="51.8">ta</w> <w n="51.9">demeure</w>,</l>
					<l n="52" num="13.4"><w n="52.1">Accorde</w>-<w n="52.2">nous</w> <w n="52.3">ta</w> <w n="52.4">grâce</w> <w n="52.5">et</w> <w n="52.6">ton</w> <w n="52.7">secours</w> <w n="52.8">puissant</w> !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">La</w> <w n="53.2">paix</w>, <w n="53.3">divin</w> <w n="53.4">Jésus</w> ! <w n="53.5">la</w> <w n="53.6">paix</w> <w n="53.7">entre</w> <w n="53.8">les</w> <w n="53.9">frères</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Oui</w>, <w n="54.2">la</w> <w n="54.3">paix</w> <w n="54.4">entre</w> <w n="54.5">ceux</w> <w n="54.6">qui</w> <w n="54.7">t</w>'<w n="54.8">appellent</w> <w n="54.9">Sauveur</w> !</l>
					<l n="55" num="14.3"><w n="55.1">O</w>, <w n="55.2">peuples</w>, <w n="55.3">calmez</w>-<w n="55.4">vous</w>, <w n="55.5">regagnez</w> <w n="55.6">vos</w> <w n="55.7">frontières</w> ;</l>
					<l n="56" num="14.4"><w n="56.1">Il</w> <w n="56.2">est</w> <w n="56.3">une</w> <w n="56.4">autre</w> <w n="56.5">loi</w> <w n="56.6">que</w> <w n="56.7">la</w> <w n="56.8">loi</w> <w n="56.9">du</w> <w n="56.10">Vainqueur</w> ;</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">C</w>'<w n="57.2">est</w> <w n="57.3">la</w> <w n="57.4">Loi</w> <w n="57.5">de</w> <w n="57.6">Jésus</w>, <w n="57.7">la</w> <w n="57.8">Loi</w> <w n="57.9">de</w> <w n="57.10">l</w>’<w n="57.11">Évangile</w>,</l>
					<l n="58" num="15.2"><w n="58.1">Loi</w> <w n="58.2">qui</w> <w n="58.3">vaut</w> <w n="58.4">beaucoup</w> <w n="58.5">mieux</w> <w n="58.6">que</w> <w n="58.7">la</w> <w n="58.8">loi</w> <w n="58.9">du</w> <w n="58.10">plus</w> <w n="58.11">fort</w> !</l>
					<l n="59" num="15.3"><w n="59.1">Rebelle</w> <w n="59.2">à</w> <w n="59.3">cette</w> <w n="59.4">Loi</w>, <w n="59.5">colosse</w> <w n="59.6">aux</w> <w n="59.7">pieds</w> <w n="59.8">d</w>'<w n="59.9">argile</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Le</w> <w n="60.2">vainqueur</w> <w n="60.3">disparaît</w> <w n="60.4">sous</w> <w n="60.5">un</w> <w n="60.6">souffle</w> <w n="60.7">de</w> <w n="60.8">mort</w> !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">O</w> <w n="61.2">Prince</w> <w n="61.3">de</w> <w n="61.4">la</w> <w n="61.5">Paix</w> ! <w n="61.6">humble</w> <w n="61.7">roi</w> <w n="61.8">débonnaire</w>,</l>
					<l n="62" num="16.2"><w n="62.1">Attire</w>-<w n="62.2">nous</w> <w n="62.3">à</w> <w n="62.4">toi</w> <w n="62.5">par</w> <w n="62.6">des</w> <w n="62.7">liens</w> <w n="62.8">d</w>'<w n="62.9">amour</w>.</l>
					<l n="63" num="16.3"><w n="63.1">Apaise</w> <w n="63.2">du</w> <w n="63.3">guerrier</w> <w n="63.4">la</w> <w n="63.5">fureur</w> <w n="63.6">sanguinaire</w> ;</l>
					<l n="64" num="16.4"><w n="64.1">Que</w> <w n="64.2">ton</w> <w n="64.3">règne</w> <w n="64.4">ici</w>-<w n="64.5">bas</w> <w n="64.6">grandisse</w>, <w n="64.7">dès</w> <w n="64.8">ce</w> <w n="64.9">jour</w> !</l>
				</lg>
				<closer>
					<note type="footnote" id="1">
						Ce morceau a déjà paru dans le Journal évangélique ; mais l'auteur a pensé
						qu'il pouvait encore trouver sa place à la fin de ce petit poème, comme l'ode finale.
					</note>
				</closer>
			</div></body></text></TEI>