<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES DOULEURS DE LA GUERRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VRM">
					<name>
						<forename>Louis-Lucien</forename>
						<surname>VERMEIL</surname>
					</name>
					<date from="1833" to="1901">1833-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>620 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VRM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES DOULEURS DE LA GUERRE</title>
						<author>LOUIS-LUCIEN VERMEIL</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=l7xDAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES DOULEURS DE LA GUERRE</title>
								<author>LOUIS-LUCIEN VERMEIL</author>
								<imprint>
									<pubPlace>LAUSANNE</pubPlace>
									<publisher>LIBRAIRIE BLANC IMER et LEBET</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VRM5">
				<head type="number">V</head>
				<head type="main">LA SENTINELLE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Près</w> <w n="1.2">d</w>'<w n="1.3">un</w> <w n="1.4">arbre</w> <w n="1.5">noueux</w> <w n="1.6">est</w> <w n="1.7">une</w> <w n="1.8">sentinelle</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Elle</w> <w n="2.2">pense</w> <w n="2.3">au</w> <w n="2.4">pays</w>, <w n="2.5">voit</w> <w n="2.6">son</w> <w n="2.7">toit</w>, <w n="2.8">sa</w> <w n="2.9">tonnelle</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">clocher</w> <w n="3.3">du</w> <w n="3.4">village</w> <w n="3.5">où</w> <w n="3.6">frappe</w> <w n="3.7">sur</w> <w n="3.8">l</w>'<w n="3.9">airain</w></l>
					<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">temps</w> <w n="4.3">qui</w> <w n="4.4">coule</w> <w n="4.5">en</w> <w n="4.6">paix</w> <w n="4.7">sur</w> <w n="4.8">les</w> <w n="4.9">rives</w> <w n="4.10">du</w> <w n="4.11">Rhin</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Elle</w> <w n="5.2">Voit</w> <w n="5.3">tout</w> <w n="5.4">distinct</w> <w n="5.5">et</w> <w n="5.6">comme</w> <w n="5.7">dans</w> <w n="5.8">un</w> <w n="5.9">rêve</w> :</l>
					<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">beau</w> <w n="6.3">fleuve</w> <w n="6.4">rapide</w>, <w n="6.5">et</w> <w n="6.6">le</w> <w n="6.7">pont</w>, <w n="6.8">puis</w> <w n="6.9">la</w> <w n="6.10">grève</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">jardin</w> <w n="7.3">où</w> <w n="7.4">le</w> <w n="7.5">soir</w> <w n="7.6">viennent</w> <w n="7.7">causer</w> <w n="7.8">ses</w> <w n="7.9">sœurs</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">le</w> <w n="8.3">vieux</w> <w n="8.4">père</w> <w n="8.5">aussi</w> <w n="8.6">qui</w> <w n="8.7">cultive</w> <w n="8.8">ses</w> <w n="8.9">fleurs</w> !</l>
					<l n="9" num="1.9"><w n="9.1">Ah</w> ! <w n="9.2">comment</w> <w n="9.3">oublier</w> <w n="9.4">les</w> <w n="9.5">parents</w>, <w n="9.6">la</w> <w n="9.7">famille</w> ?</l>
					<l n="10" num="1.10"><w n="10.1">Le</w> <w n="10.2">berceau</w> <w n="10.3">vert</w> <w n="10.4">et</w> <w n="10.5">frais</w> <w n="10.6">de</w> <w n="10.7">la</w> <w n="10.8">jeune</w> <w n="10.9">charmille</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Où</w> <w n="11.2">l</w>'<w n="11.3">enfant</w> <w n="11.4">blond</w>, <w n="11.5">joyeux</w>, <w n="11.6">au</w> <w n="11.7">visage</w> <w n="11.8">narquois</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Montait</w> <w n="12.2">en</w> <w n="12.3">commandant</w> <w n="12.4">sur</w> <w n="12.5">son</w> <w n="12.6">cheval</w> <w n="12.7">de</w> <w n="12.8">bois</w> !</l>
					<l n="13" num="1.13"><w n="13.1">Beau</w> <w n="13.2">jeune</w> <w n="13.3">homme</w> <w n="13.4">à</w> <w n="13.5">cette</w> <w n="13.6">heure</w> <w n="13.7">et</w> <w n="13.8">bonne</w> <w n="13.9">sentinelle</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Qui</w> <w n="14.2">croit</w> <w n="14.3">voir</w>, <w n="14.4">en</w> <w n="14.5">songeant</w>, <w n="14.6">la</w> <w n="14.7">maison</w> <w n="14.8">paternelle</w> !</l>
					<l n="15" num="1.15"><w n="15.1">Enfant</w> <w n="15.2">hier</w> <w n="15.3">encore</w> <w n="15.4">et</w> <w n="15.5">soldat</w> <w n="15.6">aujourd</w>'<w n="15.7">hui</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Conrad</w> <w n="16.2">pense</w> <w n="16.3">aux</w> <w n="16.4">absens</w>, <w n="16.5">mais</w> <w n="16.6">non</w> <w n="16.7">pas</w> <w n="16.8">sans</w> <w n="16.9">ennui</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Il</w> <w n="17.2">ne</w> <w n="17.3">peut</w> <w n="17.4">que</w> <w n="17.5">songer</w>, <w n="17.6">sentinelle</w> <w n="17.7">perdue</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Sur</w> <w n="18.2">son</w> <w n="18.3">coursier</w> <w n="18.4">fougueux</w> <w n="18.5">dont</w> <w n="18.6">l</w>'<w n="18.7">oreille</w> <w n="18.8">est</w> <w n="18.9">tendue</w> !</l>
					<l n="19" num="1.19"><w n="19.1">Conrad</w> <w n="19.2">est</w> <w n="19.3">fiancé</w> ; <w n="19.4">son</w> <w n="19.5">cœur</w> <w n="19.6">en</w> <w n="19.7">ce</w> <w n="19.8">moment</w></l>
					<l n="20" num="1.20"><w n="20.1">Est</w> <w n="20.2">plein</w> <w n="20.3">de</w> <w n="20.4">son</w> <w n="20.5">amour</w> ; <w n="20.6">c</w>'<w n="20.7">est</w> <w n="20.8">un</w> <w n="20.9">fidèle</w> <w n="20.10">amant</w>. —</l>
					<l n="21" num="1.21">« <w n="21.1">Qui</w> <w n="21.2">vive</w> ? <w n="21.3">halte</w>-<w n="21.4">là</w> ! » — <w n="21.5">Mais</w> <w n="21.6">c</w>'<w n="21.7">est</w> <w n="21.8">la</w> <w n="21.9">relevée</w>.</l>
					<l n="22" num="1.22"><w n="22.1">Et</w> <w n="22.2">Conrad</w> <w n="22.3">rentre</w> <w n="22.4">au</w> <w n="22.5">camp</w>, <w n="22.6">sa</w> <w n="22.7">garde</w> <w n="22.8">est</w> <w n="22.9">achevée</w>.</l>
					<l n="23" num="1.23"><w n="23.1">Il</w> <w n="23.2">guerroie</w>, <w n="23.3">il</w> <w n="23.4">combat</w> <w n="23.5">avec</w> <w n="23.6">un</w> <w n="23.7">noble</w> <w n="23.8">élan</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Léger</w>, <w n="24.2">il</w> <w n="24.3">court</w>, <w n="24.4">il</w> <w n="24.5">vole</w>, <w n="24.6">oh</w> ! <w n="24.7">l</w>'<w n="24.8">intrépide</w> <w n="24.9">uhlan</w> !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="2">
					<l n="25" num="2.1"><w n="25.1">Hélas</w> ! <w n="25.2">trois</w> <w n="25.3">jours</w> <w n="25.4">après</w>, <w n="25.5">sur</w> <w n="25.6">le</w> <w n="25.7">champ</w> <w n="25.8">de</w> <w n="25.9">bataille</w>,</l>
					<l n="26" num="2.2"><w n="26.1">Un</w> <w n="26.2">maraudeur</w> <w n="26.3">eût</w> <w n="26.4">fait</w> <w n="26.5">une</w> <w n="26.6">triste</w> <w n="26.7">trouvaille</w>,</l>
					<l n="27" num="2.3"><w n="27.1">Conrad</w> <w n="27.2">était</w> <w n="27.3">couché</w> <w n="27.4">sur</w> <w n="27.5">le</w> <w n="27.6">bord</w> <w n="27.7">d</w>'<w n="27.8">un</w> <w n="27.9">chemin</w>,</l>
					<l n="28" num="2.4"><w n="28.1">Et</w> <w n="28.2">là</w> <w n="28.3">tenait</w> <w n="28.4">encor</w> <w n="28.5">sa</w> <w n="28.6">Bible</w> <w n="28.7">dans</w> <w n="28.8">la</w> <w n="28.9">main</w>.</l>
					<l n="29" num="2.5"><w n="29.1">Sa</w> <w n="29.2">Bible</w> <w n="29.3">précieuse</w> <w n="29.4">était</w> <w n="29.5">restée</w> <w n="29.6">ouverte</w> ;</l>
					<l n="30" num="2.6"><w n="30.1">Il</w> <w n="30.2">s</w>'<w n="30.3">y</w> <w n="30.4">trouvait</w> <w n="30.5">aussi</w> <w n="30.6">quelques</w> <w n="30.7">brins</w> <w n="30.8">d</w>'<w n="30.9">herbe</w> <w n="30.10">verte</w>,</l>
					<l n="31" num="2.7"><w n="31.1">Une</w> <w n="31.2">photographie</w> <w n="31.3">et</w> <w n="31.4">deux</w> <w n="31.5">mots</w> <w n="31.6">au</w> <w n="31.7">crayon</w> :</l>
					<l n="32" num="2.8"><w n="32.1">Suprême</w> <w n="32.2">et</w> <w n="32.3">doux</w> <w n="32.4">adieu</w> ! <w n="32.5">du</w> <w n="32.6">cœur</w> <w n="32.7">dernier</w> <w n="32.8">rayon</w> !</l>
					<l n="33" num="2.9"><w n="33.1">Conrad</w> <w n="33.2">mourut</w> <w n="33.3">ainsi</w> : <w n="33.4">sa</w> <w n="33.5">dernière</w> <w n="33.6">pensée</w></l>
					<l n="34" num="2.10"><w n="34.1">Fut</w> <w n="34.2">pour</w> <w n="34.3">son</w> <w n="34.4">Dieu</w>-<w n="34.5">Sauveur</w> <w n="34.6">et</w> <w n="34.7">pour</w> <w n="34.8">sa</w> <w n="34.9">fiancée</w> !…</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="3">
					<l n="35" num="3.1"><w n="35.1">Et</w> <w n="35.2">seul</w> <w n="35.3">le</w> <w n="35.4">vent</w> <w n="35.5">pleurait</w>, <w n="35.6">en</w> <w n="35.7">passant</w> <w n="35.8">sur</w> <w n="35.9">les</w> <w n="35.10">bois</w> ;</l>
					<l n="36" num="3.2"><w n="36.1">On</w> <w n="36.2">eût</w> <w n="36.3">dit</w> <w n="36.4">les</w> <w n="36.5">sanglots</w> <w n="36.6">d</w>'<w n="36.7">une</w> <w n="36.8">lugubre</w> <w n="36.9">voix</w> !</l>
				</lg>
			</div></body></text></TEI>