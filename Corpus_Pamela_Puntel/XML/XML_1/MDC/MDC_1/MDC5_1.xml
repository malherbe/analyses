<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">HAINE AUX BARBARES</title>
				<title type="sub_1">CHANTS PATRIOTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="MDC">
					<name>
						<forename>Victor</forename>
						<nameLink>de</nameLink>
						<surname>MÉRI DE LA CANORGUE</surname>
					</name>
					<date from="1805" to="1875">1805-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>272 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MDC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">HAINE AUX BARBARES. CHANTS PATRIOTIQUES</title>
						<author>VICTOR DE MÉRI DE LA CANORGUE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30929677r.public</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>HAINE AUX BARBARES. CHANTS PATRIOTIQUES</title>
								<author>VICTOR DE MÉRI DE LA CANORGUE</author>
								<imprint>
									<pubPlace>MARSEILLE</pubPlace>
									<publisher>CAMOIN LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="MDC5">
				<head type="main">A LA FRANCE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quel</w> <w n="1.2">sort</w> <w n="1.3">sera</w> <w n="1.4">le</w> <w n="1.5">tien</w>, <w n="1.6">ô</w> <w n="1.7">France</w> <w n="1.8">bien</w>-<w n="1.9">aimée</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Vas</w>-<w n="2.2">tu</w> <w n="2.3">servir</w> <w n="2.4">de</w> <w n="2.5">proie</w> <w n="2.6">la</w> <w n="2.7">horde</w> <w n="2.8">affamée</w></l>
					<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">déchire</w> <w n="3.3">ton</w> <w n="3.4">sein</w>, <w n="3.5">s</w>'<w n="3.6">abreuve</w> <w n="3.7">de</w> <w n="3.8">ton</w> <w n="3.9">sang</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">laisse</w> <w n="4.3">sur</w> <w n="4.4">ton</w> <w n="4.5">sol</w> <w n="4.6">sa</w> <w n="4.7">souillure</w>, <w n="4.8">en</w> <w n="4.9">passant</w> ?</l>
					<l n="5" num="1.5"><w n="5.1">Ou</w> <w n="5.2">de</w> <w n="5.3">Charles</w>-<w n="5.4">Martel</w>, <w n="5.5">reprenant</w> <w n="5.6">la</w> <w n="5.7">massue</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Vas</w>-<w n="6.2">tu</w> <w n="6.3">broyer</w> <w n="6.4">encor</w>, <w n="6.5">comme</w> <w n="6.6">au</w> <w n="6.7">jour</w> <w n="6.8">de</w> <w n="6.9">Poitiers</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Une</w> <w n="7.2">race</w> <w n="7.3">maudite</w> <w n="7.4">et</w> <w n="7.5">dans</w> <w n="7.6">l</w>'<w n="7.7">enfer</w> <w n="7.8">conçue</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">convertir</w> <w n="8.3">ainsi</w> <w n="8.4">tes</w> <w n="8.5">cyprès</w> <w n="8.6">en</w> <w n="8.7">lauriers</w> ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Quoi</w> ! <w n="9.2">tu</w> <w n="9.3">ne</w> <w n="9.4">réponds</w> <w n="9.5">rien</w>, <w n="9.6">ô</w> <w n="9.7">ma</w> <w n="9.8">chère</w> <w n="9.9">mourante</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Et</w> <w n="10.2">la</w> <w n="10.3">terre</w> <w n="10.4">h</w> <w n="10.5">tes</w> <w n="10.6">maux</w> <w n="10.7">demeure</w> <w n="10.8">indifférente</w> !</l>
					<l n="11" num="2.3"><w n="11.1">Hélas</w> ! <w n="11.2">autour</w> <w n="11.3">de</w> <w n="11.4">toi</w> <w n="11.5">quel</w> <w n="11.6">silence</w> <w n="11.7">effrayant</w> :</l>
					<l n="12" num="2.4"><w n="12.1">Tout</w> <w n="12.2">tremble</w>, <w n="12.3">tout</w> <w n="12.4">se</w> <w n="12.5">tait</w> <w n="12.6">et</w> <w n="12.7">l</w>'<w n="12.8">abîme</w> <w n="12.9">est</w> <w n="12.10">béant</w> !</l>
					<l n="13" num="2.5"><w n="13.1">Pourtant</w> <w n="13.2">tu</w> <w n="13.3">n</w>'<w n="13.4">es</w> <w n="13.5">point</w> <w n="13.6">morte</w>, <w n="13.7">ô</w> <w n="13.8">France</w> <w n="13.9">que</w> <w n="13.10">j</w>'<w n="13.11">adore</w> !</l>
					<l n="14" num="2.6"><w n="14.1">Ton</w> <w n="14.2">cœur</w>, <w n="14.3">quoique</w> <w n="14.4">saignant</w>, <w n="14.5">ton</w> <w n="14.6">cœur</w> <w n="14.7">palpite</w> <w n="14.8">encore</w>.</l>
					<l n="15" num="2.7"><w n="15.1">Nul</w> <w n="15.2">peuple</w>, <w n="15.3">nul</w> <w n="15.4">héros</w> <w n="15.5">ne</w> <w n="15.6">vient</w> <w n="15.7">te</w> <w n="15.8">secourir</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Et</w>, <w n="16.2">seule</w>, <w n="16.3">sans</w> <w n="16.4">secours</w> <w n="16.5">te</w> <w n="16.6">faudra</w>-<w n="16.7">t</w>-<w n="16.8">il</w> <w n="16.9">mourir</w> ?</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Ton</w> <w n="17.2">barbare</w> <w n="17.3">ennemi</w>, <w n="17.4">contre</w> <w n="17.5">toi</w> <w n="17.6">plein</w> <w n="17.7">de</w> <w n="17.8">rage</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Te</w> <w n="18.2">foule</w> <w n="18.3">sous</w> <w n="18.4">ses</w> <w n="18.5">pieds</w>, <w n="18.6">te</w> <w n="18.7">dépouille</w> <w n="18.8">et</w> <w n="18.9">t</w>'<w n="18.10">outrage</w> :</l>
					<l n="19" num="3.3"><w n="19.1">Il</w> <w n="19.2">a</w> <w n="19.3">juré</w> <w n="19.4">ta</w> <w n="19.5">mort</w>, <w n="19.6">le</w> <w n="19.7">poignard</w> <w n="19.8">sur</w> <w n="19.9">ton</w> <w n="19.10">sein</w>,</l>
					<l n="20" num="3.4"><w n="20.1">Et</w> <w n="20.2">de</w> <w n="20.3">roi</w> <w n="20.4">qu</w>'<w n="20.5">il</w> <w n="20.6">était</w> <w n="20.7">il</w> <w n="20.8">s</w>'<w n="20.9">est</w> <w n="20.10">fait</w> <w n="20.11">assassin</w> !</l>
					<l n="21" num="3.5"><w n="21.1">Qui</w> <w n="21.2">donc</w> <w n="21.3">viendra</w> <w n="21.4">panser</w> <w n="21.5">tes</w> <w n="21.6">blessures</w> <w n="21.7">profondes</w> ?</l>
					<l n="22" num="3.6"><w n="22.1">J</w>'<w n="22.2">interroge</w> <w n="22.3">le</w> <w n="22.4">Ciel</w>, <w n="22.5">et</w> <w n="22.6">la</w> <w n="22.7">terre</w>, <w n="22.8">et</w> <w n="22.9">les</w> <w n="22.10">ondes</w> ;</l>
					<l n="23" num="3.7"><w n="23.1">A</w> <w n="23.2">défaut</w> <w n="23.3">des</w> <w n="23.4">vivants</w> <w n="23.5">pour</w> <w n="23.6">chasser</w> <w n="23.7">ton</w> <w n="23.8">bourreau</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Si</w> <w n="24.2">j</w>'<w n="24.3">évoquais</w> <w n="24.4">soudain</w> <w n="24.5">les</w> <w n="24.6">morts</w> <w n="24.7">de</w> <w n="24.8">leur</w> <w n="24.9">tombeau</w> !…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">Vous</w>, <w n="25.2">guerriers</w> <w n="25.3">d</w>'<w n="25.4">autrefois</w>, <w n="25.5">sortez</w> <w n="25.6">donc</w> <w n="25.7">de</w> <w n="25.8">la</w> <w n="25.9">tombe</w>,</l>
					<l n="26" num="4.2"><w n="26.1">Et</w> <w n="26.2">ne</w> <w n="26.3">permettez</w> <w n="26.4">pas</w> <w n="26.5">que</w> <w n="26.6">la</w> <w n="26.7">France</w> <w n="26.8">succombe</w> !</l>
					<l n="27" num="4.3"><w n="27.1">Vous</w>, <w n="27.2">Bayard</w>, <w n="27.3">Duguesclin</w>, <w n="27.4">et</w> <w n="27.5">Lahire</w>, <w n="27.6">et</w> <w n="27.7">Dunois</w>,</l>
					<l n="28" num="4.4"><w n="28.1">Armez</w>-<w n="28.2">vous</w> <w n="28.3">du</w> <w n="28.4">drapeau</w> <w n="28.5">que</w> <w n="28.6">portaient</w> <w n="28.7">nos</w> <w n="28.8">vieux</w> <w n="28.9">rois</w> ;</l>
					<l n="29" num="4.5"><w n="29.1">Jeanne</w> <w n="29.2">d</w>'<w n="29.3">Arc</w> <w n="29.4">et</w> <w n="29.5">Clisson</w>, <w n="29.6">accourez</w> <w n="29.7">aux</w> <w n="29.8">batailles</w> .</l>
					<l n="30" num="4.6"><w n="30.1">Et</w> <w n="30.2">chassez</w> <w n="30.3">l</w>'<w n="30.4">ennemi</w> <w n="30.5">debout</w> <w n="30.6">sur</w> <w n="30.7">nos</w> <w n="30.8">murailles</w> ;</l>
					<l n="31" num="4.7"><w n="31.1">Vous</w>, <w n="31.2">Turenne</w> <w n="31.3">et</w> <w n="31.4">Condé</w>, <w n="31.5">Catinat</w> <w n="31.6">et</w> <w n="31.7">Villars</w></l>
					<l n="32" num="4.8"><w n="32.1">Faites</w> <w n="32.2">fuir</w> <w n="32.3">sous</w> <w n="32.4">vos</w> <w n="32.5">coups</w> <w n="32.6">ces</w> <w n="32.7">bandes</w> <w n="32.8">de</w> <w n="32.9">pillards</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">Point</w> <w n="33.2">de</w> <w n="33.3">Pape</w> <w n="33.4">Léon</w>, <w n="33.5">de</w> <w n="33.6">Geneviève</w> <w n="33.7">sainte</w></l>
					<l n="34" num="5.2"><w n="34.1">Qui</w> <w n="34.2">de</w> <w n="34.3">nos</w> <w n="34.4">murs</w> <w n="34.5">sacrés</w> <w n="34.6">sauvegarde</w> <w n="34.7">l</w>'<w n="34.8">enceinte</w>,</l>
					<l n="35" num="5.3"><w n="35.1">Qui</w> <w n="35.2">fasse</w> <w n="35.3">reculer</w> <w n="35.4">ce</w> <w n="35.5">moderne</w> <w n="35.6">Attila</w></l>
					<l n="36" num="5.4"><w n="36.1">Que</w> <w n="36.2">Dieu</w>, <w n="36.3">pour</w> <w n="36.4">nous</w> <w n="36.5">punir</w>, <w n="36.6">de</w> <w n="36.7">nos</w> <w n="36.8">jours</w> <w n="36.9">appela</w>.</l>
					<l n="37" num="5.5"><w n="37.1">De</w> <w n="37.2">notre</w> <w n="37.3">capitale</w> <w n="37.4">il</w> <w n="37.5">franchit</w> <w n="37.6">la</w> <w n="37.7">barrière</w>,</l>
					<l n="38" num="5.6"><w n="38.1">Et</w> <w n="38.2">point</w> <w n="38.3">de</w> <w n="38.4">bras</w> <w n="38.5">vengeur</w> <w n="38.6">qui</w> <w n="38.7">le</w> <w n="38.8">tienne</w> <w n="38.9">en</w> <w n="38.10">arrière</w>,</l>
					<l n="39" num="5.7"><w n="39.1">Qui</w> <w n="39.2">détourne</w> <w n="39.3">de</w> <w n="39.4">nous</w> <w n="39.5">ce</w> <w n="39.6">barbare</w> <w n="39.7">oppresseur</w></l>
					<l n="40" num="5.8"><w n="40.1">Qui</w> <w n="40.2">traîne</w> <w n="40.3">sur</w> <w n="40.4">ses</w> <w n="40.5">pas</w> <w n="40.6">le</w> <w n="40.7">carnage</w> <w n="40.8">et</w> <w n="40.9">l</w>'<w n="40.10">horreur</w> !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><w n="41.1">Vous</w>, <w n="41.2">les</w> <w n="41.3">vaillants</w>, <w n="41.4">les</w> <w n="41.5">forts</w>, <w n="41.6">vous</w> <w n="41.7">qui</w> <w n="41.8">vivez</w> <w n="41.9">encore</w>,</l>
					<l n="42" num="6.2"><w n="42.1">Vous</w> <w n="42.2">que</w> <w n="42.3">la</w> <w n="42.4">gloire</w> <w n="42.5">appelle</w> <w n="42.6">et</w> <w n="42.7">que</w> <w n="42.8">l</w>'<w n="42.9">honneur</w> <w n="42.10">décore</w> ;</l>
					<l n="43" num="6.3"><w n="43.1">Et</w> <w n="43.2">vous</w>, <w n="43.3">fiers</w> <w n="43.4">Vendéens</w>, <w n="43.5">vous</w>, <w n="43.6">les</w> <w n="43.7">fils</w> <w n="43.8">des</w> <w n="43.9">Croisés</w>,</l>
					<l n="44" num="6.4"><w n="44.1">Que</w> <w n="44.2">déjà</w> <w n="44.3">cent</w> <w n="44.4">combats</w> <w n="44.5">ont</w> <w n="44.6">immortalisés</w>,</l>
					<l n="45" num="6.5"><w n="45.1">Laisserez</w>-<w n="45.2">vous</w> <w n="45.3">ainsi</w> <w n="45.4">notre</w> <w n="45.5">vieille</w> <w n="45.6">patrie</w></l>
					<l n="46" num="6.6"><w n="46.1">Pencher</w> <w n="46.2">vers</w> <w n="46.3">sa</w> <w n="46.4">ruine</w>, <w n="46.5">et</w> <w n="46.6">s</w>'<w n="46.7">abimer</w> <w n="46.8">flétrie</w>,</l>
					<l n="47" num="6.7"><w n="47.1">Et</w> <w n="47.2">ne</w> <w n="47.3">ferez</w>-<w n="47.4">vous</w> <w n="47.5">point</w> <w n="47.6">à</w> <w n="47.7">ce</w> <w n="47.8">noble</w> <w n="47.9">pays</w></l>
					<l n="48" num="6.8"><w n="48.1">Quelque</w> <w n="48.2">nouveau</w> <w n="48.3">rempart</w> <w n="48.4">couvert</w> <w n="48.5">de</w> <w n="48.6">vos</w> <w n="48.7">débris</w> ?</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1"><w n="49.1">Mais</w> <w n="49.2">vous</w>, <w n="49.3">mon</w> <w n="49.4">Dieu</w>, <w n="49.5">mais</w> <w n="49.6">vous</w>, <w n="49.7">laisserez</w>-<w n="49.8">vous</w> <w n="49.9">la</w> <w n="49.10">France</w></l>
					<l n="50" num="7.2"><w n="50.1">S</w>'<w n="50.2">affaisser</w> <w n="50.3">sous</w> <w n="50.4">le</w> <w n="50.5">poids</w> <w n="50.6">de</w> <w n="50.7">sa</w> <w n="50.8">rude</w> <w n="50.9">souffrance</w> ?</l>
					<l n="51" num="7.3"><w n="51.1">N</w>'<w n="51.2">a</w>-<w n="51.3">t</w>-<w n="51.4">elle</w> <w n="51.5">point</w> <w n="51.6">encore</w> <w n="51.7">épuisé</w> <w n="51.8">devant</w> <w n="51.9">vous</w>,</l>
					<l n="52" num="7.4"><w n="52.1">Tout</w> <w n="52.2">son</w> <w n="52.3">amer</w> <w n="52.4">calice</w>, <w n="52.5">et</w> <w n="52.6">de</w> <w n="52.7">votre</w> <w n="52.8">courroux</w></l>
					<l n="53" num="7.5"><w n="53.1">Doit</w>-<w n="53.2">elle</w> <w n="53.3">ressentir</w> <w n="53.4">les</w> <w n="53.5">suites</w> <w n="53.6">effroyables</w></l>
					<l n="54" num="7.6"><w n="54.1">Sans</w> <w n="54.2">toucher</w> <w n="54.3">votre</w> <w n="54.4">cœur</w> ? <w n="54.5">Ses</w> <w n="54.6">accents</w> <w n="54.7">lamentables</w></l>
					<l n="55" num="7.7"><w n="55.1">Sont</w> <w n="55.2">montés</w> <w n="55.3">jusqu</w>'<w n="55.4">au</w> <w n="55.5">Ciel</w>, <w n="55.6">ne</w> <w n="55.7">la</w> <w n="55.8">repoussez</w> <w n="55.9">pas</w>,</l>
					<l n="56" num="7.8"><w n="56.1">Ouvrez</w>-<w n="56.2">lui</w> <w n="56.3">votre</w> <w n="56.4">sein</w> <w n="56.5">et</w> <w n="56.6">tendez</w>-<w n="56.7">lui</w> <w n="56.8">les</w> <w n="56.9">bras</w> !</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1"><w n="57.1">Pitié</w>, <w n="57.2">mon</w> <w n="57.3">Dieu</w>, <w n="57.4">pitié</w> ! <w n="57.5">Seigneur</w>, <w n="57.6">pardon</w> <w n="57.7">pour</w> <w n="57.8">elle</w> !</l>
					<l n="58" num="8.2"><w n="58.1">Rendez</w>-<w n="58.2">lui</w>, <w n="58.3">désormais</w>, <w n="58.4">une</w> <w n="58.5">vie</w> <w n="58.6">immortelle</w>.</l>
					<l n="59" num="8.3"><w n="59.1">Venez</w> <w n="59.2">à</w> <w n="59.3">son</w> <w n="59.4">secours</w>, <w n="59.5">vous</w>, <w n="59.6">maître</w> <w n="59.7">tout</w>-<w n="59.8">puissant</w> !</l>
					<l n="60" num="8.4"><w n="60.1">Elle</w> <w n="60.2">s</w>'<w n="60.3">est</w> <w n="60.4">épurée</w> <w n="60.5">des</w> <w n="60.6">torrents</w> <w n="60.7">de</w> <w n="60.8">sang</w>,</l>
					<l n="61" num="8.5"><w n="61.1">Et</w> <w n="61.2">parmi</w> <w n="61.3">les</w> <w n="61.4">combats</w> <w n="61.5">subissant</w> <w n="61.6">leur</w> <w n="61.7">martyre</w>,</l>
					<l n="62" num="8.6"><w n="62.1">Ses</w> <w n="62.2">héros</w> <w n="62.3">invoquaient</w>, <w n="62.4">dans</w> <w n="62.5">leur</w> <w n="62.6">pieux</w> <w n="62.7">délire</w>,</l>
					<l n="63" num="8.7"><w n="63.1">Le</w> <w n="63.2">nom</w> <w n="63.3">de</w> <w n="63.4">votre</w> <w n="63.5">Fils</w> ; <w n="63.6">de</w> <w n="63.7">leur</w> <w n="63.8">mourante</w> <w n="63.9">voix</w>,</l>
					<l n="64" num="8.8"><w n="64.1">Ils</w> <w n="64.2">priaient</w> <w n="64.3">comme</w> <w n="64.4">lui</w>, <w n="64.5">tout</w> <w n="64.6">en</w> <w n="64.7">baisant</w> <w n="64.8">sa</w> <w n="64.9">croix</w>.</l>
				</lg>
				<lg n="9">
					<l n="65" num="9.1"><w n="65.1">Et</w> <w n="65.2">je</w> <w n="65.3">priais</w> <w n="65.4">ainsi</w>, <w n="65.5">lorsque</w> <w n="65.6">du</w> <w n="65.7">sein</w> <w n="65.8">des</w> <w n="65.9">ombres</w></l>
					<l n="66" num="9.2"><w n="66.1">Qui</w> <w n="66.2">couvraient</w> <w n="66.3">mon</w> <w n="66.4">pays</w>, <w n="66.5">couché</w> <w n="66.6">sur</w> <w n="66.7">ses</w> <w n="66.8">décombres</w> ;</l>
					<l n="67" num="9.3"><w n="67.1">Le</w> <w n="67.2">soleil</w> <w n="67.3">s</w>'<w n="67.4">élança</w>, <w n="67.5">brillant</w> <w n="67.6">et</w> <w n="67.7">radieux</w>,</l>
					<l n="68" num="9.4"><w n="68.1">Et</w> <w n="68.2">j</w>'<w n="68.3">aperçus</w> <w n="68.4">là</w>-<w n="68.5">haut</w>, <w n="68.6">dans</w> <w n="68.7">la</w> <w n="68.8">splendeur</w> <w n="68.9">des</w> <w n="68.10">cieux</w>,</l>
					<l n="69" num="9.5"><w n="69.1">Le</w> <w n="69.2">Christ</w> <w n="69.3">qui</w> <w n="69.4">demandait</w> <w n="69.5">notre</w> <w n="69.6">grâce</w> <w n="69.7">à</w> <w n="69.8">son</w> <w n="69.9">Père</w> ;</l>
					<l n="70" num="9.6"><w n="70.1">Et</w> <w n="70.2">la</w> <w n="70.3">paix</w> <w n="70.4">aussitôt</w>, <w n="70.5">divine</w> <w n="70.6">messagère</w>,</l>
					<l n="71" num="9.7"><w n="71.1">Apparut</w> <w n="71.2">à</w> <w n="71.3">nos</w> <w n="71.4">yeux</w>, <w n="71.5">ravis</w> <w n="71.6">de</w> <w n="71.7">ce</w> <w n="71.8">bonheur</w>,</l>
					<l n="72" num="9.8"><w n="72.1">Conduisant</w> <w n="72.2">par</w> <w n="72.3">la</w> <w n="72.4">main</w> <w n="72.5">notre</w> <w n="72.6">unique</w> <w n="72.7">Sauveur</w>.</l>
				</lg>
				<lg n="10">
					<l n="73" num="10.1"><w n="73.1">La</w> <w n="73.2">France</w>, <w n="73.3">sous</w> <w n="73.4">ses</w> <w n="73.5">lois</w>, <w n="73.6">et</w> <w n="73.7">forte</w>, <w n="73.8">et</w> <w n="73.9">rajeunie</w>,</l>
					<l n="74" num="10.2"><w n="74.1">Reprenait</w>, <w n="74.2">grâce</w> <w n="74.3">lui</w>, <w n="74.4">sa</w> <w n="74.5">force</w> <w n="74.6">et</w> <w n="74.7">son</w> <w n="74.8">génie</w> ;</l>
					<l n="75" num="10.3"><w n="75.1">Reniant</w> <w n="75.2">du</w> <w n="75.3">passé</w> <w n="75.4">les</w> <w n="75.5">fatales</w> <w n="75.6">erreurs</w>,</l>
					<l n="76" num="10.4"><w n="76.1">Elle</w> <w n="76.2">ne</w> <w n="76.3">livrait</w> <w n="76.4">plus</w> <w n="76.5">sa</w> <w n="76.6">vie</w> <w n="76.7">aux</w> <w n="76.8">empereurs</w>,</l>
					<l n="77" num="10.5"><w n="77.1">A</w> <w n="77.2">ces</w> <w n="77.3">hideux</w> <w n="77.4">Césars</w> <w n="77.5">qui</w>, <w n="77.6">montés</w> <w n="77.7">sur</w> <w n="77.8">le</w> <w n="77.9">trône</w>,</l>
					<l n="78" num="10.6"><w n="78.1">Sur</w> <w n="78.2">leur</w> <w n="78.3">front</w> <w n="78.4">sans</w> <w n="78.5">pudeur</w> <w n="78.6">attachaient</w> <w n="78.7">la</w> <w n="78.8">couronne</w>,</l>
					<l n="79" num="10.7"><w n="79.1">Dans</w> <w n="79.2">la</w> <w n="79.3">boue</w> <w n="79.4">et</w> <w n="79.5">le</w> <w n="79.6">sang</w> <w n="79.7">se</w> <w n="79.8">vautraient</w> <w n="79.9">à</w> <w n="79.10">plaisir</w>,</l>
					<l n="80" num="10.8"><w n="80.1">Et</w>, <w n="80.2">nous</w> <w n="80.3">faisant</w> <w n="80.4">tuer</w>, <w n="80.5">se</w> <w n="80.6">gardaient</w> <w n="80.7">de</w> <w n="80.8">mourir</w>.</l>
				</lg>
			</div></body></text></TEI>