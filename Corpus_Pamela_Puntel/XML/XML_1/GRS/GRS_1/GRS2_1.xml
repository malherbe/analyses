<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'ANNÉE MAUDITE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="GRS">
					<name>
						<forename>Charles</forename>
						<surname>GRANDSARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2146 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'ANNÉE MAUDITE 1870-1871</title>
						<author>Charles Grandsard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61285325.r=GRANDSARD%20L%27ANN%C3%89E%20MAUDITE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'ANNÉE MAUDITE 1870-1871</title>
								<author>Charles Grandsard</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE DU PETIT JOURNAL</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRS2">
				<head type="main">STRASBOURG</head>
				<div n="1" type="section">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">veux</w> <w n="1.3">me</w> <w n="1.4">souvenir</w>, <w n="1.5">et</w> <w n="1.6">puis</w>, <w n="1.7">je</w> <w n="1.8">veux</w> <w n="1.9">pleurer</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">gouttes</w> <w n="2.3">de</w> <w n="2.4">la</w> <w n="2.5">pluie</w> <w n="2.6">usent</w> <w n="2.7">enfin</w> <w n="2.8">la</w> <w n="2.9">pierre</w> :</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">veux</w> <w n="3.3">savoir</w> <w n="3.4">si</w> <w n="3.5">l</w>'<w n="3.6">eau</w> <w n="3.7">coulant</w> <w n="3.8">de</w> <w n="3.9">la</w> <w n="3.10">paupière</w></l>
						<l n="4" num="1.4"><w n="4.1">Peut</w> <w n="4.2">user</w> <w n="4.3">ma</w> <w n="4.4">douleur</w> <w n="4.5">et</w> <w n="4.6">me</w> <w n="4.7">faire</w> <w n="4.8">espérer</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C</w>'<w n="5.2">est</w> <w n="5.3">que</w> <w n="5.4">je</w> <w n="5.5">t</w>'<w n="5.6">ai</w> <w n="5.7">connue</w>, <w n="5.8">Alsace</w> ! <w n="5.9">oui</w> ! <w n="5.10">bien</w> <w n="5.11">connue</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Avec</w> <w n="6.2">ton</w> <w n="6.3">grandiose</w> <w n="6.4">et</w> <w n="6.5">paisible</w> <w n="6.6">horizon</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">tes</w> <w n="7.3">fils</w> <w n="7.4">généreux</w>, <w n="7.5">faits</w> <w n="7.6">de</w>, <w n="7.7">forte</w> <w n="7.8">raison</w>,</l>
						<l n="8" num="2.4"><w n="8.1">D</w>'<w n="8.2">austère</w> <w n="8.3">probité</w>, <w n="8.4">de</w> <w n="8.5">vigueur</w> <w n="8.6">contenue</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Depuis</w> <w n="9.2">qu</w>'<w n="9.3">au</w> <w n="9.4">milieu</w> <w n="9.5">d</w>'<w n="9.6">eux</w> <w n="9.7">m</w>'<w n="9.8">a</w> <w n="9.9">conduit</w> <w n="9.10">mon</w> <w n="9.11">chemin</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Je</w> <w n="10.2">sais</w> <w n="10.3">qu</w>'<w n="10.4">à</w> <w n="10.5">leur</w> <w n="10.6">parole</w> <w n="10.7">on</w> <w n="10.8">peut</w> <w n="10.9">croire</w> <w n="10.10">sans</w> <w n="10.11">crainte</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Qu</w>'<w n="11.2">on</w> <w n="11.3">peut</w>, <w n="11.4">d</w>'<w n="11.5">une</w> <w n="11.6">sereine</w> <w n="11.7">et</w> <w n="11.8">confiante</w> <w n="11.9">étreinte</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Rendre</w> <w n="12.2">la</w> <w n="12.3">pression</w> <w n="12.4">de</w> <w n="12.5">leur</w> <w n="12.6">loyale</w> <w n="12.7">main</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Noble</w> <w n="13.2">terre</w>, <w n="13.3">d</w>'<w n="13.4">honneur</w> <w n="13.5">et</w> <w n="13.6">de</w> <w n="13.7">bonté</w> <w n="13.8">pétrie</w> !</l>
						<l n="14" num="4.2"><w n="14.1">Je</w> <w n="14.2">voulais</w> <w n="14.3">dans</w> <w n="14.4">ton</w> <w n="14.5">sein</w> <w n="14.6">me</w> <w n="14.7">faire</w> <w n="14.8">un</w> <w n="14.9">doux</w> <w n="14.10">séjour</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Puis</w>, <w n="15.2">du</w> <w n="15.3">dernier</w> <w n="15.4">sommeil</w> <w n="15.5">y</w> <w n="15.6">reposer</w> <w n="15.7">un</w> <w n="15.8">jour</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Car</w> <w n="16.2">je</w> <w n="16.3">voyais</w> <w n="16.4">en</w> <w n="16.5">toi</w> <w n="16.6">ma</w> <w n="16.7">seconde</w> <w n="16.8">patrie</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Aussi</w>, <w n="17.2">quand</w> <w n="17.3">tu</w> <w n="17.4">tombas</w> <w n="17.5">sous</w> <w n="17.6">la</w> <w n="17.7">main</w> <w n="17.8">du</w> <w n="17.9">vainqueur</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Je</w> <w n="18.2">me</w> <w n="18.3">sentis</w> <w n="18.4">frémir</w> <w n="18.5">d</w>'<w n="18.6">une</w> <w n="18.7">étrange</w> <w n="18.8">souffrance</w> :</l>
						<l n="19" num="5.3"><w n="19.1">Il</w> <w n="19.2">me</w> <w n="19.3">sembla</w> <w n="19.4">qu</w>'<w n="19.5">avec</w> <w n="19.6">un</w> <w n="19.7">lambeau</w> <w n="19.8">de</w> <w n="19.9">la</w> <w n="19.10">France</w></l>
						<l n="20" num="5.4"><w n="20.1">Ces</w> <w n="20.2">Germains</w> <w n="20.3">arrachaient</w> <w n="20.4">un</w> <w n="20.5">lambeau</w> <w n="20.6">de</w> <w n="20.7">mon</w> <w n="20.8">cœur</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Aussi</w>, <w n="21.2">depuis</w> <w n="21.3">qu</w>'<w n="21.4">on</w> <w n="21.5">a</w> <w n="21.6">prononcé</w> <w n="21.7">ta</w> <w n="21.8">sentence</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Comme</w> <w n="22.2">homme</w> <w n="22.3">et</w> <w n="22.4">citoyen</w> <w n="22.5">je</w> <w n="22.6">me</w> <w n="22.7">sens</w> <w n="22.8">amoindri</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">depuis</w> <w n="23.3">ce</w> <w n="23.4">moment</w> <w n="23.5">je</w> <w n="23.6">n</w>'<w n="23.7">ai</w> <w n="23.8">jamais</w> <w n="23.9">souri</w> ;</l>
						<l n="24" num="6.4"><w n="24.1">Un</w> <w n="24.2">voile</w> <w n="24.3">noir</w>, <w n="24.4">pour</w> <w n="24.5">moi</w>, <w n="24.6">s</w>'<w n="24.7">étend</w>' <w n="24.8">sur</w> <w n="24.9">l</w>'<w n="24.10">existence</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">cette</w> <w n="25.3">plaie</w> <w n="25.4">au</w> <w n="25.5">cœur</w>, <w n="25.6">je</w> <w n="25.7">ne</w> <w n="25.8">puis</w> <w n="25.9">l</w>'<w n="25.10">effleurer</w></l>
						<l n="26" num="7.2"><w n="26.1">Sans</w> <w n="26.2">irriter</w> <w n="26.3">soudain</w> <w n="26.4">la</w> <w n="26.5">cuisante</w> <w n="26.6">morsure</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Quel</w> <w n="27.2">baume</w>, <w n="27.3">cependant</w>, <w n="27.4">verser</w> <w n="27.5">à</w> <w n="27.6">ma</w> <w n="27.7">blessure</w> ?…</l>
						<l n="28" num="7.4"><w n="28.1">Je</w> <w n="28.2">veux</w> <w n="28.3">me</w> <w n="28.4">souvenir</w>, <w n="28.5">et</w> <w n="28.6">puis</w>, <w n="28.7">je</w> <w n="28.8">veux</w> <w n="28.9">pleurer</w> !</l>
					</lg>
				</div>
				<div n="2" type="section">
					<head type="number">II</head>
					<lg n="1">
						<l n="29" num="1.1"><w n="29.1">O</w> <w n="29.2">mon</w> <w n="29.3">vaillant</w> <w n="29.4">Strasbourg</w> ! <w n="29.5">mon</w> <w n="29.6">Strasbourg</w> <w n="29.7">héroïque</w> !</l>
						<l n="30" num="1.2"><w n="30.1">Tu</w> <w n="30.2">fus</w> <w n="30.3">grand</w>, <w n="30.4">tu</w> <w n="30.5">fus</w> <w n="30.6">beau</w>, <w n="30.7">quand</w> <w n="30.8">tu</w> <w n="30.9">bravais</w>, <w n="30.10">stoïque</w>,</l>
						<l n="31" num="1.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="31.1">D</w>'<w n="31.2">effroyables</w> <w n="31.3">calamités</w> ;</l>
						<l n="32" num="1.4"><w n="32.1">Et</w> <w n="32.2">ton</w> <w n="32.3">front</w>, <w n="32.4">ravagé</w> <w n="32.5">par</w> <w n="32.6">le</w> <w n="32.7">fer</w> <w n="32.8">et</w> <w n="32.9">la</w> <w n="32.10">flamme</w>,</l>
						<l n="33" num="1.5"><w n="33.1">D</w>'<w n="33.2">horreur</w> <w n="33.3">et</w> <w n="33.4">de</w> <w n="33.5">respect</w>, <w n="33.6">à</w> <w n="33.7">la</w> <w n="33.8">fois</w>, <w n="33.9">remplit</w> <w n="33.10">l</w>'<w n="33.11">âme</w>,</l>
						<l n="34" num="1.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="34.1">O</w> <w n="34.2">martyre</w> <w n="34.3">entre</w> <w n="34.4">les</w> <w n="34.5">cités</w> !</l>
					</lg>
					<lg n="2">
						<l n="35" num="2.1"><w n="35.1">Ensemble</w> <w n="35.2">nous</w> <w n="35.3">avons</w>, <w n="35.4">durant</w> <w n="35.5">ton</w> <w n="35.6">agonie</w>,</l>
						<l n="36" num="2.2"><w n="36.1">Traversé</w> <w n="36.2">bien</w> <w n="36.3">des</w> <w n="36.4">nuits</w> <w n="36.5">de</w> <w n="36.6">cruelle</w> <w n="36.7">insomnie</w>,</l>
						<l n="37" num="2.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="37.1">Bien</w> <w n="37.2">des</w> <w n="37.3">jours</w> <w n="37.4">tout</w> <w n="37.5">souillés</w> <w n="37.6">de</w> <w n="37.7">sang</w> ;</l>
						<l n="38" num="2.4"><w n="38.1">Et</w> <w n="38.2">la</w> <w n="38.3">fraternité</w> <w n="38.4">des</w> <w n="38.5">douleurs</w> <w n="38.6">et</w> <w n="38.7">des</w> <w n="38.8">larmes</w></l>
						<l n="39" num="2.5"><w n="39.1">A</w> <w n="39.2">relié</w>, <w n="39.3">parmi</w> <w n="39.4">ces</w> <w n="39.5">terribles</w> <w n="39.6">alarmes</w>,</l>
						<l n="40" num="2.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="40.1">Mon</w> <w n="40.2">cœur</w> <w n="40.3">au</w> <w n="40.4">tien</w> <w n="40.5">d</w>'<w n="40.6">un</w> <w n="40.7">nœud</w> <w n="40.8">puissant</w> !</l>
					</lg>
					<lg n="3">
						<l n="41" num="3.1"><w n="41.1">Quels</w> <w n="41.2">jours</w> ! <w n="41.3">et</w> <w n="41.4">quelles</w> <w n="41.5">nuits</w> !… <w n="41.6">Farouches</w> <w n="41.7">saturnales</w></l>
						<l n="42" num="3.2"><w n="42.1">De</w> <w n="42.2">détonations</w> <w n="42.3">éclatant</w>, <w n="42.4">infernales</w>,</l>
						<l n="43" num="3.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="43.1">A</w> <w n="43.2">tous</w> <w n="43.3">les</w> <w n="43.4">coins</w> <w n="43.5">de</w> <w n="43.6">l</w>'<w n="43.7">horizon</w> ;</l>
						<l n="44" num="3.4"><w n="44.1">Troupeaux</w> <w n="44.2">de</w> <w n="44.3">malheureux</w> <w n="44.4">réfugiés</w> <w n="44.5">sous</w> <w n="44.6">terre</w>,</l>
						<l n="45" num="3.5"><w n="45.1">Tremblant</w> <w n="45.2">de</w> <w n="45.3">voir</w> <w n="45.4">crouler</w> <w n="45.5">leur</w> <w n="45.6">toit</w> <w n="45.7">héréditaire</w></l>
						<l n="46" num="3.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="46.1">Sur</w> <w n="46.2">les</w> <w n="46.3">voûtes</w> <w n="46.4">de</w> <w n="46.5">leur</w> <w n="46.6">prison</w> ;</l>
					</lg>
					<lg n="4">
						<l n="47" num="4.1"><w n="47.1">Longs</w> <w n="47.2">obus</w> <w n="47.3">regorgeant</w> <w n="47.4">de</w> <w n="47.5">meurtre</w> <w n="47.6">et</w> <w n="47.7">de</w> <w n="47.8">ravages</w>,</l>
						<l n="48" num="4.2"><w n="48.1">Déchirant</w> <w n="48.2">l</w>'<w n="48.3">air</w> <w n="48.4">avec</w> <w n="48.5">des</w> <w n="48.6">sifflements</w> <w n="48.7">sauvages</w>,</l>
						<l n="49" num="4.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="49.1">Comme</w> <w n="49.2">un</w> <w n="49.3">vol</w> <w n="49.4">d</w>'<w n="49.5">énormes</w> <w n="49.6">vautours</w>,</l>
						<l n="50" num="4.4"><w n="50.1">Puis</w>, <w n="50.2">sur</w> <w n="50.3">les</w> <w n="50.4">durs</w> <w n="50.5">pavés</w> <w n="50.6">se</w> <w n="50.7">brisant</w> <w n="50.8">en</w> <w n="50.9">mitraille</w>,</l>
						<l n="51" num="4.5"><w n="51.1">Et</w>, <w n="51.2">sous</w> <w n="51.3">des</w> <w n="51.4">jets</w> <w n="51.5">stridents</w> <w n="51.6">de</w> <w n="51.7">tranchante</w> <w n="51.8">ferraille</w>,</l>
						<l n="52" num="4.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="52.1">Fauchant</w> <w n="52.2">la</w> <w n="52.3">foule</w> <w n="52.4">aux</w> <w n="52.5">alentours</w> ;</l>
					</lg>
					<lg n="5">
						<l n="53" num="5.1"><w n="53.1">Lourdes</w> <w n="53.2">bombes</w> <w n="53.3">qu</w>'<w n="53.4">on</w> <w n="53.5">voit</w>, <w n="53.6">sinistrement</w> <w n="53.7">ronflantes</w>,</l>
						<l n="54" num="5.2"><w n="54.1">Tracer</w> <w n="54.2">dans</w> <w n="54.3">le</w> <w n="54.4">ciel</w> <w n="54.5">noir</w> <w n="54.6">leurs</w> <w n="54.7">courbes</w> <w n="54.8">rutilantes</w></l>
						<l n="55" num="5.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="55.1">Et</w> <w n="55.2">s</w>'<w n="55.3">élancer</w> <w n="55.4">jusqu</w>'<w n="55.5">au</w> <w n="55.6">zénith</w>,</l>
						<l n="56" num="5.4"><w n="56.1">Puis</w>, <w n="56.2">sur</w> <w n="56.3">un</w> <w n="56.4">haut</w> <w n="56.5">pignon</w> <w n="56.6">tout</w> <w n="56.7">à</w> <w n="56.8">coup</w> <w n="56.9">venant</w> <w n="56.10">fondre</w>,</l>
						<l n="57" num="5.5"><w n="57.1">Éclater</w> <w n="57.2">en</w> <w n="57.3">trouant</w> <w n="57.4">la</w> <w n="57.5">maison</w> <w n="57.6">qui</w> <w n="57.7">s</w>'<w n="57.8">effondre</w>.</l>
						<l n="58" num="5.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="58.1">Broyer</w> <w n="58.2">en</w> <w n="58.3">poudre</w> <w n="58.4">le</w> <w n="58.5">granit</w> ;</l>
					</lg>
					<lg n="6">
						<l n="59" num="6.1"><w n="59.1">Puis</w>, <w n="59.2">d</w>'<w n="59.3">instants</w> <w n="59.4">en</w> <w n="59.5">instants</w>, <w n="59.6">le</w> <w n="59.7">sourd</w> <w n="59.8">tocsin</w> <w n="59.9">qui</w> <w n="59.10">gronde</w>,</l>
						<l n="60" num="6.2"><w n="60.1">Le</w> <w n="60.2">lugubre</w> <w n="60.3">incendie</w> <w n="60.4">allumant</w> <w n="60.5">à</w> <w n="60.6">la</w> <w n="60.7">ronde</w></l>
						<l n="61" num="6.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="61.1">Son</w> <w n="61.2">formidable</w> <w n="61.3">flamboiement</w>,</l>
						<l n="62" num="6.4"><w n="62.1">Puis</w>, <w n="62.2">les</w> <w n="62.3">toits</w> <w n="62.4">calcinés</w> <w n="62.5">s</w>'<w n="62.6">abîmant</w> <w n="62.7">dans</w> <w n="62.8">les</w> <w n="62.9">flammes</w>,</l>
						<l n="63" num="6.5"><w n="63.1">Et</w> <w n="63.2">des</w> <w n="63.3">gerbes</w> <w n="63.4">de</w> <w n="63.5">feu</w>, <w n="63.6">comme</w> <w n="63.7">de</w> <w n="63.8">fauves</w> <w n="63.9">lames</w>,</l>
						<l n="64" num="6.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="64.1">Jaillissant</w> <w n="64.2">vers</w> <w n="64.3">le</w> <w n="64.4">firmament</w> ;</l>
					</lg>
					<lg n="7">
						<l n="65" num="7.1"><w n="65.1">Hideux</w> <w n="65.2">brancards</w>, <w n="65.3">portant</w> <w n="65.4">sur</w> <w n="65.5">leurs</w> <w n="65.6">toiles</w> <w n="65.7">sanglantes</w></l>
						<l n="66" num="7.2"><w n="66.1">Des</w> <w n="66.2">mutilés</w> <w n="66.3">aux</w> <w n="66.4">chairs</w> <w n="66.5">rouges</w> <w n="66.6">et</w> <w n="66.7">pantelantes</w></l>
						<l n="67" num="7.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="67.1">Des</w> <w n="67.2">mourants</w> <w n="67.3">à</w> <w n="67.4">l</w>'<w n="67.5">aspect</w> <w n="67.6">hagard</w>,</l>
						<l n="68" num="7.4"><w n="68.1">Des</w> <w n="68.2">femmes</w>, <w n="68.3">des</w>. <w n="68.4">enfants</w> <w n="68.5">au</w> <w n="68.6">front</w> <w n="68.7">morne</w> <w n="68.8">et</w> <w n="68.9">livide</w>,</l>
						<l n="69" num="7.5"><w n="69.1">Aux</w> <w n="69.2">yeux</w> <w n="69.3">déjà</w> <w n="69.4">vitreux</w> <w n="69.5">et</w> <w n="69.6">fixés</w> <w n="69.7">dans</w> <w n="69.8">le</w> <w n="69.9">vide</w>,</l>
						<l n="70" num="7.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="70.1">Horreur</w> <w n="70.2">et</w> <w n="70.3">pitié</w> <w n="70.4">du</w> <w n="70.5">regard</w> ;</l>
					</lg>
					<lg n="8">
						<l n="71" num="8.1"><w n="71.1">Puis</w>, <w n="71.2">la</w> <w n="71.3">ruine</w> <w n="71.4">immense</w>, <w n="71.5">à</w> <w n="71.6">demi</w> <w n="71.7">consumée</w>,</l>
						<l n="72" num="8.2"><w n="72.1">Des</w> <w n="72.2">pans</w> <w n="72.3">de</w> <w n="72.4">murs</w> <w n="72.5">croulants</w> <w n="72.6">et</w> <w n="72.7">noircis</w> <w n="72.8">de</w> <w n="72.9">fumée</w>,</l>
						<l n="73" num="8.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="73.1">Restes</w> <w n="73.2">à</w> <w n="73.3">peine</w> <w n="73.4">refroidis</w>,</l>
						<l n="74" num="8.4"><w n="74.1">Cadavre</w> <w n="74.2">d</w>'<w n="74.3">une</w> <w n="74.4">ville</w>, <w n="74.5">étendu</w> <w n="74.6">sur</w> <w n="74.7">la</w> <w n="74.8">terre</w>,</l>
						<l n="75" num="8.5"><w n="75.1">Pareil</w> <w n="75.2">à</w> <w n="75.3">ces</w> <w n="75.4">cités</w> <w n="75.5">mortes</w>, <w n="75.6">sous</w> <w n="75.7">le</w> <w n="75.8">cratère</w></l>
						<l n="76" num="8.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="76.1">Qui</w> <w n="76.2">les</w> <w n="76.3">mit</w> <w n="76.4">en</w> <w n="76.5">cendres</w> <w n="76.6">jadis</w> ;</l>
					</lg>
					<lg n="9">
						<l n="77" num="9.1"><w n="77.1">Puis</w>, <w n="77.2">enfin</w>, <w n="77.3">la</w> <w n="77.4">splendide</w> <w n="77.5">et</w> <w n="77.6">noble</w> <w n="77.7">basilique</w>,</l>
						<l n="78" num="9.2"><w n="78.1">Des</w> <w n="78.2">âges</w> <w n="78.3">reculés</w> <w n="78.4">merveilleuse</w> <w n="78.5">relique</w>,</l>
						<l n="79" num="9.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="79.1">Parure</w>, <w n="79.2">orgueil</w> <w n="79.3">de</w> <w n="79.4">la</w> <w n="79.5">Cité</w>,</l>
						<l n="80" num="9.4"><w n="80.1">Étalant</w> <w n="80.2">aux</w> <w n="80.3">regards</w> <w n="80.4">ses</w> <w n="80.5">douloureuses</w> <w n="80.6">plaies</w>,</l>
						<l n="81" num="9.5"><w n="81.1">Jonchant</w> <w n="81.2">au</w> <w n="81.3">loin</w> <w n="81.4">le</w> <w n="81.5">sol</w> <w n="81.6">de</w> <w n="81.7">pierres</w> <w n="81.8">mutilées</w>,</l>
						<l n="82" num="9.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="82.1">Débris</w> <w n="82.2">de</w> <w n="82.3">son</w> <w n="82.4">front</w> <w n="82.5">dévasté</w> :</l>
					</lg>
					<lg n="10">
						<l n="83" num="10.1"><w n="83.1">Telles</w> <w n="83.2">sont</w> <w n="83.3">les</w> <w n="83.4">horreurs</w> <w n="83.5">qui</w> <w n="83.6">t</w>'<w n="83.7">ont</w> <w n="83.8">frappé</w> <w n="83.9">naguère</w> ;</l>
						<l n="84" num="10.2"><w n="84.1">Car</w>, <w n="84.2">de</w> <w n="84.3">tous</w> <w n="84.4">les</w> <w n="84.5">fléaux</w> <w n="84.6">que</w> <w n="84.7">déchaîne</w> <w n="84.8">la</w> <w n="84.9">guerre</w>,</l>
						<l n="85" num="10.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="85.1">Pas</w> <w n="85.2">un</w> <w n="85.3">ne</w> <w n="85.4">te</w> <w n="85.5">fut</w> <w n="85.6">épargné</w> ;</l>
						<l n="86" num="10.4"><w n="86.1">Et</w> <w n="86.2">cependant</w>, <w n="86.3">Strasbourg</w> ! <w n="86.4">durant</w> <w n="86.5">ce</w> <w n="86.6">long</w> <w n="86.7">orage</w>,</l>
						<l n="87" num="10.5"><w n="87.1">Je</w> <w n="87.2">le</w> <w n="87.3">jure</w>, <w n="87.4">jamais</w> <w n="87.5">n</w>'<w n="87.6">a</w> <w n="87.7">faibli</w> <w n="87.8">ton</w> <w n="87.9">courage</w>,</l>
						<l n="88" num="10.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="88.1">A</w> <w n="88.2">tous</w> <w n="88.3">les</w> <w n="88.4">malheurs</w> <w n="88.5">résigné</w> !</l>
					</lg>
					<lg n="11">
						<l n="89" num="11.1"><w n="89.1">Et</w> <w n="89.2">quand</w>, <w n="89.3">au</w> <w n="89.4">dernier</w> <w n="89.5">jour</w>, <w n="89.6">par</w> <w n="89.7">la</w> <w n="89.8">brèche</w> <w n="89.9">élargie</w></l>
						<l n="90" num="11.2"><w n="90.1">Allaient</w> <w n="90.2">monter</w> <w n="90.3">l</w>'<w n="90.4">assaut</w>, <w n="90.5">le</w> <w n="90.6">massacre</w>, <w n="90.7">l</w>'<w n="90.8">orgie</w></l>
						<l n="91" num="11.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="91.1">De</w> <w n="91.2">la</w> <w n="91.3">fureur</w> <w n="91.4">et</w> <w n="91.5">du</w> <w n="91.6">trépas</w>,</l>
						<l n="92" num="11.4"><w n="92.1">Comme</w> <w n="92.2">une</w> <w n="92.3">explosion</w> <w n="92.4">jaillit</w> <w n="92.5">de</w> <w n="92.6">tes</w> <w n="92.7">entrailles</w></l>
						<l n="93" num="11.5"><w n="93.1">Ce</w> <w n="93.2">cri</w> <w n="93.3">de</w> <w n="93.4">la</w> <w n="93.5">valeur</w> : « <w n="93.6">Aux</w> <w n="93.7">armes</w> ! <w n="93.8">aux</w> <w n="93.9">murailles</w> !</l>
						<l n="94" num="11.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space>« <w n="94.1">Mourons</w> <w n="94.2">et</w> <w n="94.3">ne</w> <w n="94.4">nous</w> <w n="94.5">rendons</w> <w n="94.6">pas</w> ! »</l>
					</lg>
					<lg n="12">
						<l n="95" num="12.1"><w n="95.1">Plus</w> <w n="95.2">tard</w>, <w n="95.3">ô</w> <w n="95.4">mon</w> <w n="95.5">Strasbourg</w> ! <w n="95.6">l</w>'<w n="95.7">impartiale</w> <w n="95.8">histoire</w></l>
						<l n="96" num="12.2"><w n="96.1">Nous</w> <w n="96.2">dira</w> <w n="96.3">tes</w> <w n="96.4">revers</w>, <w n="96.5">plus</w> <w n="96.6">beaux</w> <w n="96.7">qu</w>'<w n="96.8">une</w> <w n="96.9">victoire</w>,</l>
						<l n="97" num="12.3"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="97.1">Et</w> <w n="97.2">comment</w> <w n="97.3">tu</w> <w n="97.4">les</w> <w n="97.5">subissais</w> ;</l>
						<l n="98" num="12.4"><w n="98.1">Mais</w> <w n="98.2">moi</w>, <w n="98.3">ton</w> <w n="98.4">fils</w> <w n="98.5">de</w> <w n="98.6">cœur</w>, <w n="98.7">bien</w> <w n="98.8">haut</w> <w n="98.9">je</w> <w n="98.10">le</w> <w n="98.11">proclame</w> :</l>
						<l n="99" num="12.5"><w n="99.1">Jusqu</w>'<w n="99.2">au</w> <w n="99.3">dernier</w> <w n="99.4">moment</w> <w n="99.5">tu</w> <w n="99.6">n</w>'<w n="99.7">eus</w> <w n="99.8">qu</w>'<w n="99.9">un</w> <w n="99.10">vœu</w> <w n="99.11">dans</w> <w n="99.12">l</w>'<w n="99.13">âme</w>,</l>
						<l n="100" num="12.6"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="100.1">Un</w> <w n="100.2">seul</w> : <w n="100.3">vivre</w> <w n="100.4">ou</w> <w n="100.5">mourir</w> <w n="100.6">Français</w> !</l>
					</lg>
				</div>
				<div n="3" type="section">
					<head type="number">III</head>
					<lg n="1">
						<l n="101" num="1.1"><w n="101.1">Ah</w> ! <w n="101.2">celui</w> <w n="101.3">qui</w>, <w n="101.4">durant</w> <w n="101.5">cette</w> <w n="101.6">épreuve</w> <w n="101.7">abhorrée</w>,</l>
						<l n="102" num="1.2"><w n="102.1">A</w> <w n="102.2">senti</w> <w n="102.3">comme</w> <w n="102.4">moi</w> <w n="102.5">palpiter</w> <w n="102.6">sous</w> <w n="102.7">sa</w> <w n="102.8">main</w></l>
						<l n="103" num="1.3"><w n="103.1">Le</w> <w n="103.2">grand</w> <w n="103.3">cœur</w> <w n="103.4">de</w> <w n="103.5">l</w>'<w n="103.6">Alsace</w>, <w n="103.7">et</w> <w n="103.8">puis</w>, <w n="103.9">morne</w>, <w n="103.10">éplorée</w>,</l>
						<l n="104" num="1.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="104.1">L</w>'<w n="104.2">a</w> <w n="104.3">vue</w> <w n="104.4">au</w> <w n="104.5">pouvoir</w> <w n="104.6">du</w> <w n="104.7">Germain</w>,</l>
					</lg>
					<lg n="2">
						<l n="105" num="2.1"><w n="105.1">Celui</w>-<w n="105.2">là</w> <w n="105.3">vous</w> <w n="105.4">dirait</w> <w n="105.5">si</w> <w n="105.6">la</w> <w n="105.7">mère</w> <w n="105.8">patrie</w></l>
						<l n="106" num="2.2"><w n="106.1">Compte</w> <w n="106.2">un</w> <w n="106.3">seul</w> <w n="106.4">fils</w> <w n="106.5">l</w>'<w n="106.6">aimant</w> <w n="106.7">d</w>'<w n="106.8">un</w> <w n="106.9">amour</w> <w n="106.10">plus</w> <w n="106.11">puissant</w>,</l>
						<l n="107" num="2.3"><w n="107.1">Un</w> <w n="107.2">fils</w> <w n="107.3">plus</w> <w n="107.4">prompt</w>, <w n="107.5">sans</w> <w n="107.6">même</w> <w n="107.7">attendre</w> <w n="107.8">qu</w>'<w n="107.9">elle</w> <w n="107.10">prie</w>,</l>
						<l n="108" num="2.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="108.1">A</w> <w n="108.2">verser</w> <w n="108.3">pour</w> <w n="108.4">elle</w> <w n="108.5">son</w> <w n="108.6">sang</w> !</l>
					</lg>
					<lg n="3">
						<l n="109" num="3.1"><w n="109.1">Car</w>, <w n="109.2">à</w> <w n="109.3">leurs</w> <w n="109.4">yeux</w>, <w n="109.5">la</w> <w n="109.6">France</w> <w n="109.7">est</w> <w n="109.8">toujours</w> <w n="109.9">la</w> <w n="109.10">première</w></l>
						<l n="110" num="3.2"><w n="110.1">Qui</w> <w n="110.2">brisa</w> <w n="110.3">sur</w> <w n="110.4">le</w> <w n="110.5">sol</w> <w n="110.6">l</w>'<w n="110.7">ancienne</w> <w n="110.8">iniquité</w>,</l>
						<l n="111" num="3.3"><w n="111.1">Et</w> <w n="111.2">qui</w> <w n="111.3">des</w> <w n="111.4">droits</w> <w n="111.5">nouveaux</w> <w n="111.6">éleva</w> <w n="111.7">la</w> <w n="111.8">bannière</w></l>
						<l n="112" num="3.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="112.1">Aux</w> <w n="112.2">regards</w> <w n="112.3">de</w> <w n="112.4">l</w>'<w n="112.5">Humanité</w> !</l>
					</lg>
					<lg n="4">
						<l n="113" num="4.1"><w n="113.1">Celui</w>-<w n="113.2">là</w> <w n="113.3">vous</w> <w n="113.4">dirait</w> <w n="113.5">quelle</w> <w n="113.6">souffrance</w> <w n="113.7">amère</w></l>
						<l n="114" num="4.2"><w n="114.1">Fit</w> <w n="114.2">pleurer</w> <w n="114.3">ces</w> <w n="114.4">vaillants</w>, <w n="114.5">si</w> <w n="114.6">forts</w> <w n="114.7">dans</w> <w n="114.8">le</w> <w n="114.9">danger</w>,</l>
						<l n="115" num="4.3"><w n="115.1">Quand</w> <w n="115.2">vint</w> <w n="115.3">les</w> <w n="115.4">arracher</w> <w n="115.5">au</w> <w n="115.6">doux</w> <w n="115.7">sein</w> <w n="115.8">de</w> <w n="115.9">leur</w> <w n="115.10">mère</w></l>
						<l n="116" num="4.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="116.1">La</w> <w n="116.2">rude</w> <w n="116.3">main</w> <w n="116.4">de</w> <w n="116.5">l</w>'<w n="116.6">étranger</w> !</l>
					</lg>
					<lg n="5">
						<l n="117" num="5.1"><w n="117.1">Il</w> <w n="117.2">vous</w> <w n="117.3">dirait</w> <w n="117.4">combien</w>, <w n="117.5">à</w> <w n="117.6">l</w>'<w n="117.7">heure</w> <w n="117.8">douloureuse</w>,</l>
						<l n="118" num="5.2"><w n="118.1">Perd</w> <w n="118.2">la</w> <w n="118.3">France</w> <w n="118.4">en</w> <w n="118.5">perdant</w> <w n="118.6">ce</w> <w n="118.7">lambeau</w> <w n="118.8">de</w> <w n="118.9">sa</w> <w n="118.10">chair</w>,</l>
						<l n="119" num="5.3"><w n="119.1">Cette</w> <w n="119.2">robuste</w> <w n="119.3">race</w> <w n="119.4">à</w> <w n="119.5">l</w>'<w n="119.6">âme</w> <w n="119.7">généreuse</w>,</l>
						<l n="120" num="5.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="120.1">Où</w> <w n="120.2">son</w> <w n="120.3">nom</w> <w n="120.4">fut</w> <w n="120.5">toujours</w> <w n="120.6">si</w> <w n="120.7">cher</w> !</l>
					</lg>
					<lg n="6">
						<l n="121" num="6.1"><w n="121.1">O</w> <w n="121.2">France</w>, <w n="121.3">noble</w> <w n="121.4">Alsace</w> ! <w n="121.5">O</w> <w n="121.6">sœurs</w> <w n="121.7">infortunées</w> !</l>
						<l n="122" num="6.2"><w n="122.1">Est</w>-<w n="122.2">ce</w> <w n="122.3">donc</w> <w n="122.4">bien</w> <w n="122.5">fini</w> <w n="122.6">pour</w> <w n="122.7">vous</w> ? <w n="122.8">Dans</w> <w n="122.9">l</w>'<w n="122.10">avenir</w>,</l>
						<l n="123" num="6.3"><w n="123.1">A</w> <w n="123.2">vous</w> <w n="123.3">tendre</w> <w n="123.4">les</w> <w n="123.5">bras</w> <w n="123.6">serez</w>-<w n="123.7">vous</w> <w n="123.8">condamnées</w></l>
						<l n="124" num="6.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="124.1">Sans</w> <w n="124.2">pouvoir</w> <w n="124.3">jamais</w> <w n="124.4">les</w> <w n="124.5">unir</w> !</l>
					</lg>
					<lg n="7">
						<l n="125" num="7.1"><w n="125.1">Et</w> <w n="125.2">par</w>-<w n="125.3">dessus</w> <w n="125.4">les</w> <w n="125.5">monts</w>, <w n="125.6">odieuse</w> <w n="125.7">barrière</w>,</l>
						<l n="126" num="7.2"><w n="126.1">Levant</w> <w n="126.2">chacune</w> <w n="126.3">un</w> <w n="126.4">front</w> <w n="126.5">par</w> <w n="126.6">le</w> <w n="126.7">deuil</w> <w n="126.8">abattu</w>,</l>
						<l n="127" num="7.3"><w n="127.1">Vous</w> <w n="127.2">direz</w>-<w n="127.3">vous</w> <w n="127.4">toujours</w>, <w n="127.5">des</w> <w n="127.6">pleurs</w> <w n="127.7">dans</w> <w n="127.8">la</w> <w n="127.9">paupière</w></l>
						<l n="128" num="7.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space>« <w n="128.1">O</w> <w n="128.2">ma</w> <w n="128.3">sœur</w> ! <w n="128.4">c</w>'<w n="128.5">est</w> <w n="128.6">moi</w>… <w n="128.7">m</w>'<w n="128.8">entends</w>-<w n="128.9">tu</w> ? »</l>
					</lg>
					<lg n="8">
						<l n="129" num="8.1"><w n="129.1">Mais</w> <w n="129.2">non</w> ! <w n="129.3">Consolez</w>-<w n="129.4">vous</w>, <w n="129.5">ô</w> <w n="129.6">noble</w> <w n="129.7">Alsace</w>, <w n="129.8">ô</w> <w n="129.9">France</w> !</l>
						<l n="130" num="8.2"><w n="130.1">Les</w> <w n="130.2">astres</w> <w n="130.3">ne</w> <w n="130.4">sont</w> <w n="130.5">pas</w> <w n="130.6">tous</w> <w n="130.7">éteints</w> ; <w n="130.8">dans</w> <w n="130.9">vos</w> <w n="130.10">cieux</w></l>
						<l n="131" num="8.3"><w n="131.1">Il</w> <w n="131.2">en</w> <w n="131.3">reste</w> <w n="131.4">un</w> <w n="131.5">encore</w>, <w n="131.6">on</w> <w n="131.7">le</w> <w n="131.8">nomme</w> : <w n="131.9">Espérance</w>…</l>
						<l n="132" num="8.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="132.1">Ne</w> <w n="132.2">le</w> <w n="132.3">perdez</w> <w n="132.4">jamais</w> <w n="132.5">des</w> <w n="132.6">yeux</w> !</l>
					</lg>
				</div>
			</div></body></text></TEI>