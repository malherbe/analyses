<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'ANNÉE MAUDITE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="GRS">
					<name>
						<forename>Charles</forename>
						<surname>GRANDSARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2146 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'ANNÉE MAUDITE 1870-1871</title>
						<author>Charles Grandsard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61285325.r=GRANDSARD%20L%27ANN%C3%89E%20MAUDITE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'ANNÉE MAUDITE 1870-1871</title>
								<author>Charles Grandsard</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE DU PETIT JOURNAL</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRS25">
				<head type="main">VIE ET MORT</head>
				<div n="1" type="section">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">la</w> <w n="1.3">nature</w> <w n="1.4">veut</w> <w n="1.5">entretenir</w> <w n="1.6">la</w> <w n="1.7">vie</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="2.1">Qui</w> <w n="2.2">roule</w> <w n="2.3">en</w> <w n="2.4">nous</w>, <w n="2.5">fleuve</w> <w n="2.6">écumant</w>,</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">puissante</w> <w n="3.3">alchimiste</w> <w n="3.4">à</w> <w n="3.5">son</w> <w n="3.6">œuvre</w> <w n="3.7">convie</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="4.1">Les</w> <w n="4.2">forces</w> <w n="4.3">de</w> <w n="4.4">chaque</w> <w n="4.5">élément</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Pour</w> <w n="5.2">nourrir</w> <w n="5.3">notre</w> <w n="5.4">fibre</w>, <w n="5.5">elle</w> <w n="5.6">emprunte</w> <w n="5.7">à</w> <w n="5.8">la</w> <w n="5.9">terre</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="6.1">Les</w> <w n="6.2">fruits</w> <w n="6.3">que</w> <w n="6.4">son</w> <w n="6.5">sein</w> <w n="6.6">a</w> <w n="6.7">conçus</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Aux</w> <w n="7.2">animaux</w>, <w n="7.3">le</w> <w n="7.4">sang</w> <w n="7.5">qui</w> <w n="7.6">leur</w> <w n="7.7">gonfle</w> <w n="7.8">l</w>'<w n="7.9">artère</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="8.1">Leur</w> <w n="8.2">chair</w> <w n="8.3">aux</w> <w n="8.4">savoureux</w> <w n="8.5">tissus</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Elle</w> <w n="9.2">emprunte</w> <w n="9.3">au</w> <w n="9.4">soleil</w> <w n="9.5">sa</w> <w n="9.6">flamme</w> <w n="9.7">qui</w> <w n="9.8">ruisselle</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="10.1">Fauve</w>, <w n="10.2">à</w> <w n="10.3">travers</w> <w n="10.4">l</w>'<w n="10.5">immensité</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Pour</w> <w n="11.2">secouer</w> <w n="11.3">nos</w> <w n="11.4">corps</w> <w n="11.5">au</w> <w n="11.6">choc</w> <w n="11.7">de</w> <w n="11.8">l</w>'<w n="11.9">étincelle</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="12.1">Que</w> <w n="12.2">lance</w> <w n="12.3">l</w>'<w n="12.4">électricité</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Pour</w> <w n="13.2">rendre</w> <w n="13.3">à</w> <w n="13.4">notre</w> <w n="13.5">sang</w> <w n="13.6">l</w>'<w n="13.7">ardeur</w> <w n="13.8">qu</w>'<w n="13.9">il</w> <w n="13.10">a</w> <w n="13.11">perdue</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="14.1">Dans</w> <w n="14.2">son</w> <w n="14.3">impétueux</w> ,<w n="14.4">courant</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Elle</w> <w n="15.2">soutire</w> <w n="15.3">à</w> <w n="15.4">l</w>'<w n="15.5">air</w> <w n="15.6">qui</w> <w n="15.7">remplit</w> <w n="15.8">l</w>'<w n="15.9">étendue</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="16.1">Son</w> <w n="16.2">gaz</w> <w n="16.3">subtil</w> <w n="16.4">et</w> <w n="16.5">dévorant</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Enfin</w>, <w n="17.2">à</w> <w n="17.3">l</w>'<w n="17.4">eau</w> <w n="17.5">des</w> <w n="17.6">mers</w>, <w n="17.7">comme</w> <w n="17.8">à</w> <w n="17.9">celle</w> <w n="17.10">des</w> <w n="17.11">sources</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="18.1">Elle</w> <w n="18.2">prend</w> <w n="18.3">leur</w> <w n="18.4">chaux</w> <w n="18.5">et</w> <w n="18.6">leur</w> <w n="18.7">sel</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">complète</w> <w n="19.3">son</w> <w n="19.4">œuvre</w>, <w n="19.5">épuisant</w> <w n="19.6">les</w> <w n="19.7">ressources</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="20.1">Du</w> <w n="20.2">grand</w> <w n="20.3">bazar</w> <w n="20.4">universel</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">tous</w> <w n="21.3">ces</w> <w n="21.4">éléments</w>, <w n="21.5">la</w> <w n="21.6">puissante</w> <w n="21.7">sorcière</w></l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="22.1">Les</w> <w n="22.2">fond</w> <w n="22.3">dans</w> <w n="22.4">son</w> <w n="22.5">creuset</w> <w n="22.6">vital</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Et</w>, <w n="23.2">mêlant</w> <w n="23.3">ces</w> <w n="23.4">fragments</w> <w n="23.5">d</w>'<w n="23.6">une</w> <w n="23.7">vile</w> <w n="23.8">poussière</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="24.1">Elle</w> <w n="24.2">en</w> <w n="24.3">crée</w> <w n="24.4">un</w> <w n="24.5">noble</w> <w n="24.6">métal</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Ce</w> <w n="25.2">métal</w>, <w n="25.3">c</w>'<w n="25.4">est</w> <w n="25.5">la</w> <w n="25.6">chair</w> <w n="25.7">colorée</w>, <w n="25.8">éclatante</w></l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="26.1">Comme</w> <w n="26.2">la</w> <w n="26.3">mûre</w> <w n="26.4">du</w> <w n="26.5">buisson</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Qui</w> <w n="27.2">vibre</w> <w n="27.3">au</w> <w n="27.4">moindre</w> <w n="27.5">souffle</w>, <w n="27.6">et</w> <w n="27.7">frémit</w>, <w n="27.8">palpitante</w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="28.1">Dans</w> <w n="28.2">un</w> <w n="28.3">voluptueux</w> <w n="28.4">frisson</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">La</w> <w n="29.2">chair</w> <w n="29.3">au</w> <w n="29.4">galbe</w> <w n="29.5">riche</w> <w n="29.6">et</w> <w n="29.7">pulpeux</w>, <w n="29.8">que</w> <w n="29.9">sillonne</w></l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="30.1">Un</w> <w n="30.2">léger</w> <w n="30.3">réseau</w> <w n="30.4">bleuissant</w>,</l>
						<l n="31" num="8.3"><w n="31.1">La</w> <w n="31.2">chair</w> <w n="31.3">humide</w> <w n="31.4">et</w> <w n="31.5">chaude</w>, <w n="31.6">où</w> <w n="31.7">roule</w> <w n="31.8">et</w> <w n="31.9">tourbillonne</w></l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="32.1">La</w> <w n="32.2">vie</w> <w n="32.3">avec</w> <w n="32.4">un</w> <w n="32.5">flot</w> <w n="32.6">de</w> <w n="32.7">sang</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Mais</w> <w n="33.2">elle</w> <w n="33.3">veut</w> <w n="33.4">surtout</w>, <w n="33.5">la</w> <w n="33.6">grande</w> <w n="33.7">vie</w> <w n="33.8">intense</w>,</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="34.1">L</w>'<w n="34.2">accord</w> <w n="34.3">de</w> <w n="34.4">tous</w> <w n="34.5">ces</w> <w n="34.6">éléments</w> :</l>
						<l n="35" num="9.3"><w n="35.1">Chacun</w> <w n="35.2">n</w>'<w n="35.3">est</w>, <w n="35.4">pris</w> <w n="35.5">à</w> <w n="35.6">part</w>, <w n="35.7">qu</w>'<w n="35.8">une</w> <w n="35.9">aveugle</w> <w n="35.10">substance</w>,</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="36.1">Inféconde</w> <w n="36.2">en</w> <w n="36.3">ses</w> <w n="36.4">mouvements</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Or</w>, <w n="37.2">quand</w> <w n="37.3">se</w> <w n="37.4">lasse</w>, <w n="37.5">un</w> <w n="37.6">jour</w>, <w n="37.7">cette</w> <w n="37.8">main</w> <w n="37.9">souveraine</w></l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="38.1">Qui</w> <w n="38.2">les</w> <w n="38.3">fondait</w> <w n="38.4">en</w> <w n="38.5">une</w> <w n="38.6">chair</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Chacun</w> <w n="39.2">d</w>'<w n="39.3">eux</w>, <w n="39.4">libre</w> <w n="39.5">enfin</w>, <w n="39.6">veut</w> <w n="39.7">aller</w> <w n="39.8">où</w> <w n="39.9">l</w>'<w n="39.10">entraîne</w></l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="40.1">Son</w> <w n="40.2">penchant</w> <w n="40.3">primitif</w> <w n="40.4">et</w> <w n="40.5">cher</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Impatients</w> <w n="41.2">du</w> <w n="41.3">joug</w> <w n="41.4">qui</w> <w n="41.5">pliait</w> <w n="41.6">leur</w> <w n="41.7">nature</w></l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="42.1">Au</w> <w n="42.2">grand</w> <w n="42.3">dessein</w> <w n="42.4">prémédité</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Ils</w> <w n="43.2">veulent</w> <w n="43.3">dans</w> <w n="43.4">l</w>'<w n="43.5">espace</w> <w n="43.6">errer</w> <w n="43.7">à</w> <w n="43.8">l</w>'<w n="43.9">aventure</w>,</l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="44.1">Dans</w> <w n="44.2">leur</w> <w n="44.3">sauvage</w> <w n="44.4">liberté</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">L</w>'<w n="45.2">étincelle</w> <w n="45.3">ravie</w> <w n="45.4">à</w> <w n="45.5">la</w> <w n="45.6">brûlante</w> <w n="45.7">sphère</w></l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="46.1">Y</w> <w n="46.2">remonte</w> <w n="46.3">à</w> <w n="46.4">travers</w> <w n="46.5">les</w> <w n="46.6">airs</w> ;</l>
						<l n="47" num="12.3"><w n="47.1">Les</w> <w n="47.2">gaz</w>, <w n="47.3">s</w>'<w n="47.4">éparpillant</w> <w n="47.5">dans</w> <w n="47.6">l</w>'<w n="47.7">immense</w> <w n="47.8">atmosphère</w>,</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="48.1">Regagnent</w> <w n="48.2">leurs</w> <w n="48.3">vagues</w> <w n="48.4">déserts</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Les</w> <w n="49.2">liquides</w>, <w n="49.3">plongeant</w> <w n="49.4">dans</w> <w n="49.5">le</w> <w n="49.6">sein</w> <w n="49.7">de</w> <w n="49.8">la</w> <w n="49.9">terre</w>,</l>
						<l n="50" num="13.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="50.1">En</w> <w n="50.2">vont</w> <w n="50.3">humecter</w> <w n="50.4">les</w> <w n="50.5">bas</w>-<w n="50.6">fonds</w> ;</l>
						<l n="51" num="13.3"><w n="51.1">Les</w> <w n="51.2">sels</w> <w n="51.3">vont</w>, <w n="51.4">dans</w> <w n="51.5">l</w>'<w n="51.6">humus</w>, <w n="51.7">poursuivre</w> <w n="51.8">le</w> <w n="51.9">mystère</w></l>
						<l n="52" num="13.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="52.1">De</w> <w n="52.2">leurs</w> <w n="52.3">amalgames</w> <w n="52.4">profonds</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Et</w> <w n="53.2">la</w> <w n="53.3">chair</w>, <w n="53.4">dépouillant</w> <w n="53.5">sa</w> <w n="53.6">nuance</w> <w n="53.7">rosée</w>.</l>
						<l n="54" num="14.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="54.1">Rapidement</w> <w n="54.2">s</w>'<w n="54.3">altère</w> <w n="54.4">aux</w> <w n="54.5">yeux</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Devient</w> <w n="55.2">livide</w>, <w n="55.3">et</w> <w n="55.4">puis</w> <w n="55.5">noire</w>, <w n="55.6">décomposée</w></l>
						<l n="56" num="14.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="56.1">Par</w> <w n="56.2">ce</w> <w n="56.3">travail</w> <w n="56.4">silencieux</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Ses</w> <w n="57.2">éléments</w> <w n="57.3">disjoints</w>, <w n="57.4">de</w> <w n="57.5">leur</w> <w n="57.6">odeur</w> <w n="57.7">putride</w>,</l>
						<l n="58" num="15.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="58.1">Effarouchent</w> <w n="58.2">notre</w> <w n="58.3">odorat</w> ;</l>
						<l n="59" num="15.3"><w n="59.1">Il</w> <w n="59.2">n</w>'<w n="59.3">en</w> <w n="59.4">reste</w> <w n="59.5">bientôt</w> <w n="59.6">qu</w>'<w n="59.7">un</w> <w n="59.8">peu</w> <w n="59.9">de</w> <w n="59.10">poudre</w> <w n="59.11">aride</w>,</l>
						<l n="60" num="15.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="60.1">Qui</w> <w n="60.2">lentement</w> <w n="60.3">disparaîtra</w> !</l>
					</lg>
				</div>
				<div n="2" type="section">
					<head type="number">II</head>
					<lg n="1">
						<l n="61" num="1.1"><w n="61.1">Ainsi</w>, <w n="61.2">dans</w> <w n="61.3">l</w>'<w n="61.4">univers</w> <w n="61.5">quand</w> <w n="61.6">se</w> <w n="61.7">fonde</w> <w n="61.8">un</w> <w n="61.9">empire</w>,</l>
						<l n="62" num="1.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="62.1">Suivant</w> <w n="62.2">un</w> <w n="62.3">éternel</w> <w n="62.4">dessein</w>,</l>
						<l n="63" num="1.3"><w n="63.1">Pour</w> <w n="63.2">soutenir</w> <w n="63.3">sa</w> <w n="63.4">vie</w> <w n="63.5">et</w> <w n="63.6">pour</w> <w n="63.7">croître</w>, <w n="63.8">il</w> <w n="63.9">aspire</w></l>
						<l n="64" num="1.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="64.1">Tout</w> <w n="64.2">élément</w> <w n="64.3">robuste</w> <w n="64.4">et</w> <w n="64.5">sain</w>.</l>
					</lg>
					<lg n="2">
						<l n="65" num="2.1"><w n="65.1">Ces</w> <w n="65.2">éléments</w> <w n="65.3">vitaux</w>, <w n="65.4">c</w>'<w n="65.5">est</w> <w n="65.6">tout</w> <w n="65.7">ce</w> <w n="65.8">que</w> <w n="65.9">renferme</w></l>
						<l n="66" num="2.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="66.1">L</w>'<w n="66.2">âme</w> <w n="66.3">et</w> <w n="66.4">le</w> <w n="66.5">cœur</w> <w n="66.6">des</w> <w n="66.7">citoyens</w> ;</l>
						<l n="67" num="2.3"><w n="67.1">C</w>'<w n="67.2">est</w> <w n="67.3">leur</w> <w n="67.4">ardent</w> <w n="67.5">amour</w> ; <w n="67.6">c</w>'<w n="67.7">est</w> <w n="67.8">leur</w> <w n="67.9">volonté</w> <w n="67.10">ferme</w></l>
						<l n="68" num="2.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="68.1">De</w> <w n="68.2">le</w> <w n="68.3">servir</w> <w n="68.4">par</w> <w n="68.5">tous</w> <w n="68.6">moyens</w>,</l>
					</lg>
					<lg n="3">
						<l n="69" num="3.1"><w n="69.1">Ce</w> <w n="69.2">flot</w> <w n="69.3">de</w> <w n="69.4">volontés</w>, <w n="69.5">qui</w> <w n="69.6">roule</w> <w n="69.7">et</w> <w n="69.8">qui</w> <w n="69.9">bouillonne</w></l>
						<l n="70" num="3.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="70.1">Des</w> <w n="70.2">membres</w> <w n="70.3">au</w> <w n="70.4">cœur</w> <w n="70.5">languissant</w>,</l>
						<l n="71" num="3.3"><w n="71.1">Y</w> <w n="71.2">porte</w> <w n="71.3">la</w> <w n="71.4">vigueur</w>, <w n="71.5">l</w>'<w n="71.6">échauffe</w> <w n="71.7">et</w> <w n="71.8">l</w>'<w n="71.9">aiguillonne</w></l>
						<l n="72" num="3.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="72.1">Comme</w> <w n="72.2">les</w> <w n="72.3">flots</w> <w n="72.4">rouges</w> <w n="72.5">du</w> <w n="72.6">sang</w> !</l>
					</lg>
					<lg n="4">
						<l n="73" num="4.1"><w n="73.1">Dans</w> <w n="73.2">son</w> <w n="73.3">reflux</w>, <w n="73.4">alors</w>, <w n="73.5">là</w> <w n="73.6">pléthore</w> <w n="73.7">vitale</w></l>
						<l n="74" num="4.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="74.1">Retourne</w> <w n="74.2">aux</w> <w n="74.3">membres</w> <w n="74.4">surnourris</w> ;</l>
						<l n="75" num="4.3"><w n="75.1">Et</w> <w n="75.2">sur</w> <w n="75.3">le</w> <w n="75.4">corps</w> <w n="75.5">entier</w>, <w n="75.6">de</w> <w n="75.7">la</w> <w n="75.8">santé</w> <w n="75.9">s</w>'<w n="75.10">étale</w></l>
						<l n="76" num="4.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="76.1">Le</w> <w n="76.2">chaud</w> <w n="76.3">et</w> <w n="76.4">brillant</w> <w n="76.5">coloris</w>.</l>
					</lg>
					<lg n="5">
						<l n="77" num="5.1"><w n="77.1">Et</w> <w n="77.2">le</w> <w n="77.3">colosse</w>, <w n="77.4">alors</w>, <w n="77.5">atteint</w> <w n="77.6">dans</w> <w n="77.7">sa</w> <w n="77.8">croissance</w></l>
						<l n="78" num="5.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="78.1">L</w>'<w n="78.2">âge</w> <w n="78.3">de</w> <w n="78.4">la</w> <w n="78.5">virilité</w> ;</l>
						<l n="79" num="5.3"><w n="79.1">Et</w> <w n="79.2">ses</w> <w n="79.3">muscles</w> <w n="79.4">d</w>'<w n="79.5">acier</w> <w n="79.6">démontrent</w> <w n="79.7">sa</w> <w n="79.8">puissance</w>,</l>
						<l n="80" num="5.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="80.1">Et</w> <w n="80.2">font</w> <w n="80.3">sa</w> <w n="80.4">splendide</w> <w n="80.5">beauté</w> !</l>
					</lg>
					<lg n="6">
						<l n="81" num="6.1"><w n="81.1">Et</w> <w n="81.2">toutes</w> <w n="81.3">ses</w> <w n="81.4">humeurs</w>, <w n="81.5">dans</w> <w n="81.6">leur</w> <w n="81.7">juste</w> <w n="81.8">équilibre</w>,</l>
						<l n="82" num="6.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="82.1">Le</w> <w n="82.2">rendent</w> <w n="82.3">sain</w> <w n="82.4">et</w> <w n="82.5">florissant</w> ;</l>
						<l n="83" num="6.3"><w n="83.1">Et</w> <w n="83.2">son</w> <w n="83.3">vaste</w> <w n="83.4">poumon</w>, <w n="83.5">d</w>'<w n="83.6">un</w> <w n="83.7">jeu</w> <w n="83.8">facile</w> <w n="83.9">et</w> <w n="83.10">libre</w>,</l>
						<l n="84" num="6.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="84.1">Se</w> <w n="84.2">meut</w> <w n="84.3">dans</w> <w n="84.4">son</w> <w n="84.5">torse</w> <w n="84.6">puissant</w> !</l>
					</lg>
					<lg n="7">
						<l n="85" num="7.1"><w n="85.1">Si</w> <w n="85.2">d</w>'<w n="85.3">un</w> <w n="85.4">mal</w> <w n="85.5">de</w> <w n="85.6">rencontre</w> <w n="85.7">il</w> <w n="85.8">ressent</w> <w n="85.9">quelque</w> <w n="85.10">crise</w>,</l>
						<l n="86" num="7.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="86.1">Il</w> <w n="86.2">la</w> <w n="86.3">franchit</w>, <w n="86.4">ferme</w>, <w n="86.5">indompté</w> ;</l>
						<l n="87" num="7.3"><w n="87.1">Des</w> <w n="87.2">plus</w> <w n="87.3">acres</w> <w n="87.4">douleurs</w> <w n="87.5">l</w>'<w n="87.6">assaut</w> <w n="87.7">vaincu</w> <w n="87.8">se</w> <w n="87.9">brise</w></l>
						<l n="88" num="7.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="88.1">Contre</w> <w n="88.2">sa</w> <w n="88.3">robuste</w> <w n="88.4">santé</w> !</l>
					</lg>
					<lg n="8">
						<l n="89" num="8.1"><w n="89.1">Et</w> <w n="89.2">si</w> <w n="89.3">quelque</w> <w n="89.4">adversaire</w> <w n="89.5">ose</w>, <w n="89.6">dans</w> <w n="89.7">sa</w> <w n="89.8">démence</w>,</l>
						<l n="90" num="8.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="90.1">Jeter</w> <w n="90.2">au</w> <w n="90.3">colosse</w> <w n="90.4">le</w> <w n="90.5">gant</w>,</l>
						<l n="91" num="8.3"><w n="91.1">Du</w> <w n="91.2">plus</w> <w n="91.3">léger</w> <w n="91.4">effort</w> <w n="91.5">de</w> <w n="91.6">sa</w> <w n="91.7">vigueur</w> <w n="91.8">immense</w></l>
						<l n="92" num="8.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="92.1">Il</w> <w n="92.2">brise</w> <w n="92.3">à</w> <w n="92.4">ses</w> <w n="92.5">pieds</w> <w n="92.6">l</w>'<w n="92.7">arrogant</w> !</l>
					</lg>
					<lg n="9">
						<l n="93" num="9.1"><w n="93.1">Mais</w>, <w n="93.2">le</w> <w n="93.3">jour</w> <w n="93.4">où</w> <w n="93.5">ce</w> <w n="93.6">flot</w> <w n="93.7">de</w> <w n="93.8">volontés</w> <w n="93.9">fondues</w></l>
						<l n="94" num="9.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="94.1">Se</w> <w n="94.2">porte</w> <w n="94.3">au</w> <w n="94.4">centre</w> <w n="94.5">avec</w> <w n="94.6">langueur</w>,</l>
						<l n="95" num="9.3"><w n="95.1">Quand</w> <w n="95.2">elles</w> <w n="95.3">ont</w> <w n="95.4">cessé</w> <w n="95.5">d</w>'<w n="95.6">être</w> <w n="95.7">vers</w> <w n="95.8">lui</w> <w n="95.9">tendues</w></l>
						<l n="96" num="9.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="96.1">Pour</w> <w n="96.2">entretenir</w> <w n="96.3">sa</w> <w n="96.4">vigueur</w>,</l>
					</lg>
					<lg n="10">
						<l n="97" num="10.1"><w n="97.1">Quand</w> <w n="97.2">tous</w> <w n="97.3">ces</w> <w n="97.4">éléments</w> <w n="97.5">qui</w> <w n="97.6">forment</w> <w n="97.7">sa</w> <w n="97.8">substance</w></l>
						<l n="98" num="10.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="98.1">Entre</w> <w n="98.2">eux</w> <w n="98.3">ont</w> <w n="98.4">rompu</w> <w n="98.5">tout</w> <w n="98.6">lien</w>,</l>
						<l n="99" num="10.3"><w n="99.1">Que</w> <w n="99.2">chacun</w>, <w n="99.3">s</w>'<w n="99.4">isolant</w> <w n="99.5">dans</w> <w n="99.6">sa</w> <w n="99.7">propre</w> <w n="99.8">existence</w>,</l>
						<l n="100" num="10.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="100.1">Cherche</w> <w n="100.2">son</w> <w n="100.3">égoïste</w> <w n="100.4">bien</w>.</l>
					</lg>
					<lg n="11">
						<l n="101" num="11.1"><w n="101.1">Tous</w> <w n="101.2">les</w> <w n="101.3">signes</w> <w n="101.4">hideux</w> <w n="101.5">de</w> <w n="101.6">la</w> <w n="101.7">décrépitude</w></l>
						<l n="102" num="11.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="102.1">Éclatent</w> <w n="102.2">sur</w> <w n="102.3">ce</w> <w n="102.4">corps</w> <w n="102.5">géant</w> ; '</l>
						<l n="103" num="11.3"><w n="103.1">Et</w> <w n="103.2">ceux</w> <w n="103.3">qui</w>, <w n="103.4">de</w> <w n="103.5">le</w> <w n="103.6">craindre</w>, <w n="103.7">avaient</w> <w n="103.8">pris</w> <w n="103.9">l</w>'<w n="103.10">habitude</w>,</l>
						<l n="104" num="11.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="104.1">Insultent</w> <w n="104.2">ce</w> <w n="104.3">prochain</w> <w n="104.4">néant</w> !</l>
					</lg>
					<lg n="12">
						<l n="105" num="12.1"><w n="105.1">Et</w> <w n="105.2">sa</w> <w n="105.3">chair</w> <w n="105.4">a</w> <w n="105.5">perdu</w> <w n="105.6">cette</w> <w n="105.7">couleur</w> <w n="105.8">vitale</w></l>
						<l n="106" num="12.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="106.1">Qui</w> <w n="106.2">jadis</w> <w n="106.3">la</w> <w n="106.4">fit</w> <w n="106.5">resplendir</w> ;</l>
						<l n="107" num="12.3"><w n="107.1">La</w> <w n="107.2">putréfaction</w>, <w n="107.3">de</w> <w n="107.4">sa</w> <w n="107.5">teinte</w> <w n="107.6">fatale</w>,</l>
						<l n="108" num="12.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="108.1">Vient</w> <w n="108.2">la</w> <w n="108.3">souiller</w> <w n="108.4">et</w> <w n="108.5">l</w>'<w n="108.6">enlaidir</w> !</l>
					</lg>
					<lg n="13">
						<l n="109" num="13.1"><w n="109.1">Indices</w> <w n="109.2">des</w> <w n="109.3">dégâts</w> <w n="109.4">que</w> <w n="109.5">font</w> <w n="109.6">dans</w> <w n="109.7">ses</w> <w n="109.8">viscères</w></l>
						<l n="110" num="13.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="110.1">Tous</w> <w n="110.2">ses</w> <w n="110.3">atomes</w> <w n="110.4">en</w> <w n="110.5">discords</w>,</l>
						<l n="111" num="13.3"><w n="111.1">Les</w> <w n="111.2">vices</w> <w n="111.3">monstrueux</w>, <w n="111.4">ces</w> <w n="111.5">fétides</w> <w n="111.6">ulcères</w>,</l>
						<l n="112" num="13.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="112.1">S</w>'<w n="112.2">étalent</w> <w n="112.3">partout</w> <w n="112.4">sur</w> <w n="112.5">son</w> <w n="112.6">corps</w> !</l>
					</lg>
					<lg n="14">
						<l n="113" num="14.1"><w n="113.1">Puis</w>, <w n="113.2">ses</w> <w n="113.3">chairs</w>, <w n="113.4">s</w>'<w n="113.5">égrenant</w>, <w n="113.6">se</w> <w n="113.7">fondent</w>, <w n="113.8">se</w> <w n="113.9">séparent</w></l>
						<l n="114" num="14.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="114.1">En</w> <w n="114.2">lambeaux</w> <w n="114.3">visqueux</w> <w n="114.4">et</w> <w n="114.5">flétris</w></l>
						<l n="115" num="14.3"><w n="115.1">Qui</w> <w n="115.2">révoltent</w> <w n="115.3">la</w> <w n="115.4">vue</w> ; <w n="115.5">et</w> <w n="115.6">les</w> <w n="115.7">vers</w> <w n="115.8">s</w>'<w n="115.9">en</w> <w n="115.10">emparent</w></l>
						<l n="116" num="14.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="116.1">Et</w> <w n="116.2">rongent</w> <w n="116.3">ces</w> <w n="116.4">hideux</w> <w n="116.5">débris</w> !</l>
					</lg>
					<lg n="15">
						<l n="117" num="15.1"><w n="117.1">Puis</w>, <w n="117.2">à</w> <w n="117.3">sa</w> <w n="117.4">place</w>, <w n="117.5">on</w> <w n="117.6">voit</w> <w n="117.7">des</w> <w n="117.8">ruines</w>, <w n="117.9">poussière</w></l>
						<l n="118" num="15.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="118.1">De</w> <w n="118.2">tout</w> <w n="118.3">empire</w> <w n="118.4">évanoui</w> ;</l>
						<l n="119" num="15.3"><w n="119.1">Et</w> <w n="119.2">l</w>'<w n="119.3">étranger</w> <w n="119.4">qui</w> <w n="119.5">passe</w> <w n="119.6">écrit</w> <w n="119.7">sur</w> <w n="119.8">une</w> <w n="119.9">pierre</w> :</l>
						<l n="120" num="15.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space>« <w n="120.1">Un</w> <w n="120.2">peuple</w>, <w n="120.3">ici</w>, <w n="120.4">dort</w> <w n="120.5">enfoui</w> ! »</l>
					</lg>
				</div>
			</div></body></text></TEI>