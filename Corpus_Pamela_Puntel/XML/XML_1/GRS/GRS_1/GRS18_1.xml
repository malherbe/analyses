<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'ANNÉE MAUDITE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="GRS">
					<name>
						<forename>Charles</forename>
						<surname>GRANDSARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2146 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'ANNÉE MAUDITE 1870-1871</title>
						<author>Charles Grandsard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61285325.r=GRANDSARD%20L%27ANN%C3%89E%20MAUDITE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'ANNÉE MAUDITE 1870-1871</title>
								<author>Charles Grandsard</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE DU PETIT JOURNAL</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRS18">
				<head type="main">LE CADAVRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Sur</w> <w n="1.2">une</w> <w n="1.3">table</w> <w n="1.4">il</w> <w n="1.5">gît</w>, <w n="1.6">livide</w>, <w n="1.7">à</w> <w n="1.8">la</w> <w n="1.9">lumière</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="2.1">Que</w> <w n="2.2">lui</w> <w n="2.3">jette</w> <w n="2.4">un</w> <w n="2.5">pâle</w> <w n="2.6">flambeau</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Ses</w> <w n="3.2">traits</w> <w n="3.3">couleur</w> <w n="3.4">de</w> <w n="3.5">cire</w>, <w n="3.6">et</w> <w n="3.7">durs</w> <w n="3.8">comme</w> <w n="3.9">la</w> <w n="3.10">pierre</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="4.1">Semblent</w> <w n="4.2">sculptés</w> <w n="4.3">pour</w> <w n="4.4">un</w> <w n="4.5">tombeau</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">s</w>'<w n="5.3">exhale</w> <w n="5.4">de</w> <w n="5.5">lui</w> <w n="5.6">du</w> <w n="5.7">néant</w>, <w n="5.8">du</w> <w n="5.9">silence</w>,</l>
					<l n="6" num="2.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="6.1">Un</w> <w n="6.2">calme</w> <w n="6.3">si</w> <w n="6.4">raide</w> <w n="6.5">et</w> <w n="6.6">si</w> <w n="6.7">froid</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Que</w> <w n="7.2">le</w> <w n="7.3">frisson</w> <w n="7.4">vous</w> <w n="7.5">prend</w>, <w n="7.6">et</w> <w n="7.7">que</w> .<w n="7.8">l</w>'<w n="7.9">âme</w> <w n="7.10">balance</w></l>
					<l n="8" num="2.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="8.1">Entre</w> <w n="8.2">la</w> <w n="8.3">douleur</w> <w n="8.4">et</w> <w n="8.5">l</w>'<w n="8.6">effroi</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Voyez</w> : <w n="9.2">le</w> <w n="9.3">cœur</w> <w n="9.4">glacé</w> <w n="9.5">vient</w> <w n="9.6">soudain</w> <w n="9.7">de</w> <w n="9.8">se</w> <w n="9.9">taire</w> :</l>
					<l n="10" num="3.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="10.1">On</w> <w n="10.2">n</w>'<w n="10.3">entend</w> <w n="10.4">plus</w> <w n="10.5">son</w> <w n="10.6">battement</w> ;</l>
					<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">cesse</w> <w n="11.3">de</w> <w n="11.4">lancer</w> <w n="11.5">la</w> <w n="11.6">vie</w> <w n="11.7">en</w> <w n="11.8">chaque</w> <w n="11.9">artère</w></l>
					<l n="12" num="3.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="12.1">Avec</w> <w n="12.2">le</w> <w n="12.3">sang</w> <w n="12.4">rouge</w>, <w n="12.5">écumant</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">membres</w> <w n="13.3">ont</w> <w n="13.4">gardé</w> <w n="13.5">l</w>'<w n="13.6">apparence</w> <w n="13.7">vitale</w> ;</l>
					<l n="14" num="4.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="14.1">Ils</w> <w n="14.2">sont</w> <w n="14.3">encor</w> <w n="14.4">fermes</w> <w n="14.5">à</w> <w n="14.6">voir</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">On</w> <w n="15.2">dirait</w> <w n="15.3">qu</w>'<w n="15.4">échappant</w> <w n="15.5">à</w> <w n="15.6">l</w>'<w n="15.7">étreinte</w> <w n="15.8">fatale</w></l>
					<l n="16" num="4.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="16.1">Pour</w> <w n="16.2">agir</w> <w n="16.3">ils</w> <w n="16.4">vont</w> <w n="16.5">se</w> <w n="16.6">mouvoir</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Mais</w>, <w n="17.2">ne</w> <w n="17.3">recevant</w> <w n="17.4">plus</w> <w n="17.5">la</w> <w n="17.6">sève</w> <w n="17.7">nourricière</w>,</l>
					<l n="18" num="5.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="18.1">Leurs</w> <w n="18.2">chairs</w> <w n="18.3">vont</w> <w n="18.4">mollir</w>, <w n="18.5">s</w>'<w n="18.6">affaisser</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Puis</w>, <w n="19.2">enfin</w>, <w n="19.3">se</w> <w n="19.4">résoudre</w> <w n="19.5">en</w> <w n="19.6">l</w>'<w n="19.7">immonde</w> <w n="19.8">poussière</w></l>
					<l n="20" num="5.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="20.1">A</w> <w n="20.2">laquelle</w> <w n="20.3">on</w> <w n="20.4">n</w>'<w n="20.5">ose</w> <w n="20.6">penser</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Vous</w> <w n="21.2">qui</w>, <w n="21.3">dans</w> <w n="21.4">l</w>'<w n="21.5">heure</w> <w n="21.6">sombre</w> <w n="21.7">où</w> <w n="21.8">râle</w> <w n="21.9">la</w> <w n="21.10">patrie</w>,</l>
					<l n="22" num="6.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="22.1">Pour</w> <w n="22.2">vous</w> <w n="22.3">seuls</w>, <w n="22.4">vous</w> <w n="22.5">inquiétez</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Venez</w> <w n="23.2">voir</w> <w n="23.3">de</w> <w n="23.4">plus</w> <w n="23.5">près</w> <w n="23.6">cette</w> <w n="23.7">face</w> <w n="23.8">flétrie</w> ;</l>
					<l n="24" num="6.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="24.1">Venez</w>, <w n="24.2">vous</w> <w n="24.3">dis</w>-<w n="24.4">je</w>, <w n="24.5">et</w> <w n="24.6">méditez</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Vous</w> <w n="25.2">êtes</w>, <w n="25.3">après</w> <w n="25.4">tout</w>, <w n="25.5">les</w> <w n="25.6">membres</w> <w n="25.7">de</w> <w n="25.8">la</w> <w n="25.9">France</w> !</l>
					<l n="26" num="7.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="26.1">Quand</w> <w n="26.2">le</w> <w n="26.3">sang</w> <w n="26.4">lui</w> <w n="26.5">coule</w> <w n="26.6">du</w> <w n="26.7">cœur</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Au</w> <w n="27.2">lieu</w> <w n="27.3">de</w> <w n="27.4">le</w> <w n="27.5">voir</w> <w n="27.6">fuir</w> <w n="27.7">avec</w> <w n="27.8">indifférence</w>,</l>
					<l n="28" num="7.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="28.1">Rendez</w>-<w n="28.2">lui</w> <w n="28.3">la</w> <w n="28.4">rouge</w> <w n="28.5">liqueur</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Son</w> <w n="29.2">sang</w>, <w n="29.3">c</w>'<w n="29.4">est</w> <w n="29.5">votre</w> <w n="29.6">amour</w>, <w n="29.7">c</w>'<w n="29.8">est</w> <w n="29.9">votre</w> <w n="29.10">intelligence</w>,</l>
					<l n="30" num="8.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="30.1">Votre</w> <w n="30.2">or</w>, <w n="30.3">que</w> <w n="30.4">des</w> <w n="30.5">yeux</w> <w n="30.6">vous</w> <w n="30.7">couvez</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">Dans</w> <w n="31.2">le</w> <w n="31.3">mal</w> <w n="31.4">qui</w> <w n="31.5">la</w> <w n="31.6">tue</w>, <w n="31.7">et</w> <w n="31.8">dans</w> <w n="31.9">son</w> <w n="31.10">indigence</w>,</l>
					<l n="32" num="8.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="32.1">Voilà</w> <w n="32.2">ce</w> <w n="32.3">que</w> <w n="32.4">vous</w> <w n="32.5">lui</w> <w n="32.6">devez</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Ici</w>, <w n="33.2">toute</w> <w n="33.3">réserve</w> <w n="33.4">est</w> <w n="33.5">lâche</w>, <w n="33.6">superflue</w> !</l>
					<l n="34" num="9.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="34.1">Que</w>, <w n="34.2">des</w> <w n="34.3">extrémités</w> <w n="34.4">au</w> <w n="34.5">cœur</w>,</l>
					<l n="35" num="9.3"><w n="35.1">A</w> <w n="35.2">torrents</w> <w n="35.3">écumeux</w> <w n="35.4">le</w> <w n="35.5">suc</w> <w n="35.6">vital</w> <w n="35.7">afflue</w>,</l>
					<l n="36" num="9.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="36.1">Ou</w> <w n="36.2">le</w> <w n="36.3">néant</w> <w n="36.4">sera</w> <w n="36.5">vainqueur</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">quand</w> <w n="37.3">la</w> <w n="37.4">vie</w>, <w n="37.5">en</w> <w n="37.6">elle</w>, <w n="37.7">enfin</w> <w n="37.8">sera</w> <w n="37.9">tarie</w>,</l>
					<l n="38" num="10.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="38.1">Espérez</w>-<w n="38.2">vous</w> <w n="38.3">donc</w>, <w n="38.4">insensés</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Pouvoir</w> <w n="39.2">encore</w>, <w n="39.3">vous</w>, <w n="39.4">membres</w> <w n="39.5">de</w> <w n="39.6">la</w> <w n="39.7">patrie</w>,</l>
					<l n="40" num="10.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="40.1">Vivre</w> <w n="40.2">sur</w> <w n="40.3">vos</w> <w n="40.4">biens</w> <w n="40.5">entassés</w> ?</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Non</w> ! <w n="41.2">des</w> <w n="41.3">membres</w> <w n="41.4">au</w> <w n="41.5">cœur</w>, <w n="41.6">la</w> <w n="41.7">vie</w> <w n="41.8">est</w> <w n="41.9">solidaire</w> !</l>
					<l n="42" num="11.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="42.1">Aussi</w>, <w n="42.2">lorsqu</w>'<w n="42.3">il</w> <w n="42.4">s</w>'<w n="42.5">arrêtera</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Que</w> <w n="43.2">leur</w> <w n="43.3">destruction</w> <w n="43.4">se</w> <w n="43.5">hâte</w> <w n="43.6">ou</w> <w n="43.7">se</w> <w n="43.8">modère</w>,</l>
					<l n="44" num="11.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="44.1">Leur</w> <w n="44.2">tour</w>, <w n="44.3">infaillible</w>, <w n="44.4">viendra</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Oui</w> ! <w n="45.2">ne</w> <w n="45.3">recevant</w> <w n="45.4">plus</w> <w n="45.5">la</w> <w n="45.6">sève</w> <w n="45.7">nourricière</w>,</l>
					<l n="46" num="12.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="46.1">Vous</w> <w n="46.2">allez</w> <w n="46.3">tous</w> <w n="46.4">vous</w> <w n="46.5">affaisser</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Puis</w>, <w n="47.2">enfin</w>, <w n="47.3">vous</w> <w n="47.4">résoudre</w> <w n="47.5">en</w> <w n="47.6">l</w>'<w n="47.7">immonde</w> <w n="47.8">poussière</w></l>
					<l n="48" num="12.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="48.1">A</w> <w n="48.2">laquelle</w> <w n="48.3">on</w> <w n="48.4">n</w>'<w n="48.5">ose</w> <w n="48.6">penser</w> !</l>
				</lg>
			</div></body></text></TEI>