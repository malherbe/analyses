<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'ANNÉE MAUDITE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="GRS">
					<name>
						<forename>Charles</forename>
						<surname>GRANDSARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2146 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'ANNÉE MAUDITE 1870-1871</title>
						<author>Charles Grandsard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61285325.r=GRANDSARD%20L%27ANN%C3%89E%20MAUDITE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'ANNÉE MAUDITE 1870-1871</title>
								<author>Charles Grandsard</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE DU PETIT JOURNAL</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRS17">
				<head type="main">LE CATÉCHISME DU BON FRANÇAIS</head>
				<lg n="1">
					<l part="I" n="1" num="1.1">— <w n="1.1">A</w> <w n="1.2">quoi</w> <w n="1.3">reconnaît</w>-<w n="1.4">on</w> <w n="1.5">le</w> <w n="1.6">bon</w> <w n="1.7">Français</w> ? </l>
					<l part="F" n="1" num="1.1">— <w n="1.8">Sa</w> <w n="1.9">mine</w></l>
					<l n="2" num="1.2"><w n="2.1">Est</w> <w n="2.2">sévère</w>, <w n="2.3">et</w> <w n="2.4">trahit</w> <w n="2.5">la</w> <w n="2.6">douleur</w> <w n="2.7">qui</w> <w n="2.8">le</w> <w n="2.9">mine</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">sourire</w> <w n="3.3">jamais</w> <w n="3.4">ne</w> <w n="3.5">brille</w> <w n="3.6">dans</w> <w n="3.7">ses</w> <w n="3.8">yeux</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Mais</w> <w n="4.2">par</w> <w n="4.3">la</w> <w n="4.4">foule</w> <w n="4.5">il</w> <w n="4.6">va</w>, <w n="4.7">concentré</w>, <w n="4.8">sérieux</w>.</l>
					<l n="5" num="1.5"><w n="5.1">On</w> <w n="5.2">ne</w> <w n="5.3">le</w> <w n="5.4">voit</w> <w n="5.5">jamais</w> <w n="5.6">s</w>'<w n="5.7">égayer</w> <w n="5.8">dans</w> <w n="5.9">les</w> <w n="5.10">fêtes</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">Mais</w> <w n="6.2">il</w> <w n="6.3">vit</w> <w n="6.4">à</w> <w n="6.5">l</w>'<w n="6.6">écart</w>, <w n="6.7">comme</w> <w n="6.8">ces</w> <w n="6.9">vieux</w> <w n="6.10">prophètes</w></l>
					<l n="7" num="1.7"><w n="7.1">Que</w> <w n="7.2">les</w> <w n="7.3">peuples</w> <w n="7.4">surpris</w> <w n="7.5">voyaient</w> <w n="7.6">de</w> <w n="7.7">loin</w> <w n="7.8">passer</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Pâles</w>, <w n="8.2">courbant</w> <w n="8.3">le</w> <w n="8.4">front</w> <w n="8.5">sous</w> <w n="8.6">un</w> <w n="8.7">grave</w> <w n="8.8">penser</w> !</l>
					<l part="I" n="9" num="1.9">— <w n="9.1">A</w> <w n="9.2">quoi</w> <w n="9.3">pense</w> <w n="9.4">le</w> <w n="9.5">bon</w> <w n="9.6">Français</w> ? </l>
					<l part="F" n="9" num="1.9">— <w n="9.7">A</w> <w n="9.8">sa</w> <w n="9.9">patrie</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Dont</w> <w n="10.2">l</w>'<w n="10.3">image</w> <w n="10.4">livide</w> <w n="10.5">et</w> <w n="10.6">par</w> <w n="10.7">le</w> <w n="10.8">deuil</w> <w n="10.9">flétrie</w></l>
					<l n="11" num="1.11"><w n="11.1">Comme</w> <w n="11.2">un</w> <w n="11.3">spectre</w> <w n="11.4">plaintif</w> <w n="11.5">en</w> <w n="11.6">tout</w> <w n="11.7">lieu</w> <w n="11.8">le</w> <w n="11.9">poursuit</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">même</w> <w n="12.3">vient</w> <w n="12.4">hanter</w> <w n="12.5">ses</w> <w n="12.6">rêves</w> <w n="12.7">de</w> <w n="12.8">la</w> <w n="12.9">nuit</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Sur</w> <w n="13.2">ce</w> <w n="13.3">corps</w> <w n="13.4">vénérable</w> <w n="13.5">et</w> <w n="13.6">noir</w> <w n="13.7">de</w> <w n="13.8">meurtrissures</w></l>
					<l n="14" num="1.14"><w n="14.1">Avec</w> <w n="14.2">un</w> <w n="14.3">doigt</w> <w n="14.4">pieux</w> <w n="14.5">il</w> <w n="14.6">compte</w> <w n="14.7">les</w> <w n="14.8">blessures</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">En</w> <w n="15.2">songeant</w> <w n="15.3">qu</w>'<w n="15.4">il</w> <w n="15.5">n</w>'<w n="15.6">a</w> <w n="15.7">pu</w> <w n="15.8">la</w> <w n="15.9">sauver</w>, <w n="15.10">le</w> <w n="15.11">remord</w></l>
					<l n="16" num="1.16"><w n="16.1">Le</w> <w n="16.2">déchire</w>, <w n="16.3">et</w> <w n="16.4">le</w> <w n="16.5">rend</w> <w n="16.6">triste</w> <w n="16.7">jusqu</w>'<w n="16.8">à</w> <w n="16.9">la</w> <w n="16.10">mort</w> !</l>
				</lg>
				<lg n="2">
					<l part="I" n="17" num="2.1">— <w n="17.1">Que</w> <w n="17.2">désire</w> <w n="17.3">le</w> <w n="17.4">bon</w> <w n="17.5">Français</w> ? </l>
					<l part="F" n="17" num="2.1">— <w n="17.6">La</w> <w n="17.7">délivrance</w></l>
					<l n="18" num="2.2"><w n="18.1">De</w> <w n="18.2">sa</w> <w n="18.3">chère</w> <w n="18.4">patrie</w>. <w n="18.5">Il</w> <w n="18.6">veut</w> <w n="18.7">revoir</w> <w n="18.8">la</w> <w n="18.9">France</w>,</l>
					<l n="19" num="2.3"><w n="19.1">Après</w> <w n="19.2">tant</w> <w n="19.3">de</w> <w n="19.4">douleurs</w> <w n="19.5">et</w> <w n="19.6">de</w> <w n="19.7">convulsions</w>.</l>
					<l n="20" num="2.4"><w n="20.1">Au</w> <w n="20.2">rang</w> <w n="20.3">qui</w> <w n="20.4">lui</w> <w n="20.5">revient</w> <w n="20.6">parmi</w> <w n="20.7">les</w> <w n="20.8">nations</w>.</l>
					<l n="21" num="2.5"><w n="21.1">Voilà</w> <w n="21.2">ce</w> <w n="21.3">qu</w>'<w n="21.4">il</w> <w n="21.5">paierait</w> <w n="21.6">même</w> <w n="21.7">au</w> <w n="21.8">prix</w> <w n="21.9">du</w> <w n="21.10">supplice</w>.</l>
					<l n="22" num="2.6"><w n="22.1">Oui</w> ! <w n="22.2">si</w>, <w n="22.3">pour</w> <w n="22.4">obtenir</w> <w n="22.5">que</w> <w n="22.6">son</w> <w n="22.7">vœu</w> <w n="22.8">s</w>'<w n="22.9">accomplisse</w>,</l>
					<l n="23" num="2.7"><w n="23.1">On</w> <w n="23.2">ne</w> <w n="23.3">lui</w> <w n="23.4">demandait</w> <w n="23.5">que</w> <w n="23.6">de</w> <w n="23.7">donner</w> <w n="23.8">son</w> <w n="23.9">sang</w>,</l>
					<l n="24" num="2.8"><w n="24.1">On</w> <w n="24.2">pourrait</w> <w n="24.3">préparer</w> <w n="24.4">la</w> <w n="24.5">hache</w> : <w n="24.6">il</w> <w n="24.7">y</w> <w n="24.8">consent</w> !</l>
				</lg>
				<lg n="3">
					<l part="I" n="25" num="3.1">— <w n="25.1">Qu</w>'<w n="25.2">aime</w> <w n="25.3">le</w> <w n="25.4">bon</w> <w n="25.5">Français</w> ? </l>
					<l part="F" n="25" num="3.1">— <w n="25.6">Toute</w> <w n="25.7">âme</w> <w n="25.8">généreuse</w></l>
					<l n="26" num="3.2"><w n="26.1">Qui</w> <w n="26.2">gémit</w> <w n="26.3">sur</w> <w n="26.4">les</w> <w n="26.5">maux</w> <w n="26.6">de</w> <w n="26.7">l</w>'<w n="26.8">heure</w> <w n="26.9">douloureuse</w>,</l>
					<l n="27" num="3.3"><w n="27.1">Qui</w> <w n="27.2">saurait</w> <w n="27.3">comme</w> <w n="27.4">lui</w>, <w n="27.5">s</w>'<w n="27.6">il</w> <w n="27.7">le</w> <w n="27.8">fallait</w>, <w n="27.9">offrir</w></l>
					<l n="28" num="3.4"><w n="28.1">Jusqu</w>'<w n="28.2">à</w> <w n="28.3">sa</w> <w n="28.4">propre</w> <w n="28.5">vie</w>, <w n="28.6">afin</w> <w n="28.7">de</w> <w n="28.8">les</w> <w n="28.9">guérir</w>.</l>
					<l part="I" n="29" num="3.5">— <w n="29.1">Que</w> <w n="29.2">hait</w> <w n="29.3">le</w> <w n="29.4">bon</w> <w n="29.5">Français</w> ? </l>
					<l part="F" n="29" num="3.5">— <w n="29.6">L</w>'<w n="29.7">égoïste</w>, <w n="29.8">le</w> <w n="29.9">lâche</w>,</l>
					<l n="30" num="3.6"><w n="30.1">Tous</w> <w n="30.2">ces</w> <w n="30.3">hommes</w> <w n="30.4">sans</w> <w n="30.5">cœur</w>, <w n="30.6">qui</w>, <w n="30.7">rêvant</w> <w n="30.8">sans</w> <w n="30.9">relâche</w></l>
					<l n="31" num="3.7"><w n="31.1">A</w> <w n="31.2">leurs</w> <w n="31.3">mesquins</w> <w n="31.4">plaisirs</w>, <w n="31.5">à</w> <w n="31.6">leur</w> <w n="31.7">vil</w> <w n="31.8">intérêt</w>,</l>
					<l n="32" num="3.8"><w n="32.1">N</w>'<w n="32.2">ont</w> <w n="32.3">pour</w> <w n="32.4">leur</w> <w n="32.5">mère</w> <w n="32.6">en</w> <w n="32.7">deuil</w> <w n="32.8">ni</w> <w n="32.9">penser</w>, <w n="32.10">ni</w> <w n="32.11">regret</w> !</l>
					<l n="33" num="3.9">— <w n="33.1">Que</w> <w n="33.2">demande</w> <w n="33.3">le</w> <w n="33.4">bon</w> <w n="33.5">Français</w> <w n="33.6">dans</w> <w n="33.7">sa</w> <w n="33.8">prière</w> ?</l>
					<l n="34" num="3.10">— <w n="34.1">Chaque</w> <w n="34.2">soir</w>, <w n="34.3">au</w> <w n="34.4">moment</w> <w n="34.5">de</w> <w n="34.6">fermer</w> <w n="34.7">la</w> <w n="34.8">paupière</w>,</l>
					<l n="35" num="3.11"><w n="35.1">Il</w> <w n="35.2">se</w> <w n="35.3">recueille</w>, <w n="35.4">au</w> <w n="35.5">fond</w> <w n="35.6">de</w> <w n="35.7">son</w> <w n="35.8">âme</w> <w n="35.9">descend</w>,</l>
					<l n="36" num="3.12"><w n="36.1">Puis</w>, <w n="36.2">élève</w> <w n="36.3">ces</w> <w n="36.4">vœux</w> <w n="36.5">aux</w> <w n="36.6">pieds</w> <w n="36.7">du</w> <w n="36.8">Tout</w>-<w n="36.9">Puissant</w> :</l>
					<l n="37" num="3.13">« <w n="37.1">Notre</w> <w n="37.2">Père</w> <w n="37.3">des</w> <w n="37.4">cieux</w> ! <w n="37.5">abaissez</w>, <w n="37.6">je</w> <w n="37.7">vous</w> <w n="37.8">prie</w>,</l>
					<l n="38" num="3.14"><w n="38.1">Un</w> <w n="38.2">regard</w> <w n="38.3">de</w> <w n="38.4">pitié</w> <w n="38.5">sur</w> <w n="38.6">ma</w> <w n="38.7">pauvre</w> <w n="38.8">patrie</w> !</l>
					<l n="39" num="3.15"><w n="39.1">Vous</w> <w n="39.2">le</w> <w n="39.3">savez</w>, <w n="39.4">son</w> <w n="39.5">cœur</w> <w n="39.6">fut</w> <w n="39.7">toujours</w> <w n="39.8">bon</w> <w n="39.9">et</w> <w n="39.10">grand</w>,</l>
					<l n="40" num="3.16"><w n="40.1">Et</w> <w n="40.2">ne</w> <w n="40.3">méritait</w> <w n="40.4">pas</w> <w n="40.5">un</w> <w n="40.6">malheur</w> <w n="40.7">si</w> <w n="40.8">navrant</w> !</l>
					<l n="41" num="3.17"><w n="41.1">Calmez</w>, <w n="41.2">Seigneur</w> ! <w n="41.3">le</w> <w n="41.4">sang</w> <w n="41.5">qui</w> <w n="41.6">coule</w> <w n="41.7">dans</w> <w n="41.8">ses</w> <w n="41.9">veines</w> ;</l>
					<l n="42" num="3.18"><w n="42.1">Préservez</w>-<w n="42.2">la</w>, <w n="42.3">surtout</w>, <w n="42.4">de</w> <w n="42.5">ces</w> <w n="42.6">colères</w> <w n="42.7">vaines</w></l>
					<l n="43" num="3.19"><w n="43.1">Dont</w> <w n="43.2">le</w> <w n="43.3">souffle</w> <w n="43.4">orageux</w> <w n="43.5">l</w>'<w n="43.6">agite</w> <w n="43.7">trop</w> <w n="43.8">souvent</w>,</l>
					<l n="44" num="3.20"><w n="44.1">Comme</w> <w n="44.2">le</w> <w n="44.3">peuplier</w> <w n="44.4">qui</w> <w n="44.5">tremble</w> <w n="44.6">au</w> <w n="44.7">moindre</w> <w n="44.8">vent</w>,</l>
					<l n="45" num="3.21"><w n="45.1">Et</w>, <w n="45.2">l</w>'<w n="45.3">entraînant</w> <w n="45.4">bien</w> <w n="45.5">loin</w> <w n="45.6">de</w> <w n="45.7">son</w> <w n="45.8">but</w> <w n="45.9">légitime</w>,</l>
					<l n="46" num="3.22"><w n="46.1">De</w> <w n="46.2">ses</w> <w n="46.3">emportements</w> <w n="46.4">font</w> <w n="46.5">d</w>'<w n="46.6">elle</w> <w n="46.7">la</w> <w n="46.8">victime</w> ! »</l>
				</lg>
				<lg n="4">
					<l part="I" n="47" num="4.1">— <w n="47.1">Où</w> <w n="47.2">trouve</w>-<w n="47.3">t</w>-<w n="47.4">on</w> <w n="47.5">de</w> <w n="47.6">bons</w> <w n="47.7">Français</w> ? </l>
					<l part="F" n="47" num="4.1">— <w n="47.8">Aux</w> <w n="47.9">bords</w> <w n="47.10">du</w> <w n="47.11">Rhin</w>.</l>
					<l n="48" num="4.2"><w n="48.1">Sur</w> <w n="48.2">la</w> <w n="48.3">Moselle</w>, <w n="48.4">il</w> <w n="48.5">est</w> <w n="48.6">de</w> <w n="48.7">ces</w> <w n="48.8">âmes</w> <w n="48.9">d</w>'<w n="48.10">airain</w></l>
					<l n="49" num="4.3"><w n="49.1">Qui</w> <w n="49.2">jettent</w> <w n="49.3">fièrement</w> <w n="49.4">le</w> <w n="49.5">saint</w> <w n="49.6">nom</w> <w n="49.7">de</w> <w n="49.8">la</w> <w n="49.9">France</w></l>
					<l n="50" num="4.4"><w n="50.1">Au</w> <w n="50.2">Germain</w>, <w n="50.3">stupéfait</w> <w n="50.4">de</w> <w n="50.5">leur</w> <w n="50.6">mâle</w> <w n="50.7">assurance</w> ;</l>
					<l n="51" num="4.5"><w n="51.1">Qui</w>, <w n="51.2">contre</w> <w n="51.3">la</w> <w n="51.4">douceur</w> <w n="51.5">ou</w> <w n="51.6">la</w> <w n="51.7">brutalité</w>,</l>
					<l n="52" num="4.6"><w n="52.1">Défendent</w> <w n="52.2">le</w> <w n="52.3">trésor</w> <w n="52.4">de</w> <w n="52.5">leur</w> <w n="52.6">fidélité</w> ;</l>
					<l n="53" num="4.7"><w n="53.1">Qui</w>, <w n="53.2">sous</w> <w n="53.3">un</w> <w n="53.4">joug</w> <w n="53.5">de</w> <w n="53.6">fer</w>, <w n="53.7">avec</w> <w n="53.8">idolâtrie</w></l>
					<l n="54" num="4.8"><w n="54.1">Entretiennent</w> <w n="54.2">le</w> <w n="54.3">feu</w> <w n="54.4">sacré</w> <w n="54.5">de</w> <w n="54.6">la</w> <w n="54.7">patrie</w> :</l>
					<l n="55" num="4.9"><w n="55.1">Chez</w> <w n="55.2">ces</w> <w n="55.3">âmes</w>, <w n="55.4">l</w>'<w n="55.5">oubli</w> <w n="55.6">n</w>'<w n="55.7">aura</w> <w n="55.8">jamais</w> <w n="55.9">d</w>'<w n="55.10">accès</w> !</l>
				</lg>
				<lg n="5">
					<l n="56" num="5.1"><w n="56.1">Que</w> <w n="56.2">Dieu</w> <w n="56.3">daigne</w> <w n="56.4">venir</w> <w n="56.5">en</w> <w n="56.6">aide</w> <w n="56.7">aux</w> <w n="56.8">bons</w> <w n="56.9">Français</w> !</l>
				</lg>
			</div></body></text></TEI>