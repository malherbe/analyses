<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'ANNÉE MAUDITE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="GRS">
					<name>
						<forename>Charles</forename>
						<surname>GRANDSARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2146 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'ANNÉE MAUDITE 1870-1871</title>
						<author>Charles Grandsard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61285325.r=GRANDSARD%20L%27ANN%C3%89E%20MAUDITE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'ANNÉE MAUDITE 1870-1871</title>
								<author>Charles Grandsard</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE DU PETIT JOURNAL</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRS28">
				<head type="main">LA RESPIRATION DE L’ÂME</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Oui</w> !'<w n="1.2">non</w> <w n="1.3">moins</w> <w n="1.4">que</w> <w n="1.5">le</w> <w n="1.6">corps</w>, <w n="1.7">notre</w> <w n="1.8">âme</w>, <w n="1.9">à</w> <w n="1.10">tous</w>, <w n="1.11">respire</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Il</w> <w n="2.2">est</w>, <w n="2.3">pour</w> <w n="2.4">elle</w>, <w n="2.5">un</w> <w n="2.6">air</w> <w n="2.7">bienfaisant</w> <w n="2.8">ou</w> <w n="2.9">fatal</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">pour</w> <w n="3.3">nos</w> <w n="3.4">poumons</w> ; <w n="3.5">et</w> <w n="3.6">le</w> <w n="3.7">milieu</w> <w n="3.8">natal</w></l>
					<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">sa</w> <w n="4.3">vigueur</w> <w n="4.4">exerce</w> <w n="4.5">un</w> <w n="4.6">souverain</w> <w n="4.7">empire</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Cet</w> <w n="5.2">air</w> <w n="5.3">s</w>'<w n="5.4">appelle</w> <w n="5.5">esprit</w>, <w n="5.6">foi</w>, <w n="5.7">grandeur</w>, <w n="5.8">liberté</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Cet</w> <w n="6.2">ensemble</w> <w n="6.3">de</w> <w n="6.4">biens</w> <w n="6.5">dont</w> <w n="6.6">jouit</w> <w n="6.7">la</w> <w n="6.8">patrie</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">C</w>'<w n="7.2">est</w> <w n="7.3">de</w> <w n="7.4">ces</w> <w n="7.5">éléments</w> <w n="7.6">qu</w>'<w n="7.7">est</w> <w n="7.8">formée</w> <w n="7.9">et</w> <w n="7.10">pétrie</w></l>
					<l n="8" num="2.4"><w n="8.1">L</w>'<w n="8.2">atmosphère</w> <w n="8.3">de</w> <w n="8.4">l</w>'<w n="8.5">âme</w>, <w n="8.6">et</w> <w n="8.7">que</w> <w n="8.8">vient</w> <w n="8.9">sa</w> <w n="8.10">santé</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Quand</w> <w n="9.2">la</w> <w n="9.3">patrie</w> <w n="9.4">est</w> <w n="9.5">grande</w> <w n="9.6">et</w> <w n="9.7">que</w> <w n="9.8">sa</w> <w n="9.9">gloire</w> <w n="9.10">éclate</w>,</l>
					<l n="10" num="3.2"><w n="10.1">L</w>'<w n="10.2">âme</w>, <w n="10.3">alors</w>, <w n="10.4">dans</w> <w n="10.5">son</w> <w n="10.6">sein</w> <w n="10.7">respire</w> <w n="10.8">librement</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Comme</w>, <w n="11.2">sous</w> <w n="11.3">le</w> <w n="11.4">dais</w> <w n="11.5">bleu</w> <w n="11.6">du</w> <w n="11.7">vaste</w> <w n="11.8">firmament</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Le</w> <w n="12.2">poumon</w>, <w n="12.3">dans</w> <w n="12.4">son</w> <w n="12.5">jeu</w>, <w n="12.6">largement</w> <w n="12.7">se</w> <w n="12.8">dilate</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">quand</w> <w n="13.3">du</w> <w n="13.4">sol</w> <w n="13.5">natal</w> <w n="13.6">s</w>'<w n="13.7">amoindrit</w> <w n="13.8">l</w>'<w n="13.9">horizon</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Quand</w> <w n="14.2">la</w> <w n="14.3">patrie</w> <w n="14.4">en</w> <w n="14.5">deuil</w> <w n="14.6">voit</w> <w n="14.7">sa</w> <w n="14.8">gloire</w> <w n="14.9">éclipsée</w>,</l>
					<l n="15" num="4.3"><w n="15.1">L</w>'<w n="15.2">âme</w> <w n="15.3">se</w> <w n="15.4">sent</w> <w n="15.5">faiblir</w>, <w n="15.6">et</w> <w n="15.7">suffoque</w>, <w n="15.8">oppressée</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Comme</w> <w n="16.2">dans</w> <w n="16.3">l</w>'<w n="16.4">air</w> <w n="16.5">épais</w> <w n="16.6">d</w>'<w n="16.7">une</w> <w n="16.8">étroite</w> <w n="16.9">prison</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Quand</w> <w n="17.2">le</w> <w n="17.3">pouls</w> <w n="17.4">social</w> <w n="17.5">s</w>'<w n="17.6">accélère</w> <w n="17.7">avec</w> <w n="17.8">rage</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Dans</w> <w n="18.2">les</w> <w n="18.3">yeux</w> <w n="18.4">des</w> <w n="18.5">partis</w> <w n="18.6">quand</w> <w n="18.7">s</w>'<w n="18.8">allume</w> <w n="18.9">l</w>'<w n="18.10">éclair</w>,</l>
					<l n="19" num="5.3"><w n="19.1">L</w>'<w n="19.2">âme</w> <w n="19.3">sent</w> <w n="19.4">le</w> <w n="19.5">frisson</w> <w n="19.6">électrique</w> <w n="19.7">de</w> <w n="19.8">l</w>'<w n="19.9">air</w> ;</l>
					<l n="20" num="5.4"><w n="20.1">Elle</w> <w n="20.2">hume</w> <w n="20.3">ce</w> <w n="20.4">feu</w> <w n="20.5">précurseur</w> <w n="20.6">de</w> <w n="20.7">l</w>'<w n="20.8">orage</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">quand</w> <w n="21.3">l</w>'<w n="21.4">esprit</w> <w n="21.5">du</w> <w n="21.6">peuple</w> <w n="21.7">a</w> <w n="21.8">perdu</w> <w n="21.9">sa</w> <w n="21.10">vigueur</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Quand</w> <w n="22.2">il</w> <w n="22.3">n</w>'<w n="22.4">est</w> <w n="22.5">plus</w>, <w n="22.6">partout</w>, <w n="22.7">que</w> <w n="22.8">marasme</w>, <w n="22.9">apathie</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Elle</w> <w n="23.2">retombe</w> <w n="23.3">alors</w> <w n="23.4">inerte</w>, <w n="23.5">appesantie</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">dans</w> <w n="24.3">cet</w> <w n="24.4">air</w> <w n="24.5">malsain</w> <w n="24.6">respire</w> <w n="24.7">avec</w> <w n="24.8">langueur</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Ah</w> ! <w n="25.2">quand</w> <w n="25.3">autour</w> <w n="25.4">de</w> <w n="25.5">moi</w> <w n="25.6">l</w>'<w n="25.7">atmosphère</w> <w n="25.8">est</w> <w n="25.9">chargée</w></l>
					<l n="26" num="7.2"><w n="26.1">De</w> <w n="26.2">ces</w> <w n="26.3">acres</w> <w n="26.4">senteurs</w> <w n="26.5">d</w>'<w n="26.6">incendie</w> <w n="26.7">et</w> <w n="26.8">de</w> <w n="26.9">sang</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Mon</w> <w n="27.2">âme</w>, <w n="27.3">qui</w> <w n="27.4">voudrait</w> <w n="27.5">un</w> <w n="27.6">air</w> <w n="27.7">plus</w> <w n="27.8">nourrissant</w>,.</l>
					<l n="28" num="7.4"><w n="28.1">S</w>'<w n="28.2">affaisse</w> <w n="28.3">bien</w> <w n="28.4">souvent</w>, <w n="28.5">triste</w>, <w n="28.6">découragée</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Comment</w> <w n="29.2">donc</w> <w n="29.3">faites</w>-<w n="29.4">vous</w>, <w n="29.5">vous</w> <w n="29.6">que</w> <w n="29.7">je</w> <w n="29.8">vois</w> <w n="29.9">errer</w></l>
					<l n="30" num="8.2"><w n="30.1">L</w>’<w n="30.2">œil</w> <w n="30.3">vif</w>, <w n="30.4">le</w> <w n="30.5">teint</w> <w n="30.6">vermeil</w> <w n="30.7">et</w> <w n="30.8">la</w> <w n="30.9">lèvre</w> <w n="30.10">fleurie</w> ?</l>
					<l n="31" num="8.3"><w n="31.1">Ne</w> <w n="31.2">flairez</w>-<w n="31.3">vous</w> <w n="31.4">donc</w> <w n="31.5">pas</w> <w n="31.6">le</w> <w n="31.7">sang</w> <w n="31.8">de</w> <w n="31.9">la</w> <w n="31.10">patrie</w> ?</l>
					<l n="32" num="8.4"><w n="32.1">Ou</w> <w n="32.2">n</w>'<w n="32.3">auriez</w>-<w n="32.4">vous</w> <w n="32.5">pas</w> <w n="32.6">d</w>'<w n="32.7">âme</w> <w n="32.8">à</w> <w n="32.9">faire</w> <w n="32.10">respirer</w> ?…</l>
				</lg>
			</div></body></text></TEI>