<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES GRAINS DE POUDRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VDS">
					<name>
						<forename>Ali-Joseph-Augustin</forename>
						<surname>VIAL DE SABLIGNY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>604 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VDS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES GRAINS DE POUDRE</title>
						<author>ALI VIAL DE SABLIGNY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5612402j.r=VIAL%20DE%20SABLIGNY%20ALI-JOSEPH-AUGUSTIN%20LES%20GRAINS%20DE%20POUDRE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES GRAINS DE POUDRE</title>
								<author>ALI VIAL DE SABLIGNY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DESCHAMPS, PAPETIER-LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VDS6">
				<head type="main">DEUIL !</head>
				<opener>
					<epigraph>
						<cit>
							<quote>A MES AMIS</quote>
							<bibl>
								<name>Édouard DE LAFONT</name>,
								 du 205e bataillon de la garde nationale<lb></lb>
								 et Alphonse RAVENEL, du 26e de ligne
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">force</w> <w n="1.3">a</w> <w n="1.4">triomphé</w>, <w n="1.5">c</w>'<w n="1.6">est</w> <w n="1.7">le</w> <w n="1.8">droit</w> <w n="1.9">qui</w> <w n="1.10">succombe</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Paris</w>, <w n="2.2">la</w> <w n="2.3">noble</w> <w n="2.4">Ville</w>, <w n="2.5">expire</w>, <w n="2.6">hélas</w> ! <w n="2.7">et</w> <w n="2.8">tombe</w>.</l>
					<l n="3" num="1.3"><w n="3.1">L</w>'<w n="3.2">inexorable</w> <w n="3.3">sort</w> <w n="3.4">nous</w> <w n="3.5">frappe</w> <w n="3.6">jusqu</w>'<w n="3.7">au</w> <w n="3.8">bout</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">nous</w> <w n="4.3">lance</w> <w n="4.4">aujourd</w>'<w n="4.5">hui</w> <w n="4.6">son</w> <w n="4.7">plus</w> <w n="4.8">terrible</w> <w n="4.9">coup</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">ne</w> <w n="5.3">nous</w> <w n="5.4">reste</w> <w n="5.5">plus</w> <w n="5.6">qu</w>'<w n="5.7">à</w> <w n="5.8">jeter</w> <w n="5.9">bas</w> <w n="5.10">les</w> <w n="5.11">armes</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Étouffer</w> <w n="6.2">nos</w> <w n="6.3">sanglots</w> <w n="6.4">et</w> <w n="6.5">dévorer</w> <w n="6.6">nos</w> <w n="6.7">larmes</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Allez</w>, <w n="7.2">cloches</w>, <w n="7.3">sonnez</w> <w n="7.4">le</w> <w n="7.5">lugubre</w> <w n="7.6">tocsin</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Sonnez</w>, <w n="8.2">et</w> <w n="8.3">vous</w> <w n="8.4">aurez</w> <w n="8.5">pour</w> <w n="8.6">écho</w> <w n="8.7">notre</w> <w n="8.8">sein</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Ils</w> <w n="9.2">sont</w> <w n="9.3">partis</w> <w n="9.4">les</w> <w n="9.5">jours</w> <w n="9.6">d</w>'<w n="9.7">espérance</w> <w n="9.8">et</w> <w n="9.9">de</w> <w n="9.10">joie</w>.</l>
					<l n="10" num="1.10"><w n="10.1">L</w>'<w n="10.2">abîme</w> <w n="10.3">s</w>'<w n="10.4">est</w> <w n="10.5">ouvert</w> <w n="10.6">et</w> <w n="10.7">nous</w> <w n="10.8">sommes</w> <w n="10.9">sa</w> <w n="10.10">proie</w>.</l>
					<l n="11" num="1.11"><w n="11.1">Au</w> <w n="11.2">monarque</w> <w n="11.3">allemand</w>, <w n="11.4">dont</w> <w n="11.5">le</w> <w n="11.6">sceptre</w> <w n="11.7">est</w> <w n="11.8">de</w> <w n="11.9">fer</w>,</l>
					<l n="12" num="1.12"><w n="12.1">A</w> <w n="12.2">Guillaume</w> <w n="12.3">notre</w> <w n="12.4">or</w>, <w n="12.5">notre</w> <w n="12.6">sang</w>, <w n="12.7">notre</w> <w n="12.8">chair</w>,</l>
					<l n="13" num="1.13"><w n="13.1">A</w> <w n="13.2">lui</w>, <w n="13.3">toujours</w> <w n="13.4">à</w> <w n="13.5">lui</w>, <w n="13.6">l</w>'<w n="13.7">Alsace</w> <w n="13.8">et</w> <w n="13.9">la</w> <w n="13.10">Lorraine</w> !</l>
					<l n="14" num="1.14"><w n="14.1">Déchaîne</w> <w n="14.2">contre</w> <w n="14.3">nous</w>, <w n="14.4">le</w> <w n="14.5">torrent</w> <w n="14.6">nous</w> <w n="14.7">entraîne</w>, !</l>
					<l n="15" num="1.15"><w n="15.1">Le</w> <w n="15.2">vainqueur</w> <w n="15.3">à</w> <w n="15.4">son</w> <w n="15.5">gré</w> <w n="15.6">nous</w> <w n="15.7">a</w> <w n="15.8">dicté</w> <w n="15.9">ses</w> <w n="15.10">lois</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Les</w> <w n="16.2">airs</w> <w n="16.3">ont</w> <w n="16.4">retenti</w> <w n="16.5">de</w> <w n="16.6">sa</w> <w n="16.7">puissante</w> <w n="16.8">voix</w> :</l>
					<l n="17" num="1.17"><w n="17.1">Nous</w> <w n="17.2">avions</w> <w n="17.3">résisté</w> <w n="17.4">pendant</w> <w n="17.5">dix</w>-<w n="17.6">huit</w> <w n="17.7">semaines</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Il</w> <w n="18.2">fallait</w>, <w n="18.3">après</w> <w n="18.4">tout</w>, <w n="18.5">le</w> <w n="18.6">payer</w> <w n="18.7">de</w> <w n="18.8">ses</w> <w n="18.9">peines</w>.</l>
				</lg>
				<lg n="2">
					<l n="19" num="2.1"><w n="19.1">Que</w> <w n="19.2">de</w> <w n="19.3">travaux</w> <w n="19.4">perdus</w>, <w n="19.5">d</w>'<w n="19.6">efforts</w> <w n="19.7">infructueux</w> !</l>
					<l n="20" num="2.2"><w n="20.1">Voilà</w> <w n="20.2">le</w> <w n="20.3">résultat</w> <w n="20.4">et</w> <w n="20.5">le</w> <w n="20.6">prix</w> <w n="20.7">monstrueux</w></l>
					<l n="21" num="2.3"><w n="21.1">De</w> <w n="21.2">tant</w> <w n="21.3">de</w> <w n="21.4">dévoûment</w> : <w n="21.5">de</w> <w n="21.6">tant</w> <w n="21.7">de</w> <w n="21.8">sacrifice</w> :</l>
					<l n="22" num="2.4"><w n="22.1">Au</w> <w n="22.2">joug</w> <w n="22.3">de</w> <w n="22.4">l</w>'<w n="22.5">étranger</w>, <w n="22.6">il</w> <w n="22.7">faut</w> <w n="22.8">qu</w>'<w n="22.9">on</w> <w n="22.10">obéisse</w> !</l>
					<l n="23" num="2.5"><w n="23.1">Tout</w> <w n="23.2">s</w>'<w n="23.3">efface</w> <w n="23.4">et</w> <w n="23.5">s</w>'<w n="23.6">éteint</w>, <w n="23.7">tout</w> <w n="23.8">croule</w> <w n="23.9">et</w> <w n="23.10">disparaît</w> !</l>
					<l n="24" num="2.6"><w n="24.1">Qui</w> <w n="24.2">l</w>'<w n="24.3">eut</w> <w n="24.4">dit</w>, <w n="24.5">qu</w>'<w n="24.6">un</w> <w n="24.7">tel</w> <w n="24.8">jour</w> <w n="24.9">pour</w> <w n="24.10">nous</w> <w n="24.11">se</w> <w n="24.12">lèverait</w> !</l>
					<l n="25" num="2.7"><w n="25.1">La</w> <w n="25.2">pampre</w> <w n="25.3">qui</w> <w n="25.4">gaîment</w> <w n="25.5">courait</w> <w n="25.6">sous</w> <w n="25.7">la</w> <w n="25.8">tonnelle</w></l>
					<l n="26" num="2.8"><w n="26.1">Fera</w> <w n="26.2">place</w> <w n="26.3">aux</w> <w n="26.4">bouquets</w> <w n="26.5">de</w> <w n="26.6">la</w> <w n="26.7">triste</w> <w n="26.8">immortelle</w> !</l>
					<l n="27" num="2.9"><w n="27.1">Le</w> <w n="27.2">sol</w> <w n="27.3">va</w> <w n="27.4">se</w> <w n="27.5">couvrir</w> <w n="27.6">de</w> <w n="27.7">funèbres</w> <w n="27.8">cyprès</w> !</l>
					<l n="28" num="2.10"><w n="28.1">Ah</w> ! <w n="28.2">qui</w> <w n="28.3">donc</w> <w n="28.4">nous</w> <w n="28.5">rendra</w> <w n="28.6">l</w>'<w n="28.7">ombre</w> <w n="28.8">de</w> <w n="28.9">nos</w> <w n="28.10">forêts</w>,</l>
					<l n="29" num="2.11"><w n="29.1">Le</w> <w n="29.2">gazon</w> <w n="29.3">et</w> <w n="29.4">les</w> <w n="29.5">fleurs</w> <w n="29.6">de</w> <w n="29.7">la</w> <w n="29.8">verte</w> <w n="29.9">prairie</w> ?</l>
					<l n="30" num="2.12"><w n="30.1">Quand</w> <w n="30.2">donc</w> <w n="30.3">entendrons</w>-<w n="30.4">nous</w> <w n="30.5">le</w> <w n="30.6">rude</w> <w n="30.7">essieu</w> <w n="30.8">qui</w> <w n="30.9">crie</w></l>
					<l n="31" num="2.13"><w n="31.1">De</w> <w n="31.2">la</w> <w n="31.3">lourde</w> <w n="31.4">charrue</w> ? <w n="31.5">Aux</w> <w n="31.6">foyers</w> <w n="31.7">ruinés</w>,</l>
					<l n="32" num="2.14"><w n="32.1">Débris</w> <w n="32.2">encor</w> <w n="32.3">fumants</w>, <w n="32.4">squelettes</w> <w n="32.5">décharnés</w>,</l>
					<l n="33" num="2.15"><w n="33.1">Qui</w> <w n="33.2">donc</w> <w n="33.3">rendra</w> <w n="33.4">le</w> <w n="33.5">calme</w>, <w n="33.6">et</w> <w n="33.7">la</w> <w n="33.8">paix</w> <w n="33.9">et</w> <w n="33.10">la</w> <w n="33.11">vie</w> ?</l>
				</lg>
				<lg n="3">
					<l n="34" num="3.1"><w n="34.1">Te</w> <w n="34.2">voilà</w> <w n="34.3">terrassée</w>, <w n="34.4">ô</w> <w n="34.5">France</w>, <w n="34.6">ô</w> <w n="34.7">ma</w> <w n="34.8">patrie</w> !</l>
					<l n="35" num="3.2"><w n="35.1">Ce</w> <w n="35.2">titre</w> <w n="35.3">de</w> <w n="35.4">Français</w>, <w n="35.5">à</w> <w n="35.6">la</w> <w n="35.7">fois</w> <w n="35.8">noble</w> <w n="35.9">et</w> <w n="35.10">doux</w>,</l>
					<l n="36" num="3.3"><w n="36.1">Trop</w> <w n="36.2">souvent</w> <w n="36.3">envié</w> <w n="36.4">ne</w> <w n="36.5">fait</w> <w n="36.6">plus</w> <w n="36.7">de</w> <w n="36.8">jaloux</w> !</l>
					<l n="37" num="3.4"><w n="37.1">Parmi</w> <w n="37.2">les</w> <w n="37.3">nations</w>, <w n="37.4">tu</w> <w n="37.5">marchais</w> <w n="37.6">la</w> <w n="37.7">première</w>,</l>
					<l n="38" num="3.5"><w n="38.1">C</w>'<w n="38.2">est</w> <w n="38.3">à</w> <w n="38.4">recommencer</w> <w n="38.5">une</w> <w n="38.6">œuvre</w> <w n="38.7">tout</w> <w n="38.8">entière</w> !…</l>
					<l n="39" num="3.6"><w n="39.1">N</w>'<w n="39.2">importe</w> ; <w n="39.3">soyons</w> <w n="39.4">grands</w> <w n="39.5">devant</w> <w n="39.6">l</w>'<w n="39.7">adversité</w>,</l>
					<l n="40" num="3.7"><w n="40.1">De</w> <w n="40.2">souffrir</w> <w n="40.3">en</w> <w n="40.4">silence</w> <w n="40.5">ayons</w> <w n="40.6">la</w> <w n="40.7">dignité</w> !</l>
					<l n="41" num="3.8"><w n="41.1">Quand</w> <w n="41.2">on</w> <w n="41.3">courbe</w> <w n="41.4">le</w> <w n="41.5">front</w>, <w n="41.6">c</w>'<w n="41.7">est</w> <w n="41.8">qu</w>'<w n="41.9">on</w> <w n="41.10">se</w> <w n="41.11">sent</w> <w n="41.12">coupable</w>,</l>
					<l n="42" num="3.9"><w n="42.1">Et</w> <w n="42.2">Paris</w> <w n="42.3">ne</w> <w n="42.4">l</w>'<w n="42.5">est</w> <w n="42.6">pas</w> ; <w n="42.7">Paris</w> <w n="42.8">fut</w> <w n="42.9">admirable</w> !</l>
					<l n="43" num="3.10"><w n="43.1">On</w> <w n="43.2">a</w> <w n="43.3">brisé</w> <w n="43.4">son</w> <w n="43.5">glaive</w>, <w n="43.6">on</w> <w n="43.7">a</w> <w n="43.8">lié</w> <w n="43.9">ses</w> <w n="43.10">bras</w>,</l>
					<l n="44" num="3.11"><w n="44.1">Mais</w> <w n="44.2">son</w> <w n="44.3">patriotisme</w>, <w n="44.4">on</w> <w n="44.5">ne</w> <w n="44.6">le</w> <w n="44.7">nîra</w> <w n="44.8">pas</w> !</l>
					<l n="45" num="3.12"><w n="45.1">Laissons</w> <w n="45.2">saigner</w> <w n="45.3">nos</w> <w n="45.4">flancs</w> <w n="45.5">sous</w> <w n="45.6">la</w> <w n="45.7">blessure</w> <w n="45.8">amère</w> !</l>
					<l n="46" num="3.13"><w n="46.1">Imposons</w> <w n="46.2">le</w> <w n="46.3">respect</w> <w n="46.4">par</w> <w n="46.5">notre</w> <w n="46.6">calme</w> <w n="46.7">austère</w> !</l>
					<l n="47" num="3.14"><w n="47.1">Que</w> <w n="47.2">seuls</w> <w n="47.3">des</w> <w n="47.4">crêpes</w> <w n="47.5">noirs</w>, <w n="47.6">aux</w> <w n="47.7">drapeaux</w> <w n="47.8">suspendus</w>,</l>
					<l n="48" num="3.15"><w n="48.1">Soient</w> <w n="48.2">le</w> <w n="48.3">signe</w> <w n="48.4">de</w> <w n="48.5">deuil</w> <w n="48.6">de</w> <w n="48.7">nos</w> <w n="48.8">cours</w> <w n="48.9">éperdus</w> !</l>
					<l n="49" num="3.16"><w n="49.1">Supportons</w> <w n="49.2">nos</w> <w n="49.3">malheurs</w>, <w n="49.4">car</w> <w n="49.5">en</w> <w n="49.6">ce</w> <w n="49.7">jour</w> <w n="49.8">funeste</w>,</l>
					<l n="50" num="3.17"><w n="50.1">Si</w> <w n="50.2">la</w> <w n="50.3">gloire</w> <w n="50.4">nous</w> <w n="50.5">manque</w>, <w n="50.6">au</w> <w n="50.7">moins</w> <w n="50.8">l</w>'<w n="50.9">honneur</w> <w n="50.10">nous</w> <w n="50.11">reste</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">3 mars 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>