<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES GRAINS DE POUDRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VDS">
					<name>
						<forename>Ali-Joseph-Augustin</forename>
						<surname>VIAL DE SABLIGNY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>604 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VDS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES GRAINS DE POUDRE</title>
						<author>ALI VIAL DE SABLIGNY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5612402j.r=VIAL%20DE%20SABLIGNY%20ALI-JOSEPH-AUGUSTIN%20LES%20GRAINS%20DE%20POUDRE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES GRAINS DE POUDRE</title>
								<author>ALI VIAL DE SABLIGNY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DESCHAMPS, PAPETIER-LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VDS12">
				<head type="main">AIMONS-NOUS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Repoussons</w> <w n="1.2">désormais</w> <w n="1.3">toute</w> <w n="1.4">guerre</w> <w n="1.5">civile</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Mes</w> <w n="2.2">frères</w>, <w n="2.3">aimons</w> <w n="2.4">nous</w> <w n="2.5">d</w>'<w n="2.6">un</w> <w n="2.7">saint</w> <w n="2.8">attachement</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">nos</w> <w n="3.3">cœurs</w> <w n="3.4">à</w> <w n="3.5">chacun</w> <w n="3.6">rendons</w> <w n="3.7">l</w>'<w n="3.8">accès</w> <w n="3.9">facile</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">qu</w>'<w n="4.3">un</w> <w n="4.4">commun</w> <w n="4.5">accord</w> <w n="4.6">nous</w> <w n="4.7">lie</w> <w n="4.8">étroitement</w> !</l>
					<l n="5" num="1.5"><w n="5.1">On</w> <w n="5.2">ne</w> <w n="5.3">peut</w> <w n="5.4">rien</w> <w n="5.5">fonder</w> <w n="5.6">par</w> <w n="5.7">le</w> <w n="5.8">sang</w> <w n="5.9">et</w> <w n="5.10">les</w> <w n="5.11">larmes</w> :</l>
					<l n="6" num="1.6"><w n="6.1">Unissons</w> <w n="6.2">nos</w> <w n="6.3">efforts</w> <w n="6.4">pour</w> <w n="6.5">marcher</w> <w n="6.6">en</w> <w n="6.7">avant</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">travail</w>, <w n="7.3">le</w> <w n="7.4">progrès</w>, <w n="7.5">sont</w> <w n="7.6">les</w> <w n="7.7">meilleures</w> <w n="7.8">armes</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Pour</w> <w n="8.2">atteindre</w> <w n="8.3">le</w> <w n="8.4">but</w> <w n="8.5">que</w> <w n="8.6">l</w>'<w n="8.7">on</w> <w n="8.8">va</w> <w n="8.9">poursuivant</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Confondons</w> <w n="9.2">tous</w> <w n="9.3">les</w> <w n="9.4">rangs</w> <w n="9.5">dans</w> <w n="9.6">un</w> <w n="9.7">même</w> <w n="9.8">principe</w>,</l>
					<l n="10" num="2.2"><w n="10.1">La</w> <w n="10.2">noblesse</w> <w n="10.3">de</w> <w n="10.4">l</w>'<w n="10.5">âme</w> <w n="10.6">avec</w> <w n="10.7">celle</w> <w n="10.8">du</w> <w n="10.9">nom</w> ;</l>
					<l n="11" num="2.3"><w n="11.1">Devant</w> <w n="11.2">l</w>'<w n="11.3">égalité</w> <w n="11.4">que</w> <w n="11.5">tout</w> <w n="11.6">fiel</w> <w n="11.7">se</w> <w n="11.8">dissipe</w>,</l>
					<l n="12" num="2.4"><w n="12.1">Du</w> <w n="12.2">lien</w> <w n="12.3">fraternel</w> <w n="12.4">soyons</w> <w n="12.5">tous</w> <w n="12.6">un</w> <w n="12.7">chaînon</w>.</l>
					<l n="13" num="2.5"><w n="13.1">Que</w> <w n="13.2">le</w> <w n="13.3">même</w> <w n="13.4">flambeau</w> <w n="13.5">nous</w> <w n="13.6">guide</w> <w n="13.7">et</w> <w n="13.8">nous</w> <w n="13.9">éclaire</w>,</l>
					<l n="14" num="2.6"><w n="14.1">Laissons</w> <w n="14.2">parler</w> <w n="14.3">en</w> <w n="14.4">nous</w> <w n="14.5">la</w> <w n="14.6">voix</w> <w n="14.7">de</w> <w n="14.8">la</w> <w n="14.9">raison</w>,</l>
					<l n="15" num="2.7"><w n="15.1">Suivons</w>, <w n="15.2">suivons</w> <w n="15.3">le</w> <w n="15.4">cours</w> <w n="15.5">du</w> <w n="15.6">bon</w>-<w n="15.7">sens</w> <w n="15.8">populaire</w>.</l>
					<l n="16" num="2.8"><w n="16.1">De</w> <w n="16.2">l</w>'<w n="16.3">arbre</w> <w n="16.4">du</w> <w n="16.5">bonheur</w> <w n="16.6">hâtons</w> <w n="16.7">la</w> <w n="16.8">floraison</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Nous</w> <w n="17.2">ne</w> <w n="17.3">devons</w> <w n="17.4">former</w> <w n="17.5">qu</w>'<w n="17.6">une</w> <w n="17.7">même</w> <w n="17.8">famille</w> ;</l>
					<l n="18" num="3.2"><w n="18.1">Au</w> <w n="18.2">foyer</w> <w n="18.3">de</w> <w n="18.4">l</w>'<w n="18.5">amour</w> <w n="18.6">réchauffons</w>-<w n="18.7">nous</w> <w n="18.8">un</w> <w n="18.9">peu</w> :</l>
					<l n="19" num="3.3"><w n="19.1">Vivre</w> <w n="19.2">sous</w> <w n="19.3">un</w> <w n="19.4">ciel</w> <w n="19.5">pur</w> <w n="19.6">où</w> <w n="19.7">resplendit</w> <w n="19.8">et</w> <w n="19.9">brille</w></l>
					<l n="20" num="3.4"><w n="20.1">Le</w> <w n="20.2">soleil</w> <w n="20.3">de</w> <w n="20.4">la</w> <w n="20.5">paix</w>, <w n="20.6">doit</w> <w n="20.7">être</w> <w n="20.8">notre</w> <w n="20.9">vœu</w>.</l>
					<l n="21" num="3.5"><w n="21.1">Préparons</w> <w n="21.2">la</w> <w n="21.3">moisson</w>, <w n="21.4">fils</w> <w n="21.5">de</w> <w n="21.6">la</w> <w n="21.7">République</w>,</l>
					<l n="22" num="3.6"><w n="22.1">Faisons</w> <w n="22.2">régner</w> <w n="22.3">partout</w> <w n="22.4">la</w> <w n="22.5">justice</w> <w n="22.6">et</w> <w n="22.7">l</w>'<w n="22.8">amour</w>,</l>
					<l n="23" num="3.7"><w n="23.1">Et</w> <w n="23.2">ce</w> <w n="23.3">que</w> <w n="23.4">nous</w> <w n="23.5">fit</w> <w n="23.6">perdre</w> <w n="23.7">un</w> <w n="23.8">souverain</w> <w n="23.9">inique</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Nous</w> <w n="24.2">l</w>'<w n="24.3">aurons</w> <w n="24.4">retrouvé</w>, <w n="24.5">mes</w> <w n="24.6">frères</w>, <w n="24.7">en</w> <w n="24.8">un</w> <w n="24.9">jour</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Paris, 30 juin 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>