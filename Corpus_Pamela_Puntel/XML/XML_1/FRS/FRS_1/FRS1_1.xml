<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">AMERTUMES ET PAIN NOIR</title>
				<title type="sub">SIÈGE DE PARIS 1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="FRS">
					<name>
						<forename>Émile</forename>
						<surname>FRANÇOIS</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>487 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
						<author>ÉMILE FRANÇOIS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61012844.r=FRAN%C3%87OIS%20E.%2C%20AMERTUMES%20ET%20PAIN%20NOIR.?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
								<author>ÉMILE FRANÇOIS</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE A. LACROIX, VERBOECKHOVEN ET Cie</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-24" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRS1">
				<head type="main">SÉPARATION-ISOLEMENT</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">Que</w> <w n="1.2">j</w>'<w n="1.3">aurai</w> <w n="1.4">donc</w> <w n="1.5">de</w> <w n="1.6">choses</w> <w n="1.7">à</w> <w n="1.8">te</w> <w n="1.9">dire</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Ma</w> <w n="2.2">bien</w> <w n="2.3">aimée</w>, <w n="2.4">au</w> <w n="2.5">jour</w>, <w n="2.6">oh</w> ! <w n="2.7">quel</w> <w n="2.8">qu</w>'<w n="2.9">il</w> <w n="2.10">soit</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">Pour</w> <w n="3.2">la</w> <w n="3.3">patrie</w>, <w n="3.4">ou</w> <w n="3.5">moins</w> <w n="3.6">mauvais</w> <w n="3.7">ou</w> <w n="3.8">pire</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Au</w> <w n="4.2">jour</w> <w n="4.3">où</w> <w n="4.4">là</w>, <w n="4.5">dans</w> <w n="4.6">ce</w> <w n="4.7">petit</w> <w n="4.8">endroit</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">Notre</w> <w n="5.2">foyer</w>, <w n="5.3">qui</w>, <w n="5.4">depuis</w> <w n="5.5">deux</w> <w n="5.6">mois</w> <w n="5.7">vide</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">S</w>'<w n="6.2">étonne</w>, <w n="6.3">lui</w> <w n="6.4">qu</w>'<w n="6.5">on</w> <w n="6.6">voyait</w> <w n="6.7">toujours</w> <w n="6.8">clair</w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1">O</w> <w n="7.2">mon</w> <w n="7.3">étoile</w> ! <w n="7.4">ô</w> <w n="7.5">mon</w> <w n="7.6">ciel</w> ! <w n="7.7">mon</w> <w n="7.8">Égide</w> !</l>
					<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">Je</w> <w n="8.2">te</w> <w n="8.3">verrai</w> <w n="8.4">reparaître</w> <w n="8.5">et</w> <w n="8.6">parler</w> !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="4"></space><w n="9.1">Je</w> <w n="9.2">te</w> <w n="9.3">dirai</w> : <w n="9.4">J</w>'<w n="9.5">étais</w> <w n="9.6">là</w> <w n="9.7">seul</w> <w n="9.8">et</w> <w n="9.9">triste</w>,</l>
					<l n="10" num="2.2"><space unit="char" quantity="4"></space><w n="10.1">Et</w> <w n="10.2">j</w>'<w n="10.3">écoutais</w>… <w n="10.4">la</w> <w n="10.5">pendule</w> <w n="10.6">qui</w> <w n="10.7">bat</w>,</l>
					<l n="11" num="2.3"><space unit="char" quantity="4"></space><w n="11.1">Et</w> <w n="11.2">bat</w>, <w n="11.3">et</w> <w n="11.4">bat</w>, <w n="11.5">sèche</w> <w n="11.6">et</w> <w n="11.7">morne</w> <w n="11.8">choriste</w>…</l>
					<l n="12" num="2.4"><space unit="char" quantity="4"></space><w n="12.1">L</w>'<w n="12.2">huile</w> <w n="12.3">qui</w> <w n="12.4">brûle</w>… <w n="12.5">un</w> <w n="12.6">grouillement</w> <w n="12.7">de</w> <w n="12.8">rat</w>…,</l>
					<l n="13" num="2.5"><space unit="char" quantity="4"></space><w n="13.1">Mon</w> <w n="13.2">souffle</w>… <w n="13.3">un</w> <w n="13.4">meuble</w> <w n="13.5">au</w> <w n="13.6">craquement</w> <w n="13.7">sonore</w>…</l>
					<l n="14" num="2.6"><space unit="char" quantity="4"></space><w n="14.1">La</w> <w n="14.2">dent</w> <w n="14.3">d</w>'<w n="14.4">un</w> <w n="14.5">ver</w> <w n="14.6">qui</w> <w n="14.7">ronge</w> <w n="14.8">un</w> <w n="14.9">vieux</w> <w n="14.10">bois</w>, <w n="14.11">là</w>…</l>
					<l n="15" num="2.7"><space unit="char" quantity="4"></space><w n="15.1">J</w>'<w n="15.2">écoutais</w> <w n="15.3">tout</w>… <w n="15.4">et</w>… <w n="15.5">j</w>'<w n="15.6">écoutais</w> <w n="15.7">encore</w>…</l>
					<l n="16" num="2.8"><space unit="char" quantity="4"></space><w n="16.1">Car</w> <w n="16.2">il</w> <w n="16.3">manquait</w> <w n="16.4">un</w> <w n="16.5">bruit</w> <w n="16.6">à</w> <w n="16.7">tout</w> <w n="16.8">cela</w>…</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space unit="char" quantity="4"></space><w n="17.1">Je</w> <w n="17.2">te</w> <w n="17.3">dirai</w> : <w n="17.4">J</w>'<w n="17.5">étais</w> <w n="17.6">là</w>… <w n="17.7">seul</w>, <w n="17.8">si</w> <w n="17.9">triste</w> !…</l>
					<l n="18" num="3.2"><space unit="char" quantity="4"></space><w n="18.1">Je</w> <w n="18.2">regardais</w>… <w n="18.3">Portes</w>, <w n="18.4">meubles</w> <w n="18.5">et</w> <w n="18.6">murs</w>,</l>
					<l n="19" num="3.3"><space unit="char" quantity="4"></space><w n="19.1">Plus</w> <w n="19.2">que</w> <w n="19.3">le</w> <w n="19.4">mien</w>, <w n="19.5">jamais</w> <w n="19.6">œil</w> <w n="19.7">d</w>'<w n="19.8">archiviste</w></l>
					<l n="20" num="3.4"><space unit="char" quantity="4"></space><w n="20.1">N</w>'<w n="20.2">avait</w> <w n="20.3">pesé</w> <w n="20.4">sur</w> <w n="20.5">eux</w>… <w n="20.6">Muets</w>, <w n="20.7">obscurs</w>,</l>
					<l n="21" num="3.5"><space unit="char" quantity="4"></space><w n="21.1">Ils</w> <w n="21.2">avaient</w> <w n="21.3">l</w>'<w n="21.4">air</w> <w n="21.5">bêtes</w> <w n="21.6">à</w> <w n="21.7">ne</w> <w n="21.8">pas</w> <w n="21.9">croire</w>…</l>
					<l n="22" num="3.6"><space unit="char" quantity="4"></space><w n="22.1">Et</w> <w n="22.2">toujours</w> <w n="22.3">plus</w> <w n="22.4">je</w> <w n="22.5">cherchais</w> <w n="22.6">çà</w> <w n="22.7">et</w> <w n="22.8">là</w>…</l>
					<l n="23" num="3.7"><space unit="char" quantity="4"></space><w n="23.1">Et</w> <w n="23.2">mon</w> <w n="23.3">regard</w> <w n="23.4">plongeait</w> <w n="23.5">dans</w> <w n="23.6">l</w>'<w n="23.7">ombre</w> <w n="23.8">noire</w>…,</l>
					<l n="24" num="3.8"><space unit="char" quantity="4"></space><w n="24.1">Car</w> <w n="24.2">il</w> <w n="24.3">manquait</w> <w n="24.4">son</w> <w n="24.5">âme</w> <w n="24.6">à</w> <w n="24.7">tout</w> <w n="24.8">cela</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space unit="char" quantity="4"></space><w n="25.1">Et</w> <w n="25.2">puis</w> <w n="25.3">mon</w> <w n="25.4">cœur</w>, <w n="25.5">du</w> <w n="25.6">fond</w> <w n="25.7">de</w> <w n="25.8">son</w> <w n="25.9">alcôve</w>,</l>
					<l n="26" num="4.2"><space unit="char" quantity="4"></space><w n="26.1">Effaré</w>, <w n="26.2">blême</w>, <w n="26.3">arrivait</w> <w n="26.4">à</w> <w n="26.5">son</w> <w n="26.6">tour</w></l>
					<l n="27" num="4.3"><space unit="char" quantity="4"></space><w n="27.1">A</w> <w n="27.2">sa</w> <w n="27.3">fenêtre</w>, <w n="27.4">ainsi</w> <w n="27.5">qu</w>'<w n="27.6">un</w> <w n="27.7">oiseau</w> <w n="27.8">fauve</w>,</l>
					<l n="28" num="4.4"><space unit="char" quantity="4"></space><w n="28.1">Qui</w> <w n="28.2">scrute</w> <w n="28.3">l</w>'<w n="28.4">ombre</w>, <w n="28.5">et</w>, <w n="28.6">dans</w> <w n="28.7">son</w> <w n="28.8">noir</w> <w n="28.9">séjour</w></l>
					<l n="29" num="4.5"><space unit="char" quantity="4"></space><w n="29.1">N</w>'<w n="29.2">entendant</w> <w n="29.3">rien</w>, <w n="29.4">jette</w> <w n="29.5">à</w> <w n="29.6">l</w>'<w n="29.7">espace</w> <w n="29.8">louche</w></l>
					<l n="30" num="4.6"><space unit="char" quantity="4"></space><w n="30.1">Un</w> <w n="30.2">cri</w> <w n="30.3">plaintif</w> <w n="30.4">dont</w> <w n="30.5">geint</w> <w n="30.6">l</w>'<w n="30.7">écho</w> <w n="30.8">du</w> <w n="30.9">lieu</w>.</l>
					<l n="31" num="4.7"><space unit="char" quantity="4"></space><w n="31.1">Il</w> <w n="31.2">venait</w> <w n="31.3">mettre</w> <w n="31.4">un</w> <w n="31.5">soupir</w> <w n="31.6">à</w> <w n="31.7">ma</w> <w n="31.8">bouche</w> :</l>
					<l n="32" num="4.8"><space unit="char" quantity="4"></space>« <w n="32.1">O</w> <w n="32.2">ma</w> <w n="32.3">Marie</w>, <w n="32.4">où</w> <w n="32.5">donc</w> <w n="32.6">es</w>-<w n="32.7">tu</w> ? <w n="32.8">Mon</w> <w n="32.9">Dieu</w> ! »</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><space unit="char" quantity="4"></space><w n="33.1">Quand</w> <w n="33.2">reviendront</w> <w n="33.3">nos</w> <w n="33.4">douces</w> <w n="33.5">causeries</w></l>
					<l n="34" num="5.2"><space unit="char" quantity="12"></space><w n="34.1">Du</w> <w n="34.2">soir</w> ? <w n="34.3">Babil</w> <w n="34.4">d</w>'<w n="34.5">enfant</w></l>
					<l n="35" num="5.3"><space unit="char" quantity="16"></space><w n="35.1">Que</w> <w n="35.2">j</w>'<w n="35.3">aime</w> <w n="35.4">tant</w>,</l>
					<l n="36" num="5.4"><space unit="char" quantity="8"></space><w n="36.1">Et</w> <w n="36.2">que</w> <w n="36.3">mes</w> <w n="36.4">oreilles</w> <w n="36.5">ravies</w></l>
					<l n="37" num="5.5"><space unit="char" quantity="8"></space><w n="37.1">Voudraient</w> <w n="37.2">ouïr</w> <w n="37.3">et</w> <w n="37.4">jours</w> <w n="37.5">et</w> <w n="37.6">nuits</w>,…</l>
					<l n="38" num="5.6"><space unit="char" quantity="8"></space><w n="38.1">Mais</w> <w n="38.2">surtout</w> <w n="38.3">du</w> <w n="38.4">soir</w> <w n="38.5">à</w> <w n="38.6">l</w>'<w n="38.7">aurore</w> !…</l>
					<l n="39" num="5.7"><space unit="char" quantity="8"></space><w n="39.1">Je</w> <w n="39.2">m</w>'<w n="39.3">endors</w> <w n="39.4">en</w> <w n="39.5">disant</w> : « <w n="39.6">Et</w> <w n="39.7">puis</w>… »</l>
					<l n="40" num="5.8"><space unit="char" quantity="8"></space><w n="40.1">Je</w> <w n="40.2">m</w>'<w n="40.3">éveille</w> <w n="40.4">en</w> <w n="40.5">disant</w> : « <w n="40.6">Encore</w>. »</l>
					<l n="41" num="5.9"><space unit="char" quantity="8"></space><w n="41.1">Cause</w>, <w n="41.2">cause</w>, <w n="41.3">oiseau</w> <w n="41.4">du</w> <w n="41.5">taillis</w>…</l>
					<l n="42" num="5.10"><space unit="char" quantity="12"></space><w n="42.1">De</w> <w n="42.2">ton</w> <w n="42.3">plus</w> <w n="42.4">beau</w> <w n="42.5">ramage</w>,</l>
					<l n="43" num="5.11"><w n="43.1">Des</w> <w n="43.2">sons</w> <w n="43.3">les</w> <w n="43.4">plus</w> <w n="43.5">discrets</w>, <w n="43.6">de</w> <w n="43.7">ton</w> <w n="43.8">bec</w> <w n="43.9">rejaillis</w>,</l>
					<l n="44" num="5.12"><space unit="char" quantity="10"></space><w n="44.1">Remplis</w> <w n="44.2">l</w>'<w n="44.3">ombre</w> <w n="44.4">du</w> <w n="44.5">bocage</w>.</l>
					<l n="45" num="5.13"><space unit="char" quantity="4"></space><w n="45.1">Ah</w> ! <w n="45.2">je</w> <w n="45.3">t</w>'<w n="45.4">écoute</w> <w n="45.5">avec</w> <w n="45.6">tant</w> <w n="45.7">de</w> <w n="45.8">plaisir</w> !</l>
					<l n="46" num="5.14"><space unit="char" quantity="4"></space><w n="46.1">Ne</w> <w n="46.2">finis</w> <w n="46.3">pas</w>. — <w n="46.4">Et</w> <w n="46.5">toi</w>, <w n="46.6">ma</w> <w n="46.7">voix</w> <w n="46.8">ailée</w>,</l>
					<l n="47" num="5.15"><space unit="char" quantity="4"></space><w n="47.1">Comme</w> <w n="47.2">l</w>'<w n="47.3">oiseau</w>, <w n="47.4">cause</w> <w n="47.5">pour</w> <w n="47.6">me</w> <w n="47.7">ravir</w>.</l>
					<l n="48" num="5.16"><space unit="char" quantity="4"></space><w n="48.1">Ta</w> <w n="48.2">voix</w> <w n="48.3">aussi</w> <w n="48.4">charmerait</w> <w n="48.5">la</w> <w n="48.6">vallée</w> !…</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">22 novembre, minuit.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>