<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">AMERTUMES ET PAIN NOIR</title>
				<title type="sub">SIÈGE DE PARIS 1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="FRS">
					<name>
						<forename>Émile</forename>
						<surname>FRANÇOIS</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>487 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
						<author>ÉMILE FRANÇOIS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61012844.r=FRAN%C3%87OIS%20E.%2C%20AMERTUMES%20ET%20PAIN%20NOIR.?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
								<author>ÉMILE FRANÇOIS</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE A. LACROIX, VERBOECKHOVEN ET Cie</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-24" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRS7">
				<head type="main">CAUSES</head>
				<div type="section" n="1">
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Et</w> <w n="1.2">qui</w> <w n="1.3">fit</w> <w n="1.4">donc</w> <w n="1.5">naître</w> <w n="1.6">la</w> <w n="1.7">guerre</w> ?…</l>
						<l n="2" num="1.2"><w n="2.1">Deux</w> <w n="2.2">chefs</w> : <w n="2.3">un</w> <w n="2.4">Empereur</w>, <w n="2.5">un</w> <w n="2.6">Roi</w>.</l>
						<l n="3" num="1.3"><w n="3.1">L</w>'<w n="3.2">un</w>, <w n="3.3">dans</w> <w n="3.4">la</w> <w n="3.5">guerre</w> <w n="3.6">espérant</w> <w n="3.7">taire</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">ses</w> <w n="4.3">budgets</w> <w n="4.4">le</w> <w n="4.5">désarroi</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">L</w>'<w n="5.2">autre</w>, <w n="5.3">y</w> <w n="5.4">menant</w>, <w n="5.5">en</w> <w n="5.6">homme</w> <w n="5.7">habile</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Pour</w> <w n="6.2">les</w> <w n="6.3">miner</w> <w n="6.4">sans</w> <w n="6.5">les</w> <w n="6.6">vexer</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Trois</w> <w n="7.2">peuples</w> <w n="7.3">voisins</w>, <w n="7.4">qu</w>'<w n="7.5">à</w> <w n="7.6">sa</w> <w n="7.7">file</w></l>
						<l n="8" num="1.8"><w n="8.1">Il</w> <w n="8.2">voulait</w> <w n="8.3">ensuite</w> <w n="8.4">annexer</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Deux</w> <w n="9.2">chefs</w>, <w n="9.3">deux</w> <w n="9.4">têtes</w> <w n="9.5">couronnées</w> !…</l>
						<l n="10" num="2.2"><w n="10.1">Qui</w>, <w n="10.2">pour</w> <w n="10.3">leurs</w> <w n="10.4">stupides</w> <w n="10.5">passions</w>,</l>
						<l n="11" num="2.3"><w n="11.1">Traînent</w> <w n="11.2">aux</w> <w n="11.3">guerres</w> <w n="11.4">forcenées</w></l>
						<l n="12" num="2.4"><w n="12.1">Leurs</w> <w n="12.2">malheureuses</w> <w n="12.3">nations</w> !</l>
						<l n="13" num="2.5"><w n="13.1">O</w> <w n="13.2">rois</w>, <w n="13.3">qu</w>'<w n="13.4">est</w>-<w n="13.5">ce</w> <w n="13.6">donc</w> <w n="13.7">que</w> <w n="13.8">vous</w> <w n="13.9">êtes</w> ?</l>
						<l n="14" num="2.6"><w n="14.1">Quel</w> <w n="14.2">coin</w> <w n="14.3">du</w> <w n="14.4">ciel</w> <w n="14.5">vous</w> <w n="14.6">fit</w> <w n="14.7">ces</w> <w n="14.8">droits</w> ?…</l>
						<l n="15" num="2.7"><w n="15.1">Peuples</w> ! <w n="15.2">que</w> <w n="15.3">vous</w> <w n="15.4">êtes</w> <w n="15.5">donc</w> <w n="15.6">bêtes</w></l>
						<l n="16" num="2.8"><w n="16.1">D</w>'<w n="16.2">avoir</w> <w n="16.3">encor</w> <w n="16.4">besoin</w> <w n="16.5">des</w> <w n="16.6">rois</w> ?…</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">L</w>'<w n="17.2">annexeur</w>… <w n="17.3">ô</w> <w n="17.4">le</w> <w n="17.5">fin</w> <w n="17.6">compère</w> !</l>
						<l n="18" num="3.2"><w n="18.1">Savait</w> <w n="18.2">l</w>'<w n="18.3">embarras</w> <w n="18.4">du</w> <w n="18.5">premier</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Et</w> <w n="19.2">de</w> <w n="19.3">son</w> <w n="19.4">peuple</w> <w n="19.5">l</w>'<w n="19.6">humeur</w> <w n="19.7">fière</w>,</l>
						<l n="20" num="3.4"><w n="20.1">Toujours</w> <w n="20.2">prête</w> <w n="20.3">à</w> <w n="20.4">guerroyer</w>.</l>
						<l n="21" num="3.5"><w n="21.1">Il</w> <w n="21.2">se</w> <w n="21.3">prépara</w> ; <w n="21.4">puis</w> <w n="21.5">fit</w> <w n="21.6">bruire</w></l>
						<l n="22" num="3.6"><w n="22.1">Sourdement</w> <w n="22.2">l</w>'<w n="22.3">insulte</w> <w n="22.4">dans</w> <w n="22.5">l</w>'<w n="22.6">air</w>.</l>
						<l n="23" num="3.7"><w n="23.1">L</w>'<w n="23.2">insulté</w> <w n="23.3">bondit</w> ; <w n="23.4">et</w> <w n="23.5">l</w>'<w n="23.6">Empire</w>,</l>
						<l n="24" num="3.8"><w n="24.1">Avec</w> <w n="24.2">bonheur</w>, <w n="24.3">saisit</w> <w n="24.4">le</w> <w n="24.5">fer</w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Ainsi</w>, <w n="25.2">c</w>'<w n="25.3">est</w> <w n="25.4">nous</w> <w n="25.5">qui</w> <w n="25.6">l</w>'<w n="25.7">avons</w> <w n="25.8">déclarée</w>,</l>
						<l n="26" num="4.2"><w n="26.1">La</w> <w n="26.2">guerre</w> ?… <w n="26.3">Hypocrite</w> ! <w n="26.4">imposteur</w> !</l>
						<l n="27" num="4.3"><w n="27.1">Vieillard</w> <w n="27.2">fauve</w> ! <w n="27.3">tête</w> <w n="27.4">carrée</w> !</l>
						<l n="28" num="4.4"><w n="28.1">Tu</w> <w n="28.2">l</w>'<w n="28.3">oses</w> <w n="28.4">dire</w> !… <w n="28.5">Et</w> <w n="28.6">ta</w> <w n="28.7">candeur</w></l>
						<l n="29" num="4.5"><w n="29.1">Des</w> <w n="29.2">Cabinets</w> <w n="29.3">est</w> <w n="29.4">acceptée</w> !…</l>
						<l n="30" num="4.6"><w n="30.1">Et</w> <w n="30.2">si</w> <w n="30.3">le</w> <w n="30.4">sang</w> <w n="30.5">coule</w> <w n="30.6">aux</w> <w n="30.7">égouts</w>,</l>
						<l n="31" num="4.7"><w n="31.1">Si</w> <w n="31.2">l</w>'<w n="31.3">Europe</w> <w n="31.4">est</w> <w n="31.5">épouvantée</w>,</l>
						<l n="32" num="4.8"><w n="32.1">Dis</w>-<w n="32.2">le</w>, <w n="32.3">ô</w> <w n="32.4">roi</w>, <w n="32.5">la</w> <w n="32.6">faute</w> <w n="32.7">est</w> <w n="32.8">à</w> <w n="32.9">nous</w> ?…</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">9 décembre.</date>
						</dateline>
					</closer>
				</div>
				<div type="section" n="2">
					<lg n="1">
						<l n="33" num="1.1"><w n="33.1">Europe</w> ! <w n="33.2">Europe</w> <w n="33.3">plate</w> <w n="33.4">et</w> <w n="33.5">lâche</w>,</l>
						<l n="34" num="1.2"><w n="34.1">Qui</w> <w n="34.2">vois</w> <w n="34.3">d</w>'<w n="34.4">un</w> <w n="34.5">œil</w> <w n="34.6">indifférent</w></l>
						<l n="35" num="1.3"><w n="35.1">L</w>'<w n="35.2">injuste</w> <w n="35.3">ainsi</w> <w n="35.4">suivre</w> <w n="35.5">sa</w> <w n="35.6">tâche</w>,</l>
						<l n="36" num="1.4"><w n="36.1">Quelle</w> <w n="36.2">pitié</w> <w n="36.3">pour</w> <w n="36.4">toi</w> <w n="36.5">me</w> <w n="36.6">prend</w> !…</l>
						<l n="37" num="1.5"><w n="37.1">C</w>'<w n="37.2">est</w> <w n="37.3">un</w> <w n="37.4">crime</w>, <w n="37.5">cela</w>, <w n="37.6">ma</w> <w n="37.7">chère</w>,</l>
						<l n="38" num="1.6"><w n="38.1">Et</w> <w n="38.2">tu</w> <w n="38.3">le</w> <w n="38.4">paîras</w>, <w n="38.5">certe</w>, <w n="38.6">un</w> <w n="38.7">jour</w>.</l>
						<l n="39" num="1.7"><w n="39.1">Tu</w> <w n="39.2">te</w> <w n="39.3">gardes</w> ?… <w n="39.4">C</w>'<w n="39.5">est</w> <w n="39.6">son</w> <w n="39.7">affaire</w>,</l>
						<l n="40" num="1.8"><w n="40.1">Au</w> <w n="40.2">Double</w>-<w n="40.3">Bec</w>. <w n="40.4">Puis</w>, <w n="40.5">à</w> <w n="40.6">ton</w> <w n="40.7">tour</w> !…</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">12 décembre.</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>