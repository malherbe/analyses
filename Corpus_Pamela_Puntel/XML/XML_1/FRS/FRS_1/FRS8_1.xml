<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">AMERTUMES ET PAIN NOIR</title>
				<title type="sub">SIÈGE DE PARIS 1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="FRS">
					<name>
						<forename>Émile</forename>
						<surname>FRANÇOIS</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>487 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
						<author>ÉMILE FRANÇOIS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61012844.r=FRAN%C3%87OIS%20E.%2C%20AMERTUMES%20ET%20PAIN%20NOIR.?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
								<author>ÉMILE FRANÇOIS</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE A. LACROIX, VERBOECKHOVEN ET Cie</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-24" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRS8">
				<head type="main">DÉFAITE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Sarrebruck</w>… <w n="1.2">Dérision</w> !… <w n="1.3">O</w> <w n="1.4">France</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">battais</w> <w n="2.3">des</w> <w n="2.4">mains</w>… <w n="2.5">Et</w>, <w n="2.6">tout</w> <w n="2.7">près</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Satan</w>, <w n="3.2">riant</w> <w n="3.3">de</w> <w n="3.4">ta</w> <w n="3.5">confiance</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">ta</w> <w n="4.3">tête</w> <w n="4.4">tenait</w> <w n="4.5">tout</w> <w n="4.6">prêts</w></l>
					<l n="5" num="1.5"><w n="5.1">Les</w> <w n="5.2">jours</w> <w n="5.3">néfastes</w> <w n="5.4">qu</w>'<w n="5.5">on</w> <w n="5.6">appelle</w></l>
					<l n="6" num="1.6"><w n="6.1">Wissembourg</w>, <w n="6.2">Reischoffen</w>, <w n="6.3">Forbach</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Puis</w> <w n="7.2">vint</w> <w n="7.3">Sedan</w>, <w n="7.4">Metz</w> <w n="7.5">la</w> <w n="7.6">pucelle</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Paris</w> <w n="8.2">qu</w>'<w n="8.3">on</w> <w n="8.4">rend</w> ! <w n="8.5">Tout</w> <w n="8.6">mis</w> <w n="8.7">à</w> <w n="8.8">sac</w> !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Paris</w> <w n="9.2">pourtant</w>, <w n="9.3">oh</w> ! <w n="9.4">qu</w>'<w n="9.5">on</w> <w n="9.6">le</w> <w n="9.7">sache</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Paris</w> <w n="10.2">fut</w> <w n="10.3">beau</w>, <w n="10.4">beau</w> <w n="10.5">comme</w> <w n="10.6">un</w> <w n="10.7">lion</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Qui</w>, <w n="11.2">faisant</w> <w n="11.3">face</w> <w n="11.4">au</w> <w n="11.5">danger</w>, <w n="11.6">cache</w></l>
					<l n="12" num="2.4"><w n="12.1">Sous</w> <w n="12.2">lui</w> <w n="12.3">ses</w> <w n="12.4">lionceaux</w>, <w n="12.5">et</w> <w n="12.6">tient</w> <w n="12.7">bon</w>…</l>
					<l n="13" num="2.5"><w n="13.1">Il</w> <w n="13.2">n</w>'<w n="13.3">envoya</w> <w n="13.4">personne</w>. <w n="13.5">Entrèrent</w></l>
					<l n="14" num="2.6"><w n="14.1">Ceux</w> <w n="14.2">qui</w> <w n="14.3">voulurent</w>. <w n="14.4">A</w> <w n="14.5">l</w>'<w n="14.6">égal</w>,</l>
					<l n="15" num="2.7"><w n="15.1">Pauvres</w> <w n="15.2">et</w> <w n="15.3">riches</w> <w n="15.4">partagèrent</w></l>
					<l n="16" num="2.8"><w n="16.1">Aux</w> <w n="16.2">palais</w> <w n="16.3">pain</w> <w n="16.4">noir</w> <w n="16.5">et</w> <w n="16.6">cheval</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">De</w> <w n="17.2">plainte</w>, <w n="17.3">point</w>. <w n="17.4">Mais</w> <w n="17.5">la</w> <w n="17.6">défense</w>,</l>
					<l n="18" num="3.2"><w n="18.1">La</w> <w n="18.2">noble</w> <w n="18.3">lutte</w>, <w n="18.4">les</w> <w n="18.5">grands</w> <w n="18.6">coups</w>,</l>
					<l n="19" num="3.3"><w n="19.1">Et</w> <w n="19.2">l</w>'<w n="19.3">enthousiasme</w>, <w n="19.4">et</w> <w n="19.5">l</w>'<w n="19.6">espérance</w> :</l>
					<l n="20" num="3.4"><w n="20.1">C</w>'<w n="20.2">était</w> <w n="20.3">l</w>'<w n="20.4">inspiration</w> <w n="20.5">de</w> <w n="20.6">tous</w>,</l>
					<l n="21" num="3.5"><w n="21.1">Comme</w> <w n="21.2">un</w> <w n="21.3">ruisseau</w> <w n="21.4">courant</w> <w n="21.5">aux</w> <w n="21.6">rues</w> !</l>
					<l n="22" num="3.6"><w n="22.1">Partout</w> <w n="22.2">l</w>'<w n="22.3">école</w> <w n="22.4">du</w> <w n="22.5">soldat</w> !…</l>
					<l n="23" num="3.7"><w n="23.1">Et</w>, <w n="23.2">comme</w> <w n="23.3">aux</w> <w n="23.4">murs</w> <w n="23.5">étaient</w> <w n="23.6">courues</w></l>
					<l n="24" num="3.8"><w n="24.1">Les</w> <w n="24.2">gardes</w>, <w n="24.3">qu</w>'<w n="24.4">il</w> <w n="24.5">plût</w> <w n="24.6">ou</w> <w n="24.7">gelât</w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">Riche</w>, <w n="25.2">pauvre</w>, <w n="25.3">savant</w>, <w n="25.4">manœuvre</w>,</l>
					<l n="26" num="4.2"><w n="26.1">Maître</w>, <w n="26.2">ouvrier</w>, <w n="26.3">bourgeois</w>, <w n="26.4">marchand</w>,</l>
					<l n="27" num="4.3"><w n="27.1">Le</w> <w n="27.2">pêle</w>-<w n="27.3">mêle</w> <w n="27.4">était</w> <w n="27.5">à</w> <w n="27.6">l</w>’<w n="27.7">œuvre</w>,</l>
					<l n="28" num="4.4"><w n="28.1">Dispos</w>, <w n="28.2">actif</w>, <w n="28.3">superbe</w>, <w n="28.4">grand</w> !…</l>
					<l n="29" num="4.5"><w n="29.1">Les</w> <w n="29.2">usines</w> <w n="29.3">partout</w> <w n="29.4">chauffaient</w> ;</l>
					<l n="30" num="4.6"><w n="30.1">Partout</w> <w n="30.2">on</w> <w n="30.3">faisait</w> <w n="30.4">des</w> <w n="30.5">canons</w>,</l>
					<l n="31" num="4.7"><w n="31.1">Et</w> <w n="31.2">les</w> <w n="31.3">canons</w> <w n="31.4">par</w> <w n="31.5">cents</w> <w n="31.6">sortaient</w>,</l>
					<l n="32" num="4.8"><w n="32.1">Brillants</w> <w n="32.2">d</w>'<w n="32.3">éclat</w> <w n="32.4">comme</w> <w n="32.5">de</w> <w n="32.6">noms</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">O</w> <w n="33.2">cœur</w> <w n="33.3">de</w> <w n="33.4">notre</w> <w n="33.5">brave</w> <w n="33.6">France</w> !</l>
					<l n="34" num="5.2"><w n="34.1">Paris</w> !… <w n="34.2">cher</w> <w n="34.3">peuple</w> <w n="34.4">valeureux</w> ?</l>
					<l part="I" n="35" num="5.3"><w n="35.1">Oh</w> ! <w n="35.2">si</w> <w n="35.3">tes</w> <w n="35.4">chefs</w> </l>
					<l part="F" n="35" num="5.3"><w n="35.5">Mais</w>, <w n="35.6">le</w> <w n="35.7">cœur</w> <w n="35.8">rance</w></l>
					<l n="36" num="5.4"><w n="36.1">Et</w> <w n="36.2">le</w> <w n="36.3">front</w> <w n="36.4">plat</w>, <w n="36.5">les</w> <w n="36.6">malheureux</w> !</l>
					<l n="37" num="5.5"><w n="37.1">Sans</w> <w n="37.2">souffle</w> <w n="37.3">ils</w> <w n="37.4">te</w> <w n="37.5">carbonisèrent</w>,</l>
					<l n="38" num="5.6"><w n="38.1">Au</w> <w n="38.2">lieu</w> <w n="38.3">d</w>'<w n="38.4">aviver</w> <w n="38.5">ton</w> <w n="38.6">ardeur</w>,</l>
					<l n="39" num="5.7"><w n="39.1">Et</w> <w n="39.2">lâchement</w> <w n="39.3">en</w> <w n="39.4">arrivèrent</w></l>
					<l n="40" num="5.8"><w n="40.1">A</w> <w n="40.2">te</w> <w n="40.3">livrer</w>, <w n="40.4">sauvant</w> <w n="40.5">l</w>'<w n="40.6">honneur</w> !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><w n="41.1">Oui</w>, <w n="41.2">dans</w> <w n="41.3">cette</w> <w n="41.4">ville</w>, <w n="41.5">décidée</w></l>
					<l n="42" num="6.2"><w n="42.1">A</w> <w n="42.2">tenir</w> <w n="42.3">tête</w> <w n="42.4">au</w> <w n="42.5">krupp</w> <w n="42.6">prussien</w>,</l>
					<l n="43" num="6.3"><w n="43.1">De</w> <w n="43.2">se</w> <w n="43.3">plaindre</w>, <w n="43.4">même</w> <w n="43.5">l</w>'<w n="43.6">idée</w></l>
					<l n="44" num="6.4"><w n="44.1">Ne</w> <w n="44.2">parut</w> !… <w n="44.3">Et</w> <w n="44.4">pourtant</w> !… <w n="44.5">combien</w></l>
					<l n="45" num="6.5"><w n="45.1">La</w> <w n="45.2">privation</w>, <w n="45.3">maigre</w> <w n="45.4">et</w> <w n="45.5">cruelle</w>,</l>
					<l n="46" num="6.6"><w n="46.1">Variant</w> <w n="46.2">sa</w> <w n="46.3">forme</w> <w n="46.4">et</w> <w n="46.5">ses</w> <w n="46.6">rigueurs</w>,</l>
					<l n="47" num="6.7"><w n="47.1">A</w> <w n="47.2">coups</w> <w n="47.3">redoublés</w> <w n="47.4">frappa</w>-<w n="47.5">t</w>-<w n="47.6">elle</w></l>
					<l n="48" num="6.8"><w n="48.1">De</w> <w n="48.2">manière</w> <w n="48.3">à</w> <w n="48.4">briser</w> <w n="48.5">les</w> <w n="48.6">cœurs</w> !…</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1"><w n="49.1">On</w> <w n="49.2">n</w>'<w n="49.3">avait</w> <w n="49.4">qu</w>'<w n="49.5">un</w> <w n="49.6">pain</w> <w n="49.7">noir</w>, <w n="49.8">mollasse</w>,</l>
					<l n="50" num="7.2"><w n="50.1">Glutineux</w>, <w n="50.2">de</w> <w n="50.3">paille</w> <w n="50.4">et</w> <w n="50.5">de</w> <w n="50.6">son</w>,</l>
					<l n="51" num="7.3"><w n="51.1">Plein</w> <w n="51.2">de</w> <w n="51.3">débris</w> <w n="51.4">où</w> <w n="51.5">la</w> <w n="51.6">mélasse</w></l>
					<l n="52" num="7.4"><w n="52.1">Paraissait</w> <w n="52.2">faire</w> <w n="52.3">la</w> <w n="52.4">liaison</w> ;</l>
					<l n="53" num="7.5"><w n="53.1">Plus</w> <w n="53.2">rien</w> <w n="53.3">en</w> <w n="53.4">viande</w> <w n="53.5">ordinaire</w>,</l>
					<l n="54" num="7.6"><w n="54.1">Ou</w> <w n="54.2">porc</w>, <w n="54.3">ou</w> <w n="54.4">veau</w>, <w n="54.5">mouton</w>, <w n="54.6">ou</w> <w n="54.7">bœuf</w> ;</l>
					<l n="55" num="7.7"><w n="55.1">Mais</w> <w n="55.2">du</w> <w n="55.3">cheval</w>, <w n="55.4">la</w> <w n="55.5">chair</w> <w n="55.6">guerrière</w>,</l>
					<l n="56" num="7.8"><w n="56.1">Par</w> <w n="56.2">tête</w> <w n="56.3">un</w> <w n="56.4">gramme</w> <w n="56.5">avec</w> <w n="56.6">vingt</w>-<w n="56.7">neuf</w> !</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1"><w n="57.1">A</w> <w n="57.2">prix</w> <w n="57.3">d</w>'<w n="57.4">or</w> <w n="57.5">on</w> <w n="57.6">pouvait</w> <w n="57.7">encore</w></l>
					<l n="58" num="8.2"><w n="58.1">Trouver</w> <w n="58.2">un</w> <w n="58.3">peu</w> <w n="58.4">de</w> <w n="58.5">lait</w> <w n="58.6">sans</w> <w n="58.7">goût</w>,</l>
					<l n="59" num="8.3"><w n="59.1">Des</w> <w n="59.2">œufs</w>, <w n="59.3">quelque</w> <w n="59.4">maigre</w> <w n="59.5">pécore</w></l>
					<l n="60" num="8.4"><w n="60.1">De</w> <w n="60.2">chien</w>, <w n="60.3">de</w> <w n="60.4">chat</w> <w n="60.5">ou</w> <w n="60.6">rat</w> <w n="60.7">d</w>'<w n="60.8">égout</w> !</l>
					<l n="61" num="8.5"><w n="61.1">La</w> <w n="61.2">verdure</w> !… <w n="61.3">quelle</w> <w n="61.4">était</w> <w n="61.5">chère</w> !</l>
					<l n="62" num="8.6"><w n="62.1">Deux</w> <w n="62.2">francs</w> <w n="62.3">étaient</w> <w n="62.4">sur</w> <w n="62.5">quelques</w> <w n="62.6">bancs</w></l>
					<l n="63" num="8.7"><w n="63.1">Le</w> <w n="63.2">prix</w> <w n="63.3">d</w>'<w n="63.4">une</w> <w n="63.5">pomme</w> <w n="63.6">de</w> <w n="63.7">terre</w> !</l>
					<l n="64" num="8.8"><w n="64.1">Un</w> <w n="64.2">petit</w> <w n="64.3">chou</w> <w n="64.4">valait</w> <w n="64.5">vingt</w> <w n="64.6">francs</w> !</l>
				</lg>
				<lg n="9">
					<l n="65" num="9.1"><w n="65.1">Dans</w> <w n="65.2">cet</w> <w n="65.3">hiver</w>, <w n="65.4">un</w> <w n="65.5">des</w> <w n="65.6">plus</w> <w n="65.7">rudes</w>,</l>
					<l n="66" num="9.2"><w n="66.1">On</w> <w n="66.2">n</w>'<w n="66.3">eut</w> <w n="66.4">bientôt</w> <w n="66.5">charbon</w> <w n="66.6">ni</w> <w n="66.7">bois</w>.</l>
					<l n="67" num="9.3"><w n="67.1">Voleurs</w> <w n="67.2">ou</w> <w n="67.3">non</w>, <w n="67.4">par</w> <w n="67.5">habitudes</w>,</l>
					<l n="68" num="9.4"><w n="68.1">Sans</w> <w n="68.2">s</w>'<w n="68.3">inquiéter</w> <w n="68.4">de</w> <w n="68.5">droits</w> <w n="68.6">ou</w> <w n="68.7">lois</w>,</l>
					<l n="69" num="9.5"><w n="69.1">Aux</w> <w n="69.2">chantiers</w>, <w n="69.3">aux</w> <w n="69.4">clos</w>, <w n="69.5">aux</w> <w n="69.6">clôtures</w>,</l>
					<l n="70" num="9.6"><w n="70.1">Partout</w>, <w n="70.2">hommes</w>, <w n="70.3">femmes</w>, <w n="70.4">enfants</w>,</l>
					<l n="71" num="9.7"><w n="71.1">Pour</w> <w n="71.2">se</w> <w n="71.3">chauffer</w>, <w n="71.4">en</w> <w n="71.5">fournitures</w></l>
					<l n="72" num="9.8"><w n="72.1">Allaient</w>, <w n="72.2">tous</w>… <w n="72.3">mornes</w> <w n="72.4">et</w> <w n="72.5">méchants</w>.</l>
				</lg>
				<lg n="10">
					<l n="73" num="10.1"><w n="73.1">Et</w> <w n="73.2">partout</w>, <w n="73.3">aux</w> <w n="73.4">places</w> <w n="73.5">boisées</w>,</l>
					<l n="74" num="10.2"><w n="74.1">La</w> <w n="74.2">hache</w> <w n="74.3">tranchait</w> <w n="74.4">sous</w> <w n="74.5">nos</w> <w n="74.6">yeux</w> !</l>
					<l n="75" num="10.3"><w n="75.1">Doyens</w> <w n="75.2">de</w> <w n="75.3">nos</w> <w n="75.4">Champs</w>-<w n="75.5">Élysées</w>,</l>
					<l n="76" num="10.4"><w n="76.1">O</w> <w n="76.2">vastes</w> <w n="76.3">ormes</w>, <w n="76.4">vous</w>, <w n="76.5">si</w> <w n="76.6">vieux</w>,</l>
					<l n="77" num="10.5"><w n="77.1">Si</w> <w n="77.2">respectables</w> <w n="77.3">par</w> <w n="77.4">votre</w> <w n="77.5">âge</w></l>
					<l n="78" num="10.6"><w n="78.1">Et</w> <w n="78.2">par</w> <w n="78.3">les</w> <w n="78.4">actes</w> <w n="78.5">glorieux</w></l>
					<l n="79" num="10.7"><w n="79.1">Qu</w>'<w n="79.2">a</w> <w n="79.3">vus</w> <w n="79.4">s</w>'<w n="79.5">accomplir</w> <w n="79.6">votre</w> <w n="79.7">ombrage</w>,</l>
					<l n="80" num="10.8"><w n="80.1">On</w> <w n="80.2">vous</w> <w n="80.3">coupa</w>, <w n="80.4">vous</w>, <w n="80.5">nos</w> <w n="80.6">aïeux</w> !</l>
				</lg>
				<lg n="11">
					<l n="81" num="11.1"><w n="81.1">Croire</w>, <w n="81.2">en</w> <w n="81.3">effet</w>, <w n="81.4">qu</w>'<w n="81.5">un</w> <w n="81.6">jour</w>… <w n="81.7">sur</w> <w n="81.8">elle</w></l>
					<l n="82" num="11.2"><w n="82.1">Cette</w> <w n="82.2">allée</w>… <w n="82.3">en</w> <w n="82.4">travers</w>… <w n="82.5">sentit</w></l>
					<l n="83" num="11.3"><w n="83.1">Tomber</w>, <w n="83.2">comme</w> <w n="83.3">une</w> <w n="83.4">citadelle</w>,</l>
					<l n="84" num="11.4"><w n="84.1">Sous</w> <w n="84.2">les</w> <w n="84.3">coups</w> <w n="84.4">de</w> <w n="84.5">quelque</w> <w n="84.6">bandit</w></l>
					<l n="85" num="11.5">(<w n="85.1">Car</w> <w n="85.2">pour</w> <w n="85.3">cette</w> <w n="85.4">œuvre</w> <w n="85.5">il</w> <w n="85.6">fallait</w> <w n="85.7">l</w>'<w n="85.8">homme</w>),</l>
					<l n="86" num="11.6"><w n="86.1">Un</w> <w n="86.2">de</w> <w n="86.3">ces</w> <w n="86.4">arbres</w>, <w n="86.5">et</w> <w n="86.6">puis</w> <w n="86.7">deux</w>…</l>
					<l n="87" num="11.7"><w n="87.1">Puis</w> <w n="87.2">trois</w>, <w n="87.3">et</w>… <w n="87.4">qu</w>'<w n="87.5">on</w> <w n="87.6">n</w>'<w n="87.7">osait</w> <w n="87.8">en</w> <w n="87.9">somme</w></l>
					<l n="88" num="11.8"><w n="88.1">Rien</w> <w n="88.2">dire</w> <w n="88.3">à</w> <w n="88.4">tous</w> <w n="88.5">ces</w> <w n="88.6">malheureux</w> !…</l>
				</lg>
				<lg n="12">
					<l n="89" num="12.1"><w n="89.1">Puis</w> <w n="89.2">une</w> <w n="89.3">foule</w> <w n="89.4">mate</w>, <w n="89.5">affamée</w>,</l>
					<l n="90" num="12.2"><w n="90.1">Femmes</w>, <w n="90.2">vieillards</w>, <w n="90.3">filles</w>, <w n="90.4">garçons</w>,</l>
					<l n="91" num="12.3"><w n="91.1">D</w>'<w n="91.2">incroyables</w> <w n="91.3">outils</w> <w n="91.4">armée</w>,</l>
					<l n="92" num="12.4"><w n="92.1">Se</w> <w n="92.2">précipitait</w> <w n="92.3">sur</w> <w n="92.4">ces</w> <w n="92.5">troncs</w>,</l>
					<l n="93" num="12.5"><w n="93.1">Les</w> <w n="93.2">dépeçait</w>, <w n="93.3">et</w>, <w n="93.4">par</w> <w n="93.5">parcelle</w></l>
					<l n="94" num="12.6"><w n="94.1">Toute</w> <w n="94.2">verte</w>, <w n="94.3">les</w> <w n="94.4">emportait</w>,</l>
					<l n="95" num="12.7"><w n="95.1">Pour</w> <w n="95.2">faire</w> <w n="95.3">un</w> <w n="95.4">feu</w> <w n="95.5">dont</w> <w n="95.6">l</w>'<w n="95.7">étincelle</w></l>
					<l n="96" num="12.8"><w n="96.1">En</w> <w n="96.2">brillant</w> <w n="96.3">sans</w> <w n="96.4">doute</w> <w n="96.5">glaçait</w> !</l>
				</lg>
				<lg n="13">
					<l n="97" num="13.1"><w n="97.1">Cela</w> <w n="97.2">se</w> <w n="97.3">faisait</w> <w n="97.4">sans</w> <w n="97.5">rien</w> <w n="97.6">dire</w></l>
					<l n="98" num="13.2"><w n="98.1">On</w> <w n="98.2">était</w> <w n="98.3">sombre</w>, <w n="98.4">silencieux</w>,</l>
					<l n="99" num="13.3"><w n="99.1">Pas</w> <w n="99.2">un</w> <w n="99.3">seul</w> <w n="99.4">chant</w>, <w n="99.5">pas</w> <w n="99.6">un</w> <w n="99.7">sourire</w> ;</l>
					<l n="100" num="13.4"><w n="100.1">C</w>'<w n="100.2">était</w> <w n="100.3">l</w>'<w n="100.4">office</w> <w n="100.5">religieux</w></l>
					<l n="101" num="13.5"><w n="101.1">Des</w> <w n="101.2">morts</w> !… <w n="101.3">Les</w> <w n="101.4">débris</w> <w n="101.5">sur</w> <w n="101.6">la</w> <w n="101.7">borne</w></l>
					<l n="102" num="13.6"><w n="102.1">Accusaient</w>, <w n="102.2">comme</w> <w n="102.3">un</w> <w n="102.4">crime</w>, <w n="102.5">à</w> <w n="102.6">l</w>’<w n="102.7">œil</w>.</l>
					<l n="103" num="13.7"><w n="103.1">On</w> <w n="103.2">sentait</w> <w n="103.3">frémir</w> <w n="103.4">dans</w> <w n="103.5">l</w>'<w n="103.6">air</w> <w n="103.7">morne</w></l>
					<l n="104" num="13.8"><w n="104.1">L</w>'<w n="104.2">ombre</w> <w n="104.3">de</w> <w n="104.4">la</w> <w n="104.5">patrie</w> <w n="104.6">en</w> <w n="104.7">deuil</w> !</l>
				</lg>
				<lg n="14">
					<l n="105" num="14.1"><w n="105.1">Faute</w> <w n="105.2">de</w> <w n="105.3">pain</w> <w n="105.4">mourait</w> <w n="105.5">la</w> <w n="105.6">mère</w> !…</l>
					<l n="106" num="14.2"><w n="106.1">Faute</w> <w n="106.2">de</w> <w n="106.3">lait</w> <w n="106.4">mourait</w> <w n="106.5">l</w>'<w n="106.6">enfant</w> !…</l>
					<l n="107" num="14.3"><w n="107.1">Faute</w> <w n="107.2">de</w> <w n="107.3">bois</w>, <w n="107.4">le</w> <w n="107.5">bon</w> <w n="107.6">vieux</w> <w n="107.7">père</w></l>
					<l n="108" num="14.4"><w n="108.1">Mourait</w>, <w n="108.2">le</w> <w n="108.3">froid</w> <w n="108.4">au</w> <w n="108.5">cœur</w> <w n="108.6">gagnant</w> !</l>
					<l n="109" num="14.5"><w n="109.1">Horreur</w> ! <w n="109.2">Et</w> <w n="109.3">rien</w>, <w n="109.4">or</w> <w n="109.5">ni</w> <w n="109.6">prière</w>,</l>
					<l n="110" num="14.6"><w n="110.1">Rien</w> <w n="110.2">qui</w> <w n="110.3">fît</w> ! <w n="110.4">Et</w>, <w n="110.5">les</w> <w n="110.6">yeux</w> <w n="110.7">taris</w>,</l>
					<l n="111" num="14.7"><w n="111.1">Ils</w> <w n="111.2">attendaient</w> <w n="111.3">l</w>'<w n="111.4">heure</w> <w n="111.5">dernière</w> !…</l>
					<l n="112" num="14.8"><w n="112.1">O</w> <w n="112.2">mères</w> ! <w n="112.3">ô</w> <w n="112.4">fils</w> !… <w n="112.5">ô</w> <w n="112.6">maris</w> !…</l>
				</lg>
				<lg n="15">
					<l n="113" num="15.1"><w n="113.1">Fallait</w>-<w n="113.2">il</w> <w n="113.3">vouloir</w> <w n="113.4">se</w> <w n="113.5">défendre</w> !</l>
					<l n="114" num="15.2"><w n="114.1">Cinq</w> <w n="114.2">fois</w> <w n="114.3">on</w> <w n="114.4">sortit</w>, <w n="114.5">pour</w> <w n="114.6">calmer</w></l>
					<l n="115" num="15.3"><w n="115.1">Ce</w> <w n="115.2">peuple</w>, <w n="115.3">impatient</w> <w n="115.4">d</w>'<w n="115.5">attendre</w>.</l>
					<l n="116" num="15.4"><w n="116.1">Vous</w> <w n="116.2">êtes</w> <w n="116.3">là</w> <w n="116.4">pour</w> <w n="116.5">proclamer</w></l>
					<l rhyme="none" n="117" num="15.5"><w n="117.1">S</w>'<w n="117.2">il</w> <w n="117.3">se</w> <w n="117.4">battit</w> <w n="117.5">avec</w> <w n="117.6">courage</w>,</l>
					<l n="118" num="15.6"><w n="118.1">Lieux</w> <w n="118.2">de</w> <w n="118.3">Rueil</w> ; <w n="118.4">de</w> <w n="118.5">Châtillon</w>,</l>
					<l n="119" num="15.7"><w n="119.1">De</w> <w n="119.2">l</w>'<w n="119.3">Hay</w>, <w n="119.4">du</w> <w n="119.5">Bourget</w>, <w n="119.6">de</w> <w n="119.7">Joinville</w> !</l>
					<l n="120" num="15.8"><w n="120.1">Pourquoi</w>, <w n="120.2">vainqueurs</w>, <w n="120.3">après</w> <w n="120.4">l</w>'<w n="120.5">action</w>,</l>
					<l n="121" num="15.9"><w n="121.1">Toujours</w>, <w n="121.2">ô</w> <w n="121.3">chefs</w>, <w n="121.4">rentrer</w> <w n="121.5">en</w> <w n="121.6">ville</w> ?</l>
				</lg>
				<lg n="16">
					<l n="122" num="16.1"><w n="122.1">Et</w> <w n="122.2">quand</w> <w n="122.3">fut</w> <w n="122.4">prête</w> <w n="122.5">la</w> <w n="122.6">défense</w>,</l>
					<l n="123" num="16.2"><w n="123.1">Qu</w>'<w n="123.2">on</w> <w n="123.3">eût</w> <w n="123.4">de</w> <w n="123.5">toutes</w> <w n="123.6">les</w> <w n="123.7">douleurs</w></l>
					<l n="124" num="16.3"><w n="124.1">Expertisé</w> <w n="124.2">l</w>'<w n="124.3">âpre</w> <w n="124.4">jouissance</w>,</l>
					<l n="125" num="16.4"><w n="125.1">Et</w> <w n="125.2">qu</w>'<w n="125.3">au</w> <w n="125.4">sérieux</w> <w n="125.5">les</w> <w n="125.6">trois</w> <w n="125.7">couleurs</w></l>
					<l n="126" num="16.5"><w n="126.1">Et</w> <w n="126.2">la</w> <w n="126.3">citoyenne</w> <w n="126.4">milice</w></l>
					<l n="127" num="16.6"><w n="127.1">Eurent</w> <w n="127.2">droit</w> <w n="127.3">de</w> <w n="127.4">se</w> <w n="127.5">prendre</w>, <w n="127.6">là</w>,</l>
					<l n="128" num="16.7"><w n="128.1">Un</w> <w n="128.2">bruit</w> <w n="128.3">singulier</w> <w n="128.4">d</w>'<w n="128.5">armistice</w>,</l>
					<l n="129" num="16.8"><w n="129.1">De</w> <w n="129.2">paix</w>, <w n="129.3">de</w> <w n="129.4">rançon</w>, <w n="129.5">circula</w> !…</l>
				</lg>
				<lg n="17">
					<l n="130" num="17.1"><w n="130.1">Un</w> <w n="130.2">armistice</w> !… <w n="130.3">Et</w> <w n="130.4">par</w> <w n="130.5">derrière</w></l>
					<l n="131" num="17.2">(<w n="131.1">Lorraine</w>, <w n="131.2">Alsace</w>, <w n="131.3">pardonnez</w>,</l>
					<l n="132" num="17.3"><w n="132.1">Sœurs</w>, <w n="132.2">vous</w> <w n="132.3">qu</w>'<w n="132.4">on</w> <w n="132.5">aime</w> !) <w n="132.6">la</w> <w n="132.7">frontière</w></l>
					<l n="133" num="17.4"><w n="133.1">Et</w> <w n="133.2">trois</w> <w n="133.3">villes</w> <w n="133.4">que</w> <w n="133.5">vous</w> <w n="133.6">donnez</w>,</l>
					<l n="134" num="17.5"><w n="134.1">L</w>'<w n="134.2">occupation</w>, <w n="134.3">la</w> <w n="134.4">ruine</w> <w n="134.5">immense</w> !…</l>
					<l n="135" num="17.6"><w n="135.1">Mais</w> <w n="135.2">les</w> <w n="135.3">vivres</w> <w n="135.4">étaient</w> <w n="135.5">passés</w> ;</l>
					<l n="136" num="17.7"><w n="136.1">Pousser</w> <w n="136.2">plus</w> <w n="136.3">loin</w> <w n="136.4">la</w> <w n="136.5">résistance</w>,</l>
					<l n="137" num="17.8"><w n="137.1">Impossible</w> !… <w n="137.2">c</w>'<w n="137.3">était</w> <w n="137.4">assez</w> !</l>
				</lg>
				<lg n="18">
					<l n="138" num="18.1"><w n="138.1">Ah</w> ! <w n="138.2">Chefs</w> ! <w n="138.3">c</w>'<w n="138.4">était</w> <w n="138.5">assez</w>, <w n="138.6">sans</w> <w n="138.7">doute</w>,</l>
					<l n="139" num="18.2"><w n="139.1">Pour</w> <w n="139.2">vous</w> <w n="139.3">à</w> <w n="139.4">qui</w> <w n="139.5">vaincre</w> <w n="139.6">eût</w> <w n="139.7">gêné</w> !</l>
					<l n="140" num="18.3"><w n="140.1">Vous</w> <w n="140.2">étapiez</w> <w n="140.3">une</w> <w n="140.4">autre</w> <w n="140.5">route</w> ;</l>
					<l n="141" num="18.4"><w n="141.1">Car</w> <w n="141.2">la</w> <w n="141.3">victoire</w> <w n="141.4">eût</w> <w n="141.5">sanctionné</w></l>
					<l n="142" num="18.5"><w n="142.1">Bien</w> <w n="142.2">sûrement</w> <w n="142.3">la</w> <w n="142.4">République</w>.</l>
					<l n="143" num="18.6"><w n="143.1">Et</w> <w n="143.2">la</w> <w n="143.3">République</w>, <w n="143.4">Uhlans</w>,</l>
					<l n="144" num="18.7"><w n="144.1">Ne</w> <w n="144.2">promet</w> <w n="144.3">à</w> <w n="144.4">qui</w> <w n="144.5">la</w> <w n="144.6">pratique</w></l>
					<l n="145" num="18.8"><w n="145.1">Ni</w> <w n="145.2">faveurs</w>, <w n="145.3">ni</w> <w n="145.4">gros</w> <w n="145.5">traitements</w> !…</l>
				</lg>
				<lg n="19">
					<l n="146" num="19.1"><w n="146.1">Mais</w>, <w n="146.2">insulté</w> <w n="146.3">dans</w> <w n="146.4">son</w> <w n="146.5">courage</w>,</l>
					<l n="147" num="19.2"><w n="147.1">Trompé</w>, <w n="147.2">joué</w>, <w n="147.3">désespéré</w>,</l>
					<l n="148" num="19.3"><w n="148.1">Ce</w> <w n="148.2">peuple</w>, <w n="148.3">un</w> <w n="148.4">moment</w> <w n="148.5">dans</w> <w n="148.6">sa</w> <w n="148.7">rage</w>,</l>
					<l n="149" num="19.4"><w n="149.1">Se</w> <w n="149.2">tut</w>… <w n="149.3">Et</w> <w n="149.4">puis</w>, <w n="149.5">exaspéré</w>,</l>
					<l n="150" num="19.5"><w n="150.1">S</w>'<w n="150.2">insurgea</w>. — <w n="150.3">Passez</w>, <w n="150.4">passez</w> <w n="150.5">vite</w>,</l>
					<l n="151" num="19.6"><w n="151.1">Oiseaux</w> <w n="151.2">noirs</w>, <w n="151.3">sans</w> <w n="151.4">vous</w> <w n="151.5">retourner</w> !</l>
					<l n="152" num="19.7"><w n="152.1">Pour</w> <w n="152.2">le</w> <w n="152.3">dompter</w>, <w n="152.4">Messieurs</w>, <w n="152.5">ensuite</w></l>
					<l n="153" num="19.8"><w n="153.1">Il</w> <w n="153.2">vous</w> <w n="153.3">fallut</w> <w n="153.4">l</w>'<w n="153.5">assassiner</w> !</l>
				</lg>
				<lg n="20">
					<l n="154" num="20.1"><w n="154.1">Paris</w> ! <w n="154.2">quand</w> <w n="154.3">de</w> <w n="154.4">fleurs</w> <w n="154.5">tu</w> <w n="154.6">tapisses</w></l>
					<l n="155" num="20.2"><w n="155.1">Aux</w> <w n="155.2">jours</w> <w n="155.3">de</w> <w n="155.4">mai</w> <w n="155.5">tes</w> <w n="155.6">moindres</w> <w n="155.7">coins</w>,</l>
					<l n="156" num="20.3"><w n="156.1">En</w> <w n="156.2">ces</w> <w n="156.3">jours</w>-<w n="156.4">là</w> <w n="156.5">tes</w> <w n="156.6">frontispices</w>,</l>
					<l n="157" num="20.4"><w n="157.1">Tes</w> <w n="157.2">monuments</w>, <w n="157.3">sur</w> <w n="157.4">tous</w> <w n="157.5">les</w> <w n="157.6">points</w>,</l>
					<l n="158" num="20.5"><w n="158.1">Virent</w> <w n="158.2">sur</w> <w n="158.3">eux</w>, <w n="158.4">lugubre</w> <w n="158.5">année</w> !</l>
					<l n="159" num="20.6"><w n="159.1">Tomber</w> <w n="159.2">de</w> <w n="159.3">singulières</w> <w n="159.4">fleurs</w> !</l>
					<l n="160" num="20.7"><w n="160.1">De</w> <w n="160.2">chair</w> <w n="160.3">la</w> <w n="160.4">pierre</w> <w n="160.5">fut</w> <w n="160.6">ornée</w>,</l>
					<l n="161" num="20.8"><w n="161.1">Et</w> <w n="161.2">les</w> <w n="161.3">murs</w> <w n="161.4">versèrent</w> <w n="161.5">des</w> <w n="161.6">pleurs</w> !</l>
				</lg>
				<lg n="21">
					<l n="162" num="21.1"><w n="162.1">Il</w> <w n="162.2">faut</w> <w n="162.3">donc</w> <w n="162.4">croire</w> <w n="162.5">que</w> <w n="162.6">sur</w> <w n="162.7">terre</w></l>
					<l n="163" num="21.2"><w n="163.1">Il</w> <w n="163.2">est</w> <w n="163.3">toujours</w> <w n="163.4">un</w> <w n="163.5">dieu</w> <w n="163.6">jaloux</w></l>
					<l n="164" num="21.3"><w n="164.1">Qui</w> <w n="164.2">ne</w> <w n="164.3">se</w> <w n="164.4">plait</w> <w n="164.5">que</w> <w n="164.6">dans</w> <w n="164.7">la</w> <w n="164.8">guerre</w>,</l>
					<l n="165" num="21.4"><w n="165.1">Et</w> <w n="165.2">veut</w>, <w n="165.3">pour</w> <w n="165.4">charmer</w> <w n="165.5">son</w> <w n="165.6">courroux</w>,</l>
					<l n="166" num="21.5"><w n="166.1">Qu</w>'<w n="166.2">on</w> <w n="166.3">hache</w> <w n="166.4">et</w> <w n="166.5">qu</w>'<w n="166.6">on</w> <w n="166.7">tue</w> <w n="166.8">à</w> <w n="166.9">ses</w> <w n="166.10">fêtes</w> !</l>
					<l n="167" num="21.6"><w n="167.1">Et</w> <w n="167.2">les</w> <w n="167.3">sacrifices</w> <w n="167.4">humains</w></l>
					<l n="168" num="21.7"><w n="168.1">Se</w> <w n="168.2">font</w> <w n="168.3">encor</w> ; — <w n="168.4">car</w> <w n="168.5">vous</w> <w n="168.6">en</w> <w n="168.7">êtes</w>,</l>
					<l n="169" num="21.8"><w n="169.1">Affreux</w> <w n="169.2">jours</w>, <w n="169.3">sanglants</w> <w n="169.4">lendemains</w> !…</l>
				</lg>
				<lg n="22">
					<l n="170" num="22.1"><w n="170.1">Dieu</w> ?— <w n="170.2">Non</w>… <w n="170.3">Comme</w> <w n="170.4">un</w> <w n="170.5">sphynx</w>, <w n="170.6">l</w>'<w n="170.7">égoïsme</w>,</l>
					<l n="171" num="22.2"><w n="171.1">Assis</w> <w n="171.2">aux</w> <w n="171.3">portes</w> <w n="171.4">des</w> <w n="171.5">cités</w>,</l>
					<l n="172" num="22.3"><w n="172.1">La</w> <w n="172.2">dent</w> <w n="172.3">dehors</w>, <w n="172.4">avec</w> <w n="172.5">cynisme</w>,</l>
					<l part="I" n="173" num="22.4"><w n="173.1">Garde</w> </l>
					<l part="F" n="173" num="22.4"><w n="173.2">Aux</w> <w n="173.3">moindres</w> <w n="173.4">velléités</w></l>
					<l n="174" num="22.5"><w n="174.1">D</w>'<w n="174.2">un</w> <w n="174.3">progrès</w>, <w n="174.4">en</w> <w n="174.5">brèche</w> <w n="174.6">avec</w> <w n="174.7">l</w>'<w n="174.8">ordre</w>,</l>
					<l n="175" num="22.6"><w n="175.1">Qui</w>, <w n="175.2">survenant</w>, <w n="175.3">a</w> <w n="175.4">toujours</w> <w n="175.5">tort</w>,</l>
					<l n="176" num="22.7"><w n="176.1">Le</w> <w n="176.2">chien</w>, <w n="176.3">sans</w> <w n="176.4">se</w> <w n="176.5">borner</w> <w n="176.6">à</w> <w n="176.7">mordre</w>,</l>
					<l n="177" num="22.8"><w n="177.1">Attrape</w>, <w n="177.2">écorche</w>, <w n="177.3">étrangle</w>, <w n="177.4">tord</w> !</l>
				</lg>
				<lg n="23">
					<l n="178" num="23.1"><w n="178.1">Et</w> <w n="178.2">maintenant</w> <w n="178.3">le</w> <w n="178.4">sacrifice</w></l>
					<l n="179" num="23.2"><w n="179.1">Est</w> <w n="179.2">consommé</w>… <w n="179.3">Dormez</w>, <w n="179.4">ô</w> <w n="179.5">Morts</w> !</l>
					<l n="180" num="23.3"><w n="180.1">Le</w> <w n="180.2">Prussien</w> <w n="180.3">est</w> <w n="180.4">vainqueur</w>. <w n="180.5">Le</w> <w n="180.6">vice</w>,</l>
					<l n="181" num="23.4"><w n="181.1">O</w> <w n="181.2">Vérité</w>, <w n="181.3">sur</w> <w n="181.4">ton</w> <w n="181.5">beau</w> <w n="181.6">corps</w></l>
					<l n="182" num="23.5"><w n="182.1">A</w> <w n="182.2">fait</w> <w n="182.3">estocade</w> <w n="182.4">nouvelle</w>.</l>
					<l n="183" num="23.6"><w n="183.1">O</w> <w n="183.2">mon</w> <w n="183.3">pays</w>, <w n="183.4">recueille</w>-<w n="183.5">toi</w>.</l>
					<l n="184" num="23.7"><w n="184.1">A</w> <w n="184.2">toi</w>, <w n="184.3">France</w>, <w n="184.4">l</w>'<w n="184.5">arche</w> <w n="184.6">immortelle</w> !</l>
					<l n="185" num="23.8"><w n="185.1">Marche</w>, — <w n="185.2">Dieu</w> <w n="185.3">te</w> <w n="185.4">protège</w>,— <w n="185.5">Foi</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Septembre 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>