<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">PENDANT L’INVASION</title>
				<title type="medium">Édition électronique</title>
				<author key="SOU">
					<name>
						<forename>Joséphin</forename>
						<surname>SOULARY</surname>
					</name>
					<date from="1815" to="1891">1815-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>650 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">SOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>PENDANT L’INVASION</title>
						<author>JOSÉPHIN SOULARY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=_IuxR_m8ok8C</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PENDANT L’INVASION</title>
								<author>JOSÉPHIN SOULARY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="SOU1">
				<head type="main">LE CANTIQUE<lb></lb>DU ROI GUILLAUME</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Adveniet sicut latro.</quote>
							<bibl>
								<name>(Nov. Testam.)</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dieu</w> <w n="1.2">des</w> <w n="1.3">tendres</w> <w n="1.4">pitiés</w>, <w n="1.5">Dieu</w> <w n="1.6">des</w> <w n="1.7">vertus</w> <w n="1.8">clémentes</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="2.1">Vois</w> ! — <w n="2.2">D</w>’<w n="2.3">un</w> <w n="2.4">cœur</w> <w n="2.5">humble</w> <w n="2.6">et</w> <w n="2.7">pénitent</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">m</w>’<w n="3.3">approche</w> <w n="3.4">de</w> <w n="3.5">toi</w>, <w n="3.6">les</w> <w n="3.7">mains</w> <w n="3.8">toutes</w> <w n="3.9">fumantes</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="4.1">De</w> <w n="4.2">ce</w> <w n="4.3">sang</w> <w n="4.4">chaud</w> <w n="4.5">qui</w> <w n="4.6">te</w> <w n="4.7">plaît</w> <w n="4.8">tant</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Instrument</w> <w n="5.2">résigné</w> <w n="5.3">de</w> <w n="5.4">ta</w>. <w n="5.5">sainte</w> <w n="5.6">colère</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="6.1">Je</w> <w n="6.2">contemple</w>, — <w n="6.3">et</w> <w n="6.4">j</w>'<w n="6.5">en</w> <w n="6.6">ai</w> <w n="6.7">pleuré</w>, —</l>
						<l n="7" num="2.3"><w n="7.1">Tes</w> <w n="7.2">ennemis</w> <w n="7.3">couchés</w> <w n="7.4">comme</w> <w n="7.5">l</w>'<w n="7.6">épi</w> <w n="7.7">sur</w> <w n="7.8">l</w>’<w n="7.9">aire</w>.</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="8.1">Suis</w>-<w n="8.2">je</w> <w n="8.3">un</w> <w n="8.4">fléau</w> <w n="8.5">selon</w> <w n="8.6">ton</w> <w n="8.7">gré</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Cent</w> <w n="9.2">mille</w> <w n="9.3">sont</w> <w n="9.4">tombés</w> <w n="9.5">à</w> <w n="9.6">ma</w> <w n="9.7">droite</w>, <w n="9.8">à</w> <w n="9.9">ma</w> <w n="9.10">gauche</w> !</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="10.1">David</w> <w n="10.2">fit</w> <w n="10.3">moins</w>, <w n="10.4">à</w> <w n="10.5">beaucoup</w> <w n="10.6">près</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2">homme</w> <w n="11.3">alors</w> <w n="11.4">s</w>’<w n="11.5">égorgeait</w> ; <w n="11.6">de</w> <w n="11.7">nos</w> <w n="11.8">jours</w> <w n="11.9">il</w> <w n="11.10">se</w> <w n="11.11">fauche</w> :</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="12.1">Ta</w> <w n="12.2">loi</w> <w n="12.3">d</w>’<w n="12.4">amour</w> <w n="12.5">est</w> <w n="12.6">en</w> <w n="12.7">progrès</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Des</w> <w n="13.2">nouveaux</w> <w n="13.3">Philistins</w> <w n="13.4">j</w>’<w n="13.5">ai</w> <w n="13.6">défait</w> <w n="13.7">la</w> <w n="13.8">cohorte</w> ;</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="14.1">Mais</w> <w n="14.2">plus</w> <w n="14.3">avisé</w> <w n="14.4">que</w> <w n="14.5">Samson</w>,</l>
						<l n="15" num="4.3"><w n="15.1">J</w> ’<w n="15.2">avais</w> <w n="15.3">renforcé</w> <w n="15.4">Moltke</w>, — <w n="15.5">une</w> <w n="15.6">mâchoire</w> <w n="15.7">forte</w>, —</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="16.1">D</w>’<w n="16.2">un</w> <w n="16.3">engin</w> <w n="16.4">Krupp</w> <w n="16.5">de</w> <w n="16.6">ma</w> <w n="16.7">façon</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ces</w> <w n="17.2">Francs</w>, <w n="17.3">fils</w> <w n="17.4">de</w> <w n="17.5">Baal</w>, <w n="17.6">n</w>'<w n="17.7">ont</w>-<w n="17.8">ils</w> <w n="17.9">pas</w> <w n="17.10">l</w>’<w n="17.11">impudence</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="18.1">De</w> <w n="18.2">combattre</w> <w n="18.3">en</w> <w n="18.4">pleine</w> <w n="18.5">clarté</w> ?</l>
						<l n="19" num="5.3"><w n="19.1">Nous</w>, <w n="19.2">Seigneur</w>, <w n="19.3">que</w> <w n="19.4">tu</w> <w n="19.5">fis</w> <w n="19.6">serpents</w> <w n="19.7">par</w> <w n="19.8">la</w> <w n="19.9">prudence</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="20.1">Et</w> <w n="20.2">loups</w> <w n="20.3">par</w> <w n="20.4">la</w> <w n="20.5">férocité</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Dans</w> <w n="21.2">le</w> <w n="21.3">ravin</w> <w n="21.4">muet</w>, <w n="21.5">sous</w> <w n="21.6">la</w> <w n="21.7">forêt</w> <w n="21.8">confuse</w> ,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="22.1">Nous</w> <w n="22.2">glissons</w>, <w n="22.3">furtifs</w> <w n="22.4">et</w> <w n="22.5">rampants</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Dix</w> <w n="23.2">contre</w> <w n="23.3">un</w> ! — <w n="23.4">Nous</w> <w n="23.5">fondons</w> <w n="23.6">par</w> <w n="23.7">l</w>’<w n="23.8">éclat</w> <w n="23.9">de</w> <w n="23.10">la</w> <w n="23.11">ruse</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="24.1">L</w>’<w n="24.2">héroïsme</w> <w n="24.3">du</w> <w n="24.4">guet</w>-<w n="24.5">apens</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Sous</w> <w n="25.2">ta</w> <w n="25.3">bannière</w> <w n="25.4">même</w>, <w n="25.5">aux</w> <w n="25.6">blessés</w> <w n="25.7">si</w> <w n="25.8">bénigne</w>,</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="26.1">Nous</w> <w n="26.2">dressons</w> <w n="26.3">nos</w> <w n="26.4">pièges</w> <w n="26.5">de</w> <w n="26.6">mort</w>.</l>
						<l n="27" num="7.3"><w n="27.1">N</w>’<w n="27.2">a</w>-<w n="27.3">t</w>-<w n="27.4">il</w> <w n="27.5">pas</w> <w n="27.6">été</w> <w n="27.7">dit</w> : « <w n="27.8">Tu</w> <w n="27.9">vaincras</w> <w n="27.10">par</w> <w n="27.11">ce</w> <w n="27.12">signe</w> ? »</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="28.1">Ta</w> <w n="28.2">parole</w> <w n="28.3">n</w>’<w n="28.4">a</w> <w n="28.5">jamais</w> <w n="28.6">tort</w> !</l>
						</lg>
					</div>
				<div type="section" n="2">
						<head type="number">II</head>
					<lg n="1">
						<l n="29" num="1.1"><w n="29.1">Qui</w> <w n="29.2">soutiendra</w> <w n="29.3">le</w> <w n="29.4">choc</w> <w n="29.5">des</w> <w n="29.6">miens</w> ? — <w n="29.7">De</w> <w n="29.8">vos</w> <w n="29.9">valises</w></l>
						<l n="30" num="1.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="30.1">Qui</w> <w n="30.2">sondera</w> <w n="30.3">la</w> <w n="30.4">profondeur</w>,</l>
						<l n="31" num="1.3"><w n="31.1">Der</w> <w n="31.2">Thann</w>, <w n="31.3">héros</w> <w n="31.4">pillard</w> ; <w n="31.5">Werder</w>, <w n="31.6">brûleur</w> <w n="31.7">d</w>'<w n="31.8">églises</w> ;</l>
						<l n="32" num="1.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="32.1">Von</w> <w n="32.2">Treskow</w>, <w n="32.3">gendarme</w> <w n="32.4">pendeur</w> ?</l>
					</lg>
					<lg n="2">
						<l n="33" num="2.1"><w n="33.1">L</w>’<w n="33.2">épouvante</w> <w n="33.3">et</w> <w n="33.4">le</w> <w n="33.5">deuil</w> <w n="33.6">vous</w> <w n="33.7">suivent</w> <w n="33.8">à</w> <w n="33.9">la</w> <w n="33.10">trace</w> ;</l>
						<l n="34" num="2.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="34.1">Mais</w>, <w n="34.2">Seigneur</w>, <w n="34.3">quel</w> <w n="34.4">juste</w> <w n="34.5">est</w> <w n="34.6">parfait</w> ?</l>
						<l n="35" num="2.3"><w n="35.1">Au</w> <w n="35.2">nom</w> <w n="35.3">du</w> <w n="35.4">mal</w> <w n="35.5">commis</w> <w n="35.6">je</w> <w n="35.7">te</w> <w n="35.8">demande</w> <w n="35.9">grâce</w></l>
						<l n="36" num="2.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="36.1">Pour</w> <w n="36.2">tout</w> <w n="36.3">le</w> <w n="36.4">mal</w> <w n="36.5">qu</w>’<w n="36.6">ils</w> <w n="36.7">n</w>'<w n="36.8">ont</w> <w n="36.9">pas</w> <w n="36.10">fait</w> !</l>
					</lg>
					<lg n="3">
						<l n="37" num="3.1"><w n="37.1">Puis</w>-<w n="37.2">je</w> <w n="37.3">au</w> <w n="37.4">moins</w> <w n="37.5">me</w> <w n="37.6">flatter</w> <w n="37.7">que</w> <w n="37.8">dans</w> <w n="37.9">ces</w> <w n="37.10">aventures</w>,</l>
						<l n="38" num="3.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="38.1">Jaloux</w> <w n="38.2">de</w> <w n="38.3">l</w>’<w n="38.4">honneur</w> <w n="38.5">à</w> <w n="38.6">gagner</w>,</l>
						<l n="39" num="3.3"><w n="39.1">Nous</w> <w n="39.2">sûmes</w> <w n="39.3">inventer</w> <w n="39.4">d</w>'<w n="39.5">assez</w> <w n="39.6">neuves</w> <w n="39.7">tortures</w> ?</l>
						<l n="40" num="3.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="40.1">L</w>’<w n="40.2">Alsace</w> <w n="40.3">en</w> <w n="40.4">pourrait</w> <w n="40.5">témoigner</w>.</l>
					</lg>
					<lg n="4">
						<l n="41" num="4.1"><w n="41.1">J</w>’<w n="41.2">ai</w> <w n="41.3">rasé</w> <w n="41.4">sous</w> <w n="41.5">le</w> <w n="41.6">feu</w> <w n="41.7">les</w> <w n="41.8">chaumières</w> <w n="41.9">souillées</w>,</l>
						<l n="42" num="4.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="42.1">Et</w> <w n="42.2">j</w>’<w n="42.3">ai</w> <w n="42.4">fait</w> <w n="42.5">fumer</w> <w n="42.6">à</w> <w n="42.7">ton</w> <w n="42.8">nez</w></l>
						<l n="43" num="4.3"><w n="43.1">Suave</w> <w n="43.2">encens</w>, <w n="43.3">la</w> <w n="43.4">chair</w> <w n="43.5">des</w> <w n="43.6">pauvresses</w> <w n="43.7">grillées</w></l>
						<l n="44" num="4.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="44.1">Et</w> <w n="44.2">des</w> <w n="44.3">paysans</w> <w n="44.4">calcinés</w>.</l>
					</lg>
					<lg n="5">
						<l n="45" num="5.1"><w n="45.1">Le</w> <w n="45.2">lion</w> <w n="45.3">renaîtrait</w> <w n="45.4">d</w>’<w n="45.5">une</w> <w n="45.6">souche</w> <w n="45.7">nouvelle</w> ;</l>
						<l n="46" num="5.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="46.1">Pour</w> <w n="46.2">supprimer</w> <w n="46.3">le</w> <w n="46.4">lionceau</w>,</l>
						<l n="47" num="5.3"><w n="47.1">Sur</w> <w n="47.2">les</w> <w n="47.3">angles</w> <w n="47.4">des</w> <w n="47.5">murs</w> <w n="47.6">j</w>’<w n="47.7">ai</w> <w n="47.8">broyé</w> <w n="47.9">la</w> <w n="47.10">cervelle</w></l>
						<l n="48" num="5.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="48.1">Des</w> <w n="48.2">petits</w> <w n="48.3">enfants</w> <w n="48.4">au</w> <w n="48.5">berceau</w>.</l>
					</lg>
					<lg n="6">
						<l n="49" num="6.1"><w n="49.1">Enfin</w>, <w n="49.2">sanctifiant</w> <w n="49.3">ainsi</w> <w n="49.4">l</w>’<w n="49.5">auguste</w> <w n="49.6">orgie</w>,</l>
						<l n="50" num="6.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="50.1">Dans</w> <w n="50.2">tes</w> <w n="50.3">enfers</w> <w n="50.4">j</w>'<w n="50.5">ai</w> <w n="50.6">dépêché</w></l>
						<l n="51" num="6.3"><w n="51.1">Par</w> <w n="51.2">le</w> <w n="51.3">viol</w> <w n="51.4">mortel</w> <w n="51.5">mainte</w> <w n="51.6">vierge</w>, <w n="51.7">rougie</w></l>
						<l n="52" num="6.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="52.1">De</w> <w n="52.2">son</w> <w n="52.3">sang</w> <w n="52.4">et</w> <w n="52.5">de</w> <w n="52.6">son</w> <w n="52.7">péché</w>.</l>
					</lg>
					<lg n="7">
						<l n="53" num="7.1"><w n="53.1">Et</w> <w n="53.2">voici</w> : <w n="53.3">j</w>’<w n="53.4">ai</w> <w n="53.5">cerné</w> <w n="53.6">la</w> <w n="53.7">Babel</w> <w n="53.8">des</w> <w n="53.9">barbares</w> ;</l>
						<l n="54" num="7.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="54.1">De</w> <w n="54.2">sa</w> <w n="54.3">chute</w> <w n="54.4">j</w>’<w n="54.5">attends</w> <w n="54.6">l</w>’<w n="54.7">écho</w>,</l>
						<l n="55" num="7.3"><w n="55.1">Dès</w> <w n="55.2">que</w> <w n="55.3">Wagner</w> <w n="55.4">aura</w> <w n="55.5">composé</w> <w n="55.6">les</w> <w n="55.7">fanfares</w></l>
						<l n="56" num="7.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="56.1">Qui</w> <w n="56.2">firent</w> <w n="56.3">tomber</w> <w n="56.4">Jéricho</w>.</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1"><w n="57.1">Elle</w> <w n="57.2">a</w> <w n="57.3">vécu</w> <w n="57.4">la</w> <w n="57.5">ville</w> <w n="57.6">aux</w> <w n="57.7">pratiques</w> <w n="57.8">infâmes</w> !</l>
						<l n="58" num="8.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="58.1">Et</w> <w n="58.2">l</w>’<w n="58.3">ange</w> <w n="58.4">blême</w> <w n="58.5">de</w> <w n="58.6">la</w> <w n="58.7">faim</w></l>
						<l n="59" num="8.3"><w n="59.1">Dans</w> <w n="59.2">le</w> <w n="59.3">dernier</w> <w n="59.4">soupir</w> <w n="59.5">de</w> <w n="59.6">deux</w> <w n="59.7">millions</w> <w n="59.8">d</w>’<w n="59.9">âmes</w></l>
						<l n="60" num="8.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="60.1">Aura</w> <w n="60.2">dit</w> <w n="60.3">le</w> <w n="60.4">mot</w> <w n="60.5">de</w> <w n="60.6">la</w> <w n="60.7">fin</w> !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="61" num="1.1"><w n="61.1">Ta</w> <w n="61.2">justice</w>, <w n="61.3">ô</w> <w n="61.4">Seigneur</w> ! <w n="61.5">est</w> <w n="61.6">comme</w> <w n="61.7">la</w> <w n="61.8">tortue</w>,</l>
						<l n="62" num="1.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="62.1">Lente</w>, <w n="62.2">mais</w> <w n="62.3">sûre</w> <w n="62.4">d</w>’<w n="62.5">arriver</w>.</l>
						<l n="63" num="1.3"><w n="63.1">La</w> <w n="63.2">mienne</w> <w n="63.3">a</w> <w n="63.4">pris</w> <w n="63.5">son</w> <w n="63.6">temps</w> ; <w n="63.7">ma</w> <w n="63.8">rancune</w> <w n="63.9">têtue</w></l>
						<l n="64" num="1.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="64.1">Mit</w> <w n="64.2">cinquante</w> <w n="64.3">ans</w> <w n="64.4">à</w> <w n="64.5">la</w> <w n="64.6">couver</w>.</l>
					</lg>
					<lg n="2">
						<l n="65" num="2.1"><w n="65.1">Oui</w>, <w n="65.2">depuis</w> <w n="65.3">Iéna</w>, <w n="65.4">je</w> <w n="65.5">n</w>’<w n="65.6">ai</w> <w n="65.7">pu</w> <w n="65.8">sans</w> <w n="65.9">souffrance</w></l>
						<l n="66" num="2.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="66.1">Digérer</w> <w n="66.2">le</w> <w n="66.3">rire</w> <w n="66.4">latin</w>.</l>
						<l n="67" num="2.3"><w n="67.1">Digérer</w> <w n="67.2">est</w> <w n="67.3">le</w> <w n="67.4">mot</w> ; <w n="67.5">s</w>’<w n="67.6">ils</w> <w n="67.7">sont</w> <w n="67.8">tout</w> <w n="67.9">cœur</w> <w n="67.10">en</w> <w n="67.11">France</w>,</l>
						<l n="68" num="2.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="68.1">Chez</w> <w n="68.2">nous</w> <w n="68.3">on</w> <w n="68.4">est</w> <w n="68.5">tout</w> <w n="68.6">intestin</w>.</l>
					</lg>
					<lg n="3">
						<l n="69" num="3.1"><w n="69.1">Bismarck</w> <w n="69.2">a</w> <w n="69.3">des</w> <w n="69.4">conseils</w> <w n="69.5">loyaux</w> <w n="69.6">sur</w> <w n="69.7">toutes</w> <w n="69.8">choses</w> ;</l>
						<l n="70" num="3.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="70.1">Il</w> <w n="70.2">me</w> <w n="70.3">souffla</w> <w n="70.4">l</w>’<w n="70.5">avis</w> <w n="70.6">divin</w></l>
						<l n="71" num="3.3"><w n="71.1">D</w>'<w n="71.2">envoyer</w> <w n="71.3">nos</w> <w n="71.4">enfants</w>, <w n="71.5">chiens</w>-<w n="71.6">couchants</w> <w n="71.7">doux</w> <w n="71.8">e</w> ; <w n="71.9">roses</w>,</l>
						<l n="72" num="3.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="72.1">Mendier</w> <w n="72.2">au</w> <w n="72.3">pays</w> <w n="72.4">du</w> <w n="72.5">vin</w>.</l>
					</lg>
					<lg n="4">
						<l n="73" num="4.1"><w n="73.1">Comment</w> <w n="73.2">se</w> <w n="73.3">défier</w> <w n="73.4">de</w> <w n="73.5">ces</w> <w n="73.6">souples</w> <w n="73.7">carrures</w> ?</l>
						<l n="74" num="4.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="74.1">Tout</w> <w n="74.2">foyer</w> <w n="74.3">leur</w> <w n="74.4">fut</w> <w n="74.5">indulgent</w>.</l>
						<l n="75" num="4.3"><w n="75.1">Mes</w> <w n="75.2">chérubins</w> <w n="75.3">ont</w> <w n="75.4">pris</w> <w n="75.5">l</w>'<w n="75.6">empreinte</w> <w n="75.7">des</w> <w n="75.8">serrures</w> :</l>
						<l n="76" num="4.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="76.1">A</w> <w n="76.2">moi</w> <w n="76.3">la</w> <w n="76.4">cave</w> ! <w n="76.5">à</w> <w n="76.6">moi</w> <w n="76.7">l</w>’<w n="76.8">argent</w> !</l>
					</lg>
					<lg n="5">
						<l n="77" num="5.1"><w n="77.1">O</w> <w n="77.2">Saint</w> <w n="77.3">espionnage</w> ! <w n="77.4">ô</w> <w n="77.5">fausse</w> <w n="77.6">clef</w> <w n="77.7">du</w> <w n="77.8">traître</w> !</l>
						<l n="78" num="5.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="78.1">O</w> <w n="78.2">couteau</w> <w n="78.3">sournois</w> <w n="78.4">du</w> <w n="78.5">poltron</w> !</l>
						<l n="79" num="5.3"><w n="79.1">Grâce</w> <w n="79.2">à</w> <w n="79.3">toi</w>, <w n="79.4">j</w>’<w n="79.5">accomplis</w> <w n="79.6">la</w> <w n="79.7">parole</w> <w n="79.8">du</w> <w n="79.9">Maître</w> :</l>
						<l n="80" num="5.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space>« <w n="80.1">J</w>’<w n="80.2">arriverai</w> <w n="80.3">comme</w> <w n="80.4">un</w> <w n="80.5">larron</w> ! »</l>
					</lg>
					<lg n="6">
						<l n="81" num="6.1"><w n="81.1">Si</w> <w n="81.2">j</w>’<w n="81.3">ai</w>, <w n="81.4">dans</w> <w n="81.5">leurs</w> <w n="81.6">celliers</w> <w n="81.7">où</w> <w n="81.8">l</w>'<w n="81.9">Aï</w> <w n="81.10">coule</w> <w n="81.11">à</w> <w n="81.12">source</w>,</l>
						<l n="82" num="6.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="82.1">Défonce</w>’ <w n="82.2">jusqu</w>’<w n="82.3">au</w> <w n="82.4">dernier</w> <w n="82.5">fût</w> ;</l>
						<l n="83" num="6.3"><w n="83.1">Si</w>, <w n="83.2">jusqu</w>’<w n="83.3">au</w> <w n="83.4">dernier</w> <w n="83.5">sou</w>, <w n="83.6">j</w>’<w n="83.7">ai</w> <w n="83.8">cueilli</w> <w n="83.9">dans</w> <w n="83.10">leur</w> <w n="83.11">bourse</w></l>
						<l n="84" num="6.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="84.1">L</w>’<w n="84.2">argent</w> , — <w n="84.3">vil</w> <w n="84.4">métal</w> <w n="84.5">s</w>’<w n="84.6">il</w> <w n="84.7">en</w> <w n="84.8">fut</w>,</l>
					</lg>
					<lg n="7">
						<l n="85" num="7.1"><w n="85.1">Grand</w> <w n="85.2">Dieu</w>, <w n="85.3">c</w>'<w n="85.4">est</w> <w n="85.5">pour</w> <w n="85.6">ton</w> <w n="85.7">bien</w> ! <w n="85.8">leur</w> <w n="85.9">luxe</w> <w n="85.10">était</w> <w n="85.11">leur</w> <w n="85.12">crime</w> ;</l>
						<l n="86" num="7.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="86.1">Et</w> <w n="86.2">les</w> <w n="86.3">vertus</w> <w n="86.4">du</w> <w n="86.5">Germain</w> <w n="86.6">blond</w></l>
						<l n="87" num="7.3"><w n="87.1">Disent</w> <w n="87.2">assez</w> <w n="87.3">la</w> <w n="87.4">grâce</w> <w n="87.5">attachée</w> <w n="87.6">au</w> <w n="87.7">régime</w></l>
						<l n="88" num="7.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="88.1">De</w> <w n="88.2">la</w> <w n="88.3">choucroûte</w> <w n="88.4">et</w> <w n="88.5">du</w> <w n="88.6">houblon</w>.</l>
					</lg>
					<lg n="8">
						<l n="89" num="8.1"><w n="89.1">Marche</w> <w n="89.2">à</w> <w n="89.3">présent</w> <w n="89.4">sans</w> <w n="89.5">peur</w>, <w n="89.6">ma</w> <w n="89.7">Prusse</w>, <w n="89.8">et</w> <w n="89.9">t</w>’<w n="89.10">achemine</w></l>
						<l n="90" num="8.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="90.1">Au</w> <w n="90.2">Chanaan</w> <w n="90.3">que</w> <w n="90.4">tu</w> <w n="90.5">rêvais</w> !</l>
						<l n="91" num="8.3"><w n="91.1">Car</w> <w n="91.2">ta</w> <w n="91.3">race</w> <w n="91.4">est</w> <w n="91.5">féconde</w>, <w n="91.6">et</w>, <w n="91.7">comme</w> <w n="91.8">la</w> <w n="91.9">vermine</w>,</l>
						<l n="92" num="8.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="92.1">Tient</w> <w n="92.2">de</w> <w n="92.3">la</w> <w n="92.4">place</w> <w n="92.5">et</w> <w n="92.6">sent</w> <w n="92.7">mauvais</w>.</l>
					</lg>
					<lg n="9">
						<l n="93" num="9.1"><w n="93.1">Va</w> <w n="93.2">gaiement</w> <w n="93.3">vendanger</w> <w n="93.4">cette</w> <w n="93.5">terre</w> <w n="93.6">promise</w> ;</l>
						<l n="94" num="9.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="94.1">Mais</w>, <w n="94.2">dans</w> <w n="94.3">l</w>’<w n="94.4">ivresse</w> <w n="94.5">du</w> <w n="94.6">raisin</w>,</l>
						<l n="95" num="9.3"><w n="95.1">Garde</w>-<w n="95.2">toi</w> <w n="95.3">d</w>’<w n="95.4">oublier</w> <w n="95.5">notre</w> <w n="95.6">antique</w> <w n="95.7">devise</w> :</l>
						<l n="96" num="9.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space>« <w n="96.1">Mon</w> <w n="96.2">ennemi</w>, <w n="96.3">c</w>'<w n="96.4">est</w> <w n="96.5">mon</w> <w n="96.6">voisin</w> ! »</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">IV</head>
					<lg n="1">
						<l n="97" num="1.1"><w n="97.1">Impose</w> <w n="97.2">un</w> <w n="97.3">frein</w>, <w n="97.4">Seigneur</w>, <w n="97.5">à</w> <w n="97.6">la</w> <w n="97.7">voix</w> <w n="97.8">qui</w> <w n="97.9">me</w> <w n="97.10">loue</w> !</l>
						<l n="98" num="1.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="98.1">Fais</w> <w n="98.2">que</w> <w n="98.3">mon</w> <w n="98.4">cœur</w> <w n="98.5">soit</w> <w n="98.6">sans</w> <w n="98.7">détour</w> !</l>
						<l n="99" num="1.3"><w n="99.1">On</w> <w n="99.2">m</w>’<w n="99.3">a</w> <w n="99.4">surnommé</w> <w n="99.5">l</w>’<w n="99.6">aigle</w> ! — <w n="99.7">on</w> <w n="99.8">me</w> <w n="99.9">flatte</w>, <w n="99.10">et</w> <w n="99.11">j</w>’<w n="99.12">avoue</w></l>
						<l n="100" num="1.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="100.1">Que</w> <w n="100.2">je</w> <w n="100.3">suis</w> <w n="100.4">à</w> <w n="100.5">peine</w> <w n="100.6">un</w> <w n="100.7">vautour</w>.</l>
					</lg>
					<lg n="2">
						<l n="101" num="2.1"><w n="101.1">Donne</w>-<w n="101.2">moi</w> <w n="101.3">de</w> <w n="101.4">braver</w> <w n="101.5">le</w> <w n="101.6">dégoût</w> <w n="101.7">de</w> <w n="101.8">l</w>’<w n="101.9">histoire</w></l>
						<l n="102" num="2.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="102.1">Où</w>, <w n="102.2">d</w>’<w n="102.3">un</w> <w n="102.4">saut</w>, <w n="102.5">j</w>'<w n="102.6">entre</w> <w n="102.7">tout</w> <w n="102.8">botté</w>.</l>
						<l n="103" num="2.3"><w n="103.1">Fi</w> <w n="103.2">du</w> <w n="103.3">respect</w> <w n="103.4">humain</w> ! <w n="103.5">pour</w> <w n="103.6">ta</w> <w n="103.7">plus</w> <w n="103.8">grande</w> <w n="103.9">gloire</w></l>
						<l n="104" num="2.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="104.1">Son</w> <w n="104.2">mépris</w> <w n="104.3">n</w>'<w n="104.4">est</w>-<w n="104.5">il</w> <w n="104.6">pas</w> <w n="104.7">compté</w> ?</l>
					</lg>
					<lg n="3">
						<l n="105" num="3.1"><w n="105.1">De</w> <w n="105.2">l</w>'<w n="105.3">homme</w> <w n="105.4">après</w> <w n="105.5">le</w> <w n="105.6">roi</w> <w n="105.7">qu</w>’<w n="105.8">importe</w> <w n="105.9">ce</w> <w n="105.10">qui</w> <w n="105.11">reste</w>,</l>
						<l n="106" num="3.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="106.1">S</w>’<w n="106.2">il</w> <w n="106.3">est</w> <w n="106.4">utile</w> <w n="106.5">à</w> <w n="106.6">ton</w> <w n="106.7">dessein</w></l>
						<l n="107" num="3.3"><w n="107.1">Que</w> <w n="107.2">le</w> <w n="107.3">monde</w> <w n="107.4">amnistie</w> <w n="107.5">Attila</w> <w n="107.6">le</w> <w n="107.7">Funeste</w></l>
						<l n="108" num="3.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="108.1">Devant</w> <w n="108.2">Guillaume</w> <w n="108.3">l</w>’<w n="108.4">Assassin</w> ?</l>
					</lg>
					<lg n="4">
						<l n="109" num="4.1"><w n="109.1">Je</w> <w n="109.2">sais</w> <w n="109.3">qu</w>’<w n="109.4">aux</w> <w n="109.5">cieux</w>, <w n="109.6">selon</w> <w n="109.7">ta</w> <w n="109.8">promesse</w> <w n="109.9">formelle</w>,</l>
						<l n="110" num="4.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="110.1">J</w> ’<w n="110.2">aurai</w> <w n="110.3">place</w>, <w n="110.4">ô</w> <w n="110.5">Père</w> <w n="110.6">infini</w>,</l>
						<l n="111" num="4.3"><w n="111.1">Entre</w> <w n="111.2">mon</w> <w n="111.3">Augusta</w>, <w n="111.4">ma</w> <w n="111.5">pieuse</w> <w n="111.6">femelle</w>,</l>
						<l n="112" num="4.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="112.1">Et</w> <w n="112.2">notre</w> <w n="112.3">Fritz</w>, <w n="112.4">son</w> <w n="112.5">fruit</w> <w n="112.6">béni</w> !</l>
					</lg>
					<lg n="5">
						<l n="113" num="5.1"><w n="113.1">Et</w> <w n="113.2">pourtant</w>, <w n="113.3">s</w>’<w n="113.4">il</w> <w n="113.5">te</w> <w n="113.6">plaît</w> <w n="113.7">de</w> <w n="113.8">m</w>’<w n="113.9">éprouver</w>, <w n="113.10">ordonne</w> !</l>
						<l n="114" num="5.2"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="114.1">Puisqu</w>’<w n="114.2">avec</w> <w n="114.3">toi</w> <w n="114.4">j</w>’<w n="114.5">ai</w> <w n="114.6">fait</w> <w n="114.7">hymen</w>,</l>
						<l n="115" num="5.3"><w n="115.1">Pour</w> <w n="115.2">toi</w> <w n="115.3">je</w> <w n="115.4">souffrirai</w> <w n="115.5">tout</w> !… — <w n="115.6">même</w> <w n="115.7">la</w> <w n="115.8">couronne</w></l>
						<l n="116" num="5.4"><space unit="char" quantity="8"></space><space quantity="8" unit="char"></space><w n="116.1">D</w>'<w n="116.2">empereur</w> <w n="116.3">d</w>’<w n="116.4">Occident</w>. — <w n="116.5">Amen</w> !</l>
					</lg>
				</div>
				<closer>
					<dateline>
						<date when="1870">27 novembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>