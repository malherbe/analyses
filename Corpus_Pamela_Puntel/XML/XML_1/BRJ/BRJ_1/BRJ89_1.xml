<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ89">
					<head type="number">LXXXIX</head>
					<head type="main">LES ENFANTS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pauvres</w> <w n="1.2">enfants</w>, <w n="1.3">d</w>'<w n="1.4">où</w> <w n="1.5">venez</w>-<w n="1.6">vous</w> ?</l>
						<l n="2" num="1.2"><w n="2.1">Dit</w> <w n="2.2">Jésus</w>, <w n="2.3">paternel</w> <w n="2.4">et</w> <w n="2.5">doux</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Entrez</w> <w n="3.2">dans</w> <w n="3.3">mon</w> <w n="3.4">divin</w> <w n="3.5">royaume</w> !</l>
						<l n="4" num="1.4"><w n="4.1">Hélas</w> ! <w n="4.2">vous</w> <w n="4.3">êtes</w> <w n="4.4">tout</w> <w n="4.5">sanglants</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Qui</w> <w n="5.2">donc</w> <w n="5.3">a</w> <w n="5.4">déchiré</w> <w n="5.5">vos</w> <w n="5.6">flancs</w> ?…</l>
						<l n="6" num="1.6"><space unit="char" quantity="4"></space>— <w n="6.1">C</w>'<w n="6.2">est</w> <w n="6.3">Bismark</w> ! <w n="6.4">c</w>'<w n="6.5">est</w> <w n="6.6">Guillaume</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Pauvres</w> <w n="7.2">enfants</w>, <w n="7.3">vos</w> <w n="7.4">corps</w> <w n="7.5">flétris</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Vos</w> <w n="8.2">petits</w> <w n="8.3">membres</w> <w n="8.4">amaigris</w></l>
						<l n="9" num="2.3"><w n="9.1">Du</w> <w n="9.2">typhus</w>, <w n="9.3">livide</w> <w n="9.4">fantôme</w>,</l>
						<l n="10" num="2.4"><w n="10.1">Portent</w> <w n="10.2">le</w> <w n="10.3">signe</w> <w n="10.4">empoisonné</w> :</l>
						<l n="11" num="2.5"><w n="11.1">Qui</w> <w n="11.2">donc</w> <w n="11.3">sur</w> <w n="11.4">vous</w> <w n="11.5">l</w>'<w n="11.6">a</w> <w n="11.7">déchaîné</w> ?…</l>
						<l n="12" num="2.6"><space unit="char" quantity="4"></space>— <w n="12.1">C</w>'<w n="12.2">est</w> <w n="12.3">Bismark</w> ! <w n="12.4">c</w>'<w n="12.5">est</w> <w n="12.6">Guillaume</w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Pauvres</w> <w n="13.2">enfants</w>, <w n="13.3">dans</w> <w n="13.4">vos</w> <w n="13.5">pâleurs</w></l>
						<l n="14" num="3.2"><w n="14.1">Je</w> <w n="14.2">lis</w> <w n="14.3">encor</w> <w n="14.4">d</w>'<w n="14.5">autres</w> <w n="14.6">douleurs</w> ;</l>
						<l n="15" num="3.3"><w n="15.1">Quel</w> <w n="15.2">est</w> <w n="15.3">ce</w> <w n="15.4">pain</w> <w n="15.5">mêlé</w> <w n="15.6">de</w> <w n="15.7">chaume</w> ?</l>
						<l n="16" num="3.4"><w n="16.1">Est</w>-<w n="16.2">ce</w> <w n="16.3">du</w> <w n="16.4">sable</w> ? <w n="16.5">est</w>-<w n="16.6">ce</w> <w n="16.7">du</w> <w n="16.8">pain</w> ?</l>
						<l n="17" num="3.5"><w n="17.1">Qui</w> <w n="17.2">donc</w> <w n="17.3">vous</w> <w n="17.4">lit</w> <w n="17.5">mourir</w> <w n="17.6">de</w> <w n="17.7">faim</w> ?</l>
						<l n="18" num="3.6"><space unit="char" quantity="4"></space>— <w n="18.1">C</w>'<w n="18.2">est</w> <w n="18.3">Bismark</w> ! <w n="18.4">c</w>'<w n="18.5">est</w> <w n="18.6">Guillaume</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">Janvier 1871.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>