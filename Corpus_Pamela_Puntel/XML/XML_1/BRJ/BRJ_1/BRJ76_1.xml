<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ76">
					<head type="number">LXXVI</head>
					<head type="main">UN PÈRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>'<w n="1.2">héroïsme</w>, <w n="1.3">parfois</w>, <w n="1.4">prend</w> <w n="1.5">un</w> <w n="1.6">air</w> <w n="1.7">familier</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">peut</w> <w n="2.3">déconcerter</w> <w n="2.4">la</w> <w n="2.5">muse</w> <w n="2.6">avec</w> <w n="2.7">la</w> <w n="2.8">rime</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">vers</w> <w n="3.3">à</w> <w n="3.4">certains</w> <w n="3.5">mots</w> <w n="3.6">semble</w> <w n="3.7">mal</w> <w n="3.8">se</w> <w n="3.9">plier</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Mais</w> <w n="4.2">qu</w>'<w n="4.3">importe</w> <w n="4.4">le</w> <w n="4.5">mot</w>, <w n="4.6">si</w> <w n="4.7">le</w> <w n="4.8">trait</w> <w n="4.9">est</w> <w n="4.10">sublime</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Un</w> <w n="5.2">bourgeois</w> <w n="5.3">voit</w> <w n="5.4">son</w> <w n="5.5">fils</w>, <w n="5.6">improvisé</w> <w n="5.7">soldat</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Du</w> <w n="6.2">comptoir</w> <w n="6.3">paternel</w> <w n="6.4">partir</w> <w n="6.5">pour</w> <w n="6.6">le</w> <w n="6.7">combat</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Son</w> <w n="7.2">cœur</w> <w n="7.3">n</w>'<w n="7.4">est</w> <w n="7.5">pas</w> <w n="7.6">armé</w> <w n="7.7">du</w> <w n="7.8">stoïcisme</w> <w n="7.9">antique</w> ;</l>
						<l n="8" num="2.4"><w n="8.1">Il</w> <w n="8.2">aime</w> <w n="8.3">son</w> <w n="8.4">enfant</w>, <w n="8.5">et</w> <w n="8.6">mourrait</w> <w n="8.7">de</w> <w n="8.8">sa</w> <w n="8.9">mort</w> ;</l>
						<l n="9" num="2.5"><w n="9.1">Autant</w> <w n="9.2">mourir</w> <w n="9.3">ensemble</w>. <w n="9.4">Il</w> <w n="9.5">ferme</w> <w n="9.6">sa</w> <w n="9.7">boutique</w>.</l>
						<l n="10" num="2.6"><w n="10.1">Va</w> <w n="10.2">rejoindre</w> <w n="10.3">son</w> <w n="10.4">fils</w>, <w n="10.5">l</w>'<w n="10.6">accompagne</w> <w n="10.7">au</w> <w n="10.8">plus</w> <w n="10.9">fort</w></l>
						<l n="11" num="2.7"><w n="11.1">De</w> <w n="11.2">la</w> <w n="11.3">bataille</w>, <w n="11.4">et</w>, <w n="11.5">calme</w>, <w n="11.6">indifférent</w>, <w n="11.7">essuie</w></l>
						<l n="12" num="2.8"><w n="12.1">Le</w> <w n="12.2">feu</w> <w n="12.3">de</w> <w n="12.4">l</w>'<w n="12.5">ennemi</w>, <w n="12.6">balles</w>, <w n="12.7">obus</w>, <w n="12.8">boulets</w>,</l>
						<l n="13" num="2.9"><w n="13.1">Écartant</w> <w n="13.2">au</w> <w n="13.3">besoin</w> <w n="13.4">sabres</w> <w n="13.5">et</w> <w n="13.6">pistolets</w></l>
						<l n="14" num="2.10"><w n="14.1">D</w>'<w n="14.2">une</w> <w n="14.3">arme</w> <w n="14.4">qui</w> <w n="14.5">fera</w> <w n="14.6">sourire</w>… <w n="14.7">un</w> <w n="14.8">parapluie</w> !</l>
						<l n="15" num="2.11"><w n="15.1">Chacun</w> <w n="15.2">a</w> <w n="15.3">ses</w> <w n="15.4">héros</w>, <w n="15.5">et</w> <w n="15.6">prône</w> <w n="15.7">leurs</w> <w n="15.8">vertus</w> ;</l>
						<l n="16" num="2.12"><w n="16.1">Je</w> <w n="16.2">préfère</w>, <w n="16.3">pour</w> <w n="16.4">moi</w>, <w n="16.5">mon</w> <w n="16.6">bourgeois</w> <w n="16.7">à</w> <w n="16.8">Brutus</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>