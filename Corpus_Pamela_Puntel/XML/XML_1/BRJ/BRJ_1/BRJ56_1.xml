<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ56">
					<head type="number">LVI</head>
					<head type="main">PRIÈRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>'<w n="1.2">âme</w> <w n="1.3">d</w>'<w n="1.4">un</w> <w n="1.5">peuple</w> <w n="1.6">entier</w> <w n="1.7">s</w>'<w n="1.8">élève</w> <w n="1.9">à</w> <w n="1.10">toi</w>, <w n="1.11">Dieu</w> <w n="1.12">juste</w></l>
						<l n="2" num="1.2"><w n="2.1">Et</w>, <w n="2.2">dans</w> <w n="2.3">le</w> <w n="2.4">même</w> <w n="2.5">temps</w> <w n="2.6">inclinés</w> <w n="2.7">sous</w> <w n="2.8">ta</w> <w n="2.9">loi</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">appeler</w> <w n="3.3">sur</w> <w n="3.4">eux</w> <w n="3.5">ton</w> <w n="3.6">jugement</w> <w n="3.7">auguste</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">faibles</w> <w n="4.3">et</w> <w n="4.4">les</w> <w n="4.5">forts</w> <w n="4.6">se</w> <w n="4.7">retournent</w> <w n="4.8">vers</w> <w n="4.9">toi</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Une</w> <w n="5.2">même</w> <w n="5.3">douleur</w> <w n="5.4">a</w> <w n="5.5">fait</w> <w n="5.6">la</w> <w n="5.7">même</w> <w n="5.8">foi</w> !</l>
					</lg>
					<lg n="3">
						<l n="6" num="3.1"><w n="6.1">Ce</w> <w n="6.2">n</w>'<w n="6.3">est</w> <w n="6.4">pas</w> <w n="6.5">la</w> <w n="6.6">prière</w> <w n="6.7">impie</w> <w n="6.8">et</w> <w n="6.9">sacrilège</w></l>
						<l n="7" num="3.2"><w n="7.1">D</w>'<w n="7.2">un</w> <w n="7.3">roi</w> <w n="7.4">qui</w> <w n="7.5">t</w>'<w n="7.6">associe</w> <w n="7.7">à</w> <w n="7.8">ses</w> <w n="7.9">ambitions</w>,</l>
						<l n="8" num="3.3"><w n="8.1">Qui</w> <w n="8.2">croit</w> <w n="8.3">dévotement</w> <w n="8.4">que</w> <w n="8.5">ta</w> <w n="8.6">droite</w> <w n="8.7">protège</w></l>
						<l n="9" num="3.4"><w n="9.1">L</w>'<w n="9.2">assassinat</w>, <w n="9.3">le</w> <w n="9.4">vol</w> <w n="9.5">armé</w> <w n="9.6">des</w> <w n="9.7">nations</w> ;</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">Sa</w> <w n="10.2">prière</w> <w n="10.3">retombe</w> <w n="10.4">en</w> <w n="10.5">malédictions</w> !</l>
						<l n="11" num="4.2"><w n="11.1">C</w>'<w n="11.2">est</w> <w n="11.3">celle</w> <w n="11.4">d</w>'<w n="11.5">un</w> <w n="11.6">pays</w> <w n="11.7">dont</w> <w n="11.8">l</w>'<w n="11.9">orgueil</w> <w n="11.10">s</w>'<w n="11.11">humilie</w>,</l>
						<l n="12" num="4.3"><w n="12.1">Dont</w> <w n="12.2">les</w> <w n="12.3">égarements</w> <w n="12.4">n</w>'<w n="12.5">ont</w> <w n="12.6">pas</w> <w n="12.7">prescrit</w> <w n="12.8">les</w> <w n="12.9">droits</w>,</l>
						<l n="13" num="4.4"><w n="13.1">Qui</w> <w n="13.2">les</w> <w n="13.3">met</w> <w n="13.4">dans</w> <w n="13.5">ta</w> <w n="13.6">main</w>, <w n="13.7">seigneur</w>, <w n="13.8">et</w> <w n="13.9">te</w> <w n="13.10">supplie</w></l>
						<l n="14" num="4.5"><w n="14.1">De</w> <w n="14.2">suspendre</w> <w n="14.3">tes</w> <w n="14.4">coups</w> <w n="14.5">et</w> <w n="14.6">d</w>'<w n="14.7">alléger</w> <w n="14.8">sa</w> <w n="14.9">croix</w> !</l>
					</lg>
					<lg n="5">
						<l n="15" num="5.1"><w n="15.1">Dieu</w> <w n="15.2">tout</w> <w n="15.3">puissant</w>, <w n="15.4">choisis</w> <w n="15.5">des</w> <w n="15.6">peuples</w> <w n="15.7">ou</w> <w n="15.8">des</w> <w n="15.9">rois</w> !…</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1"><w n="16.1">Un</w> <w n="16.2">rayon</w> <w n="16.3">de</w> <w n="16.4">soleil</w> <w n="16.5">a</w>, <w n="16.6">sous</w> <w n="16.7">les</w> <w n="16.8">voûtes</w> <w n="16.9">sombres</w>,</l>
						<l n="17" num="6.2"><w n="17.1">D</w>'<w n="17.2">une</w> <w n="17.3">clarté</w> <w n="17.4">soudaine</w> <w n="17.5">illuminé</w> <w n="17.6">le</w> <w n="17.7">chœur</w> ;</l>
						<l n="18" num="6.3"><w n="18.1">Des</w> <w n="18.2">brumes</w> <w n="18.3">de</w> <w n="18.4">l</w>'<w n="18.5">église</w> <w n="18.6">il</w> <w n="18.7">dissipe</w> <w n="18.8">les</w> <w n="18.9">ombres</w> ;</l>
						<l n="19" num="6.4"><w n="19.1">De</w> <w n="19.2">la</w> <w n="19.3">foule</w> <w n="19.4">à</w> <w n="19.5">genoux</w> <w n="19.6">il</w> <w n="19.7">envahit</w> <w n="19.8">le</w> <w n="19.9">cœur</w> !…</l>
					</lg>
					<lg n="7">
						<l n="20" num="7.1"><w n="20.1">O</w> <w n="20.2">peuple</w> <w n="20.3">humilié</w>, <w n="20.4">relève</w>-<w n="20.5">toi</w> <w n="20.6">vainqueur</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">22 novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>