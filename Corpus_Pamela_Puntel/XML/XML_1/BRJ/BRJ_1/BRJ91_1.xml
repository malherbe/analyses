<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ91">
					<head type="number">XCI</head>
					<head type="main">SERMENT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">dernier</w> <w n="1.3">mot</w> ! — <w n="1.4">Ce</w> <w n="1.5">roi</w> <w n="1.6">Guillaume</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">nos</w> <w n="2.3">haines</w> <w n="2.4">et</w> <w n="2.5">nos</w> <w n="2.6">mépris</w></l>
						<l n="3" num="1.3"><w n="3.1">Croyaient</w> <w n="3.2">déjà</w> <w n="3.3">dans</w> <w n="3.4">son</w> <w n="3.5">royaume</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Commence</w> <w n="4.2">à</w> <w n="4.3">bombarder</w> <w n="4.4">Paris</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ses</w> <w n="5.2">obus</w>, <w n="5.3">qui</w> <w n="5.4">faisaient</w> <w n="5.5">sourire</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Ses</w> <w n="6.2">canons</w>, <w n="6.3">par</w> <w n="6.4">nous</w> <w n="6.5">bafoués</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Remplissent</w> <w n="7.2">la</w> <w n="7.3">cité</w> <w n="7.4">martyre</w></l>
						<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">femmes</w> <w n="8.3">et</w> <w n="8.4">d</w>'<w n="8.5">enfants</w> <w n="8.6">tués</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Bourbaki</w> <w n="9.2">fait</w> <w n="9.3">campagne</w> <w n="9.4">en</w> <w n="9.5">France</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">laisse</w> <w n="10.3">le</w> <w n="10.4">Rhin</w> <w n="10.5">indompté</w>. —</l>
						<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">importe</w> <w n="11.3">peu</w> !… <w n="11.4">L</w>'<w n="11.5">espérance</w></l>
						<l n="12" num="3.4"><w n="12.1">Deviendra</w> <w n="12.2">la</w> <w n="12.3">réalité</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Le</w> <w n="13.2">châtiment</w> <w n="13.3">aura</w> <w n="13.4">son</w> <w n="13.5">heure</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">nous</w> <w n="14.3">verrons</w> <w n="14.4">fuir</w> <w n="14.5">ce</w> <w n="14.6">maudit</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Qui</w> <w n="15.2">boit</w>, <w n="15.3">qui</w> <w n="15.4">bombarde</w> <w n="15.5">et</w> <w n="15.6">qui</w> <w n="15.7">pleure</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Plus</w> <w n="16.2">vite</w> <w n="16.3">encor</w> <w n="16.4">que</w> <w n="16.5">je</w> <w n="16.6">n</w>'<w n="16.7">ai</w> <w n="16.8">dit</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Si</w> <w n="17.2">le</w> <w n="17.3">destin</w>, <w n="17.4">par</w> <w n="17.5">impossible</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Lui</w> <w n="18.2">livrait</w> <w n="18.3">Paris</w> <w n="18.4">aujourd</w>'<w n="18.5">hui</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Demain</w>, <w n="19.2">ce</w> <w n="19.3">Cartouche</w> <w n="19.4">invincible</w></l>
						<l n="20" num="5.4"><w n="20.1">Aurait</w> <w n="20.2">la</w> <w n="20.3">France</w> <w n="20.4">devant</w> <w n="20.5">lui</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Si</w> <w n="21.2">la</w> <w n="21.3">France</w> <w n="21.4">même</w> <w n="21.5">succombe</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Qu</w>'<w n="22.2">importe</w> <w n="22.3">que</w>, <w n="22.4">frappés</w> <w n="22.5">au</w> <w n="22.6">front</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Tous</w> <w n="23.2">les</w> <w n="23.3">pères</w> <w n="23.4">soient</w> <w n="23.5">dans</w> <w n="23.6">la</w> <w n="23.7">tombe</w></l>
						<l n="24" num="6.4"><w n="24.1">Leurs</w> <w n="24.2">fils</w>, <w n="24.3">un</w> <w n="24.4">jour</w>, <w n="24.5">les</w> <w n="24.6">vengeront</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Cela</w> <w n="25.2">sera</w> ! — <w n="25.3">Maintenant</w> <w n="25.4">tue</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Pille</w>, <w n="26.2">bombarde</w>, <w n="26.3">prostitue</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Brûle</w>, <w n="27.2">dévaste</w>, <w n="27.3">règne</w> <w n="27.4">enfin</w> !</l>
						<l n="28" num="7.4"><w n="28.1">Enveloppé</w> <w n="28.2">de</w> <w n="28.3">funérailles</w>,</l>
						<l n="29" num="7.5"><w n="29.1">Donne</w> <w n="29.2">des</w> <w n="29.3">banquets</w> <w n="29.4">à</w> <w n="29.5">Versailles</w>,</l>
						<l n="30" num="7.6"><w n="30.1">Tandis</w> <w n="30.2">que</w> <w n="30.3">Paris</w> <w n="30.4">meurt</w> <w n="30.5">de</w> <w n="30.6">faim</w> !</l>
					</lg>
					<lg n="8">
						<l n="31" num="8.1"><w n="31.1">Que</w> <w n="31.2">par</w> <w n="31.3">les</w> <w n="31.4">soupiraux</w> <w n="31.5">des</w> <w n="31.6">caves</w></l>
						<l n="32" num="8.2"><w n="32.1">Tes</w> <w n="32.2">soldats</w> <w n="32.3">fusillent</w> <w n="32.4">nos</w> <w n="32.5">braves</w> ;</l>
						<l n="33" num="8.3"><w n="33.1">Qu</w>'<w n="33.2">ils</w> <w n="33.3">exterminent</w> <w n="33.4">nos</w> <w n="33.5">blessés</w> ;</l>
						<l n="34" num="8.4"><w n="34.1">Que</w> <w n="34.2">tes</w> <w n="34.3">Gretchen</w>, <w n="34.4">qu</w>'<w n="34.5">on</w> <w n="34.6">croit</w> <w n="34.7">niaises</w>,</l>
						<l n="35" num="8.5"><w n="35.1">A</w> <w n="35.2">déshonorer</w> <w n="35.3">nos</w> <w n="35.4">Françaises</w></l>
						<l n="36" num="8.6"><w n="36.1">Encouragent</w> <w n="36.2">leurs</w> <w n="36.3">fiancés</w> !</l>
					</lg>
					<lg n="9">
						<l n="37" num="9.1"><w n="37.1">Ce</w> <w n="37.2">que</w> <w n="37.3">j</w>'<w n="37.4">écris</w> <w n="37.5">est</w> <w n="37.6">de</w> <w n="37.7">l</w>'<w n="37.8">histoire</w>. —</l>
						<l n="38" num="9.2"><w n="38.1">Tu</w> <w n="38.2">mets</w> <w n="38.3">ainsi</w>, <w n="38.4">je</w> <w n="38.5">veux</w> <w n="38.6">t</w>'<w n="38.7">en</w> <w n="38.8">croire</w>,</l>
						<l n="39" num="9.3"><w n="39.1">Un</w> <w n="39.2">frein</w> <w n="39.3">aux</w> <w n="39.4">appétits</w> <w n="39.5">brutaux</w> !…</l>
						<l n="40" num="9.4"><w n="40.1">Surtout</w>, <w n="40.2">mon</w> <w n="40.3">prince</w>, <w n="40.4">je</w> <w n="40.5">t</w>'<w n="40.6">en</w> <w n="40.7">prie</w>,</l>
						<l n="41" num="9.5"><w n="41.1">Ordonne</w> <w n="41.2">à</w> <w n="41.3">ton</w> <w n="41.4">artillerie</w></l>
						<l n="42" num="9.6"><w n="42.1">De</w> <w n="42.2">tirer</w> <w n="42.3">sur</w> <w n="42.4">les</w> <w n="42.5">hôpitaux</w> !</l>
					</lg>
					<lg n="10">
						<l n="43" num="10.1"><w n="43.1">Le</w> <w n="43.2">monde</w> <w n="43.3">égaré</w> <w n="43.4">te</w> <w n="43.5">regarde</w>,</l>
						<l n="44" num="10.2"><w n="44.1">Sans</w> <w n="44.2">qu</w>'<w n="44.3">un</w> <w n="44.4">seul</w> <w n="44.5">peuple</w> <w n="44.6">se</w> <w n="44.7">hasarde</w></l>
						<l n="45" num="10.3"><w n="45.1">A</w> <w n="45.2">surgir</w> <w n="45.3">entre</w> <w n="45.4">nous</w> <w n="45.5">et</w> <w n="45.6">toi</w> ;</l>
						<l n="46" num="10.4"><w n="46.1">C</w>'<w n="46.2">est</w> <w n="46.3">bien</w> ! <w n="46.4">poursuis</w> <w n="46.5">ton</w> <w n="46.6">œuvre</w> <w n="46.7">a</w> <w n="46.8">l</w>'<w n="46.9">aise</w></l>
						<l n="47" num="10.5"><w n="47.1">Sais</w>-<w n="47.2">tu</w> <w n="47.3">bien</w> <w n="47.4">qu</w>'<w n="47.5">un</w> <w n="47.6">Quatre</w>-<w n="47.7">vingt</w>-<w n="47.8">treize</w></l>
						<l n="48" num="10.6"><w n="48.1">A</w> <w n="48.2">ce</w> <w n="48.3">labeur</w> <w n="48.4">vaut</w> <w n="48.5">moins</w> <w n="48.6">qu</w>'<w n="48.7">un</w> <w n="48.8">roi</w> ?</l>
					</lg>
					<lg n="11">
						<l n="49" num="11.1"><w n="49.1">Courage</w> <w n="49.2">donc</w> ! <w n="49.3">et</w> <w n="49.4">moralise</w>.</l>
						<l n="50" num="11.2"><w n="50.1">Prêche</w>, <w n="50.2">convertis</w>, <w n="50.3">civilise</w></l>
						<l n="51" num="11.3"><w n="51.1">Par</w> <w n="51.2">le</w> <w n="51.3">plomb</w>, <w n="51.4">le</w> <w n="51.5">fer</w> <w n="51.6">et</w> <w n="51.7">le</w> <w n="51.8">feu</w> ;</l>
						<l n="52" num="11.4"><w n="52.1">Puis</w>, <w n="52.2">selon</w> <w n="52.3">ta</w> <w n="52.4">sainte</w> <w n="52.5">habitude</w>,</l>
						<l n="53" num="11.5"><w n="53.1">Au</w> <w n="53.2">temple</w>, <w n="53.3">avec</w> <w n="53.4">la</w> <w n="53.5">multitude</w>,</l>
						<l n="54" num="11.6"><w n="54.1">Agenouille</w>-<w n="54.2">toi</w> <w n="54.3">devant</w> <w n="54.4">Dieu</w> !</l>
					</lg>
					<lg n="12">
						<l n="55" num="12.1"><w n="55.1">Ah</w> ! <w n="55.2">ce</w> <w n="55.3">Dieu</w> <w n="55.4">que</w> <w n="55.5">ta</w> <w n="55.6">lèvre</w> <w n="55.7">prie</w></l>
						<l n="56" num="12.2"><w n="56.1">De</w> <w n="56.2">bénir</w> <w n="56.3">cette</w> <w n="56.4">boucherie</w>,</l>
						<l n="57" num="12.3"><w n="57.1">Depuis</w> <w n="57.2">l</w>'<w n="57.3">heure</w> <w n="57.4">où</w> <w n="57.5">le</w> <w n="57.6">sang</w> <w n="57.7">coula</w>,</l>
						<l n="58" num="12.4"><w n="58.1">Ce</w> <w n="58.2">Dieu</w> <w n="58.3">que</w> <w n="58.4">tu</w> <w n="58.5">nommes</w> <w n="58.6">ton</w> <w n="58.7">maître</w></l>
						<l n="59" num="12.5"><w n="59.1">Tu</w> <w n="59.2">ne</w> <w n="59.3">crois</w> <w n="59.4">pas</w> <w n="59.5">en</w> <w n="59.6">lui</w> <w n="59.7">peut</w>-<w n="59.8">être</w> ?…</l>
						<l n="60" num="12.6"><w n="60.1">Prends</w> <w n="60.2">garde</w>, <w n="60.3">Guillaume</w>, <w n="60.4">il</w> <w n="60.5">est</w> <w n="60.6">là</w> !</l>
					</lg>
					<ab type="dot">─────────</ab>
					<lg n="13">
						<l n="61" num="13.1"><w n="61.1">J</w>'<w n="61.2">ai</w> <w n="61.3">tout</w> <w n="61.4">dit</w> : <w n="61.5">je</w> <w n="61.6">ferme</w> <w n="61.7">ce</w> <w n="61.8">livre</w>. —</l>
						<l n="62" num="13.2"><w n="62.1">Vers</w> <w n="62.2">enfantés</w> <w n="62.3">dans</w> <w n="62.4">la</w> <w n="62.5">douleur</w>,</l>
						<l n="63" num="13.3"><w n="63.1">Vous</w> <w n="63.2">dont</w> <w n="63.3">la</w> <w n="63.4">fièvre</w> <w n="63.5">m</w>'<w n="63.6">a</w> <w n="63.7">fait</w> <w n="63.8">vivre</w>,</l>
						<l n="64" num="13.4"><w n="64.1">Adieu</w>, <w n="64.2">compagnons</w> <w n="64.3">du</w> <w n="64.4">malheur</w> !</l>
						<l n="65" num="13.5"><w n="65.1">J</w>'<w n="65.2">ignore</w> <w n="65.3">où</w> <w n="65.4">le</w> <w n="65.5">destin</w> <w n="65.6">vous</w> <w n="65.7">mène</w>,</l>
						<l n="66" num="13.6"><w n="66.1">Mais</w> <w n="66.2">votre</w> <w n="66.3">cri</w> <w n="66.4">dans</w> <w n="66.5">l</w>'<w n="66.6">âme</w> <w n="66.7">humaine</w></l>
						<l n="67" num="13.7"><w n="67.1">Aura</w> <w n="67.2">son</w> <w n="67.3">retentissement</w>,</l>
						<l n="68" num="13.8"><w n="68.1">Et</w>, <w n="68.2">partout</w> <w n="68.3">où</w> <w n="68.4">la</w> <w n="68.5">haine</w> <w n="68.6">vibre</w>,</l>
						<l n="69" num="13.9"><w n="69.1">Les</w> <w n="69.2">voix</w>, <w n="69.3">les</w> <w n="69.4">cœurs</w> <w n="69.5">d</w>'<w n="69.6">un</w> <w n="69.7">peuple</w> <w n="69.8">libre</w></l>
						<l n="70" num="13.10"><w n="70.1">Répondront</w> <w n="70.2">à</w> <w n="70.3">votre</w> <w n="70.4">serment</w> !</l>
					</lg>
					<lg n="14">
						<l n="71" num="14.1"><w n="71.1">Oui</w>, <w n="71.2">tous</w>, <w n="71.3">à</w> <w n="71.4">l</w>'<w n="71.5">heure</w> <w n="71.6">solennelle</w></l>
						<l n="72" num="14.2"><w n="72.1">De</w> <w n="72.2">la</w> <w n="72.3">défaite</w> <w n="72.4">ou</w> <w n="72.5">du</w> <w n="72.6">succès</w>,</l>
						<l n="73" num="14.3"><w n="73.1">Jurons</w> <w n="73.2">une</w> <w n="73.3">haine</w> <w n="73.4">éternelle</w></l>
						<l n="74" num="14.4"><w n="74.1">Aux</w> <w n="74.2">bourreaux</w> <w n="74.3">du</w> <w n="74.4">peuple</w> <w n="74.5">français</w> !</l>
						<l n="75" num="14.5"><w n="75.1">Que</w> <w n="75.2">rien</w> <w n="75.3">n</w>'<w n="75.4">en</w> <w n="75.5">détruise</w> <w n="75.6">le</w> <w n="75.7">germe</w>,</l>
						<l n="76" num="14.6"><w n="76.1">Qu</w>'<w n="76.2">en</w> <w n="76.3">son</w> <w n="76.4">cœur</w> <w n="76.5">la</w> <w n="76.6">France</w> <w n="76.7">l</w>'<w n="76.8">enferme</w>,</l>
						<l n="77" num="14.7"><w n="77.1">Que</w> <w n="77.2">le</w> <w n="77.3">champ</w> <w n="77.4">la</w> <w n="77.5">conte</w> <w n="77.6">au</w> <w n="77.7">buisson</w> ;</l>
						<l n="78" num="14.8"><w n="78.1">Sur</w> <w n="78.2">la</w> <w n="78.3">terre</w> <w n="78.4">de</w> <w n="78.5">sang</w> <w n="78.6">fumée</w>,</l>
						<l n="79" num="14.9"><w n="79.1">Leurs</w> <w n="79.2">mains</w> <w n="79.3">barbares</w> <w n="79.4">l</w>'<w n="79.5">ont</w> <w n="79.6">semée</w>,</l>
						<l n="80" num="14.10"><w n="80.1">Qu</w>'<w n="80.2">ils</w> <w n="80.3">en</w> <w n="80.4">récoltent</w> <w n="80.5">la</w> <w n="80.6">moisson</w> !</l>
					</lg>
					<lg n="15">
						<l n="81" num="15.1"><w n="81.1">Que</w> <w n="81.2">pas</w> <w n="81.3">un</w> <w n="81.4">d</w>'<w n="81.5">eux</w>, <w n="81.6">après</w> <w n="81.7">la</w> <w n="81.8">guerre</w>,</l>
						<l n="82" num="15.2"><w n="82.1">Ne</w> <w n="82.2">retrouve</w> <w n="82.3">sur</w> <w n="82.4">son</w> <w n="82.5">chemin</w></l>
						<l n="83" num="15.3"><w n="83.1">Un</w> <w n="83.2">seuil</w> <w n="83.3">ami</w>, <w n="83.4">comme</w> <w n="83.5">naguère</w>,</l>
						<l n="84" num="15.4"><w n="84.1">Une</w> <w n="84.2">main</w> <w n="84.3">pour</w> <w n="84.4">serrer</w> <w n="84.5">sa</w> <w n="84.6">main</w> !</l>
						<l n="85" num="15.5"><w n="85.1">L</w>'<w n="85.2">hospitalité</w> <w n="85.3">déshonore</w></l>
						<l n="86" num="15.6"><w n="86.1">Celui</w> <w n="86.2">qui</w> <w n="86.3">les</w> <w n="86.4">pourrait</w> <w n="86.5">encore</w></l>
						<l n="87" num="15.7"><w n="87.1">Garder</w> <w n="87.2">en</w> <w n="87.3">ses</w> <w n="87.4">foyers</w> <w n="87.5">trahis</w> ;</l>
						<l n="88" num="15.8"><w n="88.1">Et</w> <w n="88.2">celle</w>-<w n="88.3">là</w> <w n="88.4">serait</w> <w n="88.5">infame</w></l>
						<l n="89" num="15.9"><w n="89.1">Qui</w> <w n="89.2">jamais</w> <w n="89.3">deviendrait</w> <w n="89.4">la</w> <w n="89.5">femme</w></l>
						<l n="90" num="15.10"><w n="90.1">D</w>'<w n="90.2">un</w> <w n="90.3">assassin</w> <w n="90.4">de</w> <w n="90.5">son</w> <w n="90.6">pays</w> !</l>
					</lg>
					<lg n="16">
						<l n="91" num="16.1"><w n="91.1">Plus</w> <w n="91.2">d</w>'<w n="91.3">espions</w> <w n="91.4">dans</w> <w n="91.5">nos</w> <w n="91.6">familles</w> !</l>
						<l n="92" num="16.2"><w n="92.1">Plus</w> <w n="92.2">de</w> <w n="92.3">voleurs</w> <w n="92.4">dans</w> <w n="92.5">nos</w> <w n="92.6">maisons</w> !</l>
						<l n="93" num="16.3"><w n="93.1">Serviteurs</w> <w n="93.2">mielleux</w>, <w n="93.3">blondes</w> <w n="93.4">filles</w>,</l>
						<l n="94" num="16.4"><w n="94.1">Portez</w> <w n="94.2">ailleurs</w> <w n="94.3">vos</w> <w n="94.4">trahisons</w> !</l>
						<l n="95" num="16.5"><w n="95.1">Que</w> <w n="95.2">d</w>'<w n="95.3">un</w> <w n="95.4">mâle</w> <w n="95.5">patriotisme</w></l>
						<l n="96" num="16.6"><w n="96.1">La</w> <w n="96.2">haine</w> <w n="96.3">soit</w> <w n="96.4">le</w> <w n="96.5">catéchisme</w> !</l>
						<l n="97" num="16.7"><w n="97.1">Que</w> <w n="97.2">la</w> <w n="97.3">grand</w>'<w n="97.4">mère</w>, <w n="97.5">à</w> <w n="97.6">son</w> <w n="97.7">rouet</w>,</l>
						<l n="98" num="16.8"><w n="98.1">De</w> <w n="98.2">haine</w> <w n="98.3">imprègne</w> <w n="98.4">sa</w> <w n="98.5">parole</w>,</l>
						<l n="99" num="16.9"><w n="99.1">Et</w> <w n="99.2">que</w> <w n="99.3">les</w> <w n="99.4">enfants</w> <w n="99.5">à</w> <w n="99.6">l</w>'<w n="99.7">école</w></l>
						<l n="100" num="16.10"><w n="100.1">L</w>'<w n="100.2">épèlent</w> <w n="100.3">dans</w> <w n="100.4">leur</w> <w n="100.5">alphabet</w> ! —</l>
					</lg>
					<lg n="17">
						<l n="101" num="17.1"><w n="101.1">Toi</w>, <w n="101.2">mon</w> <w n="101.3">fils</w>, <w n="101.4">dont</w> <w n="101.5">l</w>'<w n="101.6">âme</w> <w n="101.7">étonnée</w></l>
						<l n="102" num="17.2"><w n="102.1">Frémit</w> <w n="102.2">de</w> <w n="102.3">rage</w> <w n="102.4">et</w> <w n="102.5">non</w> <w n="102.6">d</w>'<w n="102.7">effroi</w>,</l>
						<l n="103" num="17.3"><w n="103.1">Mûris</w> <w n="103.2">ta</w> <w n="103.3">dix</w>-<w n="103.4">septième</w> <w n="103.5">année</w>,</l>
						<l n="104" num="17.4"><w n="104.1">Déjà</w> <w n="104.2">plus</w> <w n="104.3">grave</w>, <w n="104.4">et</w> <w n="104.5">souviens</w>-<w n="104.6">toi</w> !</l>
						<l n="105" num="17.5"><w n="105.1">Soit</w> <w n="105.2">que</w> <w n="105.3">je</w> <w n="105.4">vive</w> <w n="105.5">ou</w> <w n="105.6">que</w> <w n="105.7">je</w> <w n="105.8">meure</w>.</l>
						<l n="106" num="17.6"><w n="106.1">Garde</w> <w n="106.2">jusqu</w>'<w n="106.3">à</w> <w n="106.4">ta</w> <w n="106.5">dernière</w> <w n="106.6">heure</w></l>
						<l n="107" num="17.7"><w n="107.1">Cette</w> <w n="107.2">haine</w> <w n="107.3">du</w> <w n="107.4">nom</w> <w n="107.5">Prussien</w> !</l>
						<l n="108" num="17.8"><w n="108.1">Il</w> <w n="108.2">faudra</w> <w n="108.3">les</w> <w n="108.4">tuer</w> <w n="108.5">en</w> <w n="108.6">somme</w> !…</l>
						<l n="109" num="17.9"><w n="109.1">Sache</w> <w n="109.2">avant</w> <w n="109.3">tout</w> <w n="109.4">tenir</w> <w n="109.5">en</w> <w n="109.6">homme</w></l>
						<l n="110" num="17.10"><w n="110.1">Un</w> <w n="110.2">fusil</w> !… — <w n="110.3">Tu</w> <w n="110.4">le</w> <w n="110.5">sais</w> ?… <w n="110.6">c</w>'<w n="110.7">est</w> <w n="110.8">bien</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">14 janvier 1871.</date>
						</dateline>
					</closer></div></body></text></TEI>