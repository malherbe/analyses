<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ44">
					<head type="number">XLIV</head>
					<head type="main">LA CANNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Hélas</w> ! <w n="1.2">on</w> <w n="1.3">était</w> <w n="1.4">convaincu</w></l>
						<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">Moltke</w> <w n="2.3">était</w> <w n="2.4">mort</w> ; <w n="2.5">il</w> <w n="2.6">respire</w> !…</l>
						<l n="3" num="1.3"><w n="3.1">Or</w>, <w n="3.2">voici</w> <w n="3.3">ce</w> <w n="3.4">qu</w>'<w n="3.5">il</w> <w n="3.6">vient</w> <w n="3.7">de</w> <w n="3.8">dire</w> :</l>
						<l n="4" num="1.4">« <w n="4.1">Pas</w> <w n="4.2">un</w> <w n="4.3">fusil</w>, <w n="4.4">pas</w> <w n="4.5">un</w> <w n="4.6">écu</w>,</l>
						<l n="5" num="1.5">» <w n="5.1">Rien</w>, <w n="5.2">pas</w> <w n="5.3">même</w> <w n="5.4">une</w> <w n="5.5">sarbacane</w>,</l>
						<l n="6" num="1.6">» <w n="6.1">A</w> <w n="6.2">la</w> <w n="6.3">France</w> <w n="6.4">ne</w> <w n="6.5">restera</w> ;</l>
						<l n="7" num="1.7">» <w n="7.1">Nous</w> <w n="7.2">lui</w> <w n="7.3">laisserons</w> <w n="7.4">une</w> <w n="7.5">canne</w></l>
						<l n="8" num="1.8">» <w n="8.1">Dont</w> <w n="8.2">elle</w> <w n="8.3">se</w> <w n="8.4">pavanera</w> ! »</l>
						<l n="9" num="1.9">— <w n="9.1">Une</w> <w n="9.2">canne</w> ! <w n="9.3">quelle</w> <w n="9.4">imprudence</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Maréchal</w> ! <w n="10.2">vous</w> <w n="10.3">n</w>'<w n="10.4">y</w> <w n="10.5">songez</w> <w n="10.6">pas</w> !</l>
						<l n="11" num="1.11"><w n="11.1">Elle</w> <w n="11.2">peut</w> <w n="11.3">battre</w> <w n="11.4">la</w> <w n="11.5">cadence</w></l>
						<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">vous</w> <w n="12.3">faire</w> <w n="12.4">changer</w> <w n="12.5">le</w> <w n="12.6">pas</w> !</l>
						<l n="13" num="1.13"><w n="13.1">Pour</w> <w n="13.2">qui</w> <w n="13.3">veut</w> <w n="13.4">s</w>'<w n="13.5">en</w> <w n="13.6">donner</w> <w n="13.7">la</w> <w n="13.8">peine</w></l>
						<l n="14" num="1.14"><w n="14.1">Les</w> <w n="14.2">cannes</w> <w n="14.3">ont</w> <w n="14.4">plus</w> <w n="14.5">d</w>'<w n="14.6">un</w> <w n="14.7">emploi</w> ;</l>
						<l n="15" num="1.15"><w n="15.1">A</w> <w n="15.2">la</w> <w n="15.3">France</w> <w n="15.4">républicaine</w></l>
						<l n="16" num="1.16"><w n="16.1">Il</w> <w n="16.2">n</w>'<w n="16.3">en</w> <w n="16.4">faut</w> <w n="16.5">pas</w> <w n="16.6">plus</w>, <w n="16.7">croyez</w>-<w n="16.8">moi</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Pour</w> <w n="17.2">administrer</w> <w n="17.3">à</w> <w n="17.4">la</w> <w n="17.5">Prusse</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Sans</w> <w n="18.2">oublier</w> <w n="18.3">personne</w>, — <w n="18.4">fût</w>-<w n="18.5">ce</w></l>
						<l n="19" num="1.19"><w n="19.1">Guillaume</w>-<w n="19.2">le</w>-<w n="19.3">Victorieux</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Sa</w> <w n="20.2">cour</w> <w n="20.3">de</w> <w n="20.4">rois</w>, <w n="20.5">un</w> <w n="20.6">peu</w> <w n="20.7">mêlée</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Bismark</w>, <w n="21.2">et</w> <w n="21.3">vous</w>-<w n="21.4">même</w> <w n="21.5">avec</w> <w n="21.6">eux</w>, —</l>
						<l n="22" num="1.22"><w n="22.1">La</w> <w n="22.2">plus</w> <w n="22.3">effroyable</w> <w n="22.4">volée</w></l>
						<l n="23" num="1.23"><w n="23.1">Qui</w> <w n="23.2">jamais</w> <w n="23.3">ait</w> <w n="23.4">frotté</w> <w n="23.5">le</w> <w n="23.6">dos</w></l>
						<l n="24" num="1.24"><w n="24.1">De</w> <w n="24.2">la</w> <w n="24.3">valetaille</w> <w n="24.4">et</w> <w n="24.5">des</w> <w n="24.6">sots</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>