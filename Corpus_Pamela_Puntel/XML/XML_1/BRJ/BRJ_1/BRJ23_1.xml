<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ23">
					<head type="number">XXIII</head>
					<head type="main">L'ARRACHEUR DE DENTS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">charlatan</w>, <w n="1.3">coiffé</w> <w n="1.4">d</w>'<w n="1.5">un</w> <w n="1.6">casque</w> <w n="1.7">et</w> <w n="1.8">sabre</w> <w n="1.9">en</w> <w n="1.10">main</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Est</w> <w n="2.2">debout</w> <w n="2.3">sur</w> <w n="2.4">son</w> <w n="2.5">char</w>, <w n="2.6">au</w> <w n="2.7">milieu</w> <w n="2.8">du</w> <w n="2.9">chemin</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">arrache</w> <w n="3.3">les</w> <w n="3.4">dents</w> ; <w n="3.5">son</w> <w n="3.6">existence</w> <w n="3.7">errante</w></l>
						<l n="4" num="1.4"><w n="4.1">S</w>'<w n="4.2">emploie</w> <w n="4.3">à</w> <w n="4.4">soulager</w> <w n="4.5">l</w>'<w n="4.6">humanité</w> <w n="4.7">souffrante</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Son</w> <w n="5.2">zèle</w> <w n="5.3">pour</w> <w n="5.4">Bacchas</w> <w n="5.5">se</w> <w n="5.6">devine</w> <w n="5.7">a</w> <w n="5.8">son</w> <w n="5.9">né</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">Derrière</w> <w n="6.2">lui</w> <w n="6.3">se</w> <w n="6.4">carre</w> <w n="6.5">un</w> <w n="6.6">valet</w> <w n="6.7">galonné</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Prêt</w> <w n="7.2">à</w> <w n="7.3">jouer</w> <w n="7.4">de</w> <w n="7.5">l</w>'<w n="7.6">orgue</w>. <w n="7.7">En</w> <w n="7.8">habits</w> <w n="7.9">de</w> <w n="7.10">princesse</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Modestement</w> <w n="8.2">assise</w>, <w n="8.3">Augusta</w> <w n="8.4">tient</w> <w n="8.5">la</w> <w n="8.6">caisse</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">— « <w n="9.1">Militaires</w>, <w n="9.2">bourgeois</w> <w n="9.3">et</w> <w n="9.4">la</w> <w n="9.5">société</w> !</l>
						<l n="10" num="2.2">» <w n="10.1">J</w>'<w n="10.2">appelle</w> <w n="10.3">à</w> <w n="10.4">moi</w> <w n="10.5">les</w> <w n="10.6">cœurs</w> <w n="10.7">de</w> <w n="10.8">bonne</w> <w n="10.9">volonté</w> ;</l>
						<l n="11" num="2.3">» <w n="11.1">Quiconque</w> <w n="11.2">en</w> <w n="11.3">vos</w> <w n="11.4">esprits</w> <w n="11.5">tenterait</w> <w n="11.6">de</w> <w n="11.7">me</w> <w n="11.8">nuire</w></l>
						<l n="12" num="2.4">» <w n="12.1">N</w>'<w n="12.2">est</w> <w n="12.3">qu</w>'<w n="12.4">un</w> <w n="12.5">sot</w> <w n="12.6">maladroit</w> <w n="12.7">qui</w> <w n="12.8">cherche</w> <w n="12.9">à</w> <w n="12.10">vous</w> <w n="12.11">séduire</w> !</l>
						<l n="13" num="2.5">» <w n="13.1">C</w>'<w n="13.2">est</w> <w n="13.3">pour</w> <w n="13.4">votre</w> <w n="13.5">bonheur</w> <w n="13.6">que</w> <w n="13.7">je</w> <w n="13.8">viens</w> <w n="13.9">en</w> <w n="13.10">ce</w> <w n="13.11">lieu</w>,</l>
						<l n="14" num="2.6">» <w n="14.1">Et</w> <w n="14.2">mon</w> <w n="14.3">sabre</w> <w n="14.4">guérit</w>… <w n="14.5">par</w> <w n="14.6">la</w> <w n="14.7">grâce</w> <w n="14.8">de</w> <w n="14.9">Dieu</w> !</l>
						<l n="15" num="2.7">» <w n="15.1">Quand</w> <w n="15.2">mes</w> <w n="15.3">clients</w> <w n="15.4">encor</w> <w n="15.5">n</w>'<w n="15.6">en</w> <w n="15.7">ont</w> <w n="15.8">pas</w> <w n="15.9">l</w>'<w n="15.10">habitude</w>,</l>
						<l n="16" num="2.8">» <w n="16.1">Ce</w> <w n="16.2">sabre</w> <w n="16.3">leur</w> <w n="16.4">inspire</w> <w n="16.5">un</w> <w n="16.6">peu</w> <w n="16.7">d</w>'<w n="16.8">inquiétude</w>,</l>
						<l n="17" num="2.9">» <w n="17.1">Je</w> <w n="17.2">le</w> <w n="17.3">sais</w> !… <w n="17.4">mais</w> <w n="17.5">cela</w> <w n="17.6">sied</w>-<w n="17.7">il</w> <w n="17.8">aux</w> <w n="17.9">gens</w> <w n="17.10">d</w>'<w n="17.11">esprit</w> ?</l>
						<l n="18" num="2.10">» <w n="18.1">Mon</w> <w n="18.2">sabre</w> <w n="18.3">ne</w> <w n="18.4">fait</w> <w n="18.5">pas</w> <w n="18.6">de</w> <w n="18.7">mal</w>, <w n="18.8">puisqu</w>'<w n="18.9">il</w> <w n="18.10">guérit</w> !</l>
						<l n="19" num="2.11">» <w n="19.1">Victimes</w>, <w n="19.2">m</w>'<w n="19.3">a</w>-<w n="19.4">t</w>-<w n="19.5">on</w> <w n="19.6">dit</w>, <w n="19.7">d</w>'<w n="19.8">une</w> <w n="19.9">imposture</w> <w n="19.10">énorme</w>…</l>
						<l n="20" num="2.12">» <w n="20.1">Vous</w> <w n="20.2">avez</w>, <w n="20.3">avant</w> <w n="20.4">moi</w>, <w n="20.5">tâté</w> <w n="20.6">du</w> <w n="20.7">chloroforme</w> ;</l>
						<l n="21" num="2.13">» <w n="21.1">Un</w> <w n="21.2">charlatan</w> <w n="21.3">vulgaire</w> <w n="21.4">et</w> <w n="21.5">des</w> <w n="21.6">plus</w>— <w n="21.7">impudents</w></l>
						<l n="22" num="2.14">» <w n="22.1">A</w>, <w n="22.2">par</w> <w n="22.3">son</w> <w n="22.4">spécifique</w>, <w n="22.5">endommagé</w> <w n="22.6">vos</w> <w n="22.7">dents</w> !</l>
						<l n="23" num="2.15">» <w n="23.1">Je</w> <w n="23.2">viens</w> <w n="23.3">les</w> <w n="23.4">arracher</w> !… <w n="23.5">que</w> <w n="23.6">dis</w>-<w n="23.7">je</w> ?… <w n="23.8">les</w> <w n="23.9">extraire</w> ?</l>
						<l n="24" num="2.16">» <w n="24.1">Ne</w> <w n="24.2">jugez</w> <w n="24.3">pas</w> <w n="24.4">de</w> <w n="24.5">moi</w> <w n="24.6">par</w> <w n="24.7">ce</w> <w n="24.8">triste</w> <w n="24.9">confrère</w>.</l>
						<l n="25" num="2.17">» <w n="25.1">Le</w> <w n="25.2">chloroforme</w> <w n="25.3">peut</w> <w n="25.4">amener</w> <w n="25.5">un</w> <w n="25.6">malheur</w> ;</l>
						<l n="26" num="2.18">» <w n="26.1">J</w>'<w n="26.2">ai</w> <w n="26.3">mon</w> <w n="26.4">sabre</w>, <w n="26.5">il</w> <w n="26.6">suffit</w> ! <w n="26.7">J</w>'<w n="26.8">opère</w>… <w n="26.9">sans</w> <w n="26.10">douleur</w> !…</l>
						<l n="27" num="2.19">» <w n="27.1">Voyez</w>, <w n="27.2">car</w> <w n="27.3">je</w> <w n="27.4">m</w>'<w n="27.5">adresse</w> <w n="27.6">au</w> <w n="27.7">pauvre</w> <w n="27.8">comme</w> <w n="27.9">au</w> <w n="27.10">riche</w>,</l>
						<l n="28" num="2.20">» <w n="28.1">Les</w> <w n="28.2">dents</w> <w n="28.3">du</w> <w n="28.4">Danemark</w> <w n="28.5">et</w> <w n="28.6">celles</w> <w n="28.7">de</w> <w n="28.8">l</w>'<w n="28.9">Autriche</w> !</l>
						<l n="29" num="2.21">» <w n="29.1">Les</w> <w n="29.2">vôtres</w> <w n="29.3">sont</w> <w n="29.4">l</w>'<w n="29.5">Alsace</w> <w n="29.6">et</w> <w n="29.7">la</w> <w n="29.8">Lorraine</w> !… <w n="29.9">Eh</w> <w n="29.10">bien</w> !</l>
						<l n="30" num="2.22">» <w n="30.1">Pour</w> <w n="30.2">cinq</w> <w n="30.3">milliards</w>, <w n="30.4">Français</w> ! <w n="30.5">cinq</w> <w n="30.6">milliards</w> ! <w n="30.7">c</w>'<w n="30.8">est</w> <w n="30.9">pour</w> <w n="30.10">rien</w></l>
						<l n="31" num="2.23">» <w n="31.1">Je</w> <w n="31.2">vous</w> <w n="31.3">en</w> <w n="31.4">débarrasse</w>, <w n="31.5">et</w> <w n="31.6">sans</w> <w n="31.7">anesthésique</w> !…</l>
						<l n="32" num="2.24">» <w n="32.1">Cinq</w> <w n="32.2">milliards</w> !… <w n="32.3">je</w> <w n="32.4">lai</w> <w n="32.5">dit</w> ! <w n="32.6">cinq</w> !… <w n="32.7">allez</w>, <w n="32.8">la</w> <w n="32.9">musique</w> ! »</l>
					</lg>
					<lg n="3">
						<l n="33" num="3.1"><w n="33.1">Maître</w> <w n="33.2">Guillaume</w> <w n="33.3">parle</w>, <w n="33.4">et</w> <w n="33.5">Bismark</w> <w n="33.6">aussitôt</w></l>
						<l n="34" num="3.2"><w n="34.1">Tourne</w> <w n="34.2">sa</w> <w n="34.3">manivelle</w> <w n="34.4">avec</w> <w n="34.5">un</w> <w n="34.6">œil</w> <w n="34.7">dévôt</w>,</l>
						<l n="35" num="3.3"><w n="35.1">Et</w> <w n="35.2">couvre</w> <w n="35.3">les</w> <w n="35.4">clameurs</w> <w n="35.5">du</w> <w n="35.6">patient</w> <w n="35.7">qui</w> <w n="35.8">crie</w></l>
						<l n="36" num="3.4"><w n="36.1">D</w>'<w n="36.2">un</w> <w n="36.3">air</w> <w n="36.4">cher</w> <w n="36.5">aux</w> <w n="36.6">Français</w> : <w n="36.7">Partant</w> <w n="36.8">pour</w> <w n="36.9">la</w> <w n="36.10">Syrie</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Septembre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>