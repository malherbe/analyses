<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ36">
					<head type="number">XXXVI</head>
					<head type="main">ENFANTS ET VIEILLARDS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Des</w> <w n="1.2">enfants</w> <w n="1.3">de</w> <w n="1.4">quinze</w> <w n="1.5">ans</w> ! <w n="1.6">des</w> <w n="1.7">vieillards</w> <w n="1.8">de</w> <w n="1.9">soixante</w> !</l>
						<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1">Un</w> <w n="2.2">troupeau</w> <w n="2.3">voyageur</w></l>
						<l n="3" num="1.3"><w n="3.1">Marchant</w> <w n="3.2">à</w> <w n="3.3">l</w>'<w n="3.4">abattoir</w>, <w n="3.5">victime</w> <w n="3.6">obéissante</w> !…</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">Est</w>-<w n="4.2">ce</w>-<w n="4.3">vrai</w>, <w n="4.4">Dieu</w> <w n="4.5">vengeur</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Puis</w> <w n="5.2">les</w> <w n="5.3">femmes</w>, <w n="5.4">suivant</w> <w n="5.5">le</w> <w n="5.6">fils</w>, <w n="5.7">l</w>'<w n="5.8">époux</w>, <w n="5.9">le</w> <w n="5.10">père</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="12"></space><w n="6.1">Sur</w> <w n="6.2">un</w> <w n="6.3">sol</w> <w n="6.4">étranger</w> !</l>
						<l n="7" num="2.3">— <w n="7.1">Hélas</w> ! <w n="7.2">l</w>'<w n="7.3">homme</w> <w n="7.4">parti</w>, <w n="7.5">la</w> <w n="7.6">femme</w> <w n="7.7">désespère</w> !</l>
						<l n="8" num="2.4"><space unit="char" quantity="12"></space><w n="8.1">L</w>'<w n="8.2">enfant</w> <w n="8.3">voudrait</w> <w n="8.4">manger</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Voilà</w>, <w n="9.2">voilà</w> <w n="9.3">par</w> <w n="9.4">qui</w> <w n="9.5">vos</w> <w n="9.6">hordes</w> <w n="9.7">sont</w> <w n="9.8">accrues</w>.</l>
						<l n="10" num="3.2"><space unit="char" quantity="12"></space><w n="10.1">O</w> <w n="10.2">bourreaux</w> <w n="10.3">sans</w> <w n="10.4">remords</w> !</l>
						<l n="11" num="3.3"><w n="11.1">Princes</w> <w n="11.2">et</w> <w n="11.3">rois</w>, <w n="11.4">voilà</w> <w n="11.5">les</w> <w n="11.6">nouvelles</w> <w n="11.7">recrues</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="12"></space><w n="12.1">Qui</w> <w n="12.2">remplacent</w> <w n="12.3">vos</w> <w n="12.4">morts</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ah</w> ! <w n="13.2">je</w> <w n="13.3">n</w>'<w n="13.4">y</w> <w n="13.5">puis</w> <w n="13.6">durer</w> ! <w n="13.7">l</w>'<w n="13.8">horreur</w> <w n="13.9">est</w> <w n="13.10">dans</w> <w n="13.11">mon</w> <w n="13.12">âme</w> ;</l>
						<l n="14" num="4.2"><space unit="char" quantity="12"></space><w n="14.1">Ma</w> <w n="14.2">force</w> <w n="14.3">me</w> <w n="14.4">trahit</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">malgré</w> <w n="15.3">leur</w> <w n="15.4">fureur</w>, <w n="15.5">leur</w> <w n="15.6">mitraille</w> <w n="15.7">et</w> <w n="15.8">leur</w> <w n="15.9">flamme</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="12"></space><w n="16.1">La</w> <w n="16.2">pitié</w> <w n="16.3">m</w>'<w n="16.4">envahit</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Mais</w>, <w n="17.2">lorsque</w> <w n="17.3">nos</w> <w n="17.4">Français</w> <w n="17.5">vous</w> <w n="17.6">verront</w> <w n="17.7">sous</w> <w n="17.8">vos</w> <w n="17.9">armes</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="12"></space><w n="18.1">Si</w> <w n="18.2">jeunes</w> <w n="18.3">ou</w> <w n="18.4">si</w> <w n="18.5">vieux</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Les</w> <w n="19.2">fusils</w> <w n="19.3">tomberont</w> <w n="19.4">de</w> <w n="19.5">leurs</w> <w n="19.6">mains</w>, <w n="19.7">et</w> <w n="19.8">les</w> <w n="19.9">larmes</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="12"></space><w n="20.1">Jailliront</w> <w n="20.2">de</w> <w n="20.3">leurs</w> <w n="20.4">yeux</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Que</w> <w n="21.2">voulez</w>-<w n="21.3">vous</w> ? <w n="21.4">ils</w> <w n="21.5">ont</w> <w n="21.6">ces</w> <w n="21.7">scrupules</w> <w n="21.8">infâmes</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="12"></space><w n="22.1">Vaincus</w> <w n="22.2">ou</w> <w n="22.3">triomphants</w>,</l>
						<l n="23" num="6.3"><w n="23.1">D</w>'<w n="23.2">épargner</w> <w n="23.3">les</w> <w n="23.4">vieillards</w>, <w n="23.5">de</w> <w n="23.6">protéger</w> <w n="23.7">les</w> <w n="23.8">femmes</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="12"></space><w n="24.1">De</w> <w n="24.2">sauver</w> <w n="24.3">les</w> <w n="24.4">enfants</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Ainsi</w>, <w n="25.2">sans</w> <w n="25.3">autre</w> <w n="25.4">but</w> <w n="25.5">et</w> <w n="25.6">sans</w> <w n="25.7">autre</w> <w n="25.8">espérance</w>,</l>
						<l n="26" num="7.2"><space unit="char" quantity="12"></space><w n="26.1">Vos</w> <w n="26.2">maîtres</w> <w n="26.3">obéis</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Pour</w> <w n="27.2">l</w>'<w n="27.3">unique</w> <w n="27.4">plaisir</w> <w n="27.5">d</w>'<w n="27.6">assassiner</w> <w n="27.7">la</w> <w n="27.8">France</w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="12"></space><w n="28.1">Dépeuplent</w> <w n="28.2">leur</w> <w n="28.3">pays</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Ils</w> <w n="29.2">veulent</w> <w n="29.3">que</w> <w n="29.4">la</w> <w n="29.5">proie</w>, <w n="29.6">à</w> <w n="29.7">leurs</w> <w n="29.8">pieds</w> <w n="29.9">abattue</w>,</l>
						<l n="30" num="8.2"><space unit="char" quantity="12"></space><w n="30.1">Expire</w> <w n="30.2">en</w> <w n="30.3">frissonnant</w> !…</l>
						<l n="31" num="8.3"><w n="31.1">Ah</w> ! <w n="31.2">vous</w> <w n="31.3">le</w> <w n="31.4">voyez</w> <w n="31.5">bien</w> <w n="31.6">qu</w>'<w n="31.7">il</w> <w n="31.8">faudra</w> <w n="31.9">qu</w>'<w n="31.10">on</w> <w n="31.11">vous</w> <w n="31.12">tue</w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="12"></space><w n="32.1">Même</w> <w n="32.2">en</w> <w n="32.3">se</w> <w n="32.4">détournant</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Il</w> <w n="33.2">le</w> <w n="33.3">faudra</w> !… — <w n="33.4">Mais</w> <w n="33.5">vous</w> <w n="33.6">qui</w> <w n="33.7">faites</w> <w n="33.8">cette</w> <w n="33.9">guerre</w>,</l>
						<l n="34" num="9.2"><space unit="char" quantity="12"></space><w n="34.1">Pour</w> <w n="34.2">en</w> <w n="34.3">payer</w> <w n="34.4">le</w> <w n="34.5">prix</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Rites</w>, <w n="35.2">est</w>-<w n="35.3">il</w> <w n="35.4">assez</w> <w n="35.5">de</w> <w n="35.6">haine</w> <w n="35.7">sur</w> <w n="35.8">la</w> <w n="35.9">terre</w>,</l>
						<l n="36" num="9.4"><space unit="char" quantity="12"></space><w n="36.1">De</w> <w n="36.2">haine</w> <w n="36.3">et</w> <w n="36.4">de</w> <w n="36.5">mépris</w> ?</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">O</w> <w n="37.2">Guillaume</w>, <w n="37.3">ô</w> <w n="37.4">Bismark</w>, <w n="37.5">pour</w> <w n="37.6">conter</w> <w n="37.7">votre</w> <w n="37.8">gloire</w></l>
						<l n="38" num="10.2"><space unit="char" quantity="12"></space><w n="38.1">Aux</w> <w n="38.2">générations</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Est</w>-<w n="39.2">il</w> <w n="39.3">assez</w> <w n="39.4">d</w>'<w n="39.5">opprobre</w> <w n="39.6">at</w>» <w n="39.7">livre</w> <w n="39.8">de</w> <w n="39.9">l</w>'<w n="39.10">histoire</w>,</l>
						<l n="40" num="10.4"><space unit="char" quantity="12"></space><w n="40.1">Et</w> <w n="40.2">d</w>'<w n="40.3">imprécations</w> ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">De</w> <w n="41.2">palais</w> <w n="41.3">consumés</w> <w n="41.4">en</w> <w n="41.5">vos</w> <w n="41.6">propres</w> <w n="41.7">royaumes</w></l>
						<l n="42" num="11.2"><space unit="char" quantity="12"></space><w n="42.1">Pour</w> <w n="42.2">nos</w> <w n="42.3">hameaux</w> <w n="42.4">détruits</w> ;</l>
						<l n="43" num="11.3"><w n="43.1">De</w> <w n="43.2">remords</w> <w n="43.3">pour</w> <w n="43.4">vos</w> <w n="43.5">jours</w>, <w n="43.6">et</w> <w n="43.7">de</w> <w n="43.8">pâles</w> <w n="43.9">fantômes</w></l>
						<l n="44" num="11.4"><space unit="char" quantity="12"></space><w n="44.1">Pour</w> <w n="44.2">l</w>'<w n="44.3">ombre</w> <w n="44.4">de</w> <w n="44.5">vos</w> <w n="44.6">nuits</w> ?</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Assez</w> <w n="45.2">de</w> <w n="45.3">Némésis</w>, <w n="45.4">assez</w> <w n="45.5">de</w> <w n="45.6">gémonies</w></l>
						<l n="46" num="12.2"><space unit="char" quantity="12"></space><w n="46.1">Pour</w> <w n="46.2">votre</w> <w n="46.3">cruauté</w> ?</l>
						<l n="47" num="12.3"><w n="47.1">Et</w>, <w n="47.2">pour</w> <w n="47.3">votre</w> <w n="47.4">âme</w>, <w n="47.5">au</w> <w n="47.6">sein</w> <w n="47.7">des</w> <w n="47.8">douleurs</w> <w n="47.9">infinies</w>,</l>
						<l n="48" num="12.4"><space unit="char" quantity="12"></space><w n="48.1">Assez</w> <w n="48.2">d</w>'<w n="48.3">éternité</w> ?…</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>