<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ2">
					<head type="number">I</head>
					<head type="main">PROLOGUE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">socle</w> <w n="1.3">doit</w> <w n="1.4">porter</w> <w n="1.5">le</w> <w n="1.6">nom</w> <w n="1.7">de</w> <w n="1.8">la</w> <w n="1.9">statue</w> :</l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">ne</w> <w n="2.3">sais</w> <w n="2.4">pas</w> <w n="2.5">tuer</w> ; <w n="2.6">je</w> <w n="2.7">suis</w> <w n="2.8">de</w> <w n="2.9">ceux</w> <w n="2.10">qu</w>'<w n="2.11">on</w> <w n="2.12">tue</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Oui</w>, <w n="3.2">c</w>'<w n="3.3">est</w> <w n="3.4">mon</w> <w n="3.5">premier</w> <w n="3.6">mot</w>, <w n="3.7">le</w> <w n="3.8">sang</w> <w n="3.9">me</w> <w n="3.10">fait</w> <w n="3.11">horreur</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Étrange</w> <w n="4.2">aveu</w> <w n="4.3">chez</w> <w n="4.4">qui</w> <w n="4.5">s</w>'<w n="4.6">annonce</w> <w n="4.7">franc</w>-<w n="4.8">tireur</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Pourtant</w>, <w n="5.2">c</w>'<w n="5.3">est</w> <w n="5.4">au</w> <w n="5.5">combat</w> <w n="5.6">que</w> <w n="5.7">ma</w> <w n="5.8">muse</w> <w n="5.9">vous</w> <w n="5.10">mène</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">France</w> <w n="6.3">doit</w> <w n="6.4">passer</w> <w n="6.5">avant</w> <w n="6.6">la</w> <w n="6.7">vie</w> <w n="6.8">humaine</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Qu</w>'<w n="7.2">est</w>-<w n="7.3">ce</w> <w n="7.4">donc</w> ? <w n="7.5">D</w>'<w n="7.6">où</w> <w n="7.7">nous</w> <w n="7.8">vient</w> <w n="7.9">cette</w> <w n="7.10">épreuve</w> ? <w n="7.11">pourquoi</w> ?</l>
						<l n="8" num="2.2"><w n="8.1">Quelle</w> <w n="8.2">fatalité</w> <w n="8.3">nous</w> <w n="8.4">impose</w> <w n="8.5">sa</w> <w n="8.6">loi</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Quoi</w> ! <w n="9.2">voilant</w> <w n="9.3">leurs</w> <w n="9.4">desseins</w> <w n="9.5">d</w>'<w n="9.6">un</w> <w n="9.7">prétexte</w> <w n="9.8">vulgaire</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">plaît</w> <w n="10.3">à</w> <w n="10.4">deux</w> <w n="10.5">coquins</w> <w n="10.6">de</w> <w n="10.7">se</w> <w n="10.8">faire</w> <w n="10.9">la</w> <w n="10.10">guerre</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">L</w>'<w n="11.2">un</w> <w n="11.3">coquin</w> <w n="11.4">de</w> <w n="11.5">hasard</w>, <w n="11.6">l</w>'<w n="11.7">autre</w> <w n="11.8">de</w> <w n="11.9">droit</w> <w n="11.10">divin</w>,</l>
						<l n="12" num="3.4"><w n="12.1">L</w>'<w n="12.2">un</w> <w n="12.3">tristement</w> <w n="12.4">à</w> <w n="12.5">jeun</w>, <w n="12.6">et</w> <w n="12.7">l</w>'<w n="12.8">autre</w> <w n="12.9">pris</w> <w n="12.10">de</w> <w n="12.11">vin</w> ;</l>
						<l n="13" num="3.5"><w n="13.1">Et</w>, <w n="13.2">jouets</w> <w n="13.3">de</w> <w n="13.4">ces</w> <w n="13.5">rois</w> <w n="13.6">bandits</w> <w n="13.7">qui</w> <w n="13.8">les</w> <w n="13.9">dominent</w>,</l>
						<l n="14" num="3.6"><w n="14.1">Deux</w> <w n="14.2">peuples</w> <w n="14.3">aussitôt</w> <w n="14.4">s</w>'<w n="14.5">égorgent</w>, <w n="14.6">s</w>'<w n="14.7">exterminent</w> !</l>
						<l n="15" num="3.7"><w n="15.1">L</w>'<w n="15.2">assassinat</w>, <w n="15.3">le</w> <w n="15.4">vol</w> <w n="15.5">prélèvent</w> <w n="15.6">leur</w> <w n="15.7">tribut</w> ;</l>
						<l n="16" num="3.8"><w n="16.1">Tuer</w> <w n="16.2">est</w> <w n="16.3">le</w> <w n="16.4">moyen</w>, <w n="16.5">et</w> <w n="16.6">voler</w> <w n="16.7">est</w> <w n="16.8">le</w> <w n="16.9">but</w> !</l>
						<l n="17" num="3.9"><w n="17.1">Toutes</w> <w n="17.2">les</w> <w n="17.3">notions</w> <w n="17.4">de</w> <w n="17.5">morale</w> <w n="17.6">s</w>'<w n="17.7">effacent</w> !…</l>
						<l n="18" num="3.10"><w n="18.1">O</w> <w n="18.2">peuples</w>, <w n="18.3">il</w> <w n="18.4">fallait</w> <w n="18.5">que</w> <w n="18.6">vos</w> <w n="18.7">mains</w> <w n="18.8">étouffassent</w></l>
						<l n="19" num="3.11"><w n="19.1">Ces</w> <w n="19.2">despotes</w> <w n="19.3">sans</w> <w n="19.4">cœur</w>, <w n="19.5">dont</w> <w n="19.6">l</w>'<w n="19.7">orgueil</w> <w n="19.8">insensé</w></l>
						<l n="20" num="3.12"><w n="20.1">Réveillait</w> <w n="20.2">les</w> <w n="20.3">échos</w> <w n="20.4">barbares</w> <w n="20.5">du</w> <w n="20.6">passé</w> !…</l>
						<l n="21" num="3.13"><w n="21.1">La</w> <w n="21.2">guerre</w>, <w n="21.3">un</w> <w n="21.4">droit</w> ? <w n="21.5">La</w> <w n="21.6">guerre</w>, <w n="21.7">à</w> <w n="21.8">l</w>'<w n="21.9">honneur</w> <w n="21.10">intrépide</w></l>
						<l n="22" num="3.14"><w n="22.1">Un</w> <w n="22.2">glorieux</w> <w n="22.3">appel</w> ?… <w n="22.4">Non</w> ! <w n="22.5">la</w> <w n="22.6">guerre</w> <w n="22.7">stupide</w>,</l>
						<l n="23" num="3.15"><w n="23.1">Immolant</w> <w n="23.2">la</w> <w n="23.3">raison</w> <w n="23.4">sous</w> <w n="23.5">le</w> <w n="23.6">glaive</w> <w n="23.7">ou</w> <w n="23.8">l</w>'<w n="23.9">épieu</w>,</l>
						<l n="24" num="3.16"><w n="24.1">Et</w>, <w n="24.2">comme</w> <w n="24.3">le</w> <w n="24.4">duel</w>, <w n="24.5">outrageant</w> <w n="24.6">l</w>'<w n="24.7">homme</w> <w n="24.8">et</w> <w n="24.9">Dieu</w> !</l>
						<l n="25" num="3.17"><w n="25.1">Un</w> <w n="25.2">jour</w> <w n="25.3">les</w> <w n="25.4">nations</w> <w n="25.5">en</w> <w n="25.6">auront</w> <w n="25.7">conscience</w>,</l>
						<l n="26" num="3.18"><w n="26.1">Et</w> <w n="26.2">l</w>'<w n="26.3">on</w> <w n="26.4">s</w>'<w n="26.5">étonnera</w> <w n="26.6">de</w> <w n="26.7">cette</w> <w n="26.8">patience</w></l>
						<l n="27" num="3.19"><w n="27.1">A</w> <w n="27.2">souffrir</w> <w n="27.3">qu</w>'<w n="27.4">un</w> <w n="27.5">tyran</w> <w n="27.6">donnât</w>, <w n="27.7">le</w> <w n="27.8">sceptre</w> <w n="27.9">en</w> <w n="27.10">main</w>,</l>
						<l n="28" num="3.20"><w n="28.1">Le</w> <w n="28.2">signal</w> <w n="28.3">du</w> <w n="28.4">carnage</w> <w n="28.5">à</w> <w n="28.6">tout</w> <w n="28.7">le</w> <w n="28.8">genre</w> <w n="28.9">humain</w> !</l>
					</lg>
					<lg n="4">
						<l n="29" num="4.1"><w n="29.1">Que</w> <w n="29.2">d</w>'<w n="29.3">autres</w>, <w n="29.4">cependant</w>, <w n="29.5">conçoivent</w> <w n="29.6">l</w>'<w n="29.7">espérance</w></l>
						<l n="30" num="4.2"><w n="30.1">De</w> <w n="30.2">voir</w> <w n="30.3">s</w>'<w n="30.4">entendre</w> <w n="30.5">alors</w> <w n="30.6">la</w> <w n="30.7">Prusse</w> <w n="30.8">avec</w> <w n="30.9">la</w> <w n="30.10">France</w> ;</l>
						<l n="31" num="4.3"><w n="31.1">Ce</w> <w n="31.2">lieu</w>-<w n="31.3">commun</w> <w n="31.4">devient</w> <w n="31.5">risible</w> <w n="31.6">désormais</w>,</l>
						<l n="32" num="4.4"><w n="32.1">Et</w> <w n="32.2">Français</w> <w n="32.3">et</w> <w n="32.4">Prussiens</w> <w n="32.5">ne</w> <w n="32.6">s</w>'<w n="32.7">entendront</w> <w n="32.8">jamais</w> !</l>
						<l n="33" num="4.5"><w n="33.1">Ils</w> <w n="33.2">ne</w> <w n="33.3">se</w> <w n="33.4">tûront</w> <w n="33.5">plus</w>, <w n="33.6">leur</w> <w n="33.7">fureur</w> <w n="33.8">assouvie</w> ;</l>
						<l n="34" num="4.6"><w n="34.1">Mais</w> <w n="34.2">rien</w> <w n="34.3">ne</w> <w n="34.4">comblera</w> <w n="34.5">cet</w> <w n="34.6">abîme</w>, <w n="34.7">l</w>'<w n="34.8">envie</w> !…</l>
						<l n="35" num="4.7"><w n="35.1">A</w> <w n="35.2">ces</w> <w n="35.3">mots</w>, <w n="35.4">un</w> <w n="35.5">docteur</w> <w n="35.6">m</w>'<w n="35.7">interpelle</w> <w n="35.8">et</w> <w n="35.9">sourit</w> :</l>
						<l n="36" num="4.8">« <w n="36.1">Vous</w> <w n="36.2">envier</w> ? <w n="36.3">quoi</w> <w n="36.4">donc</w> ? » — <w n="36.5">Peu</w> <w n="36.6">de</w> <w n="36.7">chose</w>, <w n="36.8">l</w>'<w n="36.9">esprit</w>.</l>
						<l n="37" num="4.9"><w n="37.1">Vainement</w> <w n="37.2">convoité</w> <w n="37.3">de</w> <w n="37.4">la</w> <w n="37.5">race</w> <w n="37.6">germaine</w>,</l>
						<l n="38" num="4.10"><w n="38.1">Ce</w> <w n="38.2">fruit</w> <w n="38.3">du</w> <w n="38.4">sol</w> <w n="38.5">français</w> <w n="38.6">n</w>'<w n="38.7">est</w> <w n="38.8">pas</w> <w n="38.9">de</w> <w n="38.10">son</w> <w n="38.11">domaine</w> ;</l>
						<l n="39" num="4.11"><w n="39.1">Le</w> <w n="39.2">Prussien</w> <w n="39.3">anguleux</w> <w n="39.4">grimace</w> <w n="39.5">en</w> <w n="39.6">le</w> <w n="39.7">cherchant</w> ;</l>
						<l n="40" num="4.12"><w n="40.1">Il</w> <w n="40.2">est</w> <w n="40.3">bête</w> !… <w n="40.4">plus</w> <w n="40.5">bête</w> <w n="40.6">encore</w> <w n="40.7">que</w> <w n="40.8">méchant</w>.</l>
						<l n="41" num="4.13"><w n="41.1">Voilà</w> <w n="41.2">ce</w> <w n="41.3">que</w> <w n="41.4">ne</w> <w n="41.5">peut</w> <w n="41.6">nous</w> <w n="41.7">pardonner</w> <w n="41.8">sa</w> <w n="41.9">haine</w> !</l>
						<l n="42" num="4.14"><w n="42.1">Que</w> <w n="42.2">l</w>'<w n="42.3">un</w> <w n="42.4">d</w>'<w n="42.5">eux</w>, <w n="42.6">par</w> <w n="42.7">hasard</w> (<w n="42.8">je</w> <w n="42.9">choisis</w> <w n="42.10">Henri</w> <w n="42.11">Heine</w>).</l>
						<l n="43" num="4.15"><w n="43.1">De</w> <w n="43.2">ce</w> <w n="43.3">nouveau</w> <w n="43.4">Colchos</w> <w n="43.5">ait</w> <w n="43.6">su</w> <w n="43.7">trouver</w> <w n="43.8">l</w>'<w n="43.9">accès</w>,</l>
						<l n="44" num="4.16"><w n="44.1">De</w> <w n="44.2">Prussien</w> <w n="44.3">aussitôt</w> <w n="44.4">il</w> <w n="44.5">deviendra</w> <w n="44.6">Français</w>.</l>
						<l n="45" num="4.17"><w n="45.1">Les</w> <w n="45.2">autres</w>, <w n="45.3">j</w>'<w n="45.4">en</w> <w n="45.5">conviens</w>, <w n="45.6">seront</w> <w n="45.7">instruits</w>, <w n="45.8">honnêtes</w>,</l>
						<l n="46" num="4.18"><w n="46.1">Philosophes</w>, <w n="46.2">savants</w>, <w n="46.3">musiciens</w>… <w n="46.4">mais</w> <w n="46.5">bêtes</w>,</l>
						<l n="47" num="4.19"><w n="47.1">Bêtes</w> <w n="47.2">jusqu</w>'<w n="47.3">à</w> <w n="47.4">la</w> <w n="47.5">mort</w>… <w n="47.6">en</w> <w n="47.7">un</w> <w n="47.8">mot</w> <w n="47.9">comme</w> <w n="47.10">en</w> <w n="47.11">cent</w>,</l>
						<l n="48" num="4.20"><w n="48.1">Subalternes</w> !… <w n="48.2">cela</w> <w n="48.3">vaut</w>-<w n="48.4">il</w> <w n="48.5">des</w> <w n="48.6">flots</w> <w n="48.7">de</w> <w n="48.8">sang</w> ?</l>
					</lg>
					<lg n="5">
						<l n="49" num="5.1"><w n="49.1">Et</w>, <w n="49.2">par</w> <w n="49.3">une</w> <w n="49.4">fortune</w> <w n="49.5">incroyable</w>, <w n="49.6">inouïe</w>,</l>
						<l n="50" num="5.2"><w n="50.1">De</w> <w n="50.2">ses</w> <w n="50.3">propres</w> <w n="50.4">succès</w> <w n="50.5">elle</w>-<w n="50.6">même</w> <w n="50.7">éblouie</w>.</l>
						<l n="51" num="5.3"><w n="51.1">Cette</w> <w n="51.2">race</w> <w n="51.3">sur</w> <w n="51.4">toi</w> <w n="51.5">pose</w> <w n="51.6">un</w> <w n="51.7">genou</w> <w n="51.8">vainqueur</w>,</l>
						<l n="52" num="5.4"><w n="52.1">O</w> <w n="52.2">France</w>, <w n="52.3">et</w> <w n="52.4">te</w> <w n="52.5">retourne</w> <w n="52.6">un</w> <w n="52.7">couteau</w> <w n="52.8">dans</w> <w n="52.9">le</w> <w n="52.10">cœur</w> !…</l>
					</lg>
					<lg n="6">
						<l n="53" num="6.1"><w n="53.1">Ah</w> ! <w n="53.2">devant</w> <w n="53.3">cette</w> <w n="53.4">honte</w>, <w n="53.5">une</w> <w n="53.6">douleur</w> <w n="53.7">immense</w></l>
						<l n="54" num="6.2"><w n="54.1">Étouffe</w> <w n="54.2">la</w> <w n="54.3">pitié</w>, <w n="54.4">la</w> <w n="54.5">raison</w>, <w n="54.6">la</w> <w n="54.7">clémence</w> ;</l>
						<l n="55" num="6.3"><w n="55.1">Tout</w> <w n="55.2">disparaît</w> <w n="55.3">devant</w> <w n="55.4">la</w> <w n="55.5">patrie</w> <w n="55.6">en</w> <w n="55.7">danger</w>,</l>
						<l n="56" num="6.4"><w n="56.1">Sinon</w> <w n="56.2">qu</w>'<w n="56.3">on</w> <w n="56.4">l</w>'<w n="56.5">assassine</w> <w n="56.6">et</w> <w n="56.7">qu</w>'<w n="56.8">il</w> <w n="56.9">faut</w> <w n="56.10">la</w> <w n="56.11">venger</w> !</l>
					</lg>
					<lg n="7">
						<l n="57" num="7.1"><w n="57.1">Loin</w> <w n="57.2">de</w> <w n="57.3">moi</w>, <w n="57.4">vains</w> <w n="57.5">travaux</w> <w n="57.6">où</w> <w n="57.7">s</w>'<w n="57.8">énervait</w> <w n="57.9">mon</w> <w n="57.10">âme</w> !</l>
						<l n="58" num="7.2"><w n="58.1">O</w> <w n="58.2">mes</w> <w n="58.3">vers</w>, <w n="58.4">jaillissez</w> <w n="58.5">en</w> <w n="58.6">paroles</w> <w n="58.7">de</w> <w n="58.8">flamme</w> !</l>
						<l n="59" num="7.3"><w n="59.1">Courez</w>, <w n="59.2">volez</w> ! <w n="59.3">soyez</w> <w n="59.4">le</w> <w n="59.5">clairon</w>, <w n="59.6">le</w> <w n="59.7">tocsin</w>,</l>
						<l n="60" num="7.4"><w n="60.1">La</w> <w n="60.2">chanson</w> <w n="60.3">du</w> <w n="60.4">soldat</w>, <w n="60.5">le</w> <w n="60.6">glas</w> <w n="60.7">de</w> <w n="60.8">l</w>'<w n="60.9">assassin</w>,</l>
						<l n="61" num="7.5"><w n="61.1">L</w>'<w n="61.2">écho</w> <w n="61.3">retentissant</w> <w n="61.4">des</w> <w n="61.5">douleurs</w>, <w n="61.6">des</w> <w n="61.7">outrages</w>,</l>
						<l n="62" num="7.6"><w n="62.1">Des</w> <w n="62.2">grandes</w> <w n="62.3">lâchetés</w> <w n="62.4">après</w> <w n="62.5">les</w> <w n="62.6">grands</w> <w n="62.7">courages</w> !</l>
						<l n="63" num="7.7"><w n="63.1">Par</w> <w n="63.2">les</w> <w n="63.3">événements</w> <w n="63.4">à</w> <w n="63.5">moi</w>-<w n="63.6">même</w> <w n="63.7">arraché</w>,</l>
						<l n="64" num="7.8"><w n="64.1">Le</w> <w n="64.2">ne</w> <w n="64.3">vous</w> <w n="64.4">cherchais</w> <w n="64.5">pas</w>, <w n="64.6">et</w> <w n="64.7">vous</w> <w n="64.8">m</w>'<w n="64.9">avez</w> <w n="64.10">cherché</w> ;</l>
						<l n="65" num="7.9"><w n="65.1">Au</w> <w n="65.2">spectacle</w> <w n="65.3">entraînant</w> <w n="65.4">de</w> <w n="65.5">toutes</w> <w n="65.6">ces</w> <w n="65.7">vaillances</w>,</l>
						<l n="66" num="7.10"><w n="66.1">Vous</w> <w n="66.2">avez</w> <w n="66.3">de</w> <w n="66.4">mon</w> <w n="66.5">cœur</w> <w n="66.6">dompté</w> <w n="66.7">les</w> <w n="66.8">défaillances</w> !</l>
						<l n="67" num="7.11"><w n="67.1">En</w> <w n="67.2">vain</w>, <w n="67.3">quand</w> <w n="67.4">il</w> <w n="67.5">mesure</w> <w n="67.6">où</w> <w n="67.7">vos</w> <w n="67.8">coups</w> <w n="67.9">ont</w> <w n="67.10">porté</w>,</l>
						<l n="68" num="7.12"><w n="68.1">Ce</w> <w n="68.2">faible</w> <w n="68.3">cœur</w> <w n="68.4">parfois</w> <w n="68.5">recule</w> <w n="68.6">épouvanté</w> ;</l>
						<l n="69" num="7.13"><w n="69.1">Réchauffez</w> <w n="69.2">ses</w> <w n="69.3">élans</w> <w n="69.4">de</w> <w n="69.5">vos</w> <w n="69.6">mâles</w> <w n="69.7">ivresses</w>,</l>
						<l n="70" num="7.14"><w n="70.1">Et</w> <w n="70.2">suivez</w> <w n="70.3">dans</w> <w n="70.4">leur</w> <w n="70.5">vol</w> <w n="70.6">les</w> <w n="70.7">balles</w> <w n="70.8">vengeresses</w> !</l>
						<l n="71" num="7.15"><w n="71.1">Soyez</w> <w n="71.2">dignes</w> <w n="71.3">de</w> <w n="71.4">ceux</w> <w n="71.5">qui</w>, <w n="71.6">dans</w> <w n="71.7">un</w> <w n="71.8">jour</w> <w n="71.9">de</w> <w n="71.10">deuil</w>,</l>
						<l n="72" num="7.16"><w n="72.1">Voyant</w> <w n="72.2">la</w> <w n="72.3">France</w> <w n="72.4">morte</w> <w n="72.5">et</w> <w n="72.6">couchée</w> <w n="72.7">au</w> <w n="72.8">cercueil</w>,</l>
						<l n="73" num="7.17"><w n="73.1">D</w>'<w n="73.2">un</w> <w n="73.3">geste</w> <w n="73.4">tout</w> <w n="73.5">puissant</w> <w n="73.6">ont</w> <w n="73.7">soulevé</w> <w n="73.8">la</w> <w n="73.9">pierre</w>,</l>
						<l n="74" num="7.18"><w n="74.1">Écarte</w> <w n="74.2">le</w> <w n="74.3">linceul</w>, <w n="74.4">dessillé</w> <w n="74.5">la</w> <w n="74.6">paupière</w>,</l>
						<l n="75" num="7.19"><w n="75.1">Et</w>, <w n="75.2">dans</w> <w n="75.3">l</w>'<w n="75.4">enivrement</w> <w n="75.5">superbe</w> <w n="75.6">de</w> <w n="75.7">leur</w> <w n="75.8">foi</w>,</l>
						<l n="76" num="7.20"><w n="76.1">Ont</w> <w n="76.2">crié</w> : <w n="76.3">Lève</w>-<w n="76.4">toi</w>, <w n="76.5">Lazare</w> ! <w n="76.6">lève</w>-<w n="76.7">toi</w> !…</l>
					</lg>
					<lg n="8">
						<l n="77" num="8.1"><w n="77.1">Ah</w> ! <w n="77.2">ceux</w>-<w n="77.3">là</w> <w n="77.4">parmi</w> <w n="77.5">tous</w> <w n="77.6">méritent</w> <w n="77.7">des</w> <w n="77.8">couronnes</w>,</l>
						<l n="78" num="8.2"><w n="78.1">Non</w> <w n="78.2">pas</w> <w n="78.3">celles</w> <w n="78.4">des</w> <w n="78.5">rois</w>, <w n="78.6">mais</w> <w n="78.7">celles</w> <w n="78.8">que</w> <w n="78.9">tu</w> <w n="78.10">donnes</w>,</l>
						<l n="79" num="8.3"><w n="79.1">Juste</w> <w n="79.2">Postérité</w>, <w n="79.3">de</w> <w n="79.4">qui</w> <w n="79.5">l</w>'<w n="79.6">arrêt</w> <w n="79.7">fatal</w></l>
						<l n="80" num="8.4"><w n="80.1">Dresse</w> <w n="80.2">le</w> <w n="80.3">pilori</w>, <w n="80.4">fonde</w> <w n="80.5">le</w> <w n="80.6">piédestal</w> !</l>
						<l n="81" num="8.5"><w n="81.1">Impassibles</w>, <w n="81.2">debout</w>, <w n="81.3">sans</w> <w n="81.4">haine</w> <w n="81.5">et</w> <w n="81.6">sans</w> <w n="81.7">colère</w>,</l>
						<l n="82" num="8.6"><w n="82.1">Contenant</w> <w n="82.2">les</w> <w n="82.3">fureurs</w> <w n="82.4">du</w> <w n="82.5">lion</w> <w n="82.6">populaire</w>,</l>
						<l n="83" num="8.7"><w n="83.1">Ils</w>' <w n="83.2">ont</w> <w n="83.3">pris</w> <w n="83.4">les</w> <w n="83.5">canons</w> <w n="83.6">par</w> <w n="83.7">l</w>'<w n="83.8">empire</w> <w n="83.9">encloués</w>,</l>
						<l n="84" num="8.8"><w n="84.1">Et</w>, <w n="84.2">nouveaux</w> <w n="84.3">Décius</w>, <w n="84.4">ils</w> <w n="84.5">se</w> <w n="84.6">sont</w> <w n="84.7">dévoués</w> !</l>
					</lg>
					<lg n="9">
						<l n="85" num="9.1"><w n="85.1">Que</w> <w n="85.2">l</w>'<w n="85.3">ingrat</w> <w n="85.4">les</w> <w n="85.5">délaisse</w> <w n="85.6">ou</w> <w n="85.7">l</w>'<w n="85.8">aveugle</w> <w n="85.9">les</w> <w n="85.10">nie</w> !…</l>
						<l n="86" num="9.2"><w n="86.1">Ambitieux</w> ! <w n="86.2">dira</w> <w n="86.3">l</w>'<w n="86.4">impure</w> <w n="86.5">calomnie</w>. —</l>
						<l n="87" num="9.3"><w n="87.1">Ambitieux</w>, <w n="87.2">c</w>'<w n="87.3">est</w> <w n="87.4">vrai</w>… <w n="87.5">de</w> <w n="87.6">lutter</w>, <w n="87.7">de</w> <w n="87.8">souffrir</w>,</l>
						<l n="88" num="9.4"><w n="88.1">De</w> <w n="88.2">sauver</w> <w n="88.3">le</w> <w n="88.4">pays</w>, <w n="88.5">de</w> <w n="88.6">vaincre</w> <w n="88.7">et</w> <w n="88.8">de</w> <w n="88.9">mourir</w> !…</l>
					</lg>
					<lg n="10">
						<l n="89" num="10.1"><w n="89.1">Et</w> <w n="89.2">ces</w> <w n="89.3">grands</w> <w n="89.4">dévoûments</w>, <w n="89.5">ces</w> <w n="89.6">courages</w> <w n="89.7">sublimes</w></l>
						<l n="90" num="10.2"><w n="90.1">N</w>'<w n="90.2">auraient</w> <w n="90.3">pas</w> <w n="90.4">le</w> <w n="90.5">pouvoir</w> <w n="90.6">de</w> <w n="90.7">racheter</w> <w n="90.8">nos</w> <w n="90.9">crimes</w> ?</l>
						<l n="91" num="10.3"><w n="91.1">Cette</w> <w n="91.2">France</w>, <w n="91.3">que</w> <w n="91.4">Dieu</w> <w n="91.5">rejette</w> <w n="91.6">à</w> <w n="91.7">l</w>'<w n="91.8">abandon</w>,</l>
						<l n="92" num="10.4"><w n="92.1">Après</w> <w n="92.2">le</w> <w n="92.3">châtiment</w> <w n="92.4">n</w>'<w n="92.5">aurait</w> <w n="92.6">pas</w> <w n="92.7">le</w> <w n="92.8">pardon</w> ?…</l>
						<l n="93" num="10.5"><w n="93.1">O</w> <w n="93.2">France</w>, <w n="93.3">espère</w> <w n="93.4">mieux</w> <w n="93.5">du</w> <w n="93.6">ciel</w> <w n="93.7">et</w> <w n="93.8">de</w> <w n="93.9">toi</w>-<w n="93.10">même</w> !</l>
						<l n="94" num="10.6"><w n="94.1">Tends</w> <w n="94.2">tes</w> <w n="94.3">muscles</w> <w n="94.4">brisés</w> <w n="94.5">dans</w> <w n="94.6">un</w> <w n="94.7">effort</w> <w n="94.8">suprême</w>,</l>
						<l n="95" num="10.7"><w n="95.1">Et</w> <w n="95.2">redresse</w> <w n="95.3">d</w>'<w n="95.4">un</w> <w n="95.5">bond</w> <w n="95.6">ton</w> <w n="95.7">pauvre</w> <w n="95.8">corps</w> <w n="95.9">sanglant</w></l>
						<l n="96" num="10.8"><w n="96.1">Sous</w> <w n="96.2">l</w>'<w n="96.3">exécrable</w> <w n="96.4">fouet</w> <w n="96.5">d</w>'<w n="96.6">un</w> <w n="96.7">vainqueur</w> <w n="96.8">insolent</w> !</l>
						<l n="97" num="10.9"><w n="97.1">Un</w> <w n="97.2">peuple</w> <w n="97.3">n</w>'<w n="97.4">est</w> <w n="97.5">vaincu</w> <w n="97.6">que</w> <w n="97.7">s</w>'<w n="97.8">il</w> <w n="97.9">consent</w> <w n="97.10">à</w> <w n="97.11">l</w>'<w n="97.12">être</w>.</l>
						<l n="98" num="10.10"><w n="98.1">Tu</w> <w n="98.2">combats</w> <w n="98.3">pour</w> <w n="98.4">toi</w>-<w n="98.5">même</w> <w n="98.6">et</w> <w n="98.7">non</w> <w n="98.8">plus</w> <w n="98.9">pour</w> <w n="98.10">un</w> <w n="98.11">maître</w></l>
						<l n="99" num="10.11"><w n="99.1">Soldat</w> <w n="99.2">de</w> <w n="99.3">la</w> <w n="99.4">justice</w> <w n="99.5">et</w> <w n="99.6">de</w> <w n="99.7">la</w> <w n="99.8">liberté</w>,</l>
						<l n="100" num="10.12"><w n="100.1">Une</w> <w n="100.2">seconde</w> <w n="100.3">fois</w> <w n="100.4">sauve</w> <w n="100.5">l</w>'<w n="100.6">humanité</w> !</l>
						<l n="101" num="10.13"><w n="101.1">Ton</w> <w n="101.2">sang</w>, <w n="101.3">flot</w> <w n="101.4">rédempteur</w>, <w n="101.5">de</w> <w n="101.6">cette</w> <w n="101.7">croix</w> <w n="101.8">immonde</w>,</l>
						<l n="102" num="10.14"><w n="102.1">Comme</w> <w n="102.2">celui</w> <w n="102.3">du</w> <w n="102.4">Christ</w>, <w n="102.5">coulera</w> <w n="102.6">sur</w> <w n="102.7">le</w> <w n="102.8">monde</w> ;</l>
						<l n="103" num="10.15"><w n="103.1">Et</w> <w n="103.2">le</w> <w n="103.3">troisième</w> <w n="103.4">jour</w>, <w n="103.5">tendant</w> <w n="103.6">aux</w> <w n="103.7">cieux</w> <w n="103.8">les</w> <w n="103.9">bras</w>,</l>
						<l n="104" num="10.16"><w n="104.1">O</w> <w n="104.2">France</w>, <w n="104.3">comme</w> <w n="104.4">lui</w>, <w n="104.5">tu</w> <w n="104.6">ressusciteras</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Novembre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>