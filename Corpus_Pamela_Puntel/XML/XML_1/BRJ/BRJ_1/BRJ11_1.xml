<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ11">
					<head type="number">XI</head>
					<head type="main">LES BULLETINS DE VICTOIRE</head>
					<div type="section" n="1">
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Non</w>, <w n="1.2">Bertrand</w> <w n="1.3">et</w> <w n="1.4">Robert</w> <w n="1.5">Macaire</w></l>
							<l n="2" num="1.2"><w n="2.1">Ne</w> <w n="2.2">furent</w> <w n="2.3">pas</w> <w n="2.4">plus</w> <w n="2.5">impudents</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Ils</w> <w n="3.2">mentent</w>, <w n="3.3">ces</w> <w n="3.4">foudres</w> <w n="3.5">de</w> <w n="3.6">guerre</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Gomme</w> <w n="4.2">des</w> <w n="4.3">arracheurs</w> <w n="4.4">de</w> <w n="4.5">dents</w> :</l>
							<l n="5" num="1.5"><w n="5.1">Ces</w> <w n="5.2">pick</w>-<w n="5.3">pocket</w> <w n="5.4">de</w> <w n="5.5">la</w> <w n="5.6">victoire</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Escamoteurs</w> <w n="6.2">de</w> <w n="6.3">bulletins</w>,</l>
							<l n="7" num="1.7"><w n="7.1">Vous</w> <w n="7.2">brusquent</w> <w n="7.3">sans</w> <w n="7.4">façon</w> <w n="7.5">la</w> <w n="7.6">gloire</w>.</l>
							<l n="8" num="1.8"><w n="8.1">Comme</w> <w n="8.2">la</w> <w n="8.3">pire</w> <w n="8.4">des</w> <w n="8.5">catins</w>.</l>
							<l n="9" num="1.9"><w n="9.1">Brave</w> <w n="9.2">fille</w>, <w n="9.3">à</w> <w n="9.4">coups</w> <w n="9.5">de</w> <w n="9.6">cravache</w></l>
							<l n="10" num="1.10"><w n="10.1">Elle</w> <w n="10.2">corrige</w> <w n="10.3">ces</w> <w n="10.4">valets</w> ;</l>
							<l n="11" num="1.11"><w n="11.1">C</w>'<w n="11.2">est</w> <w n="11.3">avec</w> <w n="11.4">des</w> <w n="11.5">airs</w> <w n="11.6">de</w> <w n="11.7">bravache</w></l>
							<l n="12" num="1.12"><w n="12.1">Qu</w>'<w n="12.2">ils</w> <w n="12.3">en</w> <w n="12.4">reçoivent</w> <w n="12.5">les</w> <w n="12.6">soufflets</w>.</l>
							<l n="13" num="1.13"><w n="13.1">Ils</w> <w n="13.2">vous</w> <w n="13.3">ont</w> <w n="13.4">des</w> <w n="13.5">transports</w> <w n="13.6">de</w> <w n="13.7">joie</w>,</l>
							<l n="14" num="1.14"><w n="14.1">Ces</w> <w n="14.2">pantins</w> <w n="14.3">à</w> <w n="14.4">casques</w> <w n="14.5">pointus</w>,</l>
							<l n="15" num="1.15"><w n="15.1">Triomphants</w> <w n="15.2">quand</w> <w n="15.3">on</w> <w n="15.4">les</w> <w n="15.5">foudroie</w>,</l>
							<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">contents</w> <w n="16.3">quand</w> <w n="16.4">ils</w> <w n="16.5">sont</w> <w n="16.6">battus</w> !</l>
							<l n="17" num="1.17"><w n="17.1">Que</w> <w n="17.2">le</w> <w n="17.3">mensonge</w> <w n="17.4">soit</w> <w n="17.5">leur</w> <w n="17.6">règle</w>,</l>
							<l n="18" num="1.18"><w n="18.1">Faut</w>-<w n="18.2">il</w> <w n="18.3">nous</w> <w n="18.4">en</w> <w n="18.5">plaindre</w> ?… <w n="18.6">Fi</w> <w n="18.7">donc</w> !</l>
							<l n="19" num="1.19"><w n="19.1">L</w>'<w n="19.2">Europe</w> <w n="19.3">sait</w> <w n="19.4">trop</w> <w n="19.5">que</w> <w n="19.6">leur</w> <w n="19.7">aigle</w></l>
							<l n="20" num="1.20"><w n="20.1">A</w> <w n="20.2">des</w> <w n="20.3">allures</w> <w n="20.4">de</w> <w n="20.5">dindon</w>.</l>
							<l n="21" num="1.21"><w n="21.1">Au</w> <w n="21.2">bruit</w> <w n="21.3">fêlé</w> <w n="21.4">de</w> <w n="21.5">son</w> <w n="21.6">tonnerre</w>,</l>
							<l n="22" num="1.22"><w n="22.1">On</w> <w n="22.2">reconduira</w> <w n="22.3">ce</w> <w n="22.4">vilain</w>,</l>
							<l n="23" num="1.23"><w n="23.1">Qui</w> <w n="23.2">se</w> <w n="23.3">prétend</w> <w n="23.4">sorti</w> <w n="23.5">d</w>'<w n="23.6">une</w> <w n="23.7">aire</w>,</l>
							<l n="24" num="1.24"><w n="24.1">Dans</w> <w n="24.2">son</w> <w n="24.3">poulailler</w> <w n="24.4">de</w> <w n="24.5">Berlin</w> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1870">22 août 1870</date>
							</dateline>
						</closer>
					</div>
					<div type="section" n="2">
						<lg n="1">
							<l n="25" num="1.1"><w n="25.1">Hélas</w> ! <w n="25.2">la</w> <w n="25.3">trahison</w>, <w n="25.4">les</w> <w n="25.5">crimes</w>,</l>
							<l n="26" num="1.2"><w n="26.1">Arrêtant</w> <w n="26.2">la</w> <w n="26.3">France</w> <w n="26.4">en</w> <w n="26.5">chemin</w>,</l>
							<l n="27" num="1.3"><w n="27.1">Ont</w> <w n="27.2">fait</w> <w n="27.3">mentir</w> <w n="27.4">ces</w> <w n="27.5">pauvres</w> <w n="27.6">rimes</w> ;</l>
							<l n="28" num="1.4"><w n="28.1">Soit</w> !… <w n="28.2">elles</w> <w n="28.3">diront</w> <w n="28.4">vrai</w> <w n="28.5">demain</w> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1870">8 novembre 1 870.</date>
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>