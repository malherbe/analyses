<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ67">
					<head type="number">LXVII</head>
					<head type="main">LA SAINTE-VEHME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>'<w n="1.2">assassinat</w> ?… <w n="1.3">jamais</w> ! — <w n="1.4">Comme</w> <w n="1.5">la</w> <w n="1.6">Sainte</w>-<w n="1.7">Vehme</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Des</w> <w n="2.2">tribunaux</w> <w n="2.3">secrets</w>, <w n="2.4">enflammant</w> <w n="2.5">les</w> <w n="2.6">esprits</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Sur</w> <w n="3.2">Guillaume</w> <w n="3.3">et</w> <w n="3.4">Bismark</w> <w n="3.5">ont</w> <w n="3.6">jeté</w> <w n="3.7">l</w>'<w n="3.8">anathême</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">Et</w> <w n="4.2">mis</w> <w n="4.3">leur</w> <w n="4.4">tête</w> <w n="4.5">à</w> <w n="4.6">prix</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Non</w> !… <w n="5.2">L</w>'<w n="5.3">honneur</w> <w n="5.4">sans</w> <w n="5.5">souillure</w> <w n="5.6">est</w> <w n="5.7">à</w> <w n="5.8">la</w> <w n="5.9">France</w> <w n="5.10">altière</w></l>
						<l n="6" num="2.2"><w n="6.1">Ce</w> <w n="6.2">qu</w>'<w n="6.3">est</w> <w n="6.4">la</w> <w n="6.5">neige</w> <w n="6.6">vierge</w> <w n="6.7">à</w> <w n="6.8">d</w>'<w n="6.9">orgueilleux</w> <w n="6.10">sommets</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Son</w> <w n="7.2">bras</w>, <w n="7.3">quand</w> <w n="7.4">il</w> <w n="7.5">le</w> <w n="7.6">faut</w>, <w n="7.7">tue</w> <w n="7.8">en</w> <w n="7.9">pleine</w> <w n="7.10">lumière</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="12"></space><w n="8.1">Mais</w> <w n="8.2">dans</w> <w n="8.3">l</w>'<w n="8.4">ombre</w>, <w n="8.5">jamais</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Qu</w>'<w n="9.2">importent</w> <w n="9.3">les</w> <w n="9.4">forfaits</w> <w n="9.5">d</w>'<w n="9.6">une</w> <w n="9.7">race</w> <w n="9.8">ennemie</w> ?</l>
						<l n="10" num="3.2"><w n="10.1">Faut</w>-<w n="10.2">il</w> <w n="10.3">par</w> <w n="10.4">le</w> <w n="10.5">poison</w> <w n="10.6">punir</w> <w n="10.7">l</w>'<w n="10.8">empoisonneur</w> ?</l>
						<l n="11" num="3.3"><w n="11.1">On</w> <w n="11.2">ne</w> <w n="11.3">mesure</w> <w n="11.4">pas</w> <w n="11.5">la</w> <w n="11.6">peine</w> <w n="11.7">à</w> <w n="11.8">l</w>'<w n="11.9">infamie</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="12"></space><w n="12.1">Mais</w> <w n="12.2">à</w> <w n="12.3">son</w> <w n="12.4">propre</w> <w n="12.5">honneur</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">N</w>'<w n="13.2">oublions</w> <w n="13.3">pas</w>, <w n="13.4">surtout</w>, <w n="13.5">que</w> <w n="13.6">l</w>'<w n="13.7">argent</w> <w n="13.8">prostitue</w></l>
						<l n="14" num="4.2"><w n="14.1">Ce</w> <w n="14.2">qu</w>'<w n="14.3">il</w> <w n="14.4">paye</w>, <w n="14.5">et</w> <w n="14.6">flétrit</w> <w n="14.7">encore</w> <w n="14.8">un</w> <w n="14.9">tel</w> <w n="14.10">dessein</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Pour</w> <w n="15.2">sauver</w> <w n="15.3">la</w> <w n="15.4">patrie</w> <w n="15.5">on</w> <w n="15.6">se</w> <w n="15.7">dévoue</w>, <w n="15.8">on</w> <w n="15.9">tue</w> !…</l>
						<l n="16" num="4.4"><space unit="char" quantity="12"></space><w n="16.1">L</w>'<w n="16.2">argent</w> <w n="16.3">fait</w> <w n="16.4">l</w>'<w n="16.5">assassin</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Qu</w>'<w n="17.2">un</w> <w n="17.3">soldat</w> <w n="17.4">les</w> <w n="17.5">ajuste</w> <w n="17.6">et</w> <w n="17.7">les</w> <w n="17.8">frappe</w> <w n="17.9">à</w> <w n="17.10">la</w> <w n="17.11">tête</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Dans</w> <w n="18.2">quelque</w> <w n="18.3">coup</w> <w n="18.4">de</w> <w n="18.5">main</w> <w n="18.6">servi</w> <w n="18.7">par</w> <w n="18.8">le</w> <w n="18.9">hasard</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">ce</w> <w n="19.3">jour</w>-<w n="19.4">là</w> <w n="19.5">sera</w> <w n="19.6">pour</w> <w n="19.7">nous</w> <w n="19.8">un</w> <w n="19.9">jour</w> <w n="19.10">de</w> <w n="19.11">fête</w> ;</l>
						<l n="20" num="5.4"><space unit="char" quantity="12"></space><w n="20.1">Mais</w> <w n="20.2">à</w> <w n="20.3">bas</w> <w n="20.4">le</w> <w n="20.5">poignard</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Ces</w> <w n="21.2">procédés</w> <w n="21.3">fâcheux</w> <w n="21.4">conviennent</w> <w n="21.5">à</w> <w n="21.6">la</w> <w n="21.7">Prusse</w> ;</l>
						<l n="22" num="6.2"><w n="22.1">Ils</w> <w n="22.2">souilleraient</w> <w n="22.3">la</w> <w n="22.4">France</w> <w n="22.5">en</w> <w n="22.6">y</w> <w n="22.7">trouvant</w> <w n="22.8">accès</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Pour</w> <w n="23.2">y</w> <w n="23.3">donner</w> <w n="23.4">ma</w> <w n="23.5">voix</w>, <w n="23.6">il</w> <w n="23.7">faudrait</w> <w n="23.8">que</w> <w n="23.9">je</w> <w n="23.10">fusse</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="12"></space><w n="24.1">Prussien</w> <w n="24.2">et</w> <w n="24.3">non</w> <w n="24.4">Français</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>