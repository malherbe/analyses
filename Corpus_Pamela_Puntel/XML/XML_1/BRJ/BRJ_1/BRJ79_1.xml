<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ79">
					<head type="number">LXXIX</head>
					<head type="main">COUR ET JARDIN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ces</w> <w n="1.2">aimables</w> <w n="1.3">scapins</w> <w n="1.4">ont</w> <w n="1.5">des</w> <w n="1.6">ruses</w> <w n="1.7">de</w> <w n="1.8">grue</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">grossissent</w> <w n="2.3">leur</w> <w n="2.4">nombre</w> <w n="2.5">aux</w> <w n="2.6">yeux</w> <w n="2.7">des</w> <w n="2.8">bons</w> <w n="2.9">bourgeois</w></l>
						<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">les</w> <w n="3.3">voit</w> <w n="3.4">défiler</w> <w n="3.5">cinq</w> <w n="3.6">cents</w> <w n="3.7">par</w> <w n="3.8">une</w> <w n="3.9">rue</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">En</w> <w n="4.2">revenant</w> <w n="4.3">par</w> <w n="4.4">l</w>'<w n="4.5">autre</w>, <w n="4.6">ils</w> <w n="4.7">sont</w> <w n="4.8">mille</w>… — <w n="4.9">sournois</w> !</l>
						<l n="5" num="1.5"><w n="5.1">C</w>'<w n="5.2">est</w> <w n="5.3">ainsi</w> <w n="5.4">que</w> <w n="5.5">chez</w> <w n="5.6">nous</w> <w n="5.7">le</w> <w n="5.8">cirque</w>, <w n="5.9">usant</w> <w n="5.10">d</w>'<w n="5.11">adresse</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Éblouit</w> <w n="6.2">son</w> <w n="6.3">public</w>, <w n="6.4">et</w>, <w n="6.5">par</w> <w n="6.6">un</w> <w n="6.7">tour</w> <w n="6.8">badin</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Fait</w> <w n="7.2">sortir</w> <w n="7.3">ses</w> <w n="7.4">soldats</w> <w n="7.5">à</w> <w n="7.6">quinze</w> <w n="7.7">sous</w> <w n="7.8">la</w> <w n="7.9">pièce</w></l>
						<l n="8" num="1.8"><w n="8.1">Côté</w> <w n="8.2">cour</w>, <w n="8.3">et</w> <w n="8.4">les</w> <w n="8.5">fait</w> <w n="8.6">rentrer</w> <w n="8.7">côté</w> <w n="8.8">jardin</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>