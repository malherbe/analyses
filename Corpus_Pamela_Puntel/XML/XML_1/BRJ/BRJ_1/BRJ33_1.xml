<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ33">
					<head type="number">XXXIII</head>
					<head type="main">LES CASQUES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>'<w n="1.2">est</w> <w n="1.3">une</w> <w n="1.4">nation</w> <w n="1.5">militaire</w>, <w n="1.6">voilà</w> !…</l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">casque</w> <w n="2.3">en</w> <w n="2.4">est</w> <w n="2.5">le</w> <w n="2.6">fond</w> : <w n="2.7">le</w> <w n="2.8">casque</w> <w n="2.9">en</w> <w n="2.10">est</w> <w n="2.11">le</w> <w n="2.12">signe</w></l>
						<l n="3" num="1.3"><w n="3.1">En</w> <w n="3.2">habits</w> <w n="3.3">de</w> <w n="3.4">conquête</w>, <w n="3.5">en</w> <w n="3.6">habits</w> <w n="3.7">de</w> <w n="3.8">gala</w>,</l>
						<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">mode</w> <w n="4.3">le</w> <w n="4.4">protège</w>, <w n="4.5">ainsi</w> <w n="4.6">que</w> <w n="4.7">la</w> <w n="4.8">consigne</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">pointe</w> <w n="5.3">en</w> <w n="5.4">est</w> <w n="5.5">superbe</w> <w n="5.6">et</w> <w n="5.7">menace</w> <w n="5.8">les</w> <w n="5.9">cieux</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Retournée</w> <w n="6.2">à</w> <w n="6.3">propos</w>, <w n="6.4">elle</w> <w n="6.5">lui</w> <w n="6.6">sert</w> <w n="6.7">de</w> <w n="6.8">base</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Suivant</w> <w n="7.2">l</w>'<w n="7.3">occasion</w>, <w n="7.4">ce</w> <w n="7.5">casque</w> <w n="7.6">audacieux</w></l>
						<l n="8" num="2.4"><w n="8.1">Tiendra</w> <w n="8.2">lieu</w> <w n="8.3">de</w> <w n="8.4">marmite</w>… <w n="8.5">ou</w> <w n="8.6">de</w> <w n="8.7">tout</w> <w n="8.8">autre</w> <w n="8.9">vase</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">Prussien</w> <w n="9.3">naît</w> <w n="9.4">et</w> <w n="9.5">meurt</w>, <w n="9.6">et</w> <w n="9.7">son</w> <w n="9.8">casque</w> <w n="9.9">le</w> <w n="9.10">suit</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Le</w> <w n="10.2">fris</w> <w n="10.3">donne</w> <w n="10.4">une</w> <w n="10.5">larme</w> <w n="10.6">au</w> <w n="10.7">casque</w> <w n="10.8">de</w> <w n="10.9">son</w> <w n="10.10">père</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">bourgeois</w>, <w n="11.3">pour</w> <w n="11.4">dormir</w>, <w n="11.5">met</w> <w n="11.6">son</w> <w n="11.7">casque</w> <w n="11.8">de</w> <w n="11.9">nuit</w> ;</l>
						<l n="12" num="3.4"><w n="12.1">Sa</w> <w n="12.2">moitié</w> <w n="12.3">met</w> <w n="12.4">le</w> <w n="12.5">sien</w>, <w n="12.6">et</w> <w n="12.7">les</w> <w n="12.8">deux</w> <w n="12.9">font</w> <w n="12.10">la</w> <w n="12.11">paire</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L</w>'<w n="13.2">amant</w> <w n="13.3">offre</w> <w n="13.4">à</w> <w n="13.5">sa</w> <w n="13.6">belle</w> <w n="13.7">et</w> <w n="13.8">son</w> <w n="13.9">casque</w> <w n="13.10">et</w> <w n="13.11">son</w> <w n="13.12">cœur</w> !</l>
						<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">casque</w> <w n="14.3">lui</w> <w n="14.4">mérite</w> <w n="14.5">un</w> <w n="14.6">baiser</w> <w n="14.7">sur</w> <w n="14.8">la</w> <w n="14.9">joue</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">S</w>'<w n="15.2">il</w> <w n="15.3">ose</w> <w n="15.4">t</w>'<w n="15.5">embrasser</w> <w n="15.6">sans</w> <w n="15.7">ce</w> <w n="15.8">casque</w> <w n="15.9">vainqueur</w>,</l>
						<l n="16" num="4.4"><w n="16.1">O</w> <w n="16.2">Gretchen</w>, <w n="16.3">tu</w> <w n="16.4">rougis</w> <w n="16.5">et</w> <w n="16.6">tu</w> <w n="16.7">lui</w> <w n="16.8">fais</w> <w n="16.9">la</w> <w n="16.10">moue</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">L</w>'<w n="17.2">illustre</w> <w n="17.3">homme</w> <w n="17.4">d</w>'<w n="17.5">état</w>, <w n="17.6">voleur</w> <w n="17.7">de</w> <w n="17.8">nations</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Qui</w>, <w n="18.2">pour</w> <w n="18.3">couvrir</w> <w n="18.4">ses</w> <w n="18.5">vols</w>, <w n="18.6">méditait</w> <w n="18.7">le</w> <w n="18.8">carnage</w>,</l>
						<l n="19" num="5.3"><w n="19.1">A</w> <w n="19.2">mûri</w> <w n="19.3">lentement</w> <w n="19.4">ses</w> <w n="19.5">machinations</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Sous</w> <w n="20.2">un</w> <w n="20.3">casque</w> <w n="20.4">qui</w> <w n="20.5">date</w> <w n="20.6">au</w> <w n="20.7">moins</w> <w n="20.8">du</w> <w n="20.9">moyen</w> <w n="20.10">âge</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Guillaume</w>, <w n="21.2">enfin</w>, <w n="21.3">parmi</w> <w n="21.4">ses</w> <w n="21.5">officiers</w> <w n="21.6">fringants</w>,</l>
						<l n="22" num="6.2">(<w n="22.1">J</w>'<w n="22.2">ai</w> <w n="22.3">de</w> <w n="22.4">mes</w> <w n="22.5">propres</w> <w n="22.6">yeux</w> <w n="22.7">vu</w> <w n="22.8">ce</w> <w n="22.9">tableau</w> <w n="22.10">fantasque</w>),</l>
						<l n="23" num="6.3"><w n="23.1">Pour</w> <w n="23.2">aller</w> <w n="23.3">au</w> <w n="23.4">théâtre</w>, <w n="23.5">avait</w> <w n="23.6">un</w> <w n="23.7">frac</w>, <w n="23.8">des</w> <w n="23.9">gants</w> ;</l>
						<l n="24" num="6.4"><w n="24.1">Comme</w> <w n="24.2">vous</w>, <w n="24.3">comme</w> <w n="24.4">moi</w>… <w n="24.5">mais</w> <w n="24.6">il</w> <w n="24.7">avait</w> <w n="24.8">son</w> <w n="24.9">casque</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>