<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ54">
					<head type="number">LIV</head>
					<head type="main">LA COUR DES MIRACLES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Eh</w> <w n="1.2">bien</w> ! <w n="1.3">vous</w> <w n="1.4">ne</w> <w n="1.5">le</w> <w n="1.6">croiriez</w> <w n="1.7">pas</w> ?</l>
						<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">choses</w> <w n="2.3">sont</w> <w n="2.4">pourtant</w> <w n="2.5">certaines</w></l>
						<l n="3" num="1.3"><w n="3.1">Ce</w> <w n="3.2">ne</w> <w n="3.3">sont</w> <w n="3.4">pas</w> <w n="3.5">tant</w> <w n="3.6">les</w> <w n="3.7">soldats</w></l>
						<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">volent</w> <w n="4.3">que</w> <w n="4.4">les</w> <w n="4.5">capitaines</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Oui</w>, <w n="5.2">tous</w> <w n="5.3">ces</w> <w n="5.4">brillants</w> <w n="5.5">officiers</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Ces</w> <w n="6.2">hobereaux</w>, <w n="6.3">ces</w> <w n="6.4">gentillâtres</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Si</w> <w n="7.2">hautains</w> <w n="7.3">qu</w>'<w n="7.4">ils</w> <w n="7.5">en</w> <w n="7.6">sont</w> <w n="7.7">grossiers</w></l>
						<l n="8" num="1.8"><w n="8.1">Dans</w> <w n="8.2">leurs</w> <w n="8.3">salons</w> <w n="8.4">et</w> <w n="8.5">leurs</w> <w n="8.6">théâtres</w> ;</l>
						<l n="9" num="1.9"><w n="9.1">Tous</w> <w n="9.2">ces</w> <w n="9.3">nobles</w> <w n="9.4">à</w> <w n="9.5">parchemin</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Qui</w> <w n="10.2">passent</w> <w n="10.3">dans</w> <w n="10.4">le</w> <w n="10.5">paysage</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">nez</w> <w n="11.3">en</w> <w n="11.4">l</w>'<w n="11.5">air</w>, <w n="11.6">et</w> <w n="11.7">de</w> <w n="11.8">la</w> <w n="11.9">main</w></l>
						<l n="12" num="1.12"><w n="12.1">Frappent</w> <w n="12.2">leurs</w> <w n="12.3">soldats</w> <w n="12.4">au</w> <w n="12.5">visage</w> ;</l>
						<l n="13" num="1.13"><w n="13.1">Tous</w> <w n="13.2">ces</w> <w n="13.3">magnifiques</w> <w n="13.4">seigneurs</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Exempts</w> <w n="14.2">de</w> <w n="14.3">peur</w> <w n="14.4">et</w> <w n="14.5">de</w> <w n="14.6">reproches</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Couverts</w> <w n="15.2">de</w> <w n="15.3">grades</w> <w n="15.4">et</w> <w n="15.5">d</w>'<w n="15.6">honneurs</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Garnissent</w> <w n="16.2">volontiers</w> <w n="16.3">leurs</w> <w n="16.4">poches</w> !</l>
						<l n="17" num="1.17"><w n="17.1">Ils</w> <w n="17.2">prennent</w>, <w n="17.3">d</w>'<w n="17.4">un</w> <w n="17.5">soin</w> <w n="17.6">diligent</w>,</l>
						<l n="18" num="1.18">(<w n="18.1">C</w>'<w n="18.2">est</w> <w n="18.3">la</w> <w n="18.4">consigne</w> <w n="18.5">universelle</w>),</l>
						<l n="19" num="1.19"><w n="19.1">Les</w> <w n="19.2">montres</w>, <w n="19.3">les</w> <w n="19.4">bijoux</w>, <w n="19.5">l</w>'<w n="19.6">argent</w>…</l>
						<l n="20" num="1.20"><w n="20.1">Ils</w> <w n="20.2">daignent</w> <w n="20.3">casser</w> <w n="20.4">la</w> <w n="20.5">vaisselle</w>.</l>
						<l n="21" num="1.21"><w n="21.1">Les</w> <w n="21.2">propriétaires</w> <w n="21.3">jaloux</w></l>
						<l n="22" num="1.22"><w n="22.1">Peuvent</w> <w n="22.2">se</w> <w n="22.3">plaindre</w> ; <w n="22.4">on</w> <w n="22.5">les</w> <w n="22.6">assomme</w></l>
						<l n="23" num="1.23"><w n="23.1">Bref</w>, <w n="23.2">une</w> <w n="23.3">guerre</w> <w n="23.4">de</w> <w n="23.5">filous</w> !…</l>
						<l n="24" num="1.24"><w n="24.1">N</w>'<w n="24.2">est</w>-<w n="24.3">ce</w> <w n="24.4">pas</w> <w n="24.5">que</w> <w n="24.6">c</w>'<w n="24.7">est</w> <w n="24.8">gentilhomme</w> ?</l>
						<l n="25" num="1.25"><w n="25.1">Transformer</w> <w n="25.2">en</w> <w n="25.3">gage</w> <w n="25.4">d</w>'<w n="25.5">amour</w></l>
						<l n="26" num="1.26"><w n="26.1">Un</w> <w n="26.2">cachemire</w> <w n="26.3">qu</w>'<w n="26.4">on</w> <w n="26.5">dérobe</w> ;</l>
						<l n="27" num="1.27"><w n="27.1">Pour</w> <w n="27.2">les</w> <w n="27.3">tendresses</w> <w n="27.4">du</w> <w n="27.5">retour</w>,</l>
						<l n="28" num="1.28"><w n="28.1">Emballer</w> <w n="28.2">notre</w> <w n="28.3">garde</w>-<w n="28.4">robe</w> !</l>
						<l n="29" num="1.29"><w n="29.1">Et</w>, <w n="29.2">plus</w> <w n="29.3">tard</w>, <w n="29.4">quels</w> <w n="29.5">étonnements</w>,</l>
						<l n="30" num="1.30"><w n="30.1">Quand</w> <w n="30.2">nos</w> <w n="30.3">belles</w> <w n="30.4">patriciennes</w></l>
						<l n="31" num="1.31"><w n="31.1">Reconnaîtront</w> <w n="31.2">leurs</w> <w n="31.3">diamants</w></l>
						<l n="32" num="1.32"><w n="32.1">Au</w> <w n="32.2">cou</w> <w n="32.3">des</w> <w n="32.4">baronnes</w> <w n="32.5">prussiennes</w> !…</l>
						<l n="33" num="1.33"><w n="33.1">Cela</w> <w n="33.2">sert</w> <w n="33.3">d</w>'<w n="33.4">exemple</w> <w n="33.5">au</w> <w n="33.6">vilain</w>,</l>
						<l n="34" num="1.34"><w n="34.1">Pour</w> <w n="34.2">qui</w> <w n="34.3">les</w> <w n="34.4">grands</w> <w n="34.5">sont</w> <w n="34.6">des</w> <w n="34.7">oracles</w>. –</l>
						<l n="35" num="1.35"><w n="35.1">On</w> <w n="35.2">disait</w> : <w n="35.3">la</w> <w n="35.4">cour</w> <w n="35.5">de</w> <w n="35.6">Berlin</w>' ;</l>
						<l n="36" num="1.36"><w n="36.1">On</w> <w n="36.2">dira</w> : <w n="36.3">la</w> <w n="36.4">cour</w> <w n="36.5">des</w> <w n="36.6">miracles</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>