<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ42">
					<head type="number">XLII</head>
					<head type="main">PROCLAMATION</head>
					<lg n="1">
						<l n="1" num="1.1">» <w n="1.1">Soldats</w> <w n="1.2">confédérés</w>, <w n="1.3">en</w> <w n="1.4">entrant</w> <w n="1.5">en</w> <w n="1.6">campagne</w>,</l>
						<l n="2" num="1.2">» <w n="2.1">J</w>'<w n="2.2">exprimai</w> <w n="2.3">devant</w> <w n="2.4">vous</w> <w n="2.5">ma</w> <w n="2.6">confiance</w> <w n="2.7">en</w> <w n="2.8">Dieu</w> ! »</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>— (<w n="3.1">Comme</w> <w n="3.2">vous</w> <w n="3.3">voyez</w>, <w n="3.4">en</w> <w n="3.5">tout</w> <w n="3.6">lieu</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Le</w> <w n="4.2">Dieu</w> <w n="4.3">des</w> <w n="4.4">combats</w> <w n="4.5">l</w>'<w n="4.6">accompagne</w>.)</l>
						<l n="5" num="1.5">» — <w n="5.1">Notre</w> <w n="5.2">cause</w> <w n="5.3">était</w> <w n="5.4">juste</w>, <w n="5.5">et</w> <w n="5.6">vos</w> <w n="5.7">bras</w> <w n="5.8">valeureux</w></l>
						<l n="6" num="1.6">» <w n="6.1">N</w>'<w n="6.2">ont</w> <w n="6.3">pas</w> <w n="6.4">déçu</w> <w n="6.5">l</w>'<w n="6.6">espoir</w> <w n="6.7">que</w> <w n="6.8">je</w> <w n="6.9">fondais</w> <w n="6.10">sur</w> <w n="6.11">eux</w> ! »</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space>— (<w n="7.1">Cinq</w> <w n="7.2">contre</w> <w n="7.3">un</w>, <w n="7.4">s</w>'<w n="7.5">il</w> <w n="7.6">vous</w> <w n="7.7">plaît</w>, <w n="7.8">mon</w> <w n="7.9">maître</w> !</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Vous</w> <w n="8.2">l</w>'<w n="8.3">avez</w> <w n="8.4">oublié</w>, <w n="8.5">peut</w>-<w n="8.6">être</w> ?)</l>
						<l n="9" num="1.9">» — <w n="9.1">Je</w> <w n="9.2">vous</w> <w n="9.3">rappelle</w> <w n="9.4">Werth</w>, <w n="9.5">Saarbruck</w>, <w n="9.6">Beaumont</w>, <w n="9.7">Strasbourg</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>— (<w n="10.1">Quoi</w> ! <w n="10.2">Strasbourg</w> <w n="10.3">en</w> <w n="10.4">ligne</w> <w n="10.5">de</w> <w n="10.6">compte</w> ?)</l>
						<l n="11" num="1.11">» — <w n="11.1">Sédan</w>, <w n="11.2">Metz</w>, <w n="11.3">qui</w> <w n="11.4">devient</w> <w n="11.5">par</w> <w n="11.6">vous</w> <w n="11.7">notre</w> <w n="11.8">faubourg</w> ! »</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space>— (<w n="12.1">Oui</w>, <w n="12.2">la</w> <w n="12.3">trahison</w> <w n="12.4">et</w> <w n="12.5">la</w> <w n="12.6">honte</w> !)</l>
						<l n="13" num="1.13">» — <w n="13.1">Chacun</w> <w n="13.2">de</w> <w n="13.3">ces</w> <w n="13.4">combats</w> <w n="13.5">a</w> <w n="13.6">prouvé</w> <w n="13.7">jusqu</w>'<w n="13.8">ici</w></l>
						<l n="14" num="1.14">» <w n="14.1">Que</w> <w n="14.2">vous</w> <w n="14.3">étiez</w> <w n="14.4">les</w> <w n="14.5">fils</w> <w n="14.6">aimés</w> <w n="14.7">de</w> <w n="14.8">la</w> <w n="14.9">victoire</w>,</l>
						<l n="15" num="1.15">» <w n="15.1">Et</w> <w n="15.2">vous</w> <w n="15.3">êtes</w> <w n="15.4">restés</w> <w n="15.5">dignes</w> <w n="15.6">de</w> <w n="15.7">votre</w> <w n="15.8">gloire</w> ! »</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space>— (<w n="16.1">C</w>'<w n="16.2">est</w> <w n="16.3">vrai</w> ! <w n="16.4">nous</w> <w n="16.5">te</w> <w n="16.6">passons</w> <w n="16.7">ceci</w>.)</l>
						<l n="17" num="1.17">» — <w n="17.1">Vous</w> <w n="17.2">avez</w> <w n="17.3">conservé</w> <w n="17.4">es</w> <w n="17.5">vertus</w> <w n="17.6">militaires</w></l>
						<l n="18" num="1.18">» <w n="18.1">Qui</w> <w n="18.2">doivent</w> <w n="18.3">distinguer</w> <w n="18.4">les</w> <w n="18.5">soldats</w> !… (<w n="18.6">Des</w> <w n="18.7">notaires</w> !…</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">Comme</w> <w n="19.2">qui</w> <w n="19.3">dirait</w> <w n="19.4">le</w> <w n="19.5">viol</w>.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">L</w>'<w n="20.2">incendie</w> <w n="20.3">et</w> <w n="20.4">les</w> <w n="20.5">fusillades</w>,</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">Les</w> <w n="21.2">assassinats</w> <w n="21.3">et</w> <w n="21.4">le</w> <w n="21.5">vol</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Le</w> <w n="22.2">meurtre</w> <w n="22.3">des</w> <w n="22.4">blessés</w>, <w n="22.5">des</w> <w n="22.6">vieillards</w>, <w n="22.7">des</w> <w n="22.8">malades</w> ;</l>
						<l n="23" num="1.23"><w n="23.1">Les</w> <w n="23.2">temples</w> <w n="23.3">dévastés</w>, <w n="23.4">les</w> <w n="23.5">palais</w> <w n="23.6">abattus</w>,</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">Les</w> <w n="24.2">prisonniers</w> <w n="24.3">tués</w> <w n="24.4">en</w> <w n="24.5">masse</w>.</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Des</w> <w n="25.2">mourants</w>, <w n="25.3">qui</w> <w n="25.4">demandaient</w> <w n="25.5">grâce</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Brûlés</w>, <w n="26.2">enterrés</w> <w n="26.3">vifs</w> !… <w n="26.4">Bref</w> ! <w n="26.5">toutes</w> <w n="26.6">les</w> <w n="26.7">vertus</w> !)</l>
						<l n="27" num="1.27">» — <w n="27.1">Metz</w>, <w n="27.2">en</w> <w n="27.3">capitulant</w>, <w n="27.4">nous</w> <w n="27.5">livre</w> <w n="27.6">de</w> <w n="27.7">la</w> <w n="27.8">France</w></l>
						<l n="28" num="1.28">» <w n="28.1">Les</w> <w n="28.2">derniers</w> <w n="28.3">défenseurs</w>, <w n="28.4">la</w> <w n="28.5">dernière</w> <w n="28.6">espérance</w> ! »</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space>— (<w n="29.1">Patiente</w> <w n="29.2">un</w> <w n="29.3">peu</w> <w n="29.4">seulement</w> ;</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">Ta</w> <w n="30.2">verras</w> !… ) — « <w n="30.3">Je</w> <w n="30.4">prends</w> <w n="30.5">avantage</w></l>
						<l n="31" num="1.31"><w n="31.1">De</w> <w n="31.2">ce</w> <w n="31.3">moment</w>… » — (<w n="31.4">Veuillez</w> <w n="31.5">excuser</w> <w n="31.6">ce</w> <w n="31.7">langage</w> ;</l>
						<l n="32" num="1.32"><w n="32.1">L</w>'<w n="32.2">Allemand</w> <w n="32.3">dans</w> <w n="32.4">les</w> <w n="32.5">mots</w> <w n="32.6">brave</w> <w n="32.7">le</w> <w n="32.8">rudiment</w>.)</l>
						<l n="33" num="1.33">» — <w n="33.1">Pour</w> <w n="33.2">vous</w> <w n="33.3">remercier</w>, <w n="33.4">depuis</w> <w n="33.5">le</w> <w n="33.6">capitaine</w></l>
						<l n="34" num="1.34">» <w n="34.1">Jusqu</w>'<w n="34.2">au</w> <w n="34.3">simple</w> <w n="34.4">soldat</w>. <w n="34.5">Tous</w> <w n="34.6">vous</w> <w n="34.7">l</w>'<w n="34.8">avez</w> <w n="34.9">prouvé</w>,</l>
						<l n="35" num="1.35">» <w n="35.1">Quelque</w> <w n="35.2">soit</w> <w n="35.3">l</w>'<w n="35.4">avenir</w> <w n="35.5">qui</w> <w n="35.6">nous</w> <w n="35.7">est</w> <w n="35.8">réservé</w>,</l>
						<l n="36" num="1.36">» <w n="36.1">La</w> <w n="36.2">victoire</w> <w n="36.3">avec</w> <w n="36.4">vous</w> <w n="36.5">ne</w> <w n="36.6">peut</w> <w n="36.7">être</w> <w n="36.8">incertaine</w> ! »</l>
					</lg>
					<lg n="2">
						<l n="37" num="2.1">— <w n="37.1">Cette</w> <w n="37.2">chûte</w> <w n="37.3">est</w> <w n="37.4">heureuse</w> <w n="37.5">et</w> <w n="37.6">ne</w> <w n="37.7">compromet</w> <w n="37.8">rien</w>.</l>
						<l n="38" num="2.2"><space unit="char" quantity="8"></space><w n="38.1">Telle</w> <w n="38.2">est</w>, <w n="38.3">fidèlement</w> <w n="38.4">transcrite</w>,</l>
						<l n="39" num="2.3"><w n="39.1">La</w> <w n="39.2">proclamation</w> <w n="39.3">de</w> <w n="39.4">ce</w> <w n="39.5">prince</w> <w n="39.6">hypocrite</w>.</l>
						<l n="40" num="2.4"><w n="40.1">Le</w> <w n="40.2">texte</w> <w n="40.3">est</w> <w n="40.4">de</w> <w n="40.5">son</w> <w n="40.6">crû</w> ; — <w n="40.7">les</w> <w n="40.8">notes</w> <w n="40.9">sont</w> <w n="40.10">du</w> <w n="40.11">mien</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>