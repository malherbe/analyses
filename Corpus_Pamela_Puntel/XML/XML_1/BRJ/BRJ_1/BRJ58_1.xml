<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ58">
					<head type="number">LVIII</head>
					<head type="main">DE PROFUNDIS</head>
					<opener>
						<salute>A un ami</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Va</w>, <w n="1.2">plans</w> <w n="1.3">le</w> <w n="1.4">pays</w> <w n="1.5">qui</w> <w n="1.6">se</w> <w n="1.7">nomme</w></l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">France</w> ! <w n="2.3">répands</w> <w n="2.4">ta</w> <w n="2.5">douleur</w></l>
						<l n="3" num="1.3"><w n="3.1">Sur</w> <w n="3.2">l</w>'<w n="3.3">inévitable</w> <w n="3.4">malheur</w></l>
						<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">va</w> <w n="4.3">nous</w> <w n="4.4">écraser</w> ; — <w n="4.5">pauvre</w> <w n="4.6">homme</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Ne</w> <w n="5.2">sais</w>-<w n="5.3">tu</w> <w n="5.4">donc</w> <w n="5.5">pas</w> <w n="5.6">que</w> <w n="5.7">jamais</w></l>
						<l n="6" num="1.6"><w n="6.1">Ce</w> <w n="6.2">peuple</w>, <w n="6.3">en</w> <w n="6.4">ses</w> <w n="6.5">élans</w> <w n="6.6">sublimes</w>,</l>
						<l n="7" num="1.7"><w n="7.1">N</w>'<w n="7.2">est</w> <w n="7.3">plus</w> <w n="7.4">près</w> <w n="7.5">d</w>'<w n="7.6">atteindre</w> <w n="7.7">aux</w> <w n="7.8">sommets</w></l>
						<l n="8" num="1.8"><w n="8.1">Que</w> <w n="8.2">quand</w> <w n="8.3">il</w> <w n="8.4">est</w> <w n="8.5">dans</w> <w n="8.6">les</w> <w n="8.7">abîmes</w> ?</l>
						<l n="9" num="1.9"><w n="9.1">Il</w> <w n="9.2">est</w> <w n="9.3">vrai</w> : <w n="9.4">tu</w> <w n="9.5">vois</w> <w n="9.6">sous</w> <w n="9.7">ses</w> <w n="9.8">pas</w></l>
						<l n="10" num="1.10"><w n="10.1">S</w>'<w n="10.2">accumuler</w> <w n="10.3">tous</w> <w n="10.4">les</w> <w n="10.5">désastres</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Lève</w> <w n="11.2">tes</w> <w n="11.3">yeux</w> ! <w n="11.4">ne</w> <w n="11.5">vois</w>-<w n="11.6">tu</w> <w n="11.7">pas</w></l>
						<l n="12" num="1.12"><w n="12.1">Son</w> <w n="12.2">front</w> <w n="12.3">qui</w> <w n="12.4">plane</w> <w n="12.5">dans</w> <w n="12.6">les</w> <w n="12.7">astres</w>…</l>
						<l n="13" num="1.13"><w n="13.1">Que</w> <w n="13.2">veux</w>-<w n="13.3">tu</w> ? <w n="13.4">Dieu</w> <w n="13.5">l</w>'<w n="13.6">a</w> <w n="13.7">fait</w> <w n="13.8">ainsi</w> ;</l>
						<l n="14" num="1.14"><w n="14.1">Tu</w> <w n="14.2">peux</w> <w n="14.3">relire</w> <w n="14.4">son</w> <w n="14.5">histoire</w> :</l>
						<l n="15" num="1.15"><w n="15.1">Ou</w> <w n="15.2">d</w>'<w n="15.3">autres</w> <w n="15.4">demandent</w> <w n="15.5">merci</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Celui</w>-<w n="16.2">là</w> <w n="16.3">sonne</w> <w n="16.4">la</w> <w n="16.5">victoire</w> ! —</l>
						<l n="17" num="1.17"><w n="17.1">Oui</w>, <w n="17.2">d</w>'<w n="17.3">un</w> <w n="17.4">million</w> <w n="17.5">de</w> <w n="17.6">bandits</w></l>
						<l n="18" num="1.18"><w n="18.1">Le</w> <w n="18.2">flot</w> <w n="18.3">menaçant</w> <w n="18.4">l</w>'<w n="18.5">enveloppe</w> ;</l>
						<l n="19" num="1.19"><w n="19.1">Tous</w> <w n="19.2">les</w> <w n="19.3">sacristains</w> <w n="19.4">de</w> <w n="19.5">l</w>'<w n="19.6">Europe</w></l>
						<l n="20" num="1.20"><w n="20.1">Entonnent</w> <w n="20.2">son</w> <w n="20.3">de</w> <w n="20.4">profundis</w> !</l>
						<l n="21" num="1.21"><w n="21.1">L</w>'<w n="21.2">une</w> <w n="21.3">après</w> <w n="21.4">l</w>'<w n="21.5">autre</w> <w n="21.6">on</w> <w n="21.7">extermine</w></l>
						<l n="22" num="1.22"><w n="22.1">Nos</w> <w n="22.2">provinces</w> <w n="22.3">et</w> <w n="22.4">nos</w> <w n="22.5">cités</w> ;</l>
						<l n="23" num="1.23"><w n="23.1">Paris</w>, <w n="23.2">étreint</w> <w n="23.3">par</w> <w n="23.4">la</w> <w n="23.5">famine</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Met</w> <w n="24.2">le</w> <w n="24.3">comble</w> <w n="24.4">aux</w> <w n="24.5">calamités</w> ;</l>
						<l n="25" num="1.25"><w n="25.1">L</w>'<w n="25.2">élu</w> <w n="25.3">de</w> <w n="25.4">Dieu</w>, <w n="25.5">Guillaume</w>, <w n="25.6">invite</w></l>
						<l n="26" num="1.26"><w n="26.1">Ses</w> <w n="26.2">confrères</w> <w n="26.3">à</w> <w n="26.4">venir</w> <w n="26.5">voir</w></l>
						<l n="27" num="1.27"><w n="27.1">Comment</w> <w n="27.2">il</w> <w n="27.3">va</w> <w n="27.4">faire</w> <w n="27.5">pleuvoir</w></l>
						<l n="28" num="1.28"><w n="28.1">Le</w> <w n="28.2">pétrole</w>, <w n="28.3">son</w> <w n="28.4">eau</w> <w n="28.5">bénite</w> !…</l>
						<l n="29" num="1.29"><w n="29.1">Eh</w> <w n="29.2">bien</w>, <w n="29.3">vois</w> <w n="29.4">si</w> <w n="29.5">mon</w> <w n="29.6">cœur</w> <w n="29.7">est</w> <w n="29.8">plein</w></l>
						<l n="30" num="1.30"><w n="30.1">D</w>'<w n="30.2">une</w> <w n="30.3">démence</w> <w n="30.4">enracinée</w> !</l>
						<l n="31" num="1.31"><w n="31.1">La</w> <w n="31.2">ville</w> <w n="31.3">à</w> <w n="31.4">mes</w> <w n="31.5">yeux</w> <w n="31.6">condamnée</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Ce</w> <w n="32.2">n</w>'<w n="32.3">est</w> <w n="32.4">pas</w> <w n="32.5">Paris</w> !… <w n="32.6">c</w>'<w n="32.7">est</w> <w n="32.8">Berlin</w> !<ref type="noteAnchor" target="1">1</ref></l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Novembre 1870.</date>
						</dateline>
						<note type="footnote" id="1">Ajourné.</note>
					</closer>
				</div></body></text></TEI>