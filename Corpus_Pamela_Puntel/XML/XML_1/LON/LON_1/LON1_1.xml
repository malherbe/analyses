<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="LON">
					<name>
						<forename>Eugène</forename>
						<nameLink>de</nameLink>
						<surname>LONLAY</surname>
					</name>
					<date from="1815" to="1886">1815-1886</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>52 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">LON_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>>POÉSIES, ÉDITION ELZÉVIRIENNE</title>
						<author>EUGÈNE DE LONLAY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k65208385/f11.image.r=EUG%C3%88NE%20DE%20LONLAY</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES, ÉDITION ELZÉVIRIENNE</title>
								<author>EUGÈNE DE LONLAY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALCAN-LEVY</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="LON1">
				<head type="main">CHANT PATRIOTIQUE</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">loin</w>, <w n="1.3">la</w> <w n="1.4">renommée</w></l>
						<l n="2" num="1.2"><w n="2.1">Exalte</w> <w n="2.2">nos</w> <w n="2.3">exploits</w> :</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">France</w> <w n="3.3">est</w> <w n="3.4">acclamée</w></l>
						<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">sa</w> <w n="4.3">gloire</w> <w n="4.4">et</w> <w n="4.5">ses</w> <w n="4.6">lois</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Quand</w> <w n="5.2">son</w> <w n="5.3">cri</w> <w n="5.4">nous</w> <w n="5.5">appelle</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Des</w> <w n="6.2">discordes</w> <w n="6.3">vainqueurs</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Groupons</w> <w n="7.2">nous</w> <w n="7.3">autour</w> <w n="7.4">d</w>'<w n="7.5">elle</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Ouvrons</w>-<w n="8.2">lui</w> <w n="8.3">tous</w> <w n="8.4">nos</w> <w n="8.5">cœurs</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="9" num="1.1"><w n="9.1">Sur</w> <w n="9.2">la</w> <w n="9.3">terre</w> <w n="9.4">et</w> <w n="9.5">sur</w> <w n="9.6">l</w>'<w n="9.7">onde</w>,</l>
						<l n="10" num="1.2"><w n="10.1">Grâce</w> <w n="10.2">à</w> <w n="10.3">nos</w> <w n="10.4">fils</w> <w n="10.5">guerriers</w>,</l>
						<l n="11" num="1.3"><w n="11.1">Aux</w> <w n="11.2">limites</w> <w n="11.3">du</w> <w n="11.4">monde</w></l>
						<l n="12" num="1.4"><w n="12.1">Nous</w> <w n="12.2">cueillons</w> <w n="12.3">des</w> <w n="12.4">lauriers</w>.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><w n="13.1">La</w> <w n="13.2">France</w>, <w n="13.3">dont</w> <w n="13.4">on</w> <w n="13.5">vante</w></l>
						<l n="14" num="2.2"><w n="14.1">Les</w> <w n="14.2">nobles</w> <w n="14.3">actions</w>,</l>
						<l n="15" num="2.3"><w n="15.1">Rayonne</w> <w n="15.2">triomphante</w></l>
						<l n="16" num="2.4"><w n="16.1">Au</w> <w n="16.2">front</w> <w n="16.3">des</w> <w n="16.4">nations</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="17" num="1.1"><w n="17.1">Notre</w> <w n="17.2">arc</w>-<w n="17.3">en</w>-<w n="17.4">ciel</w> <w n="17.5">éclaire</w></l>
						<l n="18" num="1.2"><w n="18.1">L</w>'<w n="18.2">horizon</w> <w n="18.3">ténébreux</w>,</l>
						<l n="19" num="1.3"><w n="19.1">Et</w> <w n="19.2">de</w> <w n="19.3">paix</w> <w n="19.4">ou</w> <w n="19.5">de</w> <w n="19.6">guerre</w></l>
						<l n="20" num="1.4"><w n="20.1">Est</w> <w n="20.2">le</w> <w n="20.3">présage</w> <w n="20.4">heureux</w>.</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1"><w n="21.1">L</w>'<w n="21.2">esprit</w> <w n="21.3">qui</w> <w n="21.4">nous</w> <w n="21.5">dirige</w></l>
						<l n="22" num="2.2"><w n="22.1">Partout</w> <w n="22.2">nous</w> <w n="22.3">donne</w> <w n="22.4">accès</w>,</l>
						<l n="23" num="2.3"><w n="23.1">Entoure</w> <w n="23.2">de</w> <w n="23.3">prestige</w></l>
						<l n="24" num="2.4"><w n="24.1">L</w>'<w n="24.2">éclat</w> <w n="24.3">du</w> <w n="24.4">nom</w> <w n="24.5">français</w>.</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">IV</head>
					<lg n="1">
						<l n="25" num="1.1"><w n="25.1">Puissant</w> <w n="25.2">Dieu</w> <w n="25.3">de</w> <w n="25.4">nos</w> <w n="25.5">pères</w></l>
						<l n="26" num="1.2"><w n="26.1">Prête</w>-<w n="26.2">nous</w> <w n="26.3">ton</w> <w n="26.4">secours</w> ;</l>
						<l n="27" num="1.3"><w n="27.1">De</w> <w n="27.2">nos</w> <w n="27.3">destins</w> <w n="27.4">prospères</w></l>
						<l n="28" num="1.4"><w n="28.1">Double</w> <w n="28.2">et</w> <w n="28.3">bénis</w> <w n="28.4">le</w> <w n="28.5">cours</w>.</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1"><w n="29.1">Que</w> <w n="29.2">tes</w> <w n="29.3">anges</w> <w n="29.4">fidèles</w>,</l>
						<l n="30" num="2.2"><w n="30.1">A</w> <w n="30.2">l</w>'<w n="30.3">heure</w> <w n="30.4">du</w> <w n="30.5">danger</w>,</l>
						<l n="31" num="2.3"><w n="31.1">Sur</w> <w n="31.2">nous</w> <w n="31.3">ouvrent</w> <w n="31.4">leurs</w> <w n="31.5">ailes</w>,</l>
						<l n="32" num="2.4"><w n="32.1">Viennent</w> <w n="32.2">nous</w> <w n="32.3">protéger</w>.</l>
					</lg>
				</div>
				<div type="section" n="5">
					<head type="number">V</head>
					<lg n="1">
						<l n="33" num="1.1"><w n="33.1">A</w> <w n="33.2">l</w>'<w n="33.3">appel</w> <w n="33.4">de</w> <w n="33.5">la</w> <w n="33.6">France</w>,</l>
						<l n="34" num="1.2"><w n="34.1">Dieu</w>, <w n="34.2">réponds</w> <w n="34.3">à</w> <w n="34.4">ses</w> <w n="34.5">vœux</w> :</l>
						<l n="35" num="1.3"><w n="35.1">Donne</w>-<w n="35.2">lui</w> <w n="35.3">l</w>'<w n="35.4">abondance</w></l>
						<l n="36" num="1.4"><w n="36.1">Et</w> <w n="36.2">des</w> <w n="36.3">jours</w> <w n="36.4">radieux</w>.</l>
					</lg>
					<lg n="2">
						<l n="37" num="2.1"><w n="37.1">Mais</w> <w n="37.2">si</w> <w n="37.3">des</w> <w n="37.4">cris</w> <w n="37.5">d</w>'<w n="37.6">alarmes</w></l>
						<l n="38" num="2.2"><w n="38.1">Réveillent</w> <w n="38.2">nos</w> <w n="38.3">héros</w>,</l>
						<l n="39" num="2.3"><w n="39.1">Protège</w> <w n="39.2">encor</w> <w n="39.3">nos</w> <w n="39.4">armes</w>,</l>
						<l n="40" num="2.4"><w n="40.1">Illustre</w> <w n="40.2">nos</w> <w n="40.3">drapeaux</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>