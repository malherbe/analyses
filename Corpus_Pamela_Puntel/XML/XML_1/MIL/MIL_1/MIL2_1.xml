<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TRIOLETS</title>
				<title type="sub_1">Poème publié dans le journal LE FIGARO (1870-1871)</title>
				<title type="medium">Édition électronique</title>
				<author key="MIL">
					<name>
						<forename>Albert</forename>
						<surname>MILLAUD</surname>
					</name>
					<date from="1844" to="1892">1844-1892</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>200 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>UN VIEUX PRUSSIEN À UN JEUNE CONSCRIT</title>
						<author>ALBERT MILLAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2720209/f2.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Figaro : journal non politique</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro</publisher>
									<date when="1870">22 octobre 1870</date>
								</imprint>
								<biblScope unit="issue">295</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TRIOLETS</title>
						<author>ALBERT MILLAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k272023f/f2.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Figaro : journal non politique</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro</publisher>
									<date when="1870">25 octobre 1870</date>
								</imprint>
								<biblScope unit="issue">298</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TRIOLETS</title>
						<author>ALBERT MILLAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI"> https://gallica.bnf.fr/ark:/12148/bpt6k272090q.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Figaro : journal non politique</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro</publisher>
									<date when="1870">1er janvier 1871</date>
								</imprint>
								<biblScope unit="issue">1</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1870-1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="MIL2">
				<head type="main">TRIOLETS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quel</w> <w n="1.2">fantaisiste</w> <w n="1.3">que</w> <w n="1.4">Nadar</w></l>
					<l n="2" num="1.2"><w n="2.1">Aéronaute</w> <w n="2.2">ou</w> <w n="2.3">photographe</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">est</w> <w n="3.3">aussi</w> <w n="3.4">fort</w> <w n="3.5">que</w> <w n="3.6">Godard</w> :</l>
					<l n="4" num="1.4"><w n="4.1">Quel</w> <w n="4.2">fantaisiste</w> <w n="4.3">que</w> <w n="4.4">Nadar</w> !</l>
					<l n="5" num="1.5"><w n="5.1">Mais</w>, <w n="5.2">entre</w> <w n="5.3">nous</w>, <w n="5.4">Nadar</w> <w n="5.5">n</w>'<w n="5.6">a</w> <w n="5.7">d</w>'<w n="5.8">art</w></l>
					<l n="6" num="1.6"><w n="6.1">Que</w> <w n="6.2">pour</w> <w n="6.3">écrire</w> <w n="6.4">son</w> <w n="6.5">paraphe</w>…</l>
					<l n="7" num="1.7"><w n="7.1">Quel</w> <w n="7.2">fantaisiste</w> <w n="7.3">quo</w> <w n="7.4">Nadar</w></l>
					<l n="8" num="1.8"><w n="8.1">Aéronaute</w> <w n="8.2">ou</w> <w n="8.3">photographe</w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Pour</w> <w n="9.2">vous</w> <w n="9.3">enlever</w> <w n="9.4">le</w> <w n="9.5">ballon</w></l>
					<l n="10" num="2.2"><w n="10.1">Plein</w> <w n="10.2">de</w> <w n="10.3">délice</w> <w n="10.4">est</w> <w n="10.5">son</w> <w n="10.6">hélice</w>.</l>
					<l n="11" num="2.3"><w n="11.1">Il</w> <w n="11.2">a</w> <w n="11.3">bien</w> <w n="11.4">tombé</w> <w n="11.5">l</w>'<w n="11.6">Aquilon</w>,</l>
					<l n="12" num="2.4"><w n="12.1">Pour</w> <w n="12.2">vous</w> <w n="12.3">enlever</w> <w n="12.4">le</w> <w n="12.5">ballon</w>.</l>
					<l n="13" num="2.5"><w n="13.1">Avec</w> <w n="13.2">sa</w> <w n="13.3">casquette</w> <w n="13.4">à</w> <w n="13.5">gallon</w></l>
					<l n="14" num="2.6"><w n="14.1">Il</w> <w n="14.2">rappelle</w> <w n="14.3">l</w>'<w n="14.4">amiral</w> <w n="14.5">suisse</w>.</l>
					<l n="15" num="2.7"><w n="15.1">Pour</w> <w n="15.2">vous</w> <w n="15.3">enlever</w> <w n="15.4">le</w> <w n="15.5">ballon</w></l>
					<l n="16" num="2.8"><w n="16.1">Plein</w> <w n="16.2">de</w> <w n="16.3">délice</w> <w n="16.4">est</w> <w n="16.5">son</w> <w n="16.6">hélice</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Dans</w> <w n="17.2">le</w> <w n="17.3">royaume</w> <w n="17.4">des</w> <w n="17.5">oiseaux</w></l>
					<l n="18" num="3.2"><w n="18.1">Il</w> <w n="18.2">est</w> <w n="18.3">fort</w> <w n="18.4">bon</w> <w n="18.5">aéronaute</w>.</l>
					<l n="19" num="3.3"><w n="19.1">Si</w> <w n="19.2">Gambetta</w> (<w n="19.3">Je</w> <w n="19.4">plains</w> <w n="19.5">ses</w> <w n="19.6">os</w>)</l>
					<l n="20" num="3.4"><w n="20.1">Dans</w> <w n="20.2">le</w> <w n="20.3">royaume</w> <w n="20.4">des</w> <w n="20.5">oiseaux</w></l>
					<l n="21" num="3.5"><w n="21.1">Ne</w> <w n="21.2">se</w> <w n="21.3">rompit</w> <w n="21.4">pas</w> <w n="21.5">en</w> <w n="21.6">morceaux</w>,</l>
					<l n="22" num="3.6"><w n="22.1">De</w> <w n="22.2">Nadar</w> <w n="22.3">ce</w> <w n="22.4">n</w>'<w n="22.5">est</w> <w n="22.6">point</w> <w n="22.7">la</w> <w n="22.8">faute</w>…</l>
					<l n="23" num="3.7"><w n="23.1">Dans</w> <w n="23.2">le</w> <w n="23.3">royaume</w> <w n="23.4">des</w> <w n="23.5">oiseaux</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Il</w> <w n="24.2">est</w> <w n="24.3">fort</w> <w n="24.4">bon</w> <w n="24.5">aéronaute</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">A</w> <w n="25.2">Ferrière</w>, <w n="25.3">au</w>-<w n="25.4">dessus</w> <w n="25.5">du</w> <w n="25.6">parc</w>,</l>
					<l n="26" num="4.2"><w n="26.1">Le</w> <w n="26.2">voici</w> <w n="26.3">qui</w> <w n="26.4">franchit</w> <w n="26.5">l</w>'<w n="26.6">espace</w>.</l>
					<l n="27" num="4.3"><w n="27.1">On</w> <w n="27.2">dirait</w> <w n="27.3">la</w> <w n="27.4">flèche</w> <w n="27.5">d</w>'<w n="27.6">un</w> <w n="27.7">arc</w></l>
					<l n="28" num="4.4"><w n="28.1">A</w> <w n="28.2">Ferrière</w> <w n="28.3">au</w>-<w n="28.4">dessus</w> <w n="28.5">du</w> <w n="28.6">parc</w>,</l>
					<l n="29" num="4.5"><w n="29.1">Et</w> <w n="29.2">Guillaume</w> <w n="29.3">dit</w> <w n="29.4">à</w> <w n="29.5">Bismarck</w> :</l>
					<l n="30" num="4.6">— <w n="30.1">Tais</w>-<w n="30.2">toi</w> ! <w n="30.3">voici</w> <w n="30.4">Nadar</w> <w n="30.5">qui</w> <w n="30.6">passe</w>.</l>
					<l n="31" num="4.7"><w n="31.1">A</w> <w n="31.2">Ferrière</w>, <w n="31.3">au</w>-<w n="31.4">dessus</w> <w n="31.5">du</w> <w n="31.6">parc</w>,</l>
					<l n="32" num="4.8"><w n="32.1">Le</w> <w n="32.2">voici</w> <w n="32.3">qui</w> <w n="32.4">franchit</w> <w n="32.5">l</w>'<w n="32.6">espace</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">Toi</w>, <w n="33.2">plus</w> <w n="33.3">crépu</w> <w n="33.4">que</w> <w n="33.5">Clodion</w>,</l>
					<l n="34" num="5.2"><w n="34.1">O</w> <w n="34.2">toi</w> <w n="34.3">qui</w> <w n="34.4">vas</w> <w n="34.5">porter</w> <w n="34.6">ma</w> <w n="34.7">lettre</w> :</l>
					<l n="35" num="5.3"><w n="35.1">Nous</w> <w n="35.2">ferons</w>, <w n="35.3">par</w> <w n="35.4">souscription</w>,</l>
					<l n="36" num="5.4"><w n="36.1">Toi</w>, <w n="36.2">plus</w> <w n="36.3">crépu</w> <w n="36.4">que</w> <w n="36.5">Clodion</w>,</l>
					<l n="37" num="5.5"><w n="37.1">Ta</w> <w n="37.2">statue</w> <w n="37.3">en</w> <w n="37.4">collodion</w>,</l>
					<l n="38" num="5.6"><w n="38.1">Au</w>-<w n="38.2">dessus</w> <w n="38.3">d</w>'<w n="38.4">un</w> <w n="38.5">grand</w> <w n="38.6">gazomètre</w>.</l>
					<l n="39" num="5.7"><w n="39.1">Toi</w>, <w n="39.2">plus</w> <w n="39.3">crépu</w> <w n="39.4">que</w> <w n="39.5">Clodion</w>,</l>
					<l n="40" num="5.8"><w n="40.1">O</w> <w n="40.2">toi</w> <w n="40.3">qui</w> <w n="40.4">vas</w> <w n="40.5">porter</w> <w n="40.6">ma</w> <w n="40.7">lettre</w> !</l>
				</lg>
				<closer>
					<signed>Albert Millaud.</signed>
				</closer>
			</div></body></text></TEI>