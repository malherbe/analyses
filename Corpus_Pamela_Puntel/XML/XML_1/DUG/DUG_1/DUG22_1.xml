<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG22">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ne</w> <w n="1.2">récriminons</w> <w n="1.3">pas</w>, <w n="1.4">nos</w> <w n="1.5">fautes</w> <w n="1.6">sont</w> <w n="1.7">immenses</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Notre</w> <w n="2.2">châtiment</w> <w n="2.3">mérité</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">disent</w> <w n="3.3">les</w> <w n="3.4">dévots</w>, <w n="3.5">nos</w> <w n="3.6">vices</w>, <w n="3.7">nos</w> <w n="3.8">démences</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Ont</w> <w n="4.2">aigri</w> <w n="4.3">le</w> <w n="4.4">ciel</w> <w n="4.5">irrité</w>.'…</l>
					<l n="5" num="1.5"><w n="5.1">Son</w> !… <w n="5.2">mais</w>, <w n="5.3">fils</w> <w n="5.4">des</w> <w n="5.5">héros</w> <w n="5.6">dont</w> <w n="5.7">la</w> <w n="5.8">superbe</w> <w n="5.9">tête</w></l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Du</w> <w n="6.2">destin</w> <w n="6.3">défiait</w> <w n="6.4">les</w> <w n="6.5">lois</w></l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">dont</w> <w n="7.3">la</w> <w n="7.4">main</w> <w n="7.5">lançait</w>, <w n="7.6">en</w> <w n="7.7">narguant</w> <w n="7.8">la</w> <w n="7.9">tempête</w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Toutes</w> <w n="8.2">les</w> <w n="8.3">flèches</w> <w n="8.4">du</w> <w n="8.5">carquois</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Restons</w> <w n="9.2">fermes</w> ! <w n="9.3">pansons</w> <w n="9.4">bien</w> <w n="9.5">vite</w> <w n="9.6">nos</w> <w n="9.7">blessures</w>,</l>
					<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Fiers</w> <w n="10.2">athlètes</w> <w n="10.3">que</w> <w n="10.4">rien</w> <w n="10.5">n</w>'<w n="10.6">abat</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">Préparons</w>, <w n="11.2">à</w> <w n="11.3">travers</w> <w n="11.4">toutes</w> <w n="11.5">ces</w> <w n="11.6">flétrissures</w>,</l>
					<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">La</w> <w n="12.2">revanche</w> <w n="12.3">de</w> <w n="12.4">ce</w> <w n="12.5">combat</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Et</w>, <w n="13.2">malgré</w> <w n="13.3">nos</w> <w n="13.4">revers</w>, <w n="13.5">nos</w> <w n="13.6">malheurs</w>, <w n="13.7">nos</w> <w n="13.8">défaites</w></l>
					<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">Qui</w> <w n="14.2">n</w>'<w n="14.3">entachent</w> <w n="14.4">point</w> <w n="14.5">notre</w> <w n="14.6">honneur</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Nous</w> <w n="15.2">saurons</w> <w n="15.3">d</w>'<w n="15.4">un</w> <w n="15.5">élan</w> <w n="15.6">reconquérir</w> <w n="15.7">ces</w> <w n="15.8">faîtes</w></l>
					<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">Que</w> <w n="16.2">veut</w> <w n="16.3">usurper</w> <w n="16.4">le</w> <w n="16.5">vainqueur</w> !…</l>
					<l n="17" num="1.17"><w n="17.1">La</w> <w n="17.2">République</w> <w n="17.3">aura</w> <w n="17.4">raison</w> <w n="17.5">de</w> <w n="17.6">cet</w> <w n="17.7">Empire</w></l>
					<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">Fondé</w> <w n="18.2">sur</w> <w n="18.3">le</w> <w n="18.4">sang</w> <w n="18.5">et</w> <w n="18.6">le</w> <w n="18.7">deuil</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Car</w> <w n="19.2">il</w> <w n="19.3">faut</w> <w n="19.4">qu</w>'<w n="19.5">avant</w> <w n="19.6">peu</w> <w n="19.7">leur</w> <w n="19.8">Charlemagne</w> <w n="19.9">expire</w></l>
					<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">Sous</w> <w n="20.2">le</w> <w n="20.3">genou</w> <w n="20.4">de</w> <w n="20.5">notre</w> <w n="20.6">orgueil</w> ;</l>
					<l n="21" num="1.21"><w n="21.1">Il</w> <w n="21.2">faut</w> <w n="21.3">que</w>, <w n="21.4">franchissant</w> <w n="21.5">les</w> <w n="21.6">ruines</w>, <w n="21.7">les</w> <w n="21.8">tombes</w>,</l>
					<l n="22" num="1.22"><space unit="char" quantity="8"></space><w n="22.1">L</w>'<w n="22.2">Histoire</w> <w n="22.3">au</w> <w n="22.4">magique</w> <w n="22.5">burin</w></l>
					<l n="23" num="1.23"><w n="23.1">Inscrive</w> <w n="23.2">au</w> <w n="23.3">Panthéon</w>, <w n="23.4">outragé</w> <w n="23.5">par</w> <w n="23.6">leurs</w> <w n="23.7">bombes</w>,</l>
					<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">Ces</w> <w n="24.2">deux</w> <w n="24.3">mots</w> : <w n="24.4">PEUPLE</w> <w n="24.5">SOUVERAIN</w> !</l>
				</lg>
			</div></body></text></TEI>