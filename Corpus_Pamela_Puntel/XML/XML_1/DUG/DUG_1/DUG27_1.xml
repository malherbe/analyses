<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG27">
				<head type="main">LA PAIX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">paix</w>, <w n="1.3">disent</w> <w n="1.4">ces</w> <w n="1.5">gens</w> ! <w n="1.6">la</w> <w n="1.7">paix</w>, <w n="1.8">la</w> <w n="1.9">paix</w> <w n="1.10">quand</w> <w n="1.11">même</w> !</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Telle</w> <w n="2.2">que</w> <w n="2.3">la</w> <w n="2.4">veut</w> <w n="2.5">le</w> <w n="2.6">vainqueur</w> !</l>
					<l n="3" num="1.3"><w n="3.1">En</w> <w n="3.2">ce</w> <w n="3.3">monde</w> <w n="3.4">la</w> <w n="3.5">paix</w> <w n="3.6">est</w> <w n="3.7">un</w> <w n="3.8">trésor</w> <w n="3.9">suprême</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Bien</w> <w n="4.2">plus</w> <w n="4.3">précieux</w> <w n="4.4">que</w> <w n="4.5">l</w>'<w n="4.6">honneur</w> !</l>
					<l n="5" num="1.5"><w n="5.1">Donnons</w> <w n="5.2">tout</w> <w n="5.3">sans</w> <w n="5.4">compter</w>, <w n="5.5">flotte</w>, <w n="5.6">argent</w>, <w n="5.7">territoire</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Et</w> <w n="6.2">la</w> <w n="6.3">province</w> <w n="6.4">après</w> <w n="6.5">Paris</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Effaçons</w> <w n="7.2">le</w> <w n="7.3">mot</w> <w n="7.4">France</w> <w n="7.5">aux</w> <w n="7.6">pages</w> <w n="7.7">de</w> <w n="7.8">l</w>'<w n="7.9">histoire</w></l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Pour</w> <w n="8.2">avoir</w> <w n="8.3">la</w> <w n="8.4">paix</w> <w n="8.5">à</w> <w n="8.6">tout</w> <w n="8.7">prix</w> !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">— <w n="9.1">Qu</w>'<w n="9.2">un</w> <w n="9.3">peu</w> <w n="9.4">plus</w> <w n="9.5">de</w> <w n="9.6">bon</w> <w n="9.7">sens</w> <w n="9.8">à</w> <w n="9.9">vos</w> <w n="9.10">actes</w> <w n="9.11">préside</w>,</l>
					<l n="10" num="2.2"><space unit="char" quantity="8"></space><w n="10.1">Mes</w> <w n="10.2">chers</w> <w n="10.3">messieurs</w>, <w n="10.4">répondez</w>-<w n="10.5">vous</w> :</l>
					<l n="11" num="2.3"><w n="11.1">Pour</w> <w n="11.2">éviter</w> <w n="11.3">la</w> <w n="11.4">mort</w> <w n="11.5">choisir</w> <w n="11.6">le</w> <w n="11.7">suicide</w>,</l>
					<l n="12" num="2.4"><space unit="char" quantity="8"></space><w n="12.1">Il</w> <w n="12.2">faut</w> <w n="12.3">être</w> <w n="12.4">devenus</w> <w n="12.5">fous</w> ! —</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Mais</w>, <w n="13.2">quoi</w> <w n="13.3">que</w> <w n="13.4">vous</w> <w n="13.5">disiez</w> <w n="13.6">du</w> <w n="13.7">Prussien</w> <w n="13.8">qui</w> <w n="13.9">nous</w> <w n="13.10">souille</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Ces</w> <w n="14.2">trembleurs</w> <w n="14.3">dont</w> <w n="14.4">Bismark</w> <w n="14.5">a</w> <w n="14.6">chiffré</w> <w n="14.7">la</w> <w n="14.8">dépouille</w></l>
					<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">Gardent</w> <w n="15.2">leur</w> <w n="15.3">projet</w> <w n="15.4">tout</w> <w n="15.5">entier</w>…</l>
					<l n="16" num="3.4"><w n="16.1">Ils</w> <w n="16.2">courent</w> <w n="16.3">se</w> <w n="16.4">noyer</w> <w n="16.5">pour</w> <w n="16.6">fuir</w> <w n="16.7">l</w>'<w n="16.8">eau</w> <w n="16.9">qui</w> <w n="16.10">les</w> <w n="16.11">mouille</w></l>
					<l n="17" num="3.5"><w n="17.1">Et</w> <w n="17.2">ce</w> <w n="17.3">pays</w> <w n="17.4">déchu</w> <w n="17.5">voit</w> <w n="17.6">imiter</w> <w n="17.7">Gribouille</w></l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">Par</w> <w n="18.2">Prud</w>'<w n="18.3">homme</w> <w n="18.4">son</w> <w n="18.5">héritier</w> !…</l>
				</lg>
			</div></body></text></TEI>