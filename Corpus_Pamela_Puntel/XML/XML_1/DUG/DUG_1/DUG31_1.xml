<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG31">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Inflexibles</w>, <w n="1.2">tant</w> <w n="1.3">mieux</w> ! <w n="1.4">ces</w> <w n="1.5">casques</w> <w n="1.6">sont</w> <w n="1.7">stupides</w>,</l>
					<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">Guillaume</w> <w n="2.3">aux</w> <w n="2.4">valets</w>, <w n="2.5">de</w> <w n="2.6">Bismark</w> <w n="2.7">aux</w> <w n="2.8">séides</w>…</l>
					<l n="3" num="1.3"><w n="3.1">Ils</w> <w n="3.2">étaient</w> <w n="3.3">les</w> <w n="3.4">plus</w> <w n="3.5">forts</w>, <w n="3.6">Dieu</w> <w n="3.7">l</w>'<w n="3.8">avait</w> <w n="3.9">résolu</w>…</l>
					<l n="4" num="1.4"><w n="4.1">Être</w> <w n="4.2">les</w> <w n="4.3">plus</w> <w n="4.4">cléments</w> <w n="4.5">ils</w> <w n="4.6">ne</w> <w n="4.7">l</w>'<w n="4.8">ont</w> <w n="4.9">pas</w> <w n="4.10">voulu</w>…</l>
					<l n="5" num="1.5"><w n="5.1">Tant</w> <w n="5.2">mieux</w> ! <w n="5.3">l</w>'<w n="5.4">occasion</w> <w n="5.5">était</w> <w n="5.6">pourtant</w> <w n="5.7">bien</w> <w n="5.8">belle</w></l>
					<l n="6" num="1.6"><w n="6.1">Pour</w> <w n="6.2">nous</w> <w n="6.3">humilier</w> <w n="6.4">d</w>'<w n="6.5">une</w> <w n="6.6">façon</w> <w n="6.7">cruelle</w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">L</w>'<w n="7.2">Allemand</w> <w n="7.3">qui</w> <w n="7.4">nous</w> <w n="7.5">tient</w> <w n="7.6">abattus</w> <w n="7.7">sous</w> <w n="7.8">son</w> <w n="7.9">pié</w></l>
					<l n="8" num="2.2"><w n="8.1">Pouvait</w> <w n="8.2">nous</w> <w n="8.3">infliger</w> <w n="8.4">encore</w> <w n="8.5">sa</w> <w n="8.6">pitié</w></l>
					<l n="9" num="2.3"><w n="9.1">Comme</w> <w n="9.2">un</w> <w n="9.3">dernier</w> <w n="9.4">affront</w> <w n="9.5">au</w> <w n="9.6">malheur</w>, <w n="9.7">au</w> <w n="9.8">courage</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">pouvait</w> <w n="10.3">dire</w>, <w n="10.4">ainsi</w> <w n="10.5">couronnant</w> <w n="10.6">son</w> <w n="10.7">ouvrage</w> :</l>
					<l n="11" num="2.5">— <w n="11.1">Aux</w> <w n="11.2">Parisiens</w> <w n="11.3">bruyants</w>, <w n="11.4">aux</w> <w n="11.5">Français</w> <w n="11.6">fanfarons</w></l>
					<l n="12" num="2.6"><w n="12.1">Nous</w> <w n="12.2">pouvons</w> <w n="12.3">imposer</w> <w n="12.4">tout</w> <w n="12.5">ce</w> <w n="12.6">que</w> <w n="12.7">nous</w> <w n="12.8">voudrons</w> !</l>
					<l n="13" num="2.7"><w n="13.1">Nous</w> <w n="13.2">sommes</w> <w n="13.3">les</w> <w n="13.4">vainqueurs</w>, <w n="13.5">les</w> <w n="13.6">maîtres</w>, <w n="13.7">les</w> <w n="13.8">arbitres</w>,</l>
					<l n="14" num="2.8"><w n="14.1">A</w> <w n="14.2">nos</w> <w n="14.3">ménagements</w> <w n="14.4">vous</w> <w n="14.5">n</w>'<w n="14.6">avez</w> <w n="14.7">plus</w> <w n="14.8">de</w> <w n="14.9">titres</w>,</l>
					<l n="15" num="2.9"><w n="15.1">Mais</w> <w n="15.2">nous</w> <w n="15.3">vous</w> <w n="15.4">épargnons</w>, <w n="15.5">nous</w> <w n="15.6">sommes</w> <w n="15.7">généreux</w></l>
					<l n="16" num="2.10"><w n="16.1">Vous</w> <w n="16.2">trouvant</w> <w n="16.3">plus</w> <w n="16.4">à</w> <w n="16.5">plaindre</w> <w n="16.6">étant</w> <w n="16.7">plus</w> <w n="16.8">malheureux</w> !</l>
					<l n="17" num="2.11"><w n="17.1">Nous</w> <w n="17.2">ne</w> <w n="17.3">prétendons</w> <w n="17.4">point</w> <w n="17.5">à</w> <w n="17.6">l</w>'<w n="17.7">abus</w> <w n="17.8">de</w> <w n="17.9">la</w> <w n="17.10">gloire</w></l>
					<l n="18" num="2.12"><w n="18.1">Et</w> <w n="18.2">respectons</w> <w n="18.3">en</w> <w n="18.4">vous</w> <w n="18.5">jusqu</w>'<w n="18.6">à</w> <w n="18.7">notre</w> <w n="18.8">victoire</w> ! —</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1"><w n="19.1">S</w>'<w n="19.2">ils</w> <w n="19.3">avaient</w> <w n="19.4">fait</w> <w n="19.5">cela</w> ! <w n="19.6">s</w>'<w n="19.7">ils</w> <w n="19.8">avaient</w>, <w n="19.9">ô</w> <w n="19.10">douleur</w> !</l>
					<l n="20" num="3.2"><w n="20.1">Des</w> <w n="20.2">clauses</w> <w n="20.3">de</w> <w n="20.4">la</w> <w n="20.5">paix</w> <w n="20.6">adouci</w> <w n="20.7">la</w> <w n="20.8">rigueur</w> ;</l>
					<l n="21" num="3.3"><w n="21.1">S</w>'<w n="21.2">ils</w> <w n="21.3">avaient</w>, <w n="21.4">oublieux</w> <w n="21.5">des</w> <w n="21.6">querelles</w> <w n="21.7">passées</w>,</l>
					<l n="22" num="3.4"><w n="22.1">D</w>'<w n="22.2">un</w> <w n="22.3">oubli</w> <w n="22.4">réciproque</w> <w n="22.5">évoqué</w> <w n="22.6">les</w> <w n="22.7">pensées</w></l>
					<l n="23" num="3.5"><w n="23.1">Et</w> <w n="23.2">surtout</w> <w n="23.3">refusé</w> <w n="23.4">d</w>'<w n="23.5">envahir</w> <w n="23.6">ce</w> <w n="23.7">Paris</w></l>
					<l n="24" num="3.6"><w n="24.1">Que</w> <w n="24.2">sans</w> <w n="24.3">la</w> <w n="24.4">faim</w> <w n="24.5">jamais</w> <w n="24.6">Guillaume</w> <w n="24.7">n</w>'<w n="24.8">aurait</w> <w n="24.9">pris</w> ;</l>
					<l n="25" num="3.7"><w n="25.1">La</w> <w n="25.2">France</w> <w n="25.3">était</w> <w n="25.4">alors</w> <w n="25.5">vaincue</w> <w n="25.6">et</w> <w n="25.7">désarmée</w></l>
					<l n="26" num="3.8"><w n="26.1">Plus</w> <w n="26.2">que</w> <w n="26.3">par</w> <w n="26.4">tous</w> <w n="26.5">les</w> <w n="26.6">Krupp</w> <w n="26.7">de</w> <w n="26.8">leur</w> <w n="26.9">pesante</w> <w n="26.10">armée</w>,</l>
					<l n="27" num="3.9"><w n="27.1">Car</w> <w n="27.2">la</w> <w n="27.3">reconnaissance</w> <w n="27.4">a</w> <w n="27.5">de</w> <w n="27.6">secrets</w> <w n="27.7">liens</w></l>
					<l n="28" num="3.10"><w n="28.1">Qu</w>'<w n="28.2">on</w> <w n="28.3">ne</w> <w n="28.4">saurait</w> <w n="28.5">briser</w> <w n="28.6">même</w> <w n="28.7">envers</w> <w n="28.8">des</w> <w n="28.9">Prussiens</w> !</l>
				</lg>
				<lg n="4">
					<l n="29" num="4.1"><w n="29.1">Mais</w>, <w n="29.2">ils</w> <w n="29.3">ne</w> <w n="29.4">l</w>'<w n="29.5">ont</w> <w n="29.6">pas</w> <w n="29.7">fait</w> ! <w n="29.8">affolés</w> <w n="29.9">de</w> <w n="29.10">conquête</w>,</l>
					<l n="30" num="4.2"><w n="30.1">Ils</w> <w n="30.2">poussent</w> <w n="30.3">jusqu</w>'<w n="30.4">au</w> <w n="30.5">bout</w> <w n="30.6">leur</w> <w n="30.7">rapacité</w> <w n="30.8">bête</w>…</l>
					<l n="31" num="4.3"><w n="31.1">Ah</w> ! <w n="31.2">tant</w> <w n="31.3">mieux</w> ! <w n="31.4">mille</w> <w n="31.5">fois</w>, <w n="31.6">tant</w> <w n="31.7">mieux</w> !… <w n="31.8">rien</w> <w n="31.9">ne</w> <w n="31.10">pourra</w></l>
					<l n="32" num="4.4"><w n="32.1">Nous</w> <w n="32.2">désarmer</w> <w n="32.3">non</w> <w n="32.4">plus</w> <w n="32.5">et</w> <w n="32.6">l</w>'<w n="32.7">Europe</w> <w n="32.8">verra</w></l>
					<l n="33" num="4.5"><w n="33.1">Notre</w> <w n="33.2">haine</w> <w n="33.3">implacable</w> <w n="33.4">à</w> <w n="33.5">punir</w> <w n="33.6">cette</w> <w n="33.7">engeance</w></l>
					<l n="34" num="4.6"><w n="34.1">A</w> <w n="34.2">nos</w> <w n="34.3">ressentiments</w> <w n="34.4">mesurer</w> <w n="34.5">sa</w> <w n="34.6">vengeance</w> !…</l>
				</lg>
			</div></body></text></TEI>