<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG17">
				<head type="main">L'ODEUR ALLEMANDE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Pillant</w>, <w n="1.2">brûlant</w> <w n="1.3">selon</w> <w n="1.4">l</w>'<w n="1.5">usage</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">Allemands</w> <w n="2.3">ont</w> <w n="2.4">passé</w> <w n="2.5">là</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">jamais</w> <w n="3.3">les</w> <w n="3.4">Huns</w> <w n="3.5">d</w>'<w n="3.6">Attila</w></l>
					<l n="4" num="1.4"><w n="4.1">N</w>'<w n="4.2">ont</w> <w n="4.3">mieux</w> <w n="4.4">fait</w> <w n="4.5">sentir</w> <w n="4.6">leur</w> <w n="4.7">passage</w></l>
					<l n="5" num="1.5"><w n="5.1">Que</w> <w n="5.2">ces</w> <w n="5.3">Badois</w>, <w n="5.4">Mecklembourgeois</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Hessois</w>, <w n="6.2">tous</w> <w n="6.3">noms</w> <w n="6.4">rimant</w> <w n="6.5">en</w> <w n="6.6">ois</w> ;</l>
					<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Ceux</w> <w n="7.2">qu</w>'<w n="7.3">on</w> <w n="7.4">appelle</w></l>
					<l n="8" num="1.8"><w n="8.1">Silésiens</w> <w n="8.2">et</w> <w n="8.3">Poméraniens</w>,</l>
					<l n="9" num="1.9"><w n="9.1">D</w>'<w n="9.2">autres</w> <w n="9.3">encor</w> <w n="9.4">rimant</w> <w n="9.5">à</w> <w n="9.6">chiens</w> ;</l>
					<l n="10" num="1.10"><w n="10.1">En</w> <w n="10.2">un</w> <w n="10.3">mot</w> <w n="10.4">toute</w> <w n="10.5">la</w> <w n="10.6">séquelle</w></l>
					<l n="11" num="1.11"><space unit="char" quantity="10"></space><w n="11.1">Par</w> <w n="11.2">laquelle</w></l>
					<l n="12" num="1.12"><w n="12.1">La</w> <w n="12.2">terre</w> <w n="12.3">et</w> <w n="12.4">l</w>'<w n="12.5">air</w> <w n="12.6">sont</w> <w n="12.7">corrompus</w>…</l>
					<l n="13" num="1.13"><w n="13.1">Qu</w>'<w n="13.2">on</w> <w n="13.3">fasse</w> <w n="13.4">rougir</w> <w n="13.5">une</w> <w n="13.6">pelle</w></l>
					<l n="14" num="1.14"><w n="14.1">Pour</w> <w n="14.2">brûler</w> <w n="14.3">du</w> <w n="14.4">sucre</w> <w n="14.5">dessus</w> !…</l>
				</lg>
				<lg n="2">
					<l n="15" num="2.1"><w n="15.1">C</w>'<w n="15.2">est</w> <w n="15.3">que</w> <w n="15.4">les</w> <w n="15.5">peuplades</w> <w n="15.6">vassales</w></l>
					<l n="16" num="2.2"><w n="16.1">Que</w> <w n="16.2">Guillaume</w> <w n="16.3">attache</w> <w n="16.4">à</w> <w n="16.5">ses</w> <w n="16.6">pas</w></l>
					<l n="17" num="2.3"><w n="17.1">D</w>'<w n="17.2">eau</w> <w n="17.3">propre</w> <w n="17.4">ne</w> <w n="17.5">se</w> <w n="17.6">servent</w> <w n="17.7">pas</w>…</l>
					<l n="18" num="2.4"><w n="18.1">Elles</w> <w n="18.2">sont</w> <w n="18.3">cruelles</w> <w n="18.4">mais</w> <w n="18.5">sales</w>…</l>
					<l n="19" num="2.5"><w n="19.1">Et</w> <w n="19.2">la</w> <w n="19.3">punaise</w> <w n="19.4">entre</w> <w n="19.5">les</w> <w n="19.6">draps</w>,</l>
					<l n="20" num="2.6"><w n="20.1">La</w> <w n="20.2">sueur</w> <w n="20.3">ignoble</w> <w n="20.4">des</w> <w n="20.5">bras</w>,</l>
					<l n="21" num="2.7"><space unit="char" quantity="8"></space><w n="21.1">Dalle</w> <w n="21.2">à</w> <w n="21.3">vaisselle</w>,</l>
					<l n="22" num="2.8"><w n="22.1">Gueule</w> <w n="22.2">d</w>'<w n="22.3">égout</w>, <w n="22.4">tas</w> <w n="22.5">de</w> <w n="22.6">fumier</w></l>
					<l n="23" num="2.9"><w n="23.1">Et</w> <w n="23.2">vieux</w> <w n="23.3">linges</w> <w n="23.4">de</w> <w n="23.5">l</w>'<w n="23.6">infirmier</w></l>
					<l n="24" num="2.10"><w n="24.1">N</w>'<w n="24.2">ont</w> <w n="24.3">pas</w> <w n="24.4">une</w> <w n="24.5">odeur</w> <w n="24.6">comme</w> <w n="24.7">celle</w></l>
					<l n="25" num="2.11"><space unit="char" quantity="10"></space><w n="25.1">Que</w> <w n="25.2">recèle</w></l>
					<l n="26" num="2.12"><w n="26.1">Cette</w> <w n="26.2">engeance</w> <w n="26.3">aux</w> <w n="26.4">crânes</w> <w n="26.5">obtus</w>…</l>
					<l n="27" num="2.13"><w n="27.1">Qu</w>'<w n="27.2">on</w> <w n="27.3">fasse</w> <w n="27.4">rougir</w> <w n="27.5">une</w> <w n="27.6">pelle</w></l>
					<l n="28" num="2.14"><w n="28.1">Pour</w> <w n="28.2">brûler</w> <w n="28.3">du</w> <w n="28.4">sucre</w> <w n="28.5">dessus</w> !…</l>
				</lg>
				<lg n="3">
					<l n="29" num="3.1"><w n="29.1">C</w>'<w n="29.2">est</w> <w n="29.3">une</w> <w n="29.4">odeur</w> <w n="29.5">particulière</w></l>
					<l n="30" num="3.2"><w n="30.1">A</w> <w n="30.2">cette</w> <w n="30.3">espèce</w> <w n="30.4">de</w> <w n="30.5">bétail</w> :</l>
					<l n="31" num="3.3"><w n="31.1">Je</w> <w n="31.2">n</w>'<w n="31.3">entreprends</w> <w n="31.4">point</w> <w n="31.5">le</w> <w n="31.6">travail</w></l>
					<l n="32" num="3.4"><w n="32.1">De</w> <w n="32.2">la</w> <w n="32.3">dépeindre</w> <w n="32.4">tout</w> <w n="32.5">entière</w> ;</l>
					<l n="33" num="3.5"><w n="33.1">Villon</w> <w n="33.2">et</w> <w n="33.3">Marot</w>, <w n="33.4">ces</w> <w n="33.5">Gaulois</w>,</l>
					<l n="34" num="3.6"><w n="34.1">Et</w> <w n="34.2">maître</w> <w n="34.3">Rabelais</w>, <w n="34.4">tous</w> <w n="34.5">trois</w>,</l>
					<l n="35" num="3.7"><space unit="char" quantity="8"></space><w n="35.1">Dans</w> <w n="35.2">leur</w> <w n="35.3">cervelle</w></l>
					<l n="36" num="3.8"><w n="36.1">Ne</w> <w n="36.2">trouveraient</w> <w n="36.3">pas</w> <w n="36.4">de</w> <w n="36.5">dicton</w></l>
					<l n="37" num="3.9"><w n="37.1">Pour</w> <w n="37.2">définir</w> <w n="37.3">l</w>'<w n="37.4">affreux</w> <w n="37.5">poison</w> ;</l>
					<l n="38" num="3.10"><w n="38.1">Gargantua</w> <w n="38.2">même</w> <w n="38.3">et</w> <w n="38.4">sa</w> <w n="38.5">fidelle</w></l>
					<l n="39" num="3.11"><space unit="char" quantity="10"></space><w n="39.1">Gargamelle</w></l>
					<l n="40" num="3.12"><w n="40.1">N</w>'<w n="40.2">auraient</w> <w n="40.3">pas</w> <w n="40.4">de</w> <w n="40.5">mots</w> <w n="40.6">assez</w> <w n="40.7">crûs</w>…</l>
					<l n="41" num="3.13"><w n="41.1">Qu</w>'<w n="41.2">on</w> <w n="41.3">fasse</w> <w n="41.4">rougir</w> <w n="41.5">une</w> <w n="41.6">pelle</w></l>
					<l n="42" num="3.14"><w n="42.1">Pour</w> <w n="42.2">brûler</w> <w n="42.3">du</w> <w n="42.4">sucre</w> <w n="42.5">dessus</w> !…</l>
				</lg>
				<lg n="4">
					<l n="43" num="4.1"><w n="43.1">Mais</w> <w n="43.2">ce</w> <w n="43.3">qui</w> <w n="43.4">dans</w> <w n="43.5">l</w>'<w n="43.6">ignoble</w> <w n="43.7">guerre</w></l>
					<l n="44" num="4.2"><w n="44.1">Au</w> <w n="44.2">dégoût</w> <w n="44.3">ajoute</w> <w n="44.4">l</w>'<w n="44.5">horreur</w>,</l>
					<l n="45" num="4.3"><w n="45.1">Ce</w> <w n="45.2">qui</w> <w n="45.3">nous</w> <w n="45.4">soulève</w> <w n="45.5">le</w> <w n="45.6">cœur</w></l>
					<l n="46" num="4.4"><w n="46.1">Plus</w> <w n="46.2">que</w> <w n="46.3">la</w> <w n="46.4">puanteur</w> <w n="46.5">vulgaire</w></l>
					<l n="47" num="4.5"><w n="47.1">Qu</w>'<w n="47.2">exhalent</w> <w n="47.3">ces</w> <w n="47.4">gueux</w> <w n="47.5">en</w> <w n="47.6">passant</w>,</l>
					<l n="48" num="4.6"><w n="48.1">C</w>'<w n="48.2">est</w> <w n="48.3">que</w> <w n="48.4">l</w>'<w n="48.5">affreuse</w> <w n="48.6">odeur</w> <w n="48.7">du</w> <w n="48.8">sang</w></l>
					<l n="49" num="4.7"><space unit="char" quantity="8"></space><w n="49.1">Toujours</w> <w n="49.2">s</w>'<w n="49.3">y</w> <w n="49.4">mêle</w></l>
					<l n="50" num="4.8"><w n="50.1">A</w> <w n="50.2">l</w>'<w n="50.3">acre</w> <w n="50.4">senteur</w> <w n="50.5">des</w> <w n="50.6">brasiers</w>,</l>
					<l n="51" num="4.9"><w n="51.1">Débris</w> <w n="51.2">de</w> <w n="51.3">villages</w> <w n="51.4">entiers</w></l>
					<l n="52" num="4.10"><w n="52.1">Fumants</w> <w n="52.2">sous</w> <w n="52.3">leur</w> <w n="52.4">main</w> <w n="52.5">criminelle</w> !</l>
					<l n="53" num="4.11"><space unit="char" quantity="8"></space><w n="53.1">Haine</w> <w n="53.2">éternelle</w></l>
					<l n="54" num="4.12"><w n="54.1">Aux</w> <w n="54.2">Allemands</w> <w n="54.3">qui</w> <w n="54.4">sont</w> <w n="54.5">venus</w> !…</l>
					<l n="55" num="4.13"><w n="55.1">Qu</w>'<w n="55.2">on</w> <w n="55.3">fasse</w> <w n="55.4">rougir</w> <w n="55.5">une</w> <w n="55.6">pelle</w></l>
					<l n="56" num="4.14"><w n="56.1">Pour</w> <w n="56.2">brûler</w> <w n="56.3">du</w> <w n="56.4">sucre</w> <w n="56.5">dessus</w> !…</l>
				</lg>
			</div></body></text></TEI>