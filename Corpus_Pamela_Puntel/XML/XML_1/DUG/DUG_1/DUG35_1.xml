<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG35">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ont</w>-<w n="1.2">ils</w> <w n="1.3">ri</w>, <w n="1.4">les</w> <w n="1.5">coquins</w>, <w n="1.6">se</w> <w n="1.7">sont</w>-<w n="1.8">ils</w> <w n="1.9">pâmés</w> <w n="1.10">d</w>'<w n="1.11">aise</w></l>
					<l n="2" num="1.2"><w n="2.1">Tant</w> <w n="2.2">qu</w>'<w n="2.3">a</w> <w n="2.4">duré</w> <w n="2.5">le</w> <w n="2.6">choc</w> <w n="2.7">de</w> <w n="2.8">la</w> <w n="2.9">race</w> <w n="2.10">française</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Derrière</w> <w n="3.2">leurs</w> <w n="3.3">canons</w> <w n="3.4">sur</w> <w n="3.5">nous</w> <w n="3.6">toujours</w> <w n="3.7">braqués</w>,</l>
					<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">nos</w> <w n="4.3">déchirements</w> <w n="4.4">comme</w> <w n="4.5">ils</w> <w n="4.6">se</w> <w n="4.7">sont</w> <w n="4.8">moqués</w> !…</l>
					<l n="5" num="1.5"><w n="5.1">Nos</w> <w n="5.2">bons</w> <w n="5.3">amis</w> <w n="5.4">d</w>'<w n="5.5">Europe</w> <w n="5.6">et</w> <w n="5.7">même</w> <w n="5.8">d</w>'<w n="5.9">Amérique</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Voyant</w> <w n="6.2">l</w>'<w n="6.3">effondrement</w> <w n="6.4">de</w> <w n="6.5">cette</w> <w n="6.6">république</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Faisaient</w> <w n="7.2">chorus</w>, <w n="7.3">criant</w> : — <w n="7.4">C</w>'<w n="7.5">est</w> <w n="7.6">un</w> <w n="7.7">peuple</w> <w n="7.8">perdu</w> !</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">pour</w> <w n="8.3">le</w> <w n="8.4">démembrer</w> <w n="8.5">on</w> <w n="8.6">a</w> <w n="8.7">trop</w> <w n="8.8">attendu</w> !</l>
					<l n="9" num="1.9"><w n="9.1">Que</w> <w n="9.2">le</w> <w n="9.3">boucher</w> <w n="9.4">prussien</w> <w n="9.5">termine</w> <w n="9.6">sa</w> <w n="9.7">besogne</w></l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">coupe</w> <w n="10.3">par</w> <w n="10.4">morceaux</w> <w n="10.5">la</w> <w n="10.6">nouvelle</w> <w n="10.7">Pologne</w> !…</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1"><w n="11.1">Quel</w> <w n="11.2">orgueil</w> <w n="11.3">pour</w> <w n="11.4">Bismark</w>, <w n="11.5">ivre</w> <w n="11.6">de</w> <w n="11.7">nos</w> <w n="11.8">revers</w>,</l>
					<l n="12" num="2.2"><w n="12.1">Penché</w> <w n="12.2">comme</w> <w n="12.3">un</w> <w n="12.4">vautour</w> <w n="12.5">sur</w> <w n="12.6">nos</w> <w n="12.7">flancs</w> <w n="12.8">entr</w>'<w n="12.9">ouverts</w>,</l>
					<l n="13" num="2.3"><w n="13.1">D</w>'<w n="13.2">entendre</w> <w n="13.3">ainsi</w> <w n="13.4">râler</w> <w n="13.5">l</w>'<w n="13.6">ennemi</w> <w n="13.7">qu</w>'<w n="13.8">il</w> <w n="13.9">redoute</w>,</l>
					<l n="14" num="2.4"><w n="14.1">De</w> <w n="14.2">voir</w> <w n="14.3">tout</w> <w n="14.4">notre</w> <w n="14.5">sang</w> <w n="14.6">se</w> <w n="14.7">tarir</w> <w n="14.8">goutte</w> <w n="14.9">à</w> <w n="14.10">goutte</w>,</l>
					<l n="15" num="2.5"><w n="15.1">De</w> <w n="15.2">se</w> <w n="15.3">dire</w> : — <w n="15.4">Jetons</w> <w n="15.5">le</w> <w n="15.6">cadavre</w> <w n="15.7">au</w> <w n="15.8">cercueil</w>,</l>
					<l n="16" num="2.6"><w n="16.1">Qu</w>'<w n="16.2">on</w> <w n="16.3">chante</w> <w n="16.4">un</w> <w n="16.5">Requiem</w> <w n="16.6">et</w> <w n="16.7">qu</w>'<w n="16.8">on</w> <w n="16.9">prenne</w> <w n="16.10">le</w> <w n="16.11">deuil</w>…</l>
					<l n="17" num="2.7"><w n="17.1">Je</w> <w n="17.2">vais</w> <w n="17.3">pouvoir</w>, <w n="17.4">sans</w> <w n="17.5">craindre</w> <w n="17.6">une</w> <w n="17.7">attaque</w> <w n="17.8">prochaine</w>,</l>
					<l n="18" num="2.8"><w n="18.1">Germaniser</w> <w n="18.2">en</w> <w n="18.3">paix</w> <w n="18.4">l</w>'<w n="18.5">Alsace</w> <w n="18.6">et</w> <w n="18.7">la</w> <w n="18.8">Lorraine</w> ! —</l>
					<l n="19" num="2.9"><w n="19.1">Il</w> <w n="19.2">caressait</w> <w n="19.3">surtout</w> <w n="19.4">l</w>'<w n="19.5">espoir</w>, <w n="19.6">pour</w> <w n="19.7">l</w>'<w n="19.8">avenir</w>,</l>
					<l n="20" num="2.10"><w n="20.1">Que</w> <w n="20.2">de</w> <w n="20.3">l</w>'<w n="20.4">Invasion</w> <w n="20.5">perdant</w> <w n="20.6">le</w> <w n="20.7">souvenir</w>,</l>
					<l n="21" num="2.11"><w n="21.1">La</w> <w n="21.2">France</w>, <w n="21.3">tout</w> <w n="21.4">entière</w> <w n="21.5">à</w> <w n="21.6">ses</w> <w n="21.7">luttes</w> <w n="21.8">intimes</w>,</l>
					<l n="22" num="2.12"><w n="22.1">Des</w> <w n="22.2">hordes</w> <w n="22.3">de</w> <w n="22.4">Guillaume</w> <w n="22.5">oublierait</w> <w n="22.6">les</w> <w n="22.7">victimes</w>,</l>
					<l n="23" num="2.13"><w n="23.1">Que</w> <w n="23.2">la</w> <w n="23.3">haine</w> <w n="23.4">vouée</w> <w n="23.5">à</w> <w n="23.6">l</w>'<w n="23.7">atroce</w> <w n="23.8">vainqueur</w></l>
					<l n="24" num="2.14"><w n="24.1">S</w>'<w n="24.2">amoindrirait</w> <w n="24.3">peut</w>-<w n="24.4">être</w> <w n="24.5">au</w> <w n="24.6">fond</w> <w n="24.7">de</w> <w n="24.8">notre</w> <w n="24.9">cœur</w>,</l>
					<l n="25" num="2.15"><w n="25.1">Et</w> <w n="25.2">que</w> <w n="25.3">la</w> <w n="25.4">guérison</w> <w n="25.5">lente</w> <w n="25.6">de</w> <w n="25.7">nos</w> <w n="25.8">blessures</w></l>
					<l n="26" num="2.16"><w n="26.1">Rendrait</w>, <w n="26.2">les</w> <w n="26.3">ajournant</w>, <w n="26.4">nos</w> <w n="26.5">revanches</w> <w n="26.6">moins</w> <w n="26.7">sûres</w> !…</l>
				</lg>
				<lg n="3">
					<l n="27" num="3.1"><w n="27.1">Vous</w> <w n="27.2">vous</w> <w n="27.3">trompiez</w>, <w n="27.4">monsieur</w> <w n="27.5">le</w> <w n="27.6">prince</w> <w n="27.7">chancelier</w>,</l>
					<l n="28" num="3.2"><w n="28.1">Moins</w> <w n="28.2">que</w> <w n="28.3">jamais</w> <w n="28.4">la</w> <w n="28.5">France</w> <w n="28.6">a</w> <w n="28.7">le</w> <w n="28.8">droit</w> <w n="28.9">d</w>'<w n="28.10">oublier</w>,</l>
					<l n="29" num="3.3"><w n="29.1">Et</w> <w n="29.2">vous</w> <w n="29.3">nous</w> <w n="29.4">reparlez</w> <w n="29.5">trop</w> <w n="29.6">souvent</w> <w n="29.7">de</w> <w n="29.8">nos</w> <w n="29.9">dettes</w></l>
					<l n="30" num="3.4"><w n="30.1">Pour</w> <w n="30.2">qu</w>'<w n="30.3">on</w> <w n="30.4">ne</w> <w n="30.5">songe</w> <w n="30.6">plus</w> <w n="30.7">à</w> <w n="30.8">les</w> <w n="30.9">payer</w> <w n="30.10">complètes</w> !…</l>
					<l n="31" num="3.5"><w n="31.1">Celle</w> <w n="31.2">qu</w>'<w n="31.3">on</w> <w n="31.4">croyait</w> <w n="31.5">morte</w> <w n="31.6">est</w> <w n="31.7">bien</w> <w n="31.8">vivante</w> <w n="31.9">encor</w>,</l>
					<l n="32" num="3.6"><w n="32.1">Et</w> <w n="32.2">son</w> <w n="32.3">sang</w> <w n="32.4">n</w>'<w n="32.5">est</w> <w n="32.6">pas</w> <w n="32.7">plus</w> <w n="32.8">épuisé</w> <w n="32.9">que</w> <w n="32.10">son</w> <w n="32.11">or</w> !</l>
					<l n="33" num="3.7"><w n="33.1">Le</w> <w n="33.2">cygne</w>, <w n="33.3">qu</w>'<w n="33.4">ont</w> <w n="33.5">sali</w> <w n="33.6">quelques</w> <w n="33.7">taches</w> <w n="33.8">de</w> <w n="33.9">boue</w>,</l>
					<l n="34" num="3.8"><w n="34.1">Se</w> <w n="34.2">baigne</w> <w n="34.3">dans</w> <w n="34.4">l</w>'<w n="34.5">eau</w> <w n="34.6">pure</w>, <w n="34.7">y</w> <w n="34.8">plonge</w>, <w n="34.9">se</w> <w n="34.10">secoue</w>,</l>
					<l n="35" num="3.9"><w n="35.1">Puis</w> <w n="35.2">lisse</w> <w n="35.3">au</w> <w n="35.4">beau</w> <w n="35.5">soleil</w> <w n="35.6">son</w> <w n="35.7">plumage</w> <w n="35.8">argenté</w>…</l>
					<l n="36" num="3.10"><w n="36.1">Ainsi</w> <w n="36.2">fera</w> <w n="36.3">la</w> <w n="36.4">France</w>, <w n="36.5">et</w> <w n="36.6">dans</w> <w n="36.7">sa</w> <w n="36.8">liberté</w>,</l>
					<l n="37" num="3.11"><w n="37.1">Dans</w> <w n="37.2">sa</w> <w n="37.3">force</w>, <w n="37.4">émergeant</w> <w n="37.5">de</w> <w n="37.6">cette</w> <w n="37.7">nuit</w> <w n="37.8">profonde</w>,</l>
					<l n="38" num="3.12"><w n="38.1">Faite</w> <w n="38.2">pour</w> <w n="38.3">étonner</w> <w n="38.4">et</w> <w n="38.5">dominer</w> <w n="38.6">le</w> <w n="38.7">monde</w>,</l>
					<l n="39" num="3.13"><w n="39.1">Un</w> <w n="39.2">pied</w> <w n="39.3">sur</w> <w n="39.4">la</w> <w n="39.5">révolte</w> <w n="39.6">aux</w> <w n="39.7">emblèmes</w> <w n="39.8">hideux</w>,</l>
					<l n="40" num="3.14"><w n="40.1">Elle</w> <w n="40.2">dira</w> <w n="40.3">bientôt</w> : <w n="40.4">Allemagne</w>, <w n="40.5">à</w> <w n="40.6">nous</w> <w n="40.7">deux</w> !…</l>
					<l n="41" num="3.15"><w n="41.1">Va</w>, <w n="41.2">maintenant</w>, <w n="41.3">mon</w> <w n="41.4">livre</w> ! <w n="41.5">au</w> <w n="41.6">cœur</w> <w n="41.7">de</w> <w n="41.8">la</w> <w n="41.9">Patrie</w></l>
					<l n="42" num="3.16"><w n="42.1">Ranime</w> <w n="42.2">cette</w> <w n="42.3">haine</w> <w n="42.4">un</w> <w n="42.5">moment</w> <w n="42.6">engourdie</w> !</l>
					<l n="43" num="3.17"><w n="43.1">Va</w> ! <w n="43.2">sillonne</w> <w n="43.3">les</w> <w n="43.4">airs</w> <w n="43.5">de</w> <w n="43.6">tes</w> <w n="43.7">éclats</w> <w n="43.8">vengeurs</w> !</l>
					<l n="44" num="3.18"><w n="44.1">Atteins</w> <w n="44.2">jusque</w> <w n="44.3">chez</w> <w n="44.4">eux</w> <w n="44.5">nos</w> <w n="44.6">lâches</w> <w n="44.7">ravageurs</w> !</l>
					<l n="45" num="3.19"><w n="45.1">Va</w>, <w n="45.2">mon</w> <w n="45.3">livre</w> ! <w n="45.4">et</w> <w n="45.5">sonnant</w> <w n="45.6">le</w> <w n="45.7">réveil</w> <w n="45.8">des</w> <w n="45.9">batailles</w>,</l>
					<l n="46" num="3.20"><w n="46.1">Prépare</w> <w n="46.2">notre</w> <w n="46.3">France</w> <w n="46.4">aux</w> <w n="46.5">grandes</w> <w n="46.6">représailles</w> !</l>
				</lg>
			</div></body></text></TEI>