<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG28">
				<head type="main">LES LIÈVRES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Oui</w>, <w n="1.2">ce</w> <w n="1.3">sera</w> <w n="1.4">la</w> <w n="1.5">paix</w> <w n="1.6">aux</w> <w n="1.7">conséquences</w> <w n="1.8">viles</w>,</l>
					<l n="2" num="1.2"><w n="2.1">L</w>'<w n="2.2">odieux</w> <w n="2.3">sacrifice</w> <w n="2.4">enfin</w> <w n="2.5">va</w> <w n="2.6">s</w>'<w n="2.7">accomplir</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">campagnes</w> <w n="3.3">n</w>'<w n="3.4">ont</w> <w n="3.5">pas</w> <w n="3.6">à</w> <w n="3.7">jalouser</w> <w n="3.8">les</w> <w n="3.9">villes</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Car</w>, <w n="4.2">pour</w> <w n="4.3">rivaliser</w> <w n="4.4">de</w> <w n="4.5">lâchetés</w> <w n="4.6">serviles</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Le</w> <w n="5.2">scrutin</w> <w n="5.3">va</w> <w n="5.4">les</w> <w n="5.5">réunir</w> !…</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Et</w> <w n="6.2">respect</w>, <w n="6.3">nous</w> <w n="6.4">dit</w>-<w n="6.5">on</w>, <w n="6.6">et</w> <w n="6.7">silence</w> <w n="6.8">à</w> <w n="6.9">l</w>'<w n="6.10">oracle</w> !</l>
					<l n="7" num="2.2"><w n="7.1">Il</w> <w n="7.2">doit</w> <w n="7.3">faire</w> <w n="7.4">fléchir</w> <w n="7.5">le</w> <w n="7.6">front</w> <w n="7.7">le</w> <w n="7.8">plus</w> <w n="7.9">altier</w> !</l>
					<l n="8" num="2.3"><w n="8.1">Du</w> <w n="8.2">vote</w> <w n="8.3">universel</w> <w n="8.4">c</w>'<w n="8.5">est</w> <w n="8.6">un</w> <w n="8.7">nouveau</w> <w n="8.8">miracle</w>,'</l>
					<l n="9" num="2.4"><w n="9.1">Et</w> <w n="9.2">cette</w> <w n="9.3">élection</w> <w n="9.4">qu</w>'<w n="9.5">avec</w> <w n="9.6">Bismark</w> <w n="9.7">on</w> <w n="9.8">bâcle</w>,</l>
					<l n="10" num="2.5"><space unit="char" quantity="8"></space><w n="10.1">C</w>'<w n="10.2">est</w> <w n="10.3">la</w> <w n="10.4">voix</w> <w n="10.5">du</w> <w n="10.6">pays</w> <w n="10.7">entier</w> !…</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">—<w n="11.1">S</w>'<w n="11.2">il</w> <w n="11.3">nous</w> <w n="11.4">plaît</w>, <w n="11.5">après</w> <w n="11.6">tout</w>, <w n="11.7">d</w>'<w n="11.8">être</w> <w n="11.9">lâches</w> <w n="11.10">quand</w> <w n="11.11">même</w>,</l>
					<l n="12" num="3.2"><w n="12.1">De</w> <w n="12.2">sauver</w> <w n="12.3">le</w> <w n="12.4">pays</w> <w n="12.5">par</w> <w n="12.6">son</w> <w n="12.7">abaissement</w>,</l>
					<l n="13" num="3.3"><w n="13.1">N</w>'<w n="13.2">est</w>-<w n="13.3">ce</w> <w n="13.4">pas</w> <w n="13.5">notre</w> <w n="13.6">droit</w> <w n="13.7">absolu</w>, <w n="13.8">droit</w> <w n="13.9">suprême</w> ?</l>
					<l n="14" num="3.4"><w n="14.1">Toute</w> <w n="14.2">opposition</w> <w n="14.3">est</w> <w n="14.4">un</w> <w n="14.5">crime</w>, <w n="14.6">un</w> <w n="14.7">blasphème</w>,</l>
					<l n="15" num="3.5"><space unit="char" quantity="8"></space><w n="15.1">Et</w> <w n="15.2">qui</w> <w n="15.3">dit</w> <w n="15.4">le</w> <w n="15.5">contraire</w> <w n="15.6">ment</w> !…</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">Silence</w> <w n="16.2">donc</w>, <w n="16.3">mauvais</w> <w n="16.4">citoyens</w> ! <w n="16.5">fous</w> ! <w n="16.6">rebelles</w> !</l>
					<l n="17" num="4.2"><w n="17.1">Bien</w> <w n="17.2">plus</w> <w n="17.3">que</w> <w n="17.4">les</w> <w n="17.5">Prussiens</w>, <w n="17.6">c</w>'<w n="17.7">est</w> <w n="17.8">vous</w> <w n="17.9">qu</w>'<w n="17.10">il</w> <w n="17.11">faut</w> <w n="17.12">haïr</w>,</l>
					<l n="18" num="4.3"><w n="18.1">Et</w> <w n="18.2">nos</w> <w n="18.3">mobilisés</w>, <w n="18.4">à</w> <w n="18.5">leur</w> <w n="18.6">devoir</w> <w n="18.7">fidelles</w>,</l>
					<l n="19" num="4.4"><w n="19.1">Vous</w> <w n="19.2">surveillent</w> <w n="19.3">de</w> <w n="19.4">près</w>… <w n="19.5">vous</w> <w n="19.6">en</w> <w n="19.7">verrez</w> <w n="19.8">de</w> <w n="19.9">belles</w></l>
					<l n="20" num="4.5"><space unit="char" quantity="8"></space><w n="20.1">Si</w> <w n="20.2">vous</w> <w n="20.3">osez</w> <w n="20.4">désobéir</w> !…</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">Ne</w> <w n="21.2">vous</w> <w n="21.3">avisez</w> <w n="21.4">point</w> <w n="21.5">d</w>'<w n="21.6">exciter</w> <w n="21.7">notre</w> <w n="21.8">rage</w></l>
					<l n="22" num="5.2"><w n="22.1">En</w> <w n="22.2">critiquant</w> <w n="22.3">la</w> <w n="22.4">paix</w> <w n="22.5">que</w> <w n="22.6">nous</w> <w n="22.7">désirons</w> <w n="22.8">tous</w> !</l>
					<l n="23" num="5.3"><w n="23.1">Pour</w> <w n="23.2">combattre</w> <w n="23.3">le</w> <w n="23.4">vol</w>, <w n="23.5">le</w> <w n="23.6">meurtre</w> <w n="23.7">et</w> <w n="23.8">le</w> <w n="23.9">pillage</w>,</l>
					<l n="24" num="5.4"><w n="24.1">Nous</w> <w n="24.2">avons</w> <w n="24.3">pu</w> <w n="24.4">manquer</w> <w n="24.5">très</w>-<w n="24.6">souvent</w> <w n="24.7">de</w> <w n="24.8">courage</w>…</l>
					<l n="25" num="5.5"><space unit="char" quantity="8"></space><w n="25.1">Mais</w> <w n="25.2">nous</w> <w n="25.3">en</w> <w n="25.4">aurons</w> <w n="25.5">contre</w> <w n="25.6">vous</w> !</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">Dans</w> <w n="26.2">le</w> <w n="26.3">nombre</w> <w n="26.4">aujourd</w>'<w n="26.5">hui</w> <w n="26.6">réside</w> <w n="26.7">la</w> <w n="26.8">puissance</w>,</l>
					<l n="27" num="6.2"><w n="27.1">Force</w> <w n="27.2">est</w> <w n="27.3">de</w> <w n="27.4">trouver</w> <w n="27.5">bon</w> <w n="27.6">ce</w> <w n="27.7">que</w> <w n="27.8">le</w> <w n="27.9">nombre</w> <w n="27.10">fait</w>,</l>
					<l n="28" num="6.3"><w n="28.1">Car</w> <w n="28.2">des</w> <w n="28.3">minorités</w> <w n="28.4">défiant</w> <w n="28.5">l</w>'<w n="28.6">arrogance</w>,</l>
					<l n="29" num="6.4"><w n="29.1">Nous</w> <w n="29.2">sommes</w> <w n="29.3">la</w> <w n="29.4">pensée</w> <w n="29.5">et</w> <w n="29.6">la</w> <w n="29.7">voix</w> <w n="29.8">de</w> <w n="29.9">la</w> <w n="29.10">France</w> !</l>
					<l n="30" num="6.5"><space unit="char" quantity="8"></space>— <w n="30.1">Eh</w> ! <w n="30.2">oui</w>, <w n="30.3">c</w>'<w n="30.4">est</w> <w n="30.5">la</w> <w n="30.6">France</w>, <w n="30.7">en</w> <w n="30.8">effet</w> !…</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><w n="31.1">Restons</w> <w n="31.2">donc</w> <w n="31.3">à</w> <w n="31.4">l</w>'<w n="31.5">écart</w>, <w n="31.6">le</w> <w n="31.7">dédain</w> <w n="31.8">sur</w> <w n="31.9">les</w> <w n="31.10">lèvres</w>,</l>
					<l n="32" num="7.2"><w n="32.1">Et</w>, <w n="32.2">sans</w> <w n="32.3">même</w> <w n="32.4">songer</w> <w n="32.5">à</w> <w n="32.6">des</w> <w n="32.7">rébellions</w>,</l>
					<l n="33" num="7.3"><w n="33.1">Avec</w> <w n="33.2">le</w> <w n="33.3">grand</w> <w n="33.4">poëte</w> <w n="33.5">aux</w> <w n="33.6">accents</w> <w n="33.7">pleins</w> <w n="33.8">de</w> <w n="33.9">fièvres</w>,</l>
					<l n="34" num="7.4"><w n="34.1">Disons</w> <w n="34.2">amèrement</w> <w n="34.3">que</w> <w n="34.4">la</w> <w n="34.5">race</w> <w n="34.6">des</w> <w n="34.7">lièvres</w></l>
					<l n="35" num="7.5"><space unit="char" quantity="8"></space><w n="35.1">Succède</w> <w n="35.2">à</w> <w n="35.3">celle</w> <w n="35.4">des</w> <w n="35.5">lions</w> !</l>
				</lg>
			</div></body></text></TEI>