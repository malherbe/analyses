<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG8">
				<head type="main">LE HULAN</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Dieu</w> <w n="1.2">nous</w> <w n="1.3">a</w> <w n="1.4">repris</w> <w n="1.5">notre</w> <w n="1.6">mère</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">quand</w> <w n="2.3">papa</w> <w n="2.4">travaille</w> <w n="2.5">aux</w> <w n="2.6">bois</w></l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">reste</w> <w n="3.3">seule</w> <w n="3.4">à</w> <w n="3.5">la</w> <w n="3.6">chaumière</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">je</w> <w n="4.3">berce</w> <w n="4.4">mon</w> <w n="4.5">petit</w> <w n="4.6">frère</w>…</l>
					<l n="5" num="1.5"><w n="5.1">Arrive</w> <w n="5.2">un</w> <w n="5.3">soldat</w> <w n="5.4">bavarois</w></l>
					<l n="6" num="1.6"><w n="6.1">Armé</w> <w n="6.2">d</w>'<w n="6.3">un</w> <w n="6.4">sabre</w> <w n="6.5">et</w> <w n="6.6">d</w>'<w n="6.7">une</w> <w n="6.8">lance</w>…</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">J</w>'<w n="7.2">ai</w> <w n="7.3">peur</w>, <w n="7.4">je</w> <w n="7.5">veux</w> <w n="7.6">crier</w>… <w n="7.7">Silence</w> !</l>
					<l n="8" num="2.2"><w n="8.1">Dit</w> <w n="8.2">l</w>'<w n="8.3">étranger</w> <w n="8.4">d</w>'<w n="8.5">un</w> <w n="8.6">ton</w> <w n="8.7">brutal</w>…</l>
					<l n="9" num="2.3"><w n="9.1">Mais</w> <w n="9.2">loin</w> <w n="9.3">de</w> <w n="9.4">me</w> <w n="9.5">faire</w> <w n="9.6">aucun</w> <w n="9.7">mal</w>.</l>
					<l n="10" num="2.4"><w n="10.1">Il</w> <w n="10.2">jette</w> <w n="10.3">dans</w> <w n="10.4">un</w> <w n="10.5">coin</w> <w n="10.6">ses</w> <w n="10.7">armes</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Court</w> <w n="11.2">au</w> <w n="11.3">berceau</w>, <w n="11.4">de</w> <w n="11.5">grosses</w> <w n="11.6">larmes</w></l>
					<l n="12" num="2.6"><w n="12.1">Tombent</w> <w n="12.2">de</w> <w n="12.3">ses</w> <w n="12.4">yeux</w> <w n="12.5">attendris</w>…</l>
					<l n="13" num="2.7">— <w n="13.1">A</w> <w n="13.2">mon</w> <w n="13.3">Karl</w> <w n="13.4">cet</w> <w n="13.5">enfant</w> <w n="13.6">ressemble</w>,</l>
					<l n="14" num="2.8"><w n="14.1">Murmure</w>-<w n="14.2">t</w>-<w n="14.3">il</w>, <w n="14.4">et</w> <w n="14.5">sa</w> <w n="14.6">voix</w> <w n="14.7">tremble</w> :</l>
					<l n="15" num="2.9"><w n="15.1">En</w> <w n="15.2">le</w> <w n="15.3">regardant</w> <w n="15.4">il</w> <w n="15.5">me</w> <w n="15.6">semble</w></l>
					<l n="16" num="2.10"><w n="16.1">Que</w> <w n="16.2">je</w> <w n="16.3">suis</w> <w n="16.4">encore</w> <w n="16.5">au</w> <w n="16.6">pays</w>…</l>
					<l n="17" num="2.11"><w n="17.1">Quand</w> <w n="17.2">le</w> <w n="17.3">reverrai</w>-<w n="17.4">je</w>, <w n="17.5">mon</w> <w n="17.6">fils</w> ?</l>
				</lg>
				<lg n="3">
					<l n="18" num="3.1"><w n="18.1">Puis</w> <w n="18.2">un</w> <w n="18.3">juron</w> <w n="18.4">sort</w> <w n="18.5">de</w> <w n="18.6">sa</w> <w n="18.7">bouche</w></l>
					<l n="19" num="3.2"><w n="19.1">Et</w> <w n="19.2">reprenant</w> <w n="19.3">un</w> <w n="19.4">air</w> <w n="19.5">farouche</w> :</l>
					<l n="20" num="3.3"><w n="20.1">J</w>'<w n="20.2">ai</w> <w n="20.3">faim</w> ! <w n="20.4">des</w> <w n="20.5">vivres</w> ! —<w n="20.6">Au</w> <w n="20.7">logis</w>,</l>
					<l n="21" num="3.4"><w n="21.1">Monsieur</w>, <w n="21.2">pas</w> <w n="21.3">même</w> <w n="21.4">du</w> <w n="21.5">pain</w> <w n="21.6">bis</w>,</l>
					<l n="22" num="3.5"><w n="22.1">Nous</w> <w n="22.2">n</w>'<w n="22.3">avons</w> <w n="22.4">rien</w> ! — <w n="22.5">Tu</w> <w n="22.6">mens</w>, <w n="22.7">fillette</w> !…</l>
					<l n="23" num="3.6"><w n="23.1">Il</w> <w n="23.2">venait</w> <w n="23.3">de</w> <w n="23.4">voir</w> <w n="23.5">ma</w> <w n="23.6">chevrette</w></l>
					<l n="24" num="3.7"><w n="24.1">Qui</w> <w n="24.2">dormait</w> <w n="24.3">au</w> <w n="24.4">bas</w> <w n="24.5">du</w> <w n="24.6">grand</w> <w n="24.7">lit</w>…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">Vers</w> <w n="25.2">la</w> <w n="25.3">pauvre</w> <w n="25.4">bête</w> <w n="25.5">il</w> <w n="25.6">bondit</w>,</l>
					<l n="26" num="4.2"><w n="26.1">Et</w> <w n="26.2">comme</w> <w n="26.3">une</w> <w n="26.4">proie</w> <w n="26.5">il</w> <w n="26.6">l</w>'<w n="26.7">emporte</w>,</l>
					<l n="27" num="4.3"><w n="27.1">Me</w> <w n="27.2">pousse</w>, <w n="27.3">referme</w> <w n="27.4">la</w> <w n="27.5">porte</w>…</l>
					<l n="28" num="4.4"><w n="28.1">Et</w> <w n="28.2">le</w> <w n="28.3">cœur</w> <w n="28.4">tout</w> <w n="28.5">glacé</w> <w n="28.6">d</w>'<w n="28.7">effroi</w>,</l>
					<l n="29" num="4.5"><w n="29.1">Je</w> <w n="29.2">tombe</w> <w n="29.3">à</w> <w n="29.4">genoux</w> <w n="29.5">quasi</w> <w n="29.6">morte</w>,</l>
					<l n="30" num="4.6"><w n="30.1">Mais</w> <w n="30.2">le</w> <w n="30.3">bon</w> <w n="30.4">Dieu</w> <w n="30.5">veillait</w> <w n="30.6">sur</w> <w n="30.7">moi</w> !…</l>
					<l n="31" num="4.7"><w n="31.1">Un</w> <w n="31.2">souffle</w> <w n="31.3">chaud</w> <w n="31.4">court</w> <w n="31.5">sur</w> <w n="31.6">ma</w> <w n="31.7">lèvre</w>,</l>
					<l n="32" num="4.8"><w n="32.1">Je</w> <w n="32.2">rouvre</w> <w n="32.3">les</w> <w n="32.4">yeux</w>… <w n="32.5">c</w>'<w n="32.6">est</w> <w n="32.7">la</w> <w n="32.8">chèvre</w> !</l>
					<l n="33" num="4.9"><w n="33.1">Elle</w> <w n="33.2">bêle</w> <w n="33.3">en</w> <w n="33.4">me</w> <w n="33.5">caressant</w></l>
					<l n="34" num="4.10"><w n="34.1">Et</w> <w n="34.2">sa</w> <w n="34.3">blanche</w> <w n="34.4">robe</w> <w n="34.5">a</w> <w n="34.6">du</w> <w n="34.7">sang</w>…</l>
					<l n="35" num="4.11"><w n="35.1">J</w>'<w n="35.2">ai</w> <w n="35.3">tué</w> <w n="35.4">l</w>'<w n="35.5">homme</w>, <w n="35.6">dit</w> <w n="35.7">mon</w> <w n="35.8">père</w></l>
					<l n="36" num="4.12"><w n="36.1">Tout</w> <w n="36.2">pâle</w> <w n="36.3">et</w> <w n="36.4">debout</w> <w n="36.5">sur</w> <w n="36.6">le</w> <w n="36.7">seuil</w> :</l>
					<l n="37" num="4.13"><w n="37.1">Il</w> <w n="37.2">se</w> <w n="37.3">passera</w> <w n="37.4">de</w> <w n="37.5">cercueil</w></l>
					<l n="38" num="4.14"><w n="38.1">Au</w> <w n="38.2">fond</w> <w n="38.3">de</w> <w n="38.4">la</w> <w n="38.5">grande</w> <w n="38.6">marnière</w>…</l>
					<l n="39" num="4.15"><w n="39.1">A</w> <w n="39.2">tout</w> <w n="39.3">pillard</w> <w n="39.4">le</w> <w n="39.5">même</w> <w n="39.6">sort</w> !</l>
					<l n="40" num="4.16"><w n="40.1">C</w>'<w n="40.2">est</w> <w n="40.3">égal</w>, <w n="40.4">enfant</w>, <w n="40.5">pour</w> <w n="40.6">le</w> <w n="40.7">mort</w></l>
					<l n="41" num="4.17"><w n="41.1">Il</w> <w n="41.2">faudra</w> <w n="41.3">dire</w> <w n="41.4">une</w> <w n="41.5">prière</w>…</l>
				</lg>
				<lg n="5">
					<l n="42" num="5.1"><w n="42.1">Et</w> <w n="42.2">j</w>'<w n="42.3">ai</w> <w n="42.4">prié</w> <w n="42.5">d</w>'<w n="42.6">un</w> <w n="42.7">cœur</w> <w n="42.8">sincère</w>… ,</l>
				</lg>
				<lg n="6">
					<l n="43" num="6.1"><w n="43.1">Mais</w> <w n="43.2">surtout</w> <w n="43.3">pour</w> <w n="43.4">cet</w> <w n="43.5">orphelin</w></l>
					<l n="44" num="6.2"><w n="44.1">Qui</w> <w n="44.2">ressemble</w> <w n="44.3">à</w> <w n="44.4">mon</w> <w n="44.5">petit</w> <w n="44.6">frère</w>,</l>
					<l n="45" num="6.3"><w n="45.1">Et</w> <w n="45.2">de</w> <w n="45.3">l</w>'<w n="45.4">autre</w> <w n="45.5">côté</w> <w n="45.6">du</w> <w n="45.7">Rhin</w></l>
					<l n="46" num="6.4"><w n="46.1">Va</w> <w n="46.2">connaître</w> <w n="46.3">deuil</w> <w n="46.4">et</w> <w n="46.5">misère</w> !…</l>
				</lg>
			</div></body></text></TEI>