<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">STRASBOURG</title>
				<title type="medium">Édition électronique</title>
				<author key="BRG">
					<name>
						<forename>Émile</forename>
						<surname>BERGERAT</surname>
					</name>
					<date from="1845" to="1923">1845-1923</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>130 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRG_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>STRASBOURG</title>
						<author>Émile Bergerat</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k992811r.r=%C3%A9mile%20bergerat?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>STRASBOURG</title>
								<author>Émile Bergerat</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALPHONSE LEMERRE</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRG6">
				<head type="main">STRASBOURG</head>
				<head type="sub_1">Strophes lues sur le théâtre de la Comédie-Française<lb></lb>le 5 mars 1871<lb></lb>par M. Coquelin</head>
				<opener>
					<salute>A MONSIEUR HENRY DELAS</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Moi</w>, <w n="1.2">je</w> <w n="1.3">vous</w> <w n="1.4">dis</w> <w n="1.5">ceci</w>, <w n="1.6">Vandales</w>,</l>
					<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">vous</w>, <w n="2.3">qui</w>, <w n="2.4">dans</w> <w n="2.5">notre</w> <w n="2.6">Paris</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Faites</w> <w n="3.2">goûter</w> <w n="3.3">à</w> <w n="3.4">vos</w> <w n="3.5">sandales</w></l>
					<l n="4" num="1.4"><w n="4.1">Ce</w> <w n="4.2">sol</w> <w n="4.3">que</w> <w n="4.4">vous</w> <w n="4.5">n</w>'<w n="4.6">avez</w> <w n="4.7">pas</w> <w n="4.8">pris</w> !</l>
					<l n="5" num="1.5"><w n="5.1">Moi</w>, <w n="5.2">poète</w>, <w n="5.3">dont</w> <w n="5.4">l</w>'<w n="5.5">âme</w> <w n="5.6">esc</w> <w n="5.7">faite</w></l>
					<l n="6" num="1.6"><w n="6.1">De</w> <w n="6.2">la</w> <w n="6.3">poussière</w> <w n="6.4">d</w>'<w n="6.5">un</w> <w n="6.6">prophète</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">dont</w> <w n="7.3">le</w> <w n="7.4">délire</w> <w n="7.5">invaincu</w></l>
					<l n="8" num="1.8"><w n="8.1">Devance</w> <w n="8.2">tout</w>, <w n="8.3">âge</w> <w n="8.4">et</w> <w n="8.5">science</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">ressemble</w> <w n="9.3">la</w> <w n="9.4">conscience</w></l>
					<l n="10" num="1.10"><w n="10.1">D</w>'<w n="10.2">un</w> <w n="10.3">avenir</w> <w n="10.4">déjà</w> <w n="10.5">vécu</w> : —</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1"><w n="11.1">Par</w> <w n="11.2">ces</w> <w n="11.3">Villes</w> <w n="11.4">symbolisées</w></l>
					<l n="12" num="2.2"><w n="12.1">Qu</w>'<w n="12.2">on</w> <w n="12.3">voile</w> <w n="12.4">à</w> <w n="12.5">vos</w> <w n="12.6">yeux</w> <w n="12.7">éhontés</w>,</l>
					<l n="13" num="2.3"><w n="13.1">Et</w> <w n="13.2">dont</w> <w n="13.3">les</w> <w n="13.4">spectres</w> <w n="13.5">confrontés</w></l>
					<l n="14" num="2.4"><w n="14.1">Vous</w> <w n="14.2">parquent</w> <w n="14.3">aux</w> <w n="14.4">Champs</w>-<w n="14.5">Élysées</w> ;</l>
					<l n="15" num="2.5"><w n="15.1">Au</w> <w n="15.2">nom</w> <w n="15.3">des</w> <w n="15.4">trois</w> <w n="15.5">jours</w> <w n="15.6">étouffants</w></l>
					<l n="16" num="2.6"><w n="16.1">Où</w> <w n="16.2">nous</w> <w n="16.3">avons</w> <w n="16.4">à</w> <w n="16.5">nos</w> <w n="16.6">enfants</w></l>
					<l n="17" num="2.7"><w n="17.1">Enseigné</w> <w n="17.2">leur</w> <w n="17.3">future</w> <w n="17.4">histoire</w></l>
					<l n="18" num="2.8"><w n="18.1">Et</w> <w n="18.2">le</w> <w n="18.3">nom</w> <w n="18.4">du</w> <w n="18.5">vainqueur</w> <w n="18.6">piteux</w></l>
					<l n="19" num="2.9"><w n="19.1">Qui</w> <w n="19.2">leur</w> <w n="19.3">paraissait</w> <w n="19.4">si</w> <w n="19.5">honteux</w></l>
					<l n="20" num="2.10"><w n="20.1">De</w> <w n="20.2">son</w> <w n="20.3">triomphe</w> <w n="20.4">expiatoire</w> ;</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1"><w n="21.1">Au</w> <w n="21.2">nom</w> <w n="21.3">de</w> <w n="21.4">votre</w> <w n="21.5">odeur</w> <w n="21.6">d</w>'<w n="21.7">ennui</w>,</l>
					<l n="22" num="3.2"><w n="22.1">De</w> <w n="22.2">votre</w> <w n="22.3">servilité</w> <w n="22.4">plate</w>,</l>
					<l n="23" num="3.3"><w n="23.1">Et</w> <w n="23.2">de</w> <w n="23.3">cette</w> <w n="23.4">épaisse</w> <w n="23.5">omoplate</w></l>
					<l n="24" num="3.4"><w n="24.1">Où</w> <w n="24.2">le</w> <w n="24.3">bâton</w> <w n="24.4">se</w> <w n="24.5">sent</w> <w n="24.6">chez</w> <w n="24.7">lui</w> ;</l>
					<l n="25" num="3.5"><w n="25.1">Malgré</w> <w n="25.2">le</w> <w n="25.3">démon</w> <w n="25.4">qui</w> <w n="25.5">vous</w> <w n="25.6">mène</w></l>
					<l n="26" num="3.6"><w n="26.1">Comme</w> <w n="26.2">les</w> <w n="26.3">serfs</w> <w n="26.4">de</w> <w n="26.5">son</w> <w n="26.6">domaine</w>,</l>
					<l n="27" num="3.7"><w n="27.1">Et</w> <w n="27.2">ce</w> <w n="27.3">hasard</w> <w n="27.4">à</w> <w n="27.5">court</w> <w n="27.6">délai</w></l>
					<l n="28" num="3.8"><w n="28.1">Qui</w> <w n="28.2">met</w> <w n="28.3">le</w> <w n="28.4">sceptre</w> <w n="28.5">de</w> <w n="28.6">la</w> <w n="28.7">terre</w></l>
					<l n="29" num="3.9"><w n="29.1">Aux</w> <w n="29.2">mains</w> <w n="29.3">d</w> ?<w n="29.4">un</w> <w n="29.5">peuple</w> <w n="29.6">prolétaire</w></l>
					<l n="30" num="3.10"><w n="30.1">Ne</w> <w n="30.2">pour</w> <w n="30.3">manier</w> <w n="30.4">le</w> <w n="30.5">balai</w> ;</l>
				</lg>
				<lg n="4">
					<l n="31" num="4.1"><w n="31.1">Mais</w> <w n="31.2">aussi</w>, <w n="31.3">soldats</w> <w n="31.4">d</w>'<w n="31.5">étrivières</w>,</l>
					<l n="32" num="4.2"><w n="32.1">Au</w> <w n="32.2">nom</w> <w n="32.3">du</w> <w n="32.4">sang</w>, <w n="32.5">limon</w> <w n="32.6">amer</w>,</l>
					<l n="33" num="4.3"><w n="33.1">Que</w> <w n="33.2">les</w> <w n="33.3">fleuves</w>, <w n="33.4">où</w> <w n="33.5">boit</w> <w n="33.6">la</w> <w n="33.7">mer</w>,</l>
					<l n="34" num="4.4"><w n="34.1">Boivent</w> <w n="34.2">aux</w> <w n="34.3">urnes</w> <w n="34.4">des</w> <w n="34.5">rivières</w> ;</l>
					<l n="35" num="4.5"><w n="35.1">Au</w> <w n="35.2">nom</w> <w n="35.3">d</w>'<w n="35.4">un</w> <w n="35.5">sombre</w> <w n="35.6">souvenir</w> ;</l>
					<l n="36" num="4.6"><w n="36.1">Au</w> <w n="36.2">nom</w> <w n="36.3">d</w>'<w n="36.4">un</w> <w n="36.5">plus</w> <w n="36.6">sombre</w> <w n="36.7">avenir</w>,</l>
					<l n="37" num="4.7"><w n="37.1">D</w>'<w n="37.2">une</w> <w n="37.3">haine</w> <w n="37.4">que</w> <w n="37.5">rien</w> <w n="37.6">n</w>'<w n="37.7">apaise</w></l>
					<l n="38" num="4.8"><w n="38.1">Dans</w> <w n="38.2">sa</w> <w n="38.3">mortelle</w> <w n="38.4">hérédité</w> !—</l>
					<l n="39" num="4.9"><w n="39.1">Je</w> <w n="39.2">vous</w> <w n="39.3">ai</w> <w n="39.4">vus</w> ! <w n="39.5">J</w>'<w n="39.6">ai</w> <w n="39.7">médité</w> !… —</l>
					<l n="40" num="4.10"><w n="40.1">L</w>'<w n="40.2">Alsace</w> <w n="40.3">restera</w> <w n="40.4">française</w> !</l>
				</lg>
				<lg n="5">
					<l n="41" num="5.1"><w n="41.1">Console</w>-<w n="41.2">toi</w>, <w n="41.3">Strasbourg</w> ! <w n="41.4">Tu</w> <w n="41.5">prends</w></l>
					<l n="42" num="5.2"><w n="42.1">Un</w> <w n="42.2">esclavage</w> <w n="42.3">à</w> <w n="42.4">courte</w> <w n="42.5">haleine</w> !</l>
					<l n="43" num="5.3"><w n="43.1">Si</w> <w n="43.2">les</w> <w n="43.3">montagnes</w> <w n="43.4">les</w> <w n="43.5">font</w> <w n="43.6">grands</w>,</l>
					<l n="44" num="5.4"><w n="44.1">Nous</w> <w n="44.2">les</w> <w n="44.3">avons</w> <w n="44.4">vus</w> <w n="44.5">dans</w> <w n="44.6">la</w> <w n="44.7">plaine</w> !</l>
					<l n="45" num="5.5"><w n="45.1">Ils</w> <w n="45.2">sont</w> <w n="45.3">sortis</w> <w n="45.4">de</w> <w n="45.5">leurs</w> <w n="45.6">forêts</w> ;</l>
					<l n="46" num="5.6"><w n="46.1">Nous</w> <w n="46.2">les</w> <w n="46.3">avons</w> <w n="46.4">toisés</w> <w n="46.5">de</w> <w n="46.6">près</w> :</l>
					<l n="47" num="5.7"><w n="47.1">Console</w>-<w n="47.2">toi</w>, <w n="47.3">Metz</w>, <w n="47.4">avec</w> <w n="47.5">elle</w> !</l>
					<l n="48" num="5.8"><w n="48.1">Leur</w> <w n="48.2">orgueil</w> <w n="48.3">n</w>'<w n="48.4">est</w> <w n="48.5">que</w> <w n="48.6">vanité</w> :</l>
					<l n="49" num="5.9"><w n="49.1">On</w> <w n="49.2">te</w> <w n="49.3">rend</w> <w n="49.4">ta</w> <w n="49.5">virginité</w>,</l>
					<l n="50" num="5.10"><w n="50.1">S</w>'<w n="50.2">ils</w> <w n="50.3">te</w> <w n="50.4">l</w>'<w n="50.5">ont</w> <w n="50.6">prise</w>, <w n="50.7">elle</w> <w n="50.8">est</w> <w n="50.9">pucelle</w> !</l>
				</lg>
				<lg n="6">
					<l n="51" num="6.1"><w n="51.1">Patience</w> ! <w n="51.2">on</w> <w n="51.3">en</w> <w n="51.4">voit</w> <w n="51.5">le</w> <w n="51.6">fond</w></l>
					<l n="52" num="6.2"><w n="52.1">De</w> <w n="52.2">ces</w> <w n="52.3">rêveurs</w> ! <w n="52.4">On</w> <w n="52.5">les</w> <w n="52.6">mesure</w>,</l>
					<l n="53" num="6.3"><w n="53.1">Ces</w> <w n="53.2">guerriers</w> <w n="53.3">de</w> <w n="53.4">comptoir</w> <w n="53.5">qui</w> <w n="53.6">font</w></l>
					<l n="54" num="6.4"><w n="54.1">La</w> <w n="54.2">guerre</w> <w n="54.3">comme</w> <w n="54.4">on</w> <w n="54.5">fait</w> <w n="54.6">l</w>'<w n="54.7">usure</w> !</l>
					<l n="55" num="6.5"><w n="55.1">Ces</w> <w n="55.2">lourds</w> <w n="55.3">chevaucheurs</w> <w n="55.4">de</w> <w n="55.5">brouillard</w>,</l>
					<l n="56" num="6.6"><w n="56.1">Si</w> <w n="56.2">ferrés</w> <w n="56.3">sur</w> <w n="56.4">le</w> <w n="56.5">milliard</w>,</l>
					<l n="57" num="6.7"><w n="57.1">L</w>'<w n="57.2">histoire</w> <w n="57.3">sainte</w> <w n="57.4">et</w> <w n="57.5">les</w> <w n="57.6">cédules</w> !</l>
					<l n="58" num="6.8"><w n="58.1">Gens</w> <w n="58.2">d</w>'<w n="58.3">esthétique</w>, <w n="58.4">au</w> <w n="58.5">parler</w> <w n="58.6">lent</w>,</l>
					<l n="59" num="6.9"><w n="59.1">Qui</w>, <w n="59.2">pour</w> <w n="59.3">fonder</w> <w n="59.4">leur</w> <w n="59.5">Vaterland</w>,</l>
					<l n="60" num="6.10"><w n="60.1">Avaient</w> <w n="60.2">besoin</w> <w n="60.3">de</w> <w n="60.4">nos</w> <w n="60.5">pendules</w> !</l>
				</lg>
				<lg n="7">
					<l n="61" num="7.1"><w n="61.1">Ah</w> ! <w n="61.2">oui</w>, <w n="61.3">vous</w> <w n="61.4">nous</w> <w n="61.5">appartenez</w>,</l>
					<l n="62" num="7.2"><w n="62.1">Villes</w> <w n="62.2">sublimes</w> <w n="62.3">et</w> <w n="62.4">bénies</w> !</l>
					<l n="63" num="7.3"><w n="63.1">Il</w> <w n="63.2">est</w> <w n="63.3">tramé</w> <w n="63.4">par</w> <w n="63.5">des</w> <w n="63.6">génies</w>,</l>
					<l n="64" num="7.4"><w n="64.1">Le</w> <w n="64.2">fil</w> <w n="64.3">par</w> <w n="64.4">où</w> <w n="64.5">vous</w> <w n="64.6">nous</w> <w n="64.7">tenez</w> !</l>
					<l n="65" num="7.5"><w n="65.1">Vous</w> <w n="65.2">êtes</w> <w n="65.3">bien</w> <w n="65.4">filles</w> <w n="65.5">de</w> <w n="65.6">France</w></l>
					<l n="66" num="7.6"><w n="66.1">Par</w> <w n="66.2">la</w> <w n="66.3">gloire</w> <w n="66.4">et</w> <w n="66.5">par</w> <w n="66.6">la</w> <w n="66.7">souffrance</w> ;</l>
					<l n="67" num="7.7"><w n="67.1">Vous</w> <w n="67.2">portez</w>, <w n="67.3">Ô</w> <w n="67.4">cœurs</w> <w n="67.5">fraternels</w>,</l>
					<l n="68" num="7.8"><w n="68.1">La</w> <w n="68.2">cicatrice</w> <w n="68.3">de</w> <w n="68.4">famille</w></l>
					<l n="69" num="7.9"><w n="69.1">Où</w> <w n="69.2">l</w>'<w n="69.3">on</w> <w n="69.4">reconnaît</w> <w n="69.5">toute</w> <w n="69.6">fille</w></l>
					<l n="70" num="7.10"><w n="70.1">De</w> <w n="70.2">ses</w> <w n="70.3">dévoûments</w> <w n="70.4">éternels</w> !</l>
				</lg>
				<lg n="8">
					<l n="71" num="8.1"><w n="71.1">Dans</w> <w n="71.2">quelque</w> <w n="71.3">piège</w> <w n="71.4">où</w> <w n="71.5">l</w>'<w n="71.6">on</w> <w n="71.7">t</w>'<w n="71.8">attire</w>,</l>
					<l n="72" num="8.2"><w n="72.1">Alsace</w>, <w n="72.2">tu</w> <w n="72.3">nous</w> <w n="72.4">appartiens</w>,</l>
					<l n="73" num="8.3"><w n="73.1">Et</w> <w n="73.2">nous</w> <w n="73.3">nous</w> <w n="73.4">déclarons</w> <w n="73.5">les</w> <w n="73.6">tiens</w>,</l>
					<l n="74" num="8.4"><w n="74.1">Et</w> <w n="74.2">nous</w> <w n="74.3">adoptons</w> <w n="74.4">ton</w> <w n="74.5">martyre</w> !</l>
					<l n="75" num="8.5"><w n="75.1">Quels</w> <w n="75.2">que</w> <w n="75.3">soient</w> <w n="75.4">les</w> <w n="75.5">derniers</w> <w n="75.6">effets</w></l>
					<l n="76" num="8.6"><w n="76.1">Des</w> <w n="76.2">supplices</w> <w n="76.3">ou</w> <w n="76.4">des</w> <w n="76.5">bienfaits</w></l>
					<l n="77" num="8.7"><w n="77.1">Sur</w> <w n="77.2">leur</w> <w n="77.3">constance</w> <w n="77.4">ou</w> <w n="77.5">sur</w> <w n="77.6">la</w> <w n="77.7">tienne</w>,</l>
					<l n="78" num="8.8"><w n="78.1">Tant</w> <w n="78.2">que</w> <w n="78.3">ton</w> <w n="78.4">front</w> <w n="78.5">pâle</w> <w n="78.6">et</w> <w n="78.7">charmant</w></l>
					<l n="79" num="8.9"><w n="79.1">Portera</w> <w n="79.2">le</w> <w n="79.3">pied</w> <w n="79.4">allemand</w>,</l>
					<l n="80" num="8.10"><w n="80.1">La</w> <w n="80.2">France</w> <w n="80.3">se</w> <w n="80.4">fait</w> <w n="80.5">alsacienne</w> !</l>
				</lg>
				<lg n="9">
					<l n="81" num="9.1"><w n="81.1">Comme</w> <w n="81.2">en</w> <w n="81.3">Israël</w> <w n="81.4">autrefois</w>,</l>
					<l n="82" num="9.2"><w n="82.1">Strasbourg</w> <w n="82.2">sera</w> <w n="82.3">la</w> <w n="82.4">Ville</w> <w n="82.5">sainte</w> !</l>
					<l n="83" num="9.3"><w n="83.1">Ceux</w>-<w n="83.2">là</w> <w n="83.3">seront</w> <w n="83.4">Français</w> <w n="83.5">deux</w> <w n="83.6">fois</w></l>
					<l n="84" num="9.4"><w n="84.1">Qui</w> <w n="84.2">seront</w> <w n="84.3">nés</w> <w n="84.4">dans</w> <w n="84.5">son</w> <w n="84.6">enceinte</w>.</l>
					<l n="85" num="9.5"><w n="85.1">Capitale</w> <w n="85.2">de</w> <w n="85.3">nos</w> <w n="85.4">douleurs</w>,</l>
					<l n="86" num="9.6"><w n="86.1">C</w>'<w n="86.2">est</w> <w n="86.3">à</w> <w n="86.4">Strasbourg</w>, <w n="86.5">et</w> <w n="86.6">non</w> <w n="86.7">ailleurs</w>,</l>
					<l n="87" num="9.7"><w n="87.1">Que</w> <w n="87.2">nous</w> <w n="87.3">transférons</w> <w n="87.4">la</w> <w n="87.5">patrie</w> ;</l>
					<l n="88" num="9.8"><w n="88.1">Et</w> <w n="88.2">de</w> <w n="88.3">ce</w> <w n="88.4">membre</w> <w n="88.5">mutilé</w></l>
					<l n="89" num="9.9"><w n="89.1">Tout</w> <w n="89.2">le</w> <w n="89.3">corps</w> <w n="89.4">se</w> <w n="89.5">dit</w> <w n="89.6">exilé</w>,</l>
					<l n="90" num="9.10"><w n="90.1">Toute</w> <w n="90.2">vitalité</w> <w n="90.3">flétrie</w> !</l>
				</lg>
				<lg n="10">
					<l n="91" num="10.1"><w n="91.1">Nos</w> <w n="91.2">poumons</w> <w n="91.3">ne</w> <w n="91.4">respirent</w> <w n="91.5">plus</w></l>
					<l n="92" num="10.2"><w n="92.1">L</w>'<w n="92.2">air</w> <w n="92.3">restreint</w> <w n="92.4">de</w> <w n="92.5">la</w> <w n="92.6">délivrance</w> !</l>
					<l n="93" num="10.3"><w n="93.1">Déchirez</w> <w n="93.2">les</w> <w n="93.3">pactes</w> <w n="93.4">conclus</w> :</l>
					<l n="94" num="10.4"><w n="94.1">C</w>'<w n="94.2">est</w> <w n="94.3">à</w> <w n="94.4">Strasbourg</w> <w n="94.5">que</w> <w n="94.6">dort</w> <w n="94.7">la</w> <w n="94.8">France</w> !</l>
					<l n="95" num="10.5"><w n="95.1">C</w>'<w n="95.2">est</w> <w n="95.3">nous</w> <w n="95.4">qui</w> <w n="95.5">sommes</w> <w n="95.6">prisonniers</w> :</l>
					<l n="96" num="10.6"><w n="96.1">A</w> <w n="96.2">Strasbourg</w> <w n="96.3">sont</w> <w n="96.4">les</w> <w n="96.5">pigeonniers</w></l>
					<l n="97" num="10.7"><w n="97.1">Où</w> <w n="97.2">retourneront</w> <w n="97.3">les</w> <w n="97.4">colombes</w> !</l>
					<l n="98" num="10.8"><w n="98.1">C</w>’<w n="98.2">est</w> <w n="98.3">l</w>'<w n="98.4">air</w> <w n="98.5">de</w> <w n="98.6">Strasbourg</w> <w n="98.7">qu</w>'<w n="98.8">il</w> <w n="98.9">nous</w> <w n="98.10">faut</w> !</l>
					<l n="99" num="10.9"><w n="99.1">Strasbourg</w> <w n="99.2">toujours</w>, <w n="99.3">Strasbourg</w> <w n="99.4">bientôt</w> !</l>
					<l n="100" num="10.10"><w n="100.1">Là</w> <w n="100.2">sont</w> <w n="100.3">nos</w> <w n="100.4">foyers</w> — <w n="100.5">ou</w> <w n="100.6">nos</w> <w n="100.7">tombes</w> !</l>
				</lg>
				<lg n="11">
					<l n="101" num="11.1"><w n="101.1">Défense</w> <w n="101.2">de</w> <w n="101.3">rire</w> <w n="101.4">ou</w> <w n="101.5">d</w>'<w n="101.6">aimer</w></l>
					<l n="102" num="11.2"><w n="102.1">Aux</w> <w n="102.2">enfants</w> <w n="102.3">qui</w> <w n="102.4">n</w>'<w n="102.5">ont</w> <w n="102.6">plus</w> <w n="102.7">leur</w> <w n="102.8">mère</w></l>
					<l n="103" num="11.3"><w n="103.1">Et</w> <w n="103.2">défense</w> <w n="103.3">aussi</w> <w n="103.4">de</w> <w n="103.5">semer</w></l>
					<l n="104" num="11.4"><w n="104.1">Même</w> <w n="104.2">au</w> <w n="104.3">terrain</w> <w n="104.4">de</w> <w n="104.5">la</w> <w n="104.6">chimère</w> !</l>
					<l n="105" num="11.5"><w n="105.1">Défense</w> <w n="105.2">de</w> <w n="105.3">lever</w> <w n="105.4">les</w> <w n="105.5">yeux</w></l>
					<l n="106" num="11.6"><w n="106.1">Sur</w> <w n="106.2">les</w> <w n="106.3">portraits</w> <w n="106.4">de</w> <w n="106.5">ces</w> <w n="106.6">aïeux</w></l>
					<l n="107" num="11.7"><w n="107.1">Qui</w> <w n="107.2">cessent</w> <w n="107.3">d</w>'<w n="107.4">être</w> <w n="107.5">les</w> <w n="107.6">ancêtres</w></l>
					<l n="108" num="11.8"><w n="108.1">D</w>'<w n="108.2">une</w> <w n="108.3">race</w> <w n="108.4">sans</w> <w n="108.5">feu</w> <w n="108.6">ni</w> <w n="108.7">lieu</w>,</l>
					<l n="109" num="11.9"><w n="109.1">Qui</w> <w n="109.2">laisse</w> <w n="109.3">l</w>'<w n="109.4">autel</w> <w n="109.5">de</w> <w n="109.6">son</w> <w n="109.7">dieu</w></l>
					<l n="110" num="11.10"><w n="110.1">Servir</w> <w n="110.2">d</w>'<w n="110.3">écurie</w> <w n="110.4">à</w> <w n="110.5">des</w> <w n="110.6">reîtres</w></l>
				</lg>
				<lg n="12">
					<l n="111" num="12.1"><w n="111.1">Vin</w> <w n="111.2">de</w> <w n="111.3">la</w> <w n="111.4">vengeance</w> ! <w n="111.5">vieux</w> <w n="111.6">vin</w></l>
					<l n="112" num="12.2"><w n="112.1">Dont</w> <w n="112.2">la</w> <w n="112.3">haine</w> <w n="112.4">a</w> <w n="112.5">planté</w> <w n="112.6">la</w> <w n="112.7">vigne</w> !</l>
					<l n="113" num="12.3"><w n="113.1">Celui</w> <w n="113.2">qui</w> <w n="113.3">t</w>'<w n="113.4">a</w> <w n="113.5">nommé</w> <w n="113.6">divin</w></l>
					<l n="114" num="12.4"><w n="114.1">T</w>'<w n="114.2">a</w> <w n="114.3">trouvé</w> <w n="114.4">du</w> <w n="114.5">mot</w> <w n="114.6">un</w> <w n="114.7">nom</w> <w n="114.8">digne</w>.</l>
					<l n="115" num="12.5"><w n="115.1">Quand</w> <w n="115.2">un</w> <w n="115.3">peuple</w> <w n="115.4">en</w> <w n="115.5">est</w> <w n="115.6">altéré</w>,</l>
					<l n="116" num="12.6"><w n="116.1">Malheur</w> '<w n="116.2">a</w> <w n="116.3">ceux</w> <w n="116.4">qui</w> <w n="116.5">l</w>'<w n="116.6">ont</w> <w n="116.7">tiré</w> !</l>
					<l n="117" num="12.7"><w n="117.1">Sous</w> <w n="117.2">la</w> <w n="117.3">langue</w> <w n="117.4">qui</w> <w n="117.5">le</w> <w n="117.6">fustige</w>,</l>
					<l n="118" num="12.8"><w n="118.1">Il</w> <w n="118.2">fermente</w> <w n="118.3">et</w> <w n="118.4">devient</w> <w n="118.5">du</w> <w n="118.6">sang</w> !</l>
					<l n="119" num="12.9"><w n="119.1">Et</w> <w n="119.2">l</w>'<w n="119.3">épouvante</w> <w n="119.4">alors</w> <w n="119.5">descend</w></l>
					<l n="120" num="12.10"><w n="120.1">Tous</w> <w n="120.2">les</w> <w n="120.3">escaliers</w> <w n="120.4">du</w> <w n="120.5">vertige</w> !</l>
				</lg>
				<lg n="13">
					<l n="121" num="13.1"><w n="121.1">Vigne</w> ! <w n="121.2">hâte</w>-<w n="121.3">toi</w> <w n="121.4">de</w> <w n="121.5">mûrir</w> !</l>
					<l n="122" num="13.2"><w n="122.1">Car</w> <w n="122.2">notre</w> <w n="122.3">haine</w> <w n="122.4">est</w> <w n="122.5">bien</w> <w n="122.6">âgée</w> !</l>
					<l n="123" num="13.3"><w n="123.1">Car</w> <w n="123.2">nous</w> <w n="123.3">ne</w> <w n="123.4">voulons</w> <w n="123.5">pas</w> <w n="123.6">mourir</w></l>
					<l n="124" num="13.4"><w n="124.1">Avant</w> <w n="124.2">de</w> <w n="124.3">t</w>'<w n="124.4">avoir</w> <w n="124.5">vendangée</w> !</l>
					<l n="125" num="13.5"><w n="125.1">Soleil</w>, <w n="125.2">quintuple</w> <w n="125.3">tes</w> <w n="125.4">rayons</w> !</l>
					<l n="126" num="13.6"><w n="126.1">Et</w> <w n="126.2">nous</w>, <w n="126.3">pour</w> <w n="126.4">une</w> <w n="126.5">heure</w>, <w n="126.6">enrayons</w></l>
					<l n="127" num="13.7"><w n="127.1">Sur</w> <w n="127.2">la</w> <w n="127.3">pente</w> <w n="127.4">de</w> <w n="127.5">l</w>'<w n="127.6">espérance</w>,</l>
					<l n="128" num="13.8"><w n="128.1">Et</w> <w n="128.2">berçons</w> <w n="128.3">le</w> <w n="128.4">temps</w> <w n="128.5">irrité</w> ! —</l>
					<l n="129" num="13.9"><w n="129.1">Dieu</w> <w n="129.2">sera</w> <w n="129.3">dans</w> <w n="129.4">l</w>'<w n="129.5">obscurité</w></l>
					<l n="130" num="13.10"><w n="130.1">Le</w> <w n="130.2">jour</w> <w n="130.3">où</w> <w n="130.4">s</w>'<w n="130.5">éteindra</w> <w n="130.6">la</w> <w n="130.7">France</w> !</l>
				</lg>
			</div></body></text></TEI>