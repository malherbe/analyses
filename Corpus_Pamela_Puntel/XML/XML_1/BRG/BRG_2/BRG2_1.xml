<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">HYMNE A LA FRANCE</title>
				<title type="medium">Édition électronique</title>
				<author key="BRG">
					<name>
						<forename>Émile</forename>
						<surname>BERGERAT</surname>
					</name>
					<date from="1845" to="1923">1845-1923</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>105 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRG_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>HYMNE A LA FRANCE</title>
						<author>Émile Bergerat</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k851271h/f5.image.r=%C3%A9mile%20bergerat2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>HYMNE A LA FRANCE</title>
								<author>Émile Bergerat</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALPHONSE LEMERRE</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-19" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRG2">
				<head type="main">HYMNE A LA FRANCE</head>
				<opener>
					<salute>A MON AMI ÉMILE COLLET</salute>
				</opener>
				<div n="1" type="section">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">France</w>, <w n="1.3">ô</w> <w n="1.4">mon</w> <w n="1.5">pays</w>, <w n="1.6">ô</w> <w n="1.7">terre</w> <w n="1.8">d</w>'<w n="1.9">allégresse</w></l>
						<l n="2" num="1.2"><w n="2.1">En</w> <w n="2.2">qui</w> <w n="2.3">les</w> <w n="2.4">dieux</w> <w n="2.5">païens</w> <w n="2.6">nous</w> <w n="2.7">ont</w> <w n="2.8">rendu</w> <w n="2.9">la</w> <w n="2.10">Grèce</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Les</w> <w n="3.2">dieux</w> <w n="3.3">chrétiens</w>, <w n="3.4">le</w> <w n="3.5">Paradis</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Reine</w> <w n="4.2">de</w> <w n="4.3">l</w>'<w n="4.4">idéal</w>, <w n="4.5">que</w> <w n="4.6">des</w> <w n="4.7">rois</w> <w n="4.8">d</w>'<w n="4.9">industrie</w></l>
						<l n="5" num="1.5"><w n="5.1">Convoitaient</w> <w n="5.2">courtisane</w> <w n="5.3">et</w> <w n="5.4">nous</w> <w n="5.5">laissent</w> <w n="5.6">patrie</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">France</w>, <w n="6.2">que</w> <w n="6.3">l</w>'<w n="6.4">on</w> <w n="6.5">prétend</w> <w n="6.6">flétrie</w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Moi</w>, <w n="7.2">je</w> <w n="7.3">t</w>'<w n="7.4">aime</w>, <w n="7.5">et</w> <w n="7.6">je</w> <w n="7.7">te</w> <w n="7.8">le</w> <w n="7.9">dis</w> !</l>
					</lg>
				</div>
				<div n="2" type="section">
					<head type="number">II</head>
					<lg n="1">
						<l n="8" num="1.1"><w n="8.1">Je</w> <w n="8.2">t</w>'<w n="8.3">aime</w>, <w n="8.4">ô</w> <w n="8.5">ma</w> <w n="8.6">Vaincue</w>, <w n="8.7">et</w> <w n="8.8">mon</w> <w n="8.9">orgueil</w> <w n="8.10">t</w>'<w n="8.11">appelle</w></l>
						<l n="9" num="1.2"><w n="9.1">De</w> <w n="9.2">tous</w> <w n="9.3">les</w> <w n="9.4">noms</w> <w n="9.5">flatteurs</w> <w n="9.6">que</w> <w n="9.7">le</w> <w n="9.8">plaisir</w> <w n="9.9">épelle</w></l>
						<l n="10" num="1.3"><space unit="char" quantity="8"></space><w n="10.1">Et</w> <w n="10.2">qu</w>'<w n="10.3">en</w> <w n="10.4">rêve</w> <w n="10.5">nous</w> <w n="10.6">bégayons</w> ;</l>
						<l n="11" num="1.4"><w n="11.1">Lorsque</w> <w n="11.2">ton</w> <w n="11.3">mâle</w> <w n="11.4">accent</w> <w n="11.5">caresse</w> <w n="11.6">mon</w> <w n="11.7">oreille</w>,</l>
						<l n="12" num="1.5"><w n="12.1">Mon</w> <w n="12.2">âme</w> <w n="12.3">se</w> <w n="12.4">détache</w> <w n="12.5">et</w> <w n="12.6">s</w>'<w n="12.7">envole</w>, <w n="12.8">pareille</w></l>
						<l n="13" num="1.6"><space unit="char" quantity="8"></space><w n="13.1">A</w> <w n="13.2">l</w>'<w n="13.3">enfant</w> <w n="13.4">qui</w> <w n="13.5">poursuit</w> <w n="13.6">l</w>'<w n="13.7">abeille</w></l>
						<l n="14" num="1.7"><space unit="char" quantity="8"></space><w n="14.1">Parmi</w> <w n="14.2">les</w> <w n="14.3">fleurs</w> <w n="14.4">et</w> <w n="14.5">les</w> <w n="14.6">rayons</w>.</l>
					</lg>
				</div>
				<div n="3" type="section">
					<head type="number">III</head>
					<lg n="1">
						<l n="15" num="1.1"><w n="15.1">Sans</w> <w n="15.2">toi</w> <w n="15.3">l</w>'<w n="15.4">aurore</w> <w n="15.5">n</w>'<w n="15.6">est</w> <w n="15.7">qu</w>'<w n="15.8">une</w> <w n="15.9">écharpe</w> <w n="15.10">perdue</w> ;</l>
						<l n="16" num="1.2"><w n="16.1">Le</w> <w n="16.2">printemps</w> <w n="16.3">sans</w> <w n="16.4">objet</w> <w n="16.5">erre</w> <w n="16.6">dans</w> <w n="16.7">l</w>'<w n="16.8">étendue</w> ;</l>
						<l n="17" num="1.3"><space unit="char" quantity="8"></space><w n="17.1">La</w> <w n="17.2">brise</w> <w n="17.3">souffle</w> <w n="17.4">sans</w> <w n="17.5">dessein</w>. —</l>
						<l n="18" num="1.4"><w n="18.1">L</w>'<w n="18.2">univers</w> <w n="18.3">n</w>'<w n="18.4">a</w> <w n="18.5">le</w> <w n="18.6">droit</w> <w n="18.7">de</w> <w n="18.8">se</w> <w n="18.9">regarder</w> <w n="18.10">vivre</w></l>
						<l n="19" num="1.5"><w n="19.1">Que</w> <w n="19.2">lorsque</w> <w n="19.3">tu</w> <w n="19.4">t</w>'<w n="19.5">endors</w>, <w n="19.6">le</w> <w n="19.7">coude</w> <w n="19.8">sur</w> <w n="19.9">un</w> <w n="19.10">livre</w> ,</l>
						<l n="20" num="1.6"><space unit="char" quantity="8"></space><w n="20.1">Ayant</w> <w n="20.2">usé</w> <w n="20.3">l</w>'<w n="20.4">or</w> <w n="20.5">et</w> <w n="20.6">le</w> <w n="20.7">cuivre</w></l>
						<l n="21" num="1.7"><space unit="char" quantity="8"></space><w n="21.1">Pour</w> <w n="21.2">les</w> <w n="21.3">parures</w> <w n="21.4">de</w> <w n="21.5">ton</w> <w n="21.6">sein</w>.</l>
					</lg>
				</div>
				<div n="4" type="section">
					<head type="number">IV</head>
					<lg n="1">
						<l n="22" num="1.1"><w n="22.1">Dans</w> <w n="22.2">l</w>'<w n="22.3">ombre</w> <w n="22.4">où</w> <w n="22.5">le</w> <w n="22.6">Soleil</w> <w n="22.7">se</w> <w n="22.8">baigne</w> <w n="22.9">et</w> <w n="22.10">se</w> <w n="22.11">répare</w>,</l>
						<l n="23" num="1.2"><w n="23.1">Il</w> <w n="23.2">attend</w> <w n="23.3">que</w> <w n="23.4">ton</w> <w n="23.5">ciel</w> <w n="23.6">repasse</w>, <w n="23.7">et</w> <w n="23.8">se</w> <w n="23.9">prépare</w></l>
						<l n="24" num="1.3"><space unit="char" quantity="8"></space><w n="24.1">A</w> <w n="24.2">t</w>'<w n="24.3">y</w> <w n="24.4">posséder</w> <w n="24.5">à</w> <w n="24.6">plaisir</w>,</l>
						<l n="25" num="1.4"><w n="25.1">Et</w>, <w n="25.2">remplissant</w> <w n="25.3">encor</w> <w n="25.4">tes</w> <w n="25.5">nuits</w> <w n="25.6">enchanteresses</w></l>
						<l n="26" num="1.5"><w n="26.1">Du</w> <w n="26.2">parfum</w> <w n="26.3">reposé</w> <w n="26.4">de</w> <w n="26.5">ses</w> <w n="26.6">chaudes</w> <w n="26.7">ivresses</w>,</l>
						<l n="27" num="1.6"><space unit="char" quantity="8"></space><w n="27.1">Ne</w> <w n="27.2">leur</w> <w n="27.3">laisse</w>, <w n="27.4">entre</w> <w n="27.5">deux</w> <w n="27.6">caresses</w>,</l>
						<l n="28" num="1.7"><space unit="char" quantity="8"></space><w n="28.1">Que</w> <w n="28.2">l</w>'<w n="28.3">interrègne</w> <w n="28.4">du</w> <w n="28.5">désir</w>.</l>
					</lg>
				</div>
				<div n="5" type="section">
					<head type="number">V</head>
					<lg n="1">
						<l n="29" num="1.1"><w n="29.1">Le</w> <w n="29.2">vent</w> <w n="29.3">sombre</w> <w n="29.4">et</w> <w n="29.5">mortel</w> <w n="29.6">qui</w> <w n="29.7">rayonne</w> <w n="29.8">des</w> <w n="29.9">pôles</w></l>
						<l n="30" num="1.2"><w n="30.1">Adoucit</w> <w n="30.2">son</w> <w n="30.3">haleine</w> <w n="30.4">en</w> <w n="30.5">touchant</w> <w n="30.6">tes</w> <w n="30.7">épaules</w>,</l>
						<l n="31" num="1.3"><space unit="char" quantity="8"></space><w n="31.1">Et</w> <w n="31.2">l</w>'<w n="31.3">Hiver</w>, <w n="31.4">ton</w> <w n="31.5">vieil</w> <w n="31.6">amoureux</w>,</l>
						<l n="32" num="1.4"><w n="32.1">T</w>'<w n="32.2">épargne</w> <w n="32.3">le</w> <w n="32.4">baiser</w> <w n="32.5">de</w> <w n="32.6">sa</w> <w n="32.7">barbe</w> <w n="32.8">robuste</w></l>
						<l n="33" num="1.5"><w n="33.1">Et</w> <w n="33.2">n</w>'<w n="33.3">en</w> <w n="33.4">laisse</w> <w n="33.5">tomber</w> <w n="33.6">de</w> <w n="33.7">neige</w> <w n="33.8">que</w> <w n="33.9">tout</w> <w n="33.10">juste</w></l>
						<l n="34" num="1.6"><space unit="char" quantity="8"></space><w n="34.1">Ce</w> <w n="34.2">qu</w>'<w n="34.3">il</w> <w n="34.4">faut</w> <w n="34.5">pour</w> <w n="34.6">mouler</w> <w n="34.7">ton</w> <w n="34.8">buste</w></l>
						<l n="35" num="1.7"><space unit="char" quantity="8"></space><w n="35.1">Sans</w> <w n="35.2">glacer</w> <w n="35.3">ton</w> <w n="35.4">sang</w> <w n="35.5">généreux</w> !</l>
					</lg>
				</div>
				<div n="6" type="section">
					<head type="number">VI</head>
					<lg n="1">
						<l n="36" num="1.1"><w n="36.1">Terre</w> <w n="36.2">où</w> <w n="36.3">tout</w> <w n="36.4">fructifie</w> <w n="36.5">et</w> <w n="36.6">jusqu</w>'<w n="36.7">à</w> <w n="36.8">la</w> <w n="36.9">paresse</w>,</l>
						<l n="37" num="1.2"><w n="37.1">Que</w> <w n="37.2">le</w> <w n="37.3">vent</w> <w n="37.4">fertilise</w> <w n="37.5">et</w> <w n="37.6">que</w> <w n="37.7">l</w>'<w n="37.8">engrais</w> <w n="37.9">oppresse</w>,</l>
						<l n="38" num="1.3"><space unit="char" quantity="8"></space><w n="38.1">Qui</w>, <w n="38.2">pour</w> <w n="38.3">un</w> <w n="38.4">choc</w>, <w n="38.5">rends</w> <w n="38.6">un</w> <w n="38.7">héros</w> ;</l>
						<l n="39" num="1.4"><w n="39.1">Où</w> <w n="39.2">la</w> <w n="39.3">pauvreté</w> <w n="39.4">rit</w> <w n="39.5">même</w> <w n="39.6">de</w> <w n="39.7">ses</w> <w n="39.8">guenilles</w> ,</l>
						<l n="40" num="1.5"><w n="40.1">Où</w> <w n="40.2">la</w> <w n="40.3">volupté</w> <w n="40.4">flotte</w> <w n="40.5">à</w> <w n="40.6">toutes</w> <w n="40.7">les</w> <w n="40.8">charmilles</w>,</l>
						<l n="41" num="1.6"><space unit="char" quantity="8"></space><w n="41.1">A</w> <w n="41.2">tous</w> <w n="41.3">les</w> <w n="41.4">cils</w> <w n="41.5">de</w> <w n="41.6">jeunes</w> <w n="41.7">filles</w>,</l>
						<l n="42" num="1.7"><space unit="char" quantity="8"></space><w n="42.1">A</w> <w n="42.2">tous</w> <w n="42.3">les</w> <w n="42.4">becs</w> <w n="42.5">de</w> <w n="42.6">passereaux</w> ;</l>
					</lg>
				</div>
				<div n="7" type="section">
					<head type="number">VII</head>
					<lg n="1">
						<l n="43" num="1.1"><w n="43.1">Éden</w> <w n="43.2">où</w> <w n="43.3">tout</w> <w n="43.4">Olympe</w> <w n="43.5">a</w> <w n="43.6">quelque</w> <w n="43.7">pied</w>-<w n="43.8">à</w>-<w n="43.9">terre</w> ;</l>
						<l n="44" num="1.2"><w n="44.1">Où</w> <w n="44.2">l</w>'<w n="44.3">exilé</w> <w n="44.4">bâtit</w> <w n="44.5">son</w> <w n="44.6">exil</w> <w n="44.7">volontaire</w> ;</l>
						<l n="45" num="1.3"><space unit="char" quantity="8"></space><w n="45.1">Temple</w>, <w n="45.2">qui</w> <w n="45.3">peux</w> <w n="45.4">seul</w> <w n="45.5">contenir</w></l>
						<l n="46" num="1.4"><w n="46.1">Un</w> <w n="46.2">peuple</w> <w n="46.3">dont</w> <w n="46.4">l</w>'<w n="46.5">histoire</w> <w n="46.6">est</w> <w n="46.7">le</w> <w n="46.8">roman</w> <w n="46.9">du</w> <w n="46.10">monde</w></l>
						<l n="47" num="1.5"><w n="47.1">Et</w> <w n="47.2">prépare</w> <w n="47.3">une</w> <w n="47.4">Bible</w> <w n="47.5">en</w> <w n="47.6">Sinaïs</w> <w n="47.7">féconde</w></l>
						<l n="48" num="1.6"><space unit="char" quantity="8"></space><w n="48.1">A</w> <w n="48.2">la</w> <w n="48.3">prophétique</w> <w n="48.4">faconde</w></l>
						<l n="49" num="1.7"><space unit="char" quantity="8"></space><w n="49.1">Des</w> <w n="49.2">apôtres</w> <w n="49.3">de</w> <w n="49.4">l</w>'<w n="49.5">avenir</w> !</l>
					</lg>
				</div>
				<div n="8" type="section">
					<head type="number">VIII</head>
					<lg n="1">
						<l n="50" num="1.1"><w n="50.1">France</w> <w n="50.2">que</w> <w n="50.3">Dieu</w> <w n="50.4">pétrit</w> <w n="50.5">de</w> <w n="50.6">la</w> <w n="50.7">fleur</w> <w n="50.8">de</w> <w n="50.9">sa</w> <w n="50.10">boue</w>,</l>
						<l n="51" num="1.2"><w n="51.1">Et</w> <w n="51.2">posa</w>, <w n="51.3">comme</w> <w n="51.4">on</w> <w n="51.5">niche</w> <w n="51.6">un</w> <w n="51.7">signe</w> <w n="51.8">sur</w> <w n="51.9">la</w> <w n="51.10">joue</w>,</l>
						<l n="52" num="1.3"><space unit="char" quantity="8"></space><w n="52.1">Au</w> <w n="52.2">coin</w> <w n="52.3">de</w> <w n="52.4">l</w>'<w n="52.5">œuvre</w> <w n="52.6">de</w> <w n="52.7">limon</w> ;</l>
						<l n="53" num="1.4"><w n="53.1">Perle</w> <w n="53.2">de</w> <w n="53.3">cet</w> <w n="53.4">écrin</w> <w n="53.5">dont</w> <w n="53.6">la</w> <w n="53.7">splendeur</w> <w n="53.8">l</w>'<w n="53.9">atteste</w></l>
						<l n="54" num="1.5"><w n="54.1">Et</w> <w n="54.2">ferait</w> <w n="54.3">inventer</w> <w n="54.4">quelque</w> <w n="54.5">fable</w> <w n="54.6">céleste</w></l>
						<l n="55" num="1.6"><space unit="char" quantity="8"></space><w n="55.1">D</w>'<w n="55.2">un</w> <w n="55.3">Dieu</w> <w n="55.4">réalisant</w> <w n="55.5">d</w>'<w n="55.6">un</w> <w n="55.7">geste</w></l>
						<l n="56" num="1.7"><space unit="char" quantity="8"></space><w n="56.1">Le</w> <w n="56.2">séjour</w> <w n="56.3">rêvé</w> <w n="56.4">d</w>'<w n="56.5">un</w> <w n="56.6">démon</w> !</l>
					</lg>
				</div>
				<div n="9" type="section">
					<head type="number">IX</head>
					<lg n="1">
						<l n="57" num="1.1"><w n="57.1">Que</w> <w n="57.2">te</w> <w n="57.3">manque</w>-<w n="57.4">t</w>-<w n="57.5">il</w> <w n="57.6">donc</w>, <w n="57.7">ô</w> <w n="57.8">beauté</w> <w n="57.9">trop</w> <w n="57.10">parfaite</w> ?</l>
						<l n="58" num="1.2"><w n="58.1">Ta</w> <w n="58.2">grâce</w> <w n="58.3">s</w>'<w n="58.4">embellit</w> <w n="58.5">encor</w> <w n="58.6">de</w> <w n="58.7">ta</w> <w n="58.8">défaite</w></l>
						<l n="59" num="1.3"><space unit="char" quantity="8"></space><w n="59.1">Et</w> <w n="59.2">ton</w> <w n="59.3">malheur</w> <w n="59.4">de</w> <w n="59.5">ta</w> <w n="59.6">fierté</w> !</l>
						<l n="60" num="1.4"><w n="60.1">France</w> ! <w n="60.2">écoute</w> <w n="60.3">le</w> <w n="60.4">Vœu</w> <w n="60.5">d</w>'<w n="60.6">un</w> <w n="60.7">pauvre</w> <w n="60.8">enfant</w> <w n="60.9">qui</w> <w n="60.10">t</w>'<w n="60.11">aime</w> ;</l>
						<l n="61" num="1.5"><w n="61.1">Il</w> <w n="61.2">ne</w> <w n="61.3">manque</w>, <w n="61.4">ô</w> <w n="61.5">patrie</w>, <w n="61.6">à</w> <w n="61.7">ton</w> <w n="61.8">front</w> <w n="61.9">qu</w>'<w n="61.10">un</w> <w n="61.11">baptême</w>,</l>
						<l n="62" num="1.6"><space unit="char" quantity="8"></space><w n="62.1">Qu</w>'<w n="62.2">un</w> <w n="62.3">fleuron</w> <w n="62.4">à</w> <w n="62.5">ton</w> <w n="62.6">diadème</w></l>
						<l n="63" num="1.7"><space unit="char" quantity="8"></space><w n="63.1">A</w> <w n="63.2">ton</w> <w n="63.3">cœur</w> <w n="63.4">que</w> <w n="63.5">la</w> <w n="63.6">liberté</w> !</l>
					</lg>
				</div>
				<div n="10" type="section">
					<head type="number">X</head>
					<lg n="1">
						<l n="64" num="1.1"><w n="64.1">Qu</w>'<w n="64.2">as</w>-<w n="64.3">tu</w> <w n="64.4">besoin</w> <w n="64.5">de</w> <w n="64.6">rois</w>, <w n="64.7">n</w>'<w n="64.8">as</w>-<w n="64.9">tu</w> <w n="64.10">pas</w> <w n="64.11">tes</w> <w n="64.12">poëtes</w> ?</l>
						<l n="65" num="1.2"><w n="65.1">Chercheras</w>-<w n="65.2">tu</w> <w n="65.3">toujours</w> <w n="65.4">l</w>'<w n="65.5">époux</w> <w n="65.6">que</w> <w n="65.7">tu</w> <w n="65.8">souhaites</w></l>
						<l n="66" num="1.3"><space unit="char" quantity="8"></space><w n="66.1">Parmi</w> <w n="66.2">les</w> <w n="66.3">hercules</w> <w n="66.4">hâbleurs</w> ?</l>
						<l n="67" num="1.4"><w n="67.1">Seras</w>-<w n="67.2">tu</w> <w n="67.3">toujours</w> <w n="67.4">femme</w>, <w n="67.5">ô</w> <w n="67.6">reine</w> <w n="67.7">familière</w> ?</l>
						<l n="68" num="1.5"><w n="68.1">Et</w> <w n="68.2">voudras</w>-<w n="68.3">tu</w> <w n="68.4">toujours</w>, <w n="68.5">ô</w> <w n="68.6">France</w> <w n="68.7">de</w> <w n="68.8">Molière</w> !</l>
						<l n="69" num="1.6"><space unit="char" quantity="8"></space><w n="69.1">Vivre</w> <w n="69.2">battue</w>, <w n="69.3">en</w> <w n="69.4">être</w> <w n="69.5">fière</w>,</l>
						<l n="70" num="1.7"><space unit="char" quantity="8"></space><w n="70.1">Et</w> <w n="70.2">rire</w> <w n="70.3">d</w>'<w n="70.4">amour</w> <w n="70.5">dans</w> <w n="70.6">les</w> <w n="70.7">pleurs</w> ?</l>
					</lg>
				</div>
				<div n="11" type="section">
					<head type="number">XI</head>
					<lg n="1">
						<l n="71" num="1.1"><w n="71.1">Ah</w> ! <w n="71.2">cesse</w> <w n="71.3">d</w>'<w n="71.4">éblouir</w> <w n="71.5">ta</w> <w n="71.6">bêtise</w> <w n="71.7">idolâtre</w></l>
						<l n="72" num="1.2"><w n="72.1">Par</w> <w n="72.2">la</w> <w n="72.3">pourpre</w> <w n="72.4">où</w> <w n="72.5">se</w> <w n="72.6">drape</w> <w n="72.7">un</w> <w n="72.8">fantoche</w> <w n="72.9">bellâtre</w>,</l>
						<l n="73" num="1.3"><space unit="char" quantity="8"></space><w n="73.1">Qui</w> <w n="73.2">te</w> <w n="73.3">gruge</w> <w n="73.4">nonchalamment</w> !</l>
						<l n="74" num="1.4"><w n="74.1">Cesse</w> <w n="74.2">de</w> <w n="74.3">profaner</w> <w n="74.4">l</w>'<w n="74.5">honneur</w> <w n="74.6">de</w> <w n="74.7">tes</w> <w n="74.8">délices</w></l>
						<l n="75" num="1.5"><w n="75.1">Avec</w> <w n="75.2">ces</w> <w n="75.3">bateleurs</w> <w n="75.4">de</w> <w n="75.5">sceptre</w>, <w n="75.6">à</w> <w n="75.7">cheveux</w> <w n="75.8">lisses</w>,</l>
						<l n="76" num="1.6"><space unit="char" quantity="8"></space><w n="76.1">Qui</w> <w n="76.2">te</w> <w n="76.3">vendent</w> <w n="76.4">dans</w> <w n="76.5">les</w> <w n="76.6">coulisses</w></l>
						<l n="77" num="1.7"><space unit="char" quantity="8"></space><w n="77.1">A</w> <w n="77.2">quelque</w> <w n="77.3">banquier</w> <w n="77.4">allemand</w> !</l>
					</lg>
				</div>
				<div n="12" type="section">
					<head type="number">XII</head>
					<lg n="1">
						<l n="78" num="1.1"><w n="78.1">Viens</w> ! <w n="78.2">reprends</w> <w n="78.3">à</w> <w n="78.4">deux</w> <w n="78.5">mains</w> <w n="78.6">ta</w> <w n="78.7">jeunesse</w> <w n="78.8">immortelle</w> !</l>
						<l n="79" num="1.2"><w n="79.1">Prends</w> <w n="79.2">ton</w> <w n="79.3">courage</w> <w n="79.4">et</w> <w n="79.5">ton</w> <w n="79.6">malheur</w> ! <w n="79.7">Sors</w> <w n="79.8">de</w> <w n="79.9">tutelle</w> ;</l>
						<l n="80" num="1.3"><space unit="char" quantity="8"></space><w n="80.1">Et</w> <w n="80.2">reviens</w> <w n="80.3">à</w> <w n="80.4">nous</w> <w n="80.5">qui</w> <w n="80.6">t</w>'<w n="80.7">aimons</w> !</l>
						<l n="81" num="1.4"><w n="81.1">Les</w> <w n="81.2">poëtes</w> <w n="81.3">sont</w> <w n="81.4">ceux</w> <w n="81.5">qui</w> <w n="81.6">connaissent</w> <w n="81.7">la</w> <w n="81.8">route</w>,</l>
						<l n="82" num="1.5"><w n="82.1">Qui</w> <w n="82.2">savent</w> <w n="82.3">de</w> <w n="82.4">quel</w> <w n="82.5">point</w> <w n="82.6">menace</w> <w n="82.7">la</w> <w n="82.8">déroute</w>,</l>
						<l n="83" num="1.6"><space unit="char" quantity="8"></space><w n="83.1">Et</w>, <w n="83.2">comme</w> <w n="83.3">la</w> <w n="83.4">chèvre</w> <w n="83.5">qui</w> <w n="83.6">broute</w>,</l>
						<l n="84" num="1.7"><space unit="char" quantity="8"></space><w n="84.1">Sentent</w> <w n="84.2">l</w>'<w n="84.3">orage</w> <w n="84.4">sur</w> <w n="84.5">les</w> <w n="84.6">monts</w> !</l>
					</lg>
				</div>
				<div n="13" type="section">
					<head type="number">XIII</head>
					<lg n="1">
						<l n="85" num="1.1"><w n="85.1">Viens</w> ! <w n="85.2">nous</w> <w n="85.3">t</w>'<w n="85.4">emporterons</w> <w n="85.5">sur</w> <w n="85.6">la</w> <w n="85.7">cime</w> <w n="85.8">éclatante</w></l>
						<l n="86" num="1.2"><w n="86.1">D</w>'<w n="86.2">où</w> <w n="86.3">librement</w> <w n="86.4">on</w> <w n="86.5">voit</w>, <w n="86.6">et</w> <w n="86.7">du</w> <w n="86.8">pas</w> <w n="86.9">de</w> <w n="86.10">sa</w> <w n="86.11">tente</w>,</l>
						<l n="87" num="1.3"><space unit="char" quantity="8"></space><w n="87.1">Là</w> <w n="87.2">le</w> <w n="87.3">Désert</w> — <w n="87.4">là</w> <w n="87.5">Chanaan</w> ;</l>
						<l n="88" num="1.4"><w n="88.1">A</w> <w n="88.2">droite</w> <w n="88.3">les</w> <w n="88.4">veaux</w> <w n="88.5">d</w>'<w n="88.6">or</w> <w n="88.7">submergés</w> <w n="88.8">par</w> <w n="88.9">le</w> <w n="88.10">sable</w>,</l>
						<l n="89" num="1.5"><w n="89.1">A</w> <w n="89.2">gauche</w> <w n="89.3">les</w> <w n="89.4">raisins</w> <w n="89.5">monstrueux</w> <w n="89.6">de</w> <w n="89.7">la</w> <w n="89.8">Table</w>,</l>
						<l n="90" num="1.6"><space unit="char" quantity="8"></space><w n="90.1">Là</w>, <w n="90.2">l</w>'<w n="90.3">avenir</w> <w n="90.4">intarissable</w> ;</l>
						<l n="91" num="1.7"><space unit="char" quantity="8"></space><w n="91.1">Là</w>, <w n="91.2">l</w>'<w n="91.3">insatiable</w> <w n="91.4">néant</w>.</l>
					</lg>
				</div>
				<div n="14" type="section">
					<head type="number">XIV</head>
					<lg n="1">
						<l n="92" num="1.1"><w n="92.1">Viens</w> ! <w n="92.2">tu</w> <w n="92.3">compareras</w> <w n="92.4">en</w> <w n="92.5">leur</w> <w n="92.6">course</w> <w n="92.7">sonore</w></l>
						<l n="93" num="1.2"><w n="93.1">Les</w> <w n="93.2">coteaux</w> <w n="93.3">moutonneux</w> <w n="93.4">qui</w> <w n="93.5">montent</w> <w n="93.6">vers</w> <w n="93.7">l</w>'<w n="93.8">aurore</w></l>
						<l n="94" num="1.3"><space unit="char" quantity="8"></space><w n="94.1">En</w> <w n="94.2">un</w> <w n="94.3">frais</w> <w n="94.4">tumulte</w> <w n="94.5">d</w>'<w n="94.6">amour</w>,</l>
						<l n="95" num="1.4"><w n="95.1">Et</w>, <w n="95.2">dans</w> <w n="95.3">les</w> <w n="95.4">stations</w> <w n="95.5">des</w> <w n="95.6">calvaires</w> <w n="95.7">funèbres</w>,</l>
						<l n="96" num="1.5"><w n="96.1">Les</w> <w n="96.2">générations</w>, <w n="96.3">ivres</w> <w n="96.4">de</w> <w n="96.5">leurs</w> <w n="96.6">ténèbres</w> ,</l>
						<l n="97" num="1.6"><space unit="char" quantity="8"></space><w n="97.1">Comme</w> <w n="97.2">ces</w> <w n="97.3">larves</w> <w n="97.4">sans</w> <w n="97.5">vertèbres</w></l>
						<l n="98" num="1.7"><space unit="char" quantity="8"></space><w n="98.1">Que</w> <w n="98.2">torture</w> <w n="98.3">l</w>'<w n="98.4">éclat</w> <w n="98.5">du</w> <w n="98.6">jour</w> !</l>
					</lg>
				</div>
				<div n="15" type="section">
					<head type="number">XV</head>
					<lg n="1">
						<l n="99" num="1.1"><w n="99.1">Et</w> <w n="99.2">là</w>, <w n="99.3">France</w> ! <w n="99.4">dressée</w> <w n="99.5">au</w> <w n="99.6">seuil</w> <w n="99.7">de</w> <w n="99.8">la</w> <w n="99.9">nature</w>,</l>
						<l n="100" num="1.2"><w n="100.1">Dénouant</w> <w n="100.2">tes</w> <w n="100.3">cheveux</w>, <w n="100.4">dénouant</w> <w n="100.5">ta</w> <w n="100.6">ceinture</w>,</l>
						<l n="101" num="1.3"><space unit="char" quantity="8"></space><w n="101.1">Et</w> <w n="101.2">lavant</w> <w n="101.3">tes</w> <w n="101.4">bras</w> <w n="101.5">au</w> <w n="101.6">soleil</w>,</l>
						<l n="102" num="1.4"><w n="102.1">Les</w> <w n="102.2">frissons</w> <w n="102.3">précurseurs</w> <w n="102.4">d</w>'<w n="102.5">une</w> <w n="102.6">amour</w> <w n="102.7">inconnue</w></l>
						<l n="103" num="1.5"><w n="103.1">Agiteront</w> <w n="103.2">les</w> <w n="103.3">seins</w> <w n="103.4">de</w> <w n="103.5">ta</w> <w n="103.6">poitrine</w> <w n="103.7">nue</w>,</l>
						<l n="104" num="1.6"><space unit="char" quantity="8"></space><w n="104.1">Et</w> <w n="104.2">tu</w> <w n="104.3">recevras</w> <w n="104.4">dans</w> <w n="104.5">la</w> <w n="104.6">nue</w></l>
						<l n="105" num="1.7"><space unit="char" quantity="8"></space><w n="105.1">L</w>'<w n="105.2">immense</w> <w n="105.3">baiser</w> <w n="105.4">du</w> <w n="105.5">Réveil</w> !</l>
					</lg>
				</div>
				<closer>
					<dateline>
						<date when="1871">Mars 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>