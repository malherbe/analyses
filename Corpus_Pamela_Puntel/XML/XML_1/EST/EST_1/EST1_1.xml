<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="EST">
					<name>
						<forename>Hector</forename>
						<surname>L'ESTRAZ</surname>
					</name>
					<date from="1848" to="1936">1848-1936</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>112 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">EST_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>HECTOR L'ESTRAZ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb312226608</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES (D'APRÈS LES DESSINS D'ÉMILE BAYARD)</title>
								<author>HECTOR L'ESTRAZ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE ARTISTIQUE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="EST1">
				<head type="main">A M. ÉMILE BAYARD SON ADMIRATEUR</head>
				<head type="sub">1870</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L</w>'<w n="1.2">aube</w> <w n="1.3">s</w>'<w n="1.4">est</w> <w n="1.5">levée</w> <w n="1.6">éclatante</w> <w n="1.7">et</w> <w n="1.8">pure</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">ciel</w> <w n="2.3">est</w> <w n="2.4">d</w>'<w n="2.5">azur</w>, <w n="2.6">le</w> <w n="2.7">printemps</w> <w n="2.8">sourit</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">cœurs</w> <w n="3.3">sont</w> <w n="3.4">heureux</w>, <w n="3.5">et</w> <w n="3.6">dans</w> <w n="3.7">la</w> <w n="3.8">nature</w></l>
					<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">seule</w> <w n="4.3">devise</w> « <w n="4.4">Amour</w> » <w n="4.5">est</w> <w n="4.6">écrit</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Partout</w> <w n="5.2">des</w> <w n="5.3">rayons</w>, <w n="5.4">partout</w> <w n="5.5">de</w> <w n="5.6">la</w> <w n="5.7">joie</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">chant</w> <w n="6.3">des</w> <w n="6.4">oiseaux</w> <w n="6.5">emplit</w> <w n="6.6">l</w>'<w n="6.7">air</w> <w n="6.8">vermeil</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Les</w> <w n="7.2">fleurs</w> <w n="7.3">ont</w> <w n="7.4">paré</w> <w n="7.5">leurs</w> <w n="7.6">robes</w> <w n="7.7">de</w> <w n="7.8">soie</w></l>
					<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">purs</w> <w n="8.3">diamants</w> <w n="8.4">brillants</w> <w n="8.5">au</w> <w n="8.6">soleil</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Des</w> <w n="9.2">prés</w> <w n="9.3">verts</w>, <w n="9.4">des</w> <w n="9.5">bois</w>, <w n="9.6">des</w> <w n="9.7">sources</w>, <w n="9.8">des</w> <w n="9.9">roses</w>,</l>
					<l n="10" num="3.2"><w n="10.1">De</w> <w n="10.2">l</w>'<w n="10.3">air</w> <w n="10.4">et</w> <w n="10.5">du</w> <w n="10.6">ciel</w>, <w n="10.7">de</w> <w n="10.8">chaque</w> <w n="10.9">élément</w></l>
					<l n="11" num="3.3"><w n="11.1">Sort</w> <w n="11.2">et</w> <w n="11.3">se</w> <w n="11.4">répand</w> <w n="11.5">sur</w> <w n="11.6">toutes</w> <w n="11.7">les</w> <w n="11.8">choses</w></l>
					<l n="12" num="3.4"><w n="12.1">En</w> <w n="12.2">effluves</w> <w n="12.3">saints</w> <w n="12.4">un</w> <w n="12.5">magique</w> <w n="12.6">aimant</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">L</w>'<w n="13.2">homme</w> <w n="13.3">sent</w> <w n="13.4">en</w> <w n="13.5">lui</w> <w n="13.6">son</w> <w n="13.7">âme</w> <w n="13.8">divine</w>,</l>
					<l n="14" num="4.2"><w n="14.1">La</w> <w n="14.2">sérénité</w> <w n="14.3">descend</w> <w n="14.4">du</w> <w n="14.5">ciel</w> <w n="14.6">bleu</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Tout</w> <w n="15.2">est</w> <w n="15.3">idéal</w> <w n="15.4">et</w> <w n="15.5">doux</w> : <w n="15.6">on</w> <w n="15.7">devine</w></l>
					<l n="16" num="4.4"><w n="16.1">Que</w> <w n="16.2">sur</w> <w n="16.3">terre</w> <w n="16.4">passe</w> <w n="16.5">un</w> <w n="16.6">souffle</w> <w n="16.7">de</w> <w n="16.8">Dieu</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">— <w n="17.1">Dans</w> <w n="17.2">les</w> <w n="17.3">prés</w> <w n="17.4">en</w> <w n="17.5">fleurs</w> <w n="17.6">la</w> <w n="17.7">jeune</w> <w n="17.8">famille</w></l>
					<l n="18" num="5.2"><w n="18.1">S</w>'<w n="18.2">en</w> <w n="18.3">va</w> <w n="18.4">promener</w> <w n="18.5">ses</w> <w n="18.6">amours</w> <w n="18.7">touchants</w></l>
					<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">petit</w> <w n="19.3">garçon</w>, <w n="19.4">la</w> <w n="19.5">petite</w> <w n="19.6">fille</w></l>
					<l n="20" num="5.4"><w n="20.1">Sous</w> <w n="20.2">l</w>'<w n="20.3">œil</w> <w n="20.4">des</w> <w n="20.5">parents</w> <w n="20.6">courent</w> <w n="20.7">par</w> <w n="20.8">les</w> <w n="20.9">champs</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Par</w> <w n="21.2">les</w> <w n="21.3">champs</w> <w n="21.4">joyeux</w>, <w n="21.5">dans</w> <w n="21.6">les</w> <w n="21.7">herbes</w> <w n="21.8">folles</w></l>
					<l n="22" num="6.2"><w n="22.1">Ils</w> <w n="22.2">vont</w> <w n="22.3">poursuivant</w> <w n="22.4">les</w> <w n="22.5">blancs</w> <w n="22.6">papillons</w></l>
					<l n="23" num="6.3"><w n="23.1">Ces</w> <w n="23.2">amants</w> <w n="23.3">légers</w> <w n="23.4">des</w> <w n="23.5">fraîches</w> <w n="23.6">corolles</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Ou</w> <w n="24.2">cherchant</w> <w n="24.3">l</w>'<w n="24.4">insecte</w> <w n="24.5">au</w> <w n="24.6">creux</w> <w n="24.7">des</w> <w n="24.8">sillons</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Quelle</w> <w n="25.2">ample</w> <w n="25.3">moisson</w> <w n="25.4">de</w> <w n="25.5">trésors</w> <w n="25.6">vous</w> <w n="25.7">faites</w></l>
					<l n="26" num="7.2"><w n="26.1">Parmi</w> <w n="26.2">les</w> <w n="26.3">œillets</w> <w n="26.4">et</w> <w n="26.5">parmi</w> <w n="26.6">les</w> <w n="26.7">thyms</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Les</w> <w n="27.2">coquelicots</w> <w n="27.3">et</w> <w n="27.4">les</w> <w n="27.5">paquerettes</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Bébés</w> <w n="28.2">adorés</w>, <w n="28.3">ô</w> <w n="28.4">charmants</w> <w n="28.5">lutins</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Voyez</w>-<w n="29.2">les</w> <w n="29.3">cueillir</w> <w n="29.4">la</w> <w n="29.5">fleur</w> <w n="29.6">éphémère</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Jetant</w> <w n="30.2">au</w> <w n="30.3">ciel</w> <w n="30.4">bleu</w> <w n="30.5">leurs</w> <w n="30.6">cris</w> ! <w n="30.7">Derrière</w> <w n="30.8">eux</w></l>
					<l n="31" num="8.3"><w n="31.1">Fiers</w> <w n="31.2">de</w> <w n="31.3">leurs</w> <w n="31.4">trésors</w>, <w n="31.5">le</w> <w n="31.6">père</w> <w n="31.7">et</w> <w n="31.8">la</w> <w n="31.9">mère</w></l>
					<l n="32" num="8.4"><w n="32.1">Tout</w> <w n="32.2">émerveillés</w> <w n="32.3">les</w> <w n="32.4">couvent</w> <w n="32.5">des</w> <w n="32.6">yeux</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Le</w> <w n="33.2">père</w> <w n="33.3">adorant</w> <w n="33.4">leur</w> <w n="33.5">gaîté</w> <w n="33.6">naïve</w></l>
					<l n="34" num="9.2"><w n="34.1">Fredonne</w>, <w n="34.2">joyeux</w>, <w n="34.3">léger</w>, <w n="34.4">ébloui</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">la</w> <w n="35.3">jeune</w> <w n="35.4">mère</w> <w n="35.5">émue</w> <w n="35.6">et</w> <w n="35.7">pensive</w></l>
					<l n="36" num="9.4"><w n="36.1">Sourit</w> <w n="36.2">aux</w> <w n="36.3">bébés</w>, <w n="36.4">se</w> <w n="36.5">penchant</w> <w n="36.6">vers</w> <w n="36.7">lui</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">tous</w> <w n="37.3">deux</w> <w n="37.4">unis</w> <w n="37.5">des</w> <w n="37.6">douces</w> <w n="37.7">étreintes</w></l>
					<l n="38" num="10.2"><w n="38.1">Sentent</w> <w n="38.2">un</w> <w n="38.3">seul</w> <w n="38.4">cœur</w> <w n="38.5">battre</w> <w n="38.6">dans</w> <w n="38.7">leurs</w> <w n="38.8">seins</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Éternellement</w> <w n="39.2">leurs</w> <w n="39.3">âmes</w> <w n="39.4">sont</w> <w n="39.5">jointes</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Les</w> <w n="40.2">bonheurs</w> <w n="40.3">vers</w> <w n="40.4">eux</w> <w n="40.5">volent</w> <w n="40.6">en</w> <w n="40.7">essaims</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Et</w> <w n="41.2">leurs</w> <w n="41.3">doux</w> <w n="41.4">frissons</w>, <w n="41.5">leurs</w> <w n="41.6">mains</w> <w n="41.7">enlacées</w></l>
					<l n="42" num="11.2"><w n="42.1">Leurs</w> <w n="42.2">yeux</w> <w n="42.3">attendris</w> <w n="42.4">et</w> <w n="42.5">noyés</w> <w n="42.6">d</w>'<w n="42.7">amours</w></l>
					<l n="43" num="11.3"><w n="43.1">Disent</w> <w n="43.2">le</w> <w n="43.3">secret</w> <w n="43.4">pur</w> <w n="43.5">de</w> <w n="43.6">leurs</w> <w n="43.7">pensées</w> :</l>
					<l n="44" num="11.4">« <w n="44.1">Nous</w> <w n="44.2">sommes</w> <w n="44.3">heureux</w>, <w n="44.4">liés</w> <w n="44.5">pour</w> <w n="44.6">toujours</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">» <w n="45.1">Notre</w> <w n="45.2">âme</w> <w n="45.3">à</w> <w n="45.4">tous</w> <w n="45.5">deux</w> <w n="45.6">revit</w> <w n="45.7">palpitante</w></l>
					<l n="46" num="12.2">» <w n="46.1">Dans</w> <w n="46.2">chaque</w> <w n="46.3">ange</w> <w n="46.4">né</w> <w n="46.5">de</w> <w n="46.6">notre</w> <w n="46.7">baiser</w>,</l>
					<l n="47" num="12.3">» <w n="47.1">Ils</w> <w n="47.2">sont</w> <w n="47.3">pour</w> <w n="47.4">nos</w> <w n="47.5">cœurs</w> <w n="47.6">la</w> <w n="47.7">chaîne</w> <w n="47.8">vivante</w></l>
					<l n="48" num="12.4">» <w n="48.1">Que</w> <w n="48.2">rien</w> <w n="48.3">ici</w>-<w n="48.4">bas</w> <w n="48.5">ne</w> <w n="48.6">pourra</w> <w n="48.7">briser</w> ! »</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Oh</w> ! <w n="49.2">quel</w> <w n="49.3">avenir</w> <w n="49.4">leur</w> <w n="49.5">amour</w> <w n="49.6">carresse</w> !</l>
					<l n="50" num="13.2"><w n="50.1">Leurs</w> <w n="50.2">rêves</w> <w n="50.3">s</w>'<w n="50.4">en</w> <w n="50.5">vont</w> <w n="50.6">dans</w> <w n="50.7">les</w> <w n="50.8">cieux</w> <w n="50.9">ouverts</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Et</w> <w n="51.2">leur</w> <w n="51.3">œil</w> <w n="51.4">ravi</w> <w n="51.5">suit</w> <w n="51.6">avec</w> <w n="51.7">ivresse</w></l>
					<l n="52" num="13.4"><w n="52.1">Les</w> <w n="52.2">bébés</w> <w n="52.3">courant</w> <w n="52.4">dans</w> <w n="52.5">les</w> <w n="52.6">grands</w> <w n="52.7">prés</w> <w n="52.8">verts</w>.</l>
				</lg>
			</div></body></text></TEI>