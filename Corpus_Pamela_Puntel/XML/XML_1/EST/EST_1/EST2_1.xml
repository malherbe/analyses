<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="EST">
					<name>
						<forename>Hector</forename>
						<surname>L'ESTRAZ</surname>
					</name>
					<date from="1848" to="1936">1848-1936</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>112 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">EST_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>HECTOR L'ESTRAZ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb312226608</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES (D'APRÈS LES DESSINS D'ÉMILE BAYARD)</title>
								<author>HECTOR L'ESTRAZ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE ARTISTIQUE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="EST2">
				<head type="main">1871</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">les</w> <w n="1.3">guérets</w> <w n="1.4">féconds</w> <w n="1.5">où</w> <w n="1.6">les</w> <w n="1.7">futures</w> <w n="1.8">gerbes</w></l>
					<l n="2" num="1.2"><w n="2.1">Offraient</w> <w n="2.2">leur</w> <w n="2.3">espérance</w> <w n="2.4">en</w> <w n="2.5">verdoyants</w> <w n="2.6">épis</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">les</w> <w n="3.3">prés</w> <w n="3.4">où</w> <w n="3.5">les</w> <w n="3.6">fleurs</w> <w n="3.7">vermeilles</w> <w n="3.8">dans</w> <w n="3.9">les</w> <w n="3.10">herbes</w></l>
					<l n="4" num="1.4"><w n="4.1">Faisaient</w> <w n="4.2">aux</w> <w n="4.3">amoureux</w> <w n="4.4">de</w> <w n="4.5">splendides</w> <w n="4.6">tapis</w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Dans</w> <w n="5.2">les</w> <w n="5.3">bosquets</w> <w n="5.4">ombreux</w> <w n="5.5">emplis</w> <w n="5.6">de</w> <w n="5.7">frais</w> <w n="5.8">murmures</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">les</w> <w n="6.3">sentiers</w> <w n="6.4">secrets</w> <w n="6.5">où</w> <w n="6.6">l</w>'<w n="6.7">on</w> <w n="6.8">va</w> <w n="6.9">deux</w> <w n="6.10">à</w> <w n="6.11">deux</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Dans</w> <w n="7.2">ces</w> <w n="7.3">riants</w> <w n="7.4">massifs</w> <w n="7.5">dont</w> <w n="7.6">les</w> <w n="7.7">vertes</w> <w n="7.8">ramures</w></l>
					<l n="8" num="2.4"><w n="8.1">Ouïrent</w> <w n="8.2">tant</w> <w n="8.3">de</w> <w n="8.4">mots</w> <w n="8.5">d</w>'<w n="8.6">amour</w> <w n="8.7">et</w> <w n="8.8">tant</w> <w n="8.9">d</w>'<w n="8.10">aveux</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Dans</w> <w n="9.2">ces</w> <w n="9.3">vallons</w> <w n="9.4">aimés</w>, <w n="9.5">pleins</w> <w n="9.6">de</w> <w n="9.7">joie</w> <w n="9.8">et</w> <w n="9.9">de</w> <w n="9.10">vie</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Où</w> <w n="10.2">la</w> <w n="10.3">fauvette</w> <w n="10.4">avait</w> <w n="10.5">son</w> <w n="10.6">nid</w> <w n="10.7">dans</w> <w n="10.8">les</w> <w n="10.9">buissons</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Où</w> <w n="11.2">l</w>'<w n="11.3">on</w> <w n="11.4">sentait</w> <w n="11.5">son</w> <w n="11.6">âme</w> <w n="11.7">enivrée</w> <w n="11.8">et</w> <w n="11.9">ravie</w></l>
					<l n="12" num="3.4"><w n="12.1">Des</w> <w n="12.2">rayons</w> <w n="12.3">du</w> <w n="12.4">printemps</w>, <w n="12.5">du</w> <w n="12.6">rire</w> <w n="12.7">et</w> <w n="12.8">des</w> <w n="12.9">chansons</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Tout</w> <w n="13.2">est</w> <w n="13.3">ruine</w> <w n="13.4">et</w> <w n="13.5">deuil</w> !— <w n="13.6">L</w>'<w n="13.7">ouragan</w> <w n="13.8">des</w> <w n="13.9">batailles</w></l>
					<l n="14" num="4.2"><w n="14.1">A</w> <w n="14.2">balayé</w> <w n="14.3">les</w> <w n="14.4">fleurs</w> <w n="14.5">dans</w> <w n="14.6">ses</w> <w n="14.7">noirs</w> <w n="14.8">tourbillons</w></l>
					<l n="15" num="4.3"><w n="15.1">Les</w> <w n="15.2">arbres</w> <w n="15.3">sont</w> <w n="15.4">hachés</w> <w n="15.5">de</w> <w n="15.6">profondes</w> <w n="15.7">entailles</w>,</l>
					<l n="16" num="4.4"><w n="16.1">La</w> <w n="16.2">plaine</w> <w n="16.3">a</w> <w n="16.4">sur</w> <w n="16.5">ses</w> <w n="16.6">flancs</w> <w n="16.7">de</w> <w n="16.8">lugubres</w> <w n="16.9">sillons</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">La</w> <w n="17.2">colère</w> <w n="17.3">de</w> <w n="17.4">l</w>'<w n="17.5">homme</w> <w n="17.6">implacable</w> <w n="17.7">et</w> <w n="17.8">fatale</w></l>
					<l n="18" num="5.2"><w n="18.1">Ici</w> <w n="18.2">s</w>'<w n="18.3">est</w> <w n="18.4">déchaînée</w> <w n="18.5">en</w> <w n="18.6">tempête</w> <w n="18.7">de</w> <w n="18.8">feu</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Dans</w> <w n="19.2">son</w> <w n="19.3">sauvage</w> <w n="19.4">effet</w> <w n="19.5">mille</w> <w n="19.6">fois</w> <w n="19.7">plus</w> <w n="19.8">brutale</w></l>
					<l n="20" num="5.4"><w n="20.1">Que</w> <w n="20.2">la</w> <w n="20.3">foudre</w> <w n="20.4">éclatante</w> <w n="20.5">entre</w> <w n="20.6">les</w> <w n="20.7">mains</w> <w n="20.8">de</w> <w n="20.9">Dieu</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">La</w> <w n="21.2">guerre</w> <w n="21.3">a</w> <w n="21.4">passé</w> <w n="21.5">là</w>. — <w n="21.6">Tout</w> <w n="21.7">est</w> <w n="21.8">mort</w> ! — <w n="21.9">Il</w> <w n="21.10">ne</w> <w n="21.11">reste</w></l>
					<l n="22" num="6.2"><w n="22.1">Que</w> <w n="22.2">des</w> <w n="22.3">débris</w> ! — <w n="22.4">Où</w> <w n="22.5">sont</w> <w n="22.6">les</w> <w n="22.7">espoirs</w> <w n="22.8">décevants</w> ?…</l>
					<l n="23" num="6.3"><w n="23.1">La</w> <w n="23.2">guerre</w> <w n="23.3">a</w> <w n="23.4">passé</w> <w n="23.5">là</w> ! <w n="23.6">couchant</w> <w n="23.7">d</w>'<w n="23.8">un</w> <w n="23.9">coup</w> <w n="23.10">funeste</w></l>
					<l n="24" num="6.4"><w n="24.1">Sa</w> <w n="24.2">sanglante</w> <w n="24.3">moisson</w> <w n="24.4">d</w>'<w n="24.5">hommes</w>, <w n="24.6">épis</w> <w n="24.7">vivants</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">— <w n="25.1">Et</w> <w n="25.2">dans</w> <w n="25.3">la</w> <w n="25.4">plaine</w>, <w n="25.5">va</w>, <w n="25.6">pareille</w> <w n="25.7">à</w> <w n="25.8">quelque</w> <w n="25.9">aïeule</w></l>
					<l n="26" num="7.2"><w n="26.1">Évoquant</w> <w n="26.2">du</w> <w n="26.3">passé</w> <w n="26.4">les</w> <w n="26.5">bonheurs</w> <w n="26.6">fugitifs</w>,</l>
					<l n="27" num="7.3"><w n="27.1">D</w>'<w n="27.2">un</w> <w n="27.3">pas</w> <w n="27.4">désespéré</w>, <w n="27.5">la</w> <w n="27.6">pauvre</w> <w n="27.7">femme</w>, <w n="27.8">seule</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Seule</w> <w n="28.2">avec</w> <w n="28.3">les</w> <w n="28.4">bébés</w> <w n="28.5">effarés</w> <w n="28.6">et</w> <w n="28.7">pensifs</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">D</w>'<w n="29.2">un</w> <w n="29.3">œil</w> <w n="29.4">sombre</w> <w n="29.5">cherchant</w> <w n="29.6">l</w>'<w n="29.7">absent</w> <w n="29.8">dont</w> <w n="29.9">son</w> <w n="29.10">Cœur</w> <w n="29.11">rêve</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Elle</w> <w n="30.2">s</w>'<w n="30.3">en</w> <w n="30.4">va</w> <w n="30.5">traînant</w> <w n="30.6">et</w> <w n="30.7">sa</w> <w n="30.8">vie</w> <w n="30.9">et</w> <w n="30.10">son</w> <w n="30.11">deuil</w>.</l>
					<l n="31" num="8.3"><w n="31.1">Pour</w> <w n="31.2">elle</w>, <w n="31.3">le</w> <w n="31.4">soleil</w> <w n="31.5">qui</w> <w n="31.6">dans</w> <w n="31.7">les</w> <w n="31.8">cieux</w> <w n="31.9">se</w> <w n="31.10">lève</w>,</l>
					<l n="32" num="8.4"><w n="32.1">C</w>'<w n="32.2">est</w> <w n="32.3">la</w> <w n="32.4">lampe</w> <w n="32.5">de</w> <w n="32.6">mort</w> <w n="32.7">brûlant</w> <w n="32.8">près</w> <w n="32.9">d</w>'<w n="32.10">un</w> <w n="32.11">cercueil</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Elle</w> <w n="33.2">soupire</w> : « <w n="33.3">Hélas</w>, <w n="33.4">déchirement</w> <w n="33.5">des</w> <w n="33.6">âmes</w> !</l>
					<l n="34" num="9.2">» <w n="34.1">Ces</w> <w n="34.2">lieux</w> <w n="34.3">riants</w> <w n="34.4">et</w> <w n="34.5">doux</w> <w n="34.6">sont</w> <w n="34.7">désolés</w>, — <w n="34.8">oh</w> ! <w n="34.9">voi</w>,</l>
					<l n="35" num="9.3">» <w n="35.1">Dans</w> <w n="35.2">ces</w> <w n="35.3">bois</w>, <w n="35.4">dans</w> <w n="35.5">ces</w> <w n="35.6">prés</w> <w n="35.7">jadis</w> <w n="35.8">nous</w> <w n="35.9">nous</w> <w n="35.10">aimames</w>,</l>
					<l n="36" num="9.4">» <w n="36.1">O</w> <w n="36.2">mon</w> <w n="36.3">cher</w> <w n="36.4">endormi</w>, <w n="36.5">vois</w> ! <w n="36.6">j</w>'<w n="36.7">y</w> <w n="36.8">reviens</w> <w n="36.9">sans</w> <w n="36.10">toi</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">» <w n="37.1">Pourquoi</w> <w n="37.2">n</w>'<w n="37.3">es</w>-<w n="37.4">tu</w> <w n="37.5">pas</w> <w n="37.6">là</w>, <w n="37.7">toi</w> <w n="37.8">que</w> <w n="37.9">toujours</w> <w n="37.10">je</w> <w n="37.11">pleure</w> ;</l>
					<l n="38" num="10.2">» <w n="38.1">Pourquoi</w> <w n="38.2">donc</w> <w n="38.3">nous</w> <w n="38.4">quitter</w>, <w n="38.5">toi</w> <w n="38.6">que</w> <w n="38.7">j</w>'<w n="38.8">attends</w> <w n="38.9">toujours</w>,</l>
					<l n="39" num="10.3">» <w n="39.1">Pourquoi</w> <w n="39.2">t</w>'<w n="39.3">en</w> <w n="39.4">être</w> <w n="39.5">allé</w> <w n="39.6">si</w> <w n="39.7">longtemps</w> <w n="39.8">avant</w> <w n="39.9">l</w>'<w n="39.10">heure</w>,</l>
					<l n="40" num="10.4">» <w n="40.1">Pourquoi</w> <w n="40.2">donc</w> <w n="40.3">en</w> <w n="40.4">leur</w> <w n="40.5">fleur</w> <w n="40.6">briser</w> <w n="40.7">tous</w> <w n="40.8">nos</w> <w n="40.9">amours</w> ?</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">» <w n="41.1">Ne</w> <w n="41.2">t</w>'<w n="41.3">en</w> <w n="41.4">souvient</w>-<w n="41.5">il</w> <w n="41.6">plus</w> <w n="41.7">que</w> <w n="41.8">j</w>'<w n="41.9">étais</w> <w n="41.10">ton</w> <w n="41.11">idole</w>,</l>
					<l n="42" num="11.2">» <w n="42.1">Et</w> <w n="42.2">toi</w> <w n="42.3">mon</w> <w n="42.4">cher</w> <w n="42.5">soutien</w> ? — <w n="42.6">Pourquoi</w> <w n="42.7">n</w>'<w n="42.8">es</w>-<w n="42.9">tu</w> <w n="42.10">plus</w> <w n="42.11">là</w> ?</l>
					<l n="43" num="11.3">» <w n="43.1">Oh</w> ! <w n="43.2">dis</w>-<w n="43.3">moi</w>, — <w n="43.4">que</w> <w n="43.5">mon</w> <w n="43.6">cœur</w> <w n="43.7">avec</w> <w n="43.8">mon</w> <w n="43.9">âme</w> <w n="43.10">y</w> <w n="43.11">vole</w>,</l>
					<l n="44" num="11.4">» <w n="44.1">Dis</w>-<w n="44.2">le</w> <w n="44.3">moi</w> <w n="44.4">le</w> <w n="44.5">pays</w> <w n="44.6">où</w> <w n="44.7">la</w> <w n="44.8">mort</w> <w n="44.9">t</w>'<w n="44.10">exila</w> !… »</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">— <w n="45.1">Puis</w>, <w n="45.2">morne</w>, <w n="45.3">contemplant</w> <w n="45.4">les</w> <w n="45.5">bébés</w> <w n="45.6">qu</w>'<w n="45.7">elle</w> <w n="45.8">adore</w> :</l>
					<l n="46" num="12.2">« <w n="46.1">Souvenirs</w> <w n="46.2">les</w> <w n="46.3">plus</w> <w n="46.4">chers</w> <w n="46.5">de</w> <w n="46.6">l</w>'<w n="46.7">amour</w> <w n="46.8">effacé</w>,</l>
					<l n="47" num="12.3">» <w n="47.1">Pauvres</w> <w n="47.2">anges</w>, <w n="47.3">croissez</w> ! <w n="47.4">vous</w> <w n="47.5">sourirez</w> <w n="47.6">encore</w></l>
					<l n="48" num="12.4">» <w n="48.1">Vous</w>, <w n="48.2">vivez</w> <w n="48.3">d</w>'<w n="48.4">avenir</w> ! — <w n="48.5">moi</w> <w n="48.6">je</w> <w n="48.7">vis</w> <w n="48.8">du</w> <w n="48.9">passé</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">» <w n="49.1">Vos</w> <w n="49.2">yeux</w> <w n="49.3">où</w> <w n="49.4">mes</w> <w n="49.5">baisers</w> <w n="49.6">vont</w> <w n="49.7">essuyer</w> <w n="49.8">vos</w> <w n="49.9">larmes</w></l>
					<l n="50" num="13.2">» <w n="50.1">Auront</w> <w n="50.2">bientôt</w> <w n="50.3">brillé</w>, <w n="50.4">bientôt</w> <w n="50.5">auront</w> <w n="50.6">souri</w>,</l>
					<l n="51" num="13.3">» <w n="51.1">La</w> <w n="51.2">vie</w> <w n="51.3">aura</w> <w n="51.4">pour</w> <w n="51.5">vous</w> <w n="51.6">son</w> <w n="51.7">bonheur</w> <w n="51.8">et</w> <w n="51.9">ses</w> <w n="51.10">charmes</w>,</l>
					<l n="52" num="13.4">» <w n="52.1">Mais</w> <w n="52.2">pour</w> <w n="52.3">moi</w> <w n="52.4">le</w> <w n="52.5">bonheur</w> <w n="52.6">en</w> <w n="52.7">sa</w> <w n="52.8">source</w> <w n="52.9">est</w> <w n="52.10">tari</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">» <w n="53.1">Vos</w> <w n="53.2">yeux</w> <w n="53.3">pourront</w> <w n="53.4">encor</w>, <w n="53.5">heureux</w>, <w n="53.6">pleins</w> <w n="53.7">de</w> <w n="53.8">lumière</w>,</l>
					<l n="54" num="14.2">» <w n="54.1">Se</w> <w n="54.2">lever</w> <w n="54.3">vers</w> <w n="54.4">le</w> <w n="54.5">ciel</w> ; — <w n="54.6">moi</w>, <w n="54.7">je</w> <w n="54.8">n</w>'<w n="54.9">ai</w> <w n="54.10">qu</w>'<w n="54.11">à</w> <w n="54.12">gémir</w></l>
					<l n="55" num="14.3">» <w n="55.1">En</w> <w n="55.2">contemplant</w> <w n="55.3">la</w> <w n="55.4">terre</w>, <w n="55.5">espérance</w> <w n="55.6">dernière</w></l>
					<l n="56" num="14.4">» <w n="56.1">Où</w> <w n="56.2">près</w> <w n="56.3">du</w> <w n="56.4">trépassé</w> <w n="56.5">je</w> <w n="56.6">voudrais</w> <w n="56.7">m</w>'<w n="56.8">endormir</w>. »</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">— <w n="57.1">Et</w> <w n="57.2">dans</w> <w n="57.3">la</w> <w n="57.4">plaine</w>, <w n="57.5">va</w>, <w n="57.6">pareille</w> <w n="57.7">à</w> <w n="57.8">quelque</w> <w n="57.9">aïeule</w></l>
					<l n="58" num="15.2"><w n="58.1">Évoquant</w> <w n="58.2">du</w> <w n="58.3">passé</w> <w n="58.4">les</w> <w n="58.5">bonheurs</w> <w n="58.6">fugitifs</w></l>
					<l n="59" num="15.3"><w n="59.1">D</w>'<w n="59.2">un</w> <w n="59.3">pas</w> <w n="59.4">désespéré</w>, <w n="59.5">la</w> <w n="59.6">pauvre</w> <w n="59.7">femme</w>, <w n="59.8">seule</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Seule</w> <w n="60.2">avec</w> <w n="60.3">les</w> <w n="60.4">bébés</w> <w n="60.5">effarés</w> <w n="60.6">et</w> <w n="60.7">pensifs</w>.</l>
				</lg>
			</div></body></text></TEI>