<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">JE VOUS SALUE GUILLAUME LE VAINQUEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="LCB">
					<name>
						<forename>Jean-Baptiste</forename>
						<surname>LACOMBE</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>90 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">LCB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>JE VOUS SALUE GUILLAUME LE VAINQUEUR</title>
						<author>JEAN-BAPTISTE LACOMBE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k6506752d.r=J.%20B.%20LACOMBE%2C%20%20%20JE%20VOUS%20SALUE%20GUILLAUME%20LE%20VAINQUEUR%20PARIS?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>JE VOUS SALUE GUILLAUME LE VAINQUEUR</title>
								<author>JEAN-BAPTISTE LACOMBE</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="LCB1">
				<head type="main">JE VOUS SALUE GUILLAUME LE VAINQUEUR PARIS</head>
				<opener>
					<epigraph>
						<cit>
							<quote>AveCesar.</quote>
							<bibl>
								Cette facétie fut prononcée par le grand-duc de Bade,<lb></lb>
								gendre de Guillaume de Prusse, au palais de Versailles,<lb></lb>
								le Ier janvier 1871.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Lorsque</w>, <w n="1.2">tout</w> <w n="1.3">frissonnant</w> <w n="1.4">des</w> <w n="1.5">sueurs</w> <w n="1.6">de</w> <w n="1.7">la</w> <w n="1.8">honte</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Loin</w> <w n="2.2">des</w> <w n="2.3">bruits</w>, <w n="2.4">anxieux</w>, <w n="2.5">sans</w> <w n="2.6">témoins</w>, <w n="2.7">on</w> <w n="2.8">affronte</w></l>
					<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">grand</w> <w n="3.3">livre</w> <w n="3.4">où</w> <w n="3.5">Tacite</w> <w n="3.6">enferma</w> <w n="3.7">les</w> <w n="3.8">Césars</w>… ;</l>
					<l n="4" num="1.4"><w n="4.1">Lorsqu</w>'<w n="4.2">on</w> <w n="4.3">a</w> <w n="4.4">sous</w> <w n="4.5">les</w> <w n="4.6">yeux</w> <w n="4.7">tous</w> <w n="4.8">les</w> <w n="4.9">lambeaux</w> <w n="4.10">épars</w></l>
					<l n="5" num="1.5"><w n="5.1">Qui</w> <w n="5.2">forment</w> <w n="5.3">la</w> <w n="5.4">légende</w>, <w n="5.5">et</w> <w n="5.6">que</w>, <w n="5.7">l</w>'<w n="5.8">âme</w> <w n="5.9">froissée</w>,</l>
					<l n="6" num="1.6"><w n="6.1">On</w> <w n="6.2">peut</w>, <w n="6.3">résolûment</w>, <w n="6.4">la</w> <w n="6.5">manche</w> <w n="6.6">retroussée</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Fouiller</w> <w n="7.2">l</w>'<w n="7.3">entassement</w> <w n="7.4">des</w> <w n="7.5">noms</w> <w n="7.6">accumulés</w></l>
					<l n="8" num="1.8"><w n="8.1">De</w> <w n="8.2">tous</w> <w n="8.3">les</w> <w n="8.4">malfaiteurs</w> <w n="8.5">des</w> <w n="8.6">siècles</w> <w n="8.7">écoulés</w>… ;</l>
					<l n="9" num="1.9"><w n="9.1">Quand</w> <w n="9.2">on</w> <w n="9.3">peut</w> <w n="9.4">soupeser</w> <w n="9.5">les</w> <w n="9.6">feuillets</w> <w n="9.7">de</w> <w n="9.8">l</w>'<w n="9.9">histoire</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Pour</w> <w n="10.2">savoir</w> <w n="10.3">de</w> <w n="10.4">quels</w> <w n="10.5">maux</w> <w n="10.6">se</w> <w n="10.7">compose</w> <w n="10.8">la</w> <w n="10.9">gloire</w>…</l>
					<l n="11" num="1.11"><w n="11.1">Guillaume</w>, <w n="11.2">on</w> <w n="11.3">cherche</w> <w n="11.4">en</w> <w n="11.5">vain</w>, <w n="11.6">parmi</w> <w n="11.7">tous</w> <w n="11.8">ces</w> <w n="11.9">héros</w></l>
					<l n="12" num="1.12"><w n="12.1">Dont</w> <w n="12.2">la</w> <w n="12.3">lime</w> <w n="12.4">du</w> <w n="12.5">temps</w> <w n="12.6">n</w>'<w n="12.7">a</w> <w n="12.8">pu</w> <w n="12.9">mordre</w> <w n="12.10">les</w> <w n="12.11">os</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Celui</w> <w n="13.2">qui</w>, <w n="13.3">plus</w> <w n="13.4">que</w> <w n="13.5">toi</w>, <w n="13.6">mettant</w> <w n="13.7">crime</w> <w n="13.8">sur</w> <w n="13.9">crimes</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Dans</w> <w n="14.2">nos</w> <w n="14.3">champs</w> <w n="14.4">désolés</w> <w n="14.5">entassa</w> <w n="14.6">de</w> <w n="14.7">victimes</w>…</l>
				</lg>
				<lg n="2">
					<l n="15" num="2.1"><w n="15.1">Vainement</w> <w n="15.2">j</w>'<w n="15.3">interroge</w>, <w n="15.4">allant</w> <w n="15.5">jusqu</w>'<w n="15.6">à</w> <w n="15.7">Néron</w>,</l>
					<l n="16" num="2.2"><w n="16.1">Pour</w> <w n="16.2">voir</w> <w n="16.3">s</w>'<w n="16.4">il</w> <w n="16.5">en</w> <w n="16.6">est</w> <w n="16.7">un</w>. –– <w n="16.8">L</w>'<w n="16.9">histoire</w> <w n="16.10">me</w> <w n="16.11">dit</w> : « <w n="16.12">Non</w> ! »</l>
					<l n="17" num="2.3"><w n="17.1">De</w> <w n="17.2">Cartouche</w> <w n="17.3">à</w> <w n="17.4">Mandrin</w>, <w n="17.5">jusques</w> <w n="17.6">à</w> <w n="17.7">Charlemagne</w>,</l>
					<l n="18" num="2.4"><w n="18.1">Sur</w> <w n="18.2">le</w> <w n="18.3">velours</w> <w n="18.4">du</w> <w n="18.5">trône</w> <w n="18.6">et</w> <w n="18.7">sur</w> <w n="18.8">les</w> <w n="18.9">bancs</w> <w n="18.10">du</w> <w n="18.11">bagne</w>,</l>
					<l n="19" num="2.5"><w n="19.1">Depuis</w> <w n="19.2">Caligula</w>, <w n="19.3">Winceslas</w>, <w n="19.4">Sigismond</w>,</l>
					<l n="20" num="2.6"><w n="20.1">D</w>'<w n="20.2">autres</w> <w n="20.3">que</w> <w n="20.4">l</w>'<w n="20.5">on</w> <w n="20.6">retrouve</w> <w n="20.7">en</w> <w n="20.8">creusant</w> <w n="20.9">plus</w> <w n="20.10">profond</w>,</l>
					<l n="21" num="2.7"><w n="21.1">Il</w> <w n="21.2">n</w>'<w n="21.3">est</w> <w n="21.4">pas</w> <w n="21.5">un</w> <w n="21.6">bandit</w>, <w n="21.7">pas</w> <w n="21.8">un</w> <w n="21.9">porte</w>-<w n="21.10">ferraille</w>,</l>
					<l n="22" num="2.8"><w n="22.1">Qui</w>, <w n="22.2">comme</w> <w n="22.3">criminel</w>, <w n="22.4">se</w> <w n="22.5">mesure</w> <w n="22.6">à</w> <w n="22.7">ta</w> <w n="22.8">taille</w> !</l>
				</lg>
				<lg n="3">
					<l n="23" num="3.1"><w n="23.1">Et</w> <w n="23.2">c</w>'<w n="23.3">est</w> <w n="23.4">toi</w>, <w n="23.5">tigre</w>-<w n="23.6">roi</w>, <w n="23.7">sinistre</w> <w n="23.8">maraudeur</w>,</l>
					<l n="24" num="3.2"><w n="24.1">Qu</w>'<w n="24.2">on</w> <w n="24.3">ose</w> <w n="24.4">proclamer</w> <w n="24.5">Guillaume</w> <w n="24.6">le</w> <w n="24.7">Vainqueur</w> ?…</l>
					<l n="25" num="3.3"><w n="25.1">C</w>'<w n="25.2">est</w> <w n="25.3">à</w> <w n="25.4">devenir</w> <w n="25.5">fou</w> ! –– <w n="25.6">Que</w> <w n="25.7">ton</w> <w n="25.8">gendre</w> <w n="25.9">de</w> <w n="25.10">Bade</w>,</l>
					<l n="26" num="3.4"><w n="26.1">L</w>'<w n="26.2">égorgeur</w> <w n="26.3">de</w> <w n="26.4">Strasbourg</w>, <w n="26.5">te</w> <w n="26.6">donnant</w> <w n="26.7">l</w>'<w n="26.8">accolade</w></l>
					<l n="27" num="3.5"><w n="27.1">Au</w> <w n="27.2">nom</w> <w n="27.3">des</w> <w n="27.4">souverains</w>, <w n="27.5">princes</w> <w n="27.6">et</w> <w n="27.7">hauts</w> <w n="27.8">barons</w></l>
					<l n="28" num="3.6"><w n="28.1">Que</w> <w n="28.2">déjà</w> <w n="28.3">nos</w> <w n="28.4">enfants</w> <w n="28.5">appellent</w> <w n="28.6">hauts</w> <w n="28.7">larrons</w>,</l>
					<l n="29" num="3.7"><w n="29.1">S</w>'<w n="29.2">en</w> <w n="29.3">vienne</w>, <w n="29.4">maîtrisant</w> <w n="29.5">un</w> <w n="29.6">éclat</w> <w n="29.7">de</w> <w n="29.8">fou</w> <w n="29.9">rire</w>,</l>
					<l n="30" num="3.8"><w n="30.1">Te</w> <w n="30.2">taper</w> <w n="30.3">sur</w> <w n="30.4">le</w> <w n="30.5">ventre</w> <w n="30.6">et</w> <w n="30.7">te</w> <w n="30.8">donner</w> <w n="30.9">du</w> « <w n="30.10">Sire</w> »,</l>
					<l n="31" num="3.9"><w n="31.1">Ces</w> <w n="31.2">choses</w>-<w n="31.3">là</w> <w n="31.4">se</w> <w n="31.5">font</w> <w n="31.6">peut</w>-<w n="31.7">être</w> <w n="31.8">en</w> <w n="31.9">carnaval</w> ;</l>
					<l n="32" num="3.10"><w n="32.1">Mais</w> <w n="32.2">n</w>'<w n="32.3">être</w> <w n="32.4">que</w> <w n="32.5">Guillaume</w> <w n="32.6">et</w> <w n="32.7">se</w> <w n="32.8">croire</w> <w n="32.9">Annibal</w>,</l>
					<l n="33" num="3.11"><w n="33.1">S</w>'<w n="33.2">amuser</w> <w n="33.3">au</w> <w n="33.4">César</w> ! <w n="33.5">essayer</w> <w n="33.6">la</w> <w n="33.7">couronne</w></l>
					<l n="34" num="3.12"><w n="34.1">Que</w> <w n="34.2">porta</w> <w n="34.3">Charles</w>-<w n="34.4">Quint</w> !… <w n="34.5">Sur</w> <w n="34.6">l</w>'<w n="34.7">honneur</w>, <w n="34.8">je</w> <w n="34.9">m</w>'<w n="34.10">étonne</w></l>
					<l n="35" num="3.13"><w n="35.1">Que</w> <w n="35.2">ton</w> <w n="35.3">bouffon</w> <w n="35.4">Bismarck</w>, <w n="35.5">ce</w> <w n="35.6">valet</w> <w n="35.7">sans</w> <w n="35.8">pudeur</w>,</l>
					<l n="36" num="3.14"><w n="36.1">Ne</w> <w n="36.2">t</w>'<w n="36.3">ait</w> <w n="36.4">pas</w> <w n="36.5">dit</w>, <w n="36.6">ô</w> <w n="36.7">roi</w> ! <w n="36.8">qu</w>'<w n="36.9">un</w> <w n="36.10">sceptre</w> <w n="36.11">d</w>'<w n="36.12">empereur</w></l>
					<l n="37" num="3.15"><w n="37.1">Est</w> <w n="37.2">un</w> <w n="37.3">roseau</w> <w n="37.4">fragile</w> <w n="37.5">en</w> <w n="37.6">le</w> <w n="37.7">siècle</w> <w n="37.8">où</w> <w n="37.9">nous</w> <w n="37.10">sommes</w>…</l>
					<l n="38" num="3.16"><w n="38.1">Siècle</w> <w n="38.2">où</w> <w n="38.3">Dieu</w> <w n="38.4">n</w>'<w n="38.5">est</w> <w n="38.6">plus</w> <w n="38.7">là</w> <w n="38.8">pour</w> <w n="38.9">signer</w> <w n="38.10">vos</w> <w n="38.11">diplômes</w>… ;</l>
					<l n="39" num="3.17"><w n="39.1">Qu</w>'<w n="39.2">il</w> <w n="39.3">ne</w> <w n="39.4">t</w>'<w n="39.5">ait</w> <w n="39.6">pas</w> <w n="39.7">montré</w> <w n="39.8">l</w>'<w n="39.9">homme</w> <w n="39.10">de</w> <w n="39.11">Friedland</w>,</l>
					<l n="40" num="3.18"><w n="40.1">Et</w> <w n="40.2">celui</w> <w n="40.3">qu</w>'<w n="40.4">on</w> <w n="40.5">a</w> <w n="40.6">pris</w> <w n="40.7">sous</w> <w n="40.8">les</w> <w n="40.9">murs</w> <w n="40.10">de</w> <w n="40.11">Sedan</w> :</l>
					<l n="41" num="3.19"><w n="41.1">L</w>'<w n="41.2">un</w>, <w n="41.3">couvert</w> <w n="41.4">du</w> <w n="41.5">manteau</w> <w n="41.6">que</w> <w n="41.7">brode</w> <w n="41.8">la</w> <w n="41.9">victoire</w> ;</l>
					<l n="42" num="3.20"><w n="42.1">L</w>'<w n="42.2">autre</w> <w n="42.3">sifflé</w>, <w n="42.4">hué</w>, <w n="42.5">conspué</w> <w n="42.6">par</w> <w n="42.7">l</w>'<w n="42.8">histoire</w>…</l>
					<l n="43" num="3.21"><w n="43.1">L</w>'<w n="43.2">un</w>, <w n="43.3">du</w> <w n="43.4">bruit</w> <w n="43.5">de</w> <w n="43.6">ses</w> <w n="43.7">pas</w> <w n="43.8">étonnant</w> <w n="43.9">l</w>'<w n="43.10">univers</w>,</l>
					<l n="44" num="3.22"><w n="44.1">Pendant</w> <w n="44.2">vingt</w> <w n="44.3">ans</w> <w n="44.4">luttant</w>, <w n="44.5">faisant</w> <w n="44.6">face</w> <w n="44.7">aux</w> <w n="44.8">revers</w>,</l>
					<l n="45" num="3.23"><w n="45.1">A</w> <w n="45.2">la</w> <w n="45.3">tête</w> <w n="45.4">des</w> <w n="45.5">siens</w> <w n="45.6">défiant</w> <w n="45.7">la</w> <w n="45.8">mitraille</w>,</l>
					<l n="46" num="3.24"><w n="46.1">Se</w> <w n="46.2">couchant</w> <w n="46.3">tout</w> <w n="46.4">botté</w> <w n="46.5">sur</w> <w n="46.6">les</w> <w n="46.7">champs</w> <w n="46.8">de</w> <w n="46.9">bataille</w>,</l>
					<l n="47" num="3.25"><w n="47.1">N</w>'<w n="47.2">eût</w> <w n="47.3">jamais</w> <w n="47.4">attendu</w>, <w n="47.5">les</w> <w n="47.6">pieds</w> <w n="47.7">sur</w> <w n="47.8">les</w> <w n="47.9">chenets</w>,</l>
					<l n="48" num="3.26"><w n="48.1">Buvant</w> <w n="48.2">du</w> <w n="48.3">champenois</w>, <w n="48.4">qu</w>'<w n="48.5">un</w> <w n="48.6">de</w> <w n="48.7">Moltke</w>, <w n="48.8">un</w> <w n="48.9">von</w> <w n="48.10">Reigtz</w>,</l>
					<l n="49" num="3.27"><w n="49.1">Lâchement</w> <w n="49.2">abrités</w> <w n="49.3">derrière</w> <w n="49.4">une</w> <w n="49.5">fascine</w>,</l>
					<l n="50" num="3.28"><w n="50.1">Lui</w> <w n="50.2">livrassent</w> <w n="50.3">Berlin</w> <w n="50.4">réduit</w> <w n="50.5">par</w> <w n="50.6">la</w> <w n="50.7">famine</w> !…</l>
				</lg>
				<lg n="4">
					<l n="51" num="4.1"><w n="51.1">C</w>'<w n="51.2">est</w> <w n="51.3">l</w>'<w n="51.4">épée</w> <w n="51.5">à</w> <w n="51.6">la</w> <w n="51.7">main</w> <w n="51.8">qu</w>'<w n="51.9">il</w> <w n="51.10">allait</w> <w n="51.11">aux</w> <w n="51.12">combats</w>…</l>
					<l n="52" num="4.2"><w n="52.1">Et</w> <w n="52.2">l</w>'<w n="52.3">on</w> <w n="52.4">peut</w> <w n="52.5">excuser</w> <w n="52.6">l</w>'<w n="52.7">ivresse</w> <w n="52.8">des</w> <w n="52.9">soldats</w>,</l>
					<l n="53" num="4.3"><w n="53.1">Éblouis</w> <w n="53.2">des</w> <w n="53.3">rayons</w> <w n="53.4">de</w> <w n="53.5">ce</w> <w n="53.6">grand</w> <w n="53.7">météore</w>,</l>
					<l n="54" num="4.4"><w n="54.1">Répondant</w> <w n="54.2">toujours</w> : « <w n="54.3">Oui</w> ! » <w n="54.4">quand</w> <w n="54.5">il</w> <w n="54.6">disait</w> : « <w n="54.7">Encore</w> ! »</l>
					<l n="55" num="4.5"><w n="55.1">D</w>'<w n="55.2">avoir</w> <w n="55.3">permis</w>, <w n="55.4">un</w> <w n="55.5">jour</w> <w n="55.6">que</w> <w n="55.7">l</w>'<w n="55.8">Europe</w> <w n="55.9">avait</w> <w n="55.10">peur</w>,</l>
					<l n="56" num="4.6"><w n="56.1">Où</w> <w n="56.2">tous</w> <w n="56.3">les</w> <w n="56.4">souverains</w>, <w n="56.5">affolés</w> <w n="56.6">de</w> <w n="56.7">stupeur</w>,</l>
					<l n="57" num="4.7"><w n="57.1">Se</w> <w n="57.2">tenaient</w> <w n="57.3">cois</w>, <w n="57.4">tapis</w> <w n="57.5">sous</w> <w n="57.6">le</w> <w n="57.7">dais</w> <w n="57.8">de</w> <w n="57.9">leur</w> <w n="57.10">trône</w>,</l>
					<l n="58" num="4.8"><w n="58.1">Que</w> <w n="58.2">ce</w> <w n="58.3">victorieux</w> <w n="58.4">saisisse</w> <w n="58.5">une</w> <w n="58.6">couronne</w>,</l>
					<l n="59" num="4.9"><w n="59.1">Et</w> <w n="59.2">que</w>, <w n="59.3">posant</w> <w n="59.4">son</w> <w n="59.5">pied</w> <w n="59.6">sur</w> <w n="59.7">leurs</w> <w n="59.8">fronts</w> <w n="59.9">consternés</w>,</l>
					<l n="60" num="4.10"><w n="60.1">Il</w> <w n="60.2">dise</w> <w n="60.3">à</w> <w n="60.4">ce</w> <w n="60.5">troupeau</w> <w n="60.6">de</w> <w n="60.7">princes</w> <w n="60.8">détrônés</w> :</l>
					<l n="61" num="4.11">« <w n="61.1">Élus</w> <w n="61.2">du</w> <w n="61.3">droit</w> <w n="61.4">divin</w>, <w n="61.5">je</w> <w n="61.6">suis</w> <w n="61.7">le</w> <w n="61.8">droit</w> <w n="61.9">du</w> <w n="61.10">glaive</w> !…</l>
					<l n="62" num="4.12"><w n="62.1">Vous</w> <w n="62.2">êtes</w> <w n="62.3">ce</w> <w n="62.4">qui</w> <w n="62.5">tombe</w>, <w n="62.6">et</w> <w n="62.7">moi</w> <w n="62.8">ce</w> <w n="62.9">qui</w> <w n="62.10">s</w>'<w n="62.11">élève</w> ;</l>
					<l n="63" num="4.13"><w n="63.1">Vous</w> <w n="63.2">êtes</w> <w n="63.3">le</w> <w n="63.4">passé</w>, <w n="63.5">vous</w> <w n="63.6">êtes</w> <w n="63.7">souvenir</w> !…</l>
					<l n="64" num="4.14"><w n="64.1">Moi</w>, <w n="64.2">je</w> <w n="64.3">suis</w> <w n="64.4">l</w>'<w n="64.5">inconnu</w> <w n="64.6">qui</w> <w n="64.7">s</w>'<w n="64.8">appelle</w> <w n="64.9">avenir</w> !… »</l>
				</lg>
				<lg n="5">
					<l n="65" num="5.1"><w n="65.1">Avenir</w> ! <w n="65.2">mot</w> <w n="65.3">sonore</w> <w n="65.4">avec</w> <w n="65.5">lequel</w> <w n="65.6">on</w> <w n="65.7">grise</w></l>
					<l n="66" num="5.2"><w n="66.1">L</w>'<w n="66.2">humanité</w> <w n="66.3">qui</w> <w n="66.4">marche</w> <w n="66.5">à</w> <w n="66.6">la</w> <w n="66.7">terre</w> <w n="66.8">promise</w>.</l>
					<l n="67" num="5.3"><w n="67.1">Mot</w> <w n="67.2">terrible</w> <w n="67.3">et</w> <w n="67.4">profond</w> ! <w n="67.5">phare</w> <w n="67.6">mystérieux</w></l>
					<l n="68" num="5.4"><w n="68.1">Allumé</w> <w n="68.2">par</w> <w n="68.3">la</w> <w n="68.4">main</w> <w n="68.5">du</w> <w n="68.6">jeune</w> <w n="68.7">ambitieux</w></l>
					<l n="69" num="5.5"><w n="69.1">Qui</w>, <w n="69.2">bravant</w> <w n="69.3">le</w> <w n="69.4">remous</w> <w n="69.5">de</w> <w n="69.6">la</w> <w n="69.7">marée</w> <w n="69.8">humaine</w>,</l>
					<l n="70" num="5.6"><w n="70.1">Vit</w> <w n="70.2">sombrer</w> <w n="70.3">son</w> <w n="70.4">esquif</w> <w n="70.5">au</w> <w n="70.6">cap</w> <w n="70.7">de</w> <w n="70.8">Sainte</w>-<w n="70.9">Hélène</w> !…</l>
				</lg>
				<lg n="6">
					<l n="71" num="6.1"><w n="71.1">Guillaume</w>, <w n="71.2">le</w> <w n="71.3">soldat</w> <w n="71.4">dont</w> <w n="71.5">j</w>'<w n="71.6">épelle</w> <w n="71.7">le</w> <w n="71.8">nom</w></l>
					<l n="72" num="6.2"><w n="72.1">S</w>'<w n="72.2">appela</w> <w n="72.3">Bonaparte</w>, <w n="72.4">et</w> <w n="72.5">puis</w> <w n="72.6">Napoléon</w>.</l>
					<l n="73" num="6.3"><w n="73.1">Il</w> <w n="73.2">avait</w>, <w n="73.3">à</w> <w n="73.4">vingt</w> <w n="73.5">ans</w>, <w n="73.6">conquis</w> <w n="73.7">assez</w> <w n="73.8">de</w> <w n="73.9">gloire</w></l>
					<l n="74" num="6.4"><w n="74.1">Pour</w> <w n="74.2">rester</w> <w n="74.3">sous</w> <w n="74.4">sa</w> <w n="74.5">tente</w> <w n="74.6">et</w> <w n="74.7">lasser</w> <w n="74.8">la</w> <w n="74.9">victoire</w></l>
					<l n="75" num="6.5"><w n="75.1">A</w> <w n="75.2">suivre</w> <w n="75.3">pas</w> <w n="75.4">à</w> <w n="75.5">pas</w> <w n="75.6">ce</w> <w n="75.7">jeune</w> <w n="75.8">conquérant</w>,</l>
					<l n="76" num="6.6"><w n="76.1">Qui</w>, <w n="76.2">des</w> <w n="76.3">bords</w> <w n="76.4">de</w> <w n="76.5">l</w>'<w n="76.6">Adige</w>, <w n="76.7">impétueux</w> <w n="76.8">torrent</w>,</l>
					<l n="77" num="6.7"><w n="77.1">Poursuivant</w> <w n="77.2">sans</w> <w n="77.3">repos</w> <w n="77.4">sa</w> <w n="77.5">marche</w> <w n="77.6">triomphale</w>,</l>
					<l n="78" num="6.8"><w n="78.1">Entra</w> <w n="78.2">victorieux</w> <w n="78.3">dans</w> <w n="78.4">votre</w> <w n="78.5">capitale</w> !…</l>
					<l n="79" num="6.9"><w n="79.1">Il</w> <w n="79.2">avait</w> <w n="79.3">abattu</w> <w n="79.4">l</w>'<w n="79.5">Autriche</w> <w n="79.6">à</w> <w n="79.7">Marengo</w></l>
					<l n="80" num="6.10"><w n="80.1">Et</w> <w n="80.2">dicté</w> <w n="80.3">le</w> <w n="80.4">traité</w> <w n="80.5">de</w> <w n="80.6">Campo</w>-<w n="80.7">Formio</w>… ;</l>
					<l n="81" num="6.11"><w n="81.1">Il</w> <w n="81.2">avait</w> <w n="81.3">le</w> <w n="81.4">Tyrol</w>, <w n="81.5">toute</w> <w n="81.6">la</w> <w n="81.7">Lombardie</w> !</l>
					<l n="82" num="6.12"><w n="82.1">Il</w> <w n="82.2">voulut</w> <w n="82.3">l</w>'<w n="82.4">univers</w> ! <w n="82.5">Et</w>, <w n="82.6">de</w> <w n="82.7">sa</w> <w n="82.8">main</w> <w n="82.9">hardie</w></l>
					<l n="83" num="6.13"><w n="83.1">Souffletant</w> <w n="83.2">le</w> <w n="83.3">vieux</w> <w n="83.4">monde</w>, <w n="83.5">il</w> <w n="83.6">courut</w>,, <w n="83.7">comme</w> <w n="83.8">un</w> <w n="83.9">fou</w>,</l>
					<l n="84" num="6.14"><w n="84.1">Se</w> <w n="84.2">briser</w> <w n="84.3">impuissant</w> <w n="84.4">sous</w> <w n="84.5">les</w> <w n="84.6">murs</w> <w n="84.7">de</w> <w n="84.8">Moscou</w> !…</l>
				</lg>
				<lg n="7">
					<l n="85" num="7.1"><w n="85.1">Sire</w>, <w n="85.2">c</w>'<w n="85.3">est</w> <w n="85.4">là</w> <w n="85.5">le</w> <w n="85.6">sort</w> <w n="85.7">que</w> <w n="85.8">Dieu</w>, <w n="85.9">dans</w> <w n="85.10">sa</w> <w n="85.11">justice</w>,</l>
					<l n="86" num="7.2"><w n="86.1">Garde</w> <w n="86.2">aux</w> <w n="86.3">vainqueurs</w> <w n="86.4">qui</w> <w n="86.5">vont</w>, <w n="86.6">au</w> <w n="86.7">gré</w> <w n="86.8">de</w> <w n="86.9">leur</w> <w n="86.10">caprice</w>,</l>
					<l n="87" num="7.3"><w n="87.1">Jeter</w> <w n="87.2">aux</w> <w n="87.3">nations</w> <w n="87.4">des</w> <w n="87.5">défis</w> <w n="87.6">outrageants</w>…</l>
					<l n="88" num="7.4"><w n="88.1">Cirons</w>, <w n="88.2">que</w> <w n="88.3">le</w> <w n="88.4">flatteur</w> <w n="88.5">compare</w> <w n="88.6">à</w> <w n="88.7">des</w> <w n="88.8">géants</w>,</l>
					<l n="89" num="7.5"><w n="89.1">Rome</w> <w n="89.2">vous</w> <w n="89.3">réservait</w> <w n="89.4">sa</w> <w n="89.5">roche</w> <w n="89.6">Tarpéienne</w> !…</l>
					<l n="90" num="7.6"><w n="90.1">Nous</w> <w n="90.2">avons</w> <w n="90.3">le</w> <w n="90.4">bâton</w>… <w n="90.5">et</w> <w n="90.6">nous</w> <w n="90.7">avons</w> <w n="90.8">Cayenne</w> !…</l>
				</lg>
			</div></body></text></TEI>