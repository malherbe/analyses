<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR17">
				<head type="main">LE 19 JANVIER</head>
				<div n="1" type="section">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">EST</w> <w n="1.3">le</w> <w n="1.4">dernier</w> <w n="1.5">effort</w> ! <w n="1.6">Allons</w>, <w n="1.7">Français</w> <w n="1.8">mes</w> <w n="1.9">frères</w> !</l>
						<l n="2" num="1.2"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="2.1">Déchaînez</w> <w n="2.2">vos</w> <w n="2.3">colères</w> !</l>
						<l n="3" num="1.3"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="3.1">Armez</w> <w n="3.2">vos</w> <w n="3.3">bras</w> <w n="3.4">vengeurs</w> !</l>
						<l n="4" num="1.4"><w n="4.1">Si</w> <w n="4.2">nous</w> <w n="4.3">devons</w> <w n="4.4">mourir</w>, <w n="4.5">mourons</w> <w n="4.6">dans</w> <w n="4.7">cette</w> <w n="4.8">lutte</w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="5.1">Et</w> <w n="5.2">qu</w>’<w n="5.3">au</w> <w n="5.4">moins</w> <w n="5.5">notre</w> <w n="5.6">chute</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="6.1">Soit</w> <w n="6.2">fatale</w> <w n="6.3">aux</w> <w n="6.4">vainqueurs</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Pour</w> <w n="7.2">affermir</w> <w n="7.3">vos</w> <w n="7.4">cœurs</w>, <w n="7.5">grandir</w> <w n="7.6">votre</w> <w n="7.7">courage</w>,</l>
						<l n="8" num="2.2"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="8.1">Exalter</w> <w n="8.2">votre</w> <w n="8.3">rage</w>,</l>
						<l n="9" num="2.3"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="9.1">Il</w> <w n="9.2">n</w>’<w n="9.3">est</w> <w n="9.4">besoin</w> <w n="9.5">de</w> <w n="9.6">rien</w> :</l>
						<l n="10" num="2.4"><w n="10.1">Un</w> <w n="10.2">seul</w> <w n="10.3">mot</w> <w n="10.4">vous</w> <w n="10.5">transporte</w>, <w n="10.6">un</w> <w n="10.7">seul</w> <w n="10.8">nom</w> <w n="10.9">vous</w> <w n="10.10">entraîne</w>,</l>
						<l n="11" num="2.5"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="11.1">Et</w> <w n="11.2">ce</w> <w n="11.3">mot</w> <w n="11.4">c</w>’<w n="11.5">est</w> : <w n="11.6">la</w> <w n="11.7">haine</w> !</l>
						<l n="12" num="2.6"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="12.1">Et</w> <w n="12.2">ce</w> <w n="12.3">nom</w> <w n="12.4">c</w>’<w n="12.5">est</w> : <w n="12.6">Prussien</w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Loin</w> <w n="13.2">de</w> <w n="13.3">vous</w>, <w n="13.4">près</w> <w n="13.5">de</w> <w n="13.6">vous</w>, <w n="13.7">à</w> <w n="13.8">Paris</w>, <w n="13.9">dans</w> <w n="13.10">la</w> <w n="13.11">France</w>,</l>
						<l n="14" num="3.2"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="14.1">Partout</w> <w n="14.2">c</w>'<w n="14.3">est</w> <w n="14.4">la</w> <w n="14.5">souffrance</w>,</l>
						<l n="15" num="3.3"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="15.1">Partout</w> <w n="15.2">le</w> <w n="15.3">désespoir</w> ;</l>
						<l n="16" num="3.4"><w n="16.1">Partout</w> <w n="16.2">il</w> <w n="16.3">a</w> <w n="16.4">semé</w> <w n="16.5">la</w> <w n="16.6">famine</w> <w n="16.7">et</w> <w n="16.8">la</w> <w n="16.9">guerre</w>,</l>
						<l n="17" num="3.5"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="17.1">La</w> <w n="17.2">honte</w> <w n="17.3">et</w> <w n="17.4">la</w> <w n="17.5">misère</w>,</l>
						<l n="18" num="3.6"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="18.1">Leur</w> <w n="18.2">long</w> <w n="18.3">tourbillon</w> <w n="18.4">noir</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Ah</w> ! <w n="19.2">ce</w> <w n="19.3">cercle</w> <w n="19.4">d</w>'<w n="19.5">airain</w> <w n="19.6">qui</w> <w n="19.7">nous</w> <w n="19.8">brise</w>, <w n="19.9">nous</w> <w n="19.10">serre</w>.</l>
						<l n="20" num="4.2"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="20.1">N</w>’<w n="20.2">est</w>-<w n="20.3">il</w> <w n="20.4">pas</w> <w n="20.5">de</w> <w n="20.6">colère</w></l>
						<l n="21" num="4.3"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="21.1">Qui</w> <w n="21.2">puisse</w> <w n="21.3">le</w> <w n="21.4">casser</w> ?</l>
						<l n="22" num="4.4"><w n="22.1">Ces</w> <w n="22.2">bataillons</w> <w n="22.3">nombreux</w>, <w n="22.4">cette</w> <w n="22.5">humaine</w> <w n="22.6">muraille</w>,</l>
						<l n="23" num="4.5"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="23.1">N</w>’<w n="23.2">est</w>-<w n="23.3">il</w> <w n="23.4">pas</w> <w n="23.5">de</w> <w n="23.6">mitraille</w></l>
						<l n="24" num="4.6"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="24.1">Qui</w> <w n="24.2">puisse</w> <w n="24.3">la</w> <w n="24.4">percer</w> ?</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Pour</w> <w n="25.2">nous</w> <w n="25.3">avoir</w> <w n="25.4">vaincus</w>, <w n="25.5">sont</w>-<w n="25.6">ils</w> <w n="25.7">donc</w> <w n="25.8">invincibles</w>,</l>
						<l n="26" num="5.2"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="26.1">Ces</w> <w n="26.2">ennemis</w> <w n="26.3">terribles</w> ?</l>
						<l n="27" num="5.3"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="27.1">N</w>’<w n="27.2">ont</w>-<w n="27.3">ils</w> <w n="27.4">pas</w>, <w n="27.5">comme</w> <w n="27.6">nous</w>,</l>
						<l n="28" num="5.4"><w n="28.1">Un</w> <w n="28.2">cœur</w> <w n="28.3">qu</w>’<w n="28.4">on</w> <w n="28.5">peut</w> <w n="28.6">trouer</w>, <w n="28.7">un</w> <w n="28.8">sang</w> <w n="28.9">qu</w>’<w n="28.10">on</w> <w n="28.11">peut</w> <w n="28.12">répandre</w> ?</l>
						<l n="29" num="5.5"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="29.1">Et</w>, <w n="29.2">pour</w> <w n="29.3">pouvoir</w> <w n="29.4">les</w> <w n="29.5">rendre</w>,</l>
						<l n="30" num="5.6"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="30.1">Sentent</w>-<w n="30.2">ils</w> <w n="30.3">moins</w> <w n="30.4">nos</w> <w n="30.5">coups</w> ?</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">En</w> <w n="31.2">avant</w> ! — <w n="31.3">A</w> <w n="31.4">Paris</w> <w n="31.5">la</w> <w n="31.6">France</w> <w n="31.7">est</w> <w n="31.8">attachée</w>.</l>
						<l n="32" num="6.2"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="32.1">Notre</w> <w n="32.2">gloire</w> <w n="32.3">est</w> <w n="32.4">tachée</w>,</l>
						<l n="33" num="6.3"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="33.1">Notre</w> <w n="33.2">honneur</w> <w n="33.3">est</w> <w n="33.4">perdu</w></l>
						<l n="34" num="6.4"><w n="34.1">Si</w> <w n="34.2">nous</w> <w n="34.3">ne</w> <w n="34.4">luttons</w> <w n="34.5">pas</w> <w n="34.6">jusqu</w>’<w n="34.7">à</w> <w n="34.8">la</w> <w n="34.9">dernière</w> <w n="34.10">heure</w>,</l>
						<l n="35" num="6.5"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="35.1">Il</w> <w n="35.2">faut</w> <w n="35.3">que</w> <w n="35.4">Paris</w> <w n="35.5">meure</w></l>
						<l n="36" num="6.6"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="36.1">Avant</w> <w n="36.2">d</w>‘<w n="36.3">être</w> <w n="36.4">rendu</w>.</l>
					</lg>
				</div>
				<div n="2" type="section">
					<head type="number">II</head>
					<lg n="1">
						<l n="37" num="1.1"><w n="37.1">Ce</w> <w n="37.2">jour</w>-<w n="37.3">là</w>, <w n="37.4">sur</w> <w n="37.5">le</w> <w n="37.6">haut</w> <w n="37.7">aqueduc</w> <w n="37.8">de</w> <w n="37.9">Marly</w>,</l>
						<l n="38" num="1.2"><w n="38.1">Aux</w> <w n="38.2">rayons</w> <w n="38.3">du</w> <w n="38.4">soleil</w> <w n="38.5">brille</w> <w n="38.6">un</w> <w n="38.7">casque</w> <w n="38.8">poli</w></l>
						<l n="39" num="1.3"><w n="39.1">Ombrageant</w> <w n="39.2">une</w> <w n="39.3">tête</w> <w n="39.4">avec</w> <w n="39.5">moustache</w> <w n="39.6">blanche</w>.</l>
						<l n="40" num="1.4"><w n="40.1">Au</w> <w n="40.2">bout</w> <w n="40.3">d</w>’<w n="40.4">une</w> <w n="40.5">lorgnette</w> <w n="40.6">énorme</w>, <w n="40.7">elle</w> <w n="40.8">se</w> <w n="40.9">penche</w></l>
						<l n="41" num="1.5"><w n="41.1">Et</w> <w n="41.2">sonde</w> <w n="41.3">l</w>’<w n="41.4">horizon</w> <w n="41.5">du</w> <w n="41.6">côté</w> <w n="41.7">de</w> <w n="41.8">Paris</w>.</l>
						<l n="42" num="1.6"><w n="42.1">Derrière</w>, <w n="42.2">un</w> <w n="42.3">autre</w> <w n="42.4">casque</w>, <w n="42.5">aux</w> <w n="42.6">gros</w> <w n="42.7">yeux</w> <w n="42.8">arrondis</w>,</l>
						<l n="43" num="1.7"><w n="43.1">A</w> <w n="43.2">la</w> <w n="43.3">moustache</w> <w n="43.4">épaisse</w>, <w n="43.5">attend</w>, <w n="43.6">imperturbable</w>.</l>
						<l n="44" num="1.8">« <w n="44.1">Bismarck</w>, <w n="44.2">dit</w> <w n="44.3">le</w> <w n="44.4">premier</w>, <w n="44.5">c</w>’<w n="44.6">est</w> <w n="44.7">vraiment</w> <w n="44.8">incroyable</w> !</l>
						<l n="45" num="1.9"><w n="45.1">Je</w> <w n="45.2">crois</w> <w n="45.3">voir</w> <w n="45.4">les</w> <w n="45.5">Français</w> <w n="45.6">avancer</w> ; <w n="45.7">voyez</w> <w n="45.8">donc</w>. »</l>
						<l n="46" num="1.10"><w n="46.1">Après</w> <w n="46.2">un</w> <w n="46.3">examen</w> <w n="46.4">minutieux</w> <w n="46.5">et</w> <w n="46.6">long</w> :</l>
						<l n="47" num="1.11">« <w n="47.1">Sire</w>, <w n="47.2">vous</w> <w n="47.3">vous</w> <w n="47.4">trompez</w> ; <w n="47.5">la</w> <w n="47.6">lorgnette</w> <w n="47.7">est</w> <w n="47.8">mauvaise</w>,</l>
						<l n="48" num="1.12"><w n="48.1">Dit</w> <w n="48.2">l</w>‘<w n="48.3">autre</w> ; <w n="48.4">moi</w>, <w n="48.5">je</w> <w n="48.6">vois</w> <w n="48.7">fuir</w> <w n="48.8">la</w> <w n="48.9">troupe</w> <w n="48.10">française</w>. »</l>
					</lg>
				</div>
				<div n="3" type="section">
					<head type="number">III</head>
					<lg n="1">
						<l n="49" num="1.1"><w n="49.1">En</w> <w n="49.2">avant</w> ! — <w n="49.3">C</w>’<w n="49.4">est</w> <w n="49.5">le</w> <w n="49.6">jour</w> <w n="49.7">des</w> <w n="49.8">prochaines</w> <w n="49.9">vengeances</w>,</l>
						<l n="50" num="1.2"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="50.1">La</w> <w n="50.2">fin</w> <w n="50.3">de</w> <w n="50.4">nos</w> <w n="50.5">souffrances</w> !</l>
						<l n="51" num="1.3"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="51.1">Le</w> <w n="51.2">passé</w> <w n="51.3">s</w>’<w n="51.4">est</w> <w n="51.5">enfui</w> !</l>
						<l n="52" num="1.4"><w n="52.1">Colères</w> <w n="52.2">dans</w> <w n="52.3">les</w> <w n="52.4">cœurs</w> <w n="52.5">lentement</w> <w n="52.6">amassées</w></l>
						<l n="53" num="1.5"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="53.1">Et</w> <w n="53.2">mortelles</w> <w n="53.3">pensées</w>,</l>
						<l n="54" num="1.6"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="54.1">Débordez</w> <w n="54.2">aujourd</w>’<w n="54.3">hui</w> !</l>
					</lg>
					<lg n="2">
						<l n="55" num="2.1"><w n="55.1">En</w> <w n="55.2">avant</w> ! — <w n="55.3">Notre</w> <w n="55.4">cause</w> <w n="55.5">est</w> <w n="55.6">juste</w>, <w n="55.7">grande</w>, <w n="55.8">sainte</w>,</l>
						<l n="56" num="2.2"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="56.1">Car</w> <w n="56.2">notre</w> <w n="56.3">ville</w> <w n="56.4">est</w> <w n="56.5">teinte</w></l>
						<l n="57" num="2.3"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="57.1">Du</w> <w n="57.2">sang</w> <w n="57.3">de</w> <w n="57.4">nos</w> <w n="57.5">soldats</w>.</l>
						<l n="58" num="2.4"><w n="58.1">Leurs</w> <w n="58.2">obus</w> <w n="58.3">ont</w> <w n="58.4">frappé</w> <w n="58.5">nos</w> <w n="58.6">enfants</w> <w n="58.7">et</w> <w n="58.8">nos</w> <w n="58.9">femmes</w>,</l>
						<l n="59" num="2.5"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="59.1">Et</w> <w n="59.2">nous</w> <w n="59.3">serions</w> <w n="59.4">infâmes</w></l>
						<l n="60" num="2.6"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="60.1">En</w> <w n="60.2">ne</w> <w n="60.3">les</w> <w n="60.4">vengeant</w> <w n="60.5">pas</w> !</l>
					</lg>
					<lg n="3">
						<l n="61" num="3.1"><w n="61.1">En</w> <w n="61.2">avant</w> ! — <w n="61.3">A</w> <w n="61.4">ce</w> <w n="61.5">cri</w> <w n="61.6">qui</w> <w n="61.7">parcourut</w> <w n="61.8">la</w> <w n="61.9">terre</w>,</l>
						<l n="62" num="3.2"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="62.1">Jadis</w> <w n="62.2">l</w>’<w n="62.3">Europe</w> <w n="62.4">entière</w></l>
						<l n="63" num="3.3"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="63.1">Frémit</w> <w n="63.2">et</w> <w n="63.3">s</w>’<w n="63.4">étonna</w> :</l>
						<l n="64" num="3.4"><w n="64.1">Amis</w>, <w n="64.2">souvenons</w>-<w n="64.3">nous</w> <w n="64.4">de</w> <w n="64.5">l</w>’<w n="64.6">an</w> <w n="64.7">quatre</w>-<w n="64.8">vingt</w>-<w n="64.9">treize</w>,</l>
						<l n="65" num="3.5"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="65.1">De</w> <w n="65.2">la</w> <w n="65.3">gloire</w> <w n="65.4">française</w>,</l>
						<l n="66" num="3.6"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="66.1">D</w>’<w n="66.2">Austerlitz</w>, <w n="66.3">d</w>’<w n="66.4">Iéna</w>.</l>
					</lg>
					<lg n="4">
						<l n="67" num="4.1"><w n="67.1">En</w> <w n="67.2">avant</w> ! — <w n="67.3">Voyez</w>-<w n="67.4">vous</w> <w n="67.5">cette</w> <w n="67.6">nombreuse</w> <w n="67.7">armée</w></l>
						<l n="68" num="4.2"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="68.1">A</w> <w n="68.2">travers</w> <w n="68.3">la</w> <w n="68.4">fumée</w></l>
						<l n="69" num="4.3"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="69.1">Que</w> <w n="69.2">balaye</w> <w n="69.3">le</w> <w n="69.4">vent</w> ?</l>
						<l n="70" num="4.4"><w n="70.1">C</w>’<w n="70.2">est</w> <w n="70.3">la</w> <w n="70.4">Prusse</w>, <w n="70.5">la</w> <w n="70.6">Prusse</w>, <w n="70.7">exécrable</w> <w n="70.8">ennemie</w> :</l>
						<l n="71" num="4.5"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="71.1">Enfants</w> <w n="71.2">de</w> <w n="71.3">la</w> <w n="71.4">patrie</w>,</l>
						<l n="72" num="4.6"><space unit="char" quantity="12"></space><space unit="char" quantity="12"></space><w n="72.1">En</w> <w n="72.2">avant</w> ! <w n="72.3">en</w> <w n="72.4">avant</w> !</l>
					</lg>
				</div>
				<div n="4" type="section">
					<head type="number">IV</head>
					<lg n="1">
						<l n="73" num="1.1"><w n="73.1">Sur</w> <w n="73.2">le</w> <w n="73.3">haut</w> <w n="73.4">aqueduc</w> <w n="73.5">sont</w> <w n="73.6">encor</w> <w n="73.7">les</w> <w n="73.8">deux</w> <w n="73.9">têtes</w> ;</l>
						<l n="74" num="1.2"><w n="74.1">Mais</w>, <w n="74.2">à</w> <w n="74.3">voir</w> <w n="74.4">leurs</w> <w n="74.5">nez</w> <w n="74.6">longs</w>, <w n="74.7">leurs</w> <w n="74.8">mines</w> <w n="74.9">inquiètes</w>,</l>
						<l n="75" num="1.3"><w n="75.1">Il</w> <w n="75.2">semblerait</w> <w n="75.3">que</w> <w n="75.4">tout</w> <w n="75.5">ne</w> <w n="75.6">va</w> <w n="75.7">pas</w> <w n="75.8">à</w> <w n="75.9">leur</w> <w n="75.10">gré</w>.</l>
						<l n="76" num="1.4"><w n="76.1">Le</w> <w n="76.2">plus</w> <w n="76.3">âgé</w> <w n="76.4">des</w> <w n="76.5">deux</w>, <w n="76.6">l</w>’<w n="76.7">homme</w> <w n="76.8">au</w> <w n="76.9">casque</w> <w n="76.10">doré</w>,</l>
						<l n="77" num="1.5"><w n="77.1">Dit</w> : « <w n="77.2">Bismarck</w>, <w n="77.3">la</w> <w n="77.4">lorgnette</w> <w n="77.5">est</w> <w n="77.6">mauvaise</w> <w n="77.7">sans</w> <w n="77.8">doute</w>,</l>
						<l n="78" num="1.6"><w n="78.1">Mais</w> <w n="78.2">là</w>-<w n="78.3">bas</w>, <w n="78.4">voyez</w>-<w n="78.5">vous</w>, <w n="78.6">sur</w> <w n="78.7">cette</w> <w n="78.8">grande</w> <w n="78.9">route</w>,</l>
						<l n="79" num="1.7"><w n="79.1">Lit</w>-<w n="79.2">bas</w>, <w n="79.3">à</w> <w n="79.4">Montretout</w>, <w n="79.5">ces</w> <w n="79.6">bataillons</w> <w n="79.7">épais</w></l>
						<l n="80" num="1.8"><w n="80.1">Qui</w> <w n="80.2">s</w>’<w n="80.3">avancent</w> <w n="80.4">toujours</w> ? <w n="80.5">Seraient</w>-<w n="80.6">ce</w> <w n="80.7">des</w> <w n="80.8">Français</w> ?</l>
						<l n="81" num="1.9">— <w n="81.1">Non</w>, <w n="81.2">sire</w>. — <w n="81.3">Cependant</w>… <w n="81.4">regardez</w>… — <w n="81.5">Je</w> <w n="81.6">regarde</w>,</l>
						<l n="82" num="1.10"><w n="82.1">Sire</w>, <w n="82.2">et</w> <w n="82.3">je</w> <w n="82.4">vois</w> <w n="82.5">partout</w> <w n="82.6">des</w> <w n="82.7">soldats</w> <w n="82.8">de</w> <w n="82.9">la</w> <w n="82.10">garde</w>.</l>
						<l n="83" num="1.11"><w n="83.1">N</w>’<w n="83.2">ayez</w> <w n="83.3">crainte</w>. — <w n="83.4">Pourtant</w>… — <w n="83.5">Ce</w> <w n="83.6">sont</w> <w n="83.7">des</w> <w n="83.8">Allemands</w>.</l>
						<l n="84" num="1.12">— <w n="84.1">Leurs</w> <w n="84.2">pantalons</w> <w n="84.3">sont</w>… — <w n="84.4">Noirs</w>, <w n="84.5">et</w> <w n="84.6">blancs</w> <w n="84.7">leurs</w> <w n="84.8">parements</w>.</l>
						<l n="85" num="1.13"><w n="85.1">D</w>’<w n="85.2">ailleurs</w>, <w n="85.3">si</w> <w n="85.4">les</w> <w n="85.5">Français</w> <w n="85.6">s</w>’<w n="85.7">avançaient</w> <w n="85.8">de</w> <w n="85.9">la</w> <w n="85.10">sorte</w>,</l>
						<l n="86" num="1.14"><w n="86.1">C</w>’<w n="86.2">est</w> <w n="86.3">qu</w>’<w n="86.4">ils</w> <w n="86.5">l</w>’<w n="86.6">emporteraient</w> ; <w n="86.7">or</w>, <w n="86.8">l</w>’<w n="86.9">armée</w> <w n="86.10">est</w> <w n="86.11">si</w> <w n="86.12">forte</w>,</l>
						<l n="87" num="1.15"><w n="87.1">Sire</w>… <w n="87.2">vous</w> <w n="87.3">savez</w> <w n="87.4">bien</w> <w n="87.5">que</w> <w n="87.6">cela</w> <w n="87.7">ne</w> <w n="87.8">se</w> <w n="87.9">peut</w> ! »</l>
						<l n="88" num="1.16"><w n="88.1">Tourné</w> <w n="88.2">vers</w> <w n="88.3">l</w>’<w n="88.4">horizon</w>, <w n="88.5">rouge</w> <w n="88.6">d</w>’<w n="88.7">éclairs</w>, <w n="88.8">en</w> <w n="88.9">feu</w>,</l>
						<l n="89" num="1.17"><w n="89.1">De</w> <w n="89.2">nouveau</w> <w n="89.3">vers</w> <w n="89.4">Paris</w> <w n="89.5">le</w> <w n="89.6">télescope</w> <w n="89.7">plonge</w>.</l>
						<l n="90" num="1.18">« <w n="90.1">O</w> <w n="90.2">Bismark</w>, <w n="90.3">dit</w> <w n="90.4">le</w> <w n="90.5">roi</w>, <w n="90.6">serait</w>-<w n="90.7">ce</w> <w n="90.8">donc</w> <w n="90.9">un</w> <w n="90.10">songe</w> ?</l>
						<l n="91" num="1.19"><w n="91.1">Ce</w> <w n="91.2">sont</w> <w n="91.3">bien</w> <w n="91.4">les</w> <w n="91.5">Français</w>… — <w n="91.6">Sire</w>… — <w n="91.7">Ce</w> <w n="91.8">sont</w> <w n="91.9">bien</w> <w n="91.10">eux</w>…</l>
						<l n="92" num="1.20">— <w n="92.1">Majesté</w> !… — <w n="92.2">Je</w> <w n="92.3">les</w> <w n="92.4">vois</w>, <w n="92.5">je</w> <w n="92.6">les</w> <w n="92.7">vois</w> <w n="92.8">de</w> <w n="92.9">mes</w> <w n="92.10">yeux</w>…</l>
						<l n="93" num="1.21"><w n="93.1">Quel</w> <w n="93.2">est</w> <w n="93.3">ce</w> <w n="93.4">sifflement</w> ? — <w n="93.5">Sire</w>… <w n="93.6">ce</w> <w n="93.7">sont</w> <w n="93.8">des</w> <w n="93.9">balles</w>…</l>
						<l n="94" num="1.22">—<w n="94.1">Que</w> <w n="94.2">faire</w>, <w n="94.3">ô</w> <w n="94.4">conseiller</w> ? — <w n="94.5">Sire</w>, <w n="94.6">faisons</w> <w n="94.7">nos</w> <w n="94.8">malles</w>. »</l>
					</lg>
				</div>
			</div></body></text></TEI>