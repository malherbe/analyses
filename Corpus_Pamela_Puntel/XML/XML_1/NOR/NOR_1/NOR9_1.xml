<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR9">
				<head type="main">CAUSERIE</head>
				<opener>
					<dateline>
						<date when="1870">Novembre 1870.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">ET</w> <w n="1.2">le</w> <w n="1.3">vieux</w> <w n="1.4">colonel</w>, <w n="1.5">tirant</w> <w n="1.6">sa</w> <w n="1.7">tabatière</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Me</w> <w n="2.2">dit</w> : « <w n="2.3">Que</w> <w n="2.4">voulez</w>-<w n="2.5">vous</w>, <w n="2.6">mon</w> <w n="2.7">ami</w>, <w n="2.8">c</w>’<w n="2.9">est</w> <w n="2.10">la</w> <w n="2.11">guerre</w> !</l>
					<l n="3" num="1.3">— <w n="3.1">La</w> <w n="3.2">guerre</w>, <w n="3.3">cher</w> <w n="3.4">monsieur</w> ; <w n="3.5">mais</w> <w n="3.6">je</w> <w n="3.7">ne</w> <w n="3.8">l</w>’<w n="3.9">admets</w> <w n="3.10">pas</w>.</l>
					<l n="4" num="1.4">— <w n="4.1">Comment</w> ! — <w n="4.2">A</w> <w n="4.3">mon</w> <w n="4.4">avis</w>, <w n="4.5">la</w> <w n="4.6">déesse</w> <w n="4.7">Pallas</w></l>
					<l n="5" num="1.5"><w n="5.1">Devrait</w>, <w n="5.2">au</w> <w n="5.3">lieu</w> <w n="5.4">de</w> <w n="5.5">lance</w>, <w n="5.6">et</w> <w n="5.7">d</w>’<w n="5.8">égide</w>, <w n="5.9">et</w> <w n="5.10">de</w> <w n="5.11">casque</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Porter</w> <w n="6.2">plumes</w>, <w n="6.3">rubans</w>, <w n="6.4">grelots</w>, <w n="6.5">maillot</w> <w n="6.6">et</w> <w n="6.7">masque</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">tenir</w> <w n="7.3">à</w> <w n="7.4">la</w> <w n="7.5">main</w>, <w n="7.6">comme</w> <w n="7.7">feu</w> <w n="7.8">Triboulet</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Une</w> <w n="8.2">marotte</w>. — <w n="8.3">Eh</w> <w n="8.4">quoi</w> ! <w n="8.5">riez</w>-<w n="8.6">vous</w>, <w n="8.7">s</w>’<w n="8.8">il</w> <w n="8.9">vous</w> <w n="8.10">plaît</w> ? —</l>
					<l n="9" num="1.9"><w n="9.1">Non</w>, <w n="9.2">non</w>, <w n="9.3">je</w> <w n="9.4">ne</w> <w n="9.5">ris</w> <w n="9.6">pas</w>. — <w n="9.7">Quoi</w> ! <w n="9.8">Pallas</w> <w n="9.9">une</w> <w n="9.10">folle</w> ? —</l>
					<l n="10" num="1.10"><w n="10.1">Oui</w>, <w n="10.2">mon</w> <w n="10.3">cher</w> <w n="10.4">colonel</w>. — <w n="10.5">On</w> <w n="10.6">croirait</w>, <w n="10.7">ma</w> <w n="10.8">parole</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Que</w> <w n="11.2">pour</w> <w n="11.3">vous</w> <w n="11.4">le</w> <w n="11.5">dieu</w> <w n="11.6">Mars</w> <w n="11.7">n</w>’<w n="11.8">est</w> <w n="11.9">qu</w>’<w n="11.10">un</w> <w n="11.11">bouffon</w> <w n="11.12">de</w> <w n="11.13">cour</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Un</w> <w n="12.2">pitre</w>, <w n="12.3">s</w>’<w n="12.4">essoufflant</w> <w n="12.5">au</w> <w n="12.6">coin</w> <w n="12.7">d</w>’<w n="12.8">un</w> <w n="12.9">carrefour</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Aux</w> <w n="13.2">badauds</w> <w n="13.3">ébahis</w> <w n="13.4">débitant</w> <w n="13.5">des</w> <w n="13.6">sornettes</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">dans</w> <w n="14.3">un</w> <w n="14.4">casque</w> <w n="14.5">d</w>’<w n="14.6">or</w> <w n="14.7">recueillant</w> <w n="14.8">ses</w> <w n="14.9">recettes</w>.</l>
					<l n="15" num="1.15">— <w n="15.1">Le</w> <w n="15.2">portrait</w> <w n="15.3">ci</w>-<w n="15.4">dessus</w> <w n="15.5">est</w> <w n="15.6">exact</w> <w n="15.7">en</w> <w n="15.8">tout</w> <w n="15.9">point</w>.</l>
					<l n="16" num="1.16">— <w n="16.1">Voyons</w>, <w n="16.2">vous</w> <w n="16.3">vous</w> <w n="16.4">moquez</w>… —<w n="16.5">Je</w> <w n="16.6">ne</w> <w n="16.7">me</w> <w n="16.8">moque</w> <w n="16.9">point</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Comme</w> <w n="17.2">disait</w> <w n="17.3">Alceste</w>, <w n="17.4">et</w> <w n="17.5">je</w> <w n="17.6">ne</w> <w n="17.7">puis</w> <w n="17.8">comprendre</w></l>
					<l n="18" num="1.18"><w n="18.1">Que</w> <w n="18.2">votre</w> <w n="18.3">opinion</w> <w n="18.4">se</w> <w n="18.5">puisse</w> <w n="18.6">encor</w> <w n="18.7">défendre</w>.</l>
					<l n="19" num="1.19"><w n="19.1">Vous</w> <w n="19.2">faites</w> <w n="19.3">de</w> <w n="19.4">la</w> <w n="19.5">guerre</w> <w n="19.6">une</w> <w n="19.7">nécessité</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Un</w> <w n="20.2">fléau</w> <w n="20.3">qui</w> <w n="20.4">jamais</w> <w n="20.5">ne</w> <w n="20.6">peut</w> <w n="20.7">être</w> <w n="20.8">évité</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Comme</w> <w n="21.2">le</w> <w n="21.3">choléra</w>, <w n="21.4">la</w> <w n="21.5">grêle</w> <w n="21.6">ou</w> <w n="21.7">le</w> <w n="21.8">tonnerre</w> ;</l>
					<l n="22" num="1.22"><w n="22.1">Puis</w> <w n="22.2">vous</w> <w n="22.3">vous</w> <w n="22.4">inclinez</w> <w n="22.5">en</w> <w n="22.6">disant</w> : <w n="22.7">C</w>’<w n="22.8">est</w> <w n="22.9">la</w> <w n="22.10">guerre</w> !</l>
					<l n="23" num="1.23"><w n="23.1">Ce</w> <w n="23.2">mot</w>-<w n="23.3">là</w>, <w n="23.4">parait</w>-<w n="23.5">il</w>, <w n="23.6">devra</w> <w n="23.7">tout</w> <w n="23.8">excuser</w> ;</l>
					<l n="24" num="1.24"><w n="24.1">On</w> <w n="24.2">ne</w> <w n="24.3">doit</w> <w n="24.4">s</w>’<w n="24.5">insurger</w> <w n="24.6">ni</w> <w n="24.7">se</w> <w n="24.8">scandaliser</w></l>
					<l n="25" num="1.25"><w n="25.1">En</w> <w n="25.2">voyant</w> <w n="25.3">le</w> <w n="25.4">bon</w> <w n="25.5">droit</w> <w n="25.6">se</w> <w n="25.7">donner</w> <w n="25.8">une</w> <w n="25.9">entorse</w>,</l>
					<l n="26" num="1.26"><w n="26.1">La</w> <w n="26.2">justice</w> <w n="26.3">céder</w> <w n="26.4">au</w> <w n="26.5">règne</w> <w n="26.6">de</w> <w n="26.7">la</w> <w n="26.8">force</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Et</w> <w n="27.2">la</w> <w n="27.3">matière</w> <w n="27.4">enfin</w> <w n="27.5">l</w>’<w n="27.6">emporter</w> <w n="27.7">sur</w> <w n="27.8">l</w>’<w n="27.9">esprit</w>.</l>
					<l n="28" num="1.28">— <w n="28.1">Tout</w> <w n="28.2">cela</w>, <w n="28.3">mon</w> <w n="28.4">ami</w>, <w n="28.5">sont</w> <w n="28.6">choses</w> <w n="28.7">qu</w>’<w n="28.8">on</w> <w n="28.9">écrit</w> ;</l>
					<l n="29" num="1.29"><w n="29.1">Ce</w> <w n="29.2">sont</w> <w n="29.3">raisonnements</w> <w n="29.4">qui</w> <w n="29.5">font</w> <w n="29.6">bien</w> <w n="29.7">dans</w> <w n="29.8">un</w> <w n="29.9">livre</w> ;</l>
					<l n="30" num="1.30"><w n="30.1">Mais</w> <w n="30.2">l</w>’<w n="30.3">homme</w> <w n="30.4">est</w> <w n="30.5">toujours</w> <w n="30.6">l</w>’<w n="30.7">homme</w>, <w n="30.8">et</w> <w n="30.9">la</w> <w n="30.10">gloire</w> <w n="30.11">l</w>’<w n="30.12">enivre</w>,</l>
					<l n="31" num="1.31"><w n="31.1">Et</w> <w n="31.2">de</w> <w n="31.3">son</w> <w n="31.4">sot</w> <w n="31.5">orgueil</w> <w n="31.6">telle</w> <w n="31.7">est</w> <w n="31.8">l</w>’<w n="31.9">intensité</w></l>
					<l n="32" num="1.32"><w n="32.1">Qu</w>’<w n="32.2">il</w> <w n="32.3">le</w> <w n="32.4">pousse</w> <w n="32.5">souvent</w> <w n="32.6">jusqu</w>’<w n="32.7">à</w> <w n="32.8">la</w> <w n="32.9">cruauté</w>.</l>
					<l n="33" num="1.33"><w n="33.1">La</w> <w n="33.2">guerre</w>, <w n="33.3">voyez</w>-<w n="33.4">vous</w>, <w n="33.5">est</w> <w n="33.6">un</w> <w n="33.7">mal</w> <w n="33.8">incurable</w>.</l>
					<l n="34" num="1.34">— <w n="34.1">Et</w> <w n="34.2">pourquoi</w> ? — <w n="34.3">Parce</w> <w n="34.4">que</w>… — <w n="34.5">Vous</w> <w n="34.6">connaissez</w> <w n="34.7">la</w> <w n="34.8">fable</w></l>
					<l n="35" num="1.35"><w n="35.1">De</w> <w n="35.2">Bertrand</w> <w n="35.3">et</w> <w n="35.4">Raton</w> ? — <w n="35.5">Oui</w>, <w n="35.6">les</w> <w n="35.7">marrons</w> <w n="35.8">du</w> <w n="35.9">feu</w>.</l>
					<l n="36" num="1.36">— <w n="36.1">Eh</w> <w n="36.2">bien</w>, <w n="36.3">mon</w> <w n="36.4">colonel</w>, <w n="36.5">réfléchissons</w> <w n="36.6">un</w> <w n="36.7">peu</w>.</l>
					<l n="37" num="1.37"><w n="37.1">N</w>’<w n="37.2">est</w>-<w n="37.3">il</w> <w n="37.4">pas</w> <w n="37.5">évident</w> <w n="37.6">que</w> <w n="37.7">tous</w>, <w n="37.8">tant</w> <w n="37.9">que</w> <w n="37.10">nous</w> <w n="37.11">sommes</w>,</l>
					<l n="38" num="1.38"><w n="38.1">Nous</w> <w n="38.2">sommes</w> <w n="38.3">les</w> <w n="38.4">Ratons</w>, <w n="38.5">et</w> <w n="38.6">que</w> <w n="38.7">ces</w> <w n="38.8">quelques</w> <w n="38.9">hommes</w></l>
					<l n="39" num="1.39"><w n="39.1">Que</w> <w n="39.2">l</w>’<w n="39.3">on</w> <w n="39.4">nomme</w> <w n="39.5">empereurs</w>, <w n="39.6">ministres</w>, <w n="39.7">généraux</w>,</l>
					<l n="40" num="1.40"><w n="40.1">Sont</w>… — <w n="40.2">Mon</w> <w n="40.3">ami</w>, <w n="40.4">la</w> <w n="40.5">guerre</w> <w n="40.6">enfante</w> <w n="40.7">des</w> <w n="40.8">héros</w>,</l>
					<l n="41" num="1.41"><w n="41.1">Grandit</w> <w n="41.2">les</w> <w n="41.3">sentiments</w>, <w n="41.4">dévoile</w> <w n="41.5">les</w> <w n="41.6">courages</w>,</l>
					<l n="42" num="1.42"><w n="42.1">Fait</w> <w n="42.2">des</w> <w n="42.3">hommes</w> <w n="42.4">enfin</w> ! — <w n="42.5">Mais</w> <w n="42.6">en</w> <w n="42.7">perd</w> <w n="42.8">davantage</w>.</l>
					<l n="43" num="1.43">— <w n="43.1">Mourir</w> <w n="43.2">pour</w> <w n="43.3">son</w> <w n="43.4">pays</w> <w n="43.5">est</w> <w n="43.6">un</w> <w n="43.7">bienheureux</w> <w n="43.8">sort</w>.</l>
					<l n="44" num="1.44">— <w n="44.1">Hum</w> ! <w n="44.2">hum</w> ! <w n="44.3">Celui</w> <w n="44.4">qui</w> <w n="44.5">meurt</w> <w n="44.6">jouit</w>-<w n="44.7">il</w> <w n="44.8">de</w> <w n="44.9">sa</w> <w n="44.10">mort</w> ?</l>
					<l n="45" num="1.45">— <w n="45.1">Comment</w>, <w n="45.2">vous</w>, <w n="45.3">un</w> <w n="45.4">Français</w> ! — <w n="45.5">Mon</w> <w n="45.6">Dieu</w>, <w n="45.7">je</w> <w n="45.8">dois</w> <w n="45.9">le</w> <w n="45.10">dire</w>,</l>
					<l n="46" num="1.46"><w n="46.1">Je</w> <w n="46.2">ne</w> <w n="46.3">crois</w> <w n="46.4">pas</w> <w n="46.5">qu</w>’<w n="46.6">il</w> <w n="46.7">soit</w> <w n="46.8">un</w> <w n="46.9">homme</w> <w n="46.10">qui</w> <w n="46.11">désire</w></l>
					<l n="47" num="1.47"><w n="47.1">Mourir</w> <w n="47.2">pour</w> <w n="47.3">son</w> <w n="47.4">pays</w>, <w n="47.5">alors</w> <w n="47.6">qu</w>’<w n="47.7">à</w> <w n="47.8">ses</w> <w n="47.9">côtés</w></l>
					<l n="48" num="1.48"><w n="48.1">Éclatent</w> <w n="48.2">les</w> <w n="48.3">obus</w>, <w n="48.4">et</w> <w n="48.5">qu</w>’<w n="48.6">il</w> <w n="48.7">voit</w> <w n="48.8">culbutés</w></l>
					<l n="49" num="1.49"><w n="49.1">Criant</w>, <w n="49.2">saignant</w>, <w n="49.3">geignant</w>, <w n="49.4">sept</w> <w n="49.5">ou</w> <w n="49.6">huit</w> <w n="49.7">camarades</w>.</l>
					<l n="50" num="1.50"><w n="50.1">Alors</w> <w n="50.2">les</w> <w n="50.3">chants</w> <w n="50.4">guerriers</w> <w n="50.5">lui</w> <w n="50.6">paraissent</w> <w n="50.7">très</w>-<w n="50.8">fades</w> ;</l>
					<l n="51" num="1.51"><w n="51.1">Son</w> <w n="51.2">devoir</w> <w n="51.3">l</w>’<w n="51.4">attachant</w>, <w n="51.5">quoi</w> <w n="51.6">qu</w>’<w n="51.7">il</w> <w n="51.8">doive</w> <w n="51.9">affronter</w>,</l>
					<l n="52" num="1.52"><w n="52.1">Il</w> <w n="52.2">reste</w> ; <w n="52.3">mais</w> <w n="52.4">a</w>-<w n="52.5">t</w>-<w n="52.6">il</w> <w n="52.7">grand</w> <w n="52.8">plaisir</w> <w n="52.9">à</w> <w n="52.10">rester</w> ?</l>
					<l n="53" num="1.53"><w n="53.1">Pense</w>-<w n="53.2">t</w>-<w n="53.3">il</w> <w n="53.4">qu</w>’<w n="53.5">il</w> <w n="53.6">est</w> <w n="53.7">beau</w> <w n="53.8">de</w> <w n="53.9">mourir</w> <w n="53.10">pour</w> <w n="53.11">la</w> <w n="53.12">gloire</w> ?</l>
					<l n="54" num="1.54"><w n="54.1">Le</w> <w n="54.2">chant</w> <w n="54.3">des</w> <w n="54.4">Girondins</w> <w n="54.5">lui</w> <w n="54.6">vient</w>-<w n="54.7">il</w> <w n="54.8">en</w> <w n="54.9">mémoire</w> ?</l>
					<l n="55" num="1.55"><w n="55.1">Non</w>. <w n="55.2">Dans</w> <w n="55.3">ces</w> <w n="55.4">moments</w>-<w n="55.5">là</w> <w n="55.6">le</w> <w n="55.7">cœur</w> <w n="55.8">pleure</w>, <w n="55.9">et</w> <w n="55.10">l</w>’<w n="55.11">on</w> <w n="55.12">sent</w></l>
					<l n="56" num="1.56"><w n="56.1">Qu</w>'<w n="56.2">il</w> <w n="56.3">est</w> <w n="56.4">quelqu</w>’<w n="56.5">un</w> <w n="56.6">là</w>-<w n="56.7">bas</w> <w n="56.8">qui</w> <w n="56.9">pense</w> <w n="56.10">au</w> <w n="56.11">fils</w> <w n="56.12">absent</w>…</l>
					<l n="57" num="1.57"><w n="57.1">S</w>’<w n="57.2">il</w> <w n="57.3">est</w> <w n="57.4">beau</w> <w n="57.5">de</w> <w n="57.6">mourir</w>, <w n="57.7">il</w> <w n="57.8">est</w> <w n="57.9">bien</w> <w n="57.10">doux</w> <w n="57.11">de</w> <w n="57.12">vivre</w> ;</l>
					<l n="58" num="1.58"><w n="58.1">On</w> <w n="58.2">fera</w> <w n="58.3">son</w> <w n="58.4">devoir</w> ; <w n="58.5">on</w> <w n="58.6">suivra</w>, <w n="58.7">s</w>’<w n="58.8">il</w> <w n="58.9">faut</w> <w n="58.10">suivre</w>,</l>
					<l n="59" num="1.59"><w n="59.1">Jusque</w> <w n="59.2">sous</w> <w n="59.3">les</w> <w n="59.4">canons</w> <w n="59.5">ceux</w> <w n="59.6">qui</w> <w n="59.7">vont</w> <w n="59.8">de</w> <w n="59.9">l</w>’<w n="59.10">avant</w> ;</l>
					<l n="60" num="1.60"><w n="60.1">Mais</w> <w n="60.2">au</w> <w n="60.3">fond</w> <w n="60.4">de</w> <w n="60.5">son</w> <w n="60.6">être</w> <w n="60.7">on</w> <w n="60.8">sent</w> <w n="60.9">vibrer</w> <w n="60.10">souvent</w></l>
					<l n="61" num="1.61"><w n="61.1">Tout</w> <w n="61.2">un</w> <w n="61.3">monde</w> <w n="61.4">chéri</w> <w n="61.5">qui</w> <w n="61.6">pour</w> <w n="61.7">vous</w> <w n="61.8">veille</w> <w n="61.9">et</w> <w n="61.10">prie</w>,</l>
					<l n="62" num="1.62"><w n="62.1">Et</w> <w n="62.2">l</w>’<w n="62.3">on</w> <w n="62.4">désire</w> <w n="62.5">peu</w> « <w n="62.6">mourir</w> <w n="62.7">pour</w> <w n="62.8">la</w> <w n="62.9">patrie</w> ».</l>
					<l n="63" num="1.63"><w n="63.1">Peut</w>-<w n="63.2">être</w> <w n="63.3">appelle</w>-<w n="63.4">t</w>-<w n="63.5">on</w> <w n="63.6">cela</w> <w n="63.7">la</w> <w n="63.8">lâcheté</w> ;</l>
					<l n="64" num="1.64"><w n="64.1">Je</w> <w n="64.2">suis</w> <w n="64.3">un</w> <w n="64.4">lâche</w>, <w n="64.5">alors</w>. — <w n="64.6">Vous</w> <w n="64.7">êtes</w> <w n="64.8">entêté</w>,</l>
					<l n="65" num="1.65"><w n="65.1">Voilà</w> <w n="65.2">tout</w>. » <w n="65.3">Et</w> <w n="65.4">le</w> <w n="65.5">vieux</w>, <w n="65.6">tortillant</w> <w n="65.7">sa</w> <w n="65.8">moustache</w>,</l>
					<l n="66" num="1.66"><w n="66.1">Allongea</w> <w n="66.2">sur</w> <w n="66.3">sa</w> <w n="66.4">botte</w> <w n="66.5">un</w> <w n="66.6">bon</w> <w n="66.7">coup</w> <w n="66.8">de</w> <w n="66.9">cravache</w>,</l>
					<l n="67" num="1.67"><w n="67.1">Son</w> <w n="67.2">geste</w> <w n="67.3">habituel</w> <w n="67.4">alors</w> <w n="67.5">qu</w>’<w n="67.6">il</w> <w n="67.7">ne</w> <w n="67.8">veut</w> <w n="67.9">pas</w></l>
					<l n="68" num="1.68"><w n="68.1">Avouer</w> <w n="68.2">qu</w>’<w n="68.3">il</w> <w n="68.4">a</w> <w n="68.5">tort</w>, <w n="68.6">et</w> <w n="68.7">se</w> <w n="68.8">croisa</w> <w n="68.9">les</w> <w n="68.10">bras</w>.</l>
				</lg>
			</div></body></text></TEI>