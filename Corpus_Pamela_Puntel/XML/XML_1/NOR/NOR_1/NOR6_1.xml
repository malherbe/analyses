<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR6">
				<head type="main">A MONSIEUR X***</head>
				<head type="sub">PROPRIÉTAIRE</head>
				<opener>
					<dateline>
						<date when="1870">Bobigny, novembre 1870.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">AU</w> <w n="1.2">sein</w> <w n="1.3">d</w>’<w n="1.4">un</w> <w n="1.5">vaste</w> <w n="1.6">champ</w> <w n="1.7">d</w>’<w n="1.8">ognons</w> <w n="1.9">et</w> <w n="1.10">de</w> <w n="1.11">poireaux</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Une</w> <w n="2.2">vieille</w> <w n="2.3">maison</w> <w n="2.4">sans</w> <w n="2.5">porte</w> <w n="2.6">ni</w> <w n="2.7">carreaux</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="3.1">Où</w> <w n="3.2">le</w> <w n="3.3">vent</w> <w n="3.4">pleure</w> :</l>
					<l n="4" num="1.4"><w n="4.1">Quatre</w> <w n="4.2">murs</w> <w n="4.3">enfumés</w>, <w n="4.4">ornés</w> <w n="4.5">par</w>-<w n="4.6">ci</w>, <w n="4.7">par</w>-<w n="4.8">là</w>,</l>
					<l n="5" num="1.5"><w n="5.1">De</w> <w n="5.2">créneaux</w> '<w n="5.3">et</w> <w n="5.4">de</w> <w n="5.5">trous</w>, <w n="5.6">un</w> <w n="5.7">toit</w> <w n="5.8">percé</w>, <w n="5.9">voilà</w></l>
					<l n="6" num="1.6"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="6.1">Notre</w> <w n="6.2">demeure</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">C</w>’<w n="7.2">est</w> <w n="7.3">dans</w> <w n="7.4">cette</w> <w n="7.5">oasis</w> <w n="7.6">que</w> <w n="7.7">nous</w> <w n="7.8">avons</w> <w n="7.9">passé</w></l>
					<l n="8" num="2.2"><w n="8.1">Quelques</w> <w n="8.2">nuits</w>, <w n="8.3">cet</w> <w n="8.4">hiver</w>, <w n="8.5">sur</w> <w n="8.6">un</w> <w n="8.7">parquet</w> <w n="8.8">glacé</w>,</l>
					<l n="9" num="2.3"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="9.1">Par</w> <w n="9.2">un</w> <w n="9.3">froid</w> <w n="9.4">russe</w> ;</l>
					<l n="10" num="2.4"><w n="10.1">Que</w> <w n="10.2">nous</w> <w n="10.3">avons</w> <w n="10.4">fermé</w>, <w n="10.5">sans</w> <w n="10.6">trêve</w> <w n="10.7">ni</w> <w n="10.8">merci</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Le</w> <w n="11.2">chassepot</w> <w n="11.3">en</w> <w n="11.4">main</w>, <w n="11.5">la</w> <w n="11.6">route</w> <w n="11.7">du</w> <w n="11.8">Drancy</w></l>
					<l n="12" num="2.6"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="12.1">Au</w> <w n="12.2">roi</w> <w n="12.3">de</w> <w n="12.4">Prusse</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">la</w> <w n="13.4">que</w> <w n="13.5">nous</w> <w n="13.6">montions</w> <w n="13.7">nos</w> <w n="13.8">grand</w>’<w n="13.9">gardes</w> <w n="13.10">de</w> <w n="13.11">nuit</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Près</w> <w n="14.2">d</w>’<w n="14.3">un</w> <w n="14.4">mur</w> <w n="14.5">crénelé</w>, <w n="14.6">dans</w> <w n="14.7">la</w> <w n="14.8">plaine</w>, <w n="14.9">sans</w> <w n="14.10">bruit</w>,</l>
					<l n="15" num="3.3"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="15.1">Maussades</w> <w n="15.2">poses</w> !</l>
					<l n="16" num="3.4"><w n="16.1">Aux</w> <w n="16.2">heures</w> <w n="16.3">où</w> <w n="16.4">jadis</w> <w n="16.5">nous</w> <w n="16.6">dormions</w> <w n="16.7">chaudement</w>,</l>
					<l n="17" num="3.5"><w n="17.1">Et</w> <w n="17.2">faisions</w> <w n="17.3">à</w> <w n="17.4">loisir</w> — <w n="17.5">et</w> <w n="17.6">sans</w> <w n="17.7">bombardement</w> —</l>
					<l n="18" num="3.6"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="18.1">Des</w> <w n="18.2">rêves</w> <w n="18.3">roses</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">là</w> <w n="19.4">que</w> <w n="19.5">j</w>’<w n="19.6">ai</w> <w n="19.7">fumé</w>, <w n="19.8">par</w> <w n="19.9">grand</w> <w n="19.10">désœuvrement</w>,</l>
					<l n="20" num="4.2"><w n="20.1">Moi</w> <w n="20.2">qui</w> <w n="20.3">ne</w> <w n="20.4">fumais</w> <w n="20.5">pas</w> <w n="20.6">jadis</w>, <w n="20.7">énormément</w></l>
					<l n="21" num="4.3"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="21.1">De</w> <w n="21.2">cigarettes</w> ;</l>
					<l n="22" num="4.4"><w n="22.1">La</w> <w n="22.2">que</w> <w n="22.3">plus</w> <w n="22.4">d</w>’<w n="22.5">une</w> <w n="22.6">fois</w>, — <w n="22.7">concert</w> <w n="22.8">très</w>-<w n="22.9">inconnu</w></l>
					<l n="23" num="4.5"><w n="23.1">A</w> <w n="23.2">Paris</w>, — <w n="23.3">j</w>’<w n="23.4">entendis</w> <w n="23.5">le</w> <w n="23.6">babil</w> <w n="23.7">ingénu</w></l>
					<l n="24" num="4.6"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="24.1">Des</w> <w n="24.2">alouettes</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">C</w>’<w n="25.2">est</w> <w n="25.3">là</w> <w n="25.4">que</w> <w n="25.5">j</w>’<w n="25.6">ai</w> <w n="25.7">souvent</w> <w n="25.8">maudit</w> <w n="25.9">à</w> <w n="25.10">plein</w> <w n="25.11">juron</w></l>
					<l n="26" num="5.2"><w n="26.1">Les</w> <w n="26.2">Bismarck</w>, <w n="26.3">les</w> <w n="26.4">Guillaume</w> <w n="26.5">et</w> <w n="26.6">les</w> <w n="26.7">Napoléon</w>,</l>
					<l n="27" num="5.3"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="27.1">Grands</w> <w n="27.2">tueurs</w> <w n="27.3">d</w>’<w n="27.4">hommes</w>,</l>
					<l n="28" num="5.4"><w n="28.1">Qui</w>, <w n="28.2">sans</w> <w n="28.3">jamais</w> <w n="28.4">quitter</w> <w n="28.5">sommiers</w> <w n="28.6">ni</w> <w n="28.7">matelas</w>,</l>
					<l n="29" num="5.5"><w n="29.1">Ronflent</w> <w n="29.2">toute</w> <w n="29.3">la</w> <w n="29.4">nuit</w> <w n="29.5">et</w> <w n="29.6">font</w> <w n="29.7">faire</w> <w n="29.8">aux</w> <w n="29.9">soldats</w></l>
					<l n="30" num="5.6"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="30.1">Les</w> <w n="30.2">mauvais</w> <w n="30.3">sommes</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">C</w>’<w n="31.2">est</w> <w n="31.3">là</w> <w n="31.4">qu</w>’<w n="31.5">en</w> <w n="31.6">sentinelle</w>, <w n="31.7">assis</w> <w n="31.8">sur</w> <w n="31.9">un</w> <w n="31.10">tonneau</w>,</l>
					<l n="32" num="6.2"><w n="32.1">Sans</w> <w n="32.2">broncher</w>, <w n="32.3">j</w>’<w n="32.4">ai</w> <w n="32.5">reçu</w> <w n="32.6">de</w> <w n="32.7">la</w> <w n="32.8">grêle</w>, <w n="32.9">de</w> <w n="32.10">l</w>’<w n="32.11">eau</w></l>
					<l n="33" num="6.3"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="33.1">Et</w> <w n="33.2">de</w> <w n="33.3">la</w> <w n="33.4">neige</w> ;</l>
					<l n="34" num="6.4"><w n="34.1">Là</w> <w n="34.2">que</w> <w n="34.3">plus</w> <w n="34.4">d</w>’<w n="34.5">une</w> <w n="34.6">fois</w>, <w n="34.7">dans</w> <w n="34.8">ce</w> <w n="34.9">tonneau</w> <w n="34.10">plongé</w></l>
					<l n="35" num="6.5"><w n="35.1">Jusques</w> <w n="35.2">au</w> <w n="35.3">haut</w> <w n="35.4">des</w> <w n="35.5">reins</w>, <w n="35.6">j</w>’<w n="35.7">ai</w> <w n="35.8">pris</w>, <w n="35.9">pauvre</w> <w n="35.10">assiégé</w>,</l>
					<l n="36" num="6.6"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="36.1">Des</w> <w n="36.2">bains</w> <w n="36.3">de</w> <w n="36.4">siège</w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">là</w> <w n="37.4">que</w> <w n="37.5">j</w>’<w n="37.6">ai</w> <w n="37.7">compris</w> <w n="37.8">qu</w>’<w n="37.9">un</w> <w n="37.10">soldat</w>, <w n="37.11">aujourd</w>’<w n="37.12">hui</w>,</l>
					<l n="38" num="7.2"><w n="38.1">N</w>’<w n="38.2">est</w> <w n="38.3">qu</w>’<w n="38.4">une</w> <w n="38.5">molécule</w>, <w n="38.6">un</w> <w n="38.7">lui</w> <w n="38.8">qui</w> <w n="38.9">n</w>’<w n="38.10">est</w> <w n="38.11">pas</w> <w n="38.12">lui</w>,</l>
					<l n="39" num="7.3"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="39.1">Une</w> <w n="39.2">machine</w>,</l>
					<l n="40" num="7.4"><w n="40.1">A</w> <w n="40.2">qui</w> <w n="40.3">le</w> <w n="40.4">bout</w> <w n="40.5">du</w> <w n="40.6">nez</w> <w n="40.7">doit</w> <w n="40.8">servir</w> <w n="40.9">d</w>’<w n="40.10">horizon</w>,</l>
					<l n="41" num="7.5"><w n="41.1">Et</w> <w n="41.2">qui</w> <w n="41.3">produit</w> <w n="41.4">l</w>’<w n="41.5">effet</w> <w n="41.6">sans</w> <w n="41.7">chercher</w> <w n="41.8">la</w> <w n="41.9">raison</w></l>
					<l n="42" num="7.6"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="42.1">Qui</w> <w n="42.2">détermine</w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">O</w> <w n="43.2">toi</w>, <w n="43.3">qui</w> <w n="43.4">que</w> <w n="43.5">tu</w> <w n="43.6">sois</w>, <w n="43.7">dont</w> <w n="43.8">j</w>’<w n="43.9">ignore</w> <w n="43.10">le</w> <w n="43.11">nom</w>,</l>
					<l n="44" num="8.2"><w n="44.1">Monsieur</w> <subst type="phonemization" hand="RR" reason="analysis"><del>X</del><add rend="hidden"><w n="44.2">iks</w></add></subst>***, <w n="44.3">de</w> <w n="44.4">ce</w> <w n="44.5">champ</w> <w n="44.6">et</w> <w n="44.7">de</w> <w n="44.8">cette</w> <w n="44.9">maison</w></l>
					<l n="45" num="8.3"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="45.1">Propriétaire</w>,</l>
					<l n="46" num="8.4"><w n="46.1">Sois</w> <w n="46.2">sûr</w> <w n="46.3">qu</w>’<w n="46.4">en</w> <w n="46.5">les</w> <w n="46.6">quittant</w> <w n="46.7">je</w> <w n="46.8">n</w>'<w n="46.9">ai</w> <w n="46.10">rien</w> <w n="46.11">regretté</w>,</l>
					<l n="47" num="8.5"><w n="47.1">Rien</w>, <w n="47.2">si</w> <w n="47.3">ce</w> <w n="47.4">n</w>’<w n="47.5">est</w> <w n="47.6">pourtant</w> <w n="47.7">ceci</w> : <w n="47.8">d</w>’<w n="47.9">avoir</w> <w n="47.10">été</w></l>
					<l n="48" num="8.6"><space unit="char" quantity="16"></space><space quantity="16" unit="char"></space><w n="48.1">Ton</w> <w n="48.2">locataire</w>.</l>
				</lg>
			</div></body></text></TEI>