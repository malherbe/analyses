<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA PRUSSIADE</title>
				<title type="sub_2">OU</title>
				<title type="sub_1">LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
				<title type="sub">1870-71</title>
				<title type="sub">Douze poëmes par un Suisse</title>
				<title type="medium">Édition électronique</title>
				<author key="VCL">
					<name>
						<forename>Henri</forename>
						<surname>VALLON-COLLEY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>846 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VCL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA PRUSSIADE OU LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
						<author>HENRI M. VALLON-COLLEY</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LACHAUD</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VCL4">
				<head type="main">L’HÉROÏNE DE STRASBOURG</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
									Woe to the conqueror !<lb></lb>
									Our limbs shall lie as cold as theirs<lb></lb>
									Of whom his sword berets us,<lb></lb>
									Ere we forget the deep arrears<lb></lb>
									Of vengeance they have left us !<lb></lb>
									Woe to the conqueror !
							</quote>
							<bibl>
								<name>MOORE</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Après</w> <w n="1.2">une</w> <w n="1.3">barbare</w>, <w n="1.4">exécrable</w> <w n="1.5">victoire</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">la</w> <w n="2.3">postérité</w> <w n="2.4">proclamera</w> <w n="2.5">sans</w> <w n="2.6">gloire</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">soldats</w> <w n="3.3">allemands</w> <w n="3.4">par</w> <w n="3.5">bataillons</w> <w n="3.6">nombreux</w></l>
						<l n="4" num="1.4"><w n="4.1">Pénétraient</w> <w n="4.2">dans</w> <w n="4.3">Strasbourg</w>, <w n="4.4">insolents</w>, <w n="4.5">radieux</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Les</w> <w n="5.2">airs</w> <w n="5.3">guerriers</w>, <w n="5.4">joyeux</w>, <w n="5.5">des</w> <w n="5.6">marches</w> <w n="5.7">triomphales</w></l>
						<l n="6" num="1.6"><w n="6.1">Se</w> <w n="6.2">mêlaient</w> <w n="6.3">aux</w> <w n="6.4">sanglots</w>, <w n="6.5">aux</w> <w n="6.6">pleurs</w>, <w n="6.7">aux</w> <w n="6.8">cris</w>, <w n="6.9">aux</w> <w n="6.10">râles</w></l>
						<l n="7" num="1.7"><w n="7.1">Des</w> <w n="7.2">valeureux</w> <w n="7.3">vaincus</w>, <w n="7.4">qui</w> <w n="7.5">de</w> <w n="7.6">l</w>’<w n="7.7">antiquité</w></l>
						<l n="8" num="1.8"><w n="8.1">Venaient</w> <w n="8.2">de</w> <w n="8.3">surpasser</w> <w n="8.4">le</w> <w n="8.5">courage</w> <w n="8.6">exalté</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Soudain</w>, <w n="9.2">semblant</w> <w n="9.3">sortir</w> <w n="9.4">du</w> <w n="9.5">royaume</w> <w n="9.6">des</w> <w n="9.7">ombres</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Une</w> <w n="10.2">femme</w> <w n="10.3">paraît</w> <w n="10.4">debout</w> <w n="10.5">sur</w> <w n="10.6">des</w> <w n="10.7">décombres</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Elle</w> <w n="11.2">est</w> <w n="11.3">maigre</w>, <w n="11.4">elle</w> <w n="11.5">est</w> <w n="11.6">pâle</w>, <w n="11.7">et</w> <w n="11.8">pourtant</w> <w n="11.9">son</w> <w n="11.10">œil</w> <w n="11.11">bleu</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Brillant</w> <w n="12.2">d</w>’<w n="12.3">un</w> <w n="12.4">vif</w> <w n="12.5">éclat</w>, <w n="12.6">lance</w> <w n="12.7">un</w> <w n="12.8">rayon</w> <w n="12.9">de</w> <w n="12.10">feu</w>.</l>
						<l n="13" num="1.13"><w n="13.1">De</w> <w n="13.2">dessous</w> <w n="13.3">les</w> <w n="13.4">haillons</w> <w n="13.5">qui</w> <w n="13.6">la</w> <w n="13.7">couvrent</w> <w n="13.8">à</w> <w n="13.9">peine</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Sans</w> <w n="14.2">trembler</w>, <w n="14.3">elle</w> <w n="14.4">tire</w> <w n="14.5">un</w> <w n="14.6">poignard</w>, <w n="14.7">le</w> <w n="14.8">dégaîne</w>,</l>
						<l n="15" num="1.15"><w n="15.1">S</w>’<w n="15.2">élance</w> <w n="15.3">dans</w> <w n="15.4">la</w> <w n="15.5">rue</w> <w n="15.6">en</w> <w n="15.7">s</w>’<w n="15.8">écriant</w> : « <w n="15.9">Je</w> <w n="15.10">veux</w></l>
						<l n="16" num="1.16"><w n="16.1">Faire</w> <w n="16.2">aussi</w> <w n="16.3">mon</w> <w n="16.4">devoir</w>, <w n="16.5">au</w> <w n="16.6">moins</w> <w n="16.7">en</w> <w n="16.8">tuer</w> <w n="16.9">deux</w> ! »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="17" num="1.1"><w n="17.1">Dans</w> <w n="17.2">un</w> <w n="17.3">salon</w> <w n="17.4">d</w>’<w n="17.5">hôtel</w>, <w n="17.6">sablant</w> <w n="17.7">du</w> <w n="17.8">pur</w> <w n="17.9">champagne</w>,</l>
						<l n="18" num="1.2"><w n="18.1">Un</w> <w n="18.2">général</w> <w n="18.3">badois</w> <w n="18.4">encense</w> <w n="18.5">l</w>’<w n="18.6">Allemagne</w> ;</l>
						<l n="19" num="1.3"><w n="19.1">Trois</w> <w n="19.2">colonels</w> <w n="19.3">prussiens</w> <w n="19.4">avec</w> <w n="19.5">du</w> <w n="19.6">cru</w> <w n="19.7">du</w> <w n="19.8">Rhin</w></l>
						<l n="20" num="1.4"><w n="20.1">Boivent</w> <w n="20.2">à</w> <w n="20.3">la</w> <w n="20.4">santé</w> <w n="20.5">de</w> <w n="20.6">leur</w> <w n="20.7">vieux</w> <w n="20.8">souverain</w>.</l>
						<l n="21" num="1.5"><w n="21.1">Ces</w> <w n="21.2">fiers</w> <w n="21.3">représentants</w> <w n="21.4">du</w> <w n="21.5">pouvoir</w> <w n="21.6">militaire</w> —</l>
						<l n="22" num="1.6"><w n="22.1">On</w> <w n="22.2">en</w> <w n="22.3">pourrait</w> <w n="22.4">douter</w> — <w n="22.5">sont</w> <w n="22.6">en</w> <w n="22.7">conseil</w> <w n="22.8">de</w> <w n="22.9">guerre</w></l>
						<l n="23" num="1.7"><w n="23.1">Assemblés</w>, <w n="23.2">pour</w> <w n="23.3">juger</w> <w n="23.4">un</w> <w n="23.5">traitre</w> ? <w n="23.6">un</w> <w n="23.7">déserteur</w> ?</l>
						<l n="24" num="1.8"><w n="24.1">Non</w> ! <w n="24.2">une</w> <w n="24.3">patriote</w> <w n="24.4">au</w> <w n="24.5">mâle</w> <w n="24.6">et</w> <w n="24.7">noble</w> <w n="24.8">cœur</w>.</l>
						<l n="25" num="1.9"><w n="25.1">Un</w> <w n="25.2">gigantesque</w> <w n="25.3">uhlan</w> <w n="25.4">introduit</w> <w n="25.5">la</w> <w n="25.6">coupable</w>,</l>
						<l n="26" num="1.10"><w n="26.1">Que</w> <w n="26.2">le</w> <w n="26.3">vaillant</w> <w n="26.4">Badois</w>, <w n="26.5">d</w>’<w n="26.6">une</w> <w n="26.7">voix</w> <w n="26.8">formidable</w></l>
						<l n="27" num="1.11"><w n="27.1">Apostrophe</w> <w n="27.2">en</w> <w n="27.3">ces</w> <w n="27.4">mots</w> : « <w n="27.5">Vous</w> <w n="27.6">avez</w>, <w n="27.7">sans</w> <w n="27.8">effroi</w>,</l>
						<l n="28" num="1.12"><w n="28.1">Commis</w> <w n="28.2">un</w> <w n="28.3">attentat</w> <w n="28.4">sur</w> <w n="28.5">des</w> <w n="28.6">soldats</w> <w n="28.7">du</w> <w n="28.8">roi</w>,</l>
						<l n="29" num="1.13"><w n="29.1">Vous</w> <w n="29.2">méritez</w> <w n="29.3">la</w> <w n="29.4">mort</w> ! <w n="29.5">Cependant</w> <w n="29.6">une</w> <w n="29.7">chance</w></l>
						<l n="30" num="1.14"><w n="30.1">Vous</w> <w n="30.2">est</w> <w n="30.3">offerte</w> <w n="30.4">encor</w> : <w n="30.5">prenez</w> <w n="30.6">votre</w> <w n="30.7">défense</w> ;</l>
						<l n="31" num="1.15"><w n="31.1">Mais</w> <w n="31.2">je</w> <w n="31.3">vous</w> <w n="31.4">avertis</w> <w n="31.5">qu</w>’<w n="31.6">il</w> <w n="31.7">vous</w> <w n="31.8">est</w> <w n="31.9">défendu</w></l>
						<l n="32" num="1.16"><w n="32.1">D</w>’<w n="32.2">oublier</w> <w n="32.3">un</w> <w n="32.4">instant</w> <w n="32.5">le</w> <w n="32.6">respect</w> <w n="32.7">qui</w> <w n="32.8">m</w>’<w n="32.9">est</w> <w n="32.10">dû</w>. »</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="33" num="1.1">« <w n="33.1">Monsieur</w> <w n="33.2">le</w> <w n="33.3">général</w>, <w n="33.4">j</w>’<w n="33.5">avais</w> <w n="33.6">un</w> <w n="33.7">heureux</w> <w n="33.8">père</w>,</l>
						<l n="34" num="1.2"><w n="34.1">Deux</w> <w n="34.2">innocentes</w> <w n="34.3">sœurs</w>, <w n="34.4">une</w> <w n="34.5">pieuse</w> <w n="34.6">mère</w>,</l>
						<l n="35" num="1.3"><w n="35.1">Que</w> <w n="35.2">j</w>’<w n="35.3">aimais</w> <w n="35.4">tendrement</w>. <w n="35.5">Ma</w> <w n="35.6">famille</w>, <w n="35.7">en</w> <w n="35.8">retour</w>,</l>
						<l n="36" num="1.4"><w n="36.1">Me</w> <w n="36.2">prodiguait</w> <w n="36.3">les</w> <w n="36.4">soins</w> <w n="36.5">du</w> <w n="36.6">plus</w> <w n="36.7">fervent</w> <w n="36.8">amour</w>.</l>
						<l n="37" num="1.5"><w n="37.1">D</w>’<w n="37.2">être</w> <w n="37.3">aimé</w> <w n="37.4">de</w> <w n="37.5">quelqu</w>’<w n="37.6">un</w> <w n="37.7">qu</w>’<w n="37.8">on</w> <w n="37.9">estime</w>, <w n="37.10">qu</w>’<w n="37.11">on</w> <w n="37.12">aime</w>,</l>
						<l n="38" num="1.6"><w n="38.1">Du</w> <w n="38.2">bonheur</w> <w n="38.3">n</w>’<w n="38.4">est</w>-<w n="38.5">ce</w> <w n="38.6">pas</w> <w n="38.7">la</w> <w n="38.8">volupté</w> <w n="38.9">suprême</w> ?</l>
						<l n="39" num="1.7"><w n="39.1">Bref</w>, <w n="39.2">nous</w> <w n="39.3">étions</w> <w n="39.4">heureux</w>. <w n="39.5">Tout</w> <w n="39.6">à</w> <w n="39.7">coup</w>, <w n="39.8">une</w> <w n="39.9">nuit</w>,</l>
						<l n="40" num="1.8"><w n="40.1">La</w> <w n="40.2">terre</w> <w n="40.3">est</w> <w n="40.4">ébranlée</w>, <w n="40.5">un</w> <w n="40.6">effroyable</w> <w n="40.7">bruit</w></l>
						<l n="41" num="1.9"><w n="41.1">Remplit</w> <w n="41.2">l</w>’<w n="41.3">air</w>. <w n="41.4">Des</w> <w n="41.5">lueurs</w> <w n="41.6">passent</w> <w n="41.7">dans</w> <w n="41.8">le</w> <w n="41.9">ciel</w> <w n="41.10">sombre</w> :</l>
						<l n="42" num="1.10"><w n="42.1">Des</w> <w n="42.2">bombes</w>, <w n="42.3">des</w> <w n="42.4">boulets</w> <w n="42.5">et</w> <w n="42.6">des</w> <w n="42.7">obus</w> <w n="42.8">sans</w> <w n="42.9">nombre</w></l>
						<l n="43" num="1.11"><w n="43.1">Pleuvent</w> <w n="43.2">de</w> <w n="43.3">tous</w> <w n="43.4">côtés</w>. <w n="43.5">Le</w> <w n="43.6">lendemain</w>, <w n="43.7">hélas</w> !</l>
						<l n="44" num="1.12"><w n="44.1">J</w>’<w n="44.2">appelle</w> <w n="44.3">mes</w> <w n="44.4">parents</w>, <w n="44.5">ils</w> <w n="44.6">ne</w> <w n="44.7">répondent</w> <w n="44.8">pas</w>.</l>
						<l n="45" num="1.13"><w n="45.1">Des</w> <w n="45.2">femmes</w>, <w n="45.3">des</w> <w n="45.4">enfants</w>, <w n="45.5">des</w> <w n="45.6">septuagénaires</w>,</l>
						<l n="46" num="1.14"><w n="46.1">Gisent</w> <w n="46.2">mourants</w> <w n="46.3">ou</w> <w n="46.4">morts</w> <w n="46.5">sous</w> <w n="46.6">des</w> <w n="46.7">monceaux</w> <w n="46.8">de</w> <w n="46.9">pierres</w>,</l>
						<l n="47" num="1.15"><w n="47.1">Et</w> <w n="47.2">cette</w> <w n="47.3">œuvre</w> <w n="47.4">est</w> <w n="47.5">la</w> <w n="47.6">vôtre</w>, <w n="47.7">ô</w> <w n="47.8">cruels</w> <w n="47.9">Allemands</w> !</l>
						<l n="48" num="1.16"><w n="48.1">Œuvre</w> <w n="48.2">non</w> <w n="48.3">des</w> <w n="48.4">soldats</w>, <w n="48.5">mais</w> <w n="48.6">plutôt</w> <w n="48.7">de</w> <w n="48.8">brigands</w> ! »</l>
						<l n="49" num="1.17"><w n="49.1">A</w> <w n="49.2">ces</w> <w n="49.3">mots</w> <w n="49.4">le</w> <w n="49.5">Badois</w>, <w n="49.6">tout</w> <w n="49.7">frémissant</w> <w n="49.8">de</w> <w n="49.9">rage</w>,</l>
						<l n="50" num="1.18"><w n="50.1">Va</w> <w n="50.2">droit</w> <w n="50.3">à</w> <w n="50.4">l</w>’<w n="50.5">accusé</w> <w n="50.6">et</w> <w n="50.7">la</w> <w n="50.8">frappe</w> <w n="50.9">au</w> <w n="50.10">visage</w>.</l>
						<l n="51" num="1.19"><w n="51.1">L</w>’<w n="51.2">Alsacienne</w>, <w n="51.3">alors</w>, <w n="51.4">de</w> <w n="51.5">ce</w> <w n="51.6">lâche</w> <w n="51.7">bourreau</w></l>
						<l n="52" num="1.20"><w n="52.1">Se</w> <w n="52.2">venge</w> <w n="52.3">lui</w> <w n="52.4">lançant</w> <w n="52.5">le</w> <w n="52.6">mot</w> <w n="52.7">de</w> <w n="52.8">Waterloo</w>.</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="2">
						<l n="53" num="2.1"><w n="53.1">Le</w> <w n="53.2">soir</w> <w n="53.3">du</w> <w n="53.4">même</w> <w n="53.5">jour</w> <w n="53.6">voyait</w> <w n="53.7">la</w> <w n="53.8">fin</w> <w n="53.9">du</w> <w n="53.10">drame</w> :</l>
						<l n="54" num="2.2"><w n="54.1">Six</w> <w n="54.2">grenadiers</w> <w n="54.3">prussiens</w> <w n="54.4">fusillaient</w> <w n="54.5">une</w> <w n="54.6">femme</w>,</l>
						<l n="55" num="2.3"><w n="55.1">Qui</w>, <w n="55.2">tombant</w>, <w n="55.3">leur</w> <w n="55.4">criait</w> : « <w n="55.5">Vous</w> <w n="55.6">n</w>’<w n="55.7">avez</w> <w n="55.8">pu</w>, <w n="55.9">vainqueurs</w>,</l>
						<l n="56" num="2.4"><w n="56.1">Ni</w> <w n="56.2">nous</w> <w n="56.3">déshonorer</w>, <w n="56.4">ni</w> <w n="56.5">subjuguer</w> <w n="56.6">nos</w> <w n="56.7">cœurs</w> ! »</l>
					</lg>
				</div>
			</div></body></text></TEI>