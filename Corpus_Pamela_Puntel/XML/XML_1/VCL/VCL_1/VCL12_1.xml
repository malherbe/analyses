<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA PRUSSIADE</title>
				<title type="sub_2">OU</title>
				<title type="sub_1">LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
				<title type="sub">1870-71</title>
				<title type="sub">Douze poëmes par un Suisse</title>
				<title type="medium">Édition électronique</title>
				<author key="VCL">
					<name>
						<forename>Henri</forename>
						<surname>VALLON-COLLEY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>846 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VCL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA PRUSSIADE OU LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
						<author>HENRI M. VALLON-COLLEY</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LACHAUD</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VCL12">
				<head type="main">UN HORRIBLE GUET-APENS</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
									Le 3 août 1871, à Poligny, près de Dôle, au<lb></lb>
									pied du Jura, un citoyen ayant (cela pour cause)<lb></lb>
									tiré sur des dragons prussiens, lesdits dragons<lb></lb>
									ont passé au galop dans les rues, sabrant sans<lb></lb>
									miséricorde tous ceux qu’ils rencontraient sur<lb></lb>
									leur passage.<lb></lb>
									Résultats : 38 morts et blessés (journaux<lb></lb>
									français, suisses, anglais, italiens).<lb></lb>
									Non par un manque de patriotisme, mais dans<lb></lb>
									l’espoir qu’en agissant ainsi ils prédisposaient<lb></lb>
									les vainqueurs en faveur de leurs concitoyens,<lb></lb>
									des Français occupant une position officielle ont<lb></lb>
									cru devoir ne pas refuser des invitations à eux<lb></lb>
									faites par des Prussiens d’un certain grade. 
							</quote>
							<bibl>
								<name></name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Grâce</w> <w n="1.2">à</w> <w n="1.3">la</w> <w n="1.4">trahison</w>, <w n="1.5">à</w> <w n="1.6">l</w>’<w n="1.7">incapacité</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Au</w> <w n="2.2">titanique</w> <w n="2.3">orgueil</w>, <w n="2.4">à</w> <w n="2.5">l</w>’<w n="2.6">immoralité</w></l>
					<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">plusieurs</w> <w n="3.3">de</w> <w n="3.4">ses</w> <w n="3.5">chefs</w>, <w n="3.6">la</w> <w n="3.7">France</w> <w n="3.8">malheureuse</w></l>
					<l n="4" num="1.4"><w n="4.1">Vient</w> <w n="4.2">de</w> <w n="4.3">conclure</w>, <w n="4.4">hélas</w> ! <w n="4.5">une</w> <w n="4.6">paix</w> <w n="4.7">désastreuse</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Dans</w> <w n="5.2">les</w> <w n="5.3">deux</w> <w n="5.4">camps</w> <w n="5.5">l</w>’<w n="5.6">alarme</w> <w n="5.7">a</w> <w n="5.8">fini</w> <w n="5.9">de</w> <w n="5.10">sonner</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">canons</w> <w n="6.3">des</w> <w n="6.4">rivaux</w> <w n="6.5">ont</w> <w n="6.6">cessé</w> <w n="6.7">de</w> <w n="6.8">tonner</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Mais</w> <w n="7.2">parmi</w> <w n="7.3">les</w> <w n="7.4">Germains</w> <w n="7.5">habite</w> <w n="7.6">encor</w> <w n="7.7">la</w> <w n="7.8">haine</w> :</l>
					<l n="8" num="1.8"><w n="8.1">De</w> <w n="8.2">fiel</w> <w n="8.3">et</w> <w n="8.4">de</w> <w n="8.5">venin</w> <w n="8.6">ils</w> <w n="8.7">ont</w> <w n="8.8">tous</w> <w n="8.9">l</w>’<w n="8.10">âme</w> <w n="8.11">pleine</w>.</l>
					<l n="9" num="1.9"><w n="9.1">A</w> <w n="9.2">leurs</w> <w n="9.3">crimes</w> <w n="9.4">nombreux</w>, <w n="9.5">ces</w> <w n="9.6">horribles</w> <w n="9.7">bourreaux</w></l>
					<l n="10" num="1.10"><w n="10.1">Ajoutent</w> <w n="10.2">chaque</w> <w n="10.3">jour</w> <w n="10.4">quelques</w> <w n="10.5">crimes</w> <w n="10.6">nouveaux</w>.</l>
				</lg>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="11" num="1.1"><w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">dans</w> <w n="11.4">un</w> <w n="11.5">petit</w> <w n="11.6">bourg</w>. <w n="11.7">Le</w> <w n="11.8">temps</w> <w n="11.9">est</w> <w n="11.10">à</w> <w n="11.11">la</w> <w n="11.12">pluie</w>.</l>
						<l n="12" num="1.2"><w n="12.1">D</w>’<w n="12.2">un</w> <w n="12.3">bataillon</w> <w n="12.4">prussien</w> <w n="12.5">le</w> <w n="12.6">gros</w>-<w n="12.7">major</w> <w n="12.8">s</w>’<w n="12.9">ennuie</w> ;</l>
						<l n="13" num="1.3"><w n="13.1">Tous</w> <w n="13.2">ses</w> <w n="13.3">troupiers</w> <w n="13.4">aussi</w>. <w n="13.5">L</w>’<w n="13.6">ardente</w> <w n="13.7">garnison</w></l>
						<l n="14" num="1.4"><w n="14.1">Trouve</w> <w n="14.2">que</w> <w n="14.3">les</w> <w n="14.4">vaincus</w> <w n="14.5">montrent</w> <w n="14.6">trop</w> <w n="14.7">de</w> <w n="14.8">raison</w>,</l>
						<l n="15" num="1.5"><w n="15.1">Trop</w> <w n="15.2">de</w> <w n="15.3">sang</w>-<w n="15.4">froid</w>, <w n="15.5">de</w> <w n="15.6">calme</w> <w n="15.7">et</w> <w n="15.8">trop</w> <w n="15.9">d</w>’<w n="15.10">indifférence</w>,</l>
						<l n="16" num="1.6"><w n="16.1">Lorsqu</w>’<w n="16.2">on</w> <w n="16.3">se</w> <w n="16.4">moque</w> <w n="16.5">d</w>’<w n="16.6">eux</w> <w n="16.7">ou</w> <w n="16.8">de</w> <w n="16.9">leur</w> <w n="16.10">belle</w> <w n="16.11">France</w>.</l>
						<l n="17" num="1.7">« <w n="17.1">De</w> <w n="17.2">ce</w> <w n="17.3">sommeil</w> <w n="17.4">de</w> <w n="17.5">plomb</w> <w n="17.6">je</w> <w n="17.7">veux</w> <w n="17.8">les</w> <w n="17.9">réveiller</w>,</l>
						<l n="18" num="1.8"><w n="18.1">Sans</w> <w n="18.2">cela</w> <w n="18.3">nos</w> <w n="18.4">fusils</w> <w n="18.5">vont</w> <w n="18.6">bientôt</w> <w n="18.7">se</w> <w n="18.8">rouiller</w>,</l>
						<l n="19" num="1.9"><w n="19.1">Marmotte</w> <w n="19.2">le</w> <w n="19.3">major</w>. <w n="19.4">Il</w> <w n="19.5">faut</w> <w n="19.6">qu</w>’<w n="19.7">une</w> <w n="19.8">tempête</w></l>
						<l n="20" num="1.10"><w n="20.1">Éclate</w> <w n="20.2">tout</w> <w n="20.3">à</w> <w n="20.4">coup</w>, <w n="20.5">que</w> <w n="20.6">je</w> <w n="20.7">donne</w> <w n="20.8">une</w> <w n="20.9">fête</w></l>
						<l n="21" num="1.11"><w n="21.1">Aux</w> <w n="21.2">miens</w>, <w n="21.3">L</w>’<w n="21.4">odeur</w> <w n="21.5">du</w> <w n="21.6">sang</w> <w n="21.7">et</w> <w n="21.8">les</w> <w n="21.9">cris</w> <w n="21.10">des</w> <w n="21.11">blessés</w></l>
						<l n="22" num="1.12"><w n="22.1">Les</w> <w n="22.2">feront</w> <w n="22.3">souvenir</w> <w n="22.4">de</w> <w n="22.5">leurs</w> <w n="22.6">exploits</w> <w n="22.7">passés</w>.</l>
						<l n="23" num="1.13"><w n="23.1">En</w> <w n="23.2">auteur</w> <w n="23.3">entendu</w>, <w n="23.4">d</w>’<w n="23.5">un</w> <w n="23.6">charmant</w> <w n="23.7">incendie</w></l>
						<l n="24" num="1.14"><w n="24.1">Je</w> <w n="24.2">ne</w> <w n="24.3">veux</w> <w n="24.4">pas</w> <w n="24.5">manquer</w> <w n="24.6">d</w>’<w n="24.7">orner</w> <w n="24.8">la</w> <w n="24.9">tragédie</w>.</l>
						<l n="25" num="1.15"><w n="25.1">Je</w> <w n="25.2">ferai</w>, <w n="25.3">ce</w> <w n="25.4">soir</w> <w n="25.5">même</w>, <w n="25.6">usage</w> <w n="25.7">d</w>’<w n="25.8">un</w> <w n="25.9">moyen</w></l>
						<l n="26" num="1.16"><w n="26.1">Qui</w> <w n="26.2">me</w> <w n="26.3">réussira</w>, <w n="26.4">foi</w> <w n="26.5">de</w> <w n="26.6">héros</w> <w n="26.7">prussien</w>. »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="27" num="1.1"><w n="27.1">Minuit</w> <w n="27.2">sonne</w> <w n="27.3">à</w> <w n="27.4">la</w> <w n="27.5">tour</w>. « <w n="27.6">Mon</w> <w n="27.7">Dieu</w> ! <w n="27.8">que</w> <w n="27.9">peut</w> <w n="27.10">donc</w> <w n="27.11">faire</w></l>
						<l n="28" num="1.2"><w n="28.1">Papa</w> <w n="28.2">si</w> <w n="28.3">tard</w> <w n="28.4">dehors</w> ? <w n="28.5">Tu</w> <w n="28.6">sais</w> <w n="28.7">qu</w>’<w n="28.8">à</w> ‘<w n="28.9">ordinaire</w></l>
						<l n="29" num="1.3"><w n="29.1">A</w> <w n="29.2">pareille</w> <w n="29.3">heure</w> <w n="29.4">il</w> <w n="29.5">est</w> <w n="29.6">depuis</w> <w n="29.7">longtemps</w> <w n="29.8">rentré</w></l>
						<l n="30" num="1.4">— <w n="30.1">Mon</w> <w n="30.2">enfant</w>, <w n="30.3">dans</w> <w n="30.4">la</w> <w n="30.5">rue</w> <w n="30.6">il</w> <w n="30.7">aura</w> <w n="30.8">rencontré</w></l>
						<l n="31" num="1.5"><w n="31.1">Le</w> <w n="31.2">major</w> <w n="31.3">allemand</w>, <w n="31.4">et</w>, <w n="31.5">comme</w> <w n="31.6">la</w> <w n="31.7">prudence</w></l>
						<l n="32" num="1.6"><w n="32.1">Dans</w> <w n="32.2">ces</w> <w n="32.3">temps</w> <w n="32.4">douloureux</w> <w n="32.5">vaut</w> <w n="32.6">mieux</w> <w n="32.7">que</w> <w n="32.8">la</w> <w n="32.9">vengeance</w></l>
						<l n="33" num="1.7"><w n="33.1">Il</w> <w n="33.2">est</w> <w n="33.3">l</w>’<w n="33.4">hôte</w>, <w n="33.5">ce</w> <w n="33.6">soir</w>, <w n="33.7">de</w> <w n="33.8">l</w>’<w n="33.9">officier</w> <w n="33.10">prussien</w>,</l>
						<l n="34" num="1.8"><w n="34.1">Sans</w> <w n="34.2">pour</w> <w n="34.3">cela</w> <w n="34.4">cesser</w> <w n="34.5">d</w>’<w n="34.6">être</w> <w n="34.7">un</w> <w n="34.8">bon</w> <w n="34.9">citoyen</w> !</l>
						<l n="35" num="1.9"><w n="35.1">S</w>’<w n="35.2">il</w> <w n="35.3">parle</w> <w n="35.4">à</w> <w n="35.5">nos</w> <w n="35.6">vainqueurs</w>, <w n="35.7">c</w>’<w n="35.8">est</w> <w n="35.9">dans</w> <w n="35.10">un</w> <w n="35.11">but</w> <w n="35.12">louable</w></l>
						<l n="36" num="1.10"><w n="36.1">De</w> <w n="36.2">trahir</w> <w n="36.3">son</w> <w n="36.4">pays</w> <w n="36.5">il</w> <w n="36.6">serait</w> <w n="36.7">incapable</w>.</l>
						<l n="37" num="1.11">— <w n="37.1">Maman</w>, <w n="37.2">je</w> <w n="37.3">sais</w> <w n="37.4">cela</w> ; <w n="37.5">mais</w> <w n="37.6">écoute</w> <w n="37.7">ceci</w> :</l>
						<l n="38" num="1.12"><w n="38.1">Je</w> <w n="38.2">ne</w> <w n="38.3">puis</w> <w n="38.4">supporter</w> <w n="38.5">de</w> <w n="38.6">voir</w> <w n="38.7">entrer</w> <w n="38.8">ici</w></l>
						<l n="39" num="1.13"><w n="39.1">Cet</w> <w n="39.2">orgueilleux</w> <w n="39.3">soldat</w>. <w n="39.4">Malgré</w> <w n="39.5">sa</w> <w n="39.6">politesse</w></l>
						<l n="40" num="1.14"><w n="40.1">Et</w> <w n="40.2">son</w> <w n="40.3">air</w> <w n="40.4">distingué</w>, <w n="40.5">sa</w> <w n="40.6">présence</w> <w n="40.7">me</w> <w n="40.8">blesse</w>.</l>
						<l n="41" num="1.15"><w n="41.1">Sous</w> <w n="41.2">l</w>’<w n="41.3">élégant</w> <w n="41.4">vernis</w> <w n="41.5">de</w> <w n="41.6">son</w> <w n="41.7">extérieur</w>,</l>
						<l n="42" num="1.16"><w n="42.1">J</w>’<w n="42.2">en</w> <w n="42.3">suis</w> <w n="42.4">presque</w> <w n="42.5">certaine</w>, <w n="42.6">il</w> <w n="42.7">cache</w> <w n="42.8">un</w> <w n="42.9">mauvais</w> <w n="42.10">cœur</w>.</l>
						<l n="43" num="1.17"><w n="43.1">Et</w> <w n="43.2">puis</w> <w n="43.3">c</w>’<w n="43.4">est</w> <w n="43.5">un</w> <w n="43.6">Prussien</w> ! — <w n="43.7">Je</w> <w n="43.8">te</w> <w n="43.9">comprends</w>, <w n="43.10">ma</w> <w n="43.11">fille</w>,</l>
						<l n="44" num="1.18"><w n="44.1">Répond</w> <w n="44.2">la</w> <w n="44.3">mère</w> ; <w n="44.4">aussi</w>, <w n="44.5">pour</w> <w n="44.6">que</w> <w n="44.7">notre</w> <w n="44.8">famille</w></l>
						<l n="45" num="1.19"><w n="45.1">Ne</w> <w n="45.2">soit</w> <w n="45.3">pas</w> <w n="45.4">exposée</w>… » <w n="45.5">Elle</w> <w n="45.6">n</w>’<w n="45.7">achève</w> <w n="45.8">pas</w>.</l>
						<l n="46" num="1.20"><w n="46.1">La</w> <w n="46.2">porte</w> <w n="46.3">du</w> <w n="46.4">salon</w> <w n="46.5">s</w>’<w n="46.6">ouvre</w> <w n="46.7">avec</w> <w n="46.8">grand</w> <w n="46.9">fracas</w> :</l>
						<l n="47" num="1.21"><w n="47.1">Armés</w> <w n="47.2">de</w> <w n="47.3">pied</w> <w n="47.4">en</w> <w n="47.5">cap</w>, <w n="47.6">deux</w> <w n="47.7">formidables</w> <w n="47.8">reîtres</w></l>
						<l n="48" num="1.22"><w n="48.1">S</w>’<w n="48.2">avancent</w> <w n="48.3">en</w> <w n="48.4">disant</w> : « <w n="48.5">A</w> <w n="48.6">vos</w> <w n="48.7">seigneurs</w> <w n="48.8">et</w> <w n="48.9">maîtres</w>,</l>
						<l n="49" num="1.23"><w n="49.1">Mesdames</w>, <w n="49.2">servez</w> <w n="49.3">vite</w> <w n="49.4">un</w> <w n="49.5">succulent</w> <w n="49.6">soupé</w>,</l>
						<l n="50" num="1.24"><w n="50.1">Et</w> <w n="50.2">quelques</w> <w n="50.3">vieux</w> <w n="50.4">flacons</w> <w n="50.5">de</w> <w n="50.6">champagne</w> <w n="50.7">frappé</w>.</l>
						<l n="51" num="1.25"><w n="51.1">Chez</w> <w n="51.2">vous</w> <w n="51.3">nous</w> <w n="51.4">arrivons</w> <w n="51.5">comme</w> <w n="51.6">de</w> <w n="51.7">lourdes</w> <w n="51.8">bombes</w>,</l>
						<l n="52" num="1.26"><w n="52.1">C</w>’<w n="52.2">est</w> <w n="52.3">vrai</w> ; <w n="52.4">mais</w>, <w n="52.5">croyez</w>-<w n="52.6">nous</w>, <w n="52.7">ô</w> <w n="52.8">gentilles</w> <w n="52.9">colombes</w>,</l>
						<l n="53" num="1.27"><w n="53.1">En</w> <w n="53.2">pénétrant</w> <w n="53.3">ici</w>, <w n="53.4">notre</w> <w n="53.5">but</w> <w n="53.6">seulement</w></l>
						<l n="54" num="1.28"><w n="54.1">Est</w> <w n="54.2">de</w> <w n="54.3">nous</w> <w n="54.4">procurer</w> <w n="54.5">un</w> <w n="54.6">peu</w> <w n="54.7">d</w>’<w n="54.8">amusement</w>.</l>
						<l n="55" num="1.29">— <w n="55.1">C</w>’<w n="55.2">est</w> <w n="55.3">une</w> <w n="55.4">lâcheté</w> ! <w n="55.5">Quoi</w> ! <w n="55.6">vous</w>, <w n="55.7">Prussiens</w>, <w n="55.8">nos</w> <w n="55.9">maîtres</w> ?</l>
						<l n="56" num="1.30"><w n="56.1">Jamais</w> ! <w n="56.2">Plutôt</w> <w n="56.3">mourir</w> ! » <w n="56.4">Et</w>, <w n="56.5">brisant</w> <w n="56.6">les</w> <w n="56.7">fenêtres</w>,</l>
						<l n="57" num="1.31"><w n="57.1">La</w> <w n="57.2">mère</w> <w n="57.3">et</w> <w n="57.4">son</w> <w n="57.5">enfant</w> <w n="57.6">poussent</w> <w n="57.7">des</w> <w n="57.8">cris</w> <w n="57.9">perçants</w>,</l>
						<l n="58" num="1.32"><w n="58.1">Appellent</w> <w n="58.2">au</w> <w n="58.3">secours</w>. <w n="58.4">Rares</w> <w n="58.5">sont</w> <w n="58.6">les</w> <w n="58.7">passants</w>.</l>
						<l n="59" num="1.33"><w n="59.1">Mais</w> <w n="59.2">les</w> <w n="59.3">voisins</w> <w n="59.4">bientôt</w> <w n="59.5">sont</w> <w n="59.6">debout</w>, <w n="59.7">et</w> <w n="59.8">la</w> <w n="59.9">porte</w></l>
						<l n="60" num="1.34"><w n="60.1">Cède</w> <w n="60.2">sous</w> <w n="60.3">leurs</w> <w n="60.4">efforts</w>. <w n="60.5">La</w> <w n="60.6">bourgeoise</w> <w n="60.7">cohorte</w>,</l>
						<l n="61" num="1.35"><w n="61.1">Comme</w> <w n="61.2">un</w> <w n="61.3">flot</w> <w n="61.4">en</w> <w n="61.5">courroux</w>, <w n="61.6">envahit</w> <w n="61.7">l</w>’<w n="61.8">escalier</w>,</l>
						<l n="62" num="1.36"><w n="62.1">Puis</w> <w n="62.2">la</w> <w n="62.3">cuisine</w>, <w n="62.4">enfin</w> <w n="62.5">le</w> <w n="62.6">salon</w> <w n="62.7">du</w> <w n="62.8">premier</w>.</l>
						<l n="63" num="1.37"><w n="63.1">Là</w> <w n="63.2">gît</w>, <w n="63.3">sur</w> <w n="63.4">le</w> <w n="63.5">parquet</w>, <w n="63.6">la</w> <w n="63.7">malheureuse</w> <w n="63.8">mère</w> :</l>
						<l n="64" num="1.38"><w n="64.1">Pâle</w> <w n="64.2">de</w> <w n="64.3">désespoir</w>, <w n="64.4">de</w> <w n="64.5">douleur</w>, <w n="64.6">de</w> <w n="64.7">colère</w>,</l>
						<l n="65" num="1.39"><w n="65.1">Sa</w> <w n="65.2">fille</w> <w n="65.3">aux</w> <w n="65.4">arrivants</w> <w n="65.5">dit</w> : « <w n="65.6">Les</w> <w n="65.7">deux</w> <w n="65.8">assassins</w></l>
						<l n="66" num="1.40"><w n="66.1">Viennent</w> <w n="66.2">de</w> <w n="66.3">s</w>’<w n="66.4">esquiver</w> : <w n="66.5">deux</w> <w n="66.6">cavaliers</w> <w n="66.7">germains</w>.</l>
						<l n="67" num="1.41"><w n="67.1">La</w> <w n="67.2">foule</w> <w n="67.3">redescend</w> ; <w n="67.4">elle</w> <w n="67.5">est</w> <w n="67.6">exaspérée</w>.</l>
						<l n="68" num="1.42"><w n="68.1">Les</w> <w n="68.2">Prussiens</w> <w n="68.3">sur</w> <w n="68.4">la</w> <w n="68.5">scène</w> <w n="68.6">alors</w> <w n="68.7">font</w> <w n="68.8">leur</w> <w n="68.9">entrée</w>.</l>
						<l n="69" num="1.43"><w n="69.1">Sans</w> <w n="69.2">gêne</w>, <w n="69.3">de</w> <w n="69.4">pouvoir</w> <w n="69.5">sabrer</w> <w n="69.6">de</w> <w n="69.7">tous</w> <w n="69.8">côtés</w></l>
						<l n="70" num="1.44"><w n="70.1">Les</w> <w n="70.2">hussards</w> <w n="70.3">de</w> <w n="70.4">la</w> <w n="70.5">Mort</w> <w n="70.6">sont</w> <w n="70.7">ravis</w>, <w n="70.8">enchantés</w>.</l>
						<l n="71" num="1.45"><w n="71.1">Opprobre</w> ! <w n="71.2">lâcheté</w> ! <w n="71.3">ineffaçable</w> <w n="71.4">honte</w> !</l>
						<l n="72" num="1.46"><w n="72.1">Après</w> <w n="72.2">une</w> <w n="72.3">heure</w>, <w n="72.4">hélas</w> ! <w n="72.5">sur</w> <w n="72.6">le</w> <w n="72.7">pavé</w> <w n="72.8">son</w> <w n="72.9">compte</w> —</l>
						<l n="73" num="1.47"><w n="73.1">L</w>’<w n="73.2">histoire</w> <w n="73.3">le</w> <w n="73.4">dira</w> — <w n="73.5">vingt</w> <w n="73.6">citoyens</w> <w n="73.7">blessés</w>,</l>
						<l n="74" num="1.48"><w n="74.1">Autant</w> <w n="74.2">de</w> <w n="74.3">trépassés</w>.</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="2">
						<l n="75" num="2.1"><w n="75.1">Les</w> <w n="75.2">hussards</w> <w n="75.3">de</w> <w n="75.4">la</w> <w n="75.5">Mort</w>, <w n="75.6">montés</w> <w n="75.7">sur</w> <w n="75.8">leurs</w> <w n="75.9">cavales</w>,</l>
						<l n="76" num="2.2"><w n="76.1">Paradaient</w> <w n="76.2">certain</w> <w n="76.3">jour</w>, <w n="76.4">lorsque</w> <w n="76.5">soudain</w> <w n="76.6">deux</w> <w n="76.7">balles</w></l>
						<l n="77" num="2.3"><w n="77.1">Passent</w> <w n="77.2">entre</w> <w n="77.3">leurs</w> <w n="77.4">rangs</w>. « <w n="77.5">C</w>’<w n="77.6">est</w> <w n="77.7">mon</w> <w n="77.8">tour</w> <w n="77.9">aujourd</w>’<w n="77.10">hui</w>,</l>
						<l n="78" num="2.4"><w n="78.1">Dit</w> <w n="78.2">un</w> <w n="78.3">brave</w> <w n="78.4">Français</w>, <w n="78.5">Prussiens</w>, <w n="78.6">je</w> <w n="78.7">suis</w> <w n="78.8">celui</w></l>
						<l n="79" num="2.5"><w n="79.1">Dont</w> <w n="79.2">vous</w> <w n="79.3">avez</w> <w n="79.4">un</w> <w n="79.5">soir</w>, <w n="79.6">par</w> <w n="79.7">ordre</w> <w n="79.8">d</w>’<w n="79.9">un</w> <w n="79.10">infâme</w>,</l>
						<l n="80" num="2.6"><w n="80.1">Tenté</w> <w n="80.2">d</w>’<w n="80.3">assassiner</w> <w n="80.4">et</w> <w n="80.5">la</w> <w n="80.6">fille</w> <w n="80.7">et</w> <w n="80.8">la</w> <w n="80.9">femme</w>.»</l>
						<l n="81" num="2.7"><w n="81.1">Quelques</w> <w n="81.2">instants</w> <w n="81.3">après</w>, <w n="81.4">sabrant</w> <w n="81.5">à</w> <w n="81.6">qui</w> <w n="81.7">mieux</w> <w n="81.8">mieux</w>,</l>
						<l n="82" num="2.8"><w n="82.1">Les</w> <w n="82.2">cavaliers</w> <w n="82.3">teutons</w> <w n="82.4">hachaient</w> <w n="82.5">le</w> <w n="82.6">malheureux</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>