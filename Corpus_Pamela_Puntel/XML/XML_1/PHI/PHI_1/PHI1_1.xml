<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">VIVE GAMBETTA !</title>
				<title type="medium">Édition électronique</title>
				<author key="PHI">
					<name>
						<forename>Gustave</forename>
						<surname>PHILIPPON</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">PHI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>VIVE GAMBETTA !</title>
						<author>GUSTAVE PHILIPPON</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LACROIX VERBOECKHOVEN ET CIE</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="PHI1">
				<head type="main">DÉDICACE ET ENVOI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Citoyen</w> <w n="1.2">bien</w>-<w n="1.3">aimé</w> <w n="1.4">que</w>, <w n="1.5">dans</w> <w n="1.6">les</w> <w n="1.7">temps</w> <w n="1.8">antiques</w>,</l>
					<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">eût</w>, <w n="2.3">certes</w>, <w n="2.4">chanté</w> <w n="2.5">sous</w> <w n="2.6">de</w> <w n="2.7">sacrés</w> <w n="2.8">portiques</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Âme</w> <w n="3.2">de</w> <w n="3.3">la</w> <w n="3.4">patrie</w>, <w n="3.5">intrépide</w> <w n="3.6">lutteur</w></l>
					<l n="4" num="1.4"><w n="4.1">Dont</w> <w n="4.2">la</w> <w n="4.3">mâle</w> <w n="4.4">parole</w> <w n="4.5">est</w> <w n="4.6">un</w> <w n="4.7">accent</w> <w n="4.8">du</w> <w n="4.9">cœur</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Notre</w> <w n="5.2">père</w> <w n="5.3">commun</w>, <w n="5.4">fils</w> <w n="5.5">aîné</w> <w n="5.6">de</w> <w n="5.7">la</w> <w n="5.8">France</w>,</l>
					<l n="6" num="1.6"><w n="6.1">De</w> <w n="6.2">ta</w> <w n="6.3">mère</w> <w n="6.4">et</w> <w n="6.5">de</w> <w n="6.6">nous</w> <w n="6.7">légitime</w> <w n="6.8">espérance</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Fais</w> <w n="7.2">à</w> <w n="7.3">ma</w> <w n="7.4">Muse</w> <w n="7.5">obscure</w> <w n="7.6">un</w> <w n="7.7">indulgent</w> <w n="7.8">accueil</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Te</w> <w n="8.2">chanter</w> <w n="8.3">est</w> <w n="8.4">avoir</w> <w n="8.5">peut</w>-<w n="8.6">être</w> <w n="8.7">trop</w> <w n="8.8">d</w>'<w n="8.9">orgueil</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Quelque</w> <w n="9.2">indigne</w> <w n="9.3">qu</w>'<w n="9.4">il</w> <w n="9.5">soit</w>, <w n="9.6">accepte</w> <w n="9.7">mon</w> <w n="9.8">hommage</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">même</w>, <w n="10.3">d</w>'<w n="10.4">un</w> <w n="10.5">serment</w>, <w n="10.6">qu</w>'<w n="10.7">il</w> <w n="10.8">te</w> <w n="10.9">serve</w> <w n="10.10">de</w> <w n="10.11">gage</w> !</l>
					<l n="11" num="1.11"><w n="11.1">Sache</w> <w n="11.2">que</w>, <w n="11.3">si</w> <w n="11.4">tu</w> <w n="11.5">veux</w>, <w n="11.6">sur</w> <w n="11.7">un</w> <w n="11.8">signe</w>, <w n="11.9">et</w>, <w n="11.10">pour</w> <w n="11.11">toi</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Tu</w> <w n="12.2">peux</w> <w n="12.3">me</w> <w n="12.4">demander</w> <w n="12.5">le</w> <w n="12.6">sang</w> <w n="12.7">qui</w> <w n="12.8">coule</w> <w n="12.9">en</w> <w n="12.10">moi</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Les</w> <w n="13.2">flammes</w> <w n="13.3">de</w> <w n="13.4">ton</w> <w n="13.5">sein</w>, <w n="13.6">vivifiantes</w> <w n="13.7">flammes</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Dans</w> <w n="14.2">tous</w> <w n="14.3">les</w> <w n="14.4">jeunes</w> <w n="14.5">seins</w> <w n="14.6">ont</w> <w n="14.7">réchauffé</w> <w n="14.8">les</w> <w n="14.9">âmes</w>.</l>
					<l n="15" num="1.15"><w n="15.1">Je</w> <w n="15.2">suis</w> <w n="15.3">jeune</w> : <w n="15.4">avec</w> <w n="15.5">moi</w>, <w n="15.6">puisse</w> <w n="15.7">tout</w> <w n="15.8">le</w> <w n="15.9">pays</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Saisi</w> <w n="16.2">de</w> <w n="16.3">sainte</w> <w n="16.4">ardeur</w>, <w n="16.5">faire</w> <w n="16.6">entendre</w> <w n="16.7">les</w> <w n="16.8">cris</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Cris</w> <w n="17.2">jaillissant</w> <w n="17.3">des</w> <w n="17.4">cœurs</w>, <w n="17.5">cris</w> <w n="17.6">d</w>'<w n="17.7">un</w> <w n="17.8">amour</w> <w n="17.9">unique</w>,</l>
					<l n="18" num="1.18"><w n="18.1">De</w> : <w n="18.2">Vive</w> <w n="18.3">Gambetta</w> ! <w n="18.4">vive</w> <w n="18.5">la</w> <w n="18.6">République</w> !</l>
					<l n="19" num="1.19"><w n="19.1">Puissent</w>, <w n="19.2">en</w> <w n="19.3">ce</w> <w n="19.4">beau</w> <w n="19.5">jour</w>, <w n="19.6">nos</w> <w n="19.7">frères</w> <w n="19.8">Alsaciens</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Voyant</w> <w n="20.2">pleurer</w> <w n="20.3">près</w> <w n="20.4">d</w>'<w n="20.5">eux</w> <w n="20.6">les</w> <w n="20.7">orphelins</w> <w n="20.8">prussiens</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Entonner</w> <w n="21.2">avec</w> <w n="21.3">nous</w> <w n="21.4">les</w> <w n="21.5">Vieux</w> <w n="21.6">refrains</w> <w n="21.7">de</w> <w n="21.8">France</w> !</l>
				</lg>
				<lg n="2">
					<l n="22" num="2.1"><w n="22.1">Fraternité</w>, <w n="22.2">Salut</w>, <w n="22.3">Espoir</w> <w n="22.4">et</w> <w n="22.5">Patience</w> !…</l>
				</lg>
				<closer>
					<signed>GUSTAVE PHILIPPON.</signed>
				</closer>
			</div></body></text></TEI>