<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE BAISER DE L'ALSACIENNE</title>
				<title type="medium">Édition électronique</title>
				<author key="VLM">
					<name>
						<forename>Germain</forename>
						<surname>GIRARD</surname>
						<addName type="pen_name">VILLEMER</addName>
					</name>
					<date from="1842" to="1892">1842-1892</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>138 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VLM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE BAISER DE L'ALSACIENNE</title>
						<author>VILLEMER</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Eveillard et Jacquot, Editeurs</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VLM1">
				<head type="main">LE BAISER DE L'ALSACIENNE</head>
				<head type="form">Dit par M. Delaunay de la Comédie Française</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">soleil</w> <w n="1.3">de</w> <w n="1.4">Juillet</w> <w n="1.5">illuminait</w> <w n="1.6">l</w>' <w n="1.7">Alsace</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">appels</w> <w n="2.3">du</w> <w n="2.4">clairon</w> <w n="2.5">éclataient</w> <w n="2.6">dans</w> <w n="2.7">l</w>'<w n="2.8">espace</w>…</l>
					<l n="3" num="1.3"><w n="3.1">Tout</w> <w n="3.2">le</w> <w n="3.3">jour</w>, <w n="3.4">défilaient</w>, <w n="3.5">en</w> <w n="3.6">chantant</w>, <w n="3.7">pleins</w> <w n="3.8">d</w> '<w n="3.9">entrain</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">régiments</w> <w n="4.3">français</w> <w n="4.4">s</w>'<w n="4.5">en</w> <w n="4.6">allant</w> <w n="4.7">vers</w> <w n="4.8">le</w> <w n="4.9">Rhin</w>…</l>
					<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">la</w> <w n="5.3">place</w> <w n="5.4">publique</w>, <w n="5.5">auprès</w> <w n="5.6">de</w> <w n="5.7">la</w> <w n="5.8">fontaine</w>,</l>
					<l n="6" num="1.6"><w n="6.1">De</w> <w n="6.2">son</w> <w n="6.3">regard</w> <w n="6.4">rêveur</w> <w n="6.5">une</w> <w n="6.6">jeune</w> <w n="6.7">Alsacienne</w></l>
					<l n="7" num="1.7"><w n="7.1">Depuis</w> <w n="7.2">un</w> <w n="7.3">long</w> <w n="7.4">moment</w> <w n="7.5">suivait</w> <w n="7.6">avec</w> <w n="7.7">amour</w></l>
					<l n="8" num="1.8"><w n="8.1">Les</w> <w n="8.2">zouaves</w> <w n="8.3">préparant</w> <w n="8.4">leur</w> <w n="8.5">campement</w> <w n="8.6">d</w>'<w n="8.7">un</w> <w n="8.8">jour</w> :</l>
					<l n="9" num="1.9"><w n="9.1">Quand</w> <w n="9.2">l</w>'<w n="9.3">un</w> <w n="9.4">d</w>'<w n="9.5">eux</w>, <w n="9.6">un</w> <w n="9.7">sergent</w> <w n="9.8">à</w> <w n="9.9">la</w> <w n="9.10">mine</w> <w n="9.11">éveillée</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Se</w> <w n="10.2">dirigea</w> <w n="10.3">vers</w> <w n="10.4">la</w> <w n="10.5">fontaine</w> <w n="10.6">ensoleillée</w> :</l>
					<l n="11" num="1.11">— <w n="11.1">Dites</w>, <w n="11.2">la</w> <w n="11.3">belle</w> <w n="11.4">enfant</w>, <w n="11.5">fit</w>-<w n="11.6">il</w>, <w n="11.7">portant</w> <w n="11.8">la</w> <w n="11.9">main</w></l>
					<l n="12" num="1.12"><w n="12.1">A</w> <w n="12.2">son</w> <w n="12.3">turban</w> <w n="12.4">tout</w> <w n="12.5">gris</w> <w n="12.6">des</w> <w n="12.7">poudres</w> <w n="12.8">du</w> <w n="12.9">chemin</w>,</l>
					<l n="13" num="1.13"><w n="13.1">D</w>'<w n="13.2">un</w> <w n="13.3">peu</w> <w n="13.4">d</w> '<w n="13.5">eau</w>, <w n="13.6">s</w>'<w n="13.7">il</w> <w n="13.8">vous</w> <w n="13.9">plait</w>, <w n="13.10">me</w> <w n="13.11">ferez</w>-<w n="13.12">vous</w> <w n="13.13">la</w> <w n="13.14">grâce</w> ?</l>
				</lg>
				<lg n="2">
					<l n="14" num="2.1"><w n="14.1">A</w> <w n="14.2">cette</w> <w n="14.3">question</w> <w n="14.4">la</w> <w n="14.5">blonde</w> <w n="14.6">enfant</w> <w n="14.7">d</w>'<w n="14.8">Alsace</w></l>
					<l part="I" n="15" num="2.2"><w n="15.1">Sourit</w> <w n="15.2">et</w> <w n="15.3">lui</w> <w n="15.4">tendit</w> <w n="15.5">son</w> <w n="15.6">vase</w> : </l>
				</lg>
				<lg n="3">
					<l part="F" n="15">« <w n="15.7">Assurément</w>,</l>
					<l n="16" num="3.1"><w n="16.1">Dit</w>-<w n="16.2">elle</w>, <w n="16.3">j</w>'<w n="16.4">en</w> <w n="16.5">aurais</w> <w n="16.6">pour</w> <w n="16.7">tout</w> <w n="16.8">le</w> <w n="16.9">régiment</w>.</l>
					<l n="17" num="3.2"><w n="17.1">Buvez</w> <w n="17.2">sans</w> <w n="17.3">crainte</w> <w n="17.4">l</w>'<w n="17.5">eau</w> <w n="17.6">de</w> <w n="17.7">la</w> <w n="17.8">source</w> <w n="17.9">française</w>,</l>
					<l n="18" num="3.3"><w n="18.1">Buvez</w>, <w n="18.2">petit</w> <w n="18.3">sergent</w>, <w n="18.4">buvez</w> <w n="18.5">tout</w> <w n="18.6">à</w> <w n="18.7">votre</w> <w n="18.8">aise</w> ! »</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Mais</w> <w n="19.2">quoiqu</w>'<w n="19.3">elle</w> <w n="19.4">insistât</w> <w n="19.5">le</w> <w n="19.6">sergent</w> <w n="19.7">ne</w> <w n="19.8">but</w> <w n="19.9">pas</w>,</l>
					<l n="20" num="4.2"><w n="20.1">Et</w> <w n="20.2">les</w> <w n="20.3">yeux</w> <w n="20.4">dans</w> <w n="20.5">ses</w> <w n="20.6">yeux</w>, <w n="20.7">il</w> <w n="20.8">ajouta</w> <w n="20.9">plus</w> <w n="20.10">bas</w> :</l>
					<l n="21" num="4.3">« <w n="21.1">Non</w>, <w n="21.2">vous</w> <w n="21.3">d</w>'<w n="21.4">abord</w>, <w n="21.5">buvez</w>… <w n="21.6">Ma</w> <w n="21.7">soif</w> <w n="21.8">n</w>'<w n="21.9">est</w> <w n="21.10">pas</w> <w n="21.11">pressée</w>…</l>
					<l n="22" num="4.4"><w n="22.1">Buvez</w> ! <w n="22.2">Je</w> <w n="22.3">voudrais</w> <w n="22.4">tant</w> <w n="22.5">savoir</w> <w n="22.6">votre</w> <w n="22.7">pensée</w> ! »</l>
				</lg>
				<lg n="5">
					<l n="23" num="5.1"><w n="23.1">Riant</w> <w n="23.2">de</w> <w n="23.3">son</w> <w n="23.4">caprice</w>, <w n="23.5">elle</w> <w n="23.6">but</w> <w n="23.7">lentement</w></l>
					<l n="24" num="5.2"><w n="24.1">Et</w> <w n="24.2">de</w> <w n="24.3">nouveau</w> <w n="24.4">lui</w> <w n="24.5">dit</w> : — <w n="24.6">Buvez</w> <w n="24.7">donc</w> <w n="24.8">maintenant</w>…</l>
				</lg>
				<lg n="6">
					<l n="25" num="6.1"><w n="25.1">Il</w> <w n="25.2">étancha</w> <w n="25.3">sa</w> <w n="25.4">soif</w>, <w n="25.5">il</w> <w n="25.6">but</w> <w n="25.7">avec</w> <w n="25.8">ivresse</w></l>
					<l n="26" num="6.2"><w n="26.1">Et</w> <w n="26.2">puis</w> <w n="26.3">la</w> <w n="26.4">regardant</w> <w n="26.5">soudain</w> <w n="26.6">avec</w> <w n="26.7">tendresse</w> :</l>
					<l n="27" num="6.3">« <w n="27.1">Je</w> <w n="27.2">ne</w> <w n="27.3">sais</w> <w n="27.4">rien</w>, <w n="27.5">dit</w>-<w n="27.6">il</w>, <w n="27.7">de</w> <w n="27.8">ce</w> <w n="27.9">que</w> <w n="27.10">vous</w> <w n="27.11">pensez</w> ;</l>
					<l n="28" num="6.4"><w n="28.1">Mais</w> <w n="28.2">si</w> <w n="28.3">vous</w> <w n="28.4">permettez</w>, <w n="28.5">voilà</w> <w n="28.6">ce</w> <w n="28.7">que</w> <w n="28.8">je</w> <w n="28.9">sais</w>.</l>
					<l n="29" num="6.5"><w n="29.1">Je</w> <w n="29.2">voudrais</w> <w n="29.3">bien</w>, <w n="29.4">avant</w> <w n="29.5">d</w>' <w n="29.6">affronter</w> <w n="29.7">la</w> <w n="29.8">bataille</w>,</l>
					<l n="30" num="6.6"><w n="30.1">Pouvoir</w> <w n="30.2">dans</w> <w n="30.3">mes</w> <w n="30.4">dix</w> <w n="30.5">doigts</w> <w n="30.6">enlacer</w> <w n="30.7">votre</w> <w n="30.8">taille</w>,</l>
					<l n="31" num="6.7"><w n="31.1">Et</w> <w n="31.2">vous</w> <w n="31.3">prendre</w> <w n="31.4">un</w> <w n="31.5">baiser</w>, <w n="31.6">un</w> <w n="31.7">seul</w>, <w n="31.8">là</w>, <w n="31.9">mais</w> <w n="31.10">bien</w> <w n="31.11">doux</w> !</l>
					<l n="32" num="6.8"><w n="32.1">Dites</w> , <w n="32.2">la</w> <w n="32.3">belle</w> <w n="32.4">enfant</w>, <w n="32.5">dites</w>, <w n="32.6">permettez</w>-<w n="32.7">vous</w> ! »</l>
				</lg>
				<lg n="7">
					<l n="33" num="7.1"><w n="33.1">Elle</w> <w n="33.2">se</w> <w n="33.3">défendit</w> : <w n="33.4">C</w> '<w n="33.5">est</w> <w n="33.6">un</w> <w n="33.7">peu</w> <w n="33.8">trop</w> <w n="33.9">d</w>'<w n="33.10">audace</w>,</l>
					<l n="34" num="7.2"><w n="34.1">Dit</w>-<w n="34.2">elle</w> <w n="34.3">en</w> <w n="34.4">souriant</w>, <w n="34.5">pour</w> <w n="34.6">un</w> <w n="34.7">sergent</w> <w n="34.8">qui</w> <w n="34.9">passe</w> !</l>
					<l n="35" num="7.3"><w n="35.1">Nos</w> <w n="35.2">garçons</w> <w n="35.3">ne</w> <w n="35.4">vont</w> <w n="35.5">pas</w> <w n="35.6">si</w> <w n="35.7">vite</w> <w n="35.8">par</w> <w n="35.9">ici</w>…</l>
					<l n="36" num="7.4"><w n="36.1">Bah</w> ! <w n="36.2">c</w>'<w n="36.3">est</w> <w n="36.4">qu</w>'<w n="36.5">on</w> <w n="36.6">est</w> <w n="36.7">pressée</w>, <w n="36.8">dit</w>-<w n="36.9">il</w>, <w n="36.10">riant</w> <w n="36.11">aussi</w>…</l>
					<l part="I" ana="unanalyzable" n="37" num="7.5">Et puis… , voilà je suis parisien !</l>
				</lg>
				<lg n="8">
					<l part="F" ana="unanalyzable" n="37">L' Alsacienne</l>
					<l ana="unanalyzable" n="38" num="8.1">......................................................................................</l>
				</lg>
				<lg n="9">
					<l n="39" num="9.1"><w n="39.1">Mais</w> <w n="39.2">vous</w> <w n="39.3">êtes</w> <w n="39.4">français</w>, <w n="39.5">dit</w>-<w n="39.6">elle</w>, <w n="39.7">c</w>'<w n="39.8">est</w> <w n="39.9">assez</w> !</l>
					<l n="40" num="9.2"><w n="40.1">Je</w> <w n="40.2">sens</w> <w n="40.3">que</w> <w n="40.4">tout</w> <w n="40.5">en</w> <w n="40.6">moi</w> <w n="40.7">frémit</w> <w n="40.8">quand</w> <w n="40.9">vous</w> <w n="40.10">passez</w>…</l>
					<l n="41" num="9.3"><w n="41.1">Faites</w> <w n="41.2">votre</w> <w n="41.3">devoir</w>, <w n="41.4">marchez</w> <w n="41.5">à</w> <w n="41.6">la</w> <w n="41.7">victoire</w>…</l>
					<l n="42" num="9.4"><w n="42.1">Et</w> <w n="42.2">moi</w>, <w n="42.3">je</w> <w n="42.4">vous</w> <w n="42.5">promets</w>, <w n="42.6">et</w> <w n="42.7">vous</w> <w n="42.8">pouvez</w> <w n="42.9">m</w>'<w n="42.10">en</w> <w n="42.11">croire</w>,</l>
					<l n="43" num="9.5"><w n="43.1">Si</w> <w n="43.2">loin</w> <w n="43.3">que</w> <w n="43.4">le</w> <w n="43.5">combat</w> <w n="43.6">puisse</w> <w n="43.7">entrainer</w> <w n="43.8">vos</w> <w n="43.9">pas</w>,</l>
					<l n="44" num="9.6"><w n="44.1">Qu</w> '<w n="44.2">au</w> <w n="44.3">retour</w> <w n="44.4">mon</w> <w n="44.5">baiser</w> <w n="44.6">ne</w> <w n="44.7">vous</w> <w n="44.8">manquera</w> <w n="44.9">pas</w>. »</l>
				</lg>
				<lg n="10">
					<l n="45" num="10.1"><w n="45.1">Le</w> <w n="45.2">sergent</w> <w n="45.3">s</w>'<w n="45.4">éloigna</w> <w n="45.5">rêveur</w>. <w n="45.6">Cette</w> <w n="45.7">fillette</w></l>
					<l n="46" num="10.2"><w n="46.1">A</w> <w n="46.2">la</w> <w n="46.3">parole</w> <w n="46.4">grave</w> <w n="46.5">avait</w> <w n="46.6">troublé</w> <w n="46.7">sa</w> <w n="46.8">tête</w>…</l>
					<l n="47" num="10.3"><w n="47.1">Il</w> <w n="47.2">n</w> '<w n="47.3">avait</w> <w n="47.4">qu</w>'<w n="47.5">un</w> <w n="47.6">désir</w>, <w n="47.7">lui</w> <w n="47.8">parler</w>, <w n="47.9">la</w> <w n="47.10">revoir</w>…</l>
					<l n="48" num="10.4"><w n="48.1">Sous</w> <w n="48.2">sa</w> <w n="48.3">fenêtre</w>, <w n="48.4">il</w> <w n="48.5">vint</w> <w n="48.6">encor</w> <w n="48.7">rêver</w> <w n="48.8">le</w> <w n="48.9">soir</w> ;</l>
					<l n="49" num="10.5"><w n="49.1">Quand</w> <w n="49.2">le</w> <w n="49.3">clairon</w> <w n="49.4">sonna</w> <w n="49.5">le</w> <w n="49.6">départ</w> <w n="49.7">à</w> <w n="49.8">l</w>'<w n="49.9">aurore</w>,</l>
					<l n="50" num="10.6"><w n="50.1">En</w> <w n="50.2">repliant</w> <w n="50.3">sa</w> <w n="50.4">tente</w>, <w n="50.5">il</w> <w n="50.6">rêvait</w> <w n="50.7">d</w>'<w n="50.8">elle</w> <w n="50.9">encore</w>…</l>
					<l n="51" num="10.7"><w n="51.1">Mais</w> <w n="51.2">comme</w> <w n="51.3">il</w> <w n="51.4">s</w>'<w n="51.5">éloignait</w>, <w n="51.6">suivant</w> <w n="51.7">le</w> <w n="51.8">régiment</w>,</l>
					<l n="52" num="10.8"><w n="52.1">Soudain</w> <w n="52.2">elle</w> <w n="52.3">apparut</w>, <w n="52.4">souriant</w> <w n="52.5">doucement</w></l>
					<l n="53" num="10.9"><w n="53.1">Et</w> <w n="53.2">de</w> <w n="53.3">vergiss</w>-<w n="53.4">mein</w>-<w n="53.5">nicht</w> <w n="53.6">lui</w> <w n="53.7">jetant</w> <w n="53.8">une</w> <w n="53.9">gerbe</w>,</l>
					<l n="54" num="10.10"><w n="54.1">Elle</w> <w n="54.2">semblait</w> <w n="54.3">lui</w> <w n="54.4">dire</w> <w n="54.5">en</w> <w n="54.6">un</w> <w n="54.7">geste</w> <w n="54.8">superbe</w> :</l>
					<l n="55" num="10.11">« <w n="55.1">Va</w> ! <w n="55.2">sergent</w> ! <w n="55.3">souviens</w>-<w n="55.4">toi</w> ! <w n="55.5">moi</w> <w n="55.6">je</w> <w n="55.7">me</w> <w n="55.8">souviendrai</w> !</l>
					<l n="56" num="10.12"><w n="56.1">Mérite</w> <w n="56.2">mon</w> <w n="56.3">baiser</w>, <w n="56.4">je</w> <w n="56.5">te</w> <w n="56.6">l</w>'<w n="56.7">apporterai</w> ! »</l>
				</lg>
				<lg n="11">
					<l n="57" num="11.1"><w n="57.1">Wissembourg</w> ! <w n="57.2">Wissembourg</w> ! <w n="57.3">L</w>'<w n="57.4">Allemagne</w> <w n="57.5">insolent</w></l>
					<l n="58" num="11.2"><w n="58.1">Pourra</w> <w n="58.2">nous</w> <w n="58.3">reprocher</w> <w n="58.4">cette</w> <w n="58.5">tache</w> <w n="58.6">sanglante</w> !</l>
					<l n="59" num="11.3"><w n="59.1">Lorsqu</w>'<w n="59.2">à</w> <w n="59.3">vingt</w> <w n="59.4">allemands</w> <w n="59.5">tout</w> <w n="59.6">le</w> <w n="59.7">jour</w> <w n="59.8">un</w> <w n="59.9">français</w></l>
					<l n="60" num="11.4"><w n="60.1">Tient</w> <w n="60.2">tête</w>, <w n="60.3">sans</w> <w n="60.4">faiblir</w>, <w n="60.5">c</w>'<w n="60.6">est</w> <w n="60.7">encor</w> <w n="60.8">le</w> <w n="60.9">succès</w>.</l>
					<l n="61" num="11.5"><w n="61.1">Furent</w>-<w n="61.2">ils</w> <w n="61.3">des</w> <w n="61.4">vainqueurs</w> <w n="61.5">ces</w> <w n="61.6">soldats</w> <w n="61.7">qui</w> <w n="61.8">dans</w> <w n="61.9">l</w>'<w n="61.10">ombre</w>,</l>
					<l n="62" num="11.6"><w n="62.1">Foudroyaient</w> <w n="62.2">à</w> <w n="62.3">coup</w> <w n="62.4">sûr</w>, <w n="62.5">confiants</w> <w n="62.6">dans</w> <w n="62.7">leur</w> <w n="62.8">nombre</w> !</l>
					<l n="63" num="11.7"><w n="63.1">Non</w> ! <w n="63.2">le</w> <w n="63.3">vainqueur</w> <w n="63.4">du</w> <w n="63.5">jour</w>, <w n="63.6">ce</w> <w n="63.7">fut</w> <w n="63.8">ce</w> <w n="63.9">beau</w> <w n="63.10">martyr</w>,</l>
					<l n="64" num="11.8"><w n="64.1">Douai</w>, <w n="64.2">qui</w> <w n="64.3">ne</w> <w n="64.4">pouvant</w> <w n="64.5">plus</w> <w n="64.6">vaincre</w>, <w n="64.7">sut</w> <w n="64.8">mourir</w>…</l>
					<l n="65" num="11.9"><w n="65.1">Ce</w> <w n="65.2">furent</w> <w n="65.3">ces</w> <w n="65.4">héros</w>, <w n="65.5">luttant</w> <w n="65.6">cent</w> <w n="65.7">contre</w> <w n="65.8">mille</w>,</l>
					<l n="66" num="11.10"><w n="66.1">Qui</w> <w n="66.2">sans</w> <w n="66.3">espoir</w>, <w n="66.4">sachant</w> <w n="66.5">tout</w> <w n="66.6">effort</w> <w n="66.7">inutile</w>,</l>
					<l n="67" num="11.11"><w n="67.1">S</w>' <w n="67.2">en</w> <w n="67.3">allèrent</w>, <w n="67.4">le</w> <w n="67.5">rire</w> <w n="67.6">aux</w> <w n="67.7">dents</w>, <w n="67.8">sous</w> <w n="67.9">le</w> <w n="67.10">drapeau</w>,</l>
					<l n="68" num="11.12"><w n="68.1">Dans</w> <w n="68.2">les</w> <w n="68.3">rangs</w> <w n="68.4">allemands</w> <w n="68.5">se</w> <w n="68.6">creuser</w> <w n="68.7">un</w> <w n="68.8">tombeau</w> !</l>
					<l n="69" num="11.13"><w n="69.1">Wissembourg</w> ! <w n="69.2">Wissembourg</w> ! <w n="69.3">Laisse</w> <w n="69.4">donc</w> <w n="69.5">l</w>'<w n="69.6">Allemagne</w></l>
					<l n="70" num="11.14"><w n="70.1">Célébrer</w> <w n="70.2">ce</w> <w n="70.3">début</w> <w n="70.4">de</w> <w n="70.5">l</w>'<w n="70.6">horrible</w> <w n="70.7">campagne</w> :</l>
					<l n="71" num="11.15"><w n="71.1">Les</w> <w n="71.2">tombes</w> <w n="71.3">des</w> <w n="71.4">héros</w> <w n="71.5">fleuriront</w>, <w n="71.6">quelque</w>-<w n="71.7">jour</w>,</l>
					<l n="72" num="11.16"><w n="72.1">Et</w> <w n="72.2">les</w> <w n="72.3">vaincus</w> <w n="72.4">auront</w> <w n="72.5">la</w> <w n="72.6">victoire</w> <w n="72.7">à</w> <w n="72.8">leur</w> <w n="72.9">tour</w> !</l>
				</lg>
				<lg n="12">
					<l n="73" num="12.1"><w n="73.1">C</w>'<w n="73.2">est</w> <w n="73.3">fini</w>. <w n="73.4">La</w> <w n="73.5">journée</w>, <w n="73.6">hélas</w> ! <w n="73.7">est</w> <w n="73.8">consommée</w>,</l>
					<l n="74" num="12.2"><w n="74.1">Les</w> <w n="74.2">cadavres</w> <w n="74.3">sanglants</w> <w n="74.4">de</w> <w n="74.5">tout</w> <w n="74.6">un</w> <w n="74.7">corps</w> <w n="74.8">d</w>'<w n="74.9">armée</w></l>
					<l n="75" num="12.3"><w n="75.1">Dans</w> <w n="75.2">des</w> <w n="75.3">ruisseaux</w> <w n="75.4">de</w> <w n="75.5">sang</w> <w n="75.6">sommeillent</w> <w n="75.7">pour</w> <w n="75.8">jamais</w></l>
					<l n="76" num="12.4"><w n="76.1">Et</w> <w n="76.2">Wissembourg</w> <w n="76.3">n</w>'<w n="76.4">est</w> <w n="76.5">plus</w> <w n="76.6">qu</w>'<w n="76.7">un</w> <w n="76.8">cercueil</w> <w n="76.9">désormais</w>.</l>
					<l n="77" num="12.5"><w n="77.1">La</w> <w n="77.2">ferme</w> <w n="77.3">où</w> <w n="77.4">tout</w> <w n="77.5">riait</w> <w n="77.6">la</w> <w n="77.7">veille</w> <w n="77.8">est</w> <w n="77.9">morne</w> <w n="77.10">et</w> <w n="77.11">sombre</w>,</l>
					<l n="78" num="12.6"><w n="78.1">Et</w> <w n="78.2">ses</w> <w n="78.3">vieux</w> <w n="78.4">murs</w> <w n="78.5">troués</w> <w n="78.6">par</w> <w n="78.7">des</w> <w n="78.8">boulets</w> <w n="78.9">sans</w> <w n="78.10">nombre</w>,</l>
					<l n="79" num="12.7"><w n="79.1">Ouverts</w> <w n="79.2">comme</w> <w n="79.3">une</w> <w n="79.4">porte</w> <w n="79.5">immense</w> <w n="79.6">jusqu</w>'<w n="79.7">au</w> <w n="79.8">toit</w>,</l>
					<l n="80" num="12.8"><w n="80.1">Semblent</w> <w n="80.2">dire</w> <w n="80.3">la</w> <w n="80.4">mort</w> : <w n="80.5">Entre</w> <w n="80.6">comme</w> <w n="80.7">chez</w> <w n="80.8">toi</w> !</l>
					<l n="81" num="12.9"><w n="81.1">Le</w> <w n="81.2">vieux</w> <w n="81.3">moulin</w> <w n="81.4">s</w>'<w n="81.5">est</w> <w n="81.6">tû</w>. <w n="81.7">Son</w> <w n="81.8">aile</w> <w n="81.9">pend</w> <w n="81.10">meurtrie</w>,</l>
					<l n="82" num="12.10"><w n="82.1">Et</w> <w n="82.2">du</w> <w n="82.3">ruisseau</w> <w n="82.4">qui</w> <w n="82.5">court</w> <w n="82.6">à</w> <w n="82.7">travers</w> <w n="82.8">la</w> <w n="82.9">prairie</w>,</l>
					<l n="83" num="12.11"><w n="83.1">Le</w> <w n="83.2">sang</w> <w n="83.3">a</w> <w n="83.4">coloré</w> <w n="83.5">l</w>'<w n="83.6">eau</w> <w n="83.7">si</w> <w n="83.8">pure</w> <w n="83.9">jadis</w>.</l>
					<l n="84" num="12.12"><w n="84.1">Sur</w> <w n="84.2">sa</w> <w n="84.3">rive</w>, <w n="84.4">parmi</w> <w n="84.5">les</w> <w n="84.6">doux</w> <w n="84.7">myosotis</w></l>
					<l n="85" num="12.13"><w n="85.1">Sous</w> <w n="85.2">le</w> <w n="85.3">grand</w> <w n="85.4">saule</w> <w n="85.5">où</w> <w n="85.6">le</w> <w n="85.7">soleil</w> <w n="85.8">couchant</w> <w n="85.9">se</w> <w n="85.10">joue</w>,</l>
					<l n="86" num="12.14"><w n="86.1">La</w> <w n="86.2">poitrine</w> <w n="86.3">sanglante</w> <w n="86.4">et</w> <w n="86.5">la</w> <w n="86.6">mort</w> <w n="86.7">à</w> <w n="86.8">la</w> <w n="86.9">joue</w></l>
					<l n="87" num="12.15"><w n="87.1">Le</w> <w n="87.2">sergent</w> <w n="87.3">agonise</w>. <w n="87.4">Il</w> <w n="87.5">a</w> <w n="87.6">fait</w> <w n="87.7">son</w> <w n="87.8">devoir</w></l>
					<l n="88" num="12.16"><w n="88.1">Et</w> <w n="88.2">payé</w> <w n="88.3">son</w> <w n="88.4">tribut</w> <w n="88.5">à</w> <w n="88.6">l</w>'<w n="88.7">affreux</w> <w n="88.8">désespoir</w>.</l>
					<l n="89" num="12.17"><w n="89.1">Au</w> <w n="89.2">moment</w> <w n="89.3">d</w> '<w n="89.4">expirer</w>, <w n="89.5">il</w> <w n="89.6">ouvre</w> <w n="89.7">sa</w> <w n="89.8">tunique</w> :</l>
					<l n="90" num="12.18"><w n="90.1">Sa</w> <w n="90.2">main</w> <w n="90.3">y</w> <w n="90.4">cherche</w> <w n="90.5">encor</w> <w n="90.6">une</w> <w n="90.7">chère</w> <w n="90.8">relique</w>,</l>
					<l n="91" num="12.19"><w n="91.1">Les</w> <w n="91.2">fleurs</w> <w n="91.3">de</w> <w n="91.4">l</w>'<w n="91.5">Alsacienne</w>. <w n="91.6">Hélas</w> ! <w n="91.7">il</w> <w n="91.8">espérait</w></l>
					<l n="92" num="12.20"><w n="92.1">Dans</w> <w n="92.2">ce</w> <w n="92.3">doux</w> <w n="92.4">souvenir</w>. <w n="92.5">Sans</w> <w n="92.6">rien</w> <w n="92.7">dire</w>, <w n="92.8">en</w> <w n="92.9">secret</w>,</l>
					<l n="93" num="12.21"><w n="93.1">Avant</w> <w n="93.2">de</w> <w n="93.3">s</w>'<w n="93.4">élancer</w> <w n="93.5">dans</w> <w n="93.6">la</w> <w n="93.7">mêlée</w> <w n="93.8">horrible</w>,</l>
					<l n="94" num="12.22"><w n="94.1">Il</w> <w n="94.2">pensait</w> <w n="94.3">en</w> <w n="94.4">son</w> <w n="94.5">cœur</w> : <w n="94.6">Non</w> ! <w n="94.7">ce</w> <w n="94.8">n</w>'<w n="94.9">est</w> <w n="94.10">pas</w> <w n="94.11">possible</w> !</l>
					<l n="95" num="12.23"><w n="95.1">Je</w> <w n="95.2">ne</w> <w n="95.3">peux</w> <w n="95.4">pas</w> <w n="95.5">mourir</w> ! <w n="95.6">Je</w> <w n="95.7">garde</w> <w n="95.8">cet</w> <w n="95.9">espoir</w>,</l>
					<l n="96" num="12.24"><w n="96.1">Elle</w> <w n="96.2">me</w> <w n="96.3">l</w>'<w n="96.4">a</w> <w n="96.5">promis</w> <w n="96.6">et</w> <w n="96.7">je</w> <w n="96.8">dois</w> <w n="96.9">la</w> <w n="96.10">revoir</w> !</l>
				</lg>
				<lg n="13">
					<l n="97" num="13.1"><w n="97.1">Pourtant</w> <w n="97.2">il</w> <w n="97.3">va</w> <w n="97.4">mourir</w> ! <w n="97.5">Sur</w> <w n="97.6">ses</w> <w n="97.7">lèvres</w> <w n="97.8">glacées</w>,</l>
					<l n="98" num="13.2"><w n="98.1">C</w>'<w n="98.2">est</w> <w n="98.3">vainement</w> <w n="98.4">qu</w>'<w n="98.5">il</w> <w n="98.6">tient</w> <w n="98.7">les</w> <w n="98.8">chères</w> <w n="98.9">fleurs</w> <w n="98.10">pressées</w>.</l>
					<l n="99" num="13.3"><w n="99.1">L</w>'<w n="99.2">Alsacienne</w>, <w n="99.3">échappant</w> <w n="99.4">à</w> <w n="99.5">son</w> <w n="99.6">rêve</w> <w n="99.7">éperdu</w>,</l>
					<l n="100" num="13.4"><w n="100.1">Ne</w> <w n="100.2">vient</w> <w n="100.3">pas</w> <w n="100.4">lui</w> <w n="100.5">donner</w> <w n="100.6">le</w> <w n="100.7">baiser</w> <w n="100.8">attendu</w>…</l>
					<l n="101" num="13.5"><w n="101.1">Tout</w> <w n="101.2">à</w> <w n="101.3">coup</w> <w n="101.4">un</w> <w n="101.5">appel</w> <w n="101.6">a</w> <w n="101.7">frappé</w> <w n="101.8">son</w> <w n="101.9">oreille</w>…</l>
					<l n="102" num="13.6"><w n="102.1">Aux</w> <w n="102.2">portes</w> <w n="102.3">de</w> <w n="102.4">la</w> <w n="102.5">mort</w> <w n="102.6">le</w> <w n="102.7">mourant</w> <w n="102.8">se</w> <w n="102.9">réveille</w></l>
					<l n="103" num="13.7"><w n="103.1">Quelqu</w>'<w n="103.2">un</w> <w n="103.3">vient</w>… <w n="103.4">Une</w> <w n="103.5">voix</w> <w n="103.6">soupire</w> <w n="103.7">en</w> <w n="103.8">le</w> <w n="103.9">nommant</w> :</l>
					<l n="104" num="13.8">« <w n="104.1">O</w> <w n="104.2">Dieu</w> ! <w n="104.3">permettez</w>-<w n="104.4">moi</w> <w n="104.5">de</w> <w n="104.6">tenir</w> <w n="104.7">mon</w> <w n="104.8">serment</w> ! »</l>
					<l n="105" num="13.9"><w n="105.1">C</w>'<w n="105.2">est</w> <w n="105.3">elle</w>… <w n="105.4">Elle</w> <w n="105.5">le</w> <w n="105.6">cherche</w>… <w n="105.7">Elle</w> <w n="105.8">approche</w>… <w n="105.9">Elle</w> <w n="105.10">arrive</w>,</l>
					<l n="106" num="13.10"><w n="106.1">Un</w> <w n="106.2">noir</w> <w n="106.3">pressentiment</w> <w n="106.4">lui</w> <w n="106.5">fait</w> <w n="106.6">suivre</w> <w n="106.7">la</w> <w n="106.8">rive</w></l>
					<l n="107" num="13.11"><w n="107.1">Où</w> <w n="107.2">les</w> <w n="107.3">vergiss</w>-<w n="107.4">mein</w>-<w n="107.5">nicht</w> <w n="107.6">ouvrent</w> <w n="107.7">leurs</w> <w n="107.8">doux</w> <w n="107.9">yeux</w> <w n="107.10">bleus</w>.</l>
					<l n="108" num="13.12"><w n="108.1">Soudain</w> <w n="108.2">elle</w> <w n="108.3">le</w> <w n="108.4">voit</w>, <w n="108.5">il</w> <w n="108.6">est</w> <w n="108.7">là</w>, <w n="108.8">sous</w> <w n="108.9">ses</w> <w n="108.10">yeux</w>,</l>
					<l n="109" num="13.13"><w n="109.1">Sanglant</w>, <w n="109.2">expirant</w>, <w n="109.3">mort</w>… <w n="109.4">Un</w> <w n="109.5">suprême</w> <w n="109.6">sourire</w></l>
					<l n="110" num="13.14"><w n="110.1">Sur</w> <w n="110.2">sa</w> <w n="110.3">lèvre</w> <w n="110.4">glacée</w> <w n="110.5">erre</w> <w n="110.6">et</w> <w n="110.7">semble</w> <w n="110.8">lui</w> <w n="110.9">dire</w> :</l>
					<l n="111" num="13.15">« <w n="111.1">J</w>'<w n="111.2">attendais</w> <w n="111.3">ton</w> <w n="111.4">baiser</w>… <w n="111.5">Oh</w> ! <w n="111.6">pourquoi</w> <w n="111.7">m</w>'<w n="111.8">oublier</w> ! »</l>
				</lg>
				<lg n="14">
					<l n="112" num="14.1"><w n="112.1">L</w>' <w n="112.2">Alsacienne</w> <w n="112.3">en</w> <w n="112.4">pleurant</w> <w n="112.5">vient</w> <w n="112.6">de</w> <w n="112.7">s</w>'<w n="112.8">agenouiller</w>,</l>
					<l n="113" num="14.2"><w n="113.1">Elle</w> <w n="113.2">lave</w> <w n="113.3">le</w> <w n="113.4">sang</w> <w n="113.5">qui</w> <w n="113.6">coule</w> <w n="113.7">des</w> <w n="113.8">blessures</w>,</l>
					<l n="114" num="14.3"><w n="114.1">Mais</w> <w n="114.2">en</w> <w n="114.3">vain</w> <w n="114.4">sa</w> <w n="114.5">douleur</w> <w n="114.6">se</w> <w n="114.7">répand</w> <w n="114.8">en</w> <w n="114.9">murmures</w>,</l>
					<l n="115" num="14.4"><w n="115.1">En</w> <w n="115.2">vain</w> <w n="115.3">sa</w> <w n="115.4">douce</w> <w n="115.5">voix</w> <w n="115.6">s</w>'<w n="115.7">épuise</w> <w n="115.8">à</w> <w n="115.9">répéter</w> :</l>
					<l n="116" num="14.5">« <w n="116.1">C</w>'<w n="116.2">est</w> <w n="116.3">moi</w> ! <w n="116.4">c</w>'<w n="116.5">est</w> <w n="116.6">mon</w> <w n="116.7">baiser</w> <w n="116.8">que</w> <w n="116.9">je</w> <w n="116.10">viens</w> <w n="116.11">t</w>'<w n="116.12">apporter</w> ! »</l>
					<l n="117" num="14.6"><w n="117.1">Le</w> <w n="117.2">petit</w> <w n="117.3">sergent</w> <w n="117.4">dort</w> <w n="117.5">dans</w> <w n="117.6">la</w> <w n="117.7">paix</w> <w n="117.8">éternelle</w>.</l>
				</lg>
				<lg n="15">
					<l n="118" num="15.1"><w n="118.1">Alors</w>, <w n="118.2">d</w>'<w n="118.3">un</w> <w n="118.4">long</w> <w n="118.5">baiser</w> <w n="118.6">l</w>'<w n="118.7">alsacienne</w> <w n="118.8">fidèle</w></l>
					<l n="119" num="15.2"><w n="119.1">Ferme</w> <w n="119.2">ses</w> <w n="119.3">yeux</w> <w n="119.4">éteints</w>, <w n="119.5">et</w> <w n="119.6">puis</w>, <w n="119.7">se</w> <w n="119.8">redressant</w>,</l>
					<l n="120" num="15.3"><w n="120.1">Frémissante</w>, <w n="120.2">la</w> <w n="120.3">lèvre</w> <w n="120.4">encor</w> <w n="120.5">rouge</w> <w n="120.6">de</w> <w n="120.7">sang</w> :</l>
					<l n="121" num="15.4">« <w n="121.1">Prussiens</w> ! <w n="121.2">souvenez</w>-<w n="121.3">vous</w> <w n="121.4">comme</w> <w n="121.5">moi</w>, <w n="121.6">cria</w>-<w n="121.7">t</w>-<w n="121.8">elle</w>,</l>
					<l n="122" num="15.5"><w n="122.1">Vous</w> <w n="122.2">venez</w> <w n="122.3">de</w> <w n="122.4">sceller</w> <w n="122.5">l</w>'<w n="122.6">union</w> <w n="122.7">immortelle</w>.</l>
					<l n="123" num="15.6"><w n="123.1">De</w> <w n="123.2">cet</w> <w n="123.3">hymen</w> <w n="123.4">sanglant</w> <w n="123.5">la</w> <w n="123.6">haine</w> <w n="123.7">doit</w> <w n="123.8">germer</w>,</l>
					<l n="124" num="15.7"><w n="124.1">Et</w> <w n="124.2">je</w> <w n="124.3">vous</w> <w n="124.4">hais</w> <w n="124.5">autant</w> <w n="124.6">que</w> <w n="124.7">j</w>'<w n="124.8">aurais</w> <w n="124.9">dû</w> <w n="124.10">l</w>'<w n="124.11">aimer</w> ! »</l>
				</lg>
				<lg n="16">
					<l n="125" num="16.1"><w n="125.1">Sur</w> <w n="125.2">la</w> <w n="125.3">place</w> <w n="125.4">publique</w>, <w n="125.5">auprès</w> <w n="125.6">de</w> <w n="125.7">la</w> <w n="125.8">fontaine</w></l>
					<l n="126" num="16.2"><w n="126.1">On</w> <w n="126.2">voit</w> <w n="126.3">depuis</w> <w n="126.4">ce</w> <w n="126.5">jour</w> <w n="126.6">revenir</w> <w n="126.7">l</w>'<w n="126.8">Alsacienne</w>,</l>
					<l n="127" num="16.3"><w n="127.1">Ses</w> <w n="127.2">cheveux</w> <w n="127.3">blonds</w> <w n="127.4">noués</w> <w n="127.5">d</w>'<w n="127.6">un</w> <w n="127.7">large</w> <w n="127.8">ruban</w> <w n="127.9">noir</w>…</l>
					<l n="128" num="16.4"><w n="128.1">Sur</w> <w n="128.2">son</w> <w n="128.3">front</w> <w n="128.4">est</w> <w n="128.5">écrit</w> : <w n="128.6">Je</w> <w n="128.7">suis</w> <w n="128.8">le</w> <w n="128.9">désespoir</w> !</l>
					<l n="129" num="16.5"><w n="129.1">Là</w>-<w n="129.2">bas</w>, <w n="129.3">les</w> <w n="129.4">paysans</w> <w n="129.5">qui</w> <w n="129.6">savent</w> <w n="129.7">son</w> <w n="129.8">épreuve</w>,</l>
					<l n="130" num="16.6"><w n="130.1">La</w> <w n="130.2">saluant</w> <w n="130.3">bien</w> <w n="130.4">bas</w>, <w n="130.5">murmurent</w> : <w n="130.6">C</w> '<w n="130.7">est</w> <w n="130.8">la</w> <w n="130.9">veuve</w> !</l>
					<l n="131" num="16.7"><w n="131.1">Les</w> <w n="131.2">prussiens</w> <w n="131.3">ont</w> <w n="131.4">tué</w> <w n="131.5">son</w> <w n="131.6">sergent</w>, <w n="131.7">et</w> <w n="131.8">depuis</w>,</l>
					<l n="132" num="16.8"><w n="132.1">Vouée</w> <w n="132.2">à</w> <w n="132.3">sa</w> <w n="132.4">vengeance</w>, <w n="132.5">elle</w> <w n="132.6">attend</w> <w n="132.7">près</w> <w n="132.8">du</w> <w n="132.9">puits</w></l>
					<l n="133" num="16.9"><w n="133.1">Des</w> <w n="133.2">régiments</w> <w n="133.3">français</w> <w n="133.4">la</w> <w n="133.5">prochaine</w> <w n="133.6">arrivée</w>…</l>
					<l n="134" num="16.10"><w n="134.1">Ah</w> ! <w n="134.2">sonne</w>, <w n="134.3">heure</w> <w n="134.4">bénie</w> ! <w n="134.5">heure</w> <w n="134.6">ardemment</w> <w n="134.7">rêvée</w> !</l>
					<l n="135" num="16.11"><w n="135.1">Soleil</w> <w n="135.2">de</w> <w n="135.3">la</w> <w n="135.4">revanche</w>, <w n="135.5">éclate</w> <w n="135.6">au</w> <w n="135.7">fond</w> <w n="135.8">des</w> <w n="135.9">cieux</w> !</l>
					<l n="136" num="16.12"><w n="136.1">Debout</w>, <w n="136.2">français</w> ! <w n="136.3">frappons</w> <w n="136.4">sans</w> <w n="136.5">pitié</w>, <w n="136.6">furieux</w>,</l>
					<l n="137" num="16.13"><w n="137.1">Et</w> <w n="137.2">qu</w>'<w n="137.3">aux</w> <w n="137.4">veuves</w> <w n="137.5">en</w> <w n="137.6">deuil</w> <w n="137.7">rendant</w> <w n="137.8">enfin</w> <w n="137.9">justice</w>,</l>
					<l n="138" num="16.14"><w n="138.1">Chaque</w> <w n="138.2">puits</w> <w n="138.3">Alsacien</w> <w n="138.4">de</w> <w n="138.5">sang</w> <w n="138.6">prussien</w> <w n="138.7">s</w> '<w n="138.8">emplisse</w> !</l>
				</lg>
			</div></body></text></TEI>