<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA PATRIE !</title>
				<title type="medium">Édition électronique</title>
				<author key="TST">
					<name>
						<forename>Tyrtée</forename>
						<surname>TASTET</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>392 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">TST_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA PATRIE !</title>
						<author>TYRTÉE TASTET</author>
						<imprint>
							<pubPlace>NANTES</pubPlace>
							<publisher>LIBRAIRIE MOREL</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="TST2">
				<head type="main">LA FIANCÉE DU FRANC-TIREUR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">A</w> <w n="1.2">moi</w>, <w n="1.3">balle</w> ! <w n="1.4">vers</w> <w n="1.5">toi</w> <w n="1.6">s</w>'<w n="1.7">élance</w> <w n="1.8">ma</w> <w n="1.9">pensée</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Viens</w> <w n="2.2">des</w> <w n="2.3">vengeurs</w> <w n="2.4">du</w> <w n="2.5">sol</w> <w n="2.6">m</w>'<w n="2.7">ouvrir</w> <w n="2.8">le</w> <w n="2.9">Panthéon</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Regarde</w>-<w n="3.2">moi</w>, <w n="3.3">farouche</w> <w n="3.4">et</w> <w n="3.5">sombre</w> <w n="3.6">fiancée</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">dédaignas</w> <w n="4.3">Kléber</w>, <w n="4.4">Hoche</w> <w n="4.5">et</w> <w n="4.6">Napoléon</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Leur</w> <w n="5.2">regard</w> <w n="5.3">de</w> <w n="5.4">Titans</w> <w n="5.5">effrayait</w> <w n="5.6">ton</w> <w n="5.7">audace</w> ;</l>
					<l n="6" num="2.2"><w n="6.1">Surprise</w>, <w n="6.2">tu</w> <w n="6.3">cherchais</w> <w n="6.4">l</w>'<w n="6.5">amour</w> <w n="6.6">en</w> <w n="6.7">d</w>'<w n="6.8">autres</w> <w n="6.9">bras</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Mais</w> <w n="7.2">de</w> <w n="7.3">cent</w> <w n="7.4">pieds</w> <w n="7.5">et</w> <w n="7.6">plus</w> <w n="7.7">leur</w> <w n="7.8">taille</w> <w n="7.9">me</w> <w n="7.10">dépasse</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Je</w> <w n="8.2">ne</w> <w n="8.3">suis</w> <w n="8.4">qu</w>'<w n="8.5">homme</w>, <w n="8.6">et</w> <w n="8.7">moi</w>, <w n="8.8">balle</w>, <w n="8.9">tu</w> <w n="8.10">m</w>'<w n="8.11">aimeras</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">A</w> <w n="9.2">moi</w> <w n="9.3">ta</w> <w n="9.4">voix</w>, <w n="9.5">à</w> <w n="9.6">moi</w> <w n="9.7">ton</w> <w n="9.8">baiser</w> <w n="9.9">de</w> <w n="9.10">vampire</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Ton</w> <w n="10.2">ardeur</w> <w n="10.3">de</w> <w n="10.4">bacchante</w> <w n="10.5">et</w> <w n="10.6">ton</w> <w n="10.7">souffle</w> <w n="10.8">de</w> <w n="10.9">feu</w> !</l>
					<l n="11" num="3.3"><w n="11.1">Que</w> <w n="11.2">sur</w> <w n="11.3">ton</w> <w n="11.4">cœur</w> <w n="11.5">de</w> <w n="11.6">bronze</w> <w n="11.7">un</w> <w n="11.8">moment</w> <w n="11.9">je</w> <w n="11.10">respire</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Je</w> <w n="12.2">partirai</w> <w n="12.3">content</w> <w n="12.4">et</w> <w n="12.5">rendant</w> <w n="12.6">grâce</w> <w n="12.7">à</w> <w n="12.8">Dieu</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Viens</w> ! <w n="13.2">pour</w> <w n="13.3">voile</w> <w n="13.4">sacré</w>, <w n="13.5">dans</w> <w n="13.6">la</w> <w n="13.7">noire</w> <w n="13.8">journée</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Nous</w> <w n="14.2">aurons</w> <w n="14.3">l</w>'<w n="14.4">incendie</w> <w n="14.5">et</w> <w n="14.6">son</w> <w n="14.7">vaste</w> <w n="14.8">giron</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Pour</w> <w n="15.2">prêtre</w> <w n="15.3">la</w> <w n="15.4">mitraille</w>, <w n="15.5">et</w> <w n="15.6">pour</w> <w n="15.7">chant</w> <w n="15.8">d</w>'<w n="15.9">hyménée</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Mêlés</w> <w n="16.2">au</w> <w n="16.3">bruit</w> <w n="16.4">du</w> <w n="16.5">fer</w>, <w n="16.6">les</w> <w n="16.7">éclats</w> <w n="16.8">du</w> <w n="16.9">clairon</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Remplace</w> <w n="17.2">auprès</w> <w n="17.3">de</w> <w n="17.4">moi</w>, <w n="17.5">reine</w> <w n="17.6">de</w> <w n="17.7">l</w>'<w n="17.8">hécatombe</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Le</w> <w n="18.2">toit</w> <w n="18.3">vert</w> <w n="18.4">du</w> <w n="18.5">chasseur</w> <w n="18.6">et</w> <w n="18.7">le</w> <w n="18.8">foyer</w> <w n="18.9">absent</w> ;</l>
					<l n="19" num="5.3"><w n="19.1">Tout</w> <w n="19.2">ost</w> <w n="19.3">prêt</w> ; <w n="19.4">aux</w> <w n="19.5">vautours</w> <w n="19.6">j</w>'<w n="19.7">ai</w> <w n="19.8">dit</w> : <w n="19.9">Soyez</w> <w n="19.10">ma</w> <w n="19.11">tombe</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">je</w> <w n="20.3">t</w>'<w n="20.4">offre</w> <w n="20.5">pour</w> <w n="20.6">lit</w> <w n="20.7">l</w>'<w n="20.8">herbe</w> <w n="20.9">rouge</w> <w n="20.10">de</w> <w n="20.11">sang</w>.</l>
				</lg>
			</div></body></text></TEI>