<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA PATRIE !</title>
				<title type="medium">Édition électronique</title>
				<author key="TST">
					<name>
						<forename>Tyrtée</forename>
						<surname>TASTET</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>392 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">TST_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA PATRIE !</title>
						<author>TYRTÉE TASTET</author>
						<imprint>
							<pubPlace>NANTES</pubPlace>
							<publisher>LIBRAIRIE MOREL</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="TST6">
				<head type="main">RÉPONSE. LE TALION</head>
				<head type="sub">LA FRANCE A LA PRUSSE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ton</w> <w n="1.2">poète</w> <w n="1.3">a</w> <w n="1.4">raison</w>, <w n="1.5">sœur</w> ; <w n="1.6">la</w> <w n="1.7">vengeance</w> <w n="1.8">est</w> <w n="1.9">douce</w> :</l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>'<w n="2.2">on</w> <w n="2.3">aiguise</w> <w n="2.4">la</w> <w n="2.5">faux</w> <w n="2.6">si</w> <w n="2.7">le</w> <w n="2.8">glaive</w> <w n="2.9">s</w>'<w n="2.10">émousse</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Nous</w> <w n="3.2">saurons</w> <w n="3.3">dans</w> <w n="3.4">l</w>'<w n="3.5">affreux</w> <w n="3.6">te</w> <w n="3.7">suivre</w> <w n="3.8">jusqu</w>'<w n="3.9">au</w> <w n="3.10">bout</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w>, <w n="4.2">rendant</w> <w n="4.3">ses</w> <w n="4.4">leçons</w> <w n="4.5">à</w> <w n="4.6">la</w> <w n="4.7">docte</w> <w n="4.8">maîtresse</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Y</w> <w n="5.2">promener</w> <w n="5.3">si</w> <w n="5.4">bien</w> <w n="5.5">la</w> <w n="5.6">torche</w> <w n="5.7">vengeresse</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Que</w> <w n="6.2">du</w> <w n="6.3">Rhin</w> <w n="6.4">à</w> <w n="6.5">l</w>'<w n="6.6">Oder</w> <w n="6.7">rien</w> <w n="6.8">ne</w> <w n="6.9">reste</w> <w n="6.10">debout</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Meurs</w>, <w n="7.2">ou</w> <w n="7.3">tue</w> ! <w n="7.4">Entre</w> <w n="7.5">nous</w>, <w n="7.6">et</w> <w n="7.7">c</w>'<w n="7.8">est</w> <w n="7.9">vous</w> <w n="7.10">qui</w> <w n="7.11">le</w> <w n="7.12">dites</w>,</l>
					<l n="8" num="2.2"><w n="8.1">La</w> <w n="8.2">justice</w> <w n="8.3">est</w> <w n="8.4">sans</w> <w n="8.5">force</w> <w n="8.6">et</w> <w n="8.7">les</w> <w n="8.8">lois</w> <w n="8.9">sont</w> <w n="8.10">maudites</w>,</l>
					<l n="9" num="2.3"><w n="9.1">Le</w> <w n="9.2">carnage</w> <w n="9.3">et</w> <w n="9.4">l</w>'<w n="9.5">horreur</w> <w n="9.6">ont</w> <w n="9.7">seuls</w> <w n="9.8">droit</w> <w n="9.9">d</w>'<w n="9.10">exister</w>.</l>
					<l n="10" num="2.4"><w n="10.1">Nous</w> <w n="10.2">acceptons</w>, <w n="10.3">bandits</w>, <w n="10.4">votre</w> <w n="10.5">duel</w> <w n="10.6">infâme</w> :</l>
					<l n="11" num="2.5"><w n="11.1">Aux</w> <w n="11.2">douleurs</w> <w n="11.3">de</w> <w n="11.4">l</w>'<w n="11.5">enfant</w>, <w n="11.6">au</w> <w n="11.7">râle</w> <w n="11.8">de</w> <w n="11.9">la</w> <w n="11.10">femme</w>,</l>
					<l n="12" num="2.6"><w n="12.1">Comme</w> <w n="12.2">vous</w>, <w n="12.3">comme</w> <w n="12.4">vous</w> <w n="12.5">nous</w> <w n="12.6">saurons</w> <w n="12.7">insulter</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">En</w> <w n="13.2">guerre</w> ! <w n="13.3">enivrons</w>-<w n="13.4">nous</w> <w n="13.5">de</w> <w n="13.6">meurtre</w> <w n="13.7">et</w> <w n="13.8">de</w> <w n="13.9">luxure</w> ;</l>
					<l n="14" num="3.2"><w n="14.1">Par</w> <w n="14.2">des</w> <w n="14.3">forfaits</w> <w n="14.4">sans</w> <w n="14.5">nombre</w> <w n="14.6">effrayons</w> <w n="14.7">la</w> <w n="14.8">nature</w>,</l>
					<l n="15" num="3.3"><w n="15.1">Que</w> <w n="15.2">de</w> <w n="15.3">sang</w> <w n="15.4">le</w> <w n="15.5">ciel</w> <w n="15.6">soit</w> <w n="15.7">obscurci</w> ;</l>
					<l n="16" num="3.4"><w n="16.1">A</w> <w n="16.2">Papis</w>, <w n="16.3">à</w> <w n="16.4">Berlin</w>, <w n="16.5">portons</w> <w n="16.6">nos</w> <w n="16.7">saturnales</w>,</l>
					<l n="17" num="3.5"><w n="17.1">Et</w> <w n="17.2">qu</w>'<w n="17.3">on</w> <w n="17.4">juge</w>, <w n="17.5">devant</w> <w n="17.6">nos</w> <w n="17.7">œuvres</w> <w n="17.8">infernales</w>,</l>
					<l n="18" num="3.6"><w n="18.1">Qui</w> <w n="18.2">du</w> <w n="18.3">Celte</w> <w n="18.4">ou</w> <w n="18.5">de</w> <w n="18.6">vous</w> <w n="18.7">aura</w> <w n="18.8">mieux</w> <w n="18.9">réussi</w>.</l>
				</lg>
				<closer>
					<signed>TYRTÉE.</signed>
					<note type="footnote" id="">A Dieu ne plaise que l'atrocité soit à l'ordre du jour entre les deux armées. Mais il peut n'être pas inutile de montrer à ces bêtes féroces qu'au besoin on leur appliquera résolument la loi du talion.</note>
				</closer>
			</div></body></text></TEI>