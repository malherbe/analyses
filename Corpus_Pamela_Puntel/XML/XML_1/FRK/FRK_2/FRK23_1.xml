<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉPAVES</head><div type="poem" key="FRK23">
					<head type="main">DEVANT UNE BIÈRE</head>
					<opener>
						<salute>A la mémoire de M. Küss, Maire et Député de Strasbourg<ref target="1" type="noteAnchor">1</ref></salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">TE</w> <w n="1.2">voilà</w> <w n="1.3">mort</w>, <w n="1.4">brave</w> <w n="1.5">homme</w>, <w n="1.6">et</w> <w n="1.7">ta</w> <w n="1.8">peine</w> <w n="1.9">est</w> <w n="1.10">finie</w> :</l>
						<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">ne</w> <w n="2.3">souffriras</w> <w n="2.4">plus</w> <w n="2.5">pour</w> <w n="2.6">ton</w> <w n="2.7">pays</w> <w n="2.8">perdu</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Toi</w> <w n="3.2">qui</w> <w n="3.3">l</w>’<w n="3.4">as</w> <w n="3.5">disputé</w> <w n="3.6">sur</w> <w n="3.7">ton</w> <w n="3.8">lit</w> <w n="3.9">d</w>’<w n="3.10">agonie</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Sois</w> <w n="4.2">pleuré</w> <w n="4.3">par</w> <w n="4.4">tous</w> <w n="4.5">ceux</w> <w n="4.6">qui</w> <w n="4.7">ne</w> <w n="4.8">l</w>’<w n="4.9">ont</w> <w n="4.10">pas</w> <w n="4.11">vendu</w> !…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Donc</w>, <w n="5.2">c</w>’<w n="5.3">est</w> <w n="5.4">dit</w> ? <w n="5.5">Aujourd</w>’<w n="5.6">hui</w>, <w n="5.7">pour</w> <w n="5.8">le</w> <w n="5.9">bonheur</w> <w n="5.10">de</w> <w n="5.11">vivre</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Pour</w> <w n="6.2">la</w> <w n="6.3">paix</w> — <w n="6.4">même</w> <w n="6.5">indigne</w> — <w n="6.6">on</w> <w n="6.7">vend</w> <w n="6.8">ces</w> <w n="6.9">choses</w>-<w n="6.10">là</w> !</l>
						<l n="7" num="2.3"><w n="7.1">Et</w>, <w n="7.2">jetant</w> <w n="7.3">la</w> <w n="7.4">Patrie</w> <w n="7.5">aux</w> <w n="7.6">pieds</w> <w n="7.7">d</w>’<w n="7.8">un</w> <w n="7.9">soudard</w> <w n="7.10">ivre</w>,</l>
						<l n="8" num="2.4"><w n="8.1">On</w> <w n="8.2">outrage</w> <w n="8.3">avec</w> <w n="8.4">lui</w> <w n="8.5">le</w> <w n="8.6">sein</w> <w n="8.7">qu</w>’<w n="8.8">il</w> <w n="8.9">mutila</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Donc</w>, <w n="9.2">nous</w> <w n="9.3">livrerons</w> <w n="9.4">tout</w> : <w n="9.5">Metz</w>, <w n="9.6">la</w> <w n="9.7">cité</w> <w n="9.8">lorraine</w>,</l>
						<l n="10" num="3.2"><w n="10.1">L</w>’<w n="10.2">Alsace</w> <w n="10.3">avec</w> <w n="10.4">Strasbourg</w>, <w n="10.5">le</w> <w n="10.6">sol</w> <w n="10.7">avec</w> <w n="10.8">la</w> <w n="10.9">chair</w> !…</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">l</w>’<w n="11.3">on</w> <w n="11.4">parle</w> <w n="11.5">des</w> <w n="11.6">maux</w> <w n="11.7">que</w> <w n="11.8">la</w> <w n="11.9">bataille</w> <w n="11.10">entraîne</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Lorsqu</w>’<w n="12.2">un</w> <w n="12.3">mot</w> <w n="12.4">cède</w> <w n="12.5">ainsi</w> <w n="12.6">notre</w> <w n="12.7">sang</w> <w n="12.8">le</w> <w n="12.9">plus</w> <w n="12.10">cher</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Quand</w> <w n="13.2">ils</w> <w n="13.3">tiendront</w> <w n="13.4">l</w>’<w n="13.5">honneur</w>, <w n="13.6">l</w>’<w n="13.7">or</w> <w n="13.8">et</w> <w n="13.9">la</w> <w n="13.10">terre</w> <w n="13.11">esclave</w></l>
						<l n="14" num="4.2"><w n="14.1">On</w> <w n="14.2">sentira</w> <w n="14.3">quel</w> <w n="14.4">poids</w> <w n="14.5">s</w>’<w n="14.6">est</w> <w n="14.7">abattu</w> <w n="14.8">sur</w> <w n="14.9">nous</w> :</l>
						<l n="15" num="4.3"><w n="15.1">L</w>’<w n="15.2">égoïste</w> <w n="15.3">sans</w> <w n="15.4">âme</w> <w n="15.5">et</w> <w n="15.6">le</w> <w n="15.7">goujat</w> <w n="15.8">qui</w> <w n="15.9">bave</w></l>
						<l n="16" num="4.4"><w n="16.1">Trouveront</w> <w n="16.2">le</w> <w n="16.3">chemin</w> <w n="16.4">bien</w> <w n="16.5">dur</w> <w n="16.6">pour</w> <w n="16.7">leurs</w> <w n="16.8">genoux</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Car</w> <w n="17.2">il</w> <w n="17.3">faudra</w> <w n="17.4">souvent</w> <w n="17.5">trébucher</w> <w n="17.6">sur</w> <w n="17.7">les</w> <w n="17.8">pierres</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Subir</w> <w n="18.2">du</w> <w n="18.3">tortureur</w> <w n="18.4">l</w>’<w n="18.5">immonde</w> <w n="18.6">inimitié</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">les</w> <w n="19.3">martyrs</w> <w n="19.4">du</w> <w n="19.5">Droit</w>, <w n="19.6">soulevant</w> <w n="19.7">leurs</w> <w n="19.8">paupières</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Dans</w> <w n="20.2">leurs</w> <w n="20.3">sépulcres</w> <w n="20.4">noirs</w> <w n="20.5">seront</w> <w n="20.6">pris</w> <w n="20.7">de</w> <w n="20.8">pitié</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Adieu</w>, <w n="21.2">frère</w> <w n="21.3">d</w>’<w n="21.4">Alsace</w> !… <w n="21.5">Emportez</w> <w n="21.6">la</w> <w n="21.7">dépouille</w> :</l>
						<l n="22" num="6.2"><w n="22.1">Qu</w>’<w n="22.2">on</w> <w n="22.3">l</w>’<w n="22.4">enterre</w> <w n="22.5">là</w>-<w n="22.6">bas</w>, <w n="22.7">dans</w> <w n="22.8">le</w> <w n="22.9">sol</w> <w n="22.10">envahi</w> :</l>
						<l n="23" num="6.3"><w n="23.1">Ce</w> <w n="23.2">tombeau</w> <w n="23.3">sera</w> <w n="23.4">bien</w>, <w n="23.5">près</w> <w n="23.6">du</w> <w n="23.7">foyer</w> <w n="23.8">qu</w>’<w n="23.9">on</w> <w n="23.10">souille</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Loin</w> <w n="24.2">de</w> <w n="24.3">la</w> <w n="24.4">France</w> <w n="24.5">inerte</w> <w n="24.6">et</w> <w n="24.7">du</w> <w n="24.8">drapeau</w> <w n="24.9">trahi</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">—<w n="25.1">Un</w> <w n="25.2">jour</w>, <w n="25.3">s</w>’<w n="25.4">il</w> <w n="25.5">te</w> <w n="25.6">souvient</w>, <w n="25.7">Peuple</w>, <w n="25.8">que</w> <w n="25.9">tu</w> <w n="25.10">fus</w> <w n="25.11">lâche</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Tu</w> <w n="26.2">voudras</w> <w n="26.3">ressaisir</w> <w n="26.4">le</w> <w n="26.5">spectre</w> <w n="26.6">et</w> <w n="26.7">les</w> <w n="26.8">vivants</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Français</w> <w n="27.2">malgré</w> <w n="27.3">la</w> <w n="27.4">France</w>, <w n="27.5">obstinés</w> <w n="27.6">dans</w> <w n="27.7">la</w> <w n="27.8">tâche</w></l>
						<l n="28" num="7.4"><w n="28.1">De</w> <w n="28.2">léguer</w> <w n="28.3">ton</w> <w n="28.4">grand</w> <w n="28.5">nom</w> <w n="28.6">à</w> <w n="28.7">leurs</w> <w n="28.8">derniers</w> <w n="28.9">enfants</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Ils</w> <w n="29.2">t</w>’<w n="29.3">aiment</w>, <w n="29.4">ces</w> <w n="29.5">bannis</w> ! <w n="29.6">Dans</w> <w n="29.7">leur</w> <w n="29.8">male</w> <w n="29.9">constance</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Ils</w> <w n="30.2">te</w> <w n="30.3">pardonneront</w>… <w n="30.4">si</w> <w n="30.5">tu</w> <w n="30.6">réponds</w> <w n="30.7">au</w> <w n="30.8">cri</w></l>
						<l n="31" num="8.3"><w n="31.1">De</w> <w n="31.2">l</w>’<w n="31.3">homme</w> <w n="31.4">de</w> <w n="31.5">Strasbourg</w> <w n="31.6">tué</w> <w n="31.7">par</w> <w n="31.8">ta</w> <w n="31.9">sentence</w>,</l>
						<l n="32" num="8.4"><w n="32.1">N</w>’<w n="32.2">ayant</w> <w n="32.3">plus</w> <w n="32.4">que</w> <w n="32.5">la</w> <w n="32.6">paix</w> <w n="32.7">du</w> <w n="32.8">cercueil</w> <w n="32.9">pour</w> <w n="32.10">abri</w> !</l>
					</lg>
					<closer>
						<note type="footnote" id="1">Mort à Bordeaux dans la nuit du Ier mars 1871, date de la ratification du traité portant cession de l’Alsace aux Prussiens.</note>
					</closer>
				</div></body></text></TEI>