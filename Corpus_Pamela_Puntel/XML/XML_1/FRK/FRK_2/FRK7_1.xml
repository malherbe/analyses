<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’EMPIRE</head><div type="poem" key="FRK7">
					<head type="main">LES PROSCRITS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">PENDANT</w> <w n="1.2">que</w>, <w n="1.3">satisfaits</w> <w n="1.4">de</w> <w n="1.5">boire</w> <w n="1.6">et</w> <w n="1.7">de</w> <w n="1.8">manger</w>,</l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">manger</w> <w n="2.3">sans</w> <w n="2.4">mesure</w> <w n="2.5">et</w> <w n="2.6">de</w> <w n="2.7">boire</w> <w n="2.8">à</w> <w n="2.9">plein</w> <w n="2.10">verre</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">d</w>’<w n="3.3">âpres</w> <w n="3.4">truands</w> <w n="3.5">sur</w> <w n="3.6">un</w> <w n="3.7">sol</w> <w n="3.8">étranger</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Extorquant</w> <w n="4.2">tous</w> <w n="4.3">les</w> <w n="4.4">fruits</w> <w n="4.5">de</w> <w n="4.6">notre</w> <w n="4.7">pauvre</w> <w n="4.8">terre</w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">César</w> <w n="5.2">bandit</w> <w n="5.3">et</w> <w n="5.4">ses</w> <w n="5.5">tueurs</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Pillent</w> <w n="6.2">la</w> <w n="6.3">moisson</w> <w n="6.4">et</w> <w n="6.5">la</w> <w n="6.6">vigne</w></l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Qu</w>’<w n="7.2">arrosèrent</w> <w n="7.3">tant</w> <w n="7.4">de</w> <w n="7.5">sueurs</w>,</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1"><space unit="char" quantity="8"></space><w n="8.1">Ceux</w> <w n="8.2">qui</w> <w n="8.3">sont</w> <w n="8.4">proscrits</w> <w n="8.5">nous</w> <w n="8.6">font</w> <w n="8.7">signe</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Pendant</w> <w n="9.2">que</w> <w n="9.3">nous</w> <w n="9.4">pleurons</w>, <w n="9.5">las</w> <w n="9.6">de</w> <w n="9.7">vivre</w> <w n="9.8">avilis</w>,</l>
						<l n="10" num="3.2"><w n="10.1">De</w> <w n="10.2">travailler</w> <w n="10.3">toujours</w> <w n="10.4">pour</w> <w n="10.5">un</w> <w n="10.6">maigre</w> <w n="10.7">salaire</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Ou</w> <w n="11.2">que</w> <w n="11.3">nous</w> <w n="11.4">arrachons</w> <w n="11.5">quelques</w> <w n="11.6">lambeaux</w> <w n="11.7">salis</w>,</l>
						<l n="12" num="3.4">—<w n="12.1">Refoulant</w> <w n="12.2">dans</w> <w n="12.3">nos</w> <w n="12.4">cœurs</w> <w n="12.5">l</w>’<w n="12.6">impuissante</w> <w n="12.7">colère</w>,—</l>
						<l n="13" num="3.5"><space unit="char" quantity="8"></space><w n="13.1">Là</w>-<w n="13.2">bas</w>, <w n="13.3">tristes</w> <w n="13.4">sous</w> <w n="13.5">d</w>’<w n="13.6">autres</w> <w n="13.7">cieux</w>,</l>
						<l n="14" num="3.6"><space unit="char" quantity="8"></space><w n="14.1">Dans</w> <w n="14.2">un</w> <w n="14.3">labeur</w> <w n="14.4">stoïque</w> <w n="14.5">et</w> <w n="14.6">digne</w>,</l>
						<l n="15" num="3.7"><space unit="char" quantity="8"></space><w n="15.1">Tristes</w>, <w n="15.2">mais</w> <w n="15.3">fiers</w> <w n="15.4">et</w> <w n="15.5">maîtres</w> <w n="15.6">d</w>’<w n="15.7">eux</w>,</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><space unit="char" quantity="8"></space><w n="16.1">Ceux</w> <w n="16.2">qui</w> <w n="16.3">sont</w> <w n="16.4">proscrits</w> <w n="16.5">nous</w> <w n="16.6">font</w> <w n="16.7">signe</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Pendant</w> <w n="17.2">que</w>, <w n="17.3">pour</w> <w n="17.4">tromper</w> <w n="17.5">nos</w> <w n="17.6">cuisantes</w> <w n="17.7">douleurs</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Nous</w> <w n="18.2">appelons</w> <w n="18.3">l</w>’<w n="18.4">ivresse</w> <w n="18.5">ou</w> <w n="18.6">grossière</w> <w n="18.7">ou</w> <w n="18.8">charmante</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Pour</w> <w n="19.2">que</w> <w n="19.3">tout</w>, <w n="19.4">en</w> <w n="19.5">un</w> <w n="19.6">rêve</w> <w n="19.7">aux</w> <w n="19.8">joyeuses</w> <w n="19.9">couleurs</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Nous</w> <w n="20.2">berce</w> <w n="20.3">longuement</w>, <w n="20.4">nous</w> <w n="20.5">énerve</w> <w n="20.6">et</w> <w n="20.7">nous</w> <w n="20.8">mente</w>,</l>
						<l n="21" num="5.5"><space unit="char" quantity="8"></space>— <w n="21.1">N</w>’<w n="21.2">enviant</w> <w n="21.3">pour</w> <w n="21.4">nos</w> <w n="21.5">bras</w> <w n="21.6">lascifs</w></l>
						<l n="22" num="5.6"><space unit="char" quantity="8"></space><w n="22.1">Que</w> <w n="22.2">femmes</w> <w n="22.3">aux</w> <w n="22.4">blancheurs</w> <w n="22.5">de</w> <w n="22.6">cygne</w>,—</l>
						<l n="23" num="5.7"><space unit="char" quantity="8"></space><w n="23.1">Là</w>-<w n="23.2">bas</w>, <w n="23.3">parmi</w> <w n="23.4">les</w> <w n="23.5">noirs</w> <w n="23.6">récifs</w>,</l>
					</lg>
					<lg n="6">
						<l n="24" num="6.1"><space unit="char" quantity="8"></space><w n="24.1">Ceux</w> <w n="24.2">qui</w> <w n="24.3">sont</w> <w n="24.4">proscrits</w> <w n="24.5">nous</w> <w n="24.6">font</w> <w n="24.7">signe</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Pendant</w> <w n="25.2">que</w> <w n="25.3">nous</w> <w n="25.4">rampons</w>, <w n="25.5">cherchant</w> <w n="25.6">la</w> <w n="25.7">volupté</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Eux</w>, <w n="26.2">superbes</w> <w n="26.3">et</w> <w n="26.4">purs</w>, —<w n="26.5">eux</w>, <w n="26.6">debout</w> <w n="26.7">et</w> <w n="26.8">vivaces</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Qui</w> <w n="27.2">n</w>’<w n="27.3">ont</w> <w n="27.4">jamais</w> <w n="27.5">vécu</w> <w n="27.6">de</w> <w n="27.7">l</w>’<w n="27.8">opprobre</w> <w n="27.9">accepté</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Des</w> <w n="28.2">hauteurs</w> <w n="28.3">de</w> <w n="28.4">l</w>’<w n="28.5">exil</w> <w n="28.6">vers</w> <w n="28.7">nous</w> <w n="28.8">tournant</w> <w n="28.9">leurs</w> <w n="28.10">faces</w>,</l>
						<l n="29" num="7.5"><space unit="char" quantity="8"></space><w n="29.1">Prennent</w> <w n="29.2">en</w> <w n="29.3">pitié</w> <w n="29.4">nos</w> <w n="29.5">plaisirs</w> !</l>
						<l n="30" num="7.6"><space unit="char" quantity="8"></space><w n="30.1">De</w> <w n="30.2">leur</w> <w n="30.3">devoir</w> <w n="30.4">suivant</w> <w n="30.5">la</w> <w n="30.6">ligne</w>,</l>
						<l n="31" num="7.7"><space unit="char" quantity="8"></space><w n="31.1">L</w>’<w n="31.2">âme</w> <w n="31.3">close</w> <w n="31.4">aux</w> <w n="31.5">lâches</w> <w n="31.6">désirs</w>,</l>
					</lg>
					<lg n="8">
						<l n="32" num="8.1"><space unit="char" quantity="8"></space><w n="32.1">Ceux</w> <w n="32.2">qui</w> <w n="32.3">sont</w> <w n="32.4">proscrits</w> <w n="32.5">nous</w> <w n="32.6">font</w> <w n="32.7">signe</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Pendant</w> <w n="33.2">que</w> <w n="33.3">de</w> <w n="33.4">nos</w> <w n="33.5">mains</w> <w n="33.6">nous</w> <w n="33.7">laissons</w> <w n="33.8">jour</w> <w n="33.9">par</w> <w n="33.10">jour</w></l>
						<l n="34" num="9.2"><w n="34.1">Glisser</w> <w n="34.2">ineptement</w> <w n="34.3">ce</w> <w n="34.4">qui</w> <w n="34.5">nous</w> <w n="34.6">reste</w> <w n="34.7">d</w>’<w n="34.8">armes</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Ils</w> <w n="35.2">ne</w> <w n="35.3">forgent</w> <w n="35.4">pour</w> <w n="35.5">nous</w>, <w n="35.6">préparant</w> <w n="35.7">leur</w> <w n="35.8">retour</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">nous</w> <w n="36.3">montrent</w> <w n="36.4">le</w> <w n="36.5">fer</w> <w n="36.6">qu</w>’<w n="36.7">ils</w> <w n="36.8">trempent</w> <w n="36.9">de</w> <w n="36.10">leurs</w> <w n="36.11">larmes</w>.</l>
						<l n="37" num="9.5"><space unit="char" quantity="8"></space><w n="37.1">Le</w> <w n="37.2">fer</w> <w n="37.3">qui</w> <w n="37.4">vaincra</w> <w n="37.5">es</w> <w n="37.6">tyrans</w> !</l>
						<l n="38" num="9.6"><space unit="char" quantity="8"></space><w n="38.1">Pour</w> <w n="38.2">accomplir</w> <w n="38.3">la</w> <w n="38.4">tâche</w> <w n="38.5">insigne</w></l>
						<l n="39" num="9.7"><space unit="char" quantity="8"></space><w n="39.1">Qui</w> <w n="39.2">nous</w> <w n="39.3">rendra</w> <w n="39.4">libres</w> <w n="39.5">et</w> <w n="39.6">grands</w>,</l>
					</lg>
					<lg n="10">
						<l n="40" num="10.1"><space unit="char" quantity="8"></space><w n="40.1">Ceux</w> <w n="40.2">qui</w> <w n="40.3">sont</w> <w n="40.4">proscrits</w> <w n="40.5">nous</w> <w n="40.6">font</w> <w n="40.7">signe</w> !</l>
					</lg>
				</div></body></text></TEI>