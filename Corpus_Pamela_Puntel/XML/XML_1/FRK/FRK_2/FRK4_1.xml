<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’EMPIRE</head><div type="poem" key="FRK4">
					<head type="main">L’HOMME DES RASTELLS</head>
					<p>
						« Dans les rastells, on boit, on mange, et puis l’on danse… les élections
						sont devenues des noces de Gamache. »
						(Discours de M. Jules Simon au Corps législatif sur l’élection de
						M. Justin Durand, candidat officiel.)
						<lb></lb>
						— Séance du 7 décembre 1869)
						<lb></lb>
						Discours de M. Emmanuel Arago : « Ordonnez une enquête parlementaire,
						vous rassurez le département… Mais si, après les spectacles qui s’y sont
						produits ; si, lorsqu’on a vu le maire, à la tête de ses hommes ivres,
						sortant des rastells avec son écharpe en sautoir, hurlant et vomissant
						ces injures… (Interruptions) ; si, après ce que j’ai vu, moi : des hommes,
						des femmes, des enfants, se traînant dans le ruisseau et criant : Vive
						celui qui paye ! si, après qu’on a vu l’intimidation s’exercer par ordre
						du préfet… ; si, après tout cela, l’élection de M. Durand est purement et
						simplement validée comme une élection pure et nette, je vous le déclare,
						votre décision portera la stupeur (Bruit), le découragement, le désespoir
						moral, dans tous les cœurs honnêtes. (Vive approbation et applaudissements
						à gauche. —Réclamations.) —L’enquête est rejetée. L’élection est validée. 
					</p>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Jadis</w>, <w n="1.2">César</w>, <w n="1.3">vautré</w> <w n="1.4">sur</w> <w n="1.5">la</w> <w n="1.6">pourpre</w> <w n="1.7">romaine</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Entouré</w> <w n="2.2">de</w> <w n="2.3">valets</w> <w n="2.4">et</w> <w n="2.5">de</w> <w n="2.6">centurions</w>,</l>
							<l n="3" num="1.3"><w n="3.1">En</w> <w n="3.2">lançant</w> <w n="3.3">dans</w> <w n="3.4">le</w> <w n="3.5">Cirque</w> <w n="3.6">un</w> <w n="3.7">tas</w> <w n="3.8">de</w> <w n="3.9">chair</w> <w n="3.10">humaine</w>,</l>
							<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Criait</w>, <w n="4.2">de</w> <w n="4.3">son</w> <w n="4.4">trône</w> : « <w n="4.5">Aux</w> <w n="4.6">lions</w> !»</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Plus</w> <w n="5.2">tard</w>, <w n="5.3">quand</w>, <w n="5.4">poursuivant</w> <w n="5.5">sa</w> <w n="5.6">chimère</w> <w n="5.7">insensée</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Un</w> <w n="6.2">pape</w> <w n="6.3">suscitait</w> <w n="6.4">quelque</w> <w n="6.5">royal</w> <w n="6.6">boucher</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Afin</w> <w n="7.2">qu</w>’<w n="7.3">il</w> <w n="7.4">torturât</w> <w n="7.5">l</w>’<w n="7.6">immortelle</w> <w n="7.7">Pensée</w>,</l>
							<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Les</w> <w n="8.2">prêtres</w> <w n="8.3">criaient</w> : « <w n="8.4">Au</w> <w n="8.5">bûcher</w> !»</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Casqués</w>, <w n="9.2">mitrés</w>, <w n="9.3">hautains</w> <w n="9.4">sous</w> <w n="9.5">leurs</w> <w n="9.6">habits</w> <w n="9.7">sinistres</w>,</l>
							<l n="10" num="3.2"><w n="10.1">C</w>’<w n="10.2">est</w> <w n="10.3">ainsi</w> <w n="10.4">qu</w>’<w n="10.5">ils</w> <w n="10.6">trônaient</w>… <w n="10.7">au</w>-<w n="10.8">dessus</w> <w n="10.9">du</w> <w n="10.10">remords</w> !</l>
							<l n="11" num="3.3">— <w n="11.1">Un</w> <w n="11.2">Corse</w> <w n="11.3">vint</w> <w n="11.4">enfin</w>, <w n="11.5">César</w> <w n="11.6">dont</w> <w n="11.7">les</w> <w n="11.8">ministres</w></l>
							<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Furent</w> <w n="12.2">la</w> <w n="12.3">mitraille</w> <w n="12.4">et</w> <w n="12.5">la</w> <w n="12.6">mort</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Comme</w> <w n="13.2">un</w> <w n="13.3">arbre</w> <w n="13.4">géant</w> <w n="13.5">que</w> <w n="13.6">la</w> <w n="13.7">foudre</w> <w n="13.8">enveloppe</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">monde</w>, <w n="14.3">secoué</w> <w n="14.4">par</w> <w n="14.5">ses</w> <w n="14.6">coups</w> <w n="14.7">furieux</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Sembla</w> <w n="15.2">près</w> <w n="15.3">de</w> <w n="15.4">craquer</w> <w n="15.5">sous</w> <w n="15.6">l</w>’<w n="15.7">assaut</w>, <w n="15.8">et</w> <w n="15.9">l</w>’<w n="15.10">Europe</w></l>
							<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Mit</w> <w n="16.2">ce</w> <w n="16.3">Titan</w> <w n="16.4">au</w> <w n="16.5">rang</w> <w n="16.6">des</w> <w n="16.7">Dieux</w> !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Mais</w> <w n="17.2">toi</w>, <w n="17.3">sur</w> <w n="17.4">qui</w> <w n="17.5">jaillit</w> <w n="17.6">l</w>’<w n="17.7">écume</w> <w n="17.8">de</w> <w n="17.9">sa</w> <w n="17.10">gloire</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Quand</w> <w n="18.2">ta</w> <w n="18.3">main</w>, <w n="18.4">dans</w> <w n="18.5">la</w> <w n="18.6">nuit</w>, <w n="18.7">lâchement</w> <w n="18.8">nous</w> <w n="18.9">blessa</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Tu</w> <w n="19.2">n</w>’<w n="19.3">as</w> <w n="19.4">su</w> <w n="19.5">que</w> <w n="19.6">jeter</w>, <w n="19.7">du</w> <w n="19.8">fond</w> <w n="19.9">de</w> <w n="19.10">ta</w> <w n="19.11">victoire</w>,</l>
							<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Ce</w> <w n="20.2">cri</w> <w n="20.3">lugubre</w> : « <w n="20.4">A</w> <w n="20.5">Lambessa</w> !»</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">lorsque</w> <w n="21.3">ton</w> <w n="21.4">empire</w> <w n="21.5">ébranlé</w> <w n="21.6">tremble</w> <w n="21.7">et</w> <w n="21.8">croule</w>,</l>
							<l n="22" num="6.2">— <w n="22.1">Pour</w> <w n="22.2">que</w> <w n="22.3">le</w> <w n="22.4">peuple</w> <w n="22.5">encor</w> <w n="22.6">te</w> <w n="22.7">dresse</w> <w n="22.8">des</w> <w n="22.9">autels</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Pour</w> <w n="23.2">qu</w>’<w n="23.3">il</w> <w n="23.4">encense</w> <w n="23.5">encore</w> <w n="23.6">et</w> <w n="23.7">que</w> <w n="23.8">ton</w> <w n="23.9">pied</w> <w n="23.10">le</w> <w n="23.11">foule</w> —,</l>
							<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Ta</w> <w n="24.2">voix</w> <w n="24.3">morne</w> <w n="24.4">crie</w> : « <w n="24.5">Aux</w> <w n="24.6">rastells</w> !»</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">« <w n="25.1">La</w> <w n="25.2">Peur</w> <w n="25.3">est</w> <w n="25.4">un</w> <w n="25.5">bon</w> <w n="25.6">frein</w>, <w n="25.7">mais</w> <w n="25.8">la</w> <w n="25.9">honte</w> <w n="25.10">est</w> <w n="25.11">meilleure</w></l>
							<l n="26" num="7.2"><w n="26.1">Largesse</w> ! <w n="26.2">Au</w> <w n="26.3">râtelier</w> ! <w n="26.4">Tu</w> <w n="26.5">veux</w> <w n="26.6">me</w> <w n="26.7">fuir</w> <w n="26.8">en</w> <w n="26.9">vain</w> ;</l>
							<l n="27" num="7.3"><w n="27.1">Oublie</w> <w n="27.2">et</w> <w n="27.3">chante</w>, <w n="27.4">ô</w> <w n="27.5">serf</w> ! <w n="27.6">Gorge</w>-<w n="27.7">toi</w> <w n="27.8">pour</w> <w n="27.9">une</w> <w n="27.10">heure</w>…</l>
							<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Peuple</w>, <w n="28.2">chante</w>, <w n="28.3">et</w> <w n="28.4">soûle</w> <w n="28.5">ta</w> <w n="28.6">faim</w>,</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">« <w n="29.1">Pour</w> <w n="29.2">supporter</w> <w n="29.3">le</w> <w n="29.4">poids</w> <w n="29.5">du</w> <w n="29.6">sceptre</w> <w n="29.7">qui</w> <w n="29.8">te</w> <w n="29.9">pousse</w></l>
							<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">le</w> <w n="30.3">mors</w> <w n="30.4">redouté</w> <w n="30.5">qui</w> <w n="30.6">te</w> <w n="30.7">tient</w> <w n="30.8">le</w> <w n="30.9">front</w> <w n="30.10">bas</w> ;</l>
							<l n="31" num="8.3"><w n="31.1">Si</w> <w n="31.2">bien</w> <w n="31.3">que</w> <w n="31.4">nous</w> <w n="31.5">tombions</w> <w n="31.6">d</w>’<w n="31.7">une</w> <w n="31.8">même</w> <w n="31.9">secousse</w></l>
							<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Et</w> <w n="32.2">que</w> <w n="32.3">tu</w> <w n="32.4">n</w>’<w n="32.5">y</w> <w n="32.6">survives</w> <w n="32.7">pas</w>,</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1">« <w n="33.1">Ou</w> <w n="33.2">que</w> <w n="33.3">nous</w> <w n="33.4">avancions</w> <w n="33.5">d</w>’<w n="33.6">une</w> <w n="33.7">allure</w> <w n="33.8">certaine</w>,</l>
							<l n="34" num="9.2"><w n="34.1">Empereur</w> <w n="34.2">et</w> <w n="34.3">goujat</w>, <w n="34.4">vers</w> <w n="34.5">les</w> <w n="34.6">mêmes</w> <w n="34.7">destins</w>,</l>
							<l n="35" num="9.3"><w n="35.1">Malgré</w> <w n="35.2">les</w> <w n="35.3">pleurs</w> <w n="35.4">de</w> <w n="35.5">rage</w> <w n="35.6">impuissante</w> <w n="35.7">et</w> <w n="35.8">de</w> <w n="35.9">haine</w></l>
							<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Roulant</w> <w n="36.2">dans</w> <w n="36.3">tes</w> <w n="36.4">regards</w> <w n="36.5">éteints</w> !»</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="37" num="1.1"><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">pour</w> <w n="37.4">avoir</w> <w n="37.5">cherché</w> <w n="37.6">le</w> <w n="37.7">salut</w> <w n="37.8">de</w> <w n="37.9">ta</w> <w n="37.10">race</w></l>
							<l n="38" num="1.2"><w n="38.1">Dans</w> <w n="38.2">l</w>’<w n="38.3">avilissement</w> <w n="38.4">de</w> <w n="38.5">ce</w> <w n="38.6">peuple</w> <w n="38.7">égaré</w></l>
							<l n="39" num="1.3"><w n="39.1">Que</w> <w n="39.2">tu</w> <w n="39.3">seras</w>, <w n="39.4">tyran</w> <w n="39.5">dont</w> <w n="39.6">le</w> <w n="39.7">fouet</w> <w n="39.8">nous</w> <w n="39.9">harasse</w>,</l>
							<l n="40" num="1.4"><space unit="char" quantity="8"></space><w n="40.1">Éternellement</w> <w n="40.2">abhorré</w> !</l>
						</lg>
						<lg n="2">
							<l n="41" num="2.1"><w n="41.1">La</w> <w n="41.2">tyrannie</w> <w n="41.3">est</w> <w n="41.4">peu</w>, <w n="41.5">comparée</w> <w n="41.6">à</w> <w n="41.7">l</w>’<w n="41.8">insulte</w> !</l>
							<l n="42" num="2.2"><w n="42.1">Sous</w> <w n="42.2">d</w>’<w n="42.3">énormes</w> <w n="42.4">fardeaux</w> <w n="42.5">courber</w> <w n="42.6">un</w> <w n="42.7">peuple</w> <w n="42.8">entier</w> :</l>
							<l n="43" num="2.3"><w n="43.1">Faire</w> <w n="43.2">qu</w>’<w n="43.3">il</w> <w n="43.4">soit</w> <w n="43.5">ta</w> <w n="43.6">chose</w> <w n="43.7">et</w> <w n="43.8">qu</w>’<w n="43.9">il</w> <w n="43.10">te</w> <w n="43.11">rende</w> <w n="43.12">un</w> <w n="43.13">culte</w></l>
							<l n="44" num="2.4"><space unit="char" quantity="8"></space><w n="44.1">Certes</w> <w n="44.2">c</w>’<w n="44.3">est</w> <w n="44.4">un</w> <w n="44.5">lâche</w> <w n="44.6">métier</w> !</l>
						</lg>
						<lg n="3">
							<l n="45" num="3.1"><w n="45.1">Mais</w> <w n="45.2">prétendre</w>, <w n="45.3">ô</w> <w n="45.4">pitié</w> ! <w n="45.5">que</w> <w n="45.6">ce</w> <w n="45.7">peuple</w> <w n="45.8">t</w>’<w n="45.9">imite</w>,</l>
							<l n="46" num="3.2"><w n="46.1">Dans</w> <w n="46.2">ton</w> <w n="46.3">épais</w> <w n="46.4">bourbier</w> <w n="46.5">plongeant</w> <w n="46.6">éperdûment</w>…</l>
							<l n="47" num="3.3"><w n="47.1">Pour</w> <w n="47.2">toi</w>, <w n="47.3">c</w>’<w n="47.4">est</w> <w n="47.5">l</w>’<w n="47.6">infamie</w> <w n="47.7">insigne</w> <w n="47.8">et</w> <w n="47.9">sans</w> <w n="47.10">limite</w> ;</l>
							<l n="48" num="3.4"><space unit="char" quantity="8"></space><w n="48.1">Pour</w> <w n="48.2">lui</w>, <w n="48.3">le</w> <w n="48.4">dernier</w> <w n="48.5">châtiment</w> !</l>
						</lg>
						<lg n="4">
							<l n="49" num="4.1"><w n="49.1">Sous</w> <w n="49.2">ses</w> <w n="49.3">propres</w> <w n="49.4">mépris</w> <w n="49.5">qu</w>’<w n="49.6">il</w> <w n="49.7">s</w>’<w n="49.8">affaisse</w> <w n="49.9">et</w> <w n="49.10">qu</w>’<w n="49.11">il</w> <w n="49.12">gise</w> !</l>
							<l n="50" num="4.2"><w n="50.1">Que</w> <w n="50.2">la</w> <w n="50.3">sainte</w> <w n="50.4">Pudeur</w> <w n="50.5">le</w> <w n="50.6">flétrisse</w> <w n="50.7">à</w> <w n="50.8">jamais</w> !</l>
							<l n="51" num="4.3"><w n="51.1">Et</w> <w n="51.2">que</w> <w n="51.3">mon</w> <w n="51.4">cœur</w> <w n="51.5">éclate</w>, <w n="51.6">et</w> <w n="51.7">que</w> <w n="51.8">ma</w> <w n="51.9">voix</w> <w n="51.10">se</w> <w n="51.11">brise</w> :</l>
							<l n="52" num="4.4"><space unit="char" quantity="8"></space><w n="52.1">Car</w> <w n="52.2">c</w>’<w n="52.3">est</w> <w n="52.4">mon</w> <w n="52.5">peuple</w>… <w n="52.6">et</w> <w n="52.7">je</w> <w n="52.8">l</w>’<w n="52.9">aimais</w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>