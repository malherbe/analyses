<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’EMPIRE</head><div type="poem" key="FRK11">
					<head type="main">JACQUES BONHOMME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">JE</w> <w n="1.2">suis</w> <w n="1.3">le</w> <w n="1.4">Jacques</w> ! <w n="1.5">le</w> <w n="1.6">Bonhomme</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Celui</w> <w n="2.2">qu</w>’<w n="2.3">on</w> <w n="2.4">fouaille</w> <w n="2.5">et</w> <w n="2.6">dont</w> <w n="2.7">on</w> <w n="2.8">rit</w> !</l>
						<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">ainsi</w> <w n="3.4">qu</w>’<w n="3.5">en</w> <w n="3.6">France</w> <w n="3.7">on</w> <w n="3.8">me</w> <w n="3.9">nomme</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Lisez</w> <w n="4.2">l</w>’<w n="4.3">histoire</w>… <w n="4.4">C</w>’<w n="4.5">est</w> <w n="4.6">écrit</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1">Bourreaux</w> ! <w n="5.2">valets</w> ! <w n="5.3">quand</w> <w n="5.4">sur</w> <w n="5.5">ma</w> <w n="5.6">paille</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Tout</w> <w n="6.2">meurtri</w>, <w n="6.3">je</w> <w n="6.4">me</w> <w n="6.5">dresse</w> <w n="6.6">nu</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Trêve</w> <w n="7.2">de</w> <w n="7.3">vols</w> <w n="7.4">et</w> <w n="7.5">de</w> <w n="7.6">ripaille</w> :</l>
						<l n="8" num="2.4"><w n="8.1">Arrière</w> ! <w n="8.2">mon</w> <w n="8.3">jour</w> <w n="8.4">est</w> <w n="8.5">venu</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <w n="9.1">Car</w> <w n="9.2">la</w> <w n="9.3">Justice</w> <w n="9.4">est</w> <w n="9.5">la</w> <w n="9.6">loi</w> <w n="9.7">sainte</w></l>
						<l n="10" num="3.2"><w n="10.1">Dont</w> <w n="10.2">on</w> <w n="10.3">ne</w> <w n="10.4">se</w> <w n="10.5">délivre</w> <w n="10.6">pas</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Nul</w> <w n="11.2">pardon</w> <w n="11.3">n</w>’<w n="11.4">éteindrait</w> <w n="11.5">la</w> <w n="11.6">plainte</w></l>
						<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">sort</w> <w n="12.3">d</w>’<w n="12.4">un</w> <w n="12.5">passé</w> <w n="12.6">comme</w> <w n="12.7">un</w> <w n="12.8">glas</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« <w n="13.1">Ils</w> <w n="13.2">ont</w> <w n="13.3">fait</w> <w n="13.4">de</w> <w n="13.5">joyeuses</w> <w n="13.6">Pâques</w></l>
						<l n="14" num="4.2"><w n="14.1">Avec</w> <w n="14.2">l</w>’<w n="14.3">opprobre</w> <w n="14.4">et</w> <w n="14.5">les</w> <w n="14.6">chagrins</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Le</w> <w n="15.2">sang</w> <w n="15.3">et</w> <w n="15.4">les</w> <w n="15.5">larmes</w> <w n="15.6">de</w> <w n="15.7">Jacques</w> :</l>
						<l n="16" num="4.4"><w n="16.1">Voyez</w> <w n="16.2">encor</w> <w n="16.3">saigner</w> <w n="16.4">mes</w> <w n="16.5">reins</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« <w n="17.1">Ah</w> ! <w n="17.2">plus</w> <w n="17.3">de</w> <w n="17.4">sang</w> ! <w n="17.5">plus</w> <w n="17.6">de</w> <w n="17.7">torture</w> !</l>
						<l n="18" num="5.2"><w n="18.1">Jusqu</w>’<w n="18.2">au</w> <w n="18.3">jour</w> <w n="18.4">du</w> <w n="18.5">dernier</w> <w n="18.6">départ</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Des</w> <w n="19.2">dons</w> <w n="19.3">de</w> <w n="19.4">la</w> <w n="19.5">mère</w> <w n="19.6">Nature</w></l>
						<l n="20" num="5.4"><w n="20.1">Que</w> <w n="20.2">chacun</w> <w n="20.3">reçoive</w> <w n="20.4">sa</w> <w n="20.5">part</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« <w n="21.1">Mais</w> <w n="21.2">qu</w>’<w n="21.3">on</w> <w n="21.4">ravisse</w> <w n="21.5">aux</w> <w n="21.6">gens</w> <w n="21.7">de</w> <w n="21.8">fraude</w>.</l>
						<l n="22" num="6.2"><w n="22.1">En</w> <w n="22.2">les</w> <w n="22.3">chassant</w> <w n="22.4">du</w> <w n="22.5">pied</w> <w n="22.6">bien</w> <w n="22.7">loin</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Les</w> <w n="23.2">fruits</w> <w n="23.3">de</w> <w n="23.4">leur</w> <w n="23.5">basse</w> <w n="23.6">maraude</w></l>
						<l n="24" num="6.4"><w n="24.1">Pour</w> <w n="24.2">Jacque</w> <w n="24.3">affamé</w> <w n="24.4">dans</w> <w n="24.5">son</w> <w n="24.6">coin</w> !»</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Oui</w>, <w n="25.2">Peuple</w>, <w n="25.3">aux</w> <w n="25.4">artisans</w> <w n="25.5">de</w> <w n="25.6">crime</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Soûls</w> <w n="26.2">de</w> <w n="26.3">tes</w> <w n="26.4">pleurs</w> <w n="26.5">et</w> <w n="26.6">de</w> <w n="26.7">ton</w> <w n="26.8">bien</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Oppose</w> <w n="27.2">un</w> <w n="27.3">arrêt</w> <w n="27.4">magnanime</w> ;</l>
						<l n="28" num="7.4"><w n="28.1">Contre</w> <w n="28.2">ces</w> <w n="28.3">loups</w> <w n="28.4">ne</w> <w n="28.5">tente</w> <w n="28.6">rien</w>…</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Plus</w> <w n="29.2">de</w> <w n="29.3">sang</w> : <w n="29.4">le</w> <w n="29.5">leur</w> <w n="29.6">est</w> <w n="29.7">immonde</w> !</l>
						<l n="30" num="8.2"><w n="30.1">Qu</w>’<w n="30.2">ils</w> <w n="30.3">rendent</w> <w n="30.4">l</w>’<w n="30.5">or</w> <w n="30.6">et</w> <w n="30.7">la</w> <w n="30.8">moisson</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">qu</w>’<w n="31.3">ils</w> <w n="31.4">laissent</w> <w n="31.5">l</w>’<w n="31.6">âme</w> <w n="31.7">du</w> <w n="31.8">monde</w></l>
						<l n="32" num="8.4"><w n="32.1">Chanter</w> <w n="32.2">sa</w> <w n="32.3">sublime</w> <w n="32.4">chanson</w></l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Sur</w> <w n="33.2">le</w> <w n="33.3">lèvres</w> <w n="33.4">du</w> <w n="33.5">vieux</w> <w n="33.6">Misère</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Radieux</w> <w n="34.2">et</w> <w n="34.3">ressuscité</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Pour</w> <w n="35.2">que</w> <w n="35.3">tous</w> <w n="35.4">aient</w> <w n="35.5">le</w> <w n="35.6">nécessaire</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Travaillant</w> <w n="36.2">en</w> <w n="36.3">pleine</w> <w n="36.4">clarté</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Car</w> <w n="37.2">il</w> <w n="37.3">est</w> <w n="37.4">toujours</w> <w n="37.5">le</w> <w n="37.6">Bonhomme</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Et</w>, <w n="38.2">nourricier</w> <w n="38.3">du</w> <w n="38.4">genre</w> <w n="38.5">humain</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Héros</w> <w n="39.2">qui</w> <w n="39.3">fut</w> <w n="39.4">bête</w> <w n="39.5">de</w> <w n="39.6">somme</w>,</l>
						<l n="40" num="10.4"><w n="40.1">Il</w> <w n="40.2">ne</w> <w n="40.3">demande</w> <w n="40.4">que</w> <w n="40.5">du</w> <w n="40.6">pain</w> !</l>
					</lg>
				</div></body></text></TEI>