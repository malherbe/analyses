<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉPAVES</head><div type="poem" key="FRK24">
					<head type="main">AU BORD DU GOUFFRE</head>
					<opener>
						<salute>A Monsieur Bertrand Robidou</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">APRÈS</w> <w n="1.2">tant</w> <w n="1.3">d</w>’<w n="1.4">angoisse</w> <w n="1.5">et</w> <w n="1.6">d</w>’<w n="1.7">horreurs</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Après</w> <w n="2.2">ta</w> <w n="2.3">honte</w> <w n="2.4">immense</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Quel</w> <w n="3.2">stigmate</w>, <w n="3.3">ô</w> <w n="3.4">France</w>, <w n="3.5">et</w> <w n="3.6">quels</w> <w n="3.7">pleurs</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Faut</w>-<w n="4.2">il</w> <w n="4.3">à</w> <w n="4.4">ta</w> <w n="4.5">démence</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">O</w> <w n="5.2">corps</w> <w n="5.3">meurtri</w>, <w n="5.4">c</w>’<w n="5.5">était</w> <w n="5.6">assez</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1">Que</w> <w n="6.2">le</w> <w n="6.3">fléau</w> <w n="6.4">barbare</w></l>
						<l n="7" num="2.3"><w n="7.1">Vînt</w> <w n="7.2">briser</w> <w n="7.3">tes</w> <w n="7.4">membres</w> <w n="7.5">lassés</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="4"></space><w n="8.1">Comme</w> <w n="8.2">une</w> <w n="8.3">lourde</w> <w n="8.4">barre</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Qu</w>’<w n="9.2">un</w> <w n="9.3">goujat</w> <w n="9.4">sur</w> <w n="9.5">ton</w> <w n="9.6">front</w> <w n="9.7">puissant</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="4"></space><w n="10.1">Mît</w> <w n="10.2">sa</w> <w n="10.3">rude</w> <w n="10.4">semelle</w>,</l>
						<l n="11" num="3.3"><w n="11.1">France</w>, <w n="11.2">et</w> <w n="11.3">qu</w>’<w n="11.4">une</w> <w n="11.5">source</w> <w n="11.6">de</w> <w n="11.7">sang</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="4"></space><w n="12.1">Jaillît</w> <w n="12.2">de</w> <w n="12.3">ta</w> <w n="12.4">mamelle</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Tes</w> <w n="13.2">plus</w> <w n="13.3">héroïques</w> <w n="13.4">enfants</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="4"></space><w n="14.1">Râlaient</w> <w n="14.2">au</w> <w n="14.3">bord</w> <w n="14.4">des</w> <w n="14.5">routes</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Tu</w> <w n="15.2">n</w>’<w n="15.3">entendais</w> <w n="15.4">aux</w> <w n="15.5">quatre</w> <w n="15.6">vents</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="4"></space><w n="16.1">Qu</w>’<w n="16.2">un</w> <w n="16.3">long</w> <w n="16.4">bruit</w> <w n="16.5">de</w> <w n="16.6">déroutes</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Mais</w>, <w n="17.2">tandis</w> <w n="17.3">qu</w>’<w n="17.4">on</w> <w n="17.5">sonnait</w> <w n="17.6">ton</w> <w n="17.7">glas</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="4"></space><w n="18.1">La</w> <w n="18.2">Paix</w>, <w n="18.3">noble</w> <w n="18.4">ouvrière</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Pour</w> <w n="19.2">te</w> <w n="19.3">guérir</w> <w n="19.4">ouvrait</w> <w n="19.5">ses</w> <w n="19.6">bras</w>…</l>
						<l n="20" num="5.4"><space unit="char" quantity="4"></space><w n="20.1">Qui</w> <w n="20.2">donc</w> <w n="20.3">lui</w> <w n="20.4">crie</w> : —<w n="20.5">Arrière</w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Voilà</w>, <w n="21.2">pour</w> <w n="21.3">mieux</w> <w n="21.4">te</w> <w n="21.5">mordre</w> <w n="21.6">au</w> <w n="21.7">sein</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="4"></space><w n="22.1">Voilà</w> <w n="22.2">que</w> <w n="22.3">sur</w> <w n="22.4">nos</w> <w n="22.5">villes</w></l>
						<l n="23" num="6.3"><w n="23.1">S</w>’<w n="23.2">abat</w> <w n="23.3">le</w> <w n="23.4">génie</w> <w n="23.5">assassin</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="4"></space><w n="24.1">Des</w> <w n="24.2">batailles</w> <w n="24.3">civiles</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Est</w>-<w n="25.2">ce</w> <w n="25.3">là</w> <w n="25.4">ce</w> <w n="25.5">que</w> <w n="25.6">vous</w> <w n="25.7">rêviez</w></l>
						<l n="26" num="7.2"><space unit="char" quantity="4"></space><w n="26.1">Pour</w> <w n="26.2">victoires</w> <w n="26.3">suprêmes</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Tristes</w> <w n="27.2">frères</w>, <w n="27.3">ô</w> <w n="27.4">loups</w> <w n="27.5">cerviers</w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="4"></space><w n="28.1">Qui</w> <w n="28.2">vous</w> <w n="28.3">frappez</w> <w n="28.4">vous</w>-<w n="28.5">mêmes</w> ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Le</w> <w n="29.2">saint</w> <w n="29.3">travail</w> <w n="29.4">allait</w> <w n="29.5">sortir</w></l>
						<l n="30" num="8.2"><space unit="char" quantity="4"></space><w n="30.1">Du</w> <w n="30.2">fond</w> <w n="30.3">de</w> <w n="30.4">nos</w> <w n="30.5">détresses</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">Tu</w> <w n="31.2">l</w>’<w n="31.3">as</w> <w n="31.4">perdu</w>, <w n="31.5">Peuple</w> <w n="31.6">martyr</w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="4"></space><w n="32.1">Comme</w> <w n="32.2">tes</w> <w n="32.3">forteresses</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">l</w>’<w n="33.3">Étranger</w> — <w n="33.4">de</w> <w n="33.5">son</w> <w n="33.6">coin</w> <w n="33.7">noir</w> —</l>
						<l n="34" num="9.2"><space unit="char" quantity="4"></space><w n="34.1">Dit</w>, <w n="34.2">haussant</w> <w n="34.3">les</w> <w n="34.4">épaules</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Le</w> <w n="35.2">cœur</w> <w n="35.3">gonflé</w> <w n="35.4">d</w>’<w n="35.5">un</w> <w n="35.6">sombre</w> <w n="35.7">espoir</w> :</l>
						<l n="36" num="9.4"><space unit="char" quantity="4"></space>« <w n="36.1">Coule</w>, <w n="36.2">ô</w> <w n="36.3">sève</w> <w n="36.4">des</w> <w n="36.5">Gaules</w> !»</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Ah</w> ! <w n="37.2">vous</w> <w n="37.3">dont</w> <w n="37.4">l</w>’<w n="37.5">ennemi</w> <w n="37.6">commun</w></l>
						<l n="38" num="10.2"><space unit="char" quantity="4"></space><w n="38.1">Voudrait</w> <w n="38.2">tarir</w> <w n="38.3">les</w> <w n="38.4">veines</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Pour</w> <w n="39.2">le</w> <w n="39.3">Vandale</w> <w n="39.4">et</w> <w n="39.5">pour</w> <w n="39.6">le</w> <w n="39.7">Hun</w></l>
						<l n="40" num="10.4"><space unit="char" quantity="4"></space><w n="40.1">Gardez</w> <w n="40.2">toutes</w> <w n="40.3">vos</w> <w n="40.4">haines</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Rentrez</w>, <w n="41.2">corbeaux</w>, <w n="41.3">dans</w> <w n="41.4">votre</w> <w n="41.5">nuit</w>,</l>
						<l n="42" num="11.2"><space unit="char" quantity="4"></space><w n="42.1">Bourreaux</w>, <w n="42.2">dans</w> <w n="42.3">votre</w> <w n="42.4">bouge</w> !</l>
						<l n="43" num="11.3"><w n="43.1">La</w> <w n="43.2">vie</w> <w n="43.3">écumante</w> <w n="43.4">s</w>’<w n="43.5">enfuit</w> :</l>
						<l n="44" num="11.4"><space unit="char" quantity="4"></space><w n="44.1">Arrêtez</w> <w n="44.2">ce</w> <w n="44.3">flot</w> <w n="44.4">rouge</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Sinon</w>… <w n="45.2">Patrie</w>, <w n="45.3">adieu</w> ! <w n="45.4">Je</w> <w n="45.5">vois</w>,</l>
						<l n="46" num="12.2"><space unit="char" quantity="4"></space><w n="46.1">Dans</w> <w n="46.2">l</w>’<w n="46.3">agonie</w> <w n="46.4">amère</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Je</w> <w n="47.2">vois</w> <w n="47.3">une</w> <w n="47.4">dernière</w> <w n="47.5">fois</w></l>
						<l n="48" num="12.4"><space unit="char" quantity="4"></space><w n="48.1">S</w>’<w n="48.2">agiter</w>, <w n="48.3">ô</w> <w n="48.4">ma</w> <w n="48.5">mère</w>,</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Tes</w> <w n="49.2">membres</w> <w n="49.3">nus</w> <w n="49.4">et</w> <w n="49.5">palpitants</w></l>
						<l n="50" num="13.2"><space unit="char" quantity="4"></space><w n="50.1">Et</w> <w n="50.2">traînés</w> <w n="50.3">sur</w> <w n="50.4">la</w> <w n="50.5">claie</w> ! —</l>
						<l n="51" num="13.3"><w n="51.1">La</w> <w n="51.2">France</w> <w n="51.3">est</w> <w n="51.4">morte</w>, <w n="51.5">ayant</w> <w n="51.6">longtemps</w></l>
						<l n="52" num="13.4"><space unit="char" quantity="4"></space><w n="52.1">Saigné</w> <w n="52.2">par</w> <w n="52.3">chaque</w> <w n="52.4">plaie</w>…</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Morte</w> ! — <w n="53.2">Le</w> <w n="53.3">boucher</w> <w n="53.4">sur</w> <w n="53.5">l</w>’<w n="53.6">étal</w></l>
						<l n="54" num="14.2"><space unit="char" quantity="4"></space><w n="54.1">La</w> <w n="54.2">vend</w> <w n="54.3">et</w> <w n="54.4">la</w> <w n="54.5">dépèce</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Criant</w> : « <w n="55.2">Voici</w> <w n="55.3">le</w> <w n="55.4">jour</w> <w n="55.5">fatal</w> :</l>
						<l n="56" num="14.4"><space unit="char" quantity="4"></space><w n="56.1">Que</w> <w n="56.2">chacun</w> <w n="56.3">s</w>’<w n="56.4">en</w> <w n="56.5">repaisse</w> !</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">« <w n="57.1">Que</w> <w n="57.2">les</w> <w n="57.3">peuples</w> <w n="57.4">fassent</w> <w n="57.5">leurs</w> <w n="57.6">parts</w></l>
						<l n="58" num="15.2"><space unit="char" quantity="4"></space><w n="58.1">De</w> <w n="58.2">tout</w> <w n="58.3">ce</w> <w n="58.4">qui</w> <w n="58.5">fut</w> <w n="58.6">Elle</w>…</l>
						<l n="59" num="15.3"><w n="59.1">Défendez</w>-<w n="59.2">vous</w>, <w n="59.3">lambeaux</w> <w n="59.4">épars</w> !</l>
						<l n="60" num="15.4"><space unit="char" quantity="4"></space><w n="60.1">Salut</w>, <w n="60.2">France</w> <w n="60.3">immortelle</w> !»</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Que</w> <w n="61.2">pourraient</w> <w n="61.3">alors</w> <w n="61.4">nos</w> <w n="61.5">sanglots</w></l>
						<l n="62" num="16.2"><space unit="char" quantity="4"></space><w n="62.1">Contre</w> <w n="62.2">la</w> <w n="62.3">meute</w> <w n="62.4">infâme</w> ?—</l>
						<l n="63" num="16.3"><w n="63.1">Avant</w> <w n="63.2">que</w> <w n="63.3">les</w> <w n="63.4">destins</w> <w n="63.5">soient</w> <w n="63.6">clos</w>,</l>
						<l n="64" num="16.4"><space unit="char" quantity="4"></space><w n="64.1">Mets</w> <w n="64.2">ta</w> <w n="64.3">main</w> <w n="64.4">sur</w> <w n="64.5">ton</w> <w n="64.6">âme</w> !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Et</w>, <w n="65.2">ramassant</w> <w n="65.3">ton</w> <w n="65.4">seul</w> <w n="65.5">drapeau</w>,</l>
						<l n="66" num="17.2"><space unit="char" quantity="4"></space><w n="66.1">Bondis</w>, <w n="66.2">libre</w>, <w n="66.3">une</w> <w n="66.4">et</w> <w n="66.5">fière</w>,</l>
						<l n="67" num="17.3"><w n="67.1">Comme</w> <w n="67.2">aux</w> <w n="67.3">grands</w> <w n="67.4">jours</w> <w n="67.5">de</w> <w n="67.6">Mirabeau</w>,</l>
						<l n="68" num="17.4"><space unit="char" quantity="4"></space><w n="68.1">Le</w> <w n="68.2">front</w> <w n="68.3">dans</w> <w n="68.4">la</w> <w n="68.5">lumière</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">Rennes, 4 mai 1871.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>