<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’INVASION</head><div type="poem" key="FRK14">
					<head type="main">DEBOUT, FRANCE !</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">DEBOUT</w>, <w n="1.2">France</w> ! <w n="1.3">la</w> <w n="1.4">tyrannie</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">mit</w> <w n="2.3">la</w> <w n="2.4">main</w> <w n="2.5">sur</w> <w n="2.6">ton</w> <w n="2.7">génie</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">Croyait</w>, <w n="3.2">en</w> <w n="3.3">l</w>’<w n="3.4">étouffant</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Couronner</w> <w n="4.2">sa</w> <w n="4.3">lâche</w> <w n="4.4">aventure</w> :</l>
						<l n="5" num="1.5"><w n="5.1">Mais</w> <w n="5.2">l</w>’<w n="5.3">ours</w> <w n="5.4">du</w> <w n="5.5">Nord</w> <w n="5.6">veut</w> <w n="5.7">sa</w> <w n="5.8">pâture</w>…</l>
						<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">En</w> <w n="6.2">avant</w> ! <w n="6.3">en</w> <w n="6.4">avant</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Peuple</w> <w n="7.2">des</w> <w n="7.3">champs</w>, <w n="7.4">Peuple</w> <w n="7.5">des</w> <w n="7.6">villes</w></l>
						<l n="8" num="2.2"><w n="8.1">Réduit</w> <w n="8.2">aux</w> <w n="8.3">besognes</w> <w n="8.4">serviles</w>,</l>
						<l n="9" num="2.3"><space unit="char" quantity="4"></space><w n="9.1">O</w> <w n="9.2">pauvre</w> <w n="9.3">Peuple</w> <w n="9.4">enfant</w>,</l>
						<l n="10" num="2.4"><w n="10.1">Debout</w> ! <w n="10.2">dans</w> <w n="10.3">ta</w> <w n="10.4">propre</w> <w n="10.5">détresse</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Que</w> <w n="11.2">ton</w> <w n="11.3">bras</w> <w n="11.4">robuste</w> <w n="11.5">se</w> <w n="11.6">dresse</w> :</l>
						<l n="12" num="2.6"><space unit="char" quantity="4"></space><w n="12.1">En</w> <w n="12.2">avant</w> ! <w n="12.3">en</w> <w n="12.4">avant</w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Il</w> <w n="13.2">faut</w> <w n="13.3">que</w> <w n="13.4">ton</w> <w n="13.5">âme</w> <w n="13.6">renaisse</w> !</l>
						<l n="14" num="3.2"><w n="14.1">O</w> <w n="14.2">suprême</w> <w n="14.3">espoir</w>, <w n="14.4">ô</w> <w n="14.5">Jeunesse</w>,</l>
						<l n="15" num="3.3"><space unit="char" quantity="4"></space><w n="15.1">Brise</w> <w n="15.2">un</w> <w n="15.3">joug</w> <w n="15.4">énervant</w> :</l>
						<l n="16" num="3.4"><w n="16.1">D</w>’<w n="16.2">une</w> <w n="16.3">lie</w> <w n="16.4">impure</w> <w n="16.5">enivrée</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Quoi</w> ! <w n="17.2">tu</w> <w n="17.3">garderais</w> <w n="17.4">ta</w> <w n="17.5">livrée</w> ?…</l>
						<l n="18" num="3.6"><space unit="char" quantity="4"></space><w n="18.1">En</w> <w n="18.2">avant</w> ! <w n="18.3">en</w> <w n="18.4">avant</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">O</w> <w n="19.2">nos</w> <w n="19.3">aînés</w>, <w n="19.4">ô</w> <w n="19.5">nos</w> <w n="19.6">exemples</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Vous</w> <w n="20.2">qui</w> <w n="20.3">mériteriez</w> <w n="20.4">des</w> <w n="20.5">temples</w></l>
						<l n="21" num="4.3"><space unit="char" quantity="4"></space><w n="21.1">Et</w> <w n="21.2">dont</w> <w n="21.3">on</w> <w n="21.4">va</w> <w n="21.5">rêvant</w>,</l>
						<l n="22" num="4.4"><w n="22.1">Géants</w> <w n="22.2">battus</w> <w n="22.3">par</w> <w n="22.4">vingt</w> <w n="22.5">orages</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Emplissez</w>-<w n="23.2">nous</w> <w n="23.3">de</w> <w n="23.4">vos</w> <w n="23.5">courages</w> !</l>
						<l n="24" num="4.6"><space unit="char" quantity="4"></space><w n="24.1">En</w> <w n="24.2">avant</w> ! <w n="24.3">en</w> <w n="24.4">avant</w> !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">O</w> <w n="25.2">chercheurs</w> <w n="25.3">de</w> <w n="25.4">philosophie</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Debout</w> ! <w n="26.2">l</w>’<w n="26.3">Impudeur</w> <w n="26.4">vous</w> <w n="26.5">défie</w> :</l>
						<l n="27" num="5.3"><space unit="char" quantity="4"></space><w n="27.1">Trouvez</w> <w n="27.2">un</w> <w n="27.3">mot</w> <w n="27.4">vivant</w></l>
						<l n="28" num="5.4"><w n="28.1">Qui</w> <w n="28.2">fasse</w> <w n="28.3">un</w> <w n="28.4">héros</w> <w n="28.5">d</w>’<w n="28.6">une</w> <w n="28.7">foule</w> !</l>
						<l n="29" num="5.5"><w n="29.1">La</w> <w n="29.2">vie</w> <w n="29.3">à</w> <w n="29.4">flots</w> <w n="29.5">pressés</w> <w n="29.6">s</w>’<w n="29.7">écoule</w>…</l>
						<l n="30" num="5.6"><space unit="char" quantity="4"></space><w n="30.1">En</w> <w n="30.2">avant</w> ! <w n="30.3">en</w> <w n="30.4">avant</w> !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Vous</w> <w n="31.2">qu</w>’<w n="31.3">on</w> <w n="31.4">raille</w>, <w n="31.5">ô</w> <w n="31.6">Joueurs</w> <w n="31.7">de</w> <w n="31.8">flûte</w>,</l>
						<l n="32" num="6.2"><w n="32.1">Debout</w> ! <w n="32.2">Nous</w> <w n="32.3">vaincrons</w> <w n="32.4">dans</w> <w n="32.5">la</w> <w n="32.6">lutte</w>,</l>
						<l n="33" num="6.3"><space unit="char" quantity="4"></space><w n="33.1">Maîtres</w>, <w n="33.2">en</w> <w n="33.3">vous</w> <w n="33.4">suivant</w>,</l>
						<l n="34" num="6.4"><w n="34.1">Comme</w> <w n="34.2">les</w> <w n="34.3">Grecs</w> <w n="34.4">suivaient</w> <w n="34.5">Tyrtée</w></l>
						<l n="35" num="6.5"><w n="35.1">Armé</w> <w n="35.2">de</w> <w n="35.3">sa</w> <w n="35.4">strophe</w> <w n="35.5">irritée</w> :</l>
						<l n="36" num="6.6"><space unit="char" quantity="4"></space><w n="36.1">En</w> <w n="36.2">avant</w> ! <w n="36.3">en</w> <w n="36.4">avant</w> !</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">Ah</w> ! <w n="37.2">puisse</w> <w n="37.3">une</w> <w n="37.4">autre</w> <w n="37.5">France</w> <w n="37.6">éclore</w> !</l>
						<l n="38" num="7.2"><w n="38.1">Et</w>, <w n="38.2">saluant</w> <w n="38.3">la</w> <w n="38.4">grande</w> <w n="38.5">aurore</w></l>
						<l n="39" num="7.3"><space unit="char" quantity="4"></space><w n="39.1">D</w>’<w n="39.2">un</w> <w n="39.3">hymne</w> <w n="39.4">triomphant</w>,</l>
						<l n="40" num="7.4"><w n="40.1">Que</w> <w n="40.2">notre</w> <w n="40.3">libre</w> <w n="40.4">voix</w> <w n="40.5">répète</w></l>
						<l n="41" num="7.5"><w n="41.1">Le</w> <w n="41.2">refrain</w> <w n="41.3">des</w> <w n="41.4">jours</w> <w n="41.5">de</w> <w n="41.6">tempête</w> :</l>
						<l n="42" num="7.6"><space unit="char" quantity="4"></space><w n="42.1">En</w> <w n="42.2">avant</w> ! <w n="42.3">en</w> <w n="42.4">avant</w> !…</l>
					</lg>
				</div></body></text></TEI>