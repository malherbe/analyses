<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’EMPIRE</head><div type="poem" key="FRK3">
					<head type="sub">A CEUX QUI PARLENT D’OUBLI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">je</w> <w n="1.3">n</w>’<w n="1.4">ai</w> <w n="1.5">jamais</w> <w n="1.6">caché</w> <w n="1.7">ma</w> <w n="1.8">haine</w> <w n="1.9">intense</w></l>
						<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">ce</w> <w n="2.3">faux</w> <w n="2.4">Bonaparte</w>, <w n="2.5">ombre</w> <w n="2.6">de</w> <w n="2.7">Verhuell</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Ni</w> <w n="3.2">parmi</w> <w n="3.3">ses</w> <w n="3.4">valets</w> <w n="3.5">chanté</w> <w n="3.6">la</w> <w n="3.7">Reine</w> <w n="3.8">Hortense</w>,</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">Pour</w> <w n="4.2">le</w> <w n="4.3">honnir</w> <w n="4.4">à</w> <w n="4.5">l</w>’<w n="4.6">heure</w> <w n="4.7">où</w> <w n="4.8">se</w> <w n="4.9">couvrait</w> <w n="4.10">le</w> <w n="4.11">ciel</w>,</l>
						<l n="5" num="2.2"><w n="5.1">Je</w> <w n="5.2">puis</w> <w n="5.3">jeter</w> <w n="5.4">au</w> <w n="5.5">vent</w> <w n="5.6">ces</w> <w n="5.7">hymnes</w> <w n="5.8">de</w> <w n="5.9">colère</w>,</l>
						<l n="6" num="2.3"><w n="6.1">Nés</w> <w n="6.2">de</w> <w n="6.3">l</w>’<w n="6.4">âpre</w> <w n="6.5">rancœur</w> <w n="6.6">de</w> <w n="6.7">plus</w> <w n="6.8">d</w>’<w n="6.9">un</w> <w n="6.10">jour</w> <w n="6.11">cruel</w> !</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">Car</w>, <w n="7.2">bien</w> <w n="7.3">qu</w>’<w n="7.4">on</w> <w n="7.5">ait</w> <w n="7.6">brisé</w> <w n="7.7">sa</w> <w n="7.8">couronne</w>, <w n="7.9">et</w> <w n="7.10">qu</w>’<w n="7.11">il</w> <w n="7.12">erre</w></l>
						<l n="8" num="3.2"><w n="8.1">Loin</w> <w n="8.2">de</w> <w n="8.3">nos</w> <w n="8.4">vieux</w> <w n="8.5">palais</w>, <w n="8.6">spectre</w> <w n="8.7">inerte</w> <w n="8.8">et</w> <w n="8.9">courbé</w>,</l>
						<l n="9" num="3.3"><w n="9.1">Rien</w> <w n="9.2">ne</w> <w n="9.3">doit</w> <w n="9.4">le</w> <w n="9.5">sauver</w> <w n="9.6">du</w> <w n="9.7">mépris</w> <w n="9.8">séculaire</w> :</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">Pour</w> <w n="10.2">lui</w> <w n="10.3">frayer</w> <w n="10.4">la</w> <w n="10.5">route</w>, <w n="10.6">à</w> <w n="10.7">ce</w> <w n="10.8">bourreau</w> <w n="10.9">tombé</w>,</l>
						<l n="11" num="4.2"><w n="11.1">Que</w> <w n="11.2">de</w> <w n="11.3">grands</w> <w n="11.4">cœurs</w> <w n="11.5">sont</w> <w n="11.6">morts</w> <w n="11.7">dans</w> <w n="11.8">la</w> <w n="11.9">bataille</w> <w n="11.10">ardente</w></l>
						<l n="12" num="4.3"><w n="12.1">Dans</w> <w n="12.2">l</w>’<w n="12.3">horreur</w> <w n="12.4">des</w> <w n="12.5">cachots</w> <w n="12.6">combien</w> <w n="12.7">ont</w> <w n="12.8">succombé</w> !</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1"><w n="13.1">Sait</w>-<w n="13.2">on</w> <w n="13.3">quel</w> <w n="13.4">long</w> <w n="13.5">troupeau</w> <w n="13.6">de</w> <w n="13.7">fantômes</w> <w n="13.8">le</w> <w n="13.9">hante</w>,</l>
						<l n="14" num="5.2"><w n="14.1">Lorsqu</w>’<w n="14.2">il</w> <w n="14.3">rêve</w>, <w n="14.4">le</w> <w n="14.5">soir</w>, <w n="14.6">à</w> <w n="14.7">son</w> <w n="14.8">passé</w> <w n="14.9">hideux</w>,</l>
						<l n="15" num="5.3"><w n="15.1">Cauchemar</w> <w n="15.2">où</w> <w n="15.3">Callot</w> <w n="15.4">fait</w> <w n="15.5">reculer</w> <w n="15.6">le</w> <w n="15.7">Dante</w> !</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1"><w n="16.1">Ceux</w> <w n="16.2">qui</w> <w n="16.3">parlent</w> <w n="16.4">d</w>’<w n="16.5">oubli</w> <w n="16.6">n</w>’<w n="16.7">ont</w> <w n="16.8">ils</w> <w n="16.9">pas</w> <w n="16.10">vu</w> <w n="16.11">près</w> <w n="16.12">d</w>’<w n="16.13">eux</w></l>
						<l n="17" num="6.2"><w n="17.1">Se</w> <w n="17.2">dresser</w> <w n="17.3">les</w> <w n="17.4">martyrs</w> <w n="17.5">de</w> <w n="17.6">vingt</w> <w n="17.7">ans</w> <w n="17.8">de</w> <w n="17.9">souffrance</w> ?</l>
						<l n="18" num="6.3"><w n="18.1">Décembre</w> <w n="18.2">est</w>-<w n="18.3">il</w> <w n="18.4">si</w> <w n="18.5">loin</w> <w n="18.6">qu</w>’<w n="18.7">on</w> <w n="18.8">détourne</w> <w n="18.9">les</w> <w n="18.10">yeux</w> ?</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1"><w n="19.1">Ah</w> ! <w n="19.2">la</w> <w n="19.3">pitié</w> <w n="19.4">pour</w> <w n="19.5">lui</w> <w n="19.6">serait</w> <w n="19.7">une</w> <w n="19.8">espérance</w> ;</l>
						<l n="20" num="7.2"><w n="20.1">Décembre</w> <w n="20.2">reviendrait</w> <w n="20.3">avec</w> <w n="20.4">l</w>’<w n="20.5">homme</w> <w n="20.6">fatal</w>…</l>
						<l n="21" num="7.3"><w n="21.1">Qu</w>’<w n="21.2">il</w> <w n="21.3">soit</w> <w n="21.4">nommé</w> <w n="21.5">partout</w> <w n="21.6">l</w>’<w n="21.7">assassin</w> <w n="21.8">de</w> <w n="21.9">la</w> <w n="21.10">France</w> !</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1">— <w n="22.1">Pourtant</w>, <w n="22.2">lorsque</w> <w n="22.3">j</w>’<w n="22.4">ai</w> <w n="22.5">vu</w> <w n="22.6">crouler</w> <w n="22.7">son</w> <w n="22.8">piédestal</w>,</l>
						<l n="23" num="8.2"><w n="23.1">Par</w> <w n="23.2">la</w> <w n="23.3">fraude</w> <w n="23.4">et</w> <w n="23.5">le</w> <w n="23.6">meurtre</w> <w n="23.7">édifié</w> <w n="23.8">naguère</w>,</l>
						<l n="24" num="8.3"><w n="24.1">J</w>’<w n="24.2">aurais</w> <w n="24.3">dit</w> : « <w n="24.4">Oublions</w> <w n="24.5">cet</w> <w n="24.6">ouvrier</w> <w n="24.7">du</w> <w n="24.8">mal</w></l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1"><w n="25.1">Et</w> <w n="25.2">sa</w> <w n="25.3">pourpre</w> <w n="25.4">d</w>’<w n="25.5">emprunt</w> <w n="25.6">que</w> <w n="25.7">le</w> <w n="25.8">Destin</w> <w n="25.9">lacère</w>» ;</l>
						<l n="26" num="9.2"><w n="26.1">Et</w> <w n="26.2">toi</w>, <w n="26.3">qui</w> <w n="26.4">l</w>’<w n="26.5">attendais</w>, <w n="26.6">Némésis</w> <w n="26.7">aux</w> <w n="26.8">aguets</w>,</l>
						<l n="27" num="9.3"><w n="27.1">Tu</w> <w n="27.2">salûrais</w> <w n="27.3">sa</w> <w n="27.4">fin</w>, — <w n="27.5">et</w>, <w n="27.6">pensive</w> <w n="27.7">et</w> <w n="27.8">ta</w> <w n="27.9">sphère</w>,</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1"><w n="28.1">Tu</w> <w n="28.2">faiblirais</w>, <w n="28.3">Justice</w>, <w n="28.4">ô</w> <w n="28.5">toi</w> <w n="28.6">que</w> <w n="28.7">j</w>’<w n="28.8">invoquais</w>,</l>
						<l n="29" num="10.2"><w n="29.1">S</w>’<w n="29.2">il</w> <w n="29.3">avait</w> <w n="29.4">eu</w>, <w n="29.5">farouche</w> <w n="29.6">et</w> <w n="29.7">traqué</w> <w n="29.8">par</w> <w n="29.9">la</w> <w n="29.10">guerre</w>,</l>
						<l n="30" num="10.3"><w n="30.1">La</w> <w n="30.2">chute</w> <w n="30.3">d</w>’<w n="30.4">un</w> <w n="30.5">soldat</w>, <w n="30.6">non</w> <w n="30.7">celle</w> <w n="30.8">d</w>’<w n="30.9">un</w> <w n="30.10">laquais</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">1871.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>