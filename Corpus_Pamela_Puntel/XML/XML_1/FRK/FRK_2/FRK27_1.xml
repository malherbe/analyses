<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉPAVES</head><div type="poem" key="FRK27">
					<head type="main">LA REVANCHE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">OH</w> ! <w n="1.2">oui</w>, <w n="1.3">nous</w> <w n="1.4">la</w> <w n="1.5">prendrons</w>, <w n="1.6">la</w> <w n="1.7">revanche</w> <w n="1.8">attendue</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">notre</w> <w n="2.3">génie</w> <w n="2.4">à</w> <w n="2.5">tous</w> <w n="2.6">les</w> <w n="2.7">vents</w> <w n="2.8">jeté</w>,</l>
						<l n="3" num="1.3"><w n="3.1">A</w> <w n="3.2">l</w>’<w n="3.3">infâme</w> <w n="3.4">rapine</w>, <w n="3.5">à</w> <w n="3.6">la</w> <w n="3.7">force</w> <w n="3.8">qui</w> <w n="3.9">tue</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Oui</w>, <w n="4.2">nous</w> <w n="4.3">arracherons</w> <w n="4.4">le</w> <w n="4.5">monde</w> <w n="4.6">ensanglanté</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Oui</w>, <w n="5.2">dans</w> <w n="5.3">la</w> <w n="5.4">foule</w> <w n="5.5">humaine</w> — <w n="5.6">où</w> <w n="5.7">les</w> <w n="5.8">rois</w> <w n="5.9">font</w> <w n="5.10">des</w> <w n="5.11">vides</w> —</l>
						<l n="6" num="2.2"><w n="6.1">Notre</w> <w n="6.2">invincible</w> <w n="6.3">esprit</w> <w n="6.4">s</w>’<w n="6.5">épandra</w> <w n="6.6">largement</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">l</w>’<w n="7.3">on</w> <w n="7.4">verra</w> <w n="7.5">flotter</w> <w n="7.6">nos</w> <w n="7.7">étendards</w> <w n="7.8">splendides</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Comme</w> <w n="8.2">un</w> <w n="8.3">signal</w> <w n="8.4">de</w> <w n="8.5">joie</w> <w n="8.6">et</w> <w n="8.7">d</w>’<w n="8.8">affranchissement</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Quand</w> <w n="9.2">les</w> <w n="9.3">peuples</w> <w n="9.4">meurtris</w>, <w n="9.5">en</w> <w n="9.6">criant</w> : « <w n="9.7">Délivrance</w> !»</l>
						<l n="10" num="3.2"><w n="10.1">Regarderont</w> <w n="10.2">le</w> <w n="10.3">Ciel</w>, <w n="10.4">qui</w> <w n="10.5">ne</w> <w n="10.6">les</w> <w n="10.7">venge</w> <w n="10.8">pas</w>,</l>
						<l n="11" num="3.3"><w n="11.1">A</w> <w n="11.2">leurs</w> <w n="11.3">mâles</w> <w n="11.4">accents</w> <w n="11.5">l</w>’<w n="11.6">écho</w> <w n="11.7">répondra</w> : « <w n="11.8">France</w> !»</l>
						<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">les</w> <w n="12.3">bourreaux</w> — <w n="12.4">craintifs</w> — <w n="12.5">diront</w>, <w n="12.6">parlant</w> <w n="12.7">tout</w> <w n="12.8">bas</w> :</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« <w n="13.1">Oh</w> ! <w n="13.2">comment</w> <w n="13.3">arrêter</w> <w n="13.4">ce</w> <w n="13.5">cri</w>, <w n="13.6">ce</w> <w n="13.7">nom</w> <w n="13.8">qui</w> <w n="13.9">passe</w> ?…»</l>
						<l n="14" num="4.2"><w n="14.1">Et</w>, <w n="14.2">sachant</w> <w n="14.3">ce</w> <w n="14.4">qu</w>’<w n="14.5">il</w> <w n="14.6">vaut</w>, <w n="14.7">et</w> <w n="14.8">comptant</w> <w n="14.9">les</w> <w n="14.10">délais</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Ils</w> <w n="15.2">sentiront</w> <w n="15.3">glisser</w> <w n="15.4">de</w> <w n="15.5">leur</w> <w n="15.6">main</w> <w n="15.7">qui</w> <w n="15.8">se</w> <w n="15.9">lasse</w></l>
						<l n="16" num="4.4"><w n="16.1">La</w> <w n="16.2">chaîne</w> <w n="16.3">aux</w> <w n="16.4">lourds</w> <w n="16.5">anneaux</w> <w n="16.6">partant</w> <w n="16.7">de</w> <w n="16.8">leurs</w> <w n="16.9">palais</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">—« <w n="17.1">Oh</w> ! <w n="17.2">qui</w> <w n="17.3">donc</w> <w n="17.4">pourra</w> <w n="17.5">mettre</w> <w n="17.6">un</w> <w n="17.7">carcan</w> <w n="17.8">aux</w> <w n="17.9">Doctrines</w> ?»</l>
						<l n="18" num="5.2"><w n="18.1">Personne</w> ! <w n="18.2">Place</w> <w n="18.3">au</w> <w n="18.4">Droit</w> ! <w n="18.5">car</w> <w n="18.6">les</w> <w n="18.7">destins</w> <w n="18.8">sont</w> <w n="18.9">mûrs</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">rien</w> <w n="19.3">qu</w>’<w n="19.4">avec</w> <w n="19.5">le</w> <w n="19.6">cri</w> <w n="19.7">sorti</w> <w n="19.8">de</w> <w n="19.9">nos</w> <w n="19.10">poitrines</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Du</w> <w n="20.2">vieux</w> <w n="20.3">monde</w> <w n="20.4">sapé</w> <w n="20.5">nous</w> <w n="20.6">abattrons</w> <w n="20.7">les</w> <w n="20.8">murs</w> !</l>
					</lg>
				</div></body></text></TEI>