<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA HORDE ALLEMANDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>136 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LA HORDE ALLEMANDE</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61238688.r=FRANK%20F.%2C%20%20LA%20HORDE%20ALLEMANDE%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA HORDE ALLEMANDE</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALPHONSE LEMERRE</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-24" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRK1">
				<head type="main">LA HORDE ALLEMANDE</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Entendez</w>-<w n="1.2">vous</w> <w n="1.3">le</w> <w n="1.4">bruit</w> <w n="1.5">de</w> <w n="1.6">la</w> <w n="1.7">Horde</w> <w n="1.8">allemande</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">se</w> <w n="2.3">rue</w> <w n="2.4">au</w> <w n="2.5">pillage</w> <w n="2.6">avec</w> <w n="2.7">des</w> <w n="2.8">bras</w> <w n="2.9">sanglants</w> ?…</l>
						<l n="3" num="1.3"><w n="3.1">Amis</w>, <w n="3.2">ne</w> <w n="3.3">craignons</w> <w n="3.4">rien</w> : <w n="3.5">la</w> <w n="3.6">Patrie</w> <w n="3.7">est</w> <w n="3.8">plus</w> <w n="3.9">grande</w></l>
						<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">la</w> <w n="4.3">Horde</w> <w n="4.4">sauvage</w> <w n="4.5">attachée</w> <w n="4.6">à</w> <w n="4.7">ses</w> <w n="4.8">flancs</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">Patrie</w> <w n="5.3">est</w> <w n="5.4">sublime</w>, <w n="5.5">ayant</w> <w n="5.6">en</w> <w n="5.7">elle</w> <w n="5.8">une</w> <w n="5.9">âme</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="6.1">Qui</w> <w n="6.2">lutte</w>, <w n="6.3">au</w> <w n="6.4">cri</w> <w n="6.5">de</w> <w n="6.6">liberté</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Pour</w> <w n="7.2">le</w> <w n="7.3">Droit</w> <w n="7.4">immortel</w>, <w n="7.5">non</w> <w n="7.6">pour</w> <w n="7.7">un</w> <w n="7.8">lucre</w> <w n="7.9">infâme</w> —</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="8.1">Prix</w> <w n="8.2">d</w>'<w n="8.3">un</w> <w n="8.4">carnage</w> <w n="8.5">illimité</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">O</w> <w n="9.2">fous</w> ! <w n="9.3">nous</w> <w n="9.4">respections</w> <w n="9.5">la</w> <w n="9.6">vieille</w> <w n="9.7">bonhomie</w></l>
						<l n="10" num="3.2"><w n="10.1">Louée</w> <w n="10.2">avec</w> <w n="10.3">tant</w> <w n="10.4">d</w>'<w n="10.5">art</w> <w n="10.6">par</w> <w n="10.7">leurs</w> <w n="10.8">maîtres</w> <w n="10.9">chanteurs</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Qui</w> <w n="11.2">recouvrait</w> <w n="11.3">en</w> <w n="11.4">eux</w> <w n="11.5">la</w> <w n="11.6">pensée</w> <w n="11.7">ennemie</w></l>
						<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">semblait</w> <w n="12.3">rayonner</w> <w n="12.4">dans</w> <w n="12.5">leurs</w> <w n="12.6">regards</w> <w n="12.7">menteurs</w> :</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Des</w> <w n="13.2">bandits</w> <w n="13.3">ont</w> <w n="13.4">vécu</w> <w n="13.5">chez</w> <w n="13.6">nous</w>,-<w n="13.7">pleins</w> <w n="13.8">de</w> <w n="13.9">menaces</w>,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="14.1">Guetteurs</w> <w n="14.2">cachés</w> <w n="14.3">du</w> <w n="14.4">camp</w> <w n="14.5">germain</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">nous</w>, <w n="15.3">l</w>'<w n="15.4">esprit</w> <w n="15.5">hanté</w> <w n="15.6">d</w>'<w n="15.7">illusions</w> <w n="15.8">tenaces</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="16.1">Nous</w> <w n="16.2">leur</w> <w n="16.3">avons</w> <w n="16.4">tendu</w> <w n="16.5">la</w> <w n="16.6">main</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Espions</w> <w n="17.2">sans</w> <w n="17.3">vergogne</w>, <w n="17.4">ils</w> <w n="17.5">trahissaient</w> <w n="17.6">leur</w> <w n="17.7">hôte</w> :</l>
						<l n="18" num="5.2"><w n="18.1">Un</w> <w n="18.2">jour</w>, <w n="18.3">ils</w> <w n="18.4">ont</w> <w n="18.5">quitté</w> <w n="18.6">nos</w> <w n="18.7">loyales</w> <w n="18.8">maisons</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Pour</w> <w n="19.2">revenir</w> <w n="19.3">en</w> <w n="19.4">masse</w>, <w n="19.5">armés</w>, <w n="19.6">la</w> <w n="19.7">tête</w> <w n="19.8">haute</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Disant</w> : « — <w n="20.2">J</w>'<w n="20.3">ai</w> <w n="20.4">passé</w> <w n="20.5">là</w>… <w n="20.6">Rions</w>, <w n="20.7">tuons</w>, <w n="20.8">brisons</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« <w n="21.1">Brisons</w> <w n="21.2">la</w> <w n="21.3">porte</w> <w n="21.4">ouverte</w> <w n="21.5">et</w> <w n="21.6">le</w> <w n="21.7">dernier</w> <w n="21.8">asile</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="22.1">Et</w> <w n="22.2">jetons</w>-<w n="22.3">en</w> <w n="22.4">la</w> <w n="22.5">cendre</w> <w n="22.6">au</w> <w n="22.7">vent</w> !…</l>
						<l n="23" num="6.3">« <w n="23.1">Nous</w> <w n="23.2">reconnais</w>-<w n="23.3">tu</w> <w n="23.4">bien</w> ? <w n="23.5">C</w>'<w n="23.6">est</w> <w n="23.7">nous</w>, <w n="23.8">Peuple</w> <w n="23.9">imbécile</w> ! —</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="24.1">Déshonneur</w> <w n="24.2">et</w> <w n="24.3">mort</w>, <w n="24.4">en</w> <w n="24.5">avant</w> ! »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">nous</w> <w n="25.3">n</w>'<w n="25.4">y</w> <w n="25.5">pouvions</w> <w n="25.6">croire</w> ! <w n="25.7">Et</w>, <w n="25.8">l</w>'<w n="25.9">âme</w> <w n="25.10">encore</w> <w n="25.11">emplie</w></l>
						<l n="26" num="7.2"><w n="26.1">Du</w> <w n="26.2">souvenir</w> <w n="26.3">sacré</w> <w n="26.4">des</w> <w n="26.5">géants</w> <w n="26.6">d</w>'<w n="26.7">autrefois</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Luther</w>, <w n="27.2">Goethe</w>, <w n="27.3">Schiller</w>,—<w n="27.4">sous</w> <w n="27.5">qui</w> <w n="27.6">tout</w> <w n="27.7">orgueil</w> <w n="27.8">plie</w>, —</l>
						<l n="28" num="7.4"><w n="28.1">Nous</w> <w n="28.2">leur</w> <w n="28.3">avons</w> <w n="28.4">crié</w> : « — <w n="28.5">Ce</w> <w n="28.6">n</w>'<w n="28.7">est</w> <w n="28.8">pas</w> <w n="28.9">votre</w> <w n="28.10">voix</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">« <w n="29.1">L</w>'<w n="29.2">homme</w> <w n="29.3">qui</w> <w n="29.4">nous</w> <w n="29.5">perdait</w>, <w n="29.6">César</w> <w n="29.7">du</w> <w n="29.8">Bas</w>-<w n="29.9">Empire</w>,</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="30.1">Despote</w> <w n="30.2">ici</w>, <w n="30.3">traître</w> <w n="30.4">là</w>-<w n="30.5">bas</w>,</l>
						<l n="31" num="8.3">« <w n="31.1">Veut</w> <w n="31.2">en</w> <w n="31.3">vain</w>, <w n="31.4">dans</w> <w n="31.5">l</w>'<w n="31.6">opprobre</w> <w n="31.7">où</w> <w n="31.8">sa</w> <w n="31.9">fortune</w> <w n="31.10">expire</w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="32.1">Trafiquer</w> <w n="32.2">du</w> <w n="32.3">sort</w> <w n="32.4">des</w> <w n="32.5">combats</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">« <w n="33.1">Voyez</w> : <w n="33.2">hors</w> <w n="33.3">de</w> <w n="33.4">l</w>'<w n="33.5">étreinte</w> <w n="33.6">impure</w> <w n="33.7">de</w> <w n="33.8">cet</w> <w n="33.9">homme</w>,</l>
						<l n="34" num="9.2">« <w n="34.1">La</w> <w n="34.2">France</w>, <w n="34.3">calme</w> <w n="34.4">et</w> <w n="34.5">fière</w>, <w n="34.6">offre</w> <w n="34.7">au</w> <w n="34.8">monde</w> <w n="34.9">la</w> <w n="34.10">paix</w>…</l>
						<l n="35" num="9.3">« <w n="35.1">Ce</w> <w n="35.2">n</w>'<w n="35.3">est</w> <w n="35.4">pas</w> <w n="35.5">votre</w> <w n="35.6">voix</w> <w n="35.7">qui</w> <w n="35.8">la</w> <w n="35.9">raille</w> <w n="35.10">et</w> <w n="35.11">nous</w> <w n="35.12">somme</w></l>
						<l n="36" num="9.4">« <w n="36.1">De</w> <w n="36.2">ramper</w> <w n="36.3">avilis</w> <w n="36.4">entre</w> <w n="36.5">vos</w> <w n="36.6">rangs</w> <w n="36.7">épais</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">« <w n="37.1">Cet</w> <w n="37.2">exécrable</w> <w n="37.3">cri</w> <w n="37.4">ne</w> <w n="37.5">sort</w> <w n="37.6">pas</w> <w n="37.7">de</w> <w n="37.8">vos</w> <w n="37.9">bouches</w> :</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« — <w n="38.1">Écraser</w> <w n="38.2">la</w> <w n="38.3">France</w> <w n="38.4">et</w> <w n="38.5">Paris</w> ! —</l>
						<l n="39" num="10.3">« <w n="39.1">Sommes</w>-<w n="39.2">nous</w> <w n="39.3">donc</w> <w n="39.4">au</w> <w n="39.5">temps</w> <w n="39.6">des</w> <w n="39.7">Nibelungs</w> <w n="39.8">farouches</w>,</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="40.1">Et</w> <w n="40.2">le</w> <w n="40.3">monde</w> <w n="40.4">est</w>-<w n="40.5">il</w> <w n="40.6">en</w> <w n="40.7">débris</w> ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">« <w n="41.1">La</w> <w n="41.2">paix</w> <w n="41.3">serait</w> <w n="41.4">trop</w> <w n="41.5">chère</w> <w n="41.6">au</w> <w n="41.7">prix</w> <w n="41.8">de</w> <w n="41.9">tant</w> <w n="41.10">de</w> <w n="41.11">honte</w> !</l>
						<l n="42" num="11.2">« <w n="42.1">Le</w> <w n="42.2">fer</w> <w n="42.3">et</w> <w n="42.4">la</w> <w n="42.5">famine</w> <w n="42.6">et</w> <w n="42.7">la</w> <w n="42.8">mort</w> <w n="42.9">coûtent</w> <w n="42.10">moins</w> ;</l>
						<l n="43" num="11.3">« <w n="43.1">La</w> <w n="43.2">mitraille</w> <w n="43.3">demain</w> <w n="43.4">soldera</w> <w n="43.5">notre</w> <w n="43.6">compte</w> :</l>
						<l n="44" num="11.4">« <w n="44.1">Nous</w> <w n="44.2">aurons</w>, <w n="44.3">au</w> <w n="44.4">grand</w> <w n="44.5">jour</w>, <w n="44.6">les</w> <w n="44.7">peuples</w> <w n="44.8">pour</w> <w n="44.9">témoins</w> ! »</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Or</w> <w n="45.2">ils</w> <w n="45.3">ont</w> <w n="45.4">répondu</w> : « — <w n="45.5">La</w> <w n="45.6">justice</w>, <w n="45.7">qu</w>'<w n="45.8">importe</w> ?</l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="46.1">Le</w> <w n="46.2">droit</w> <w n="46.3">de</w> <w n="46.4">la</w> <w n="46.5">guerre</w> <w n="46.6">est</w> <w n="46.7">grossier</w> :</l>
						<l n="47" num="12.3">« <w n="47.1">Nous</w> <w n="47.2">avons</w>, <w n="47.3">pour</w> <w n="47.4">broyer</w> <w n="47.5">la</w> <w n="47.6">raison</w> <w n="47.7">la</w> <w n="47.8">plus</w> <w n="47.9">forte</w>,</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="48.1">Des</w> <w n="48.2">canons</w> <w n="48.3">de</w> <w n="48.4">bronze</w> <w n="48.5">et</w> <w n="48.6">d</w>'<w n="48.7">acier</w> !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">« <w n="49.1">Des</w> <w n="49.2">bords</w> <w n="49.3">de</w> <w n="49.4">la</w> <w n="49.5">Baltique</w> <w n="49.6">et</w> <w n="49.7">du</w> <w n="49.8">fond</w> <w n="49.9">des</w> <w n="49.10">bois</w> <w n="49.11">sombres</w></l>
						<l n="50" num="13.2">« <w n="50.1">Nous</w> <w n="50.2">sommes</w> <w n="50.3">accourus</w> <w n="50.4">au</w> <w n="50.5">funèbre</w> <w n="50.6">festin</w> :</l>
						<l n="51" num="13.3">« <w n="51.1">De</w> <w n="51.2">péril</w> <w n="51.3">en</w> <w n="51.4">péril</w>, <w n="51.5">à</w> <w n="51.6">travers</w> <w n="51.7">les</w> <w n="51.8">décombres</w>,</l>
						<l n="52" num="13.4">« <w n="52.1">Nous</w> <w n="52.2">épions</w> <w n="52.3">l</w>'<w n="52.4">instant</w> <w n="52.5">marqué</w> <w n="52.6">par</w> <w n="52.7">le</w> <w n="52.8">Destin</w>,</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">« <w n="53.1">Et</w> <w n="53.2">vous</w> <w n="53.3">serez</w> <w n="53.4">broyés</w>, <w n="53.5">rêveurs</w> <w n="53.6">à</w> <w n="53.7">tête</w> <w n="53.8">folle</w>,</l>
						<l n="54" num="14.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="54.1">Qui</w> <w n="54.2">raisonnez</w> <w n="54.3">sous</w> <w n="54.4">le</w> <w n="54.5">soufflet</w> !</l>
						<l n="55" num="14.3">« <w n="55.1">Notre</w> <w n="55.2">philosophie</w> <w n="55.3">est</w> <w n="55.4">un</w> <w n="55.5">mot</w> <w n="55.6">qui</w> <w n="55.7">s</w>'<w n="55.8">envole</w>,</l>
						<l n="56" num="14.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="56.1">Laissant</w> <w n="56.2">la</w> <w n="56.3">réplique</w> <w n="56.4">au</w> <w n="56.5">boulet</w> ! »</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Ainsi</w>, <w n="57.2">Kant</w> <w n="57.3">et</w> <w n="57.4">Mozart</w> <w n="57.5">nous</w> <w n="57.6">masquaient</w> <w n="57.7">leurs</w> <w n="57.8">boutiques</w>,</l>
						<l n="58" num="15.2"><w n="58.1">Leurs</w> <w n="58.2">ateliers</w> <w n="58.3">de</w> <w n="58.4">meurtre</w> <w n="58.5">et</w> <w n="58.6">leur</w> <w n="58.7">culte</w> <w n="58.8">du</w> <w n="58.9">mal</w> !</l>
						<l n="59" num="15.3"><w n="59.1">O</w> <w n="59.2">France</w>, <w n="59.3">ô</w> <w n="59.4">ma</w> <w n="59.5">patrie</w>, <w n="59.6">à</w> <w n="59.7">ces</w> <w n="59.8">soudards</w> <w n="59.9">mystiques</w></l>
						<l n="60" num="15.4"><w n="60.1">Cesse</w> <w n="60.2">de</w> <w n="60.3">faire</w> <w n="60.4">appel</w> <w n="60.5">au</w> <w n="60.6">nom</w> <w n="60.7">de</w> <w n="60.8">l</w>'<w n="60.9">Idéal</w> !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Car</w> <w n="61.2">si</w>, <w n="61.3">croisant</w> <w n="61.4">les</w> <w n="61.5">bras</w>, <w n="61.6">tu</w> <w n="61.7">t</w>'<w n="61.8">avançais</w> <w n="61.9">pensive</w>,</l>
						<l n="62" num="16.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="62.1">Songeant</w> <w n="62.2">aux</w> <w n="62.3">Maîtres</w> <w n="62.4">bien</w>-<w n="62.5">aimés</w>,</l>
						<l n="63" num="16.3"><w n="63.1">L</w>'<w n="63.2">Allemagne</w> <w n="63.3">rirait</w>, <w n="63.4">et</w> <w n="63.5">sa</w> <w n="63.6">hache</w> <w n="63.7">massive</w></l>
						<l n="64" num="16.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="64.1">S</w>'<w n="64.2">abattrait</w> <w n="64.3">sur</w> <w n="64.4">tes</w> <w n="64.5">bras</w> <w n="64.6">fermés</w> !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">O</w> <w n="65.2">reîtres</w> <w n="65.3">diplômés</w> ! <w n="65.4">docteurs</w> <w n="65.5">en</w> <w n="65.6">fourberie</w> !</l>
						<l n="66" num="17.2"><w n="66.1">Vous</w> <w n="66.2">devez</w> <w n="66.3">devant</w> <w n="66.4">nous</w> <w n="66.5">vous</w> <w n="66.6">incliner</w> <w n="66.7">bien</w> <w n="66.8">bas</w>,</l>
						<l n="67" num="17.3"><w n="67.1">Car</w> <w n="67.2">nous</w> <w n="67.3">demeurons</w> <w n="67.4">droits</w> <w n="67.5">jusqu</w>'<w n="67.6">en</w> <w n="67.7">notre</w> <w n="67.8">furie</w> !</l>
						<l n="68" num="17.4"><w n="68.1">Car</w> <w n="68.2">nous</w> <w n="68.3">avons</w> <w n="68.4">du</w> <w n="68.5">cœur</w> <w n="68.6">et</w> <w n="68.7">vous</w> <w n="68.8">n</w>'<w n="68.9">en</w> <w n="68.10">avez</w> <w n="68.11">pas</w> !…</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">Pour</w> <w n="69.2">moi</w> <w n="69.3">qui</w> <w n="69.4">m</w>'<w n="69.5">enivrais</w> <w n="69.6">de</w> <w n="69.7">leurs</w> <w n="69.8">pures</w> <w n="69.9">idées</w>,</l>
						<l n="70" num="18.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="70.1">Moi</w>, <w n="70.2">naïf</w>, <w n="70.3">qui</w> <w n="70.4">les</w> <w n="70.5">admirais</w>,</l>
						<l n="71" num="18.3">— <w n="71.1">Sachant</w> <w n="71.2">par</w> <w n="71.3">quel</w> <w n="71.4">ressort</w> <w n="71.5">leurs</w> <w n="71.6">âmes</w> <w n="71.7">sont</w>-<w n="71.8">guidées</w>,</l>
						<l n="72" num="18.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="72.1">Je</w> <w n="72.2">les</w> <w n="72.3">repousse</w> <w n="72.4">et</w> <w n="72.5">je</w> <w n="72.6">les</w> <w n="72.7">hais</w> !</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1"><w n="73.1">Mesure</w> <w n="73.2">qui</w> <w n="73.3">voudra</w> <w n="73.4">son</w> <w n="73.5">esprit</w> <w n="73.6">à</w> <w n="73.7">leur</w> <w n="73.8">toise</w> !</l>
						<l n="74" num="19.2"><w n="74.1">J</w>'<w n="74.2">aimerais</w> <w n="74.3">mieux</w> <w n="74.4">avoir</w>, <w n="74.5">serf</w> <w n="74.6">éternellement</w>,</l>
						<l n="75" num="19.3"><w n="75.1">A</w> <w n="75.2">mendier</w> <w n="75.3">mon</w> <w n="75.4">pain</w> <w n="75.5">sur</w> <w n="75.6">la</w> <w n="75.7">terre</w> <w n="75.8">gauloise</w>,</l>
						<l n="76" num="19.4"><w n="76.1">Que</w> <w n="76.2">d</w>'<w n="76.3">être</w> <w n="76.4">couronné</w> <w n="76.5">sur</w> <w n="76.6">le</w> <w n="76.7">sol</w> <w n="76.8">allemand</w> !</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1"><w n="77.1">Depuis</w> <w n="77.2">que</w> <w n="77.3">dans</w> <w n="77.4">Strasbourg</w> <w n="77.5">tes</w> <w n="77.6">trésors</w>, <w n="77.7">ô</w> <w n="77.8">Pensée</w>,</l>
						<l n="78" num="20.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="78.1">N</w>'<w n="78.2">ont</w> <w n="78.3">pu</w> <w n="78.4">trouver</w> <w n="78.5">grâce</w> <w n="78.6">à</w> <w n="78.7">leurs</w> <w n="78.8">yeux</w>,</l>
						<l n="79" num="20.3"><w n="79.1">Je</w> <w n="79.2">sens</w>, <w n="79.3">je</w> <w n="79.4">sens</w> <w n="79.5">monter</w> <w n="79.6">en</w> <w n="79.7">mon</w> <w n="79.8">âme</w> <w n="79.9">offensée</w></l>
						<l n="80" num="20.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="80.1">Un</w> <w n="80.2">mur</w> <w n="80.3">qui</w> <w n="80.4">me</w> <w n="80.5">sépare</w> <w n="80.6">d</w>'<w n="80.7">eux</w> !</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1"><w n="81.1">Je</w> <w n="81.2">sais</w> <w n="81.3">tout</w> <w n="81.4">ce</w> <w n="81.5">que</w> <w n="81.6">vaut</w> <w n="81.7">l</w>’<w n="81.8">œuvre</w> <w n="81.9">d</w>'<w n="81.10">hypocrisie</w></l>
						<l n="82" num="21.2"><w n="82.1">Dont</w> <w n="82.2">ils</w> <w n="82.3">avaient</w> <w n="82.4">couvert</w> <w n="82.5">la</w> <w n="82.6">fange</w> <w n="82.7">de</w> <w n="82.8">leurs</w> <w n="82.9">cœurs</w> :</l>
						<l n="83" num="21.3"><w n="83.1">Leur</w> <w n="83.2">suprême</w> <w n="83.3">vertu</w>, <w n="83.4">leur</w> <w n="83.5">fleur</w> <w n="83.6">de</w> <w n="83.7">poésie</w>,</l>
						<l n="84" num="21.4"><w n="84.1">Ils</w> <w n="84.2">en</w> <w n="84.3">ont</w> <w n="84.4">fait</w> <w n="84.5">litière</w> <w n="84.6">en</w> <w n="84.7">se</w> <w n="84.8">croyant</w> <w n="84.9">vainqueurs</w> !</l>
					</lg>
					<lg n="22">
						<l n="85" num="22.1"><w n="85.1">Sans</w> <w n="85.2">doute</w> <w n="85.3">on</w> <w n="85.4">les</w> <w n="85.5">verrait</w>, <w n="85.6">s</w>'<w n="85.7">ils</w> <w n="85.8">gardaient</w> <w n="85.9">la</w> <w n="85.10">victoire</w>,</l>
						<l n="86" num="22.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="86.1">Contempler</w> <w n="86.2">d</w>'<w n="86.3">un</w> <w n="86.4">œil</w> <w n="86.5">languissant</w></l>
						<l n="87" num="22.3"><w n="87.1">Le</w> <w n="87.2">bleu</w> <w n="87.3">vergiss</w>-<w n="87.4">mein</w>-<w n="87.5">nicht</w>, <w n="87.6">en</w> <w n="87.7">savourant</w> <w n="87.8">leur</w> <w n="87.9">gloire</w>,</l>
						<l n="88" num="22.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="88.1">Les</w> <w n="88.2">pieds</w> <w n="88.3">dans</w> <w n="88.4">un</w> <w n="88.5">ruisseau</w> <w n="88.6">de</w> <w n="88.7">sang</w> !</l>
					</lg>
					<lg n="23">
						<l n="89" num="23.1"><w n="89.1">Comme</w> <w n="89.2">le</w> <w n="89.3">cri</w> <w n="89.4">brutal</w> <w n="89.5">de</w> <w n="89.6">la</w> <w n="89.7">Ménade</w> <w n="89.8">antique</w>,</l>
						<l n="90" num="23.2"><w n="90.1">Sans</w> <w n="90.2">doute</w> <w n="90.3">on</w> <w n="90.4">entendrait</w> <w n="90.5">sortir</w> <w n="90.6">du</w> <w n="90.7">sein</w> <w n="90.8">des</w> <w n="90.9">bois</w></l>
						<l n="91" num="23.3"><w n="91.1">Le</w> <w n="91.2">refrain</w> <w n="91.3">des</w> <w n="91.4">goujats</w> <w n="91.5">entonnant</w> <w n="91.6">leur</w> <w n="91.7">cantique</w>,</l>
						<l n="92" num="23.4"><w n="92.1">Pour</w> <w n="92.2">insulter</w> <w n="92.3">de</w> <w n="92.4">loin</w> <w n="92.5">l</w>'<w n="92.6">héroïsme</w> <w n="92.7">aux</w> <w n="92.8">abois</w> :</l>
					</lg>
					<lg n="24">
						<l n="93" num="24.1">—« <w n="93.1">La</w> <w n="93.2">honte</w> <w n="93.3">n</w>'<w n="93.4">est</w> <w n="93.5">qu</w>'<w n="93.6">un</w> <w n="93.7">mot</w>, <w n="93.8">et</w> <w n="93.9">le</w> <w n="93.10">droit</w> <w n="93.11">n</w>'<w n="93.12">est</w> <w n="93.13">qu</w>'<w n="93.14">une</w> <w n="93.15">ombre</w> !</l>
						<l n="94" num="24.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="94.1">Et</w> <w n="94.2">comme</w> <w n="94.3">d</w>'<w n="94.4">inertes</w> <w n="94.5">moutons</w>,</l>
						<l n="95" num="24.3">« <w n="95.1">Les</w> <w n="95.2">peuples</w> <w n="95.3">sont</w> <w n="95.4">tremblants</w> : <w n="95.5">sous</w> <w n="95.6">la</w> <w n="95.7">force</w> <w n="95.8">et</w> <w n="95.9">le</w> <w n="95.10">nombre</w></l>
						<l n="96" num="24.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="96.1">L</w>'<w n="96.2">Europe</w> <w n="96.3">va</w> <w n="96.4">mourir</w>… <w n="96.5">Chantons</w> !</l>
					</lg>
					<lg n="25">
						<l n="97" num="25.1">« <w n="97.1">Hurrah</w> ! <w n="97.2">la</w> <w n="97.3">France</w> <w n="97.4">est</w> <w n="97.5">morte</w>, <w n="97.6">et</w> <w n="97.7">de</w> <w n="97.8">sa</w> <w n="97.9">main</w> <w n="97.10">crispée</w>,</l>
						<l n="98" num="25.2">« <w n="98.1">Qui</w> <w n="98.2">ne</w> <w n="98.3">portera</w> <w n="98.4">plus</w> <w n="98.5">le</w> <w n="98.6">glaive</w> <w n="98.7">ou</w> <w n="98.8">l</w>'<w n="98.9">étendard</w>,</l>
						<l n="99" num="25.3">« <w n="99.1">Nos</w> <w n="99.2">gantelets</w> <w n="99.3">de</w> <w n="99.4">fer</w> <w n="99.5">ont</w> <w n="99.6">fait</w> <w n="99.7">tomber</w> <w n="99.8">l</w>'<w n="99.9">épée</w> !</l>
						<l n="100" num="25.4">« <w n="100.1">Hurrah</w> ! <w n="100.2">pour</w> <w n="100.3">nous</w> <w n="100.4">ravir</w> <w n="100.5">l</w>'<w n="100.6">empire</w>, <w n="100.7">il</w> <w n="100.8">est</w> <w n="100.9">trop</w> <w n="100.10">tard</w> !</l>
					</lg>
					<lg n="26">
						<l n="101" num="26.1">« <w n="101.1">Tous</w> <w n="101.2">les</w> <w n="101.3">fiers</w> <w n="101.4">combattants</w> <w n="101.5">qui</w> <w n="101.6">s</w>'<w n="101.7">agitaient</w> <w n="101.8">naguère</w></l>
						<l n="102" num="26.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="102.1">Ont</w> <w n="102.2">roulé</w> <w n="102.3">dans</w> <w n="102.4">les</w> <w n="102.5">grandes</w> <w n="102.6">eaux</w>,</l>
						<l n="103" num="26.3">« <w n="103.1">Dans</w> <w n="103.2">le</w> <w n="103.3">rouge</w> <w n="103.4">océan</w> <w n="103.5">de</w> <w n="103.6">la</w> <w n="103.7">dernière</w> <w n="103.8">guerre</w>,</l>
						<l n="104" num="26.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space>« <w n="104.1">Et</w> <w n="104.2">nous</w> <w n="104.3">n</w>'<w n="104.4">avons</w> <w n="104.5">plus</w> <w n="104.6">de</w> <w n="104.7">rivaux</w> ! »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="105" num="1.1"><w n="105.1">Or</w>, <w n="105.2">quand</w> <w n="105.3">le</w> <w n="105.4">jour</w> <w n="105.5">maudit</w> <w n="105.6">de</w> <w n="105.7">l</w>'<w n="105.8">affreuse</w> <w n="105.9">agonie</w></l>
						<l n="106" num="1.2"><w n="106.1">Sonnerait</w> <w n="106.2">pour</w> <w n="106.3">la</w> <w n="106.4">France</w> <w n="106.5">et</w> <w n="106.6">pour</w> <w n="106.7">le</w> <w n="106.8">monde</w> <w n="106.9">entier</w> ;</l>
						<l n="107" num="1.3"><w n="107.1">Quand</w> <w n="107.2">ton</w> <w n="107.3">peuple</w> <w n="107.4">sans</w> <w n="107.5">âme</w>, <w n="107.6">ô</w> <w n="107.7">lourde</w> <w n="107.8">Germanie</w>,</l>
						<l n="108" num="1.4"><w n="108.1">Des</w> <w n="108.2">peuples</w> <w n="108.3">disparus</w> <w n="108.4">se</w> <w n="108.5">dirait</w> <w n="108.6">héritier</w> ;</l>
					</lg>
					<lg n="2">
						<l n="109" num="2.1"><w n="109.1">Quand</w> <w n="109.2">il</w> <w n="109.3">croirait</w> <w n="109.4">avoir</w> <w n="109.5">écrasé</w> <w n="109.6">sous</w> <w n="109.7">sa</w> <w n="109.8">meule</w></l>
						<l n="110" num="2.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="110.1">Fierté</w>, <w n="110.2">Justice</w> <w n="110.3">et</w> <w n="110.4">Vérité</w>, —</l>
						<l n="111" num="2.3"><w n="111.1">Même</w> <w n="111.2">en</w> <w n="111.3">ces</w> <w n="111.4">jours</w> <w n="111.5">d</w>'<w n="111.6">horreur</w>, <w n="111.7">et</w> <w n="111.8">la</w> <w n="111.9">nuit</w> <w n="111.10">régnant</w> <w n="111.11">seule</w></l>
						<l n="112" num="2.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="112.1">Où</w> <w n="112.2">régna</w> <w n="112.3">jadis</w> <w n="112.4">la</w> <w n="112.5">clarté</w>,</l>
					</lg>
					<lg n="3">
						<l n="113" num="3.1"><w n="113.1">Tout</w> <w n="113.2">ne</w> <w n="113.3">serait</w> <w n="113.4">pas</w> <w n="113.5">mort</w> <w n="113.6">dans</w> <w n="113.7">l</w>'<w n="113.8">étendue</w> <w n="113.9">immense</w> :</l>
						<l n="114" num="3.2"><w n="114.1">Il</w> <w n="114.2">te</w> <w n="114.3">faudrait</w> <w n="114.4">compter</w>, <w n="114.5">race</w> <w n="114.6">au</w> <w n="114.7">stupide</w> <w n="114.8">orgueil</w>,</l>
						<l n="115" num="3.3"><w n="115.1">Avec</w> <w n="115.2">cet</w> <w n="115.3">inconnu</w> <w n="115.4">qui</w> <w n="115.5">toujours</w> <w n="115.6">recommence</w>,</l>
						<l n="116" num="3.4"><w n="116.1">Et</w> <w n="116.2">l</w>'<w n="116.3">indomptable</w> <w n="116.4">Droit</w> <w n="116.5">sortirait</w> <w n="116.6">du</w> <w n="116.7">cercueil</w> !</l>
					</lg>
					<lg n="4">
						<l n="117" num="4.1"><w n="117.1">Tu</w> <w n="117.2">tremblerais</w> <w n="117.3">encore</w>, <w n="117.4">et</w> <w n="117.5">la</w> <w n="117.6">voix</w> <w n="117.7">de</w> <w n="117.8">la</w> <w n="117.9">France</w>,</l>
						<l n="118" num="4.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="118.1">Massacrée</w> <w n="118.2">au</w> <w n="118.3">coin</w> <w n="118.4">du</w> <w n="118.5">chemin</w>,</l>
						<l n="119" num="4.3"><w n="119.1">Te</w> <w n="119.2">poursuivrait</w> <w n="119.3">partout</w> <w n="119.4">d</w>'<w n="119.5">un</w> <w n="119.6">long</w> <w n="119.7">cri</w> <w n="119.8">d</w>'<w n="119.9">espérance</w></l>
						<l n="120" num="4.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="120.1">Au</w> <w n="120.2">nom</w> <w n="120.3">d</w>'<w n="120.4">un</w> <w n="120.5">autre</w> <w n="120.6">genre</w> <w n="120.7">humain</w> !</l>
					</lg>
					<lg n="5">
						<l n="121" num="5.1"><w n="121.1">Du</w> <w n="121.2">sein</w> <w n="121.3">des</w> <w n="121.4">archipels</w>, <w n="121.5">du</w> <w n="121.6">fond</w> <w n="121.7">des</w> <w n="121.8">mers</w> <w n="121.9">lointaines</w>,</l>
						<l n="122" num="5.2"><w n="122.1">Des</w> <w n="122.2">peuples</w> <w n="122.3">surgiraient</w> <w n="122.4">qu</w>'<w n="122.5">on</w> <w n="122.6">ignorait</w> <w n="122.7">hier</w>,</l>
						<l n="123" num="5.3"><w n="123.1">Ramenant</w> <w n="123.2">les</w> <w n="123.3">soleils</w> <w n="123.4">de</w> <w n="123.5">Paris</w> <w n="123.6">et</w> <w n="123.7">d</w>'<w n="123.8">Athènes</w>,</l>
						<l n="124" num="5.4"><w n="124.1">Et</w> <w n="124.2">leur</w> <w n="124.3">âme</w> <w n="124.4">vaincrait</w> <w n="124.5">ton</w> <w n="124.6">armure</w> <w n="124.7">de</w> <w n="124.8">fer</w> !</l>
					</lg>
					<lg n="6">
						<l n="125" num="6.1"><w n="125.1">Et</w> <w n="125.2">la</w> <w n="125.3">ville</w> <w n="125.4">au</w> <w n="125.5">grand</w> <w n="125.6">cœur</w>, <w n="125.7">et</w> <w n="125.8">la</w> <w n="125.9">cité</w> <w n="125.10">brisée</w></l>
						<l n="126" num="6.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="126.1">Dont</w> <w n="126.2">tu</w> <w n="126.3">redoutes</w> <w n="126.4">jusqu</w>'<w n="126.5">au</w> <w n="126.6">nom</w>,</l>
						<l n="127" num="6.3"><w n="127.1">Triompherait</w> <w n="127.2">de</w> <w n="127.3">toi</w>, <w n="127.4">nation</w> <w n="127.5">méprisée</w>,</l>
						<l n="128" num="6.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="128.1">Gisant</w> <w n="128.2">sous</w> <w n="128.3">ton</w> <w n="128.4">dernier</w> <w n="128.5">canon</w> !</l>
					</lg>
					<lg n="7">
						<l n="129" num="7.1"><w n="129.1">Mais</w> <w n="129.2">l</w>'<w n="129.3">âme</w> <w n="129.4">de</w> <w n="129.5">Paris</w>, <w n="129.6">dans</w> <w n="129.7">sa</w> <w n="129.8">force</w> <w n="129.9">sublime</w>,</l>
						<l n="130" num="7.2"><w n="130.1">Ne</w> <w n="130.2">se</w> <w n="130.3">laissera</w> <w n="130.4">pas</w> <w n="130.5">assassiner</w> <w n="130.6">par</w> <w n="130.7">toi</w> :</l>
						<l n="131" num="7.3"><w n="131.1">Tu</w> <w n="131.2">n</w>'<w n="131.3">auras</w>, <w n="131.4">Allemagne</w>, <w n="131.5">en</w> <w n="131.6">retour</w> <w n="131.7">de</w> <w n="131.8">ton</w> <w n="131.9">crime</w>,</l>
						<l n="132" num="7.4"><w n="132.1">Que</w> <w n="132.2">le</w> <w n="132.3">fleuve</w> <w n="132.4">de</w> <w n="132.5">sang</w> <w n="132.6">où</w> <w n="132.7">se</w> <w n="132.8">noîra</w> <w n="132.9">ton</w> <w n="132.10">roi</w> !</l>
					</lg>
					<lg n="8">
						<l n="133" num="8.1"><w n="133.1">Et</w> <w n="133.2">nous</w> <w n="133.3">te</w> <w n="133.4">défions</w> <w n="133.5">à</w> <w n="133.6">la</w> <w n="133.7">face</w> <w n="133.8">du</w> <w n="133.9">monde</w>,</l>
						<l n="134" num="8.2"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="134.1">Jurant</w>, <w n="134.2">si</w> <w n="134.3">l</w>'<w n="134.4">on</w> <w n="134.5">veut</w> <w n="134.6">notre</w> <w n="134.7">mort</w>,</l>
						<l n="135" num="8.3"><w n="135.1">De</w> <w n="135.2">creuser</w> <w n="135.3">dans</w> <w n="135.4">nos</w> <w n="135.5">murs</w> <w n="135.6">une</w> <w n="135.7">fosse</w> <w n="135.8">profonde</w></l>
						<l n="136" num="8.4"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="136.1">Pour</w> <w n="136.2">tous</w> <w n="136.3">les</w> <w n="136.4">égorgeurs</w> <w n="136.5">du</w> <w n="136.6">Nord</w> !</l>
					</lg>
				</div>
					<closer>
						<dateline>
							<date when="1870">Paris, 20 octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>