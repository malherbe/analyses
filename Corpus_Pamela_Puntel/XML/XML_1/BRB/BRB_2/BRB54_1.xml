<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">DEVANT L'ENNEMI</title>
				<title type="sub">Poémes publiés dans la REVUE DES DEUX MONDES (1870)</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>124 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRB_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DEVANT L'ENNEMI</title>
						<author>AUGUSTE BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k870291</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>REVUE DES DEUX MONDES</title>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>REVUE DES DEUX MONDES</publisher>
									<date when="1870">1870</date>
								</imprint>
								<biblScope unit="tome">89</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEVANT L'ENNEMI</head><div type="poem" key="BRB54">
					<head type="main">LE FILS DES HUNS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ce</w> <w n="1.2">sont</w> <w n="1.3">bien</w> <w n="1.4">eux</w> <w n="1.5">toujours</w>, <w n="1.6">avec</w> <w n="1.7">leurs</w> <w n="1.8">mains</w> <w n="1.9">avares</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Leurs</w> <w n="2.2">yeux</w> <w n="2.3">rusés</w>, <w n="2.4">leurs</w> <w n="2.5">instruments</w> <w n="2.6">de</w> <w n="2.7">feu</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Toujours</w> <w n="3.2">des</w> <w n="3.3">ravageurs</w> <w n="3.4">farouches</w>, <w n="3.5">des</w> <w n="3.6">barbares</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Frappant</w> <w n="4.2">partout</w> <w n="4.3">gens</w> <w n="4.4">et</w> <w n="4.5">choses</w> <w n="4.6">de</w> <w n="4.7">Dieu</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Strasbourg</w> <w n="5.2">a</w> <w n="5.3">beau</w> <w n="5.4">crier</w> : — <w n="5.5">Laissez</w> <w n="5.6">sortir</w> <w n="5.7">les</w> <w n="5.8">femmes</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1">Les</w> <w n="6.2">petits</w> <w n="6.3">cœurs</w>, <w n="6.4">les</w> <w n="6.5">vieux</w> <w n="6.6">au</w> <w n="6.7">corps</w> <w n="6.8">ployé</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Tout</w> <w n="7.2">ce</w> <w n="7.3">qui</w> <w n="7.4">ne</w> <w n="7.5">peut</w> <w n="7.6">pas</w> <w n="7.7">vous</w> <w n="7.8">renvoyer</w> <w n="7.9">vos</w> <w n="7.10">flammes</w> ! —</l>
						<l n="8" num="2.4"><space unit="char" quantity="4"></space><w n="8.1">Ils</w> <w n="8.2">restent</w> <w n="8.3">sourds</w> <w n="8.4">sans</w> <w n="8.5">honte</w>, <w n="8.6">sans</w> <w n="8.7">pitié</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Un</w> <w n="9.2">saint</w> <w n="9.3">évêque</w> <w n="9.4">dit</w> : — <w n="9.5">Épargnez</w> <w n="9.6">les</w> <w n="9.7">malades</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="4"></space><w n="10.1">Les</w> <w n="10.2">murs</w> <w n="10.3">gardiens</w> <w n="10.4">des</w> <w n="10.5">merveilles</w> <w n="10.6">de</w> <w n="10.7">l</w>'<w n="10.8">art</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Ma</w> <w n="11.2">vieille</w> <w n="11.3">cathédrale</w> <w n="11.4">aux</w> <w n="11.5">sublimes</w> <w n="11.6">arcades</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="4"></space><w n="12.1">Et</w> <w n="12.2">dont</w> <w n="12.3">la</w> <w n="12.4">flèche</w> <w n="12.5">émeut</w> <w n="12.6">tant</w> <w n="12.7">le</w> <w n="12.8">regard</w> ! —</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">le</w> <w n="13.3">cruel</w> <w n="13.4">Werder</w> <w n="13.5">répond</w> <w n="13.6">à</w> <w n="13.7">sa</w> <w n="13.8">demande</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="4"></space><w n="14.1">Ces</w> <w n="14.2">mots</w> <w n="14.3">affreux</w> : — <w n="14.4">Point</w>, <w n="14.5">c</w>'<w n="14.6">est</w> <w n="14.7">par</w> <w n="14.8">la</w> <w n="14.9">terreur</w></l>
						<l n="15" num="4.3"><w n="15.1">Que</w> <w n="15.2">j</w>'<w n="15.3">espère</w> <w n="15.4">bientôt</w> <w n="15.5">que</w> <w n="15.6">le</w> <w n="15.7">soldat</w> <w n="15.8">se</w> <w n="15.9">rende</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="4"></space><w n="16.1">Et</w> <w n="16.2">sous</w> <w n="16.3">mes</w> <w n="16.4">pieds</w> <w n="16.5">abaisse</w> <w n="16.6">sa</w> <w n="16.7">valeur</w> ! —</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">le</w> <w n="17.3">mortier</w> <w n="17.4">reprend</w> <w n="17.5">sa</w> <w n="17.6">manœuvre</w> <w n="17.7">infernale</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="4"></space><w n="18.1">La</w> <w n="18.2">bombe</w> <w n="18.3">en</w> <w n="18.4">feu</w> <w n="18.5">plane</w> <w n="18.6">sur</w> <w n="18.7">les</w> <w n="18.8">abris</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">tout</w>, <w n="19.3">bibliothèque</w>, <w n="19.4">hospice</w>, <w n="19.5">cathédrale</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="4"></space><w n="20.1">Jonche</w> <w n="20.2">le</w> <w n="20.3">sol</w> <w n="20.4">de</w> <w n="20.5">chauds</w> <w n="20.6">et</w> <w n="20.7">noirs</w> <w n="20.8">débris</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Le</w> <w n="21.2">sang</w> <w n="21.3">coule</w> <w n="21.4">à</w> <w n="21.5">torrent</w>, <w n="21.6">et</w> <w n="21.7">si</w> <w n="21.8">la</w> <w n="21.9">noble</w> <w n="21.10">place</w></l>
						<l n="22" num="6.2"><space unit="char" quantity="4"></space><w n="22.1">N</w>'<w n="22.2">est</w> <w n="22.3">secourue</w>, <w n="22.4">hélas</w> ! <w n="22.5">c</w>'<w n="22.6">est</w> <w n="22.7">un</w> <w n="22.8">tombeau</w></l>
						<l n="23" num="6.3"><w n="23.1">Autour</w> <w n="23.2">duquel</w> <w n="23.3">longtemps</w> <w n="23.4">les</w> <w n="23.5">filles</w> <w n="23.6">de</w> <w n="23.7">l</w>'<w n="23.8">Alsace</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="4"></space><w n="24.1">Des</w> <w n="24.2">gens</w> <w n="24.3">u</w> <w n="24.4">nord</w> <w n="24.5">maudiront</w> <w n="24.6">le</w> <w n="24.7">fléau</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Horreur</w> ! <w n="25.2">et</w> <w n="25.3">voilà</w> <w n="25.4">bien</w> <w n="25.5">des</w> <w n="25.6">siècles</w> <w n="25.7">qu</w>'<w n="25.8">on</w> <w n="25.9">dépense</w></l>
						<l n="26" num="7.2"><space unit="char" quantity="4"></space><w n="26.1">Esprit</w> <w n="26.2">et</w> <w n="26.3">cœur</w> <w n="26.4">pour</w> <w n="26.5">en</w> <w n="26.6">arriver</w> <w n="26.7">là</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Pour</w> <w n="27.2">voir</w> <w n="27.3">recommencer</w> <w n="27.4">avec</w> <w n="27.5">plus</w> <w n="27.6">de</w> <w n="27.7">science</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="4"></space><w n="28.1">L</w>'<w n="28.2">œuvre</w> <w n="28.3">sans</w> <w n="28.4">nom</w> <w n="28.5">des</w> <w n="28.6">hordes</w> <w n="28.7">d</w>'<w n="28.8">Attila</w> !</l>
					</lg>
				</div></body></text></TEI>