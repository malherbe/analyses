<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">DEVANT L'ENNEMI</title>
				<title type="sub">Poémes publiés dans la REVUE DES DEUX MONDES (1870)</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>124 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRB_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DEVANT L'ENNEMI</title>
						<author>AUGUSTE BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k870291</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>REVUE DES DEUX MONDES</title>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>REVUE DES DEUX MONDES</publisher>
									<date when="1870">1870</date>
								</imprint>
								<biblScope unit="tome">89</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEVANT L'ENNEMI</head><div type="poem" key="BRB55">
					<head type="main">AUX ALLEMANDS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu</w>'<w n="1.2">as</w> <w n="1.3">tu</w> <w n="1.4">fait</w>, <w n="1.5">Allemagne</w> ? <w n="1.6">En</w> <w n="1.7">ce</w> <w n="1.8">conflit</w> <w n="1.9">nouveau</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1">Tu</w> <w n="2.2">t</w>'<w n="2.3">es</w> <w n="2.4">mise</w> <w n="2.5">à</w> <w n="2.6">la</w> <w n="2.7">suite</w></l>
						<l n="3" num="1.3"><w n="3.1">D</w>'<w n="3.2">un</w> <w n="3.3">féroce</w> <w n="3.4">ministre</w> <w n="3.5">et</w> <w n="3.6">de</w> <w n="3.7">son</w> <w n="3.8">roi</w> <w n="3.9">dévot</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">Bombardeur</w> <w n="4.2">hypocrite</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Toi</w> <w n="5.2">que</w> <w n="5.3">l</w>'<w n="5.4">on</w> <w n="5.5">estimait</w> <w n="5.6">parfum</w> <w n="5.7">d</w>'<w n="5.8">honnêteté</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">Et</w> <w n="6.2">fleur</w> <w n="6.3">de</w> <w n="6.4">poésie</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Tu</w> <w n="7.2">n</w>'<w n="7.3">avais</w> <w n="7.4">dans</w> <w n="7.5">le</w> <w n="7.6">cœur</w>, <w n="7.7">sous</w> <w n="7.8">masque</w> <w n="7.9">de</w> <w n="7.10">bonté</w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="12"></space><w n="8.1">Que</w> <w n="8.2">basse</w> <w n="8.3">jalousie</w> !</l>
						<l n="9" num="1.9"><w n="9.1">Servante</w> <w n="9.2">du</w> <w n="9.3">Prussien</w>, <w n="9.4">tu</w> <w n="9.5">lui</w> <w n="9.6">prêtas</w> <w n="9.7">tes</w> <w n="9.8">bras</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="12"></space><w n="10.1">Quand</w> <w n="10.2">sa</w> <w n="10.3">troupe</w> <w n="10.4">sauvage</w>,</l>
						<l n="11" num="1.11"><w n="11.1">S</w>'<w n="11.2">épandant</w> <w n="11.3">sur</w> <w n="11.4">nos</w> <w n="11.5">champs</w>, <w n="11.6">y</w> <w n="11.7">porta</w> <w n="11.8">le</w> <w n="11.9">trépas</w>,</l>
						<l n="12" num="1.12"><space unit="char" quantity="12"></space><w n="12.1">La</w> <w n="12.2">flamme</w> <w n="12.3">et</w> <w n="12.4">le</w> <w n="12.5">ravage</w> ;</l>
						<l n="13" num="1.13"><w n="13.1">Tu</w> <w n="13.2">mêlas</w> <w n="13.3">ton</w> <w n="13.4">épée</w> <w n="13.5">aux</w> <w n="13.6">glaives</w> <w n="13.7">assassins</w></l>
						<l n="14" num="1.14"><space unit="char" quantity="12"></space><w n="14.1">De</w> <w n="14.2">ces</w> <w n="14.3">hardis</w> <w n="14.4">Vandales</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">pris</w> <w n="15.3">secrète</w> <w n="15.4">part</w> <w n="15.5">à</w> <w n="15.6">tous</w> <w n="15.7">les</w> <w n="15.8">noirs</w> <w n="15.9">desseins</w></l>
						<l n="16" num="1.16"><space unit="char" quantity="12"></space><w n="16.1">Des</w> <w n="16.2">bandes</w> <w n="16.3">féodales</w> !</l>
						<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">pourquoi</w> ? <w n="17.3">Dans</w> <w n="17.4">l</w>'<w n="17.5">espoir</w> <w n="17.6">qu</w>'<w n="17.7">au</w> <w n="17.8">vil</w> <w n="17.9">démembrement</w></l>
						<l n="18" num="1.18"><space unit="char" quantity="12"></space><w n="18.1">De</w> <w n="18.2">la</w> <w n="18.3">France</w> <w n="18.4">éventrée</w></l>
						<l n="19" num="1.19"><w n="19.1">Tes</w> <w n="19.2">petits</w> <w n="19.3">rois</w> <w n="19.4">vautours</w> <w n="19.5">seraient</w> <w n="19.6">tous</w> <w n="19.7">simplement</w></l>
						<l n="20" num="1.20"><space unit="char" quantity="12"></space><w n="20.1">Admis</w> <w n="20.2">à</w> <w n="20.3">la</w> <w n="20.4">curée</w> !</l>
						<l n="21" num="1.21"><w n="21.1">Tes</w> <w n="21.2">républicains</w> <w n="21.3">même</w>, <w n="21.4">ivres</w> <w n="21.5">de</w> <w n="21.6">la</w> <w n="21.7">beauté</w></l>
						<l n="22" num="1.22"><space unit="char" quantity="12"></space><w n="22.1">De</w> <w n="22.2">cette</w> <w n="22.3">boucherie</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Muets</w> <w n="23.2">presque</w> <w n="23.3">tous</w>, <w n="23.4">ont</w> <w n="23.5">à</w> <w n="23.6">peine</w> <w n="23.7">protesté</w></l>
						<l n="24" num="1.24"><space unit="char" quantity="12"></space><w n="24.1">Contre</w> <w n="24.2">la</w> <w n="24.3">barbarie</w> !</l>
						<l n="25" num="1.25"><w n="25.1">Ah</w> ! <w n="25.2">que</w> <w n="25.3">le</w> <w n="25.4">temps</w> <w n="25.5">s</w>'<w n="25.6">écoule</w>, <w n="25.7">il</w> <w n="25.8">n</w>'<w n="25.9">effacera</w> <w n="25.10">pas</w></l>
						<l n="26" num="1.26"><space unit="char" quantity="12"></space><w n="26.1">Cette</w> <w n="26.2">action</w> <w n="26.3">coupable</w> ;</l>
						<l n="27" num="1.27"><w n="27.1">Elle</w> <w n="27.2">marque</w> <w n="27.3">ton</w> <w n="27.4">front</w> <w n="27.5">entre</w> <w n="27.6">tous</w> <w n="27.7">les</w> <w n="27.8">états</w></l>
						<l n="28" num="1.28"><space unit="char" quantity="12"></space><w n="28.1">D</w>'<w n="28.2">une</w> <w n="28.3">tache</w> <w n="28.4">effroyable</w>.</l>
						<l n="29" num="1.29"><w n="29.1">Pour</w> <w n="29.2">des</w> <w n="29.3">siècles</w> <w n="29.4">sans</w> <w n="29.5">nombre</w> <w n="29.6">elle</w> <w n="29.7">nous</w> <w n="29.8">laisse</w> <w n="29.9">au</w> <w n="29.10">cœur</w></l>
						<l n="30" num="1.30"><space unit="char" quantity="12"></space><w n="30.1">Une</w> <w n="30.2">peine</w> <w n="30.3">infinie</w></l>
						<l n="31" num="1.31"><w n="31.1">Dont</w> <w n="31.2">nulle</w> <w n="31.3">douce</w> <w n="31.4">paix</w> <w n="31.5">n</w>'<w n="31.6">amoindrira</w> <w n="31.7">l</w>'<w n="31.8">ardeur</w>,</l>
						<l n="32" num="1.32"><space unit="char" quantity="12"></space><w n="32.1">Perfide</w> <w n="32.2">Germanie</w> !</l>
						<l n="33" num="1.33"><w n="33.1">Mais</w> <w n="33.2">va</w>, <w n="33.3">ton</w> <w n="33.4">châtiment</w> <w n="33.5">s</w>'<w n="33.6">avance</w>, <w n="33.7">car</w> <w n="33.8">après</w></l>
						<l n="34" num="1.34"><space unit="char" quantity="12"></space><w n="34.1">Cette</w> <w n="34.2">horrible</w> <w n="34.3">campagne</w></l>
						<l n="35" num="1.35"><w n="35.1">Le</w> <w n="35.2">venin</w> <w n="35.3">de</w> <w n="35.4">la</w> <w n="35.5">Prusse</w> <w n="35.6">en</w> <w n="35.7">toi</w> <w n="35.8">reste</w> <w n="35.9">à</w> <w n="35.10">jamais</w>,</l>
						<l n="36" num="1.36"><space unit="char" quantity="12"></space><w n="36.1">Et</w> <w n="36.2">morte</w> <w n="36.3">est</w> <w n="36.4">l</w>'<w n="36.5">Allemagne</w>.</l>
					</lg>
				</div></body></text></TEI>