<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM7">
				<head type="main">LAOCOON</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">dragons</w>, <w n="1.3">se</w> <w n="1.4">dressant</w>, <w n="1.5">enlacent</w> <w n="1.6">la</w> <w n="1.7">victime</w>.</l>
					<l n="2" num="1.2"><w n="2.1">En</w> <w n="2.2">vain</w> <w n="2.3">ses</w> <w n="2.4">bras</w> <w n="2.5">crispés</w>, <w n="2.6">par</w> <w n="2.7">la</w> <w n="2.8">lutte</w> <w n="2.9">affaiblis</w>,</l>
					<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2">efforcent</w> <w n="3.3">de</w> <w n="3.4">briser</w> <w n="3.5">les</w> <w n="3.6">plis</w> <w n="3.7">et</w> <w n="3.8">les</w> <w n="3.9">replis</w></l>
					<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">monstrueux</w> <w n="4.3">serpents</w> <w n="4.4">que</w> <w n="4.5">la</w> <w n="4.6">fureur</w> <w n="4.7">anime</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ses</w> <w n="5.2">flancs</w> <w n="5.3">sont</w> <w n="5.4">déchirés</w>, <w n="5.5">mais</w> <w n="5.6">ô</w> <w n="5.7">torture</w> <w n="5.8">intime</w> !…</l>
					<l n="6" num="2.2"><w n="6.1">Pitié</w> !… <w n="6.2">Laocoon</w> <w n="6.3">voit</w> <w n="6.4">se</w> <w n="6.5">tordre</w> <w n="6.6">ses</w> <w n="6.7">fils</w>,</l>
					<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">un</w> <w n="7.3">bel</w> <w n="7.4">adolescent</w>, <w n="7.5">l</w>’<w n="7.6">autre</w> <w n="7.7">enfant</w>, <w n="7.8">jeune</w> <w n="7.9">lis</w>.</l>
					<l n="8" num="2.4"><w n="8.1">O</w> <w n="8.2">terre</w> <w n="8.3">et</w> <w n="8.4">cieux</w> ! <w n="8.5">leur</w> <w n="8.6">mort</w> <w n="8.7">est</w>-<w n="8.8">elle</w> <w n="8.9">légitime</w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Devant</w> <w n="9.2">ce</w> <w n="9.3">marbre</w> <w n="9.4">antique</w>, <w n="9.5">à</w> <w n="9.6">toi</w>, <w n="9.7">Peuple</w>-<w n="9.8">Héros</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Je</w> <w n="10.2">songe</w>, <w n="10.3">à</w> <w n="10.4">tes</w> <w n="10.5">enfants</w>, <w n="10.6">dont</w> <w n="10.7">le</w> <w n="10.8">sang</w> <w n="10.9">coule</w> <w n="10.10">à</w> <w n="10.11">flots</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Autour</w> <w n="11.2">de</w> <w n="11.3">vos</w> <w n="11.4">aussi</w> <w n="11.5">s</w>’<w n="11.6">enroulent</w> <w n="11.7">des</w> <w n="11.8">reptiles</w>…</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Vous</w> <w n="12.2">vaincrez</w>… <w n="12.3">Du</w> <w n="12.4">combat</w>, <w n="12.5">vaillant</w> <w n="12.6">soldat</w> <w n="12.7">de</w> <w n="12.8">Dieu</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Tu</w> <w n="13.2">sortiras</w> <w n="13.3">plus</w> <w n="13.4">pur</w>, <w n="13.5">comme</w> <w n="13.6">l</w>’<w n="13.7">or</w> <w n="13.8">sort</w> <w n="13.9">du</w> <w n="13.10">feu</w>.</l>
					<l n="14" num="4.3"><w n="14.1">Courage</w> ! <w n="14.2">à</w> <w n="14.3">tes</w> <w n="14.4">grandeurs</w> <w n="14.5">tes</w> <w n="14.6">maux</w> <w n="14.7">seront</w> <w n="14.8">utiles</w>.</l>
				</lg>
			</div></body></text></TEI>