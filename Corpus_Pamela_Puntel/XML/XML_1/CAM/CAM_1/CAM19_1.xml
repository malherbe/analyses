<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM19">
					<head type="main">THÉODORE KŒRNER</head>
					<head type="number">II</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Si</w> <w n="1.2">tu</w> <w n="1.3">pouvais</w> <w n="1.4">quitter</w> <w n="1.5">la</w> <w n="1.6">tombe</w>, <w n="1.7">tu</w> <w n="1.8">dirais</w> :</l>
						<l n="2" num="1.2">« <w n="2.1">Quels</w> <w n="2.2">triomphes</w> <w n="2.3">honteux</w>, <w n="2.4">et</w> <w n="2.5">quelle</w> <w n="2.6">ignominie</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Insensés</w>, <w n="3.2">dont</w> <w n="3.3">les</w> <w n="3.4">mains</w> <w n="3.5">fondent</w> <w n="3.6">la</w> <w n="3.7">tyrannie</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">prince</w> <w n="4.3">vous</w> <w n="4.4">enchaîne</w> <w n="4.5">à</w> <w n="4.6">ses</w> <w n="4.7">vils</w> <w n="4.8">intérêts</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1">Malheur</w> <w n="5.2">à</w> <w n="5.3">vous</w> ! <w n="5.4">vos</w> <w n="5.5">cieux</w>, <w n="5.6">vos</w> <w n="5.7">champs</w> <w n="5.8">et</w> <w n="5.9">vos</w> <w n="5.10">forêts</w></l>
						<l n="6" num="2.2"><w n="6.1">Pleurent</w> <w n="6.2">la</w> <w n="6.3">loyauté</w> <w n="6.4">de</w> <w n="6.5">vos</w> <w n="6.6">âmes</w> <w n="6.7">bannie</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Oserez</w>-<w n="7.2">vous</w> <w n="7.3">encor</w>, <w n="7.4">fils</w> <w n="7.5">de</w> <w n="7.6">la</w> <w n="7.7">Germanie</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Des</w> <w n="8.2">saintes</w> <w n="8.3">lois</w> <w n="8.4">du</w> <w n="8.5">beau</w> <w n="8.6">pénétrer</w> <w n="8.7">les</w> <w n="8.8">secrets</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <w n="9.1">O</w> <w n="9.2">profanation</w> <w n="9.3">de</w> <w n="9.4">l</w>’<w n="9.5">art</w>, <w n="9.6">de</w> <w n="9.7">la</w> <w n="9.8">science</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Est</w>-<w n="10.2">elle</w> <w n="10.3">donc</w> <w n="10.4">muette</w> <w n="10.5">en</w> <w n="10.6">vous</w> <w n="10.7">la</w> <w n="10.8">conscience</w> ?</l>
						<l n="11" num="3.3"><w n="11.1">Mais</w> <w n="11.2">nous</w> <w n="11.3">vous</w> <w n="11.4">renions</w>, <w n="11.5">vous</w> <w n="11.6">n</w>’<w n="11.7">êtes</w> <w n="11.8">pas</w> <w n="11.9">nos</w> <w n="11.10">fils</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1">« <w n="12.1">Était</w>-<w n="12.2">ce</w> <w n="12.3">pour</w> <w n="12.4">léguer</w> <w n="12.5">tant</w> <w n="12.6">d</w>’<w n="12.7">opprobre</w> <w n="12.8">à</w> <w n="12.9">vos</w> <w n="12.10">têtes</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Qu</w>’<w n="13.2">affrontant</w> <w n="13.3">du</w> <w n="13.4">canon</w> <w n="13.5">les</w> <w n="13.6">affreuses</w> <w n="13.7">tempêtes</w>,</l>
						<l n="14" num="4.3"><w n="14.1">A</w> <w n="14.2">l</w>’<w n="14.3">aigle</w> <w n="14.4">d</w>’<w n="14.5">Iéna</w> <w n="14.6">nous</w> <w n="14.7">jetions</w> <w n="14.8">nos</w> <w n="14.9">défis</w> ? »</l>
					</lg>
				</div></body></text></TEI>