<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM21">
				<head type="main">LA MORT POUR LA PATRIE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Murioï osa te phulla kaï anthea gignetaï ôrê</quote>
							<bibl>
								<name>(HOMÈRE)</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">aède</w> <w n="1.3">dont</w> <w n="1.4">le</w> <w n="1.5">chant</w> <w n="1.6">en</w> <w n="1.7">splendeur</w> <w n="1.8">se</w> <w n="1.9">déploie</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Homère</w> <w n="2.2">eut</w> <w n="2.3">dite</w> <w n="2.4">de</w> <w n="2.5">vous</w>, <w n="2.6">ô</w> <w n="2.7">jeunes</w> <w n="2.8">combattants</w>,</l>
						<l n="3" num="1.3">« <w n="3.1">Aussi</w> <w n="3.2">nombreux</w> <w n="3.3">que</w> <w n="3.4">fleurs</w> <w n="3.5">et</w> <w n="3.6">feuilles</w> <w n="3.7">au</w> <w n="3.8">printemps</w>. »</l>
						<l n="4" num="1.4"><w n="4.1">Mais</w> <w n="4.2">du</w> <w n="4.3">cruel</w> <w n="4.4">destin</w> <w n="4.5">combien</w> <w n="4.6">seront</w> <w n="4.7">la</w> <w n="4.8">proie</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">O</w> <w n="5.2">jeunesse</w> <w n="5.3">et</w> <w n="5.4">beauté</w>, <w n="5.5">que</w> <w n="5.6">couronne</w> <w n="5.7">la</w> <w n="5.8">joie</w>,</l>
						<l n="6" num="2.2"><w n="6.1">O</w> <w n="6.2">vaillants</w> <w n="6.3">cœurs</w>, <w n="6.4">de</w> <w n="6.5">gloire</w> <w n="6.6">et</w> <w n="6.7">d</w>’<w n="6.8">amour</w> <w n="6.9">palpitants</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Vous</w> <w n="7.2">aspirez</w> <w n="7.3">à</w> <w n="7.4">vivre</w>, <w n="7.5">et</w>, <w n="7.6">dans</w> <w n="7.7">quelques</w> <w n="7.8">instants</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">fer</w> <w n="8.3">entre</w> <w n="8.4">en</w> <w n="8.5">vos</w> <w n="8.6">chairs</w>, <w n="8.7">la</w> <w n="8.8">mitraille</w> <w n="8.9">vous</w> <w n="8.10">broie</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Enfants</w>, <w n="9.2">quand</w> <w n="9.3">la</w> <w n="9.4">lumière</w> <w n="9.5">est</w> <w n="9.6">ravie</w> <w n="9.7">à</w> <w n="9.8">vos</w> <w n="9.9">yeux</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Allez</w>-<w n="10.2">vous</w> <w n="10.3">tout</w> <w n="10.4">sanglants</w> <w n="10.5">habiter</w> <w n="10.6">d</w>’<w n="10.7">autres</w> <w n="10.8">cieux</w> ?</l>
						<l n="11" num="3.3"><w n="11.1">Ou</w> <w n="11.2">vous</w> <w n="11.3">absorbez</w>-<w n="11.4">vous</w> <w n="11.5">dans</w> <w n="11.6">la</w> <w n="11.7">nature</w> <w n="11.8">immense</w> ?</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">N</w>’<w n="12.2">êtes</w>-<w n="12.3">vous</w> <w n="12.4">plus</w> <w n="12.5">qu</w>’<w n="12.6">un</w> <w n="12.7">souffle</w> <w n="12.8">éteint</w> <w n="12.9">dans</w> <w n="12.10">l</w>’<w n="12.11">air</w>, <w n="12.12">un</w> <w n="12.13">flot</w></l>
						<l n="13" num="4.2"><w n="13.1">Perdu</w> <w n="13.2">dans</w> <w n="13.3">l</w>’<w n="13.4">Océan</w> ? <w n="13.5">Ou</w> <w n="13.6">croirons</w>-<w n="13.7">nous</w> <w n="13.8">plutôt</w></l>
						<l n="14" num="4.3"><w n="14.1">Qu</w>’<w n="14.2">une</w> <w n="14.3">immortelle</w> <w n="14.4">vie</w> <w n="14.5">au</w> <w n="14.6">sein</w> <w n="14.7">du</w> <w n="14.8">Dieu</w> <w n="14.9">commence</w> ?</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="15" num="1.1"><w n="15.1">Platon</w> <w n="15.2">l</w>’<w n="15.3">a</w> <w n="15.4">presenti</w>, <w n="15.5">le</w> <w n="15.6">Christ</w> <w n="15.7">révélé</w> :</l>
						<l n="16" num="1.2"><w n="16.1">La</w> <w n="16.2">vie</w> <w n="16.3">est</w> <w n="16.4">le</w> <w n="16.5">combat</w>. <w n="16.6">et</w> <w n="16.7">la</w> <w n="16.8">mort</w> <w n="16.9">la</w> <w n="16.10">couronne</w>.</l>
						<l n="17" num="1.3"><w n="17.1">L</w>’<w n="17.2">âme</w>, <w n="17.3">fille</w> <w n="17.4">des</w> <w n="17.5">cieux</w>, <w n="17.6">que</w> <w n="17.7">le</w> <w n="17.8">corps</w> <w n="17.9">environne</w>,</l>
						<l n="18" num="1.4"><w n="18.1">Est</w> <w n="18.2">un</w> <w n="18.3">rayon</w> <w n="18.4">divin</w> <w n="18.5">d</w>’<w n="18.6">un</w> <w n="18.7">nuage</w> <w n="18.8">voilé</w>.</l>
					</lg>
					<lg n="2">
						<l n="19" num="2.1"><w n="19.1">Lorsque</w> <w n="19.2">la</w> <w n="19.3">voix</w> <w n="19.4">d</w>’<w n="19.5">en</w> <w n="19.6">haut</w>, <w n="19.7">le</w> <w n="19.8">devoir</w> <w n="19.9">a</w> <w n="19.10">parlé</w>,</l>
						<l n="20" num="2.2"><w n="20.1">Des</w> <w n="20.2">passions</w> <w n="20.3">pour</w> <w n="20.4">elle</w> <w n="20.5">en</w> <w n="20.6">vain</w> <w n="20.7">le</w> <w n="20.8">bruit</w> <w n="20.9">résonne</w> ;</l>
						<l n="21" num="2.3"><w n="21.1">En</w> <w n="21.2">vain</w> <w n="21.3">des</w> <w n="21.4">sens</w> <w n="21.5">épais</w> <w n="21.6">le</w> <w n="21.7">contour</w> <w n="21.8">l</w>’<w n="21.9">emprisonne</w> :</l>
						<l n="22" num="2.4"><w n="22.1">Son</w> <w n="22.2">regard</w> <w n="22.3">va</w> <w n="22.4">plus</w> <w n="22.5">haut</w> <w n="22.6">que</w> <w n="22.7">le</w> <w n="22.8">dôme</w> <w n="22.9">étoilé</w>.</l>
					</lg>
					<lg n="3">
						<l n="23" num="3.1"><w n="23.1">Descendrait</w>-<w n="23.2">elle</w> <w n="23.3">un</w> <w n="23.4">jour</w> <w n="23.5">dans</w> <w n="23.6">une</w> <w n="23.7">sombre</w> <w n="23.8">fosse</w> ?</l>
						<l n="24" num="3.2"><w n="24.1">Non</w>, <w n="24.2">ta</w> <w n="24.3">parole</w> <w n="24.4">d</w>’<w n="24.5">or</w>, <w n="24.6">ô</w> <w n="24.7">Platon</w>, <w n="24.8">n</w>’<w n="24.9">est</w> <w n="24.10">pas</w> <w n="24.11">fausse</w> ;</l>
						<l n="25" num="3.3"><w n="25.1">Ta</w> <w n="25.2">promesse</w> <w n="25.3">sacrée</w>, <w n="25.4">ô</w> <w n="25.5">Christ</w> <w n="25.6">ne</w> <w n="25.7">trompe</w> <w n="25.8">pas</w>.</l>
					</lg>
					<lg n="4">
						<l n="26" num="4.1"><w n="26.1">Pour</w> <w n="26.2">nos</w> <w n="26.3">soldats</w> <w n="26.4">fleurit</w> <w n="26.5">la</w> <w n="26.6">palme</w> <w n="26.7">du</w> <w n="26.8">martyre</w>,</l>
						<l n="27" num="4.2"><w n="27.1">Dans</w> <w n="27.2">son</w> <w n="27.3">immense</w> <w n="27.4">amour</w> <w n="27.5">le</w> <w n="27.6">Dieu</w> <w n="27.7">bon</w> <w n="27.8">les</w> <w n="27.9">attire</w>,</l>
						<l n="28" num="4.3"><w n="28.1">Leur</w> <w n="28.2">héroïsme</w> <w n="28.3">saint</w> <w n="28.4">les</w> <w n="28.5">sauve</w> <w n="28.6">du</w> <w n="28.7">trépas</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>