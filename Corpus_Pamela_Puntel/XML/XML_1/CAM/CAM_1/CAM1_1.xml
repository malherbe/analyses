<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM1">
				<head type="main">A LA JEUNE GÉNÉRATION</head>
				<head type="sub">Remember</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Enfants</w>, <w n="1.2">souvenez</w>-<w n="1.3">vous</w> <w n="1.4">de</w> <w n="1.5">nos</w> <w n="1.6">malheurs</w> <w n="1.7">sans</w> <w n="1.8">nombre</w>,</l>
					<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">nos</w> <w n="2.3">drapeaux</w> <w n="2.4">en</w> <w n="2.5">deuil</w>, <w n="2.6">où</w> <w n="2.7">pend</w> <w n="2.8">une</w> <w n="2.9">crème</w> <w n="2.10">sombre</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">souillures</w> <w n="3.3">de</w> <w n="3.4">notre</w> <w n="3.5">sol</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Grandissez</w> <w n="4.2">pour</w> <w n="4.3">venger</w> <w n="4.4">ce</w> <w n="4.5">que</w> <w n="4.6">souffrent</w> <w n="4.7">vos</w> <w n="4.8">pères</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Pour</w> <w n="5.2">atteindre</w>, <w n="5.3">au</w>-<w n="5.4">delà</w> <w n="5.5">du</w> <w n="5.6">Rhin</w>, <w n="5.7">dans</w> <w n="5.8">leurs</w> <w n="5.9">repaires</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">héros</w> <w n="6.3">d</w> <w n="6.4">meurtre</w> <w n="6.5">et</w> <w n="6.6">du</w> <w n="6.7">vol</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Dans</w> <w n="7.2">vos</w> <w n="7.3">amusements</w>, <w n="7.4">dans</w> <w n="7.5">vos</w> <w n="7.6">joyeuses</w> <w n="7.7">fêtes</w>,</l>
					<l n="8" num="2.2"><w n="8.1">Songez</w> <w n="8.2">au</w> <w n="8.3">sang</w> <w n="8.4">versé</w>, <w n="8.5">songez</w> <w n="8.6">à</w> <w n="8.7">nos</w> <w n="8.8">défaites</w>,</l>
					<l n="9" num="2.3"><w n="9.1">Et</w>, <w n="9.2">saisis</w> <w n="9.3">d</w>’<w n="9.4">un</w> <w n="9.5">frisson</w> <w n="9.6">au</w> <w n="9.7">cœur</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Aspirez</w> <w n="10.2">à</w> <w n="10.3">ce</w> <w n="10.4">temps</w> <w n="10.5">où</w> <w n="10.6">vous</w> <w n="10.7">serez</w> <w n="10.8">de</w> <w n="10.9">taille</w></l>
					<l n="11" num="2.5"><w n="11.1">A</w> <w n="11.2">jouer</w> <w n="11.3">d</w>’<w n="11.4">autres</w> <w n="11.5">jeux</w>, <w n="11.6">les</w> <w n="11.7">jeux</w> <w n="11.8">de</w> <w n="11.9">la</w> <w n="11.10">bataille</w></l>
					<l n="12" num="2.6"><w n="12.1">Qui</w> <w n="12.2">rendra</w> <w n="12.3">le</w> <w n="12.4">pays</w> <w n="12.5">vainqueur</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Aux</w> <w n="13.2">heures</w> <w n="13.3">d</w>’<w n="13.4">étude</w> <w n="13.5">féconde</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Des</w> <w n="14.2">siècles</w> <w n="14.3">écoutez</w> <w n="14.4">la</w> <w n="14.5">voix</w> ;</l>
					<l n="15" num="3.3"><w n="15.1">Interrogez</w> <w n="15.2">l</w>’<w n="15.3">homme</w> <w n="15.4">et</w> <w n="15.5">le</w> <w n="15.6">monde</w>,</l>
					<l n="16" num="3.4"><w n="16.1">Et</w> <w n="16.2">sondez</w> <w n="16.3">les</w> <w n="16.4">divines</w> <w n="16.5">lois</w>.</l>
					<l n="17" num="3.5"><w n="17.1">Sous</w> <w n="17.2">les</w> <w n="17.3">cieux</w> <w n="17.4">la</w> <w n="17.5">pensée</w> <w n="17.6">est</w> <w n="17.7">reine</w> ;</l>
					<l n="18" num="3.6"><w n="18.1">De</w> <w n="18.2">sa</w> <w n="18.3">puissance</w> <w n="18.4">souveraine</w>,</l>
					<l n="19" num="3.7"><w n="19.1">Armez</w>-<w n="19.2">vous</w>, <w n="19.3">athlètes</w> <w n="19.4">constants</w> ;</l>
					<l n="20" num="3.8"><w n="20.1">Qu</w>’<w n="20.2">un</w> <w n="20.3">jour</w> <w n="20.4">de</w> <w n="20.5">ce</w> <w n="20.6">labeur</w> <w n="20.7">austère</w>,</l>
					<l n="21" num="3.9"><w n="21.1">Jaillisse</w>, <w n="21.2">comme</w> <w n="21.3">d</w>’<w n="21.4">un</w> <w n="21.5">cratère</w>,</l>
					<l n="22" num="3.10"><w n="22.1">La</w> <w n="22.2">vengeance</w> <w n="22.3">aux</w> <w n="22.4">traits</w> <w n="22.5">éclatants</w>.</l>
				</lg>
				<lg n="4">
					<l n="23" num="4.1"><w n="23.1">Enfants</w>, <w n="23.2">souvenez</w>-<w n="23.3">vous</w> — <w n="23.4">Et</w> <w n="23.5">quand</w> <w n="23.6">al</w> <w n="23.7">jeune</w> <w n="23.8">fille</w>,</l>
					<l n="24" num="4.2"><w n="24.1">Dont</w> <w n="24.2">le</w> <w n="24.3">charme</w> <w n="24.4">si</w> <w n="24.5">doux</w> <w n="24.6">comme</w> <w n="24.7">un</w> <w n="24.8">pur</w> <w n="24.9">rayon</w> <w n="24.10">brille</w>,</l>
					<l n="25" num="4.3"><w n="25.1">Troublera</w> <w n="25.2">vos</w> <w n="25.3">cœurs</w> <w n="25.4">enivrés</w> :</l>
					<l n="26" num="4.4"><w n="26.1">Quand</w> <w n="26.2">sa</w> <w n="26.3">lèvre</w>, <w n="26.4">fleur</w> <w n="26.5">d</w>’<w n="26.6">or</w>, <w n="26.7">s</w>’<w n="26.8">ouvrira</w> <w n="26.9">pour</w> <w n="26.10">vous</w> <w n="26.11">dire</w>,</l>
					<l n="27" num="4.5"><w n="27.1">O</w> <w n="27.2">fiancé</w>, <w n="27.3">pourquoi</w> <w n="27.4">ce</w> <w n="27.5">douloureux</w> <w n="27.6">sourire</w>,</l>
					<l n="28" num="4.6"><w n="28.1">Et</w> <w n="28.2">pourquoi</w> <w n="28.3">ces</w> <w n="28.4">regards</w> <w n="28.5">navrés</w> ?</l>
				</lg>
				<lg n="5">
					<l n="29" num="5.1"><w n="29.1">Des</w> <w n="29.2">désastres</w> <w n="29.3">publics</w> <w n="29.4">dans</w> <w n="29.5">mes</w> <w n="29.6">yeux</w> <w n="29.7">est</w> <w n="29.8">le</w> <w n="29.9">signe</w>.</l>
					<l n="30" num="5.2"><w n="30.1">Répondrez</w>-<w n="30.2">vous</w> ; <w n="30.3">de</w> <w n="30.4">toi</w> <w n="30.5">je</w> <w n="30.6">ne</w> <w n="30.7">serais</w> <w n="30.8">pas</w> <w n="30.9">digne</w></l>
					<l n="31" num="5.3"><w n="31.1">Si</w> <w n="31.2">j</w>’<w n="31.3">oubliais</w> <w n="31.4">ceux</w> <w n="31.5">qui</w> <w n="31.6">sont</w> <w n="31.7">morts</w> ;</l>
					<l n="32" num="5.4"><w n="32.1">Et</w> <w n="32.2">si</w>, <w n="32.3">sourd</w> <w n="32.4">à</w> <w n="32.5">l</w>’<w n="32.6">appel</w> <w n="32.7">de</w> <w n="32.8">la</w> <w n="32.9">sainte</w> <w n="32.10">patrie</w>,</l>
					<l n="33" num="5.5"><w n="33.1">Au</w> <w n="33.2">bonheur</w> <w n="33.3">de</w> <w n="33.4">l</w>’<w n="33.5">amour</w>, <w n="33.6">ô</w> <w n="33.7">ma</w> <w n="33.8">vierge</w> <w n="33.9">chérie</w>,</l>
					<l n="34" num="5.6"><w n="34.1">Je</w> <w n="34.2">m</w>’<w n="34.3">abandonnais</w> <w n="34.4">sans</w> <w n="34.5">remords</w>.</l>
				</lg>
				<lg n="6">
					<l n="35" num="6.1"><w n="35.1">Cachez</w>, <w n="35.2">sous</w> <w n="35.3">le</w> <w n="35.4">myrte</w> <w n="35.5">et</w> <w n="35.6">les</w> <w n="35.7">roses</w>,</l>
					<l n="36" num="6.2"><w n="36.1">Le</w> <w n="36.2">glaive</w> <w n="36.3">de</w> <w n="36.4">la</w> <w n="36.5">liberté</w>.</l>
					<l n="37" num="6.3"><w n="37.1">Qu</w>’<w n="37.2">à</w> <w n="37.3">vos</w> <w n="37.4">fronts</w> <w n="37.5">noblement</w> <w n="37.6">moroses</w></l>
					<l n="38" num="6.4"><w n="38.1">Se</w> <w n="38.2">joue</w> <w n="38.3">un</w> <w n="38.4">éclair</w> <w n="38.5">de</w> <w n="38.6">fierté</w>.</l>
					<l n="39" num="6.5"><w n="39.1">Réclamez</w> <w n="39.2">l</w>’<w n="39.3">heure</w> <w n="39.4">de</w> <w n="39.5">combattre</w>,</l>
					<l n="40" num="6.6"><w n="40.1">Et</w> <w n="40.2">ne</w> <w n="40.3">vous</w> <w n="40.4">laissez</w> <w n="40.5">pas</w> <w n="40.6">abattre</w></l>
					<l n="41" num="6.7"><w n="41.1">Par</w> <w n="41.2">tant</w> <w n="41.3">de</w> <w n="41.4">misère</w> <w n="41.5">et</w> <w n="41.6">de</w> <w n="41.7">deuil</w> ;</l>
					<l n="42" num="6.8"><w n="42.1">Car</w> <w n="42.2">de</w> <w n="42.3">l</w>’<w n="42.4">Allemagne</w> <w n="42.5">insolente</w></l>
					<l n="43" num="6.9"><w n="43.1">Némésis</w>, <w n="43.2">à</w> <w n="43.3">punir</w> <w n="43.4">trop</w> <w n="43.5">lente</w>,</l>
					<l n="44" num="6.10"><w n="44.1">Par</w> <w n="44.2">vos</w> <w n="44.3">mains</w> <w n="44.4">frappera</w> <w n="44.5">l</w>’<w n="44.6">orgueil</w>.</l>
				</lg>
				<lg n="7">
					<l n="45" num="7.1"><w n="45.1">Enfants</w>, <w n="45.2">souvenez</w>-<w n="45.3">vous</w> — <w n="45.4">Vous</w> <w n="45.5">êtes</w> <w n="45.6">purs</w> <w n="45.7">des</w> <w n="45.8">fautes</w></l>
					<l n="46" num="7.2"><w n="46.1">Qui</w> <w n="46.2">nous</w> <w n="46.3">ont</w> <w n="46.4">entraînés</w> <w n="46.5">des</w> <w n="46.6">cimes</w> <w n="46.7">les</w> <w n="46.8">plus</w> <w n="46.9">hautes</w></l>
					<l n="47" num="7.3"><w n="47.1">Dans</w> <w n="47.2">un</w> <w n="47.3">abîme</w> <w n="47.4">de</w> <w n="47.5">revers</w>.</l>
					<l n="48" num="7.4"><w n="48.1">Vos</w> <w n="48.2">cœurs</w> <w n="48.3">n</w>’<w n="48.4">ont</w> <w n="48.5">pas</w> <w n="48.6">connu</w> <w n="48.7">la</w> <w n="48.8">triste</w> <w n="48.9">défaillance</w>.</l>
					<l n="49" num="7.5"><w n="49.1">Vous</w>, <w n="49.2">l</w>’<w n="49.3">honneur</w> <w n="49.4">non</w> <w n="49.5">terni</w>, <w n="49.6">vous</w> <w n="49.7">la</w> <w n="49.8">jeune</w> <w n="49.9">vaillance</w>,</l>
					<l n="50" num="7.6"><w n="50.1">Vous</w> <w n="50.2">êtes</w> <w n="50.3">l</w>’<w n="50.4">arbre</w> <w n="50.5">aux</w> <w n="50.6">rameaux</w> <w n="50.7">verts</w>.</l>
				</lg>
				<lg n="8">
					<l n="51" num="8.1"><w n="51.1">Le</w> <w n="51.2">vieux</w> <w n="51.3">tronc</w> <w n="51.4">est</w> <w n="51.5">tombé</w> <w n="51.6">sous</w> <w n="51.7">la</w> <w n="51.8">hache</w> <w n="51.9">germaine</w> ;</l>
					<l n="52" num="8.2"><w n="52.1">Mais</w> <w n="52.2">de</w> <w n="52.3">nos</w> <w n="52.4">ennemis</w> <w n="52.5">la</w> <w n="52.6">fureur</w> <w n="52.7">inhumaine</w></l>
					<l n="53" num="8.3"><w n="53.1">Frémira</w> <w n="53.2">contre</w> <w n="53.3">vous</w> <w n="53.4">en</w> <w n="53.5">vain</w>.</l>
					<l n="54" num="8.4"><w n="54.1">Vous</w> <w n="54.2">croitrez</w>, <w n="54.3">débordant</w> <w n="54.4">de</w> <w n="54.5">généreuse</w> <w n="54.6">sève</w>,</l>
					<l n="55" num="8.5"><w n="55.1">Comme</w> <w n="55.2">sur</w> <w n="55.3">la</w> <w n="55.4">montagne</w> <w n="55.5">un</w> <w n="55.6">grand</w> <w n="55.7">chêne</w> <w n="55.8">s</w>’<w n="55.9">élève</w></l>
					<l n="56" num="8.6"><w n="56.1">Tout</w> <w n="56.2">baigné</w> <w n="56.3">de</w> <w n="56.4">l</w>’<w n="56.5">éther</w> <w n="56.6">divin</w>.</l>
				</lg>
				<lg n="9">
					<l n="57" num="9.1"><w n="57.1">A</w> <w n="57.2">vous</w> <w n="57.3">les</w> <w n="57.4">immortels</w> <w n="57.5">trophées</w>,</l>
					<l n="58" num="9.2"><w n="58.1">Les</w> <w n="58.2">patriotiques</w> <w n="58.3">exploits</w>,</l>
					<l n="59" num="9.3"><w n="59.1">Et</w> <w n="59.2">nos</w> <w n="59.3">discordes</w> <w n="59.4">étouffées</w></l>
					<l n="60" num="9.4"><w n="60.1">Par</w> <w n="60.2">votre</w> <w n="60.3">saint</w> <w n="60.4">amour</w> <w n="60.5">des</w> <w n="60.6">lois</w>.</l>
					<l n="61" num="9.5"><w n="61.1">A</w> <w n="61.2">vous</w>, <w n="61.3">la</w> <w n="61.4">gloire</w> <w n="61.5">que</w> <w n="61.6">ne</w> <w n="61.7">souille</w></l>
					<l n="62" num="9.6"><w n="62.1">Aucun</w> <w n="62.2">intérêt</w> <w n="62.3">de</w> <w n="62.4">sa</w> <w n="62.5">rouille</w> ;</l>
					<l n="63" num="9.7"><w n="63.1">A</w> <w n="63.2">vous</w> <w n="63.3">les</w> <w n="63.4">plus</w> <w n="63.5">nobles</w> <w n="63.6">ardeurs</w> ;</l>
					<l n="64" num="9.8"><w n="64.1">En</w> <w n="64.2">vous</w>, <w n="64.3">intrépide</w> <w n="64.4">jeunesse</w>,</l>
					<l n="65" num="9.9"><w n="65.1">Il</w> <w n="65.2">faut</w> <w n="65.3">que</w> <w n="65.4">la</w> <w n="65.5">France</w> <w n="65.6">renaisse</w></l>
					<l n="66" num="9.10"><w n="66.1">Dans</w> <w n="66.2">ses</w> <w n="66.3">héroïques</w> <w n="66.4">grandeurs</w>.</l>
				</lg>
				<lg n="10">
					<l n="67" num="10.1"><w n="67.1">Enfants</w>, <w n="67.2">souvenez</w>-<w n="67.3">vous</w>. — <w n="67.4">Sur</w> <w n="67.5">son</w> <w n="67.6">lit</w> <w n="67.7">d</w>’<w n="67.8">agonie</w></l>
					<l n="68" num="10.2"><w n="68.1">La</w> <w n="68.2">France</w> <w n="68.3">se</w> <w n="68.4">soulève</w>, <w n="68.5">et</w>, <w n="68.6">sanglante</w>, <w n="68.7">honnie</w>,</l>
					<l n="69" num="10.3"><w n="69.1">Vous</w> <w n="69.2">regarde</w> <w n="69.3">et</w> <w n="69.4">dit</w> : «<w n="69.5">Je</w> <w n="69.6">vivrai</w>.</l>
					<l n="70" num="10.4"><w n="70.1">D</w>’<w n="70.2">un</w> <w n="70.3">passé</w> <w n="70.4">condamné</w> <w n="70.5">cette</w> <w n="70.6">heure</w> <w n="70.7">est</w> <w n="70.8">la</w> <w n="70.9">dernière</w>.</l>
					<l n="71" num="10.5"><w n="71.1">Mes</w> <w n="71.2">fils</w> <w n="71.3">sont</w> <w n="71.4">l</w>’<w n="71.5">avenir</w> <w n="71.6">du</w> <w n="71.7">monde</w>, <w n="71.8">et</w> <w n="71.9">leur</w> <w n="71.10">bannière</w></l>
					<l n="72" num="10.6"><w n="72.1">Est</w> <w n="72.2">celle</w> <w n="72.3">du</w> <w n="72.4">juste</w> <w n="72.5">et</w> <w n="72.6">du</w> <w n="72.7">vrai</w>.»</l>
				</lg>
				<lg n="11">
					<l n="73" num="11.1"><w n="73.1">Vous</w> <w n="73.2">vaincrez</w> <w n="73.3">par</w> <w n="73.4">l</w>’<w n="73.5">effort</w> <w n="73.6">et</w> <w n="73.7">par</w> <w n="73.8">le</w> <w n="73.9">sacrifice</w>.</l>
					<l n="74" num="11.2"><w n="74.1">Des</w> <w n="74.2">molles</w> <w n="74.3">passions</w> <w n="74.4">repoussez</w> <w n="74.5">l</w>’<w n="74.6">artifice</w>.</l>
					<l n="75" num="11.3"><w n="75.1">Gardez</w> <w n="75.2">à</w> <w n="75.3">l</w>’<w n="75.4">âme</w> <w n="75.5">son</w> <w n="75.6">pouvoir</w>.</l>
					<l n="76" num="11.4"><w n="76.1">Vivre</w> <w n="76.2">n</w>’<w n="76.3">est</w> <w n="76.4">pas</w> <w n="76.5">cueillir</w> <w n="76.6">des</w> <w n="76.7">fleurs</w> <w n="76.8">dans</w> <w n="76.9">une</w> <w n="76.10">plaine</w>,</l>
					<l n="77" num="11.5"><w n="77.1">Ni</w> <w n="77.2">boire</w> <w n="77.3">en</w> <w n="77.4">un</w> <w n="77.5">banquet</w> <w n="77.6">la</w> <w n="77.7">joie</w> <w n="77.8">à</w> <w n="77.9">coup</w> <w n="77.10">pleine</w> ;</l>
					<l n="78" num="11.6"><w n="78.1">C</w>’<w n="78.2">est</w> <w n="78.3">s</w>’<w n="78.4">immoler</w> <w n="78.5">à</w> <w n="78.6">son</w> <w n="78.7">devoir</w>.</l>
				</lg>
				<lg n="12">
					<l n="79" num="12.1"><w n="79.1">Devoir</w>, <w n="79.2">patrie</w>, <w n="79.3">honneur</w> <w n="79.4">sévère</w>,</l>
					<l n="80" num="12.2"><w n="80.1">Et</w> <w n="80.2">liberté</w> <w n="80.3">qui</w> <w n="80.4">ne</w> <w n="80.5">meurt</w> <w n="80.6">pas</w>,</l>
					<l n="81" num="12.3"><w n="81.1">Tout</w> <w n="81.2">ce</w> <w n="81.3">que</w> <w n="81.4">votre</w> <w n="81.5">cœur</w> <w n="81.6">révère</w></l>
					<l n="82" num="12.4"><w n="82.1">Vous</w> <w n="82.2">convie</w> <w n="82.3">aux</w> <w n="82.4">sanglants</w> <w n="82.5">combats</w>.</l>
					<l n="83" num="12.5"><w n="83.1">De</w> <w n="83.2">tant</w> <w n="83.3">de</w> <w n="83.4">cités</w> <w n="83.5">ravagées</w></l>
					<l n="84" num="12.6"><w n="84.1">Et</w> <w n="84.2">de</w> <w n="84.3">familles</w> <w n="84.4">outragées</w></l>
					<l n="85" num="12.7"><w n="85.1">La</w> <w n="85.2">vengeance</w> <w n="85.3">est</w> <w n="85.4">l</w>’<w n="85.5">unique</w> <w n="85.6">vœu</w>.</l>
					<l n="86" num="12.8"><w n="86.1">Vengeance</w>, <w n="86.2">dit</w> <w n="86.3">le</w> <w n="86.4">vent</w> <w n="86.5">qui</w> <w n="86.6">passe</w> ;</l>
					<l n="87" num="12.9"><w n="87.1">L</w>’<w n="87.2">étoile</w>, <w n="87.3">brillant</w> <w n="87.4">dans</w> <w n="87.5">l</w>’<w n="87.6">espace</w>,</l>
					<l n="88" num="12.10"><w n="88.1">Écrit</w> <w n="88.2">Vengeance</w> <w n="88.3">en</w> <w n="88.4">traits</w> <w n="88.5">de</w> <w n="88.6">feu</w> !</l>
				</lg>
			</div></body></text></TEI>