<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM3">
				<head type="main">CHANT DE DÉPART <lb></lb>DES FRANCS-TIREURS ROUSSILLONNAIS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Alerte</w> ! <w n="1.2">Francs</w>-<w n="1.3">tireurs</w>, <w n="1.4">alerte</w> !</l>
					<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">aube</w> <w n="2.3">a</w> <w n="2.4">blanchi</w> <w n="2.5">le</w> <w n="2.6">ciel</w> : <w n="2.7">allons</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Sur</w> <w n="3.2">les</w> <w n="3.3">coteaux</w>, <w n="3.4">dans</w> <w n="3.5">les</w> <w n="3.6">vallons</w>,</l>
					<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">chasse</w> <w n="4.3">homicide</w> <w n="4.4">est</w> <w n="4.5">ouverte</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Alerte</w> ! <w n="5.2">Francs</w>-<w n="5.3">tireurs</w>, <w n="5.4">alerte</w> !</l>
					<l n="6" num="1.6"><w n="6.1">L</w>’<w n="6.2">aube</w> <w n="6.3">a</w> <w n="6.4">blanchi</w> <w n="6.5">le</w> <w n="6.6">ciel</w> : <w n="6.7">allons</w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Elle</w> <w n="7.2">saigne</w>, <w n="7.3">la</w> <w n="7.4">noble</w> <w n="7.5">France</w> !</l>
					<l n="8" num="2.2"><w n="8.1">Amis</w>, <w n="8.2">courons</w> <w n="8.3">à</w> <w n="8.4">son</w> <w n="8.5">secours</w>.</l>
					<l n="9" num="2.3"><w n="9.1">Prométhée</w> <w n="9.2">en</w> <w n="9.3">proie</w> <w n="9.4">aux</w> <w n="9.5">vautours</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Le</w> <w n="10.2">grand</w> <w n="10.3">peuple</w> <w n="10.4">est</w> <w n="10.5">dans</w> <w n="10.6">la</w> <w n="10.7">souffrance</w>.</l>
					<l n="11" num="2.5"><w n="11.1">Elle</w> <w n="11.2">saigne</w> <w n="11.3">la</w> <w n="11.4">noble</w> <w n="11.5">France</w> !</l>
					<l n="12" num="2.6"><w n="12.1">Amis</w>, <w n="12.2">courons</w> <w n="12.3">à</w> <w n="12.4">son</w> <w n="12.5">secours</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Blessée</w>, <w n="13.2">elle</w> <w n="13.3">combat</w> <w n="13.4">encore</w>.</l>
					<l n="14" num="3.2"><w n="14.1">Dans</w> <w n="14.2">nos</w> <w n="14.3">cœurs</w> <w n="14.4">palpite</w> <w n="14.5">le</w> <w n="14.6">sein</w>.</l>
					<l n="15" num="3.3"><w n="15.1">Répandons</w> <w n="15.2">le</w> <w n="15.3">sang</w> <w n="15.4">prussien</w>.</l>
					<l n="16" num="3.4"><w n="16.1">Vengeons</w> <w n="16.2">la</w> : <w n="16.3">sa</w> <w n="16.4">voix</w> <w n="16.5">nous</w> <w n="16.6">implore</w>.</l>
					<l n="17" num="3.5"><w n="17.1">Blessée</w>, <w n="17.2">elle</w> <w n="17.3">combat</w> <w n="17.4">encore</w>.</l>
					<l n="18" num="3.6"><w n="18.1">Dans</w> <w n="18.2">nos</w> <w n="18.3">cœurs</w> <w n="18.4">palpite</w> <w n="18.5">le</w> <w n="18.6">sien</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Que</w> <w n="19.2">chaque</w> <w n="19.3">arbre</w>, <w n="19.4">que</w> <w n="19.5">chaque</w> <w n="19.6">roche</w></l>
					<l n="20" num="4.2"><w n="20.1">Envoie</w> <w n="20.2">une</w> <w n="20.3">balle</w> <w n="20.4">aux</w> <w n="20.5">Uhlans</w>.</l>
					<l n="21" num="4.3"><w n="21.1">Nous</w> <w n="21.2">saurons</w>, <w n="21.3">prompts</w> <w n="21.4">et</w> <w n="21.5">vigilants</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Des</w> <w n="22.2">ennemis</w> <w n="22.3">guetter</w> <w n="22.4">l</w>’<w n="22.5">approche</w>.</l>
					<l n="23" num="4.5"><w n="23.1">Que</w> <w n="23.2">chaque</w> <w n="23.3">arbre</w>, <w n="23.4">chaque</w> <w n="23.5">roche</w></l>
					<l n="24" num="4.6"><w n="24.1">Envoie</w> <w n="24.2">une</w> <w n="24.3">balle</w> <w n="24.4">aux</w> <w n="24.5">Uhlans</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Le</w> <w n="25.2">fer</w> <w n="25.3">détruit</w>, <w n="25.4">la</w> <w n="25.5">mai</w> <w n="25.6">profane</w></l>
					<l n="26" num="5.2"><w n="26.1">Tout</w> <w n="26.2">ce</w> <w n="26.3">qui</w> <w n="26.4">faisait</w> <w n="26.5">notre</w> <w n="26.6">orgueil</w>.</l>
					<l n="27" num="5.3"><w n="27.1">Nos</w> <w n="27.2">campagnes</w> <w n="27.3">sont</w> <w n="27.4">dans</w> <w n="27.5">le</w> <w n="27.6">deuil</w> ;</l>
					<l n="28" num="5.4"><w n="28.1">Le</w> <w n="28.2">trépas</w> <w n="28.3">sur</w> <w n="28.4">nos</w> <w n="28.5">cités</w> <w n="28.6">plane</w>.</l>
					<l n="29" num="5.5"><w n="29.1">Le</w> <w n="29.2">fer</w> <w n="29.3">détruit</w>, <w n="29.4">la</w> <w n="29.5">main</w> <w n="29.6">profane</w></l>
					<l n="30" num="5.6"><w n="30.1">Tout</w> <w n="30.2">ce</w> <w n="30.3">qui</w> <w n="30.4">faisait</w> <w n="30.5">notre</w> <w n="30.6">orgueil</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">L</w>’<w n="31.2">Allemagne</w> <w n="31.3">à</w> <w n="31.4">nos</w> <w n="31.5">maux</w> <w n="31.6">insulte</w> ;</l>
					<l n="32" num="6.2"><w n="32.1">Elle</w> <w n="32.2">rit</w> <w n="32.3">de</w> <w n="32.4">nos</w> <w n="32.5">droits</w> <w n="32.6">sacrés</w>.</l>
					<l n="33" num="6.3"><w n="33.1">Gloire</w> <w n="33.2">à</w> <w n="33.3">nos</w> <w n="33.4">frères</w> <w n="33.5">massacrés</w>,</l>
					<l n="34" num="6.4"><w n="34.1">A</w> <w n="34.2">la</w> <w n="34.3">justice</w>, <w n="34.4">notre</w> <w n="34.5">culte</w> !</l>
					<l n="35" num="6.5"><w n="35.1">L</w>’<w n="35.2">Allemagne</w> <w n="35.3">à</w> <w n="35.4">nos</w> <w n="35.5">maux</w> <w n="35.6">insulte</w> ;</l>
					<l n="36" num="6.6"><w n="36.1">Elle</w> <w n="36.2">rit</w> <w n="36.3">de</w> <w n="36.4">nos</w> <w n="36.5">droits</w> <w n="36.6">sacrés</w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">A</w>-<w n="37.2">t</w>-<w n="37.3">elle</w> <w n="37.4">oublié</w> <w n="37.5">que</w> <w n="37.6">ses</w> <w n="37.7">fleuves</w></l>
					<l n="38" num="7.2"><w n="38.1">Ont</w> <w n="38.2">reflété</w> <w n="38.3">nos</w> <w n="38.4">trois</w> <w n="38.5">couleurs</w> ?</l>
					<l n="39" num="7.3"><w n="39.1">Lorraine</w>, <w n="39.2">Alsace</w>, <w n="39.3">vos</w> <w n="39.4">douleurs</w></l>
					<l n="40" num="7.4"><w n="40.1">Sont</w> <w n="40.2">de</w> <w n="40.3">passagères</w> <w n="40.4">épreuves</w>.</l>
					<l n="41" num="7.5"><w n="41.1">A</w>-<w n="41.2">t</w>-<w n="41.3">elle</w> <w n="41.4">oublié</w> <w n="41.5">que</w> <w n="41.6">ses</w> <w n="41.7">fleuves</w></l>
					<l n="42" num="7.6"><w n="42.1">Ont</w> <w n="42.2">reflété</w> <w n="42.3">nos</w> <w n="42.4">trois</w> <w n="42.5">couleurs</w> ?</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Ils</w> <w n="43.2">ont</w> <w n="43.3">dit</w> <w n="43.4">que</w> <w n="43.5">la</w> <w n="43.6">France</w> <w n="43.7">sombre</w>.</l>
					<l n="44" num="8.2"><w n="44.1">La</w> <w n="44.2">France</w> <w n="44.3">sombrer</w>… <w n="44.4">non</w>, <w n="44.5">jamais</w> !</l>
					<l n="45" num="8.3"><w n="45.1">Dieu</w> <w n="45.2">lui</w> <w n="45.3">rendra</w> <w n="45.4">les</w> <w n="45.5">grands</w> <w n="45.6">sommets</w>.</l>
					<l n="46" num="8.4"><w n="46.1">Elle</w> <w n="46.2">sortira</w> <w n="46.3">de</w> <w n="46.4">cette</w> <w n="46.5">ombre</w>.</l>
					<l n="47" num="8.5"><w n="47.1">Ils</w> <w n="47.2">ont</w> <w n="47.3">dit</w> <w n="47.4">que</w> <w n="47.5">la</w> <w n="47.6">France</w> <w n="47.7">sombre</w>.</l>
					<l n="48" num="8.6"><w n="48.1">La</w> <w n="48.2">France</w> <w n="48.3">sombrer</w>… <w n="48.4">non</w>, <w n="48.5">jamais</w> !</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">Les</w> <w n="49.2">hordes</w> <w n="49.3">souillant</w> <w n="49.4">ce</w> <w n="49.5">sol</w> <w n="49.6">libre</w></l>
					<l n="50" num="9.2"><w n="50.1">S</w>’<w n="50.2">engloutiront</w> <w n="50.3">sous</w> <w n="50.4">un</w> <w n="50.5">volcan</w>.</l>
					<l n="51" num="9.3"><w n="51.1">La</w> <w n="51.2">Patrie</w> <w n="51.3">armée</w> <w n="51.4">est</w> <w n="51.5">un</w> <w n="51.6">camp</w>,</l>
					<l n="52" num="9.4"><w n="52.1">Et</w> <w n="52.2">tout</w> <w n="52.3">cœur</w> <w n="52.4">de</w> <w n="52.5">colère</w> <w n="52.6">vibre</w>.</l>
					<l n="53" num="9.5"><w n="53.1">Les</w> <w n="53.2">hordes</w> <w n="53.3">souillant</w> <w n="53.4">ce</w> <w n="53.5">sol</w> <w n="53.6">libre</w></l>
					<l n="54" num="9.6"><w n="54.1">S</w>’<w n="54.2">engloutiront</w> <w n="54.3">sous</w> <w n="54.4">un</w> <w n="54.5">volcan</w>.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1"><w n="55.1">Compagnons</w>, <w n="55.2">espoir</w> <w n="55.3">et</w> <w n="55.4">courage</w> !</l>
					<l n="56" num="10.2"><w n="56.1">Le</w> <w n="56.2">clairon</w> <w n="56.3">sonne</w>, <w n="56.4">il</w> <w n="56.5">faut</w> <w n="56.6">partir</w>,</l>
					<l n="57" num="10.3"><w n="57.1">Intrépides</w>, <w n="57.2">adroits</w> <w n="57.3">au</w> <w n="57.4">tir</w>,</l>
					<l n="58" num="10.4"><w n="58.1">Hâtons</w> <w n="58.2">notre</w> <w n="58.3">sanglant</w> <w n="58.4">ouvrage</w></l>
					<l n="59" num="10.5"><w n="59.1">Compagnons</w>, <w n="59.2">espoir</w> <w n="59.3">et</w> <w n="59.4">courage</w> !</l>
					<l n="60" num="10.6"><w n="60.1">Le</w> <w n="60.2">clairon</w> <w n="60.3">sonne</w>, <w n="60.4">il</w> <w n="60.5">faut</w> <w n="60.6">partir</w>.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1"><w n="61.1">Adieu</w>, <w n="61.2">terre</w> <w n="61.3">roussillonnaise</w>,</l>
					<l n="62" num="11.2"><w n="62.1">Vertes</w> <w n="62.2">plaines</w>, <w n="62.3">superbes</w> <w n="62.4">monts</w> !</l>
					<l n="63" num="11.3"><w n="63.1">Mer</w>, <w n="63.2">aux</w> <w n="63.3">flots</w> <w n="63.4">bleus</w>, <w n="63.5">que</w> <w n="63.6">nous</w> <w n="63.7">aimons</w>,</l>
					<l n="64" num="11.4"><w n="64.1">Doux</w> <w n="64.2">bords</w> <w n="64.3">où</w> <w n="64.4">la</w> <w n="64.5">vague</w> <w n="64.6">s</w>’<w n="64.7">apaise</w> !</l>
					<l n="65" num="11.5"><w n="65.1">Adieu</w>, <w n="65.2">terre</w> <w n="65.3">roussillonnaise</w>,</l>
					<l n="66" num="11.6"><w n="66.1">Vertes</w> <w n="66.2">plaines</w>, <w n="66.3">superbes</w> <w n="66.4">monts</w> !</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1"><w n="67.1">O</w> <w n="67.2">mères</w>, <w n="67.3">sœurs</w> <w n="67.4">et</w> <w n="67.5">fiancées</w>,</l>
					<l n="68" num="12.2"><w n="68.1">Au</w> <w n="68.2">revoir</w> !… <w n="68.3">Mais</w> <w n="68.4">reviendrons</w>-<w n="68.5">nous</w> ?</l>
					<l n="69" num="12.3"><w n="69.1">Pour</w> <w n="69.2">vous</w>, <w n="69.3">objets</w> <w n="69.4">si</w> <w n="69.5">chers</w>, <w n="69.6">pour</w> <w n="69.7">vous</w></l>
					<l n="70" num="12.4"><w n="70.1">Seront</w> <w n="70.2">nos</w> <w n="70.3">suprêmes</w> <w n="70.4">pensées</w>.</l>
					<l n="71" num="12.5"><w n="71.1">O</w> <w n="71.2">mères</w>, <w n="71.3">sœurs</w> <w n="71.4">et</w> <w n="71.5">fiancées</w>,</l>
					<l n="72" num="12.6"><w n="72.1">Au</w> <w n="72.2">revoir</w> !… <w n="72.3">Mais</w> <w n="72.4">reviendrons</w>-<w n="72.5">nous</w> ?</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1"><w n="73.1">Qu</w>’<w n="73.2">importe</w> <w n="73.3">la</w> <w n="73.4">mort</w> <w n="73.5">à</w> <w n="73.6">qui</w> <w n="73.7">tombe</w></l>
					<l n="74" num="13.2"><w n="74.1">Pour</w> <w n="74.2">le</w> <w n="74.3">pays</w> <w n="74.4">de</w> <w n="74.5">ses</w> <w n="74.6">aïeux</w> ?</l>
					<l n="75" num="13.3"><w n="75.1">A</w> <w n="75.2">son</w> <w n="75.3">âme</w> <w n="75.4">s</w>’<w n="75.5">ouvrent</w> <w n="75.6">les</w> <w n="75.7">cieux</w>.</l>
					<l n="76" num="13.4"><w n="76.1">La</w> <w n="76.2">liberté</w> <w n="76.3">croit</w> <w n="76.4">sur</w> <w n="76.5">sa</w> <w n="76.6">tombe</w></l>
					<l n="77" num="13.5"><w n="77.1">Qu</w>’<w n="77.2">importe</w> <w n="77.3">la</w> <w n="77.4">mort</w> <w n="77.5">à</w> <w n="77.6">qui</w> <w n="77.7">tombe</w></l>
					<l n="78" num="13.6"><w n="78.1">Pour</w> <w n="78.2">le</w> <w n="78.3">pays</w> <w n="78.4">de</w> <w n="78.5">ses</w> <w n="78.6">aïeux</w> ?</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1"><w n="79.1">Alerte</w> ! <w n="79.2">Francs</w>-<w n="79.3">tireurs</w>, <w n="79.4">alerte</w> !</l>
					<l n="80" num="14.2"><w n="80.1">L</w>’<w n="80.2">aube</w> <w n="80.3">a</w> <w n="80.4">blanchi</w> <w n="80.5">le</w> <w n="80.6">ciel</w> : <w n="80.7">allons</w> !</l>
					<l n="81" num="14.3"><w n="81.1">Sur</w> <w n="81.2">les</w> <w n="81.3">coteaux</w>, <w n="81.4">dans</w> <w n="81.5">les</w> <w n="81.6">vallons</w>,</l>
					<l n="82" num="14.4"><w n="82.1">La</w> <w n="82.2">chasse</w> <w n="82.3">homicide</w> <w n="82.4">est</w> <w n="82.5">ouverte</w>.</l>
					<l n="83" num="14.5"><w n="83.1">Alerte</w> ! <w n="83.2">Francs</w>-<w n="83.3">tireurs</w>, <w n="83.4">alerte</w> !</l>
					<l n="84" num="14.6"><w n="84.1">L</w>’<w n="84.2">aube</w> <w n="84.3">a</w> <w n="84.4">blanchi</w> <w n="84.5">le</w> <w n="84.6">ciel</w> : <w n="84.7">allons</w> !</l>
				</lg>
				<ab type="dot">──────────────────────────────────────────────────</ab>
				<p>
					Cette pièce insérée, le Ier octobre, dans l’Indépendant des Pyrénées-
					Orientales, a été composé à l’occasion du départ des deux premières
					compagnies de Francs-tireurs roussillonnais. Elles font partie de
					l’armée de l’Est ; elles se sont distinguées par leur belle conduite
					et ont été citées plusieurs fois à l’ordre du jour.
				</p>
				<ab type="dot">──────────────────────────────────────────────────</ab>
				<p>
					L’Indicateur de l’Hérault a reproduit, dans le n° du 7 octobre, ce
					Chant de Départ, et l’a fait précéder de l’appréciation suivante :
					« Nous venons de lire dans l’Indépendant des Pyrénées-Orientales et
					nous reproduisons le Chant de Départ des Francs-tireurs roussillonnais,
					que M. Aimé CAMP, Inspecteur d’Académie à Perpignan, vient d’écrire.
					Nous ne donnons pas la pièce de M. A. CAMP pour qu’on en apprécie la
					forme savamment élégante et pure, ni pour que l’on constate qu’elle
					est bien réellement une des meilleures qu’ait inspirées le même sujet.
					Nous la citons comme l’expression patriotique du sentiment français.
					Les Francs-tireurs de tous les départements peuvent la redire pour
					leur compte. Ces strophes pittoresques, harmonieuses et vibrantes
					sont de celles que toute mémoire retient, parce qu’elles parlent
					à l’âme de tous.
					« Si quelque jeune compositeurs de talent prêtait à ces vers émus et
					charmants les ailes de la musique, ils formeraient un hymne d’un effet
					profond. Supposez que des voix jeunes et sonores répètent cet hymne,
					dès l’aube, à tous les échos des vallons, et les paysans qui l’entendent
					tressaillent et se rappellent la Patrie ! Ils frissonnent, ils se lèvent…»
				</p>
			</div></body></text></TEI>