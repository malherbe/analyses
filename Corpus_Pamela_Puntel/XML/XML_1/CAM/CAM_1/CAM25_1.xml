<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM25">
				<head type="main">LA CONCORDE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Deux</w> <w n="1.2">forces</w> <w n="1.3">règnent</w> : <w n="1.4">l</w>’<w n="1.5">une</w> <w n="1.6">est</w> <w n="1.7">des</w> <w n="1.8">peuples</w> <w n="1.9">la</w> <w n="1.10">vie</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Elle</w> <w n="2.2">affermit</w> <w n="2.3">les</w> <w n="2.4">lois</w>, <w n="2.5">elle</w> <w n="2.6">étouffe</w> <w n="2.7">l</w>’<w n="2.8">envie</w></l>
					<l n="3" num="1.3"><w n="3.1">Avec</w> <w n="3.2">les</w> <w n="3.3">soupçons</w> <w n="3.4">décevants</w>.</l>
					<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">autre</w> <w n="4.3">apporte</w> <w n="4.4">la</w> <w n="4.5">mort</w> <w n="4.6">et</w> <w n="4.7">ne</w> <w n="4.8">sait</w> <w n="4.9">que</w> <w n="4.10">dissoudre</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">les</w> <w n="5.3">sociétés</w> <w n="5.4">tombant</w> <w n="5.5">comme</w> <w n="5.6">la</w> <w n="5.7">foudre</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Elle</w> <w n="6.2">en</w> <w n="6.3">jette</w> <w n="6.4">la</w> <w n="6.5">cendre</w> <w n="6.6">aux</w> <w n="6.7">vents</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Frères</w>, <w n="7.2">qui</w> <w n="7.3">du</w> <w n="7.4">malheur</w> <w n="7.5">vidons</w> <w n="7.6">la</w> <w n="7.7">coupe</w> <w n="7.8">amère</w>,</l>
					<l n="8" num="2.2"><w n="8.1">Ne</w> <w n="8.2">songeons</w> <w n="8.3">qu</w>’<w n="8.4">à</w> <w n="8.5">sauver</w> <w n="8.6">la</w> <w n="8.7">France</w>, <w n="8.8">notre</w> <w n="8.9">mère</w> :</l>
					<l n="9" num="2.3"><w n="9.1">Son</w> <w n="9.2">esprit</w> <w n="9.3">nous</w> <w n="9.4">pénètre</w> <w n="9.5">tous</w>.</l>
					<l n="10" num="2.4"><w n="10.1">Son</w> <w n="10.2">courage</w> <w n="10.3">héroïque</w> <w n="10.4">a</w> <w n="10.5">passé</w> <w n="10.6">dans</w> <w n="10.7">nos</w> <w n="10.8">âmes</w> ;</l>
					<l n="11" num="2.5"><w n="11.1">Des</w> <w n="11.2">plus</w> <w n="11.3">purs</w> <w n="11.4">dévoûments</w> <w n="11.5">qu</w>’<w n="11.6">il</w> <w n="11.7">allume</w> <w n="11.8">les</w> <w n="11.9">flammes</w>,</l>
					<l n="12" num="2.6"><w n="12.1">Fils</w> <w n="12.2">de</w> <w n="12.3">la</w> <w n="12.4">Patrie</w>, <w n="12.5">aimons</w>-<w n="12.6">nous</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Le</w> <w n="13.2">même</w> <w n="13.3">péril</w> <w n="13.4">nous</w> <w n="13.5">rassemble</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Chassons</w> <w n="14.2">tout</w> <w n="14.3">sentiment</w> <w n="14.4">haineux</w>,</l>
					<l n="15" num="3.3"><w n="15.1">Lorsqu</w>’<w n="15.2">on</w> <w n="15.3">lutte</w> <w n="15.4">et</w> <w n="15.5">qu</w>’<w n="15.6">on</w> <w n="15.7">meurt</w> <w n="15.8">ensemble</w>,</l>
					<l n="16" num="3.4"><w n="16.1">On</w> <w n="16.2">est</w> <w n="16.3">uni</w> <w n="16.4">par</w> <w n="16.5">de</w> <w n="16.6">saints</w> <w n="16.7">nœuds</w>.</l>
					<l n="17" num="3.5"><w n="17.1">Notre</w> <w n="17.2">sang</w> <w n="17.3">en</w> <w n="17.4">ruisseaux</w> <w n="17.5">se</w> <w n="17.6">mêle</w> ;</l>
					<l n="18" num="3.6"><w n="18.1">De</w> <w n="18.2">la</w> <w n="18.3">concorde</w> <w n="18.4">fraternelle</w></l>
					<l n="19" num="3.7"><w n="19.1">Il</w> <w n="19.2">cimentera</w> <w n="19.3">le</w> <w n="19.4">pouvoir</w>.</l>
					<l n="20" num="3.8"><w n="20.1">Devant</w> <w n="20.2">le</w> <w n="20.3">drapeau</w> <w n="20.4">germanique</w></l>
					<l n="21" num="3.9"><w n="21.1">Soyons</w> <w n="21.2">Français</w> ; <w n="21.3">ce</w> <w n="21.4">titre</w> <w n="21.5">unique</w></l>
					<l n="22" num="3.10"><w n="22.1">Révèle</w> <w n="22.2">à</w> <w n="22.3">chacun</w> <w n="22.4">son</w> <w n="22.5">devoir</w>.</l>
				</lg>
				<lg n="4">
					<l n="23" num="4.1"><w n="23.1">Ne</w> <w n="23.2">soyons</w>, <w n="23.3">nous</w>, <w n="23.4">enfants</w> <w n="23.5">de</w> <w n="23.6">notre</w> <w n="23.7">chère</w> <w n="23.8">France</w>,</l>
					<l n="24" num="4.2"><w n="24.1">Ni</w> <w n="24.2">Gibelin</w>, <w n="24.3">ni</w> <w n="24.4">Guelfe</w>, <w n="24.5">ainsi</w> <w n="24.6">que</w> <w n="24.7">dans</w> <w n="24.8">Florence</w>,</l>
					<l n="25" num="4.3"><w n="25.1">Ni</w> <w n="25.2">Montagu</w>, <w n="25.3">ni</w> <w n="25.4">Capulet</w>.</l>
					<l n="26" num="4.4"><w n="26.1">Notre</w> <w n="26.2">foi</w>, <w n="26.3">notre</w> <w n="26.4">amour</w>, <w n="26.5">c</w>’<w n="26.6">est</w> <w n="26.7">la</w> <w n="26.8">sainte</w> <w n="26.9">Patrie</w>,</l>
					<l n="27" num="4.5"><w n="27.1">Découvrant</w> <w n="27.2">à</w> <w n="27.3">nos</w> <w n="27.4">yeux</w> <w n="27.5">sa</w> <w n="27.6">mamelle</w> <w n="27.7">meurtrie</w>,</l>
					<l n="28" num="4.6"><w n="28.1">Qui</w> <w n="28.2">nous</w> <w n="28.3">a</w> <w n="28.4">nourris</w> <w n="28.5">de</w> <w n="28.6">son</w> <w n="28.7">lait</w>.</l>
				</lg>
				<lg n="5">
					<l n="29" num="5.1"><w n="29.1">Honte</w> <w n="29.2">à</w> <w n="29.3">qui</w> <w n="29.4">songe</w> <w n="29.5">à</w> <w n="29.6">soi</w>, <w n="29.7">quand</w> <w n="29.8">de</w> <w n="29.9">la</w> <w n="29.10">Loire</w> <w n="29.11">aux</w> <w n="29.12">Vosges</w>,</l>
					<l n="30" num="5.2"><w n="30.1">Nos</w> <w n="30.2">plus</w> <w n="30.3">belles</w> <w n="30.4">cités</w> <w n="30.5">ne</w> <w n="30.6">sont</w> <w n="30.7">plus</w> <w n="30.8">que</w> <w n="30.9">des</w> <w n="30.10">bauges</w></l>
					<l n="31" num="5.3"><w n="31.1">Où</w> <w n="31.2">se</w> <w n="31.3">vautre</w> <w n="31.4">le</w> <w n="31.5">sanglier</w> ;</l>
					<l n="32" num="5.4"><w n="32.1">Quand</w> <w n="32.2">un</w> <w n="32.3">royal</w> <w n="32.4">brigand</w> <w n="32.5">a</w> <w n="32.6">son</w> <w n="32.7">lit</w> <w n="32.8">dans</w> <w n="32.9">Versaille</w>,</l>
					<l n="33" num="5.5"><w n="33.1">Et</w> <w n="33.2">que</w> <w n="33.3">d</w>’<w n="33.4">affreux</w> <w n="33.5">boulets</w>, <w n="33.6">le</w> <w n="33.7">canon</w> <w n="33.8">Krupp</w> <w n="33.9">t</w>’<w n="33.10">assaille</w>,</l>
					<l n="34" num="5.6"><w n="34.1">O</w> <w n="34.2">Paris</w>, <w n="34.3">notre</w> <w n="34.4">bouclier</w> !</l>
				</lg>
				<lg n="6">
					<l n="35" num="6.1"><w n="35.1">Qu</w>’<w n="35.2">importe</w> <w n="35.3">ce</w> <w n="35.4">qui</w> <w n="35.5">nous</w> <w n="35.6">sépare</w> ?</l>
					<l n="36" num="6.2"><w n="36.1">Étouffons</w> <w n="36.2">d</w>’<w n="36.3">insensés</w> <w n="36.4">débats</w> ;</l>
					<l n="37" num="6.3"><w n="37.1">Courons</w>, <w n="37.2">amis</w>, <w n="37.3">sur</w> <w n="37.4">le</w> <w n="37.5">barbare</w> ;</l>
					<l n="38" num="6.4"><w n="38.1">Serrons</w> <w n="38.2">nos</w> <w n="38.3">rangs</w>, <w n="38.4">dans</w> <w n="38.5">les</w> <w n="38.6">combats</w>.</l>
					<l n="39" num="6.5"><w n="39.1">Qu</w>’<w n="39.2">aux</w> <w n="39.3">pieux</w> <w n="39.4">amant</w> <w n="39.5">des</w> <w n="39.6">ruines</w></l>
					<l n="40" num="6.6"><w n="40.1">L</w>’<w n="40.2">enfant</w> <w n="40.3">des</w> <w n="40.4">nouvelles</w> <w n="40.5">doctrines</w></l>
					<l n="41" num="6.7"><w n="41.1">Tende</w> <w n="41.2">une</w> <w n="41.3">généreuse</w> <w n="41.4">main</w>.</l>
					<l n="42" num="6.8"><w n="42.1">L</w>’<w n="42.2">un</w> <w n="42.3">d</w>’<w n="42.4">un</w> <w n="42.5">passé</w> <w n="42.6">noble</w> <w n="42.7">a</w> <w n="42.8">la</w> <w n="42.9">garde</w> ;</l>
					<l n="43" num="6.9"><w n="43.1">L</w>’<w n="43.2">autre</w> <w n="43.3">vers</w> <w n="43.4">l</w>’<w n="43.5">avenir</w> <w n="43.6">regarde</w> ;</l>
					<l n="44" num="6.10"><w n="44.1">Tous</w> <w n="44.2">deux</w> <w n="44.3">s</w>’<w n="44.4">embrasseront</w> <w n="44.5">demain</w>.</l>
				</lg>
				<lg n="7">
					<l n="45" num="7.1"><w n="45.1">Notre</w> <w n="45.2">peuple</w> <w n="45.3">a</w> <w n="45.4">semé</w> <w n="45.5">dans</w> <w n="45.6">tous</w> <w n="45.7">les</w> <w n="45.8">temps</w> <w n="45.9">son</w> <w n="45.10">verbe</w>,</l>
					<l n="46" num="7.2"><w n="46.1">Il</w> <w n="46.2">recueille</w> <w n="46.3">l</w>’<w n="46.4">idée</w> <w n="46.5">en</w> <w n="46.6">glorieuse</w> <w n="46.7">gerbe</w></l>
					<l n="47" num="7.3"><w n="47.1">Sur</w> <w n="47.2">son</w> <w n="47.3">char</w> <w n="47.4">au</w> <w n="47.5">rapide</w> <w n="47.6">essieu</w>.</l>
					<l n="48" num="7.4"><w n="48.1">Ardent</w> <w n="48.2">en</w> <w n="48.3">ses</w> <w n="48.4">labeurs</w>, <w n="48.5">sous</w> <w n="48.6">un</w> <w n="48.7">ciel</w> <w n="48.8">pur</w> <w n="48.9">ou</w> <w n="48.10">sombre</w>,</l>
					<l n="49" num="7.5"><w n="49.1">Il</w> <w n="49.2">s</w>’<w n="49.3">obstine</w> <w n="49.4">à</w> <w n="49.5">poursuivre</w> <w n="49.6">une</w> <w n="49.7">chimère</w>, <w n="49.8">une</w> <w n="49.9">ombre</w> ;</l>
					<l n="50" num="7.6"><w n="50.1">Mais</w> <w n="50.2">cette</w> <w n="50.3">ombre</w> <w n="50.4">est</w> <w n="50.5">celle</w> <w n="50.6">de</w> <w n="50.7">Dieu</w>.</l>
				</lg>
				<lg n="8">
					<l n="51" num="8.1"><w n="51.1">Et</w> <w n="51.2">de</w> <w n="51.3">qui</w> <w n="51.4">donc</w> <w n="51.5">le</w> <w n="51.6">Juste</w> <w n="51.7">et</w> <w n="51.8">le</w> <w n="51.9">Vrai</w> <w n="51.10">sont</w> <w n="51.11">les</w> <w n="51.12">hôtes</w> ?</l>
					<l n="52" num="8.2"><w n="52.1">Chez</w> <w n="52.2">qui</w> <w n="52.3">l</w>’<w n="52.4">esprit</w> <w n="52.5">humain</w> <w n="52.6">de</w> <w n="52.7">ses</w> <w n="52.8">puissances</w> <w n="52.9">hautes</w></l>
					<l n="53" num="8.3"><w n="53.1">A</w> <w n="53.2">manifesté</w> <w n="53.3">le</w> <w n="53.4">trésor</w> ?</l>
					<l n="54" num="8.4"><w n="54.1">Qui</w> <w n="54.2">sur</w> <w n="54.3">un</w> <w n="54.4">Sinaï</w> <w n="54.5">nouveau</w>, <w n="54.6">dans</w> <w n="54.7">la</w> <w n="54.8">tempête</w>,</l>
					<l n="55" num="8.5"><w n="55.1">A</w> <w n="55.2">promulgué</w> <w n="55.3">des</w> <w n="55.4">lois</w> <w n="55.5">que</w> <w n="55.6">tout</w> <w n="55.7">peuple</w> <w n="55.8">répète</w> ?</l>
					<l n="56" num="8.6"><w n="56.1">C</w>’<w n="56.2">est</w> <w n="56.3">la</w> <w n="56.4">France</w>, <w n="56.5">la</w> <w n="56.6">France</w> <w n="56.7">encor</w>.</l>
				</lg>
				<lg n="9">
					<l n="57" num="9.1"><w n="57.1">Des</w> <w n="57.2">deux</w> <w n="57.3">parts</w> <w n="57.4">de</w> <w n="57.5">sa</w> <w n="57.6">course</w> <w n="57.7">immense</w>,</l>
					<l n="58" num="9.2"><w n="58.1">Que</w> <w n="58.2">la</w> <w n="58.3">Providence</w> <w n="58.4">bénit</w>,</l>
					<l n="59" num="9.3"><w n="59.1">L</w>’<w n="59.2">une</w> <w n="59.3">a</w> <w n="59.4">fini</w>, <w n="59.5">l</w>’<w n="59.6">autre</w> <w n="59.7">commence</w> :</l>
					<l n="60" num="9.4"><w n="60.1">Notre</w> <w n="60.2">soleil</w> <w n="60.3">est</w> <w n="60.4">au</w> <w n="60.5">zénith</w>.</l>
					<l n="61" num="9.5"><w n="61.1">Une</w> <w n="61.2">triste</w> <w n="61.3">éclipse</w> <w n="61.4">l</w>’<w n="61.5">efface</w> ;</l>
					<l n="62" num="9.6"><w n="62.1">Mais</w> <w n="62.2">déjà</w> <w n="62.3">dévoilant</w> <w n="62.4">sa</w> <w n="62.5">face</w>,</l>
					<l n="63" num="9.7"><w n="63.1">Il</w> <w n="63.2">brille</w> <w n="63.3">d</w>’<w n="63.4">un</w> <w n="63.5">éclat</w> <w n="63.6">plus</w> <w n="63.7">beau</w>.</l>
					<l n="64" num="9.8"><w n="64.1">La</w> <w n="64.2">France</w> <w n="64.3">ne</w> <w n="64.4">peut</w> <w n="64.5">être</w> <w n="64.6">esclave</w>,</l>
					<l n="65" num="9.9"><w n="65.1">Et</w> <w n="65.2">ni</w> <w n="65.3">le</w> <w n="65.4">Germain</w> <w n="65.5">ni</w> <w n="65.6">le</w> <w n="65.7">Slave</w></l>
					<l n="66" num="9.10"><w n="66.1">N</w>’<w n="66.2">éteindront</w> <w n="66.3">son</w> <w n="66.4">divin</w> <w n="66.5">flambeau</w>.</l>
				</lg>
				<lg n="10">
					<l n="67" num="10.1"><w n="67.1">Nos</w> <w n="67.2">discordes</w> <w n="67.3">hélas</w> ! <w n="67.4">risqueraient</w> <w n="67.5">de</w> <w n="67.6">l</w>’<w n="67.7">éteindre</w>.</l>
					<l n="68" num="10.2"><w n="68.1">Veillons</w>, <w n="68.2">soyons</w> <w n="68.3">unis</w>. <w n="68.4">Ne</w> <w n="68.5">laissons</w> <w n="68.6">pas</w> <w n="68.7">atteindre</w>,</l>
					<l n="69" num="10.3"><w n="69.1">Notre</w> <w n="69.2">âme</w> <w n="69.3">à</w> <w n="69.4">leur</w> <w n="69.5">souffle</w> <w n="69.6">infecté</w>.</l>
					<l n="70" num="10.4"><w n="70.1">Saluons</w> <w n="70.2">du</w> <w n="70.3">regard</w> <w n="70.4">le</w> <w n="70.5">labarum</w> <w n="70.6">sublime</w>,</l>
					<l n="71" num="10.5"><w n="71.1">Le</w> <w n="71.2">signe</w> <w n="71.3">d</w>’<w n="71.4">alliance</w>, <w n="71.5">arc</w>-<w n="71.6">en</w>-<w n="71.7">ciel</w> <w n="71.8">sur</w> <w n="71.9">l</w>’<w n="71.10">abîme</w></l>
					<l n="72" num="10.6"><w n="72.1">Par</w> <w n="72.2">la</w> <w n="72.3">République</w> <w n="72.4">jeté</w>.</l>
				</lg>
				<lg n="11">
					<l n="73" num="11.1">« <w n="73.1">En</w> <w n="73.2">avant</w> ! <w n="73.3">en</w> <w n="73.4">avant</w> ! <w n="73.5">Et</w> <w n="73.6">que</w> <w n="73.7">Dieu</w> <w n="73.8">nous</w> <w n="73.9">protège</w> ! »</l>
					<l n="74" num="11.2"><w n="74.1">Noble</w> <w n="74.2">cri</w> <w n="74.3">de</w> <w n="74.4">Ducrot</w>, <w n="74.5">quand</w>, <w n="74.6">lui</w> <w n="74.7">faisant</w> <w n="74.8">cortège</w>,</l>
					<l n="75" num="11.3"><w n="75.1">Paris</w> <w n="75.2">s</w>’<w n="75.3">élançait</w> <w n="75.4">avec</w> <w n="75.5">lui</w> !</l>
					<l n="76" num="11.4"><w n="76.1">Redisons</w> <w n="76.2">en</w> <w n="76.3">avant</w> ! <w n="76.4">d</w>’<w n="76.5">une</w> <w n="76.6">voix</w> <w n="76.7">unanime</w> ;</l>
					<l n="77" num="11.5"><w n="77.1">Qu</w>’<w n="77.2">un</w> <w n="77.3">seul</w> <w n="77.4">effort</w>, <w n="77.5">un</w> <w n="77.6">seul</w> <w n="77.7">sentiment</w> <w n="77.8">nous</w> <w n="77.9">anime</w> :</l>
					<l n="78" num="11.6"><w n="78.1">Bientôt</w> <w n="78.2">la</w> <w n="78.3">victoire</w> <w n="78.4">aura</w> <w n="78.5">lui</w>.</l>
				</lg>
				<lg n="12">
					<l n="79" num="12.1"><w n="79.1">Par</w> <w n="79.2">les</w> <w n="79.3">martyrs</w> <w n="79.4">de</w> <w n="79.5">notre</w> <w n="79.6">cause</w>,</l>
					<l n="80" num="12.2"><w n="80.1">Par</w> <w n="80.2">leur</w> <w n="80.3">holocauste</w> <w n="80.4">immortel</w>,</l>
					<l n="81" num="12.3"><w n="81.1">Par</w> <w n="81.2">leur</w> <w n="81.3">cœur</w> <w n="81.4">sanglant</w> <w n="81.5">qui</w> <w n="81.6">repose</w></l>
					<l n="82" num="12.4"><w n="82.1">Sous</w> <w n="82.2">un</w> <w n="82.3">tertre</w>, <w n="82.4">funèbre</w> <w n="82.5">autel</w>,</l>
					<l n="83" num="12.5"><w n="83.1">Aimons</w>, <w n="83.2">comme</w> <w n="83.3">ils</w> <w n="83.4">l</w>’<w n="83.5">aimaient</w>, <w n="83.6">la</w> <w n="83.7">France</w> ;</l>
					<l n="84" num="12.6"><w n="84.1">Ne</w> <w n="84.2">démentons</w> <w n="84.3">point</w> <w n="84.4">l</w>’<w n="84.5">espérance</w></l>
					<l n="85" num="12.7"><w n="85.1">Qui</w> <w n="85.2">les</w> <w n="85.3">consolait</w> <w n="85.4">du</w> <w n="85.5">trépas</w>.</l>
					<l n="86" num="12.8"><w n="86.1">Marchons</w> <w n="86.2">sous</w> <w n="86.3">leurs</w> <w n="86.4">sacrés</w> <w n="86.5">auspices</w> !</l>
					<l n="87" num="12.9"><w n="87.1">Les</w> <w n="87.2">cieux</w> <w n="87.3">enfin</w> <w n="87.4">seront</w> <w n="87.5">propices</w> :</l>
					<l n="88" num="12.10"><w n="88.1">La</w> <w n="88.2">nation</w> <w n="88.3">ne</w> <w n="88.4">mourra</w> <w n="88.5">pas</w>.</l>
				</lg>
			</div></body></text></TEI>