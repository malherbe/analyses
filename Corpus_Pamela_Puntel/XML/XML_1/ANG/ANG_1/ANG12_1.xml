<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">NOS RUINES</title>
				<title type="medium">Édition électronique</title>
				<author key="ANG">
					<name>
						<forename>Albert</forename>
						<surname>ANGOT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">ANG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>NOS RUINES</title>
						<author>ALBERT ANGOT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30021186v</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>NOS RUINES</title>
								<author>ALBERT ANGOT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DUNIOL ET CIE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANG12">
				<head type="main">LES COURTISANS</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">courtisans</w>, <w n="1.3">meute</w> <w n="1.4">de</w> <w n="1.5">traîtres</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Esclaves</w> <w n="2.2">de</w> <w n="2.3">toute</w> <w n="2.4">grandeur</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Valets</w>, <w n="3.2">ne</w> <w n="3.3">cherchez</w> <w n="3.4">plus</w> <w n="3.5">de</w> <w n="3.6">maîtres</w> !</l>
						<l n="4" num="1.4"><w n="4.1">Consolez</w> <w n="4.2">plutôt</w> <w n="4.3">le</w> <w n="4.4">malheur</w>.</l>
						<l n="5" num="1.5"><w n="5.1">N</w>’<w n="5.2">imitez</w> <w n="5.3">donc</w> <w n="5.4">point</w> <w n="5.5">les</w> <w n="5.6">Sicambres</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">Dans</w> <w n="6.2">de</w> <w n="6.3">nouvelles</w> <w n="6.4">antichambres</w></l>
						<l n="7" num="1.7"><w n="7.1">Ne</w> <w n="7.2">brûlez</w> <w n="7.3">point</w> <w n="7.4">vos</w> <w n="7.5">anciens</w> <w n="7.6">dieux</w>.</l>
						<l n="8" num="1.8"><w n="8.1">Assez</w> <w n="8.2">belle</w> <w n="8.3">fut</w> <w n="8.4">la</w> <w n="8.5">curée</w> !</l>
						<l n="9" num="1.9"><w n="9.1">De</w> <w n="9.2">grâce</w>, <w n="9.3">gardez</w> <w n="9.4">la</w> <w n="9.5">livrée</w></l>
						<l n="10" num="1.10"><w n="10.1">D</w>’<w n="10.2">un</w> <w n="10.3">prince</w> <w n="10.4">pour</w> <w n="10.5">vous</w> <w n="10.6">généreux</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">Je</w> <w n="11.2">sais</w> <w n="11.3">fort</w> <w n="11.4">bien</w> <w n="11.5">qu</w>’<w n="11.6">auprès</w> <w n="11.7">du</w> <w n="11.8">trône</w></l>
						<l n="12" num="2.2"><w n="12.1">Du</w> <w n="12.2">peuple</w> <w n="12.3">ou</w> <w n="12.4">d</w>’<w n="12.5">un</w> <w n="12.6">roi</w> <w n="12.7">souverain</w>,</l>
						<l n="13" num="2.3"><w n="13.1">Vous</w> <w n="13.2">savez</w> <w n="13.3">cueillir</w> <w n="13.4">quelque</w> <w n="13.5">aumône</w>,</l>
						<l n="14" num="2.4"><w n="14.1">Comme</w> <w n="14.2">le</w> <w n="14.3">pauvre</w> <w n="14.4">du</w> <w n="14.5">chemin</w> ;</l>
						<l n="15" num="2.5"><w n="15.1">Je</w> <w n="15.2">sais</w> <w n="15.3">fort</w> <w n="15.4">bien</w> <w n="15.5">que</w> <w n="15.6">votre</w> <w n="15.7">échine</w></l>
						<l n="16" num="2.6"><w n="16.1">Avec</w> <w n="16.2">promptitude</w> <w n="16.3">s</w>’<w n="16.4">incline</w></l>
						<l n="17" num="2.7"><w n="17.1">Devant</w> <w n="17.2">le</w> <w n="17.3">premier</w> <w n="17.4">soliveau</w></l>
						<l n="18" num="2.8"><w n="18.1">Ou</w> <w n="18.2">devant</w> <w n="18.3">la</w> <w n="18.4">première</w> <w n="18.5">grue</w>,</l>
						<l n="19" num="2.9"><w n="19.1">Qui</w> <w n="19.2">sur</w> <w n="19.3">nous</w> <w n="19.4">tombe</w> <w n="19.5">de</w> <w n="19.6">la</w> <w n="19.7">nue</w>,</l>
						<l n="20" num="2.10"><w n="20.1">Comme</w> <w n="20.2">chez</w> <w n="20.3">Phèdre</w>, <w n="20.4">au</w> <w n="20.5">bord</w> <w n="20.6">de</w> <w n="20.7">l</w>’<w n="20.8">eau</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">Vous</w> <w n="21.2">avez</w> <w n="21.3">joué</w> <w n="21.4">votre</w> <w n="21.5">rôle</w>,</l>
						<l n="22" num="3.2"><w n="22.1">Acteurs</w> <w n="22.2">chamarrés</w> <w n="22.3">de</w> <w n="22.4">la</w> <w n="22.5">cour</w> ;</l>
						<l n="23" num="3.3"><w n="23.1">Je</w> <w n="23.2">veux</w> <w n="23.3">vous</w> <w n="23.4">marquer</w> <w n="23.5">à</w> <w n="23.6">l</w>’<w n="23.7">épaule</w>,</l>
						<l n="24" num="3.4"><w n="24.1">Vous</w> <w n="24.2">dénoncer</w> <w n="24.3">au</w> <w n="24.4">roi</w> <w n="24.5">du</w> <w n="24.6">jour</w>.</l>
						<l n="25" num="3.5"><w n="25.1">D</w>’<w n="25.2">autres</w> <w n="25.3">sans</w> <w n="25.4">vous</w> <w n="25.5">ont</w> <w n="25.6">des</w> <w n="25.7">parjures</w></l>
						<l n="26" num="3.6"><w n="26.1">Et</w> <w n="26.2">sur</w> <w n="26.3">les</w> <w n="26.4">lèvres</w> <w n="26.5">des</w> <w n="26.6">serments</w> ;</l>
						<l n="27" num="3.7"><w n="27.1">Ne</w> <w n="27.2">venez</w> <w n="27.3">point</w> <w n="27.4">grossir</w> <w n="27.5">leur</w> <w n="27.6">nombre</w> ;</l>
						<l n="28" num="3.8"><w n="28.1">Rentrez</w> <w n="28.2">plutôt</w> <w n="28.3">au</w> <w n="28.4">sein</w> <w n="28.5">de</w> <w n="28.6">l</w>’<w n="28.7">ombre</w>,</l>
						<l n="29" num="3.9"><w n="29.1">Allons</w> ! <w n="29.2">remportez</w> <w n="29.3">votre</w> <w n="29.4">encens</w>.</l>
					</lg>
					<lg n="4">
						<l n="30" num="4.1"><w n="30.1">Pour</w> <w n="30.2">vous</w> <w n="30.3">plus</w> <w n="30.4">de</w> <w n="30.5">chaises</w> <w n="30.6">curules</w> :</l>
						<l n="31" num="4.2"><w n="31.1">Le</w> <w n="31.2">pays</w> <w n="31.3">est</w> <w n="31.4">trop</w> <w n="31.5">appauvri</w></l>
						<l n="32" num="4.3"><w n="32.1">Pour</w> <w n="32.2">appointer</w> <w n="32.3">vos</w> <w n="32.4">ridicules</w> ;</l>
						<l n="33" num="4.4"><w n="33.1">Votre</w> <w n="33.2">place</w> <w n="33.3">est</w> <w n="33.4">au</w> <w n="33.5">pilori</w>.</l>
						<l n="34" num="4.5"><w n="34.1">Votre</w> <w n="34.2">grimaçante</w> <w n="34.3">figure</w></l>
						<l n="35" num="4.6"><w n="35.1">Pleine</w> <w n="35.2">de</w> <w n="35.3">fard</w> <w n="35.4">et</w> <w n="35.5">de</w> <w n="35.6">peinture</w></l>
						<l n="36" num="4.7"><w n="36.1">Est</w> <w n="36.2">digne</w> <w n="36.3">du</w> <w n="36.4">fouet</w> <w n="36.5">des</w> <w n="36.6">Gilberts</w>.</l>
						<l n="37" num="4.8"><w n="37.1">Je</w> <w n="37.2">l</w>’<w n="37.3">userai</w> <w n="37.4">sur</w> <w n="37.5">votre</w> <w n="37.6">joue</w>,</l>
						<l n="38" num="4.9"><w n="38.1">Si</w> <w n="38.2">je</w> <w n="38.3">ne</w> <w n="38.4">croyais</w> <w n="38.5">voir</w> <w n="38.6">la</w> <w n="38.7">boue</w></l>
						<l n="39" num="4.10"><w n="39.1">Souiller</w> <w n="39.2">l</w>’<w n="39.3">hermine</w> <w n="39.4">de</w> <w n="39.5">mon</w> <w n="39.6">vers</w>.</l>
					</lg>
					<lg n="5">
						<l n="40" num="5.1"><w n="40.1">A</w> <w n="40.2">leur</w> <w n="40.3">juste</w> <w n="40.4">prix</w> <w n="40.5">j</w>’<w n="40.6">apprécie</w></l>
						<l n="41" num="5.2"><w n="41.1">Vos</w> <w n="41.2">trahisons</w>, <w n="41.3">ô</w> <w n="41.4">courtisans</w> ;</l>
						<l n="42" num="5.3"><w n="42.1">Vous</w> <w n="42.2">comptez</w> <w n="42.3">une</w> <w n="42.4">apostasie</w></l>
						<l n="43" num="5.4"><w n="43.1">Pour</w> <w n="43.2">chacun</w> <w n="43.3">de</w> <w n="43.4">vos</w> <w n="43.5">sentiments</w>.</l>
						<l n="44" num="5.5"><w n="44.1">Souvent</w> <w n="44.2">on</w> <w n="44.3">pouvait</w> <w n="44.4">vous</w> <w n="44.5">entendre</w> :</l>
						<l n="45" num="5.6">— « <w n="45.1">Mon</w> <w n="45.2">prince</w>, <w n="45.3">à</w> <w n="45.4">moi</w>, <w n="45.5">pour</w> <w n="45.6">vous</w> <w n="45.7">défendre</w>,</l>
						<l n="46" num="5.7">« <w n="46.1">De</w> <w n="46.2">tout</w> <w n="46.3">braver</w>, <w n="46.4">jusqu</w>’<w n="46.5">au</w> <w n="46.6">tombeau</w>. » —</l>
						<l n="47" num="5.8"><w n="47.1">Quand</w> <w n="47.2">venait</w> <w n="47.3">l</w>’<w n="47.4">heure</w> <w n="47.5">de</w> <w n="47.6">vous</w> <w n="47.7">battre</w>,</l>
						<l n="48" num="5.9"><w n="48.1">Votre</w> <w n="48.2">glaive</w>, <w n="48.3">au</w> <w n="48.4">lieu</w> <w n="48.5">de</w> <w n="48.6">combattre</w>,</l>
						<l n="49" num="5.10"><w n="49.1">Restait</w> <w n="49.2">cloué</w> <w n="49.3">dans</w> <w n="49.4">le</w> <w n="49.5">fourreau</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="50" num="1.1"><w n="50.1">Elle</w> <w n="50.2">sera</w> <w n="50.3">toujours</w> <w n="50.4">la</w> <w n="50.5">même</w>,</l>
						<l n="51" num="1.2"><w n="51.1">Cette</w> <w n="51.2">race</w> <w n="51.3">des</w> <w n="51.4">courtisans</w> !</l>
						<l n="52" num="1.3"><w n="52.1">Au</w> <w n="52.2">moment</w> <w n="52.3">du</w> <w n="52.4">péril</w> <w n="52.5">suprême</w>,</l>
						<l n="53" num="1.4"><w n="53.1">Loin</w> <w n="53.2">des</w> <w n="53.3">princes</w> <w n="53.4">ils</w> <w n="53.5">sont</w> <w n="53.6">absents</w>.</l>
						<l n="54" num="1.5"><w n="54.1">Ils</w> <w n="54.2">voient</w> <w n="54.3">venir</w> <w n="54.4">le</w> <w n="54.5">vent</w> <w n="54.6">d</w>’<w n="54.7">orage</w>,</l>
						<l n="55" num="1.6"><w n="55.1">Ils</w> <w n="55.2">en</w> <w n="55.3">connaissent</w> <w n="55.4">le</w> <w n="55.5">présage</w>,</l>
						<l n="56" num="1.7"><w n="56.1">Aussi</w> <w n="56.2">bien</w> <w n="56.3">qu</w>’<w n="56.4">un</w> <w n="56.5">vieux</w> <w n="56.6">loup</w> <w n="56.7">de</w> <w n="56.8">mer</w>.</l>
						<l n="57" num="1.8"><w n="57.1">Ils</w> <w n="57.2">disent</w> : — « <w n="57.3">Je</w> <w n="57.4">sais</w> <w n="57.5">qu</w>’<w n="57.6">à</w> <w n="57.7">telle</w> <w n="57.8">heure</w></l>
						<l n="58" num="1.9">« <w n="58.1">Contre</w> <w n="58.2">la</w> <w n="58.3">royale</w> <w n="58.4">demeure</w></l>
						<l n="59" num="1.10">« <w n="59.1">Écumera</w> <w n="59.2">le</w> <w n="59.3">flot</w> <w n="59.4">amer</w>. »</l>
					</lg>
					<lg n="2">
						<l n="60" num="2.1"><w n="60.1">Ils</w> <w n="60.2">rassuraient</w> <w n="60.3">encore</w> <w n="60.4">la</w> <w n="60.5">veille</w></l>
						<l n="61" num="2.2"><w n="61.1">Le</w> <w n="61.2">souverain</w> <w n="61.3">qui</w> <w n="61.4">s</w>’<w n="61.5">alarmait</w>,</l>
						<l n="62" num="2.3"><w n="62.1">Tandis</w> <w n="62.2">qu</w>’<w n="62.3">ils</w> <w n="62.4">savaient</w> <w n="62.5">à</w> <w n="62.6">merveille</w></l>
						<l n="63" num="2.4"><w n="63.1">Que</w> <w n="63.2">sa</w> <w n="63.3">couronne</w> <w n="63.4">s</w>’<w n="63.5">abîmait</w> :</l>
						<l n="64" num="2.5">— « <w n="64.1">Nous</w> <w n="64.2">sommes</w> <w n="64.3">sûrs</w> <w n="64.4">du</w> <w n="64.5">capitaine</w> ;</l>
						<l n="65" num="2.6">« <w n="65.1">Un</w> <w n="65.2">vent</w> <w n="65.3">meilleur</w> <w n="65.4">gonfle</w> <w n="65.5">l</w>’<w n="65.6">antenne</w>,</l>
						<l n="66" num="2.7">« <w n="66.1">Plus</w> <w n="66.2">sûrement</w> <w n="66.3">vogue</w> <w n="66.4">l</w>’<w n="66.5">esquif</w> ;</l>
						<l n="67" num="2.8">« <w n="67.1">Ne</w> <w n="67.2">craignons</w> <w n="67.3">point</w> <w n="67.4">une</w> <w n="67.5">tourmente</w> ;</l>
						<l n="68" num="2.9">« <w n="68.1">Nous</w> <w n="68.2">éviterons</w> <w n="68.3">le</w> <w n="68.4">récif</w>. » —</l>
					</lg>
					<lg n="3">
						<l n="69" num="3.1"><w n="69.1">Toujours</w>, <w n="69.2">oui</w>, <w n="69.3">toujours</w> <w n="69.4">le</w> <w n="69.5">mensonge</w> !</l>
						<l n="70" num="3.2"><w n="70.1">Il</w> <w n="70.2">faut</w> <w n="70.3">flatter</w> <w n="70.4">le</w> <w n="70.5">souverain</w>,</l>
						<l n="71" num="3.3"><w n="71.1">L</w>’<w n="71.2">endormir</w> <w n="71.3">avec</w> <w n="71.4">un</w> <w n="71.5">doux</w> <w n="71.6">songe</w>,</l>
						<l n="72" num="3.4"><w n="72.1">Et</w> <w n="72.2">lui</w> <w n="72.3">cacher</w> <w n="72.4">le</w> <w n="72.5">lendemain</w>.</l>
						<l n="73" num="3.5"><w n="73.1">Qu</w>’<w n="73.2">importe</w> <w n="73.3">si</w> <w n="73.4">le</w> <w n="73.5">vaisseau</w> <w n="73.6">sombre</w> !</l>
						<l n="74" num="3.6"><w n="74.1">Le</w> <w n="74.2">courtisan</w>, <w n="74.3">au</w> <w n="74.4">sein</w> <w n="74.5">de</w> <w n="74.6">l</w>’<w n="74.7">ombre</w>,</l>
						<l n="75" num="3.7"><w n="75.1">A</w> <w n="75.2">su</w> <w n="75.3">mettre</w> <w n="75.4">une</w> <w n="75.5">barque</w> <w n="75.6">à</w> <w n="75.7">flot</w> ;</l>
						<l n="76" num="3.8"><w n="76.1">Avec</w> <w n="76.2">les</w> <w n="76.3">siens</w> <w n="76.4">et</w> <w n="76.5">ses</w> <w n="76.6">richesses</w>,</l>
						<l n="77" num="3.9"><w n="77.1">Sans</w> <w n="77.2">nul</w> <w n="77.3">remords</w> <w n="77.4">et</w> <w n="77.5">sans</w> <w n="77.6">tristesses</w>,</l>
						<l n="78" num="3.10"><w n="78.1">Il</w> <w n="78.2">gagne</w> <w n="78.3">le</w> <w n="78.4">plus</w> <w n="78.5">proche</w> <w n="78.6">îlot</w>.</l>
					</lg>
					<lg n="4">
						<l n="79" num="4.1"><w n="79.1">Il</w> <w n="79.2">était</w> <w n="79.3">temps</w> !… <w n="79.4">voici</w> <w n="79.5">l</w>’<w n="79.6">orage</w></l>
						<l n="80" num="4.2"><w n="80.1">Qui</w> <w n="80.2">soulève</w> <w n="80.3">le</w> <w n="80.4">sein</w> <w n="80.5">des</w> <w n="80.6">mers</w> ;</l>
						<l n="81" num="4.3"><w n="81.1">La</w> <w n="81.2">foudre</w> <w n="81.3">mugit</w> <w n="81.4">avec</w> <w n="81.5">rage</w></l>
						<l n="82" num="4.4"><w n="82.1">Secouant</w> <w n="82.2">ses</w> <w n="82.3">gerbes</w> <w n="82.4">d</w>’<w n="82.5">éclairs</w>.</l>
						<l n="83" num="4.5"><w n="83.1">Le</w> <w n="83.2">vaisseau</w> <w n="83.3">pris</w> <w n="83.4">à</w> <w n="83.5">l</w>’<w n="83.6">improviste</w></l>
						<l n="84" num="4.6"><w n="84.1">Pendant</w> <w n="84.2">quelques</w> <w n="84.3">instants</w> <w n="84.4">résiste</w></l>
						<l n="85" num="4.7"><w n="85.1">A</w> <w n="85.2">la</w> <w n="85.3">fureur</w> <w n="85.4">de</w> <w n="85.5">l</w>’<w n="85.6">aquilon</w>.</l>
						<l n="86" num="4.8"><w n="86.1">Vains</w> <w n="86.2">efforts</w> ! <w n="86.3">Le</w> <w n="86.4">pauvre</w> <w n="86.5">navire</w></l>
						<l n="87" num="4.9"><w n="87.1">Au</w> <w n="87.2">sein</w> <w n="87.3">des</w> <w n="87.4">brisants</w> <w n="87.5">se</w> <w n="87.6">déchire</w>,</l>
						<l n="88" num="4.10"><w n="88.1">Et</w> <w n="88.2">sombre</w> <w n="88.3">dans</w> <w n="88.4">un</w> <w n="88.5">tourbillon</w>.</l>
					</lg>
					<lg n="5">
						<l n="89" num="5.1"><w n="89.1">Quand</w> <w n="89.2">le</w> <w n="89.3">vent</w> <w n="89.4">cesse</w> <w n="89.5">de</w> <w n="89.6">bruire</w>,</l>
						<l n="90" num="5.2"><w n="90.1">Le</w> <w n="90.2">courtisan</w> <w n="90.3">regarde</w> <w n="90.4">au</w> <w n="90.5">loin</w>,</l>
						<l n="91" num="5.3"><w n="91.1">Si</w> <w n="91.2">par</w> <w n="91.3">hasard</w> <w n="91.4">quelque</w> <w n="91.5">navire</w></l>
						<l n="92" num="5.4"><w n="92.1">A</w> <w n="92.2">l</w>’<w n="92.3">horizon</w> <w n="92.4">n</w>’<w n="92.5">apparaît</w> <w n="92.6">point</w>.</l>
						<l n="93" num="5.5"><w n="93.1">O</w> <w n="93.2">joie</w> ! <w n="93.3">une</w> <w n="93.4">nef</w> <w n="93.5">magnifique</w></l>
						<l n="94" num="5.6"><w n="94.1">Naît</w> <w n="94.2">sur</w> <w n="94.3">l</w>’<w n="94.4">océan</w> <w n="94.5">pacifique</w>,</l>
						<l n="95" num="5.7"><w n="95.1">Comme</w> <w n="95.2">un</w> <w n="95.3">soleil</w> <w n="95.4">à</w> <w n="95.5">l</w>’<w n="95.6">orient</w>.</l>
						<l n="96" num="5.8"><w n="96.1">Elle</w> <w n="96.2">vogue</w> <w n="96.3">vers</w> <w n="96.4">le</w> <w n="96.5">rivage</w>,</l>
						<l n="97" num="5.9"><w n="97.1">Ainsi</w> <w n="97.2">qu</w>’<w n="97.3">un</w> <w n="97.4">grand</w> <w n="97.5">cygne</w> <w n="97.6">qui</w> <w n="97.7">nage</w>,</l>
						<l n="98" num="5.10"><w n="98.1">Bercé</w> <w n="98.2">par</w> <w n="98.3">un</w> <w n="98.4">flot</w> <w n="98.5">souriant</w>.</l>
					</lg>
					<lg n="6">
						<l n="99" num="6.1"><w n="99.1">Avec</w> <w n="99.2">le</w> <w n="99.3">fard</w> <w n="99.4">et</w> <w n="99.5">la</w> <w n="99.6">peinture</w>,</l>
						<l n="100" num="6.2"><w n="100.1">En</w> <w n="100.2">un</w> <w n="100.3">clin</w> <w n="100.4">d</w>’<w n="100.5">œil</w>, <w n="100.6">le</w> <w n="100.7">courtisan</w></l>
						<l n="101" num="6.3"><w n="101.1">S</w>’<w n="101.2">est</w> <w n="101.3">refait</w> <w n="101.4">une</w> <w n="101.5">autre</w> <w n="101.6">figure</w></l>
						<l n="102" num="6.4"><w n="102.1">Pour</w> <w n="102.2">joindre</w> <w n="102.3">ce</w> <w n="102.4">vaisseau</w> <w n="102.5">puissant</w>.</l>
						<l n="103" num="6.5"><w n="103.1">Dieu</w> <w n="103.2">sait</w> <w n="103.3">avec</w> <w n="103.4">quelle</w> <w n="103.5">prudence</w>,</l>
						<l n="104" num="6.6"><w n="104.1">Tout</w> <w n="104.2">en</w> <w n="104.3">louvoyant</w>, <w n="104.4">il</w> <w n="104.5">s</w>’<w n="104.6">avance</w> ;</l>
						<l n="105" num="6.7"><w n="105.1">Bientôt</w> <w n="105.2">on</w> <w n="105.3">le</w> <w n="105.4">recueille</w> <w n="105.5">à</w> <w n="105.6">bord</w>.</l>
						<l n="106" num="6.8"><w n="106.1">Une</w> <w n="106.2">attendant</w> <w n="106.3">d</w>’<w n="106.4">autres</w> <w n="106.5">naufrages</w>,</l>
						<l n="107" num="6.9"><w n="107.1">Il</w> <w n="107.2">se</w> <w n="107.3">rit</w> <w n="107.4">des</w> <w n="107.5">lointains</w> <w n="107.6">orages</w>,</l>
						<l n="108" num="6.10"><w n="108.1">Et</w> <w n="108.2">gaiement</w> <w n="108.3">vogue</w> <w n="108.4">vers</w> <w n="108.5">le</w> <w n="108.6">port</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="109" num="1.1"><w n="109.1">O</w> <w n="109.2">rois</w>, <w n="109.3">esclaves</w> <w n="109.4">de</w> <w n="109.5">la</w> <w n="109.6">foule</w>,</l>
						<l n="110" num="1.2"><w n="110.1">Allez</w> <w n="110.2">donc</w> <w n="110.3">voguer</w> <w n="110.4">sur</w> <w n="110.5">ses</w> <w n="110.6">flots</w>,</l>
						<l n="111" num="1.3"><w n="111.1">Allez</w> <w n="111.2">donc</w> <w n="111.3">affronter</w> <w n="111.4">sa</w> <w n="111.5">houle</w></l>
						<l n="112" num="1.4"><w n="112.1">Avec</w> <w n="112.2">des</w> <w n="112.3">pareils</w> <w n="112.4">matelots</w> !</l>
						<l n="113" num="1.5"><w n="113.1">Ah</w> ! <w n="113.2">fiez</w>-<w n="113.3">vous</w> <w n="113.4">à</w> <w n="113.5">ma</w> <w n="113.6">parole</w> !</l>
						<l n="114" num="1.6"><w n="114.1">Les</w> <w n="114.2">yeux</w> <w n="114.3">fixés</w> <w n="114.4">sur</w> <w n="114.5">la</w> <w n="114.6">boussole</w>,</l>
						<l n="115" num="1.7"><w n="115.1">Prenez</w> <w n="115.2">le</w> <w n="115.3">gouvernail</w> <w n="115.4">en</w> <w n="115.5">main</w>,</l>
						<l n="116" num="1.8"><w n="116.1">Et</w> <w n="116.2">surveillez</w> <w n="116.3">votre</w> <w n="116.4">mâture</w>.</l>
						<l n="117" num="1.9"><w n="117.1">Plus</w> <w n="117.2">de</w> <w n="117.3">pilotes</w> <w n="117.4">d</w>’<w n="117.5">aventure</w></l>
						<l n="118" num="1.10"><w n="118.1">Pour</w> <w n="118.2">vous</w> <w n="118.3">indiquer</w> <w n="118.4">le</w> <w n="118.5">chemin</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">11 Avril 1871</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>