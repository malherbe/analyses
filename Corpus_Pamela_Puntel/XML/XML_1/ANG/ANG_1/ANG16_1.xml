<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">NOS RUINES</title>
				<title type="medium">Édition électronique</title>
				<author key="ANG">
					<name>
						<forename>Albert</forename>
						<surname>ANGOT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">ANG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>NOS RUINES</title>
						<author>ALBERT ANGOT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30021186v</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>NOS RUINES</title>
								<author>ALBERT ANGOT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DUNIOL ET CIE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANG16">
				<head type="main">LE CHÂTIMENT</head>
				<opener>
					<salute>A M. VICTOR HUGO</salute>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">relisais</w> <w n="1.3">hier</w> <w n="1.4">ce</w> <w n="1.5">chant</w> <w n="1.6">plein</w> <w n="1.7">d</w>’<w n="1.8">harmonie</w></l>
						<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">jadis</w> <w n="2.3">inspirait</w> <w n="2.4">à</w> <w n="2.5">ton</w> <w n="2.6">puissant</w> <w n="2.7">génie</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Hugo</w>, <w n="3.2">l</w>’<w n="3.3">Arc</w> <w n="3.4">de</w> <w n="3.5">Napoléon</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Cet</w> <w n="4.2">arc</w> <w n="4.3">démesuré</w>, <w n="4.4">ciselé</w> <w n="4.5">par</w> <w n="4.6">l</w>’<w n="4.7">Histoire</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Ensemble</w> <w n="5.2">monstrueux</w> <w n="5.3">de</w> <w n="5.4">pierre</w> <w n="5.5">et</w> <w n="5.6">de</w> <w n="5.7">victoire</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Portique</w> <w n="6.2">où</w> <w n="6.3">s</w>’<w n="6.4">épèle</w> <w n="6.5">un</w> <w n="6.6">grand</w> <w n="6.7">nom</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Quand</w> <w n="7.2">tu</w> <w n="7.3">chantais</w> <w n="7.4">si</w> <w n="7.5">bien</w> <w n="7.6">ce</w> <w n="7.7">monument</w> <w n="7.8">sublime</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Ton</w> <w n="8.2">front</w> <w n="8.3">étincelant</w> <w n="8.4">d</w>’<w n="8.5">un</w> <w n="8.6">orgueil</w> <w n="8.7">légitime</w></l>
						<l n="9" num="2.3"><w n="9.1">N</w>’<w n="9.2">était</w> <w n="9.3">point</w> <w n="9.4">voilé</w> <w n="9.5">par</w> <w n="9.6">l</w>’<w n="9.7">erreur</w>.</l>
						<l n="10" num="2.4"><w n="10.1">Poëte</w>, <w n="10.2">je</w> <w n="10.3">t</w>’<w n="10.4">aimais</w> ; <w n="10.5">et</w> <w n="10.6">ta</w> <w n="10.7">chaude</w> <w n="10.8">pensée</w></l>
						<l n="11" num="2.5"><w n="11.1">Se</w> <w n="11.2">répandait</w> <w n="11.3">alors</w> <w n="11.4">en</w> <w n="11.5">mon</w> <w n="11.6">âme</w> <w n="11.7">glacée</w>,</l>
						<l n="12" num="2.6"><w n="12.1">Et</w> <w n="12.2">l</w>’<w n="12.3">enflammait</w> <w n="12.4">de</w> <w n="12.5">son</w> <w n="12.6">ardeur</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Avec</w> <w n="13.2">toi</w> <w n="13.3">je</w> <w n="13.4">cherchais</w> <w n="13.5">à</w> <w n="13.6">voir</w>, <w n="13.7">au</w> <w n="13.8">sein</w> <w n="13.9">des</w> <w n="13.10">âges</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Comme</w> <w n="14.2">un</w> <w n="14.3">oiseau</w> <w n="14.4">perdu</w> <w n="14.5">dans</w> <w n="14.6">le</w> <w n="14.7">bleu</w> <w n="14.8">des</w> <w n="14.9">nuages</w>,</l>
						<l n="15" num="3.3"><w n="15.1">Ton</w> <w n="15.2">arc</w> <w n="15.3">paré</w> <w n="15.4">des</w> <w n="15.5">mains</w> <w n="15.6">du</w> <w n="15.7">temps</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Son</w> <w n="16.2">fronton</w> <w n="16.3">effeuillé</w>, <w n="16.4">ses</w> <w n="16.5">sculptures</w> <w n="16.6">rongées</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Par</w> <w n="17.2">les</w> <w n="17.3">sombres</w> <w n="17.4">lichens</w> <w n="17.5">et</w> <w n="17.6">le</w> <w n="17.7">lierre</w> <w n="17.8">ombragées</w></l>
						<l n="18" num="3.6"><w n="18.1">Dans</w> <w n="18.2">leurs</w> <w n="18.3">replis</w> <w n="18.4">intelligents</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Les</w> <w n="19.2">âges</w>, <w n="19.3">suivant</w> <w n="19.4">toi</w>, <w n="19.5">de</w> <w n="19.6">leur</w> <w n="19.7">touche</w> <w n="19.8">hardie</w></l>
						<l n="20" num="4.2"><w n="20.1">Devaient</w> <w n="20.2">seuls</w> <w n="20.3">ébrécher</w> <w n="20.4">la</w> <w n="20.5">grande</w> <w n="20.6">Arche</w> <w n="20.7">verdie</w></l>
						<l n="21" num="4.3"><w n="21.1">Qui</w> <w n="21.2">gardait</w> <w n="21.3">sa</w> <w n="21.4">virginité</w>.</l>
						<l n="22" num="4.4"><w n="22.1">A</w> <w n="22.2">t</w>’<w n="22.3">en</w> <w n="22.4">croire</w>, <w n="22.5">jamais</w> <w n="22.6">le</w> <w n="22.7">canon</w> <w n="22.8">ou</w> <w n="22.9">la</w> <w n="22.10">honte</w></l>
						<l n="23" num="4.5"><w n="23.1">Ne</w> <w n="23.2">devait</w> <w n="23.3">de</w> <w n="23.4">son</w> <w n="23.5">front</w> <w n="23.6">que</w> <w n="23.7">le</w> <w n="23.8">ciel</w> <w n="23.9">seul</w> <w n="23.10">surmonte</w></l>
						<l n="24" num="4.6"><w n="24.1">Ternir</w> <w n="24.2">la</w> <w n="24.3">vieille</w> <w n="24.4">majesté</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Alors</w>, <w n="25.2">sur</w> <w n="25.3">le</w> <w n="25.4">débris</w> <w n="25.5">de</w> <w n="25.6">la</w> <w n="25.7">vile</w> <w n="25.8">endormie</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Comme</w> <w n="26.2">un</w> <w n="26.3">cadavre</w> <w n="26.4">immense</w> <w n="26.5">à</w> <w n="26.6">la</w> <w n="26.7">face</w> <w n="26.8">blêmie</w>,</l>
						<l n="27" num="5.3"><w n="27.1">Sur</w> <w n="27.2">ton</w> <w n="27.3">Paris</w> <w n="27.4">mort</w> <w n="27.5">pour</w> <w n="27.6">toujours</w>,</l>
						<l n="28" num="5.4"><w n="28.1">Tu</w> <w n="28.2">voyais</w> <w n="28.3">se</w> <w n="28.4">dresser</w> <w n="28.5">un</w> <w n="28.6">glorieux</w> <w n="28.7">triangle</w></l>
						<l n="29" num="5.5"><w n="29.1">Dont</w> <w n="29.2">la</w> <w n="29.3">Colonne</w> <w n="29.4">et</w> <w n="29.5">l</w>’<w n="29.6">Arc</w> <w n="29.7">formaient</w> <w n="29.8">chacun</w> <w n="29.9">un</w> <w n="29.10">angle</w></l>
						<l n="30" num="5.6"><w n="30.1">Avec</w> <w n="30.2">Notre</w>-<w n="30.3">Dame</w> <w n="30.4">et</w> <w n="30.5">ses</w> <w n="30.6">tours</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Erreur</w> ! <w n="31.2">Illusion</w> ! <w n="31.3">Songe</w> <w n="31.4">trop</w> <w n="31.5">éphémère</w> !</l>
						<l n="32" num="6.2"><w n="32.1">Toi</w>-<w n="32.2">même</w> <w n="32.3">de</w> <w n="32.4">tes</w> <w n="32.5">mains</w> <w n="32.6">as</w> <w n="32.7">détruit</w> <w n="32.8">la</w> <w n="32.9">chimère</w></l>
						<l n="33" num="6.3"><w n="33.1">Que</w> <w n="33.2">tu</w> <w n="33.3">caressais</w> <w n="33.4">autrefois</w>.</l>
						<l n="34" num="6.4"><w n="34.1">Regarde</w> <w n="34.2">donc</w>, <w n="34.3">Hugo</w> ! <w n="34.4">la</w> <w n="34.5">Colonne</w> <w n="34.6">est</w> <w n="34.7">à</w> <w n="34.8">terre</w> ;</l>
						<l n="35" num="6.5"><w n="35.1">Et</w> <w n="35.2">Notre</w>-<w n="35.3">Dame</w> <w n="35.4">voit</w> <w n="35.5">la</w> <w n="35.6">grande</w> <w n="35.7">Arche</w> <w n="35.8">de</w> <w n="35.9">pierre</w></l>
						<l n="36" num="6.6"><w n="36.1">Blessée</w> <w n="36.2">en</w> <w n="36.3">plus</w> <w n="36.4">de</w> <w n="36.5">mille</w> <w n="36.6">endroits</w>.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">Paris</w> <w n="37.2">est</w> <w n="37.3">là</w> <w n="37.4">fumant</w>, <w n="37.5">ainsi</w> <w n="37.6">qu</w>’<w n="37.7">une</w> <w n="37.8">fournaise</w>,</l>
						<l n="38" num="7.2"><w n="38.1">Comme</w> <w n="38.2">un</w> <w n="38.3">vaste</w> <w n="38.4">volcan</w>, <w n="38.5">las</w> <w n="38.6">de</w> <w n="38.7">vomir</w> <w n="38.8">à</w> <w n="38.9">l</w>’<w n="38.10">aise</w></l>
						<l n="39" num="7.3"><w n="39.1">Lave</w> <w n="39.2">et</w> <w n="39.3">roches</w> <w n="39.4">en</w> <w n="39.5">fusion</w>.</l>
						<l n="40" num="7.4"><w n="40.1">Ses</w> <w n="40.2">porches</w>, <w n="40.3">ses</w> <w n="40.4">clochers</w> <w n="40.5">aux</w> <w n="40.6">voix</w> <w n="40.7">tumultueuses</w>,</l>
						<l n="41" num="7.5"><w n="41.1">Ses</w> <w n="41.2">dômes</w>, <w n="41.3">ses</w> <w n="41.4">palais</w>, <w n="41.5">ses</w> <w n="41.6">cités</w> <w n="41.7">tortueuses</w></l>
						<l n="42" num="7.6"><w n="42.1">Sont</w> <w n="42.2">dans</w> <w n="42.3">la</w> <w n="42.4">désolation</w>.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1"><w n="43.1">On</w> <w n="43.2">dirait</w> <w n="43.3">qu</w>’<w n="43.4">on</w> <w n="43.5">entend</w> <w n="43.6">gémir</w>, <w n="43.7">comme</w> <w n="43.8">un</w> <w n="43.9">murmure</w>,</l>
						<l n="44" num="8.2"><w n="44.1">Comme</w> <w n="44.2">un</w> <w n="44.3">écho</w> <w n="44.4">lointain</w> <w n="44.5">mourant</w> <w n="44.6">sous</w> <w n="44.7">la</w> <w n="44.8">ramure</w>,</l>
						<l n="45" num="8.3"><w n="45.1">Les</w> <w n="45.2">sons</w> <w n="45.3">affaiblis</w> <w n="45.4">du</w> <w n="45.5">canon</w>.</l>
						<l n="46" num="8.4"><w n="46.1">La</w> <w n="46.2">guerre</w> <w n="46.3">sociale</w> <w n="46.4">a</w> <w n="46.5">sévi</w> <w n="46.6">dans</w> <w n="46.7">sa</w> <w n="46.8">rage</w> ;</l>
						<l n="47" num="8.5"><w n="47.1">Hugo</w>, <w n="47.2">tu</w> <w n="47.3">secondais</w> <w n="47.4">dans</w> <w n="47.5">l</w>’<w n="47.6">ombre</w> <w n="47.7">son</w> <w n="47.8">ouvrage</w>.</l>
						<l n="48" num="8.6"><w n="48.1">Hugo</w>, <w n="48.2">sois</w> <w n="48.3">maudit</w>, <w n="48.4">ô</w> <w n="48.5">démon</w> !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1"><w n="49.1">Ah</w> ! <w n="49.2">pourquoi</w> <w n="49.3">flattais</w>-<w n="49.4">tu</w>, <w n="49.5">dis</w>-<w n="49.6">moi</w>, <w n="49.7">la</w> <w n="49.8">populace</w> ?</l>
						<l n="50" num="9.2"><w n="50.1">Pourquoi</w> <w n="50.2">ton</w> <w n="50.3">grand</w> <w n="50.4">esprit</w> <w n="50.5">recherchait</w>-<w n="50.6">il</w> <w n="50.7">sa</w> <w n="50.8">grâce</w>,</l>
						<l n="51" num="9.3"><w n="51.1">De</w> <w n="51.2">maints</w> <w n="51.3">sophismes</w> <w n="51.4">enflammé</w> ?</l>
						<l n="52" num="9.4"><w n="52.1">Ah</w> ! <w n="52.2">le</w> <w n="52.3">cœur</w> <w n="52.4">qui</w> <w n="52.5">conçut</w> <w n="52.6">un</w> <w n="52.7">acte</w> <w n="52.8">détestable</w></l>
						<l n="53" num="9.5"><w n="53.1">A</w> <w n="53.2">la</w> <w n="53.3">raison</w> <w n="53.4">parait</w> <w n="53.5">mille</w> <w n="53.6">fois</w> <w n="53.7">plus</w> <w n="53.8">coupable</w></l>
						<l n="54" num="9.6"><w n="54.1">Que</w> <w n="54.2">le</w> <w n="54.3">bras</w> <w n="54.4">qui</w> <w n="54.5">l</w>’<w n="54.6">a</w> <w n="54.7">consommé</w>.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1"><w n="55.1">Il</w> <w n="55.2">ne</w> <w n="55.3">faut</w> <w n="55.4">pas</w> <w n="55.5">jouer</w> <w n="55.6">avec</w> <w n="55.7">la</w> <w n="55.8">multitude</w> :</l>
						<l n="56" num="10.2"><w n="56.1">Elle</w> <w n="56.2">a</w> <w n="56.3">toujours</w> <w n="56.4">passé</w>, <w n="56.5">dans</w> <w n="56.6">son</w> <w n="56.7">incertitude</w>,</l>
						<l n="57" num="10.3"><w n="57.1">Le</w> <w n="57.2">but</w> <w n="57.3">qui</w> <w n="57.4">lui</w> <w n="57.5">fut</w> <w n="57.6">imposé</w>.</l>
						<l n="58" num="10.4"><w n="58.1">Toi</w> <w n="58.2">qui</w> <w n="58.3">l</w>’<w n="58.4">as</w> <w n="58.5">comparée</w> <w n="58.6">à</w> <w n="58.7">l</w>’<w n="58.8">océan</w> <w n="58.9">qui</w> <w n="58.10">gronde</w>,</l>
						<l n="59" num="10.5"><w n="59.1">Tu</w> <w n="59.2">sais</w> <w n="59.3">fort</w> <w n="59.4">bien</w> <w n="59.5">que</w> <w n="59.6">seul</w>, <w n="59.7">Dieu</w>, <w n="59.8">le</w> <w n="59.9">maître</w> <w n="59.10">du</w> <w n="59.11">monde</w>,</l>
						<l n="60" num="10.6"><w n="60.1">Peut</w> <w n="60.2">dompter</w> <w n="60.3">le</w> <w n="60.4">flot</w> <w n="60.5">courroucé</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="61" num="1.1"><w n="61.1">Allons</w> ! <w n="61.2">baisse</w> <w n="61.3">ton</w> <w n="61.4">front</w>, <w n="61.5">trop</w> <w n="61.6">orgueilleux</w> <w n="61.7">poëte</w>.</l>
						<l n="62" num="1.2"><w n="62.1">Sous</w> <w n="62.2">le</w> <w n="62.3">poids</w> <w n="62.4">de</w> <w n="62.5">nos</w> <w n="62.6">maux</w> <w n="62.7">on</w> <w n="62.8">peut</w> <w n="62.9">courber</w> <w n="62.10">la</w> <w n="62.11">tête</w></l>
						<l n="63" num="1.3"><w n="63.1">Devant</w> <w n="63.2">la</w> <w n="63.3">droite</w> <w n="63.4">du</w> <w n="63.5">Seigneur</w>.</l>
						<l n="64" num="1.4"><w n="64.1">Grande</w> <w n="64.2">est</w> <w n="64.3">la</w> <w n="64.4">majesté</w> <w n="64.5">qui</w> <w n="64.6">souvent</w> <w n="64.7">s</w>’<w n="64.8">humilie</w>.</l>
						<l n="65" num="1.5"><w n="65.1">Le</w> <w n="65.2">génie</w> <w n="65.3">hors</w> <w n="65.4">le</w> <w n="65.5">bien</w> <w n="65.6">marche</w> <w n="65.7">vers</w> <w n="65.8">la</w> <w n="65.9">folie</w></l>
						<l n="66" num="1.6"><w n="66.1">Qui</w> <w n="66.2">vient</w> <w n="66.3">succéder</w> <w n="66.4">à</w> <w n="66.5">l</w>’<w n="66.6">erreur</w>.</l>
					</lg>
					<lg n="2">
						<l n="67" num="2.1"><w n="67.1">La</w> <w n="67.2">flamme</w> <w n="67.3">qui</w> <w n="67.4">semait</w> <w n="67.5">dans</w> <w n="67.6">Paris</w> <w n="67.7">l</w>’<w n="67.8">incendie</w>,</l>
						<l n="68" num="2.2"><w n="68.1">Le</w> <w n="68.2">marteau</w> <w n="68.3">qui</w> <w n="68.4">frappait</w> <w n="68.5">la</w> <w n="68.6">Colonne</w> <w n="68.7">hardie</w>,</l>
						<l n="69" num="2.3"><w n="69.1">Le</w> <w n="69.2">canon</w> <w n="69.3">qui</w> <w n="69.4">de</w> <w n="69.5">ses</w> <w n="69.6">boulets</w></l>
						<l n="70" num="2.4"><w n="70.1">Mutilait</w> <w n="70.2">les</w> <w n="70.3">parois</w> <w n="70.4">de</w> <w n="70.5">l</w>’<w n="70.6">Arche</w> <w n="70.7">triomphale</w>,</l>
						<l n="71" num="2.5"><w n="71.1">Tu</w> <w n="71.2">les</w> <w n="71.3">as</w> <w n="71.4">suscités</w>, <w n="71.5">dans</w> <w n="71.6">ton</w> <w n="71.7">erreur</w> <w n="71.8">fatale</w> ;</l>
						<l n="72" num="2.6"><w n="72.1">Par</w> <w n="72.2">tes</w> <w n="72.3">cris</w> <w n="72.4">tu</w> <w n="72.5">les</w> <w n="72.6">appelais</w>.</l>
					</lg>
					<lg n="3">
						<l n="73" num="3.1"><w n="73.1">Baisse</w> <w n="73.2">ton</w> <w n="73.3">front</w>, <w n="73.4">vieillard</w>, <w n="73.5">ou</w> <w n="73.6">crains</w> <w n="73.7">un</w> <w n="73.8">Dieu</w> <w n="73.9">sévère</w>.</l>
						<l n="74" num="3.2"><w n="74.1">S</w>’<w n="74.2">il</w> <w n="74.3">eut</w> <w n="74.4">longtemps</w> <w n="74.5">pour</w> <w n="74.6">toi</w> <w n="74.7">la</w> <w n="74.8">faiblesse</w> <w n="74.9">d</w>’<w n="74.10">un</w> <w n="74.11">père</w>,</l>
						<l n="75" num="3.3"><w n="75.1">Il</w> <w n="75.2">peut</w> <w n="75.3">te</w> <w n="75.4">punir</w> <w n="75.5">maintenant</w>.</l>
						<l n="76" num="3.4"><w n="76.1">N</w>’<w n="76.2">as</w>-<w n="76.3">tu</w> <w n="76.4">pas</w> <w n="76.5">abusé</w> <w n="76.6">de</w> <w n="76.7">ses</w> <w n="76.8">dons</w>, <w n="76.9">du</w> <w n="76.10">génie</w></l>
						<l n="77" num="3.5"><w n="77.1">Dont</w> <w n="77.2">il</w> <w n="77.3">orna</w> <w n="77.4">ton</w> <w n="77.5">âme</w> <w n="77.6">en</w> <w n="77.7">une</w> <w n="77.8">heure</w> <w n="77.9">bénie</w> ?</l>
						<l n="78" num="3.6"><w n="78.1">Poëte</w>, <w n="78.2">crains</w> <w n="78.3">le</w> <w n="78.4">châtiment</w>.</l>
					</lg>
					<lg n="4">
						<l n="79" num="4.1"><w n="79.1">Peut</w>-<w n="79.2">être</w>, <w n="79.3">il</w> <w n="79.4">a</w> <w n="79.5">déjà</w> <w n="79.6">touché</w> <w n="79.7">ton</w> <w n="79.8">front</w> <w n="79.9">superbe</w> :</l>
						<l n="80" num="4.2"><w n="80.1">Les</w> <w n="80.2">fleurs</w> <w n="80.3">qui</w> <w n="80.4">le</w> <w n="80.5">paraient</w> <w n="80.6">sont</w> <w n="80.7">gisantes</w> <w n="80.8">dans</w> <w n="80.9">l</w>’<w n="80.10">herbe</w> ;</l>
						<l n="81" num="4.3"><w n="81.1">Ton</w> <w n="81.2">fils</w> <w n="81.3">est</w> <w n="81.4">mort</w> <w n="81.5">loin</w> <w n="81.6">de</w> <w n="81.7">tes</w> <w n="81.8">bras</w>.</l>
						<l n="82" num="4.4"><w n="82.1">Meurice</w> <w n="82.2">est</w> <w n="82.3">dans</w> <w n="82.4">les</w> <w n="82.5">fers</w> ; <w n="82.6">Paris</w>, <w n="82.7">ta</w> <w n="82.8">grande</w> <w n="82.9">ville</w>,</l>
						<l n="83" num="4.5"><w n="83.1">Est</w> <w n="83.2">pleine</w> <w n="83.3">de</w> <w n="83.4">débris</w>, <w n="83.5">et</w> <w n="83.6">la</w> <w n="83.7">guerre</w> <w n="83.8">civile</w></l>
						<l n="84" num="4.6"><w n="84.1">Expire</w> <w n="84.2">devant</w> <w n="84.3">nos</w> <w n="84.4">soldats</w>.</l>
					</lg>
					<lg n="5">
						<l n="85" num="5.1"><w n="85.1">Tu</w> <w n="85.2">marches</w> <w n="85.3">dans</w> <w n="85.4">l</w>’<w n="85.5">exil</w> ; <w n="85.6">et</w> <w n="85.7">la</w> <w n="85.8">terre</w> <w n="85.9">étrangère</w>,</l>
						<l n="86" num="5.2"><w n="86.1">Comme</w> <w n="86.2">autrefois</w>, <w n="86.3">pour</w> <w n="86.4">toi</w> <w n="86.5">n</w>’<w n="86.6">est</w> <w n="86.7">plus</w> <w n="86.8">hospitalière</w>.</l>
						<l n="87" num="5.3"><w n="87.1">Partout</w>, <w n="87.2">tu</w> <w n="87.3">peux</w> <w n="87.4">être</w> <w n="87.5">chassé</w>,</l>
						<l n="88" num="5.4"><w n="88.1">Jusqu</w>’<w n="88.2">à</w> <w n="88.3">ce</w> <w n="88.4">que</w> <w n="88.5">la</w> <w n="88.6">mort</w>, <w n="88.7">éteignant</w> <w n="88.8">ta</w> <w n="88.9">paupière</w>,</l>
						<l n="89" num="5.5"><w n="89.1">Vienne</w> <w n="89.2">poser</w> <w n="89.3">sa</w> <w n="89.4">main</w> <w n="89.5">plus</w> <w n="89.6">froide</w> <w n="89.7">que</w> <w n="89.8">la</w> <w n="89.9">pierre</w></l>
						<l n="90" num="5.6"><w n="90.1">Sur</w> <w n="90.2">ton</w> <w n="90.3">cœur</w> <w n="90.4">saignant</w> <w n="90.5">et</w> <w n="90.6">brisé</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">4 juin 1871</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>