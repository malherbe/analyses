<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">NOS RUINES</title>
				<title type="medium">Édition électronique</title>
				<author key="ANG">
					<name>
						<forename>Albert</forename>
						<surname>ANGOT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">ANG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>NOS RUINES</title>
						<author>ALBERT ANGOT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30021186v</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>NOS RUINES</title>
								<author>ALBERT ANGOT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DUNIOL ET CIE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANG14">
				<head type="main">SUR LA DÉMOLITION DE LA COLONNE DE LA PLACE VENDÔME</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">grand</w> <w n="1.3">Napoléon</w>, <w n="1.4">quelle</w> <w n="1.5">amère</w> <w n="1.6">pensée</w></l>
						<l n="2" num="1.2"><w n="2.1">Doit</w> <w n="2.2">régner</w> <w n="2.3">aujourd</w>’<w n="2.4">hui</w> <w n="2.5">dans</w> <w n="2.6">ta</w> <w n="2.7">tête</w> <w n="2.8">bronzée</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Au</w> <w n="3.2">faîte</w> <w n="3.3">du</w> <w n="3.4">trône</w> <w n="3.5">d</w>’<w n="3.6">airain</w></l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">autrefois</w> <w n="4.3">a</w> <w n="4.4">taillé</w> <w n="4.5">ta</w> <w n="4.6">droite</w> <w n="4.7">colossale</w></l>
						<l n="5" num="1.5"><w n="5.1">Dans</w> <w n="5.2">les</w> <w n="5.3">mille</w> <w n="5.4">canons</w> <w n="5.5">que</w> <w n="5.6">l</w>’<w n="5.7">Europe</w> <w n="5.8">vassale</w></l>
						<l n="6" num="1.6"><w n="6.1">Livrait</w> <w n="6.2">à</w> <w n="6.3">ton</w> <w n="6.4">bras</w> <w n="6.5">surhumain</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Quand</w> <w n="7.2">tu</w> <w n="7.3">le</w> <w n="7.4">bâtissais</w> <w n="7.5">ce</w> <w n="7.6">bronze</w> <w n="7.7">indélébile</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Tu</w> <w n="8.2">disais</w> : — « <w n="8.3">L</w>’<w n="8.4">étranger</w> <w n="8.5">et</w> <w n="8.6">la</w> <w n="8.7">guerre</w> <w n="8.8">civile</w></l>
						<l n="9" num="2.3">« <w n="9.1">Viendront</w> <w n="9.2">y</w> <w n="9.3">briser</w> <w n="9.4">leurs</w> <w n="9.5">fureurs</w> ;</l>
						<l n="10" num="2.4">« <w n="10.1">Mes</w> <w n="10.2">vieux</w> <w n="10.3">héros</w> <w n="10.4">sculptés</w>, <w n="10.5">l</w>’<w n="10.6">orgueil</w> <w n="10.7">de</w> <w n="10.8">la</w> <w n="10.9">patrie</w>,</l>
						<l n="11" num="2.5">« <w n="11.1">Arrêteront</w> <w n="11.2">les</w> <w n="11.3">rois</w> <w n="11.4">et</w> <w n="11.5">le</w> <w n="11.6">peuple</w> <w n="11.7">en</w> <w n="11.8">furie</w></l>
						<l n="12" num="2.6">« <w n="12.1">Qui</w> <w n="12.2">craindront</w> <w n="12.3">encor</w> <w n="12.4">l</w>’<w n="12.5">Empereur</w>.”</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Oh</w> ! <w n="13.2">combien</w> <w n="13.3">était</w> <w n="13.4">vaine</w>, <w n="13.5">alors</w>, <w n="13.6">ton</w> <w n="13.7">espérance</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Rêve</w> <w n="14.2">prodigieux</w> <w n="14.3">d</w>’<w n="14.4">un</w> <w n="14.5">génie</w> <w n="14.6">en</w> <w n="14.7">démence</w>,</l>
						<l n="15" num="3.3"><w n="15.1">Étincelante</w> <w n="15.2">illusion</w> !</l>
						<l n="16" num="3.4"><w n="16.1">Plus</w> <w n="16.2">tard</w> <w n="16.3">tu</w> <w n="16.4">vis</w> <w n="16.5">ici</w> <w n="16.6">l</w>’<w n="16.7">étranger</w> <w n="16.8">apparaître</w> ;</l>
						<l n="17" num="3.5"><w n="17.1">En</w> <w n="17.2">lui</w> <w n="17.3">tu</w> <w n="17.4">reconnus</w> <w n="17.5">jusqu</w>’<w n="17.6">à</w> <w n="17.7">deux</w> <w n="17.8">fois</w> <w n="17.9">ton</w> <w n="17.10">maître</w> :</l>
						<l n="18" num="3.6"><w n="18.1">Quelle</w> <w n="18.2">amère</w> <w n="18.3">déception</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Ce</w> <w n="19.2">n</w>’<w n="19.3">était</w> <w n="19.4">point</w> <w n="19.5">assez</w> !… <w n="19.6">regarde</w> <w n="19.7">dans</w> <w n="19.8">a</w> <w n="19.9">plaine</w> !</l>
						<l n="20" num="4.2"><w n="20.1">Le</w> <w n="20.2">Prussien</w> <w n="20.3">est</w> <w n="20.4">campé</w> <w n="20.5">sur</w> <w n="20.6">les</w> <w n="20.7">bords</w> <w n="20.8">de</w> <w n="20.9">la</w> <w n="20.10">Seine</w></l>
						<l n="21" num="4.3"><w n="21.1">Hélas</w> ! <w n="21.2">pour</w> <w n="21.3">la</w> <w n="21.4">troisième</w> <w n="21.5">fois</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">Pendant</w> <w n="22.2">que</w> <w n="22.3">sous</w> <w n="22.4">tes</w> <w n="22.5">pieds</w> <w n="22.6">la</w> <w n="22.7">populace</w> <w n="22.8">armée</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Maîtresse</w> <w n="23.2">de</w> <w n="23.3">Paris</w>, <w n="23.4">lutte</w> <w n="23.5">contre</w> <w n="23.6">l</w>’<w n="23.7">armée</w></l>
						<l n="24" num="4.6"><w n="24.1">De</w> <w n="24.2">la</w> <w n="24.3">pauvre</w> <w n="24.4">France</w> <w n="24.5">aux</w> <w n="24.6">abois</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Ce</w> <w n="25.2">que</w> <w n="25.3">Blücher</w> <w n="25.4">ou</w> <w n="25.5">Moltke</w>, <w n="25.6">un</w> <w n="25.7">jour</w>, <w n="25.8">dans</w> <w n="25.9">sa</w> <w n="25.10">colère</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Devant</w> <w n="26.2">Paris</w> <w n="26.3">vaincu</w>, <w n="26.4">jamais</w> <w n="26.5">n</w>’<w n="26.6">eût</w> <w n="26.7">osé</w> <w n="26.8">faire</w>,</l>
						<l n="27" num="5.3"><w n="27.1">Ce</w> <w n="27.2">peuple</w> <w n="27.3">insensé</w> <w n="27.4">le</w> <w n="27.5">fera</w>.</l>
						<l n="28" num="5.4"><w n="28.1">Quand</w> <w n="28.2">le</w> <w n="28.3">pays</w> <w n="28.4">en</w> <w n="28.5">deuil</w> <w n="28.6">sous</w> <w n="28.7">le</w> <w n="28.8">Prussien</w> <w n="28.9">succombe</w>,</l>
						<l n="29" num="5.5"><w n="29.1">Il</w> <w n="29.2">voudra</w>, <w n="29.3">sous</w> <w n="29.4">ses</w> <w n="29.5">coups</w>, <w n="29.6">que</w> <w n="29.7">la</w> <w n="29.8">Colonne</w> <w n="29.9">tombe</w> ;</l>
						<l n="30" num="5.6"><w n="30.1">Et</w> <w n="30.2">ses</w> <w n="30.3">débris</w> <w n="30.4">il</w> <w n="30.5">les</w> <w n="30.6">vendra</w> !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Un</w> <w n="31.2">ramas</w> <w n="31.3">d</w>’<w n="31.4">étrangers</w>, <w n="31.5">sans</w> <w n="31.6">foyers</w>, <w n="31.7">sans</w> <w n="31.8">famille</w>,</l>
						<l n="32" num="6.2"><w n="32.1">Rebut</w> <w n="32.2">des</w> <w n="32.3">nations</w>, <w n="32.4">exploite</w> <w n="32.5">la</w> <w n="32.6">Guenille</w></l>
						<l n="33" num="6.3"><w n="33.1">Au</w> <w n="33.2">nom</w> <w n="33.3">de</w> <w n="33.4">la</w> <w n="33.5">Fraternité</w>.</l>
						<l n="34" num="6.4"><w n="34.1">Que</w> <w n="34.2">leur</w> <w n="34.3">font</w>, <w n="34.4">après</w> <w n="34.5">tout</w>, <w n="34.6">les</w> <w n="34.7">gloires</w> <w n="34.8">de</w> <w n="34.9">la</w> <w n="34.10">France</w>,</l>
						<l n="35" num="6.5"><w n="35.1">Pourvu</w> <w n="35.2">qu</w>’<w n="35.3">avec</w> <w n="35.4">ivresse</w>, <w n="35.5">au</w> <w n="35.6">soleil</w>, <w n="35.7">leur</w> <w n="35.8">démence</w></l>
						<l n="36" num="6.6"><w n="36.1">Étale</w> <w n="36.2">sa</w> <w n="36.3">lubricité</w> !</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">Que</w> <w n="37.2">leur</w> <w n="37.3">font</w> <w n="37.4">ce</w> <w n="37.5">faisceau</w> <w n="37.6">de</w> <w n="37.7">souvenirs</w> <w n="37.8">épiques</w>,</l>
						<l n="38" num="7.2"><w n="38.1">Nos</w> <w n="38.2">guerriers</w> <w n="38.3">chérissant</w> <w n="38.4">les</w> <w n="38.5">combats</w> <w n="38.6">héroïques</w>,</l>
						<l n="39" num="7.3"><w n="39.1">Le</w> <w n="39.2">bruit</w> <w n="39.3">du</w> <w n="39.4">fer</w> <w n="39.5">contre</w> <w n="39.6">le</w> <w n="39.7">fer</w>,</l>
						<l n="40" num="7.4"><w n="40.1">Le</w> <w n="40.2">fracas</w> <w n="40.3">des</w> <w n="40.4">canons</w>, <w n="40.5">la</w> <w n="40.6">clameur</w> <w n="40.7">des</w> <w n="40.8">cymbales</w>,</l>
						<l n="41" num="7.5"><w n="41.1">Répondant</w> <w n="41.2">par</w> <w n="41.3">une</w> <w n="41.4">hymne</w> <w n="41.5">au</w> <w n="41.6">sifflement</w> <w n="41.7">des</w> <w n="41.8">balles</w>,</l>
						<l n="42" num="7.6"><w n="42.1">Avec</w> <w n="42.2">Desaix</w>, <w n="42.3">avec</w> <w n="42.4">Kléber</w> !</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1"><w n="43.1">Que</w> <w n="43.2">leur</w> <w n="43.3">font</w> <w n="43.4">nos</w> <w n="43.5">succès</w> <w n="43.6">qui</w> <w n="43.7">tiennent</w> <w n="43.8">du</w> <w n="43.9">prodige</w> :</l>
						<l n="44" num="8.2"><w n="44.1">Et</w> <w n="44.2">Jourdan</w> <w n="44.3">sur</w> <w n="44.4">la</w> <w n="44.5">Sombre</w> <w n="44.6">et</w> <w n="44.7">Joubert</w> <w n="44.8">sur</w> <w n="44.9">l</w>’<w n="44.10">Adige</w>,</l>
						<l n="45" num="8.3"><w n="45.1">Gaëte</w> <w n="45.2">pris</w> <w n="45.3">par</w> <w n="45.4">Masséna</w>,</l>
						<l n="46" num="8.4"><w n="46.1">Pichegru</w> <w n="46.2">s</w>’<w n="46.3">emparant</w> <w n="46.4">des</w> <w n="46.5">flottes</w> <w n="46.6">assiégées</w> :</l>
						<l n="47" num="8.5"><w n="47.1">Du</w> <w n="47.2">levant</w> <w n="47.3">au</w> <w n="47.4">couchant</w> <w n="47.5">cent</w> <w n="47.6">batailles</w> <w n="47.7">rangées</w>,</l>
						<l n="48" num="8.6"><w n="48.1">Thabor</w>, <w n="48.2">Austerlitz</w>, <w n="48.3">Iéna</w> !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">« <w n="49.1">Oui</w>, <w n="49.2">qu</w>’<w n="49.3">importe</w> ! <w n="49.4">Aujourd</w>’<w n="49.5">hui</w> <w n="49.6">les</w> <w n="49.7">peuples</w> <w n="49.8">sont</w> <w n="49.9">tous</w> <w n="49.10">frères</w></l>
						<l n="50" num="9.2">« <w n="50.1">A</w> <w n="50.2">bas</w> <w n="50.3">tous</w> <w n="50.4">les</w> <w n="50.5">tyrans</w> ! <w n="50.6">A</w> <w n="50.7">bas</w> <w n="50.8">toutes</w> <w n="50.9">les</w> <w n="50.10">guerres</w> !</l>
						<l n="51" num="9.3">« <w n="51.1">Le</w> <w n="51.2">Peuple</w> <w n="51.3">doit</w> <w n="51.4">tout</w> <w n="51.5">dominer</w>.</l>
						<l n="52" num="9.4">« <w n="52.1">Brisons</w> <w n="52.2">les</w> <w n="52.3">monuments</w> <w n="52.4">de</w> <w n="52.5">toute</w> <w n="52.6">tyrannie</w> !</l>
						<l n="53" num="9.5">« <w n="53.1">Que</w> <w n="53.2">la</w> <w n="53.3">route</w> <w n="53.4">par</w> <w n="53.5">nos</w> <w n="53.6">au</w> <w n="53.7">Droit</w> <w n="53.8">soit</w> <w n="53.9">aplanie</w>.</l>
						<l n="54" num="9.6">« <w n="54.1">C</w>’<w n="54.2">est</w> <w n="54.3">le</w> <w n="54.4">Peuple</w> <w n="54.5">qui</w> <w n="54.6">doit</w> <w n="54.7">régner</w>.”</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1"><w n="55.1">S</w>’<w n="55.2">ils</w> <w n="55.3">veulent</w> <w n="55.4">renverser</w> <w n="55.5">la</w> <w n="55.6">Colonne</w> <w n="55.7">immortelle</w>,</l>
						<l n="56" num="10.2"><w n="56.1">C</w>’<w n="56.2">est</w> <w n="56.3">que</w>, <w n="56.4">dans</w> <w n="56.5">leurs</w> <w n="56.6">excès</w>, <w n="56.7">ils</w> <w n="56.8">tremblent</w> <w n="56.9">devant</w> <w n="56.10">elle</w>,</l>
						<l n="57" num="10.3"><w n="57.1">C</w>’<w n="57.2">est</w> <w n="57.3">qu</w>’<w n="57.4">ils</w> <w n="57.5">redoutent</w> <w n="57.6">l</w>’<w n="57.7">Empereur</w> ;</l>
						<l n="58" num="10.4"><w n="58.1">C</w>’<w n="58.2">est</w> <w n="58.3">qu</w>’<w n="58.4">ils</w> <w n="58.5">ont</w> <w n="58.6">peur</w> <w n="58.7">de</w> <w n="58.8">voir</w>, <w n="58.9">un</w> <w n="58.10">jour</w>, <w n="58.11">leur</w> <w n="58.12">conscience</w></l>
						<l n="59" num="10.5"><w n="59.1">Leur</w> <w n="59.2">dire</w> <w n="59.3">à</w> <w n="59.4">son</w> <w n="59.5">aspect</w> : — « <w n="59.6">Tu</w> <w n="59.7">is</w> <w n="59.8">servir</w> <w n="59.9">la</w> <w n="59.10">France</w></l>
						<l n="60" num="10.6">« <w n="60.1">Au</w> <w n="60.2">profit</w> <w n="60.3">de</w> <w n="60.4">ton</w> <w n="60.5">déshonneur</w> ! »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="61" num="1.1"><w n="61.1">Oh</w> ! <w n="61.2">tu</w> <w n="61.3">ne</w> <w n="61.4">peux</w> <w n="61.5">périr</w>, <w n="61.6">Colonne</w> <w n="61.7">trois</w> <w n="61.8">fois</w> <w n="61.9">sainte</w>,</l>
						<l n="62" num="1.2"><w n="62.1">Où</w> <w n="62.2">la</w> <w n="62.3">foudre</w> <w n="62.4">en</w> <w n="62.5">tes</w> <w n="62.6">flancs</w> <w n="62.7">se</w> <w n="62.8">cache</w> <w n="62.9">à</w> <w n="62.10">peine</w> <w n="62.11">éteinte</w> ;</l>
						<l n="63" num="1.3"><w n="63.1">Dis</w>-<w n="63.2">moi</w> <w n="63.3">que</w> <w n="63.4">tu</w> <w n="63.5">ne</w> <w n="63.6">peux</w> <w n="63.7">périr</w> !</l>
						<l n="64" num="1.4"><w n="64.1">Dis</w> <w n="64.2">que</w> <w n="64.3">ton</w> <w n="64.4">bronze</w> <w n="64.5">est</w> <w n="64.6">fort</w> <w n="64.7">en</w> <w n="64.8">sa</w> <w n="64.9">longue</w> <w n="64.10">spirale</w>,</l>
						<l n="65" num="1.5"><w n="65.1">Qu</w>’<w n="65.2">il</w> <w n="65.3">saura</w> <w n="65.4">résister</w> <w n="65.5">à</w> <w n="65.6">leur</w> <w n="65.7">main</w> <w n="65.8">infernale</w> ;</l>
						<l n="66" num="1.6"><w n="66.1">Dis</w>-<w n="66.2">moi</w> <w n="66.3">que</w> <w n="66.4">tu</w> <w n="66.5">ne</w> <w n="66.6">peux</w> <w n="66.7">mourir</w> !</l>
					</lg>
					<lg n="2">
						<l n="67" num="2.1"><w n="67.1">Dis</w>-<w n="67.2">moi</w> <w n="67.3">que</w> <w n="67.4">de</w> <w n="67.5">tes</w> <w n="67.6">flancs</w> <w n="67.7">l</w>’<w n="67.8">héroïque</w> <w n="67.9">sculpture</w></l>
						<l n="68" num="2.2"><w n="68.1">Bravera</w> <w n="68.2">leurs</w> <w n="68.3">marteaux</w>, <w n="68.4">comme</w> <w n="68.5">la</w> <w n="68.6">vieille</w> <w n="68.7">armure</w></l>
						<l n="69" num="2.3"><w n="69.1">D</w>’<w n="69.2">un</w> <w n="69.3">chevalier</w> <w n="69.4">bardé</w> <w n="69.5">de</w> <w n="69.6">fer</w>,</l>
						<l n="70" num="2.4"><w n="70.1">Qui</w>, <w n="70.2">ferme</w> <w n="70.3">comme</w> <w n="70.4">un</w> <w n="70.5">roc</w>, <w n="70.6">au</w> <w n="70.7">sein</w> <w n="70.8">de</w> <w n="70.9">la</w> <w n="70.10">bataille</w>,</l>
						<l n="71" num="2.5"><w n="71.1">Supportait</w> <w n="71.2">tous</w> <w n="71.3">les</w> <w n="71.4">coups</w> <w n="71.5">et</w> <w n="71.6">d</w>’<w n="71.7">estoc</w> <w n="71.8">et</w> <w n="71.9">de</w> <w n="71.10">taille</w></l>
						<l n="72" num="2.6"><w n="72.1">Qui</w> <w n="72.2">pleuvaient</w> <w n="72.3">drus</w> <w n="72.4">sur</w> <w n="72.5">son</w> <w n="72.6">haubert</w>.</l>
					</lg>
					<lg n="3">
						<l n="73" num="3.1"><w n="73.1">Dis</w> <w n="73.2">que</w> <w n="73.3">c</w>’<w n="73.4">est</w> <w n="73.5">le</w> <w n="73.6">géant</w> <w n="73.7">de</w> <w n="73.8">la</w> <w n="73.9">Rome</w> <w n="73.10">française</w></l>
						<l n="74" num="3.2"><w n="74.1">Qui</w> <w n="74.2">tordit</w> <w n="74.3">tout</w> <w n="74.4">ensemble</w> <w n="74.5">en</w> <w n="74.6">sa</w> <w n="74.7">vaste</w> <w n="74.8">fournaise</w></l>
						<l n="75" num="3.3"><w n="75.1">Ton</w> <w n="75.2">bronze</w> <w n="75.3">et</w> <w n="75.4">l</w>’<w n="75.5">immortalité</w> !</l>
						<l n="76" num="3.4"><w n="76.1">Oh</w> ! <w n="76.2">dis</w>-<w n="76.3">moi</w> <w n="76.4">que</w> <w n="76.5">ton</w> <w n="76.6">moule</w> <w n="76.7">est</w> <w n="76.8">gardé</w> <w n="76.9">par</w> <w n="76.10">la</w> <w n="76.11">gloire</w> !</l>
						<l n="77" num="3.5"><w n="77.1">Qu</w>’<w n="77.2">on</w> <w n="77.3">peut</w> <w n="77.4">le</w> <w n="77.5">reconstruire</w>, <w n="77.6">en</w> <w n="77.7">consultant</w> <w n="77.8">l</w>’<w n="77.9">Histoire</w></l>
						<l n="78" num="3.6"><w n="78.1">Où</w> <w n="78.2">notre</w> <w n="78.3">nom</w> <w n="78.4">est</w> <w n="78.5">répété</w> !</l>
					</lg>
					<lg n="4">
						<l n="79" num="4.1"><w n="79.1">Ah</w> ! <w n="79.2">c</w>’<w n="79.3">est</w> <w n="79.4">qu</w>’<w n="79.5">on</w> <w n="79.6">a</w> <w n="79.7">besoin</w>, <w n="79.8">en</w> <w n="79.9">ces</w> <w n="79.10">jours</w> <w n="79.11">de</w> <w n="79.12">tristesses</w>,</l>
						<l n="80" num="4.2"><w n="80.1">D</w>’<w n="80.2">avoir</w> <w n="80.3">un</w> <w n="80.4">souvenir</w> <w n="80.5">de</w> <w n="80.6">nos</w> <w n="80.7">vieilles</w> <w n="80.8">prouesses</w>,</l>
						<l n="81" num="4.3"><w n="81.1">Avant</w> <w n="81.2">de</w> <w n="81.3">nous</w> <w n="81.4">pouvoir</w> <w n="81.5">venger</w> ;</l>
						<l n="82" num="4.4"><w n="82.1">Pour</w> <w n="82.2">voir</w> <w n="82.3">en</w> <w n="82.4">ce</w> <w n="82.5">moment</w>, <w n="82.6">sans</w> <w n="82.7">éprouver</w> <w n="82.8">de</w> <w n="82.9">honte</w>,</l>
						<l n="83" num="4.5"><w n="83.1">Sans</w> <w n="83.2">qu</w>’<w n="83.3">un</w> <w n="83.4">flot</w> <w n="83.5">de</w> <w n="83.6">rougeur</w> <w n="83.7">à</w> <w n="83.8">la</w> <w n="83.9">face</w> <w n="83.10">nous</w> <w n="83.11">monte</w>,</l>
						<l n="84" num="4.6"><w n="84.1">Sous</w> <w n="84.2">nos</w> <w n="84.3">murs</w> <w n="84.4">camper</w> <w n="84.5">l</w>’<w n="84.6">étranger</w> !</l>
					</lg>
					<lg n="5">
						<l n="85" num="5.1"><w n="85.1">Certes</w>, <w n="85.2">on</w> <w n="85.3">a</w> <w n="85.4">besoin</w> <w n="85.5">d</w>’<w n="85.6">un</w> <w n="85.7">monument</w> <w n="85.8">sublime</w>,</l>
						<l n="86" num="5.2"><w n="86.1">Quelque</w> <w n="86.2">bronze</w> <w n="86.3">vengeur</w>, <w n="86.4">quelque</w> <w n="86.5">trophée</w> <w n="86.6">opime</w>,</l>
						<l n="87" num="5.3"><w n="87.1">Pour</w> <w n="87.2">montrer</w> <w n="87.3">à</w> <w n="87.4">ses</w> <w n="87.5">fiers</w> <w n="87.6">vainqueurs</w> ;</l>
						<l n="88" num="5.4"><w n="88.1">Pour</w> <w n="88.2">leur</w> <w n="88.3">dire</w> <w n="88.4">qu</w>’<w n="88.5">aussi</w>, <w n="88.6">lors</w> <w n="88.7">de</w> <w n="88.8">notre</w> <w n="88.9">puissance</w>,</l>
						<l n="89" num="5.5"><w n="89.1">Nous</w> <w n="89.2">les</w> <w n="89.3">vîmes</w> <w n="89.4">plongés</w> <w n="89.5">au</w> <w n="89.6">sein</w> <w n="89.7">de</w> <w n="89.8">la</w> <w n="89.9">souffrance</w>,</l>
						<l n="90" num="5.6"><w n="90.1">Nous</w> <w n="90.2">les</w> <w n="90.3">vîmes</w> <w n="90.4">verser</w> <w n="90.5">des</w> <w n="90.6">pleurs</w>.</l>
					</lg>
					<lg n="6">
						<l n="91" num="6.1"><w n="91.1">Voilà</w> <w n="91.2">pourquoi</w>, <w n="91.3">Colonne</w>, <w n="91.4">image</w> <w n="91.5">de</w> <w n="91.6">victoire</w>,</l>
						<l n="92" num="6.2"><w n="92.1">Pourquoi</w> <w n="92.2">je</w> <w n="92.3">veux</w> <w n="92.4">garder</w> <w n="92.5">les</w> <w n="92.6">fantômes</w> <w n="92.7">de</w> <w n="92.8">gloire</w></l>
						<l n="93" num="6.3"><w n="93.1">Qui</w> <w n="93.2">se</w> <w n="93.3">pressent</w> <w n="93.4">à</w> <w n="93.5">tes</w> <w n="93.6">côtés</w>,</l>
						<l n="94" num="6.4"><w n="94.1">Je</w> <w n="94.2">veux</w> <w n="94.3">voir</w> <w n="94.4">tes</w> <w n="94.5">soldats</w>, <w n="94.6">ces</w> <w n="94.7">héros</w> <w n="94.8">d</w>’<w n="94.9">un</w> <w n="94.10">autre</w> <w n="94.11">âge</w>,</l>
						<l n="95" num="6.5"><w n="95.1">Sortir</w> <w n="95.2">encor</w> <w n="95.3">vainqueurs</w> <w n="95.4">du</w> <w n="95.5">nouvel</w> <w n="95.6">esclavage</w></l>
						<l n="96" num="6.6"><w n="96.1">De</w> <w n="96.2">ces</w> <w n="96.3">Vandales</w> <w n="96.4">détestés</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="97" num="1.1"><w n="97.1">Tremblez</w>, <w n="97.2">vils</w> <w n="97.3">étrangers</w> <w n="97.4">et</w> <w n="97.5">vile</w> <w n="97.6">populace</w> !</l>
						<l n="98" num="1.2"><w n="98.1">Dans</w> <w n="98.2">le</w> <w n="98.3">sein</w> <w n="98.4">des</w> <w n="98.5">Français</w> <w n="98.6">la</w> <w n="98.7">colère</w> <w n="98.8">s</w>’<w n="98.9">amasse</w>,</l>
						<l n="99" num="1.3"><w n="99.1">Déjà</w> <w n="99.2">votre</w> <w n="99.3">règne</w> <w n="99.4">est</w> <w n="99.5">passé</w>.</l>
						<l n="100" num="1.4"><w n="100.1">Tremblez</w> ! <w n="100.2">J</w>’<w n="100.3">entends</w> <w n="100.4">déjà</w> <w n="100.5">notre</w> <w n="100.6">canon</w> <w n="100.7">qui</w> <w n="100.8">gronde</w></l>
						<l n="101" num="1.5"><w n="101.1">Dans</w> <w n="101.2">le</w> <w n="101.3">lointain</w> ; <w n="101.4">et</w> <w n="101.5">sans</w> <w n="101.6">que</w> <w n="101.7">le</w> <w n="101.8">vôtre</w> <w n="101.9">y</w> <w n="101.10">réponde</w>,</l>
						<l n="102" num="1.6"><w n="102.1">Le</w> <w n="102.2">vrai</w> <w n="102.3">Français</w> <w n="102.4">s</w>’<w n="102.5">est</w> <w n="102.6">avancé</w>.</l>
					</lg>
					<lg n="2">
						<l n="103" num="2.1"><w n="103.1">Tremblez</w> ! <w n="103.2">déjà</w> <w n="103.3">j</w>’<w n="103.4">entends</w>, <w n="103.5">avec</w> <w n="103.6">de</w> <w n="103.7">longs</w> <w n="103.8">murmures</w>,</l>
						<l n="104" num="2.2"><w n="104.1">Au</w> <w n="104.2">sein</w> <w n="104.3">du</w> <w n="104.4">monument</w> <w n="104.5">résonner</w> <w n="104.6">des</w> <w n="104.7">armures</w>,</l>
						<l n="105" num="2.3"><w n="105.1">Et</w> <w n="105.2">comme</w> <w n="105.3">un</w> <w n="105.4">bruit</w> <w n="105.5">confus</w> <w n="105.6">de</w> <w n="105.7">pas</w>.</l>
						<l n="106" num="2.4"><w n="106.1">On</w> <w n="106.2">dirait</w> <w n="106.3">que</w>, <w n="106.4">soudain</w>, <w n="106.5">renaissent</w> <w n="106.6">de</w> <w n="106.7">leur</w> <w n="106.8">cendre</w>,</l>
						<l n="107" num="2.5"><w n="107.1">Les</w> <w n="107.2">vieux</w> <w n="107.3">héros</w> <w n="107.4">bronzés</w> <w n="107.5">cherchent</w> <w n="107.6">à</w> <w n="107.7">redescendre</w></l>
						<l n="108" num="2.6"><w n="108.1">Pour</w> <w n="108.2">combattre</w> <w n="108.3">avec</w> <w n="108.4">nos</w> <w n="108.5">soldats</w>.</l>
					</lg>
					<lg n="3">
						<l n="109" num="3.1"><w n="109.1">Vous</w> <w n="109.2">avez</w> <w n="109.3">réveillé</w> <w n="109.4">ces</w> <w n="109.5">ombres</w> <w n="109.6">immortelles</w>.</l>
						<l n="110" num="3.2"><w n="110.1">Les</w> <w n="110.2">aigles</w>, <w n="110.3">leurs</w> <w n="110.4">gardiens</w>, <w n="110.5">ont</w> <w n="110.6">agité</w> <w n="110.7">leurs</w> <w n="110.8">ailes</w>.</l>
						<l n="111" num="3.3"><w n="111.1">Craignez</w> <w n="111.2">peut</w>-<w n="111.3">être</w> <w n="111.4">leur</w> <w n="111.5">fureur</w> !</l>
						<l n="112" num="3.4"><w n="112.1">Les</w> <w n="112.2">foudres</w> <w n="112.3">apaisées</w> <w n="112.4">qu</w>’<w n="112.5">ils</w> <w n="112.6">portent</w> <w n="112.7">dans</w> <w n="112.8">leurs</w> <w n="112.9">serres</w></l>
						<l n="113" num="3.5"><w n="113.1">Pourraient</w> <w n="113.2">bien</w> <w n="113.3">contre</w> <w n="113.4">vous</w> <w n="113.5">déchaînant</w> <w n="113.6">leurs</w> <w n="113.7">colères</w>,</l>
						<l n="114" num="3.6"><w n="114.1">Vous</w> <w n="114.2">renverser</w> <w n="114.3">d</w>’<w n="114.4">un</w> <w n="114.5">trait</w> <w n="114.6">vengeur</w>.</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">ÉPILOGUE</head>
					<lg n="1">
						<l n="115" num="1.1"><w n="115.1">Et</w> <w n="115.2">toi</w>, <w n="115.3">Hugo</w>, <w n="115.4">toi</w>, <w n="115.5">qui</w> <w n="115.6">célébrais</w> <w n="115.7">la</w> <w n="115.8">Colonne</w></l>
						<l n="116" num="1.2"><w n="116.1">Sur</w> <w n="116.2">ton</w> <w n="116.3">luth</w> <w n="116.4">immortel</w>, <w n="116.5">dans</w> <w n="116.6">un</w> <w n="116.7">vers</w> <w n="116.8">qui</w> <w n="116.9">résonne</w></l>
						<l n="117" num="1.3"><w n="117.1">Ainsi</w> <w n="117.2">qu</w>’<w n="117.3">une</w> <w n="117.4">cloche</w> <w n="117.5">d</w>’<w n="117.6">airain</w> ;</l>
						<l n="118" num="1.4"><w n="118.1">Toi</w>, <w n="118.2">qui</w> <w n="118.3">chantais</w> <w n="118.4">jadis</w> <w n="118.5">nos</w> <w n="118.6">splendides</w> <w n="118.7">annales</w>,</l>
						<l n="119" num="1.5"><w n="119.1">Au</w> <w n="119.2">sein</w> <w n="119.3">des</w> <w n="119.4">nations</w> <w n="119.5">nos</w> <w n="119.6">courses</w> <w n="119.7">triomphales</w>,</l>
						<l n="120" num="1.6"><w n="120.1">Le</w> <w n="120.2">front</w> <w n="120.3">pur</w> <w n="120.4">comme</w> <w n="120.5">un</w> <w n="120.6">jour</w> <w n="120.7">serein</w> ;</l>
					</lg>
					<lg n="2">
						<l n="121" num="2.1"><w n="121.1">Oh</w> ! <w n="121.2">que</w> <w n="121.3">tu</w> <w n="121.4">dois</w> <w n="121.5">rougir</w> <w n="121.6">de</w> <w n="121.7">cette</w> <w n="121.8">multitude</w></l>
						<l n="122" num="2.2"><w n="122.1">Que</w> <w n="122.2">flattais</w> <w n="122.3">avec</w> <w n="122.4">trop</w> <w n="122.5">de</w> <w n="122.6">sollicitude</w>,</l>
						<l n="123" num="2.3"><w n="123.1">De</w> <w n="123.2">ces</w> <w n="123.3">cruels</w> <w n="123.4">démolisseurs</w> !</l>
						<l n="124" num="2.4"><w n="124.1">S</w>’<w n="124.2">il</w> <w n="124.3">est</w> <w n="124.4">bon</w> <w n="124.5">de</w> <w n="124.6">songer</w> <w n="124.7">à</w> <w n="124.8">clamer</w> <w n="124.9">leur</w> <w n="124.10">souffrance</w>,</l>
						<l n="125" num="2.5"><w n="125.1">Il</w> <w n="125.2">faut</w> <w n="125.3">être</w> <w n="125.4">assez</w> <w n="125.5">fort</w> <w n="125.6">pour</w> <w n="125.7">braver</w> <w n="125.8">leur</w> <w n="125.9">licence</w>,</l>
						<l n="126" num="2.6"><w n="126.1">Et</w> <w n="126.2">pour</w> <w n="126.3">enchaîner</w> <w n="126.4">leurs</w> <w n="126.5">erreurs</w>.</l>
					</lg>
					<lg n="3">
						<l n="127" num="3.1"><w n="127.1">Démolir</w> <w n="127.2">ta</w> <w n="127.3">Colonne</w> !… <w n="127.4">y</w> <w n="127.5">songes</w>-<w n="127.6">tu</w> ?… <w n="127.7">quel</w> <w n="127.8">crime</w> !</l>
						<l n="128" num="3.2"><w n="128.1">Lève</w>-<w n="128.2">toi</w> <w n="128.3">donc</w>, <w n="128.4">poëte</w> !… <w n="128.5">à</w> <w n="128.6">ta</w> <w n="128.7">lyre</w> <w n="128.8">sublime</w></l>
						<l n="129" num="3.3"><w n="129.1">Attache</w> <w n="129.2">une</w> <w n="129.3">corde</w> <w n="129.4">de</w> <w n="129.5">fer</w>.</l>
						<l n="130" num="3.4"><w n="130.1">Car</w> <w n="130.2">tu</w> <w n="130.3">l</w>’<w n="130.4">as</w> <w n="130.5">dit</w> :— »<w n="130.6">Malheur</w> <w n="130.7">à</w> <w n="130.8">qui</w> <w n="130.9">dit</w> <w n="130.10">à</w> <w n="130.11">ses</w> <w n="130.12">frères</w>,</l>
						<l n="131" num="3.5">« <w n="131.1">Dans</w> <w n="131.2">un</w> <w n="131.3">temps</w> <w n="131.4">tout</w> <w n="131.5">rongé</w> <w n="131.6">de</w> <w n="131.7">haine</w> <w n="131.8">et</w> <w n="131.9">de</w> <w n="131.10">misères</w> :</l>
						<l n="132" num="3.6">« <w n="132.1">Je</w> <w n="132.2">retourne</w> <w n="132.3">dans</w> <w n="132.4">le</w> <w n="132.5">désert</w>. »</l>
					</lg>
					<lg n="4">
						<l n="133" num="4.1"><w n="133.1">Me</w> <w n="133.2">laisseras</w>-<w n="133.3">tu</w> <w n="133.4">seul</w>, <w n="133.5">égoïste</w> <w n="133.6">poëte</w>,</l>
						<l n="134" num="4.2"><w n="134.1">Exhaler</w> <w n="134.2">mes</w> <w n="134.3">soupirs</w> <w n="134.4">qu</w>’<w n="134.5">emporte</w> <w n="134.6">la</w> <w n="134.7">tempête</w></l>
						<l n="135" num="4.3"><w n="135.1">Dans</w> <w n="135.2">une</w> <w n="135.3">rafale</w> <w n="135.4">de</w> <w n="135.5">vent</w> ?</l>
						<l n="136" num="4.4"><w n="136.1">Viens</w> <w n="136.2">à</w> <w n="136.3">ma</w> <w n="136.4">faible</w> <w n="136.5">voix</w> <w n="136.6">unir</w> <w n="136.7">ta</w> <w n="136.8">voix</w> <w n="136.9">de</w> <w n="136.10">flamme</w>,</l>
						<l n="137" num="4.5"><w n="137.1">Pour</w> <w n="137.2">dompter</w> <w n="137.3">le</w> <w n="137.4">courroux</w> <w n="137.5">et</w> <w n="137.6">le</w> <w n="137.7">bruit</w> <w n="137.8">de</w> <w n="137.9">la</w> <w n="137.10">lame</w>,</l>
						<l n="138" num="4.6"><w n="138.1">Et</w> <w n="138.2">faire</w> <w n="138.3">taire</w> <w n="138.4">l</w>’<w n="138.5">ouragan</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">25 avril 1871.</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>