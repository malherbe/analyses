<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">NOS RUINES</title>
				<title type="medium">Édition électronique</title>
				<author key="ANG">
					<name>
						<forename>Albert</forename>
						<surname>ANGOT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">ANG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>NOS RUINES</title>
						<author>ALBERT ANGOT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30021186v</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>NOS RUINES</title>
								<author>ALBERT ANGOT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DUNIOL ET CIE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANG4">
				<head type="main">SEDAN</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">bataille</w> <w n="1.3">a</w> <w n="1.4">duré</w> <w n="1.5">trois</w> <w n="1.6">mortelles</w> <w n="1.7">journées</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">sol</w> <w n="2.3">est</w> <w n="2.4">parsemé</w> <w n="2.5">d</w>’<w n="2.6">armes</w> <w n="2.7">abandonnées</w>,</l>
						<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">casques</w>, <w n="3.3">de</w> <w n="3.4">képis</w> <w n="3.5">sanglants</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">cadavres</w> <w n="4.3">en</w> <w n="4.4">tas</w> <w n="4.5">s</w>’<w n="4.6">élèvent</w> <w n="4.7">dans</w> <w n="4.8">la</w> <w n="4.9">plaine</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">Français</w> <w n="5.3">expiré</w> <w n="5.4">serrant</w> <w n="5.5">son</w> <w n="5.6">arme</w> <w n="5.7">vaine</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Menace</w> <w n="6.2">encore</w> <w n="6.3">les</w> <w n="6.4">Allemands</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Là</w>, <w n="7.2">ce</w> <w n="7.3">sont</w> <w n="7.4">des</w> <w n="7.5">affûts</w> <w n="7.6">brisés</w> <w n="7.7">par</w> <w n="7.8">la</w> <w n="7.9">mitraille</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Des</w> <w n="8.2">caissons</w>, <w n="8.3">des</w> <w n="8.4">boulets</w> ; <w n="8.5">là</w>, <w n="8.6">des</w> <w n="8.7">pans</w> <w n="8.8">de</w> <w n="8.9">muraille</w></l>
						<l n="9" num="2.3"><w n="9.1">Troués</w> <w n="9.2">ainsi</w> <w n="9.3">que</w> <w n="9.4">des</w> <w n="9.5">haillons</w>.</l>
						<l n="10" num="2.4"><w n="10.1">Par</w> <w n="10.2">là</w> <w n="10.3">des</w> <w n="10.4">cavaliers</w> <w n="10.5">ont</w> <w n="10.6">passé</w> <w n="10.7">par</w> <w n="10.8">nuées</w> ;</l>
						<l n="11" num="2.5"><w n="11.1">Leurs</w> <w n="11.2">troupes</w> <w n="11.3">par</w> <w n="11.4">la</w> <w n="11.5">mort</w> <w n="11.6">en</w> <w n="11.7">vain</w> <w n="11.8">diminuées</w></l>
						<l n="12" num="2.6"><w n="12.1">Chargeaient</w> <w n="12.2">ici</w> <w n="12.3">les</w> <w n="12.4">bataillons</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">En</w> <w n="13.2">braves</w> <w n="13.3">ils</w> <w n="13.4">son</w> <w n="13.5">morts</w>, <w n="13.6">au</w> <w n="13.7">trépas</w> <w n="13.8">faisant</w> <w n="13.9">face</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Raillant</w> <w n="14.2">les</w> <w n="14.3">biscaïens</w> <w n="14.4">qui</w> <w n="14.5">trouaient</w> <w n="14.6">leur</w> <w n="14.7">cuirasse</w></l>
						<l n="15" num="3.3"><w n="15.1">Et</w> <w n="15.2">faussaient</w> <w n="15.3">leur</w> <w n="15.4">casque</w> <w n="15.5">d</w>’<w n="15.6">acier</w>.</l>
						<l n="16" num="3.4"><w n="16.1">Un</w> <w n="16.2">blessé</w> <w n="16.3">çà</w> <w n="16.4">et</w> <w n="16.5">là</w> <w n="16.6">un</w> <w n="16.7">instant</w> <w n="16.8">se</w> <w n="16.9">redresse</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Au</w> <w n="17.2">ciel</w> <w n="17.3">levant</w> <w n="17.4">les</w> <w n="17.5">mains</w> <w n="17.6">en</w> <w n="17.7">signe</w> <w n="17.8">de</w> <w n="17.9">détresse</w> ;</l>
						<l n="18" num="3.6"><w n="18.1">Plus</w> <w n="18.2">loin</w> <w n="18.3">meurt</w> <w n="18.4">un</w> <w n="18.5">pauvre</w> <w n="18.6">coursier</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="19" num="1.1"><w n="19.1">Empereur</w>, <w n="19.2">que</w> <w n="19.3">fais</w>-<w n="19.4">tu</w>, <w n="19.5">pendant</w> <w n="19.6">que</w> <w n="19.7">ton</w> <w n="19.8">armée</w>,</l>
						<l n="20" num="1.2"><w n="20.1">Comme</w> <w n="20.2">l</w>’<w n="20.3">or</w> <w n="20.4">d</w>’<w n="20.5">un</w> <w n="20.6">prodigue</w>, <w n="20.7">à</w> <w n="20.8">terre</w>, <w n="20.9">est</w> <w n="20.10">là</w> <w n="20.11">semée</w>,</l>
						<l n="21" num="1.3"><w n="21.1">A</w> <w n="21.2">Sedan</w>, <w n="21.3">au</w> <w n="21.4">sein</w> <w n="21.5">des</w> <w n="21.6">sillons</w> ?</l>
						<l n="22" num="1.4"><w n="22.1">Songes</w>-<w n="22.2">tu</w> <w n="22.3">que</w> <w n="22.4">la</w> <w n="22.5">France</w>, <w n="22.6">en</w> <w n="22.7">son</w> <w n="22.8">courroux</w> <w n="22.9">bien</w> <w n="22.10">juste</w>,</l>
						<l n="23" num="1.5"><w n="23.1">Demain</w> <w n="23.2">va</w> <w n="23.3">s</w>’<w n="23.4">écrier</w>, <w n="23.5">comme</w> <w n="23.6">autrefois</w> <w n="23.7">Auguste</w> :</l>
						<l n="24" num="1.6">« — <w n="24.1">Varus</w>, <w n="24.2">rends</w>-<w n="24.3">moi</w> <w n="24.4">mes</w> <w n="24.5">légions</w> !</l>
					</lg>
					<lg n="2">
						<l n="25" num="2.1"><w n="25.1">Rends</w>-<w n="25.2">moi</w> <w n="25.3">mon</w> <w n="25.4">maréchal</w> <w n="25.5">sans</w> <w n="25.6">reproche</w> <w n="25.7">et</w> <w n="25.8">sans</w> <w n="25.9">crainte</w>,</l>
						<l n="26" num="2.2"><w n="26.1">Qui</w> <w n="26.2">maudissait</w> <w n="26.3">son</w> <w n="26.4">sort</w>, <w n="26.5">et</w> <w n="26.6">subissait</w> <w n="26.7">sans</w> <w n="26.8">plainte</w></l>
						<l n="27" num="2.3"><w n="27.1">Tes</w> <w n="27.2">plans</w> <w n="27.3">absurdes</w> <w n="27.4">de</w> <w n="27.5">combat</w> !</l>
						<l n="28" num="2.4"><w n="28.1">Rends</w>-<w n="28.2">moi</w> <w n="28.3">mes</w> <w n="28.4">vieux</w> <w n="28.5">dragons</w>, <w n="28.6">faces</w> <w n="28.7">échevelées</w>,</l>
						<l n="29" num="2.5"><w n="29.1">Mes</w> <w n="29.2">hussards</w> <w n="29.3">qui</w> <w n="29.4">passaient</w> <w n="29.5">au</w> <w n="29.6">milieu</w> <w n="29.7">des</w> <w n="29.8">mêlées</w></l>
						<l n="30" num="2.6"><w n="30.1">Comme</w> <w n="30.2">la</w> <w n="30.3">foudre</w> <w n="30.4">qui</w> <w n="30.5">s</w>’<w n="30.6">abat</w> !</l>
					</lg>
					<lg n="3">
						<l n="31" num="3.1"><w n="31.1">Varus</w>, <w n="31.2">Varus</w>, <w n="31.3">rends</w>-<w n="31.4">moi</w> <w n="31.5">mes</w> <w n="31.6">régiments</w> <w n="31.7">d</w>’<w n="31.8">Afrique</w>,</l>
						<l n="32" num="3.2"><w n="32.1">Mes</w> <w n="32.2">zouaves</w>, <w n="32.3">mes</w> <w n="32.4">turcos</w>, <w n="32.5">dont</w> <w n="32.6">l</w>’<w n="32.7">élan</w> <w n="32.8">frénétique</w></l>
						<l n="33" num="3.3"><w n="33.1">Enlevait</w> <w n="33.2">si</w> <w n="33.3">bien</w> <w n="33.4">les</w> <w n="33.5">canons</w> ;</l>
						<l n="34" num="3.4"><w n="34.1">Mes</w> <w n="34.2">héros</w> <w n="34.3">échappées</w> <w n="34.4">aux</w> <w n="34.5">neiges</w> <w n="34.6">de</w> <w n="34.7">Crimée</w>,</l>
						<l n="35" num="3.5"><w n="35.1">Au</w> <w n="35.2">soleil</w> <w n="35.3">du</w> <w n="35.4">Mexique</w> ! <w n="35.5">oh</w> ! <w n="35.6">rends</w>-<w n="35.7">moi</w> <w n="35.8">mon</w> <w n="35.9">armée</w> !</l>
						<l n="36" num="3.6"><w n="36.1">Varus</w>, <w n="36.2">rends</w>-<w n="36.3">moi</w> <w n="36.4">mes</w> <w n="36.5">légions</w> ! »</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="37" num="1.1"><w n="37.1">Mais</w> <w n="37.2">le</w> <w n="37.3">voilà</w> !… <w n="37.4">c</w>’<w n="37.5">est</w> <w n="37.6">lui</w> !… <w n="37.7">bercé</w> <w n="37.8">dans</w> <w n="37.9">sa</w> <w n="37.10">berline</w>,</l>
						<l n="38" num="1.2"><w n="38.1">Il</w> <w n="38.2">s</w>’<w n="38.3">étend</w> <w n="38.4">mollement</w> ; <w n="38.5">et</w> <w n="38.6">sa</w> <w n="38.7">tête</w> <w n="38.8">s</w>’<w n="38.9">incline</w>,</l>
						<l n="39" num="1.3"><w n="39.1">L</w>’<w n="39.2">œil</w> <w n="39.3">terne</w> <w n="39.4">et</w> <w n="39.5">le</w> <w n="39.6">cigare</w> <w n="39.7">aux</w> <w n="39.8">dents</w>.</l>
						<l n="40" num="1.4"><w n="40.1">Les</w> <w n="40.2">cent</w>-<w n="40.3">gardes</w> <w n="40.4">géants</w>, <w n="40.5">ces</w> <w n="40.6">soldats</w> <w n="40.7">de</w> <w n="40.8">parade</w>,</l>
						<l n="41" num="1.5"><w n="41.1">Accompagnent</w> <w n="41.2">encore</w> <w n="41.3">sa</w> <w n="41.4">triste</w> <w n="41.5">promenade</w>,</l>
						<l n="42" num="1.6"><w n="42.1">Comme</w> <w n="42.2">en</w> <w n="42.3">ses</w> <w n="42.4">jours</w> <w n="42.5">les</w> <w n="42.6">plus</w> <w n="42.7">brillants</w>.</l>
					</lg>
					<lg n="2">
						<l n="43" num="2.1"><w n="43.1">Voyez</w>-<w n="43.2">vous</w> <w n="43.3">les</w> <w n="43.4">chevaux</w> <w n="43.5">courir</w> <w n="43.6">comme</w> <w n="43.7">la</w> <w n="43.8">foudre</w> ?</l>
						<l n="44" num="2.2"><w n="44.1">Qu</w>’<w n="44.2">importent</w> <w n="44.3">les</w> <w n="44.4">mourants</w> <w n="44.5">qui</w> <w n="44.6">gisent</w> <w n="44.7">dans</w> <w n="44.8">la</w> <w n="44.9">poudre</w></l>
						<l n="45" num="2.3"><w n="45.1">Et</w> <w n="45.2">les</w> <w n="45.3">cadavres</w> <w n="45.4">en</w> <w n="45.5">monceau</w> !</l>
						<l n="46" num="2.4"><w n="46.1">Les</w> <w n="46.2">morts</w> <w n="46.3">ne</w> <w n="46.4">sauront</w> <w n="46.5">point</w>, <w n="46.6">au</w> <w n="46.7">sein</w> <w n="46.8">de</w> <w n="46.9">la</w> <w n="46.10">poussière</w>,</l>
						<l n="47" num="2.5"><w n="47.1">Rouvrir</w>, <w n="47.2">sous</w> <w n="47.3">les</w> <w n="47.4">chevaux</w> <w n="47.5">qui</w> <w n="47.6">les</w> <w n="47.7">broient</w>, <w n="47.8">leur</w> <w n="47.9">paupière</w> ;</l>
						<l n="48" num="2.6"><w n="48.1">Et</w> <w n="48.2">les</w> <w n="48.3">blessés</w> <w n="48.4">mourront</w> <w n="48.5">bientôt</w> !</l>
					</lg>
					<lg n="3">
						<l n="49" num="3.1"><w n="49.1">Ils</w> <w n="49.2">vont</w> !… <w n="49.3">Mais</w> <w n="49.4">les</w> <w n="49.5">chevaux</w> <w n="49.6">sont</w> <w n="49.7">glacées</w> <w n="49.8">d</w>’<w n="49.9">épouvante</w> :</l>
						<l n="50" num="3.2"><w n="50.1">Ils</w> <w n="50.2">ont</w> <w n="50.3">vu</w> <w n="50.4">d</w>’<w n="50.5">un</w> <w n="50.6">coursier</w> <w n="50.7">la</w> <w n="50.8">blessure</w> <w n="50.9">béante</w>,</l>
						<l n="51" num="3.3"><w n="51.1">D</w>’<w n="51.2">où</w> <w n="51.3">le</w> <w n="51.4">sang</w> <w n="51.5">coulait</w> <w n="51.6">à</w> <w n="51.7">bouillons</w>.</l>
						<l n="52" num="3.4"><w n="52.1">Qu</w>’<w n="52.2">importe</w> !… <w n="52.3">il</w> <w n="52.4">faut</w> <w n="52.5">passer</w>… — <w n="52.6">Ce</w> <w n="52.7">blessé</w> <w n="52.8">se</w> <w n="52.9">relève</w></l>
						<l n="53" num="3.5"><w n="53.1">Et</w> <w n="53.2">son</w> <w n="53.3">poing</w> <w n="53.4">menaçant</w> <w n="53.5">vers</w> <w n="53.6">l</w>’<w n="53.7">empereur</w> <w n="53.8">s</w>’<w n="53.9">élève</w>… ,</l>
						<l n="54" num="3.6"><w n="54.1">Qu</w>’<w n="54.2">importe</w> !… <w n="54.3">Fouettez</w>, <w n="54.4">postillons</w> !…</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">IV</head>
					<lg n="1">
						<l n="55" num="1.1"><w n="55.1">Quoi</w> ! <w n="55.2">c</w>’<w n="55.3">est</w> <w n="55.4">un</w> <w n="55.5">empereur</w>, <w n="55.6">en</w> <w n="55.7">empereur</w> <w n="55.8">de</w> <w n="55.9">France</w> !</l>
						<l n="56" num="1.2"><w n="56.1">Vit</w>-<w n="56.2">on</w> <w n="56.3">tant</w> <w n="56.4">d</w>’<w n="56.5">impudeur</w> <w n="56.6">et</w> <w n="56.7">tant</w> <w n="56.8">d</w>’’<w n="56.9">indifférence</w></l>
						<l n="57" num="1.3"><w n="57.1">Chez</w> <w n="57.2">nos</w> <w n="57.3">plus</w> <w n="57.4">mauvais</w> <w n="57.5">souverains</w> ?</l>
						<l n="58" num="1.4"><w n="58.1">De</w> <w n="58.2">cent</w> <w n="58.3">mille</w> <w n="58.4">soldats</w> <w n="58.5">les</w> <w n="58.6">uns</w> <w n="58.7">sont</w> <w n="58.8">mort</w> <w n="58.9">en</w> <w n="58.10">braves</w> ;</l>
						<l n="59" num="1.5"><w n="59.1">Les</w> <w n="59.2">autres</w> <w n="59.3">sont</w> <w n="59.4">vendus</w>, <w n="59.5">comme</w> <w n="59.6">on</w> <w n="59.7">vend</w> <w n="59.8">des</w> <w n="59.9">esclaves</w>,</l>
						<l n="60" num="1.6"><w n="60.1">O</w> <w n="60.2">Bonaparte</w>, <w n="60.3">par</w> <w n="60.4">tes</w> <w n="60.5">mains</w>.</l>
					</lg>
					<lg n="2">
						<l n="61" num="2.1"><w n="61.1">Bonaparte</w>, <w n="61.2">oh</w> ! <w n="61.3">dis</w>-<w n="61.4">moi</w>… , <w n="61.5">ce</w> <w n="61.6">n</w>’<w n="61.7">était</w> <w n="61.8">point</w> <w n="61.9">la</w> <w n="61.10">peine</w></l>
						<l n="62" num="2.2"><w n="62.1">De</w> <w n="62.2">te</w> <w n="62.3">servir</w> <w n="62.4">du</w> <w n="62.5">nom</w> <w n="62.6">d</w>’<w n="62.7">un</w> <w n="62.8">si</w> <w n="62.9">grand</w> <w n="62.10">capitaine</w></l>
						<l n="63" num="2.3"><w n="63.1">Pour</w> <w n="63.2">nous</w> <w n="63.3">tromper</w> <w n="63.4">sur</w> <w n="63.5">ton</w> <w n="63.6">néant</w> ;</l>
						<l n="64" num="2.4"><w n="64.1">D</w>’<w n="64.2">associer</w> <w n="64.3">ta</w> <w n="64.4">honte</w> <w n="64.5">à</w> <w n="64.6">son</w> <w n="64.7">nom</w> <w n="64.8">dans</w> <w n="64.9">l</w>’<w n="64.10">histoire</w>,</l>
						<l n="65" num="2.5"><w n="65.1">Et</w> <w n="65.2">de</w> <w n="65.3">te</w> <w n="65.4">cramponner</w>, <w n="65.5">parasite</w> <w n="65.6">à</w> <w n="65.7">sa</w> <w n="65.8">gloire</w>,</l>
						<l n="66" num="2.6"><w n="66.1">Comme</w> <w n="66.2">un</w> <w n="66.3">lierre</w> <w n="66.4">au</w> <w n="66.5">chêne</w> <w n="66.6">géant</w>.</l>
					</lg>
					<lg n="3">
						<l n="67" num="3.1"><w n="67.1">Ah</w> ! <w n="67.2">que</w> <w n="67.3">n</w>’<w n="67.4">est</w>-<w n="67.5">tu</w> <w n="67.6">resté</w> <w n="67.7">dans</w> <w n="67.8">la</w> <w n="67.9">libre</w> <w n="67.10">Angleterre</w>,</l>
						<l n="68" num="3.2"><w n="68.1">Au</w> <w n="68.2">lieu</w> <w n="68.3">de</w> <w n="68.4">profiter</w> <w n="68.5">de</w> <w n="68.6">nos</w> <w n="68.7">jours</w> <w n="68.8">de</w> <w n="68.9">colère</w>,</l>
						<l n="69" num="3.3"><w n="69.1">Pour</w> <w n="69.2">couronner</w> <w n="69.3">aussi</w> <w n="69.4">ton</w> <w n="69.5">front</w> ;</l>
						<l n="70" num="3.4"><w n="70.1">Pour</w> <w n="70.2">tailler</w> <w n="70.3">sans</w> <w n="70.4">trembler</w> <w n="70.5">ta</w> <w n="70.6">pourpre</w> <w n="70.7">impériale</w></l>
						<l n="71" num="3.5"><w n="71.1">Dans</w> <w n="71.2">le</w> <w n="71.3">large</w> <w n="71.4">manteau</w> <w n="71.5">dont</w> <w n="71.6">sa</w> <w n="71.7">main</w> <w n="71.8">colossale</w></l>
						<l n="72" num="3.6"><w n="72.1">Couvrait</w> <w n="72.2">le</w> <w n="72.3">monde</w> <w n="72.4">sans</w> <w n="72.5">façon</w> ?</l>
					</lg>
					<lg n="4">
						<l n="73" num="4.1"><w n="73.1">Ah</w> ! <w n="73.2">que</w> <w n="73.3">n</w>’<w n="73.4">as</w>-<w n="73.5">tu</w> <w n="73.6">laissé</w> <w n="73.7">dans</w> <w n="73.8">notre</w> <w n="73.9">panoplie</w></l>
						<l n="74" num="4.2"><w n="74.1">Sa</w> <w n="74.2">gigantesque</w> <w n="74.3">épée</w> <w n="74.4">aux</w> <w n="74.5">combats</w> <w n="74.6">anoblie</w>,</l>
						<l n="75" num="4.3"><w n="75.1">Au</w> <w n="75.2">lieu</w> <w n="75.3">de</w> <w n="75.4">t</w>’<w n="75.5">en</w> <w n="75.6">ceindre</w> <w n="75.7">les</w> <w n="75.8">reins</w> !</l>
						<l n="76" num="4.4"><w n="76.1">Tu</w> <w n="76.2">devais</w> <w n="76.3">bien</w> <w n="76.4">savoir</w> <w n="76.5">qu</w>’<w n="76.6">au</w> <w n="76.7">jour</w> <w n="76.8">de</w> <w n="76.9">la</w> <w n="76.10">bataille</w>,</l>
						<l n="77" num="4.5"><w n="77.1">Trop</w> <w n="77.2">longue</w> <w n="77.3">elle</w> <w n="77.4">serait</w> <w n="77.5">pour</w> <w n="77.6">ta</w> <w n="77.7">petite</w> <w n="77.8">taille</w></l>
						<l n="78" num="4.6"><w n="78.1">Et</w> <w n="78.2">qu</w>’<w n="78.3">elle</w> <w n="78.4">te</w> <w n="78.5">choirait</w> <w n="78.6">des</w> <w n="78.7">mains</w>.</l>
					</lg>
					<lg n="5">
						<l n="79" num="5.1"><w n="79.1">Tous</w> <w n="79.2">les</w> <w n="79.3">Grecs</w> <w n="79.4">ne</w> <w n="79.5">pouvaient</w> <w n="79.6">bander</w> <w n="79.7">l</w>’<w n="79.8">arme</w> <w n="79.9">d</w>’<w n="79.10">Ulysse</w> ;</l>
						<l n="80" num="5.2"><w n="80.1">C</w>’<w n="80.2">était</w>, <w n="80.3">Napoléon</w>, <w n="80.4">un</w> <w n="80.5">exemple</w> <w n="80.6">propice</w></l>
						<l n="81" num="5.3"><w n="81.1">Pour</w> <w n="81.2">calmer</w> <w n="81.3">ton</w> <w n="81.4">ambition</w>.</l>
						<l n="82" num="5.4"><w n="82.1">Le</w> <w n="82.2">casque</w> <w n="82.3">du</w> <w n="82.4">guerrier</w> <w n="82.5">est</w> <w n="82.6">trop</w> <w n="82.7">lourd</w> <w n="82.8">pour</w> <w n="82.9">l</w>’<w n="82.10">enfance</w> ;</l>
						<l n="83" num="5.5"><w n="83.1">Elle</w> <w n="83.2">peut</w> <w n="83.3">se</w> <w n="83.4">blesser</w> <w n="83.5">en</w> <w n="83.6">soulevant</w> <w n="83.7">sa</w> <w n="83.8">lance</w> :</l>
						<l n="84" num="5.6"><w n="84.1">Hercule</w> <w n="84.2">est</w> <w n="84.3">une</w> <w n="84.4">exception</w>.</l>
					</lg>
					<lg n="6">
						<l n="85" num="6.1"><w n="85.1">Le</w> <w n="85.2">bouffon</w> <w n="85.3">qui</w> <w n="85.4">voudra</w> <w n="85.5">jouer</w> <w n="85.6">la</w> <w n="85.7">tragédie</w>,</l>
						<l n="86" num="6.2"><w n="86.1">En</w> <w n="86.2">fera</w>, <w n="86.3">je</w> <w n="86.4">suis</w> <w n="86.5">sûr</w>, <w n="86.6">toujours</w> <w n="86.7">la</w> <w n="86.8">parodie</w></l>
						<l n="87" num="6.3"><w n="87.1">Avec</w> <w n="87.2">un</w> <w n="87.3">debout</w> <w n="87.4">orageux</w>.</l>
						<l n="88" num="6.4"><w n="88.1">Puisqu</w>’<w n="88.2">il</w> <w n="88.3">chaussait</w> <w n="88.4">hier</w> <w n="88.5">le</w> <w n="88.6">brodequin</w> <w n="88.7">comique</w>,</l>
						<l n="89" num="6.5"><w n="89.1">Il</w> <w n="89.2">doit</w> <w n="89.3">trouver</w> <w n="89.4">trop</w> <w n="89.5">grand</w> <w n="89.6">le</w> <w n="89.7">cotture</w> <w n="89.8">tragique</w> :</l>
						<l n="90" num="6.6"><w n="90.1">Sur</w> <w n="90.2">la</w> <w n="90.3">scène</w> <w n="90.4">il</w> <w n="90.5">sera</w> <w n="90.6">boiteux</w>.</l>
					</lg>
					<lg n="7">
						<l n="91" num="7.1"><w n="91.1">Bonaparte</w>, <w n="91.2">oh</w> ! <w n="91.3">dis</w>-<w n="91.4">moi</w>… , <w n="91.5">malgré</w> <w n="91.6">ton</w> <w n="91.7">impudence</w>,</l>
						<l n="92" num="7.2"><w n="92.1">Ne</w> <w n="92.2">crains</w>-<w n="92.3">tu</w> <w n="92.4">point</w> <w n="92.5">qu</w>’<w n="92.6">un</w> <w n="92.7">jour</w> <w n="92.8">l</w>’<w n="92.9">Hercule</w> <w n="92.10">de</w> <w n="92.11">la</w> <w n="92.12">France</w></l>
						<l n="93" num="7.3"><w n="93.1">Ne</w> <w n="93.2">vienne</w>, <w n="93.3">un</w> <w n="93.4">éclair</w> <w n="93.5">dans</w> <w n="93.6">les</w> <w n="93.7">yeux</w>,</l>
						<l n="94" num="7.4"><w n="94.1">Te</w> <w n="94.2">reprocher</w> <w n="94.3">d</w>’<w n="94.4">avoir</w> <w n="94.5">volé</w> <w n="94.6">son</w> <w n="94.7">héritage</w> ;</l>
						<l n="95" num="7.5"><w n="95.1">D</w>’<w n="95.2">avoir</w> <w n="95.3">traîné</w> <w n="95.4">son</w> <w n="95.5">glaive</w> <w n="95.6">en</w> <w n="95.7">ce</w> <w n="95.8">lieu</w> <w n="95.9">de</w> <w n="95.10">carnage</w>,</l>
						<l n="96" num="7.6"><w n="96.1">Et</w> <w n="96.2">terni</w> <w n="96.3">son</w> <w n="96.4">nom</w> <w n="96.5">glorieux</w> ?</l>
					</lg>
					<lg n="8">
						<l n="97" num="8.1"><w n="97.1">Ne</w> <w n="97.2">crains</w>-<w n="97.3">tu</w> <w n="97.4">point</w> <w n="97.5">de</w> <w n="97.6">dire</w>, <w n="97.7">un</w> <w n="97.8">jour</w>, <w n="97.9">baissant</w> <w n="97.10">la</w> <w n="97.11">tête</w>,</l>
						<l n="98" num="8.2"><w n="98.1">Comme</w> <w n="98.2">un</w> <w n="98.3">frêle</w> <w n="98.4">roseau</w> <w n="98.5">brisé</w> <w n="98.6">par</w> <w n="98.7">la</w> <w n="98.8">tempête</w> :</l>
						<l n="99" num="8.3">« <w n="99.1">Sire</w>, <w n="99.2">daignez</w> <w n="99.3">me</w> <w n="99.4">pardonner</w> :</l>
						<l n="100" num="8.4">« <w n="100.1">Oui</w>, <w n="100.2">mon</w> <w n="100.3">ambition</w> <w n="100.4">me</w> <w n="100.5">rendit</w> <w n="100.6">bien</w> <w n="100.7">coupable</w> ;</l>
						<l n="101" num="8.5">« <w n="101.1">Oh</w> ! <w n="101.2">cesser</w> <w n="101.3">de</w> <w n="101.4">froncer</w> <w n="101.5">ce</w> <w n="101.6">sourcil</w> <w n="101.7">redoutable</w> !</l>
						<l n="102" num="8.6">« <w n="102.1">Ne</w> <w n="102.2">veuillez</w> <w n="102.3">point</w> <w n="102.4">me</w> <w n="102.5">condamner</w> !</l>
					</lg>
					<lg n="9">
						<l n="103" num="9.1">« <w n="103.1">Ce</w> <w n="103.2">n</w>’<w n="103.3">était</w> <w n="103.4">point</w> <w n="103.5">pour</w> <w n="103.6">moi</w> <w n="103.7">qu</w>’<w n="103.8">autrefois</w> <w n="103.9">votre</w> <w n="103.10">épée</w></l>
						<l n="104" num="9.2">« <w n="104.1">En</w> <w n="104.2">traits</w> <w n="104.3">rouges</w> <w n="104.4">gravait</w> <w n="104.5">l</w>’<w n="104.6">incroyable</w> <w n="104.7">épopée</w></l>
						<l n="105" num="9.3">« <w n="105.1">De</w> <w n="105.2">tant</w> <w n="105.3">de</w> <w n="105.4">gloire</w> <w n="105.5">et</w> <w n="105.6">de</w> <w n="105.7">revers</w> ;</l>
						<l n="106" num="9.4">« <w n="106.1">Qu</w>’<w n="106.2">en</w> <w n="106.3">Égypte</w>, <w n="106.4">passant</w> <w n="106.5">comme</w> <w n="106.6">les</w> <w n="106.7">vents</w> <w n="106.8">humides</w></l>
						<l n="107" num="9.5">« <w n="107.1">Vous</w> <w n="107.2">évoquiez</w> <w n="107.3">du</w> <w n="107.4">sein</w> <w n="107.5">des</w> <w n="107.6">vieilles</w> <w n="107.7">pyramides</w></l>
						<l n="108" num="9.6">« <w n="108.1">Les</w> <w n="108.2">siècles</w> <w n="108.3">du</w> <w n="108.4">vieil</w> <w n="108.5">univers</w>.</l>
					</lg>
					<lg n="10">
						<l n="109" num="10.1">« <w n="109.1">Ce</w> <w n="109.2">n</w>’<w n="109.3">était</w> <w n="109.4">point</w> <w n="109.5">pour</w> <w n="109.6">moi</w> <w n="109.7">que</w> <w n="109.8">votre</w> <w n="109.9">humeur</w> <w n="109.10">guerrière</w></l>
						<l n="110" num="10.2">« <w n="110.1">De</w> <w n="110.2">Naples</w> <w n="110.3">au</w> <w n="110.4">Kremlin</w> <w n="110.5">promenait</w> <w n="110.6">l</w>’<w n="110.7">aigle</w> <w n="110.8">fière</w></l>
						<l n="111" num="10.3">« <w n="111.1">Avec</w> <w n="111.2">Lannes</w> <w n="111.3">et</w> <w n="111.4">Masséna</w> ;</l>
						<l n="112" num="10.4">« <w n="112.1">Ce</w> <w n="112.2">n</w>’<w n="112.3">était</w> <w n="112.4">point</w> <w n="112.5">pour</w> <w n="112.6">voir</w> <w n="112.7">cette</w> <w n="112.8">affreuse</w> <w n="112.9">mêlée</w></l>
						<l n="113" num="10.5">« <w n="113.1">Qui</w> <w n="113.2">souille</w> <w n="113.3">de</w> <w n="113.4">Sedan</w> <w n="113.5">la</w> <w n="113.6">fatale</w> <w n="113.7">vallée</w>,</l>
						<l n="114" num="10.6">« <w n="114.1">Que</w> <w n="114.2">vous</w> <w n="114.3">avez</w> <w n="114.4">fait</w> <w n="114.5">Iéna</w>. »</l>
					</lg>
					<lg n="11">
						<l n="115" num="11.1"><w n="115.1">Ton</w> <w n="115.2">expiation</w>, <w n="115.3">prince</w>, <w n="115.4">sera</w> <w n="115.5">complète</w> :</l>
						<l n="116" num="11.2"><w n="116.1">Plus</w> <w n="116.2">bas</w>, <w n="116.3">plus</w> <w n="116.4">bas</w> <w n="116.5">encor</w> <w n="116.6">tu</w> <w n="116.7">baisseras</w> <w n="116.8">la</w> <w n="116.9">tête</w></l>
						<l n="117" num="11.3"><w n="117.1">Devant</w> <w n="117.2">l</w>’<w n="117.3">Empereur</w>, <w n="117.4">à</w> <w n="117.5">genoux</w>.</l>
						<l n="118" num="11.4"><w n="118.1">En</w> <w n="118.2">vain</w> <w n="118.3">tu</w> <w n="118.4">tenteras</w> <w n="118.5">de</w> <w n="118.6">fléchir</w> <w n="118.7">sa</w> <w n="118.8">colère</w>.</l>
						<l n="119" num="11.5"><w n="119.1">Mais</w> <w n="119.2">lui</w>, <w n="119.3">te</w> <w n="119.4">repoussant</w> <w n="119.5">du</w> <w n="119.6">pied</w> <w n="119.7">dans</w> <w n="119.8">la</w> <w n="119.9">poussière</w></l>
						<l n="120" num="11.6"><w n="120.1">T</w>’<w n="120.2">Accablera</w> <w n="120.3">de</w> <w n="120.4">son</w> <w n="120.5">courroux</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>