<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">NOS RUINES</title>
				<title type="medium">Édition électronique</title>
				<author key="ANG">
					<name>
						<forename>Albert</forename>
						<surname>ANGOT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">ANG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>NOS RUINES</title>
						<author>ALBERT ANGOT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30021186v</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>NOS RUINES</title>
								<author>ALBERT ANGOT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DUNIOL ET CIE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANG8">
				<head type="main">VERSAILLES</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">histoire</w> <w n="1.3">a</w> <w n="1.4">bien</w> <w n="1.5">des</w> <w n="1.6">railleries</w></l>
						<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">nos</w> <w n="2.3">projets</w>, <w n="2.4">nos</w> <w n="2.5">passions</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Nos</w> <w n="3.2">politiques</w> <w n="3.3">rêveries</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Ce</w> <w n="4.2">sont</w> <w n="4.3">comme</w> <w n="4.4">autant</w> <w n="4.5">de</w> <w n="4.6">leçons</w>.</l>
						<l n="5" num="1.5"><w n="5.1">On</w> <w n="5.2">sent</w> <w n="5.3">alors</w> <w n="5.4">mieux</w> <w n="5.5">la</w> <w n="5.6">faiblesse</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">l</w>’<w n="6.3">orgueil</w>, <w n="6.4">et</w> <w n="6.5">la</w> <w n="6.6">folle</w> <w n="6.7">ivresse</w></l>
						<l n="7" num="1.7"><w n="7.1">De</w> <w n="7.2">notre</w> <w n="7.3">pauvre</w> <w n="7.4">humanité</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">De</w> <w n="8.2">nos</w> <w n="8.3">vœux</w>, <w n="8.4">autant</w> <w n="8.5">de</w> <w n="8.6">chimères</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">de</w> <w n="9.3">nos</w> <w n="9.4">œuvres</w> <w n="9.5">éphémères</w></l>
						<l n="10" num="1.10"><w n="10.1">On</w> <w n="10.2">sent</w> <w n="10.3">la</w> <w n="10.4">stérilité</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">La</w> <w n="11.2">gloire</w> <w n="11.3">fut</w> <w n="11.4">à</w> <w n="11.5">tire</w>-<w n="11.6">d</w>’<w n="11.7">aile</w> :</l>
						<l n="12" num="2.2"><w n="12.1">Trônes</w>, <w n="12.2">glaives</w> <w n="12.3">des</w> <w n="12.4">généraux</w>,</l>
						<l n="13" num="2.3"><w n="13.1">Mîtres</w> <w n="13.2">d</w>’<w n="13.3">or</w> <w n="13.4">roulent</w> <w n="13.5">pêle</w>-<w n="13.6">mêle</w></l>
						<l n="14" num="2.4"><w n="14.1">Avec</w> <w n="14.2">la</w> <w n="14.3">hache</w> <w n="14.4">des</w> <w n="14.5">bourreaux</w>.</l>
						<l n="15" num="2.5"><w n="15.1">Dans</w> <w n="15.2">la</w> <w n="15.3">course</w> <w n="15.4">vertigineuse</w>,</l>
						<l n="16" num="2.6"><w n="16.1">Que</w> <w n="16.2">poursuit</w> <w n="16.3">sa</w> <w n="16.4">vague</w> <w n="16.5">par</w> <w n="16.6">le</w> <w n="16.7">Temps</w>.</l>
						<l n="17" num="2.7"><w n="17.1">Rien</w> <w n="17.2">ne</w> <w n="17.3">flotte</w>, <w n="17.4">rien</w> <w n="17.5">ne</w> <w n="17.6">surnage</w>,</l>
						<l n="18" num="2.8"><w n="18.1">Pas</w> <w n="18.2">même</w> <w n="18.3">l</w>’<w n="18.4">herbe</w> <w n="18.5">du</w> <w n="18.6">rivage</w>.</l>
						<l n="19" num="2.9"><w n="19.1">Le</w> <w n="19.2">temps</w>, <w n="19.3">c</w>’<w n="19.4">est</w> <w n="19.5">le</w> <w n="19.6">roi</w> <w n="19.7">des</w> <w n="19.8">torrents</w>.</l>
					</lg>
					<lg n="3">
						<l n="20" num="3.1"><w n="20.1">Pourquoi</w>, <w n="20.2">dis</w>, <w n="20.3">dans</w> <w n="20.4">ton</w> <w n="20.5">impuissance</w>,</l>
						<l n="21" num="3.2"><w n="21.1">Veux</w>-<w n="21.2">tu</w> <w n="21.3">combattre</w>, <w n="21.4">homme</w> <w n="21.5">insensé</w>,</l>
						<l n="22" num="3.3"><w n="22.1">Cette</w> <w n="22.2">force</w>, <w n="22.3">cette</w> <w n="22.4">puissance</w></l>
						<l n="23" num="3.4"><w n="23.1">Qui</w> <w n="23.2">dans</w> <w n="23.3">un</w> <w n="23.4">jour</w> <w n="23.5">t</w>’<w n="23.6">aura</w> <w n="23.7">brisé</w> ?</l>
						<l n="24" num="3.5"><w n="24.1">N</w>’<w n="24.2">élève</w> <w n="24.3">plus</w> <w n="24.4">dans</w> <w n="24.5">ton</w> <w n="24.6">délire</w>,</l>
						<l n="25" num="3.6"><w n="25.1">Des</w> <w n="25.2">colosses</w> <w n="25.3">au</w> <w n="25.4">front</w> <w n="25.5">d</w>’<w n="25.6">airain</w>.</l>
						<l n="26" num="3.7"><w n="26.1">Ne</w> <w n="26.2">taille</w> <w n="26.3">plus</w> <w n="26.4">dans</w> <w n="26.5">le</w> <w n="26.6">carrare</w></l>
						<l n="27" num="3.8"><w n="27.1">Tes</w> <w n="27.2">traits</w> <w n="27.3">que</w> <w n="27.4">le</w> <w n="27.5">cercueil</w> <w n="27.6">avare</w></l>
						<l n="28" num="3.9"><w n="28.1">Viendra</w> <w n="28.2">te</w> <w n="28.3">disputer</w> <w n="28.4">demain</w>.</l>
					</lg>
					<lg n="4">
						<l n="29" num="4.1"><w n="29.1">Ton</w> <w n="29.2">colosse</w> <w n="29.3">a</w> <w n="29.4">des</w> <w n="29.5">pieds</w> <w n="29.6">d</w>’<w n="29.7">argile</w> :</l>
						<l n="30" num="4.2"><w n="30.1">Il</w> <w n="30.2">croule</w> <w n="30.3">miné</w> <w n="30.4">par</w> <w n="30.5">le</w> <w n="30.6">temps</w>,</l>
						<l n="31" num="4.3"><w n="31.1">Et</w> <w n="31.2">ta</w> <w n="31.3">statue</w>, <w n="31.4">œuvre</w> <w n="31.5">débile</w>,</l>
						<l n="32" num="4.4"><w n="32.1">N</w>’<w n="32.2">offre</w> <w n="32.3">plus</w> <w n="32.4">que</w> <w n="32.5">débris</w> <w n="32.6">gisants</w>.</l>
						<l n="33" num="4.5"><w n="33.1">Si</w>, <w n="33.2">par</w> <w n="33.3">hasard</w>, <w n="33.4">elle</w> <w n="33.5">subsiste</w>,</l>
						<l n="34" num="4.6"><w n="34.1">Elle</w> <w n="34.2">contemple</w> <w n="34.3">toute</w> <w n="34.4">triste</w>,</l>
						<l n="35" num="4.7"><w n="35.1">Ton</w> <w n="35.2">grand</w> <w n="35.3">œuvre</w> <w n="35.4">en</w> <w n="35.5">morceaux</w> <w n="35.6">brisé</w>,</l>
						<l n="36" num="4.8"><w n="36.1">Ce</w> <w n="36.2">grand</w> <w n="36.3">œuvre</w> <w n="36.4">que</w> <w n="36.5">ton</w> <w n="36.6">génie</w></l>
						<l n="37" num="4.9"><w n="37.1">Rêva</w> <w n="37.2">tout</w> <w n="37.3">rempli</w> <w n="37.4">d</w>’<w n="37.5">harmonie</w>,</l>
						<l n="38" num="4.10"><w n="38.1">Et</w> <w n="38.2">dur</w> <w n="38.3">comme</w> <w n="38.4">un</w> <w n="38.5">lingot</w> <w n="38.6">bronzé</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="39" num="1.1"><w n="39.1">Hier</w>, <w n="39.2">Louis</w>, <w n="39.3">toi</w> <w n="39.4">que</w> <w n="39.5">Turenne</w></l>
						<l n="40" num="1.2"><w n="40.1">Enrichit</w>, <w n="40.2">après</w> <w n="40.3">maints</w> <w n="40.4">combats</w>,</l>
						<l n="41" num="1.3"><w n="41.1">De</w> <w n="41.2">l</w>’<w n="41.3">Alsace</w> <w n="41.4">et</w> <w n="41.5">de</w> <w n="41.6">la</w> <w n="41.7">Lorraine</w> ;</l>
						<l n="42" num="1.4"><w n="42.1">Grand</w> <w n="42.2">Louis</w>, <w n="42.3">ne</w> <w n="42.4">voyais</w>-<w n="42.5">tu</w> <w n="42.6">pas</w>,</l>
						<l n="43" num="1.5"><w n="43.1">Vainqueur</w> <w n="43.2">dans</w> <w n="43.3">plus</w> <w n="43.4">de</w> <w n="43.5">dix</w> <w n="43.6">batailles</w>,</l>
						<l n="44" num="1.6"><w n="44.1">L</w>’<w n="44.2">Allemand</w> <w n="44.3">railler</w> <w n="44.4">dans</w> <w n="44.5">Versailles</w></l>
						<l n="45" num="1.7"><w n="45.1">Ta</w> <w n="45.2">gloire</w> <w n="45.3">et</w> <w n="45.4">tes</w> <w n="45.5">rayons</w> <w n="45.6">éteints</w> ?</l>
						<l n="46" num="1.8"><w n="46.1">Il</w> <w n="46.2">insultait</w> <w n="46.3">ta</w> <w n="46.4">belle</w> <w n="46.5">France</w></l>
						<l n="47" num="1.9"><w n="47.1">Qu</w>’<w n="47.2">un</w> <w n="47.3">usurpateur</w> <w n="47.4">sans</w> <w n="47.5">puissance</w></l>
						<l n="48" num="1.10"><w n="48.1">Avait</w> <w n="48.2">livré</w> <w n="48.3">entre</w> <w n="48.4">ses</w> <w n="48.5">mains</w>.</l>
					</lg>
					<lg n="2">
						<l n="49" num="2.1"><w n="49.1">Combats</w> <w n="49.2">qui</w> <w n="49.3">faisaient</w> <w n="49.4">notre</w> <w n="49.5">gloire</w>,</l>
						<l n="50" num="2.2"><w n="50.1">Turckeim</w>, <w n="50.2">Mergentheim</w> <w n="50.3">et</w> <w n="50.4">Salzbach</w>,</l>
						<l n="51" num="2.3"><w n="51.1">Sont</w> <w n="51.2">effacés</w> <w n="51.3">de</w> <w n="51.4">la</w> <w n="51.5">mémoire</w></l>
						<l n="52" num="2.4"><w n="52.1">Par</w> <w n="52.2">Wissembourg</w>, <w n="52.3">Woërth</w> <w n="52.4">et</w> <w n="52.5">Forbach</w>.</l>
						<l n="53" num="2.5"><w n="53.1">Honte</w> ! <w n="53.2">un</w> <w n="53.3">descendant</w> <w n="53.4">des</w> <w n="53.5">burgraves</w>,</l>
						<l n="54" num="2.6"><w n="54.1">Foulant</w> <w n="54.2">notre</w> <w n="54.3">terre</w> <w n="54.4">de</w> <w n="54.5">braves</w></l>
						<l n="55" num="2.7">(<w n="55.1">Ils</w> <w n="55.2">gémissaient</w> <w n="55.3">dans</w> <w n="55.4">le</w> <w n="55.5">cercueil</w>),</l>
						<l n="56" num="2.8"><w n="56.1">Au</w> <w n="56.2">son</w> <w n="56.3">des</w> <w n="56.4">fanfares</w> <w n="56.5">guerrières</w>,</l>
						<l n="57" num="2.9"><w n="57.1">Se</w> <w n="57.2">faisait</w>, <w n="57.3">narguant</w> <w n="57.4">nos</w> <w n="57.5">bannières</w>,</l>
						<l n="58" num="2.10"><w n="58.1">Sacrer</w> <w n="58.2">dans</w> <w n="58.3">ton</w> <w n="58.4">palais</w> <w n="58.5">en</w> <w n="58.6">deuil</w> !</l>
					</lg>
					<lg n="3">
						<l n="59" num="3.1"><w n="59.1">Né</w> <w n="59.2">du</w> <w n="59.3">suffrage</w> <w n="59.4">populaire</w>,</l>
						<l n="60" num="3.2"><w n="60.1">Aujourd</w>’<w n="60.2">hui</w> <w n="60.3">siège</w> <w n="60.4">un</w> <w n="60.5">Parlement</w>,</l>
						<l n="61" num="3.3"><w n="61.1">Image</w> <w n="61.2">de</w> <w n="61.3">la</w> <w n="61.4">France</w> <w n="61.5">entière</w> ;</l>
						<l n="62" num="3.4"><w n="62.1">Déjà</w> <w n="62.2">ta</w> <w n="62.3">demeure</w> <w n="62.4">l</w>’<w n="62.5">attend</w>.</l>
						<l n="63" num="3.5"><w n="63.1">La</w> <w n="63.2">même</w>, <w n="63.3">où</w> <w n="63.4">tu</w> <w n="63.5">parlais</w> <w n="63.6">en</w> <w n="63.7">maître</w>,</l>
						<l n="64" num="3.6"><w n="64.1">Dans</w> <w n="64.2">un</w> <w n="64.3">instant</w> <w n="64.4">il</w> <w n="64.5">va</w> <w n="64.6">paraître</w>.</l>
						<l n="65" num="3.7"><w n="65.1">Non</w> ! <w n="65.2">non</w> ! <w n="65.3">l</w>’<w n="65.4">État</w> <w n="65.5">ce</w> <w n="65.6">n</w>’<w n="65.7">est</w> <w n="65.8">plus</w> <w n="65.9">toi</w>.</l>
						<l n="66" num="3.8"><w n="66.1">C</w>’<w n="66.2">est</w> <w n="66.3">le</w> <w n="66.4">peuple</w> <w n="66.5">et</w> <w n="66.6">cette</w> <w n="66.7">assemblée</w> :</l>
						<l n="67" num="3.9"><w n="67.1">La</w> <w n="67.2">royauté</w> <w n="67.3">s</w>’<w n="67.4">est</w> <w n="67.5">écroulée</w>.</l>
						<l n="68" num="3.10"><w n="68.1">Non</w> ! <w n="68.2">l</w>’<w n="68.3">État</w>, <w n="68.4">ce</w> <w n="68.5">n</w>’<w n="68.6">est</w> <w n="68.7">plus</w> <w n="68.8">un</w> <w n="68.9">roi</w> !</l>
					</lg>
					<lg n="4">
						<l n="69" num="4.1"><w n="69.1">Louis</w> <w n="69.2">Seize</w>, <w n="69.3">en</w> <w n="69.4">place</w> <w n="69.5">de</w> <w n="69.6">Grève</w>,</l>
						<l n="70" num="4.2"><w n="70.1">Est</w> <w n="70.2">mort</w> <w n="70.3">frappé</w> <w n="70.4">sur</w> <w n="70.5">l</w>’<w n="70.6">échafaud</w> ;</l>
						<l n="71" num="4.3"><w n="71.1">Et</w> <w n="71.2">sur</w> <w n="71.3">une</w> <w n="71.4">lointaine</w> <w n="71.5">grève</w>,</l>
						<l n="72" num="4.4"><w n="72.1">Qui</w> <w n="72.2">lui</w> <w n="72.3">servira</w> <w n="72.4">de</w> <w n="72.5">tombeau</w>,</l>
						<l n="73" num="4.5"><w n="73.1">Proscrit</w>, <w n="73.2">ton</w> <w n="73.3">héritier</w> <w n="73.4">unique</w>,</l>
						<l n="74" num="4.6"><w n="74.1">Lassé</w> <w n="74.2">par</w> <w n="74.3">un</w> <w n="74.4">destin</w> <w n="74.5">inique</w>,</l>
						<l n="75" num="4.7"><w n="75.1">Mange</w> <w n="75.2">un</w> <w n="75.3">pain</w> <w n="75.4">arrosé</w> <w n="75.5">de</w> <w n="75.6">pleurs</w>.</l>
						<l n="76" num="4.8"><w n="76.1">Quel</w> <w n="76.2">contraste</w> (<w n="76.3">ironie</w> <w n="76.4">amère</w>)</l>
						<l n="77" num="4.9"><w n="77.1">Entre</w> <w n="77.2">ta</w> <w n="77.3">gloire</w> <w n="77.4">et</w> <w n="77.5">sa</w> <w n="77.6">misère</w>,</l>
						<l n="78" num="4.10"><w n="78.1">Entre</w> <w n="78.2">ta</w> <w n="78.3">joie</w> <w n="78.4">et</w> <w n="78.5">ses</w> <w n="78.6">douleurs</w> !</l>
					</lg>
					<lg n="5">
						<l n="79" num="5.1"><w n="79.1">O</w> <w n="79.2">Roi</w>-<w n="79.3">Soleil</w>, <w n="79.4">ton</w> <w n="79.5">fuit</w> <w n="79.6">de</w> <w n="79.7">chasse</w></l>
						<l n="80" num="5.2"><w n="80.1">Et</w> <w n="80.2">ton</w> <w n="80.3">sceptre</w> <w n="80.4">sont</w> <w n="80.5">impuissants</w>.</l>
						<l n="81" num="5.3"><w n="81.1">Dépose</w>-<w n="81.2">les</w> <w n="81.3">de</w> <w n="81.4">bonne</w> <w n="81.5">grâce</w>,</l>
						<l n="82" num="5.4"><w n="82.1">Et</w> <w n="82.2">de</w> <w n="82.3">ton</w> <w n="82.4">piédestal</w> <w n="82.5">descends</w>.</l>
						<l n="83" num="5.5"><w n="83.1">Ton</w> <w n="83.2">coursier</w> <w n="83.3">plein</w> <w n="83.4">d</w>’<w n="83.5">impatience</w></l>
						<l n="84" num="5.6"><w n="84.1">Se</w> <w n="84.2">cabre</w> <w n="84.3">avec</w> <w n="84.4">trop</w> <w n="84.5">d</w>’<w n="84.6">arrogance</w>,</l>
						<l n="85" num="5.7"><w n="85.1">Quand</w> <w n="85.2">leur</w> <w n="85.3">pouvoir</w> <w n="85.4">n</w>’<w n="85.5">existe</w> <w n="85.6">plus</w> :</l>
						<l n="86" num="5.8"><w n="86.1">Les</w> <w n="86.2">provinces</w> <w n="86.3">par</w> <w n="86.4">toi</w> <w n="86.5">conquises</w></l>
						<l n="87" num="5.9"><w n="87.1">Par</w> <w n="87.2">les</w> <w n="87.3">Allemands</w> <w n="87.4">sont</w> <w n="87.5">reprises</w> ;</l>
						<l n="88" num="5.10"><w n="88.1">Les</w> <w n="88.2">parlements</w> <w n="88.3">sont</w> <w n="88.4">revenus</w>.</l>
					</lg>
					<lg n="6">
						<l n="89" num="6.1"><w n="89.1">Bien</w> ! <w n="89.2">ici</w> <w n="89.3">siège</w> <w n="89.4">l</w>’<w n="89.5">assemblée</w>.</l>
						<l n="90" num="6.2"><w n="90.1">Ainsi</w> <w n="90.2">qu</w>’<w n="90.3">une</w> <w n="90.4">troupe</w> <w n="90.5">d</w>’<w n="90.6">acteurs</w>,</l>
						<l n="91" num="6.3"><w n="91.1">Sur</w> <w n="91.2">plus</w> <w n="91.3">d</w>’<w n="91.4">une</w> <w n="91.5">scène</w> <w n="91.6">enrôlée</w>,</l>
						<l n="92" num="6.4"><w n="92.1">Courant</w> <w n="92.2">après</w> <w n="92.3">des</w> <w n="92.4">spectateurs</w>,</l>
						<l n="93" num="6.5"><w n="93.1">Le</w> <w n="93.2">parlement</w>, <w n="93.3">de</w> <w n="93.4">scène</w> <w n="93.5">en</w> <w n="93.6">scène</w>,</l>
						<l n="94" num="6.6"><w n="94.1">De</w> <w n="94.2">Bordeaux</w> <w n="94.3">ici</w> <w n="94.4">se</w> <w n="94.5">promène</w></l>
						<l n="95" num="6.7"><w n="95.1">Pour</w> <w n="95.2">faire</w> <w n="95.3">ses</w> <w n="95.4">seconds</w> <w n="95.5">débuts</w>.</l>
						<l n="96" num="6.8"><w n="96.1">C</w>’<w n="96.2">est</w> <w n="96.3">un</w> <w n="96.4">grand</w> <w n="96.5">drame</w> <w n="96.6">qui</w> <w n="96.7">se</w> <w n="96.8">joue</w></l>
						<l n="97" num="6.9"><w n="97.1">Et</w> <w n="97.2">dont</w> <w n="97.3">l</w>’<w n="97.4">intrigue</w> <w n="97.5">se</w> <w n="97.6">dénoue</w></l>
						<l n="98" num="6.10"><w n="98.1">Devant</w> <w n="98.2">les</w> <w n="98.3">peuples</w> <w n="98.4">confondus</w>.</l>
					</lg>
					<lg n="7">
						<l n="99" num="7.1"><w n="99.1">Ce</w> <w n="99.2">n</w>’<w n="99.3">est</w> <w n="99.4">plus</w> <w n="99.5">une</w> <w n="99.6">Iphigénie</w>,</l>
						<l n="100" num="7.2"><w n="100.1">Au</w> <w n="100.2">spectacle</w> <w n="100.3">de</w> <w n="100.4">ses</w> <w n="100.5">douleurs</w></l>
						<l n="101" num="7.3"><w n="101.1">Tracé</w> <w n="101.2">par</w> <w n="101.3">la</w> <w n="101.4">main</w> <w n="101.5">du</w> <w n="101.6">génie</w>,</l>
						<l n="102" num="7.4"><w n="102.1">Qui</w> <w n="102.2">nous</w> <w n="102.3">fera</w> <w n="102.4">verser</w> <w n="102.5">des</w> <w n="102.6">pleurs</w>.</l>
						<l n="103" num="7.5"><w n="103.1">L</w>’<w n="103.2">héroïne</w> <w n="103.3">est</w> <w n="103.4">plus</w> <w n="103.5">attachante</w>,</l>
						<l n="104" num="7.6"><w n="104.1">Car</w> <w n="104.2">c</w>’<w n="104.3">est</w> <w n="104.4">la</w> <w n="104.5">France</w> <w n="104.6">agonisante</w></l>
						<l n="105" num="7.7"><w n="105.1">Dont</w> <w n="105.2">se</w> <w n="105.3">déroulent</w> <w n="105.4">les</w> <w n="105.5">destins</w>.</l>
						<l n="106" num="7.8"><w n="106.1">Le</w> <w n="106.2">décor</w> <w n="106.3">est</w> <w n="106.4">immense</w> <w n="106.5">et</w> <w n="106.6">sombre</w> :</l>
						<l n="107" num="7.9"><w n="107.1">Là</w>-<w n="107.2">bas</w> <w n="107.3">du</w> <w n="107.4">sang</w>, <w n="107.5">partout</w> <w n="107.6">de</w> <w n="107.7">l</w>’<w n="107.8">ombre</w>,</l>
						<l n="108" num="7.10"><w n="108.1">Et</w> <w n="108.2">des</w> <w n="108.3">horizons</w> <w n="108.4">incertains</w>.</l>
					</lg>
					<lg n="8">
						<l n="109" num="8.1"><w n="109.1">Malgré</w> <w n="109.2">l</w>’<w n="109.3">intérêt</w> <w n="109.4">de</w> <w n="109.5">la</w> <w n="109.6">scène</w>,</l>
						<l n="110" num="8.2"><w n="110.1">S</w>’<w n="110.2">envole</w> <w n="110.3">notre</w> <w n="110.4">souvenir</w> ;</l>
						<l n="111" num="8.3"><w n="111.1">Et</w> <w n="111.2">pour</w> <w n="111.3">un</w> <w n="111.4">instant</w> <w n="111.5">il</w> <w n="111.6">ramène</w></l>
						<l n="112" num="8.4"><w n="112.1">Tout</w> <w n="112.2">un</w> <w n="112.3">passé</w> <w n="112.4">qui</w> <w n="112.5">va</w> <w n="112.6">surgir</w>.</l>
						<l n="113" num="8.5"><w n="113.1">Quelles</w> <w n="113.2">ombres</w> ! <w n="113.3">Quel</w> <w n="113.4">personnages</w> !</l>
						<l n="114" num="8.6"><w n="114.1">Et</w> <w n="114.2">quels</w> <w n="114.3">fantastiques</w> <w n="114.4">mirages</w> !</l>
						<l n="115" num="8.7"><w n="115.1">Oh</w> ! <w n="115.2">quel</w> <w n="115.3">tourbillon</w> <w n="115.4">insensé</w> !</l>
						<l n="116" num="8.8"><w n="116.1">Tout</w> <w n="116.2">tourne</w>, <w n="116.3">tout</w> <w n="116.4">passe</w> <w n="116.5">et</w> <w n="116.6">repasse</w> ;</l>
						<l n="117" num="8.9"><w n="117.1">En</w> <w n="117.2">sans</w> <w n="117.3">laisser</w> <w n="117.4">la</w> <w n="117.5">moindre</w> <w n="117.6">trace</w>,</l>
						<l n="118" num="8.10"><w n="118.1">Se</w> <w n="118.2">mêle</w> <w n="118.3">et</w> <w n="118.4">se</w> <w n="118.5">trouve</w> <w n="118.6">effacé</w>.</l>
					</lg>
					<lg n="9">
						<l n="119" num="9.1"><w n="119.1">Roi</w>, <w n="119.2">reine</w>, <w n="119.3">dauphins</w> <w n="119.4">et</w> <w n="119.5">dauphines</w>,</l>
						<l n="120" num="9.2"><w n="120.1">Favorites</w> <w n="120.2">et</w> <w n="120.3">grands</w> <w n="120.4">seigneurs</w> ;</l>
						<l n="121" num="9.3"><w n="121.1">Belles</w> <w n="121.2">dames</w>, <w n="121.3">aux</w> <w n="121.4">taille</w> <w n="121.5">fines</w> ;</l>
						<l n="122" num="9.4"><w n="122.1">Petits</w> <w n="122.2">abbés</w> <w n="122.3">et</w> <w n="122.4">monseigneurs</w>,</l>
						<l n="123" num="9.5"><w n="123.1">Ministres</w>, <w n="123.2">galants</w>, <w n="123.3">politiques</w>,</l>
						<l n="124" num="9.6"><w n="124.1">Prosateurs</w>, <w n="124.2">poëtes</w> <w n="124.3">comiques</w>.</l>
						<l n="125" num="9.7"><w n="125.1">C</w>’<w n="125.2">est</w> <w n="125.3">Luxembourg</w> <w n="125.4">et</w> <w n="125.5">Saint</w>-<w n="125.6">Simon</w>,</l>
						<l n="126" num="9.8"><w n="126.1">Louvois</w>, <w n="126.2">Bossuet</w>, <w n="126.3">et</w> <w n="126.4">La</w> <w n="126.5">Vallière</w>,</l>
						<l n="127" num="9.9"><w n="127.1">Colbert</w>, <w n="127.2">Henriette</w> <w n="127.3">d</w>’<w n="127.4">Angleterre</w>,</l>
						<l n="128" num="9.10"><w n="128.1">Villars</w>, <w n="128.2">Racine</w> <w n="128.3">et</w> <w n="128.4">Fénelon</w>.</l>
					</lg>
					<lg n="10">
						<l n="129" num="10.1"><w n="129.1">Cordons</w> <w n="129.2">bleus</w> <w n="129.3">et</w> <w n="129.4">robes</w> <w n="129.5">bouffantes</w>,</l>
						<l n="130" num="10.2"><w n="130.1">Perruques</w>, <w n="130.2">dentelles</w>, <w n="130.3">canons</w>,</l>
						<l n="131" num="10.3"><w n="131.1">Paniers</w> <w n="131.2">et</w> <w n="131.3">gazes</w> <w n="131.4">transparentes</w>,</l>
						<l n="132" num="10.4"><w n="132.1">Jabots</w>, <w n="132.2">seins</w> <w n="132.3">blancs</w> <w n="132.4">et</w> <w n="132.5">pieds</w> <w n="132.6">mignons</w>,</l>
						<l n="133" num="10.5"><w n="133.1">Élégances</w>, <w n="133.2">galanteries</w>,</l>
						<l n="134" num="10.6"><w n="134.1">Aveux</w> <w n="134.2">d</w>’<w n="134.3">amour</w>, <w n="134.4">mutineries</w>,</l>
						<l n="135" num="10.7"><w n="135.1">Intrigues</w> <w n="135.2">et</w> <w n="135.3">frivolités</w>,</l>
						<l n="136" num="10.8"><w n="136.1">Tout</w> <w n="136.2">disparaît</w> (<w n="136.3">rêve</w> <w n="136.4">éphémère</w> !)</l>
						<l n="137" num="10.9"><w n="137.1">Dans</w> <w n="137.2">un</w> <w n="137.3">flot</w> <w n="137.4">de</w> <w n="137.5">poudre</w> <w n="137.6">légère</w></l>
						<l n="138" num="10.10"><w n="138.1">Au</w> <w n="138.2">souffle</w> <w n="138.3">des</w> <w n="138.4">réalités</w>.</l>
					</lg>
					<lg n="11">
						<l n="139" num="11.1"><w n="139.1">Nos</w> <w n="139.2">députés</w>, <w n="139.3">tête</w> <w n="139.4">baissée</w>,</l>
						<l n="140" num="11.2"><w n="140.1">Préoccupés</w>, <w n="140.2">tout</w> <w n="140.3">soucieux</w>,</l>
						<l n="141" num="11.3"><w n="141.1">Viennent</w> <w n="141.2">promener</w> <w n="141.3">leur</w> <w n="141.4">pensée</w></l>
						<l n="142" num="11.4"><w n="142.1">Sous</w> <w n="142.2">les</w> <w n="142.3">grands</w> <w n="142.4">arbres</w>, <w n="142.5">près</w> <w n="142.6">des</w> <w n="142.7">Dieux</w>,</l>
						<l n="143" num="11.5"><w n="143.1">Ou</w> <w n="143.2">près</w> <w n="143.3">des</w> <w n="143.4">bassins</w> <w n="143.5">de</w> <w n="143.6">Latone</w></l>
						<l n="144" num="11.6"><w n="144.1">Et</w> <w n="144.2">de</w> <w n="144.3">Neptune</w> <w n="144.4">qui</w> <w n="144.5">s</w>’<w n="144.6">étonne</w></l>
						<l n="145" num="11.7"><w n="145.1">De</w> <w n="145.2">ces</w> <w n="145.3">hôtes</w> <w n="145.4">nouveau</w>-<w n="145.5">venus</w> ;</l>
						<l n="146" num="11.8"><w n="146.1">Près</w> <w n="146.2">des</w> <w n="146.3">sylvains</w> <w n="146.4">et</w> <w n="146.5">des</w> <w n="146.6">naïades</w>,</l>
						<l n="147" num="11.9"><w n="147.1">Des</w> <w n="147.2">faunes</w>, <w n="147.3">des</w> <w n="147.4">hamadryades</w></l>
						<l n="148" num="11.10"><w n="148.1">Ouvrant</w> <w n="148.2">de</w> <w n="148.3">grand</w> <w n="148.4">yeux</w> <w n="148.5">éperdus</w>.</l>
					</lg>
					<lg n="12">
						<l n="149" num="12.1"><w n="149.1">Alors</w>, <w n="149.2">du</w> <w n="149.3">fond</w> <w n="149.4">de</w> <w n="149.5">ma</w> <w n="149.6">poitrine</w></l>
						<l n="150" num="12.2"><w n="150.1">Je</w> <w n="150.2">sens</w> <w n="150.3">s</w>’<w n="150.4">exhaler</w> <w n="150.5">un</w> <w n="150.6">soupir</w> ;</l>
						<l n="151" num="12.3"><w n="151.1">Et</w> <w n="151.2">mon</w> <w n="151.3">esprit</w> <w n="151.4">que</w> <w n="151.5">tout</w> <w n="151.6">chagrine</w></l>
						<l n="152" num="12.4"><w n="152.1">Cherche</w> <w n="152.2">à</w> <w n="152.3">pénétrer</w> <w n="152.4">l</w>’<w n="152.5">avenir</w>.</l>
						<l n="153" num="12.5"><w n="153.1">Efforts</w> <w n="153.2">superflus</w> ! <w n="153.3">Tout</w> <w n="153.4">est</w> <w n="153.5">sombre</w>…</l>
						<l n="154" num="12.6"><w n="154.1">Pauvre</w> <w n="154.2">pays</w> !… <w n="154.3">Ah</w> ! <w n="154.4">puisse</w> <w n="154.5">l</w>’<w n="154.6">ombre</w></l>
						<l n="155" num="12.7"><w n="155.1">Des</w> <w n="155.2">Condé</w>, <w n="155.3">Louvois</w>, <w n="155.4">des</w> <w n="155.5">Colbert</w>,</l>
						<l n="156" num="12.8"><w n="156.1">Errants</w> <w n="156.2">au</w> <w n="156.3">sein</w> <w n="156.4">de</w> <w n="156.5">la</w> <w n="156.6">verdure</w></l>
						<l n="157" num="12.9"><w n="157.1">Guider</w>, <w n="157.2">pour</w> <w n="157.3">panser</w> <w n="157.4">ta</w> <w n="157.5">blessure</w>.</l>
						<l n="158" num="12.10"><w n="158.1">Nos</w> <w n="158.2">ministres</w> <w n="158.3">nommées</w> <w n="158.4">hier</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">12 Mars 1871</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>