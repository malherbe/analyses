<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DU SIÈGE DE PARIS</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="SFL">
					<name>
						<forename>Théobald</forename>
						<surname>SAINT-FÉLIX</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>161 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">SFL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>CHANTS DU SIÈGE DE PARIS. 1870-1871</title>
						<author>THÉOBALD SAINT-FÉLIX</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Imprimerie Ch. Schiller</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="SFL2">
				<head type="main">LES OISEAUX DU PROSCRIT</head>
				<head type="form">MÉLODIE</head>
				<opener>
					<salute>A VICTOR HUGO</salute>
				</opener>
				<div type="section" n="1">
					<head type="number">1</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Petits</w> <w n="1.2">oiseaux</w>, <w n="1.3">vous</w> <w n="1.4">que</w> <w n="1.5">je</w> <w n="1.6">vis</w> <w n="1.7">éclore</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">votre</w> <w n="2.3">nid</w>, <w n="2.4">fruit</w> <w n="2.5">de</w> <w n="2.6">chastes</w> <w n="2.7">amours</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Restez</w> <w n="3.2">longtemps</w>, <w n="3.3">amis</w> <w n="3.4">restez</w> <w n="3.5">encore</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Car</w> <w n="4.2">le</w> <w n="4.3">malheur</w> <w n="4.4">vient</w> <w n="4.5">planer</w> <w n="4.6">sur</w> <w n="4.7">vos</w> <w n="4.8">jours</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Pauvres</w> <w n="5.2">petits</w>, <w n="5.3">le</w> <w n="5.4">destin</w> <w n="5.5">nous</w> <w n="5.6">opprime</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Dieu</w> <w n="6.2">semble</w> <w n="6.3">sourd</w> <w n="6.4">à</w> <w n="6.5">mes</w> <w n="6.6">vœux</w>, <w n="6.7">à</w> <w n="6.8">vos</w> <w n="6.9">cris</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Mais</w> <w n="7.2">il</w> <w n="7.3">viendra</w> <w n="7.4">consoler</w> <w n="7.5">la</w> <w n="7.6">victime</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Alors</w> <w n="8.2">son</w> <w n="8.3">bras</w> <w n="8.4">vengera</w> <w n="8.5">les</w> <w n="8.6">proscrits</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">2</head>
					<lg n="1">
						<l n="9" num="1.1"><w n="9.1">Petits</w>, <w n="9.2">oiseaux</w>, <w n="9.3">déjà</w> <w n="9.4">croissent</w> <w n="9.5">vos</w> <w n="9.6">ailes</w>,</l>
						<l n="10" num="1.2"><w n="10.1">Besoin</w> <w n="10.2">d</w>’<w n="10.3">aimer</w> <w n="10.4">à</w> <w n="10.5">tous</w> <w n="10.6">se</w> <w n="10.7">fait</w> <w n="10.8">sentir</w>…</l>
						<l n="11" num="1.3"><w n="11.1">Libres</w>, <w n="11.2">volez</w>, <w n="11.3">mais</w> <w n="11.4">revenez</w> <w n="11.5">fidèles</w></l>
						<l n="12" num="1.4"><w n="12.1">Au</w> <w n="12.2">joli</w> <w n="12.3">nid</w> <w n="12.4">qui</w> <w n="12.5">vous</w> <w n="12.6">verra</w> <w n="12.7">partir</w> !</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><w n="13.1">Pauvres</w> <w n="13.2">petits</w>, <w n="13.3">le</w> <w n="13.4">destin</w> <w n="13.5">nous</w> <w n="13.6">opprime</w>,</l>
						<l n="14" num="2.2"><w n="14.1">Dieu</w> <w n="14.2">semble</w> <w n="14.3">sourd</w> <w n="14.4">à</w> <w n="14.5">mes</w> <w n="14.6">vœux</w>, <w n="14.7">à</w> <w n="14.8">vos</w> <w n="14.9">cris</w> ;</l>
						<l n="15" num="2.3"><w n="15.1">Mais</w> <w n="15.2">il</w> <w n="15.3">viendra</w> <w n="15.4">consoler</w> <w n="15.5">la</w> <w n="15.6">victime</w>,</l>
						<l n="16" num="2.4"><w n="16.1">Alors</w> <w n="16.2">son</w> <w n="16.3">bras</w> <w n="16.4">vengera</w> <w n="16.5">les</w> <w n="16.6">proscrits</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">3</head>
					<lg n="1">
						<l n="17" num="1.1"><w n="17.1">Petits</w> <w n="17.2">oiseaux</w>, <w n="17.3">n</w>’<w n="17.4">allez</w> <w n="17.5">pas</w> <w n="17.6">au</w> <w n="17.7">bocage</w>.</l>
						<l n="18" num="1.2"><w n="18.1">Craignez</w> <w n="18.2">le</w> <w n="18.3">plomb</w>, <w n="18.4">la</w> <w n="18.5">serre</w> <w n="18.6">des</w> <w n="18.7">vautours</w>,</l>
						<l n="19" num="1.3"><w n="19.1">De</w> <w n="19.2">ma</w> <w n="19.3">prison</w> <w n="19.4">faites</w>-<w n="19.5">vous</w> <w n="19.6">une</w> <w n="19.7">cage</w>.</l>
						<l n="20" num="1.4"><w n="20.1">Là</w>, <w n="20.2">je</w> <w n="20.3">pourrai</w> <w n="20.4">veiller</w> <w n="20.5">sur</w> <w n="20.6">vos</w> <w n="20.7">amours</w>.</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1"><w n="21.1">Pauvres</w> <w n="21.2">petits</w>, <w n="21.3">le</w> <w n="21.4">destin</w> <w n="21.5">nous</w> <w n="21.6">opprime</w>,</l>
						<l n="22" num="2.2"><w n="22.1">Dieu</w> <w n="22.2">semble</w> <w n="22.3">sourd</w> <w n="22.4">à</w> <w n="22.5">mes</w> <w n="22.6">vœux</w>, <w n="22.7">à</w> <w n="22.8">vos</w> <w n="22.9">cris</w> ;</l>
						<l n="23" num="2.3"><w n="23.1">Mais</w> <w n="23.2">il</w> <w n="23.3">viendra</w> <w n="23.4">consoler</w> <w n="23.5">la</w> <w n="23.6">victime</w>,</l>
						<l n="24" num="2.4"><w n="24.1">Alors</w> <w n="24.2">son</w> <w n="24.3">bras</w> <w n="24.4">vengera</w> <w n="24.5">les</w> <w n="24.6">proscrits</w>.</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">4</head>
					<lg n="1">
						<l n="25" num="1.1"><w n="25.1">Petits</w> <w n="25.2">oiseaux</w>, <w n="25.3">n</w>’<w n="25.4">allez</w> <w n="25.5">pas</w> <w n="25.6">dans</w> <w n="25.7">la</w> <w n="25.8">plaine</w>,</l>
						<l n="26" num="1.2"><w n="26.1">Car</w> <w n="26.2">l</w>’<w n="26.3">oiseleur</w> <w n="26.4">vous</w> <w n="26.5">donnerait</w> <w n="26.6">la</w> <w n="26.7">mort</w>. —</l>
						<l n="27" num="1.3"><w n="27.1">Restez</w> <w n="27.2">ici</w> <w n="27.3">pour</w> <w n="27.4">alléger</w> <w n="27.5">ma</w> <w n="27.6">peine</w>,</l>
						<l n="28" num="1.4"><w n="28.1">Restez</w> <w n="28.2">ici</w> <w n="28.3">pour</w> <w n="28.4">adoucir</w> <w n="28.5">mon</w> <w n="28.6">sort</w> !</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1"><w n="29.1">Pauvres</w> <w n="29.2">petits</w>, <w n="29.3">le</w> <w n="29.4">destin</w> <w n="29.5">nous</w> <w n="29.6">opprime</w>,</l>
						<l n="30" num="2.2"><w n="30.1">Dieu</w> <w n="30.2">semble</w> <w n="30.3">sourd</w> <w n="30.4">à</w> <w n="30.5">mes</w> <w n="30.6">vœux</w>, <w n="30.7">à</w> <w n="30.8">vos</w> <w n="30.9">cris</w> ;</l>
						<l n="31" num="2.3"><w n="31.1">Mais</w> <w n="31.2">il</w> <w n="31.3">viendra</w> <w n="31.4">consoler</w> <w n="31.5">la</w> <w n="31.6">victime</w>,</l>
						<l n="32" num="2.4"><w n="32.1">Alors</w> <w n="32.2">son</w> <w n="32.3">bras</w> <w n="32.4">vengera</w> <w n="32.5">les</w> <w n="32.6">proscrits</w>.</l>
					</lg>
				</div>
				<closer>
					<note type="footnote" id="">N.B. — La musique, chant et piano, composée par M. Marfaing, est en vente chez M. Sylvain Saint-Étienne, éditeur, n°31 bis, Faubourg Montmartre, à Paris.</note>
				</closer>
			</div></body></text></TEI>