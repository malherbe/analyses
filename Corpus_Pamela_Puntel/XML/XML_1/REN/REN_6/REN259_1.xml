<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">AU BRUIT DU CANON</title>
				<title type="medium">Édition électronique</title>
				<author key="REN">
					<name>
						<forename>Armand</forename>
						<surname>RENAUD</surname>
					</name>
					<date from="1836" to="1895">1836-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">REN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>AU BRUIT DU CANON</title>
						<author>ARMAND RENAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https://archive.org/details/posiesrecueili00renauoft/page/n321</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>AU BRUIT DU CANON</title>
								<author>ARMAND RENAUD</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="REN259">
				<head type="main">JUSTICE D’OUTRE-TOMBE</head>
				<opener>
					<salute>A LA MÉMOIRE D’HENRI.REGNAULT TUÉ A MONTRETOUT</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">Guillaume</w>, <w n="1.3">césar</w> <w n="1.4">vainqueur</w> <w n="1.5">mais</w> <w n="1.6">éphémère</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Après</w> <w n="2.2">le</w> <w n="2.3">joyeux</w> <w n="2.4">trône</w> <w n="2.5">aura</w> <w n="2.6">la</w> <w n="2.7">tombe</w> <w n="2.8">amère</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Quand</w>, <w n="3.2">sceptre</w> <w n="3.3">en</w> <w n="3.4">main</w>, <w n="3.5">couvert</w> <w n="3.6">d</w>’<w n="3.7">un</w> <w n="3.8">marbre</w> <w n="3.9">noir</w> <w n="3.10">ou</w> <w n="3.11">blanc</w></l>
					<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">l</w>’<w n="4.3">on</w> <w n="4.4">aura</w> <w n="4.5">sculpté</w> <w n="4.6">l</w>’<w n="4.7">aigle</w> <w n="4.8">des</w> <w n="4.9">soldatesques</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">ne</w> <w n="5.3">pourra</w>, <w n="5.4">malgré</w> <w n="5.5">ses</w> <w n="5.6">titres</w> <w n="5.7">gigantesques</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Repousser</w> <w n="6.2">l</w>’<w n="6.3">humble</w> <w n="6.4">ver</w> <w n="6.5">qui</w> <w n="6.6">lui</w> <w n="6.7">mordra</w> <w n="6.8">le</w> <w n="6.9">flanc</w>,</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Il</w> <w n="7.2">se</w> <w n="7.3">peut</w> <w n="7.4">que</w> <w n="7.5">son</w> <w n="7.6">ombre</w>, <w n="7.7">au</w> <w n="7.8">gouffre</w> <w n="7.9">du</w> <w n="7.10">mystère</w>,</l>
					<l n="8" num="2.2"><w n="8.1">Doive</w> <w n="8.2">retrouver</w> <w n="8.3">ceux</w> <w n="8.4">qui</w> <w n="8.5">vécurent</w> <w n="8.6">sur</w> <w n="8.7">terre</w>,</l>
					<l n="9" num="2.3"><w n="9.1">Et</w> <w n="9.2">qu</w>’<w n="9.3">il</w> <w n="9.4">ait</w> <w n="9.5">à</w> <w n="9.6">revoir</w> <w n="9.7">étrangement</w> <w n="9.8">là</w>-<w n="9.9">bas</w></l>
					<l n="10" num="2.4"><w n="10.1">Le</w> <w n="10.2">tas</w> <w n="10.3">des</w> <w n="10.4">morts</w> <w n="10.5">sanglants</w> <w n="10.6">tués</w> <w n="10.7">dans</w> <w n="10.8">ses</w> <w n="10.9">batailles</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Qui</w>, <w n="11.2">les</w> <w n="11.3">reins</w> <w n="11.4">fracassés</w> <w n="11.5">ou</w> <w n="11.6">perdant</w> <w n="11.7">leurs</w> <w n="11.8">entrailles</w>,</l>
					<l n="12" num="2.6"><w n="12.1">Au</w> <w n="12.2">vent</w> <w n="12.3">glacé</w> <w n="12.4">des</w> <w n="12.5">nuits</w> <w n="12.6">se</w> <w n="12.7">sont</w> <w n="12.8">tordu</w> <w n="12.9">les</w> <w n="12.10">bras</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Et</w> <w n="13.2">peut</w>-<w n="13.3">être</w>, <w n="13.4">gardant</w> <w n="13.5">ses</w> <w n="13.6">vieux</w> <w n="13.7">instincts</w> <w n="13.8">d</w>’<w n="13.9">empire</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Passera</w>-<w n="14.2">t</w>-<w n="14.3">il</w> <w n="14.4">fier</w>, <w n="14.5">calme</w>, <w n="14.6">ébauchant</w> <w n="14.7">un</w> <w n="14.8">sourire</w>,</l>
					<l n="15" num="3.3"><w n="15.1">Content</w> <w n="15.2">de</w> <w n="15.3">lui</w> <w n="15.4">devant</w> <w n="15.5">ces</w> <w n="15.6">visions</w> <w n="15.7">d</w>’<w n="15.8">horreur</w>,</l>
					<l n="16" num="3.4"><w n="16.1">Pensant</w> : <w n="16.2">Je</w> <w n="16.3">suis</w> <w n="16.4">toujours</w> <w n="16.5">de</w> <w n="16.6">la</w> <w n="16.7">race</w> <w n="16.8">des</w> <w n="16.9">maîtres</w> ;</l>
					<l n="17" num="3.5"><w n="17.1">Je</w> <w n="17.2">reconnais</w> <w n="17.3">ma</w> <w n="17.4">gloire</w> <w n="17.5">à</w> <w n="17.6">ces</w> <w n="17.7">fauchaisons</w> <w n="17.8">d</w>’<w n="17.9">êtres</w>.</l>
					<l n="18" num="3.6"><w n="18.1">Qu</w>’<w n="18.2">on</w> <w n="18.3">rende</w> <w n="18.4">la</w> <w n="18.5">couronne</w> <w n="18.6">à</w> <w n="18.7">qui</w> <w n="18.8">fut</w> <w n="18.9">l</w>’<w n="18.10">Empereur</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Mais</w>, <w n="19.2">si</w> <w n="19.3">le</w> <w n="19.4">droit</w> <w n="19.5">n</w>’<w n="19.6">est</w> <w n="19.7">pas</w> <w n="19.8">un</w> <w n="19.9">vain</w> <w n="19.10">mot</w>, <w n="19.11">de</w> <w n="19.12">la</w> <w n="19.13">foule</w></l>
					<l n="20" num="4.2"><w n="20.1">Des</w> <w n="20.2">combattants</w> <w n="20.3">tombés</w> <w n="20.4">jadis</w> <w n="20.5">dans</w> <w n="20.6">cette</w> <w n="20.7">houle</w>,</l>
					<l n="21" num="4.3"><w n="21.1">Sans</w> <w n="21.2">grade</w>, <w n="21.3">sans</w> <w n="21.4">éclat</w>, <w n="21.5">par</w> <w n="21.6">soif</w> <w n="21.7">de</w> <w n="21.8">liberté</w></l>
					<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">par</w> <w n="22.3">soif</w> <w n="22.4">du</w> <w n="22.5">devoir</w>, <w n="22.6">noble</w> <w n="22.7">et</w> <w n="22.8">calme</w>, <w n="22.9">un</w> <w n="22.10">fantôme</w></l>
					<l n="23" num="4.5"><w n="23.1">Se</w> <w n="23.2">lèvera</w> <w n="23.3">devant</w> <w n="23.4">le</w> <w n="23.5">potentat</w> <w n="23.6">Guillaume</w></l>
					<l n="24" num="4.6"><w n="24.1">Qui</w> <w n="24.2">roulera</w> <w n="24.3">sur</w> <w n="24.4">lui</w> <w n="24.5">son</w> <w n="24.6">regard</w> <w n="24.7">hébété</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Le</w> <w n="25.2">visage</w> <w n="25.3">sera</w> <w n="25.4">d</w>’<w n="25.5">un</w> <w n="25.6">jeune</w> <w n="25.7">homme</w> <w n="25.8">un</w> <w n="25.9">peu</w> <w n="25.10">sombre</w>,</l>
					<l n="26" num="5.2"><w n="26.1">A</w> <w n="26.2">l</w>’<w n="26.3">œil</w> <w n="26.4">profond</w>, <w n="26.5">avec</w> <w n="26.6">des</w> <w n="26.7">cheveux</w> <w n="26.8">noirs</w> <w n="26.9">sans</w> <w n="26.10">nombre</w>,</l>
					<l n="27" num="5.3"><w n="27.1">Où</w> <w n="27.2">tant</w> <w n="27.3">de</w> <w n="27.4">rêve</w> <w n="27.5">et</w> <w n="27.6">tant</w> <w n="27.7">de</w> <w n="27.8">majesté</w> <w n="27.9">luiront</w></l>
					<l n="28" num="5.4"><w n="28.1">Que</w> <w n="28.2">l</w>’<w n="28.3">autre</w> <w n="28.4">en</w> <w n="28.5">cheveux</w> <w n="28.6">blancs</w> <w n="28.7">cessera</w> <w n="28.8">d</w>’<w n="28.9">être</w> <w n="28.10">auguste</w>,</l>
					<l n="29" num="5.5"><w n="29.1">Et</w> <w n="29.2">malgré</w> <w n="29.3">lui</w> <w n="29.4">ploîra</w>, <w n="29.5">par</w> <w n="29.6">une</w> <w n="29.7">force</w> <w n="29.8">juste</w>,</l>
					<l n="30" num="5.6"><w n="30.1">Sous</w> <w n="30.2">tant</w> <w n="30.3">d</w>’<w n="30.4">amour</w> <w n="30.5">au</w> <w n="30.6">cœur</w> <w n="30.7">et</w> <w n="30.8">de</w> <w n="30.9">génie</w> <w n="30.10">au</w> <w n="30.11">front</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Ce</w> <w n="31.2">front</w>, <w n="31.3">en</w> <w n="31.4">plein</w> <w n="31.5">milieu</w> <w n="31.6">troué</w> <w n="31.7">par</w> <w n="31.8">une</w> <w n="31.9">balle</w>,</l>
					<l n="32" num="6.2"><w n="32.1">Rendra</w> <w n="32.2">ce</w> <w n="32.3">souvenir</w> <w n="32.4">à</w> <w n="32.5">l</w>’<w n="32.6">ombre</w> <w n="32.7">impériale</w></l>
					<l n="33" num="6.3"><w n="33.1">Que</w>, <w n="33.2">braquant</w> <w n="33.3">ses</w> <w n="33.4">canons</w> <w n="33.5">sur</w> <w n="33.6">Paris</w> <w n="33.7">autrefois</w>,</l>
					<l n="34" num="6.4"><w n="34.1">Il</w> <w n="34.2">entendit</w> <w n="34.3">conter</w> <w n="34.4">avec</w> <w n="34.5">indifférence</w></l>
					<l n="35" num="6.5"><w n="35.1">La</w> <w n="35.2">mort</w>, <w n="35.3">à</w> <w n="35.4">vingt</w>-<w n="35.5">huit</w> <w n="35.6">ans</w>, <w n="35.7">d</w>’<w n="35.8">un</w> <w n="35.9">grand</w> <w n="35.10">peintre</w> <w n="35.11">de</w> <w n="35.12">France</w>,</l>
					<l n="36" num="6.6"><w n="36.1">Par</w> <w n="36.2">un</w> <w n="36.3">de</w> <w n="36.4">ses</w> <w n="36.5">soldats</w> <w n="36.6">frappé</w> <w n="36.7">du</w> <w n="36.8">fond</w> <w n="36.9">d</w>’<w n="36.10">un</w> <w n="36.11">bois</w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">Sa</w> <w n="37.2">première</w> <w n="37.3">surprise</w> <w n="37.4">étant</w> <w n="37.5">alors</w> <w n="37.6">passé</w>,</l>
					<l n="38" num="7.2"><w n="38.1">Il</w> <w n="38.2">se</w> <w n="38.3">redressera</w> <w n="38.4">hautain</w> <w n="38.5">à</w> <w n="38.6">la</w> <w n="38.7">pensée</w></l>
					<l n="39" num="7.3"><w n="39.1">Qu</w>’<w n="39.2">il</w> <w n="39.3">n</w>’<w n="39.4">a</w> <w n="39.5">là</w> <w n="39.6">devant</w> <w n="39.7">lui</w> <w n="39.8">nul</w> <w n="39.9">prince</w> <w n="39.10">de</w> <w n="39.11">son</w> <w n="39.12">rang</w>,</l>
					<l n="40" num="7.4"><w n="40.1">Rien</w> <w n="40.2">qu</w>’<w n="40.3">un</w> <w n="40.4">chétif</w> <w n="40.5">n</w>’<w n="40.6">ayant</w> <w n="40.7">jamais</w> <w n="40.8">conduit</w> <w n="40.9">d</w>’<w n="40.10">armée</w>,</l>
					<l n="41" num="7.5"><w n="41.1">Et</w> <w n="41.2">n</w>’<w n="41.3">ayant</w> <w n="41.4">jamais</w> <w n="41.5">fait</w> <w n="41.6">une</w> <w n="41.7">immense</w> <w n="41.8">fumée</w></l>
					<l n="42" num="7.6"><w n="42.1">Des</w> <w n="42.2">villes</w> <w n="42.3">que</w> <w n="42.4">condamne</w> <w n="42.5">à</w> <w n="42.6">mort</w> <w n="42.7">le</w> <w n="42.8">conquérant</w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Mais</w> <w n="43.2">tandis</w> <w n="43.3">que</w> <w n="43.4">Césars</w> <w n="43.5">restera</w> <w n="43.6">sans</w> <w n="43.7">couronne</w>,</l>
					<l n="44" num="8.2"><w n="44.1">Voilà</w> <w n="44.2">qu</w>’<w n="44.3">un</w> <w n="44.4">diadème</w> <w n="44.5">où</w> <w n="44.6">l</w>’<w n="44.7">étoile</w> <w n="44.8">rayonne</w></l>
					<l n="45" num="8.3"><w n="45.1">Glorifîra</w> <w n="45.2">le</w> <w n="45.3">front</w> <w n="45.4">du</w> <w n="45.5">jeune</w> <w n="45.6">homme</w> <w n="45.7">inspiré</w></l>
					<l n="46" num="8.4"><w n="46.1">Qui</w>, <w n="46.2">sous</w> <w n="46.3">un</w> <w n="46.4">vaste</w> <w n="46.5">dôme</w> <w n="46.6">entouré</w> <w n="46.7">de</w> <w n="46.8">portiques</w>,</l>
					<l n="47" num="8.5"><w n="47.1">Dans</w> <w n="47.2">des</w> <w n="47.3">flots</w> <w n="47.4">de</w> <w n="47.5">lumière</w> <w n="47.6">aux</w> <w n="47.7">nuances</w> <w n="47.8">mystiques</w>,</l>
					<l n="48" num="8.6"><w n="48.1">Du</w> <w n="48.2">beau</w> <w n="48.3">suivra</w> <w n="48.4">sans</w> <w n="48.5">fin</w> <w n="48.6">le</w> <w n="48.7">rêve</w> <w n="48.8">immesuré</w>.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">L</w>’<w n="49.2">ex</w>-<w n="49.3">empereur</w> <w n="49.4">et</w> <w n="49.5">roi</w>, <w n="49.6">sans</w> <w n="49.7">trône</w>, <w n="49.8">sans</w> <w n="49.9">escorte</w>,</l>
					<l n="50" num="9.2"><w n="50.1">Du</w> <w n="50.2">palais</w> <w n="50.3">triomphal</w> <w n="50.4">n</w>’<w n="50.5">osant</w> <w n="50.6">franchir</w> <w n="50.7">la</w> <w n="50.8">porte</w>,</l>
					<l n="51" num="9.3"><w n="51.1">S</w>’<w n="51.2">affaissera</w> <w n="51.3">pleurant</w>. <w n="51.4">Court</w> <w n="51.5">loisir</w> <w n="51.6">de</w> <w n="51.7">l</w>’<w n="51.8">orgueil</w> !</l>
					<l n="52" num="9.4"><w n="52.1">Bientôt</w> <w n="52.2">viendra</w> <w n="52.3">quelqu</w>’<w n="52.4">un</w> <w n="52.5">pour</w> <w n="52.6">lui</w> <w n="52.7">marquer</w> <w n="52.8">sa</w> <w n="52.9">tâche</w>,</l>
					<l n="53" num="9.5"><w n="53.1">Qui</w>, <w n="53.2">le</w> <w n="53.3">fouet</w> <w n="53.4">à</w> <w n="53.5">la</w> <w n="53.6">main</w>, <w n="53.7">rendant</w> <w n="53.8">du</w> <w n="53.9">cœur</w> <w n="53.10">au</w> <w n="53.11">lâche</w>,</l>
					<l n="54" num="9.6"><w n="54.1">Lui</w> <w n="54.2">fera</w> <w n="54.3">balayer</w> <w n="54.4">la</w> <w n="54.5">poussière</w> <w n="54.6">du</w> <w n="54.7">seuil</w>.</l>
				</lg>
			</div></body></text></TEI>