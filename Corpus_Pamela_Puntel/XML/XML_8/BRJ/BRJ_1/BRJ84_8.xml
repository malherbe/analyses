<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ84" modus="sp" lm_max="7" metProfile="7, (5), (2)" form="suite périodique" schema="5(ababcdcd)" er_moy="0.7" er_max="6" er_min="0" er_mode="0(15/20)" er_moy_et="1.45">
					<head type="number">LXXXIV</head>
					<head type="main">TERESITA</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="1.2">f<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="1.5" punct="vg:7"><pgtc id="1" weight="2" schema="CR">p<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="2.3">f<seg phoneme="a" type="vs" value="1" rule="192" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="2.5" punct="pv:7"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="pv">ou</seg>x</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">F<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>t</w>-<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg></w> <w n="3.5">d<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>s<pgtc id="1" weight="2" schema="CR">p<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1">C<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>x</w> <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="4.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>r</w> <w n="4.5" punct="pi:7">n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="7" punct="pi">ou</seg>s</rhyme></pgtc></w> ?</l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1">L</w>'<w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">l</w>'<w n="5.5">h<seg phoneme="œ̃" type="vs" value="1" rule="260" place="4">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="496" place="3">y</seg></w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>vr<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="6.5" punct="pe:7">p<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pe ps">a</seg>s</rhyme></pgtc></w> !…</l>
						<l n="7" num="1.7" lm="5"><space unit="char" quantity="4"></space><w n="7.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ld<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="7.2" punct="pe:5">pl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="8" num="1.8" lm="2"><space unit="char" quantity="10"></space><w n="8.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababcdcd">
						<l n="9" num="2.1" lm="7" met="7"><w n="9.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="9.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="9.4">l</w>'<w n="9.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">I</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="2.2" lm="7" met="7"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="4">en</seg>t</w> <w n="10.4">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="5">a</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="10.5"><pgtc id="6" weight="2" schema="[CR">s<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>l</rhyme></pgtc></w></l>
						<l n="11" num="2.3" lm="7" met="7"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.3">qu</w>'<w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.5" punct="vg:7"><seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>bl<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="2.4" lm="7" met="7"><w n="12.1">L</w>'<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="12.3">d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt</w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="12.6" punct="pt:7">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">in</seg><pgtc id="6" weight="2" schema="CR">c<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="7" punct="pt">eu</seg>l</rhyme></pgtc></w>.</l>
						<l n="13" num="2.5" lm="7" met="7"><w n="13.1">L</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.6">h<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="2.6" lm="7" met="7"><w n="14.1">N</w>'<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>d<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t</w> <w n="14.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4">oin</seg>t</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="14.5" punct="pe:7">tr<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>p<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pe ps">a</seg>s</rhyme></pgtc></w> !…</l>
						<l n="15" num="2.7" lm="5"><space unit="char" quantity="4"></space><w n="15.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ld<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="15.2" punct="pe:5">pl<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="16" num="2.8" lm="2"><space unit="char" quantity="10"></space><w n="16.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababcdcd">
						<l n="17" num="3.1" lm="7" met="7">« <w n="17.1">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg></w> <w n="17.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.4">qu</w>'<w n="17.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="17.6" punct="pe:7"><pgtc id="9" weight="0" schema="[R"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="7">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe ps">e</seg></rhyme></pgtc></w> !… »</l>
						<l n="18" num="3.2" lm="7" met="7"><w n="18.1">Cr<seg phoneme="i" type="vs" value="1" rule="467" place="1">î</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="18.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="18.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.4">p<seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.5">c<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rps</rhyme></pgtc></w></l>
						<l n="19" num="3.3" lm="7" met="7"><w n="19.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="19.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>v<seg phoneme="o" type="vs" value="1" rule="414" place="3">ô</seg>t</w> <w n="19.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="19.4">l</w>'<w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>th<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="20" num="3.4" lm="7" met="7"><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="20.3">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="20.5" punct="pe:7">m<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" punct="pe">o</seg>rts</rhyme></pgtc></w> !</l>
						<l n="21" num="3.5" lm="7" met="7"><w n="21.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="21.4" punct="pv:7">m<seg phoneme="ɛ" type="vs" value="1" rule="381" place="6">e</seg>ill<pgtc id="11" weight="0" schema="R"><rhyme label="c" id="11" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="22" num="3.6" lm="7" met="7"><w n="22.1">N<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>s</w> <w n="22.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>ts</w> <w n="22.3">s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="22.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.5" punct="pe:7">gl<pgtc id="12" weight="0" schema="R"><rhyme label="d" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pe ps">a</seg>s</rhyme></pgtc></w> !…</l>
						<l n="23" num="3.7" lm="5"><space unit="char" quantity="4"></space><w n="23.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ld<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="23.2" punct="pe:5">pl<pgtc id="11" weight="0" schema="R"><rhyme label="c" id="11" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="24" num="3.8" lm="2"><space unit="char" quantity="10"></space><w n="24.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<pgtc id="12" weight="0" schema="R"><rhyme label="d" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="huitain" rhyme="ababcdcd">
						<l n="25" num="4.1" lm="7" met="7"><w n="25.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="25.2">l</w>'<w n="25.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="25.6" punct="vg:7">s<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg><pgtc id="13" weight="2" schema="CR">ffr<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="26" num="4.2" lm="7" met="7"><w n="26.1">S<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>t</w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.3">l</w>'<w n="26.4" punct="vg:7">h<seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>n<pgtc id="14" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="vg">é</seg></rhyme></pgtc></w>,</l>
						<l n="27" num="4.3" lm="7" met="7"><w n="27.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="27.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="300" place="3">ai</seg>t</w> <w n="27.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="27.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="27.5"><pgtc id="13" weight="2" schema="[CR">Fr<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="28" num="4.4" lm="7" met="7"><w n="28.1">D</w>'<w n="28.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="28.3">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="28.4" punct="pt:7"><seg phoneme="i" type="vs" value="1" rule="466" place="4">i</seg>mm<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>r<pgtc id="14" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>t<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="7" punct="pt">é</seg></rhyme></pgtc></w>.</l>
						<l n="29" num="4.5" lm="7" met="7">« <w n="29.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="29.2">n</w>'<w n="29.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="2">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="29.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="29.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.6" punct="pv:7">m<pgtc id="15" weight="0" schema="R"><rhyme label="c" id="15" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="30" num="4.6" lm="7" met="7"><w n="30.1">D<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w>-<w n="30.2" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg in">i</seg>l</w>, « <w n="30.3">su<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="5">ez</seg></w> <w n="30.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="30.5" punct="pe:7">p<pgtc id="16" weight="0" schema="R"><rhyme label="d" id="16" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pe ps">a</seg>s</rhyme></pgtc></w> !… »</l>
						<l n="31" num="4.7" lm="5"><space unit="char" quantity="4"></space><w n="31.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ld<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="31.2" punct="pe:5">pl<pgtc id="15" weight="0" schema="R"><rhyme label="c" id="15" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="32" num="4.8" lm="2"><space unit="char" quantity="10"></space><w n="32.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<pgtc id="16" weight="0" schema="R"><rhyme label="d" id="16" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="huitain" rhyme="ababcdcd">
						<l n="33" num="5.1" lm="7" met="7"><w n="33.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="33.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="33.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="33.5"><pgtc id="17" weight="0" schema="[R"><rhyme label="a" id="17" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></pgtc></w></l>
						<l n="34" num="5.2" lm="7" met="7"><w n="34.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="34.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="34.3">m<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="34.4">s</w>'<w n="34.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg><pgtc id="18" weight="2" schema="CR">t<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></rhyme></pgtc></w></l>
						<l n="35" num="5.3" lm="7" met="7"><w n="35.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="35.2">v<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="35.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="35.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="35.5">l<pgtc id="17" weight="0" schema="R"><rhyme label="a" id="17" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></pgtc></w></l>
						<l n="36" num="5.4" lm="7" met="7"><w n="36.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="36.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="2">om</seg></w> <w n="36.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="36.4" punct="pt:7">T<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg><pgtc id="18" weight="2" schema="CR">t<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pt">a</seg></rhyme></pgtc></w>.</l>
						<l n="37" num="5.5" lm="7" met="7"><w n="37.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="37.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="37.3">v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="3">e</seg>rs</w> <w n="37.4">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="37.5"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ffl<pgtc id="19" weight="0" schema="R"><rhyme label="c" id="19" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="38" num="5.6" lm="7" met="7"><w n="38.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="38.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="2">om</seg></w> <w n="38.3" punct="pe:4">s<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pe ps">é</seg></w> !… <w n="38.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rl<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="38.5" punct="pe:7">b<pgtc id="20" weight="0" schema="R"><rhyme label="d" id="20" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="7" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
						<l n="39" num="5.7" lm="5"><space unit="char" quantity="4"></space><w n="39.1">G<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ld<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="39.2" punct="pe:5">pl<pgtc id="19" weight="0" schema="R"><rhyme label="c" id="19" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="40" num="5.8" lm="2"><space unit="char" quantity="10"></space><w n="40.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<pgtc id="20" weight="0" schema="R"><rhyme label="d" id="20" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">Janvier 1871</date>
						</dateline>
					</closer>
				</div></body></text></TEI>