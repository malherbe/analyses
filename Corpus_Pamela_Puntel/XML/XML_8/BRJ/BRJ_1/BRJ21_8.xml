<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ21" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="8(abab)" er_moy="1.62" er_max="6" er_min="0" er_mode="0(8/16)" er_moy_et="2.23">
					<head type="number">XXI</head>
					<head type="main">PARIS</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="381" place="3">e</seg>il</w> <w n="1.3" punct="vg:5">br<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="1.6" punct="pe:8">j<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="2.2">g<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="2.3">r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t</w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="2.7" punct="pe:8"><pgtc id="2" weight="1" schema="GR">y<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">qu</w>'<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="3.5">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="5">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="3.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">am</seg>b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="422" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>s</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="4.7" punct="pe:8">c<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2" punct="vg:2">p<seg phoneme="œ" type="vs" value="1" rule="406" place="2" punct="vg">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="5.4">h<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ts</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="466" place="7">i</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d</w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="6.5" punct="pv:8">b<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg>rds</rhyme></pgtc></w> ;</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="7.3">ch<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="7.7" punct="vg:8">br<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="wa" type="vs" value="1" rule="419" place="1">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg>x</w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="8.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">in</seg>s</w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="8.5" punct="pe:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>rds</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="9.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" punct="vg">en</seg>d</w>, <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>x</w> <w n="9.5">j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rs</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7" punct="vg:8">f<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="10.2">cl<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="10.5" punct="pv:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">am</seg><pgtc id="6" weight="2" schema="CR">b<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pv">ou</seg>rs</rhyme></pgtc></w> ;</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="o" type="vs" value="1" rule="437" place="3">o</seg>t</w> <w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="11.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>v<seg phoneme="i" type="vs" value="1" rule="481" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">à</seg></w> <w n="11.6" punct="pv:8">b<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="12.2">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="12.6" punct="pt:8">f<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg><pgtc id="6" weight="2" schema="CR">b<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pt">ou</seg>rgs</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">Pl<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="13.5">s</w>'<w n="13.6"><seg phoneme="e" type="vs" value="1" rule="352" place="6">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">D</w>'<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="14.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg></w> <w n="14.4">qu</w>'<w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><pgtc id="8" weight="6" schema="[V[CR" part="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></pgtc></w> <w n="14.8" punct="pv:8"><pgtc id="8" weight="6" schema="[V[CR" part="2">ch<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="248" place="8" punct="pv">œu</seg>r</rhyme></pgtc></w> ;</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="15.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>ts</w> <w n="15.3">v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="3">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">b<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="15.7">b<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">L</w>'<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>r</w> <w n="16.3">v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="3">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">c<seg phoneme="œ" type="vs" value="1" rule="248" place="6">œu</seg>r</w> <w n="16.6"><pgtc id="8" weight="6" schema="[V[CR" part="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></pgtc></w> <w n="16.7" punct="pe:8"><pgtc id="8" weight="6" schema="[V[CR" part="2">c<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="248" place="8" punct="pe">œu</seg>r</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>x</w> <w n="17.2" punct="vg:3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">em</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="vg">a</seg>rts</w>, <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="17.4">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="17.5">s</w>'<w n="17.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>pp<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>nt</rhyme></pgtc></w></l>
						<l n="18" num="5.2" lm="8" met="8">« <w n="18.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="pe">i</seg>s</w> ! » <w n="18.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>s</w> <w n="18.3">c<seg phoneme="o" type="vs" value="1" rule="434" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.4">l<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>rs</w> <w n="18.5" punct="pv:8"><pgtc id="10" weight="2" schema="[CR" part="1">n<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="8" punct="pv">om</seg>s</rhyme></pgtc></w> ;</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ts</w> <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="19.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="19.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.6" punct="vg:8">m<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>nt</rhyme></pgtc></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="20.2">m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="20.5" punct="pe:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="10" weight="2" schema="CR" part="1">n<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pe">on</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="21.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="2">im</seg>pr<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.4" punct="vg:8">m<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>r<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="a" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="306" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="22.2">b<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="22.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>d</w> <w n="22.5" punct="vg:8">ch<pgtc id="12" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="vg">in</seg></rhyme></pgtc></w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="23.2" punct="vg:2">c<seg phoneme="o" type="vs" value="1" rule="317" place="2" punct="vg">au</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="23.4" punct="vg:5">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" punct="vg">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="23.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="23.6">tr<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="a" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="306" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="24.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="24.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>ts</w> <w n="24.4">d<pgtc id="12" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></pgtc></w> <w n="24.5" punct="pt:8"><pgtc id="12" weight="6" schema="V[CR" part="2">m<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="25.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="25.3">s<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s</w> <w n="25.4">s</w>'<w n="25.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rr<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="vg">E</seg>t</w>, <w n="26.2">v<seg phoneme="wa" type="vs" value="1" rule="439" place="2">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="26.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="26.4">d</w>'<w n="26.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>m<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg><pgtc id="14" weight="6" schema="CVR" part="1">t<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1" punct="dp:3">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="dp in">e</seg></w> : « <w n="27.2">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="27.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="27.5" punct="pi:8">f<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi ps">e</seg></rhyme></pgtc></w> ?… »</l>
						<l n="28" num="7.4" lm="8" met="8">— <w n="28.1">C</w>'<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="28.3"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="28.4" punct="pe:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>v<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg><pgtc id="14" weight="6" schema="CVR" part="1">t<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pe">on</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="8" type="quatrain" rhyme="abab">
						<l n="29" num="8.1" lm="8" met="8"><w n="29.1">C</w>'<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="29.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="29.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="29.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="29.6">s</w>'<w n="29.7" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>cr<pgtc id="15" weight="0" schema="R" part="1"><rhyme label="a" id="15" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="30" num="8.2" lm="8" met="8"><w n="30.1">C</w>'<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="30.3" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="422" place="2" punct="vg">oi</seg></w>, <w n="30.4" punct="vg:5">l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="5" punct="vg">é</seg></w>, <w n="30.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="30.6" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<pgtc id="16" weight="1" schema="GR" part="1">i<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="8" punct="pe">en</seg>s</rhyme></pgtc></w> !</l>
						<l n="31" num="8.3" lm="8" met="8"><w n="31.1">C</w>'<w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="31.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="31.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310" place="4">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="31.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="31.7" punct="pe:8">f<pgtc id="15" weight="0" schema="R" part="1"><rhyme label="a" id="15" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="32" num="8.4" lm="8" met="8"><w n="32.1">C</w>'<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="32.3" punct="tc:3">P<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="ti">i</seg>s</w>— <w n="32.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="32.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="32.6" punct="pe:8">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>ss<pgtc id="16" weight="1" schema="GR" part="1">i<rhyme label="b" id="16" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8" punct="pe">en</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Septembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>