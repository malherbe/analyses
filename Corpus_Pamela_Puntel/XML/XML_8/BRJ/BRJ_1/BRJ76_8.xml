<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ76" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="2[abab] 2[aa] 1[abba]" er_moy="0.88" er_max="2" er_min="0" er_mode="0(4/8)" er_moy_et="0.93">
					<head type="number">LXXVI</head>
					<head type="main">UN PÈRE</head>
					<lg n="1" type="regexp" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>'<w n="1.2" punct="vg:4">h<seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg><seg phoneme="i" type="vs" value="1" rule="476" place="3">ï</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="1.3" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="1.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>d</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>r</w> <w n="1.7">f<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>li<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="346" place="12">er</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="2.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>t</w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" mp="M">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="2.5">m<seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="10">e</seg>c</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="2.8" punct="pt:12">r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="466" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="2">e</seg>rs</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="P">à</seg></w> <w n="3.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" mp="M">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>s</w> <w n="3.5">m<seg phoneme="o" type="vs" value="1" rule="437" place="6" caesura="1">o</seg>ts</w><caesura></caesura> <w n="3.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>l</w> <w n="3.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="3.9" punct="pv:12">pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="346" place="12" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="4.2">qu</w>'<w n="4.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="2" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.5" punct="vg:6">m<seg phoneme="o" type="vs" value="1" rule="437" place="6" punct="vg" caesura="1">o</seg>t</w>,<caesura></caesura> <w n="4.6">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>t</w> <w n="4.9"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="10">e</seg>st</w> <w n="4.10" punct="pi:12">s<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg>bl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="466" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
					</lg>
					<lg n="2" type="regexp" rhyme="aaabababbaaa">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1" mp="C">Un</seg></w> <w n="5.2">b<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>s</w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>t</w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="5.5" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>ls</w>,<caesura></caesura> <w n="5.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="7" mp="M">im</seg>pr<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg></w> <w n="5.7" punct="vg:12">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="11" mp="M">o</seg>ld<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="vg">a</seg>t</rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="M">om</seg>pt<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>r</w> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="P">ou</seg>r</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.7" punct="pt:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="M">om</seg>b<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="pt">a</seg>t</rhyme></pgtc></w>.</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="7.2">c<seg phoneme="œ" type="vs" value="1" rule="248" place="2">œu</seg>r</w> <w n="7.3">n</w>'<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="3">e</seg>st</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.7">d<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="C">u</seg></w> <w n="7.8">st<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg><seg phoneme="i" type="vs" value="1" rule="476" place="9" mp="M">ï</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>sm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="M">an</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="a" id="4" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" mp="C">on</seg></w> <w n="8.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="8.6">m<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>t</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.8">s<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="8.9" punct="pv:12">m<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="12" punct="pv">o</seg>rt</rhyme></pgtc></w> ;</l>
						<l n="9" num="2.5" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="M">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="9.2">m<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="9.3" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6" punct="pt" caesura="1">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>.<caesura></caesura> <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="C">I</seg>l</w> <w n="9.5">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.6">s<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="9.7" punct="pt:12">b<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="M">ou</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="a" id="4" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="10" num="2.6" lm="12" met="6+6"><w n="10.1">V<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>j<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="3">oin</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="C">on</seg></w> <w n="10.4" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>ls</w>,<caesura></caesura> <w n="10.5">l</w>'<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="o" type="vs" value="1" rule="317" place="10" mp="C">au</seg></w> <w n="10.8">pl<seg phoneme="y" type="vs" value="1" rule="449" place="11">u</seg>s</w> <w n="10.9">f<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="12">o</seg>rt</rhyme></pgtc></w></l>
						<l n="11" num="2.7" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="11.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="4" punct="vg">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.4" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="188" place="5" punct="vg">e</seg>t</w>, <w n="11.5" punct="vg:6">c<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg" caesura="1">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="11.6" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="10" punct="vg">en</seg>t</w>, <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="352" place="11" mp="M">e</seg>ss<pgtc id="6" weight="1" schema="GR">u<rhyme label="a" id="6" gender="f" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="2.8" lm="12" met="6+6"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">f<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="12.4">l</w>'<w n="12.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="169" place="4" mp="M">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="12.6" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" punct="vg" mp="F">e</seg>s</w>, <w n="12.7" punct="vg:10"><seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>b<seg phoneme="y" type="vs" value="1" rule="449" place="10" punct="vg">u</seg>s</w>, <w n="12.8" punct="vg:12">b<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="M">ou</seg><pgtc id="7" weight="2" schema="CR">l<rhyme label="b" id="7" gender="m" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="12" punct="vg">e</seg>ts</rhyme></pgtc></w>,</l>
						<l n="13" num="2.9" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">É</seg>c<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg></w> <w n="13.3">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="6" caesura="1">oin</seg></w><caesura></caesura> <w n="13.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="188" place="9">e</seg>t</w> <w n="13.6">p<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>st<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg><pgtc id="7" weight="2" schema="CR">l<rhyme label="b" id="7" gender="m" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="12">e</seg>ts</rhyme></pgtc></w></l>
						<l n="14" num="2.10" lm="12" met="6+6"><w n="14.1">D</w>'<w n="14.2"><seg phoneme="y" type="vs" value="1" rule="452" place="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="14.5">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="14.6" punct="ps:8">s<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="ps">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9" mp="C">un</seg></w> <w n="14.8" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>pl<pgtc id="6" weight="1" schema="GR">u<rhyme label="a" id="6" gender="f" type="e" stanza="4"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						<l n="15" num="2.11" lm="12" met="6+6"><w n="15.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="15.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="15.4" punct="vg:6">h<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437" place="6" punct="vg" caesura="1">o</seg>s</w>,<caesura></caesura> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="15.6">pr<seg phoneme="o" type="vs" value="1" rule="414" place="8">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="15.7">l<seg phoneme="œ" type="vs" value="1" rule="406" place="10" mp="C">eu</seg>rs</w> <w n="15.8" punct="pv:12">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>r<pgtc id="8" weight="2" schema="CR">t<rhyme label="a" id="8" gender="m" type="a" stanza="5"><seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="pv">u</seg>s</rhyme></pgtc></w> ;</l>
						<l n="16" num="2.12" lm="12" met="6+6"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="16.2" punct="vg:4">pr<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="16.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>r</w> <w n="16.4" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="422" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="16.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="16.6">b<seg phoneme="u" type="vs" value="1" rule="424" place="8" mp="M">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>s</w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="16.8" punct="pe:12">Br<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg><pgtc id="8" weight="2" schema="CR">t<rhyme label="a" id="8" gender="m" type="e" stanza="5"><seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="pe">u</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>