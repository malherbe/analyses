<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ32" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(aabb)" er_moy="3.5" er_max="6" er_min="1" er_mode="1(1/2)" er_moy_et="2.5">
					<head type="number">XXXII</head>
					<head type="main">ARITHMÉTIQUE</head>
					<lg n="1" type="quatrain" rhyme="aabb">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">in</seg>q</w> <w n="1.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3" punct="vg:3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" punct="vg">un</seg></w>, <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="1.5">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6" caesura="1">en</seg>s</w><caesura></caesura> <w n="1.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>t</w> <w n="1.7" punct="pv:12">v<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="443" place="10" mp="M">o</seg><pgtc id="1" weight="6" schema="CVR">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3" punct="pt:4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4" punct="pt">un</seg></w>. <w n="2.4" punct="pv:6">b<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pv" caesura="1">a</seg>rds</w> ;<caesura></caesura> <w n="2.5">tr<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s</w> <w n="2.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7" punct="vg:9"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9" punct="vg">un</seg></w>, <w n="2.8" punct="pv:12">s<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg><pgtc id="1" weight="6" schema="CVR">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>x</w> <w n="3.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3" punct="vg:3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" punct="vg">un</seg></w>, <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="C">i</seg>ls</w> <w n="3.5">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="3.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="vg">ai</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="10">e</seg>c</w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="3.9" punct="pv:12">f<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="4.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3" punct="vg:3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" punct="vg">un</seg></w>, <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.5">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="4.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.7">s<seg phoneme="o" type="vs" value="1" rule="317" place="8">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.8">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="11" mp="C">un</seg></w> <w n="4.10" punct="pt:12">l<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Octobre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>