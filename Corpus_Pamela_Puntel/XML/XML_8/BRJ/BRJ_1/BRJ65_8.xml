<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ65" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="3[abba] 1[abab]" er_moy="1.88" er_max="10" er_min="0" er_mode="0(4/8)" er_moy_et="3.18">
					<head type="number">LXV</head>
					<head type="main">LES BISMARK</head>
					<lg n="1" type="regexp" rhyme="abbaabbaabbaabab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">dr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="1.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="1.6" punct="vg:8">f<pgtc id="1" weight="10" schema="VCVR"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="2.2">n<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3" punct="vg:5">tr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg></w> <w n="2.5" punct="vg:8">th<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ø" type="vs" value="1" rule="397" place="1">eu</seg>x</w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4" punct="ps:5">p<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="ps">i</seg>c</w>… <w n="3.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>d<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>d<pgtc id="1" weight="10" schema="VCVR"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">p<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="5.5" punct="vg:8">c<pgtc id="3" weight="1" schema="GR">u<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">T<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ls</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.5" punct="vg:8">s<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>d<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>c<pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="7.2" punct="vg:4">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="7.4" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="6">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>s<pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">in</seg>s</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="8.5" punct="pe:8">j<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>s<pgtc id="3" weight="1" schema="GR">u<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
						<l n="9" num="1.9" lm="8" met="8"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">m<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>t</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4" punct="vg:5">tr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="9.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="9.6" punct="vg:8">m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">en</seg></w> <w n="10.2">qu</w>'<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="10.4">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="10.5">d<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="10.8" punct="pt:8">m<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="11" num="1.11" lm="8" met="8"><w n="11.1">N</w>'<w n="11.2"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="11.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361" place="5">en</seg>s</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="11.7">l<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="1.12" lm="8" met="8"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="12.4">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="5">oin</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.6">l</w>'<w n="12.7" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>pl<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></rhyme></pgtc></w>.</l>
						<l n="13" num="1.13" lm="8" met="8"><w n="13.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg></w> <w n="13.3">b<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.5">l</w>'<w n="13.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">Au</seg>tr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="1.14" lm="8" met="8"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="14.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="14.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg></w> <w n="14.6">D<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="8" weight="2" schema="CR">m<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>rck</rhyme></pgtc></w></l>
						<l n="15" num="1.15" lm="8" met="8"><w n="15.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="15.2">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="15.4">j<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="15.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>s</w> <w n="15.7" punct="pv:8">r<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="16" num="1.16" lm="8" met="8"><w n="16.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="16.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="2">om</seg></w> <w n="16.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="16.4" punct="dp:5">t<seg phoneme="u" type="vs" value="1" rule="424" place="5" punct="dp">ou</seg>t</w> : <w n="16.5">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="16.6" punct="pt:8">B<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>s<pgtc id="8" weight="2" schema="CR">m<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg>rk</rhyme></pgtc></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>