<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ11" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="7[abab]" er_moy="1.71" er_max="6" er_min="0" er_mode="0(6/14)" er_moy_et="1.98">
					<head type="number">XI</head>
					<head type="main">LES BULLETINS DE VICTOIRE</head>
					<div type="section" n="1">
						<lg n="1" type="regexp" rhyme="abababababababababababab">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg></w>, <w n="1.2">B<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rtr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="1.4">R<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rt</w> <w n="1.5">M<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">f<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="2.5" punct="pv:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="6">im</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg><pgtc id="2" weight="2" schema="CR">d<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="8" punct="pv">en</seg>ts</rhyme></pgtc></w> ;</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>ls</w> <w n="3.2" punct="vg:3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>nt</w>, <w n="3.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="3.4">f<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6" punct="vg:8">gu<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">G<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ch<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>rs</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.5" punct="dp:8"><pgtc id="2" weight="2" schema="[CR">d<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="8" punct="dp">en</seg>ts</rhyme></pgtc></w> :</l>
							<l n="5" num="1.5" lm="8" met="8"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="5.2">p<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ck</w>-<w n="5.3">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ck<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="5.6" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ct<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>rs</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.3" punct="vg:8">b<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="vg">in</seg>s</rhyme></pgtc></w>,</l>
							<l n="7" num="1.7" lm="8" met="8"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2">br<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="7.4">f<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="7.6" punct="pt:8">gl<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
							<l n="8" num="1.8" lm="8" met="8"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="8.3">p<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="8.5" punct="pt:8">c<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="pt">in</seg>s</rhyme></pgtc></w>.</l>
							<l n="9" num="1.9" lm="8" met="8"><w n="9.1">Br<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2" punct="vg:3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="9.4">c<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>ps</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">cr<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="10" num="1.10" lm="8" met="8"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">c<seg phoneme="o" type="vs" value="1" rule="434" place="3">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="10.4" punct="pv:8">v<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pv">e</seg>ts</rhyme></pgtc></w> ;</l>
							<l n="11" num="1.11" lm="8" met="8"><w n="11.1">C</w>'<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="3">e</seg>c</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>rs</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.7">br<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>v<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="12" num="1.12" lm="8" met="8"><w n="12.1">Qu</w>'<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>ls</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="12.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="12.6" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>ffl<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" punct="pt">e</seg>ts</rhyme></pgtc></w>.</l>
							<l n="13" num="1.13" lm="8" met="8"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>ls</w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="13.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>rts</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7" punct="vg:8">j<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="wa" type="vs" value="1" rule="422" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="14" num="1.14" lm="8" met="8"><w n="14.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="14.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="14.4">c<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="14.5" punct="vg:8">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="7">oin</seg><pgtc id="8" weight="2" schema="CR">t<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="y" type="vs" value="1" rule="449" place="8" punct="vg">u</seg>s</rhyme></pgtc></w>,</l>
							<l n="15" num="1.15" lm="8" met="8"><w n="15.1">Tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>ts</w> <w n="15.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d</w> <w n="15.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="15.5" punct="vg:8">f<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>dr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="wa" type="vs" value="1" rule="422" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="16" num="1.16" lm="8" met="8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="16.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>ts</w> <w n="16.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>d</w> <w n="16.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ls</w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="16.6" punct="pe:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="8" weight="2" schema="CR">tt<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="y" type="vs" value="1" rule="449" place="8" punct="pe">u</seg>s</rhyme></pgtc></w> !</l>
							<l n="17" num="1.17" lm="8" met="8"><w n="17.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">s<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>t</w> <w n="17.5">l<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>r</w> <w n="17.6" punct="vg:8"><pgtc id="9" weight="2" schema="[CR">r<rhyme label="a" id="9" gender="f" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="18" num="1.18" lm="8" met="8"><w n="18.1">F<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>t</w>-<w n="18.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="18.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="18.5" punct="pi:6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="pi ps">e</seg></w> ?… <w n="18.6">F<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="18.7" punct="pe:8"><pgtc id="10" weight="2" schema="[CR">d<rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pe">on</seg>c</rhyme></pgtc></w> !</l>
							<l n="19" num="1.19" lm="8" met="8"><w n="19.1">L</w>'<w n="19.2"><seg phoneme="ø" type="vs" value="1" rule="404" place="1">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="19.4">tr<seg phoneme="o" type="vs" value="1" rule="432" place="5">o</seg>p</w> <w n="19.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6">l<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg><pgtc id="9" weight="2" schema="C[R" part="1">r</pgtc></w> <w n="19.7"><pgtc id="9" weight="2" schema="C[R" part="2"><rhyme label="a" id="9" gender="f" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="20" num="1.20" lm="8" met="8"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.5" punct="pt:8">d<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg><pgtc id="10" weight="2" schema="CR" part="1">d<rhyme label="b" id="10" gender="m" type="e" stanza="5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
							<l n="21" num="1.21" lm="8" met="8"><w n="21.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="21.2">bru<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>t</w> <w n="21.3">f<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg></w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="21.6" punct="vg:8">t<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg><pgtc id="11" weight="0" schema="R" part="1">nn<rhyme label="a" id="11" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="22" num="1.22" lm="8" met="8"><w n="22.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="22.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>du<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="22.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.4" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="12" weight="2" schema="CR" part="1">l<rhyme label="b" id="12" gender="m" type="a" stanza="6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
							<l n="23" num="1.23" lm="8" met="8"><w n="23.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="23.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">pr<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>d</w> <w n="23.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg></w> <w n="23.5">d</w>'<w n="23.6"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg><pgtc id="11" weight="0" schema="[R" part="1">n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></pgtc></w> <w n="23.7" punct="vg:8"><pgtc id="11" weight="0" schema="[R" part="2"><rhyme label="a" id="11" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="24" num="1.24" lm="8" met="8"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="24.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="24.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="306" place="4">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.5" punct="pe:8">B<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>r<pgtc id="12" weight="2" schema="CR" part="1">l<rhyme label="b" id="12" gender="m" type="e" stanza="6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="pe">in</seg></rhyme></pgtc></w> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1870">22 août 1870</date>
							</dateline>
						</closer>
					</div>
					<div type="section" n="2">
						<lg n="1" type="regexp" rhyme="abab">
							<l n="25" num="1.1" lm="8" met="8"><w n="25.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</w> ! <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="25.3" punct="vg:6">tr<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg">on</seg></w>, <w n="25.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="25.5" punct="vg:8">cr<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="a" stanza="7"><seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
							<l n="26" num="1.2" lm="8" met="8"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="26.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="26.5" punct="vg:8">ch<pgtc id="14" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="14" gender="m" type="a" stanza="7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="vg">in</seg></rhyme></pgtc></w>,</l>
							<l n="27" num="1.3" lm="8" met="8"><w n="27.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg>t</w> <w n="27.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="27.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="27.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="27.5">p<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="27.6" punct="pv:8">r<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="e" stanza="7"><seg phoneme="i" type="vs" value="1" rule="466" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
							<l n="28" num="1.4" lm="8" met="8"><w n="28.1" punct="pe:1">S<seg phoneme="wa" type="vs" value="1" rule="419" place="1" punct="pe ps">oi</seg>t</w> !… <w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="28.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>t</w> <w n="28.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="28.5" punct="pe:8">d<pgtc id="14" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="14" gender="m" type="e" stanza="7"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pe">ain</seg></rhyme></pgtc></w> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1870">8 novembre 1 870.</date>
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>