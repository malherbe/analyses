<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ33" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="6(abab)" er_moy="1.25" er_max="4" er_min="0" er_mode="0(5/12)" er_moy_et="1.23">
					<head type="number">XXXIII</head>
					<head type="main">LES CASQUES</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="452" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="1.4">n<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="1.5" punct="vg:10">m<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="1.6" punct="pe:12">v<seg phoneme="wa" type="vs" value="1" rule="419" place="11" mp="M">oi</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="12" punct="pe ps">à</seg></rhyme></pgtc></w> !…</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">c<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.6" punct="dp:6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="dp" caesura="1">on</seg>d</w> :<caesura></caesura> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.8">c<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="9">en</seg></w> <w n="2.10"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="10">e</seg>st</w> <w n="2.11">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="2.12"><pgtc id="2" weight="2" schema="[CR">s<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="3.2">h<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ts</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="3.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">on</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6" punct="vg" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="3.6">h<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>ts</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="3.8" punct="vg:12">g<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="4.2">m<seg phoneme="ɔ" type="vs" value="1" rule="442" place="2">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.4" punct="vg:6">pr<seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">è</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="4.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="C">a</seg></w> <w n="4.8" punct="pt:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="M">on</seg><pgtc id="2" weight="2" schema="CR">s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="5.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="2">oin</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" caesura="1">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="5.7">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="5.9" punct="pv:12">ci<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="P">à</seg></w> <w n="6.3" punct="vg:6">pr<seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>p<seg phoneme="o" type="vs" value="1" rule="437" place="6" punct="vg" caesura="1">o</seg>s</w>,<caesura></caesura> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="6.5">lu<seg phoneme="i" type="vs" value="1" rule="490" place="9" mp="C">i</seg></w> <w n="6.6">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rt</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="6.8" punct="pv:12">b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Su<seg phoneme="i" type="vs" value="1" rule="490" place="1" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="7.2">l</w>'<w n="7.3" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="434" place="3" mp="M">o</seg>cc<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.5">c<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="317" place="9" mp="M">au</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="12">eu</seg>x</rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="1" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="8.2">li<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="8.4" punct="ps:6">m<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="ps" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>…<caesura></caesura> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="8.7">t<seg phoneme="u" type="vs" value="1" rule="424" place="9">ou</seg>t</w> <w n="8.8"><seg phoneme="o" type="vs" value="1" rule="317" place="10">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="8.9" punct="pt:12">v<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="M">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3">en</seg></w> <w n="9.3">n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">aî</seg>t</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="9.5" punct="vg:6">m<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg" caesura="1">eu</seg>rt</w>,<caesura></caesura> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="9.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="9.8">c<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.10" punct="pv:12">s<pgtc id="5" weight="1" schema="GR">u<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="490" place="12" punct="pv">i</seg>t</rhyme></pgtc></w> ;</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">fr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="10.3">d<seg phoneme="ɔ" type="vs" value="1" rule="418" place="3">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg></w> <w n="10.7">c<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="10.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="C">on</seg></w> <w n="10.10" punct="pv:12"><pgtc id="6" weight="2" schema="[CR">p<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2" punct="vg:3">b<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="vg">oi</seg>s</w>, <w n="11.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="P">ou</seg>r</w> <w n="11.4" punct="vg:6">d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="11.7">c<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="11.9" punct="pv:12">n<pgtc id="5" weight="1" schema="GR">u<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="490" place="12" punct="pv">i</seg>t</rhyme></pgtc></w> ;</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="12.2">m<seg phoneme="wa" type="vs" value="1" rule="419" place="2" mp="M">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg></w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.5" punct="vg:6">si<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="6" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="12.8">d<seg phoneme="ø" type="vs" value="1" rule="397" place="9">eu</seg>x</w> <w n="12.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>t</w> <w n="12.10">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="12.11" punct="pt:12"><pgtc id="6" weight="2" schema="[CR">p<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">L</w>'<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="13.3"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="P">à</seg></w> <w n="13.5">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="13.6">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="13.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="13.9">c<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.10"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="13.11">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="C">on</seg></w> <w n="13.12" punct="pe:12"><pgtc id="7" weight="2" schema="[CR">c<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="248" place="12" punct="pe">œu</seg>r</rhyme></pgtc></w> !</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">c<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4" mp="C">i</seg></w> <w n="14.4">m<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7" mp="C">un</seg></w> <w n="14.6">b<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="9">er</seg></w> <w n="14.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="10" mp="P">u</seg>r</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="14.9" punct="pv:12">j<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">S</w>'<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.4">t</w>'<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" mp="M">em</seg>br<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="15.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" mp="P">an</seg>s</w> <w n="15.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="15.8">c<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="15.9" punct="vg:12">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="11" mp="M">ain</seg><pgtc id="7" weight="2" schema="CR">qu<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="12" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="16.2" punct="vg:3">Gr<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2" mp="M">e</seg>tch<seg phoneme="ɛ" type="vs" value="1" rule="24" place="3" punct="vg">e</seg>n</w>, <w n="16.3">t<seg phoneme="y" type="vs" value="1" rule="449" place="4" mp="C">u</seg></w> <w n="16.4">r<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="16.6">t<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="C">u</seg></w> <w n="16.7">lu<seg phoneme="i" type="vs" value="1" rule="490" place="9" mp="C">i</seg></w> <w n="16.8">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>s</w> <w n="16.9">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="16.10" punct="pe:12">m<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1">L</w>'<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="17.3">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="17.4">d</w>'<w n="17.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg" caesura="1">a</seg>t</w>,<caesura></caesura> <w n="17.6">v<seg phoneme="o" type="vs" value="1" rule="443" place="7" mp="M">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r</w> <w n="17.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="17.8" punct="vg:12">n<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>t<pgtc id="9" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1" punct="vg">i</seg></w>, <w n="18.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="P">ou</seg>r</w> <w n="18.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>r</w> <w n="18.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5" mp="C">e</seg>s</w> <w n="18.5" punct="vg:6">v<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>ls</w>,<caesura></caesura> <w n="18.6">m<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>t</w> <w n="18.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="18.8" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>rn<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="19.2">m<seg phoneme="y" type="vs" value="1" rule="444" place="2" mp="M">û</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="19.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" mp="M">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="19.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="19.5" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>ch<seg phoneme="i" type="vs" value="1" rule="466" place="9" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>t<pgtc id="9" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="12" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
						<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>s</w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="20.3">c<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="20.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="20.5">d<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="20.6"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg></w> <w n="20.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8">oin</seg>s</w> <w n="20.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="C">u</seg></w> <w n="20.9">m<seg phoneme="wa" type="vs" value="1" rule="439" place="10" mp="M">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="11">en</seg></w> <w n="20.10" punct="pe:12"><pgtc id="10" weight="0" schema="[R"><rhyme label="b" id="10" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="12">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="12" met="6+6"><w n="21.1" punct="vg:2">Gu<seg phoneme="i" type="vs" value="1" rule="484" place="1" mp="M">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317" place="2" punct="vg">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" punct="vg">in</seg></w>, <w n="21.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="21.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="21.5"><seg phoneme="o" type="vs" value="1" rule="434" place="8" mp="M">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ci<seg phoneme="e" type="vs" value="1" rule="346" place="10">er</seg>s</w> <w n="21.6" punct="vg:12">fr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11" mp="M">in</seg><pgtc id="11" weight="2" schema="CR">g<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg>ts</rhyme></pgtc></w>,</l>
						<l n="22" num="6.2" lm="12" met="6+6">(<w n="22.1">J</w>'<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg></w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="22.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="22.5">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="22.6">y<seg phoneme="ø" type="vs" value="1" rule="397" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="22.7">v<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg></w> <w n="22.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="22.9">t<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="314" place="10">eau</seg></w> <w n="22.10" punct="pf:12">f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="M">an</seg>t<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>),</l>
						<l n="23" num="6.3" lm="12" met="6+6"><w n="23.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="23.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="C">au</seg></w> <w n="23.4" punct="vg:6">th<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg" caesura="1">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>t</w> <w n="23.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9" mp="C">un</seg></w> <w n="23.7" punct="vg:10">fr<seg phoneme="a" type="vs" value="1" rule="339" place="10" punct="vg">a</seg>c</w>, <w n="23.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="23.9" punct="pv:12"><pgtc id="11" weight="2" schema="[CR">g<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pv">an</seg>ts</rhyme></pgtc></w> ;</l>
						<l n="24" num="6.4" lm="12" met="6+6"><w n="24.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="24.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>, <w n="24.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="24.4" punct="ps:6">m<seg phoneme="wa" type="vs" value="1" rule="422" place="6" punct="ps" caesura="1">oi</seg></w>…<caesura></caesura> <w n="24.5">m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s</w> <w n="24.6"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="C">i</seg>l</w> <w n="24.7"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>t</w> <w n="24.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="C">on</seg></w> <w n="24.9" punct="pe:12">c<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>