<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR24" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="4(abab)" er_moy="0.75" er_max="2" er_min="0" er_mode="0(5/8)" er_moy_et="0.97">
				<head type="main">UN CONCERT</head>
				<opener>
					<dateline>
						<date when="1871">Montmorency, avril 1871.</date>
					</dateline>
				</opener>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">AU</seg></w> <w n="1.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="2">oin</seg></w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="1.5">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="1.7">c<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>ps</w> <w n="1.8" punct="pt:12">pr<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="2.3">gr<seg phoneme="o" type="vs" value="1" rule="433" place="3">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>x</w> <w n="2.5">f<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="402" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="3.3" punct="vg:6">fr<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>qu<seg phoneme="a" type="vs" value="1" rule="364" place="5" mp="M">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="P">a</seg>r</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="9">en</seg>t</w> <w n="3.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438" place="11" mp="M">o</seg>r<pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="4.2">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>ts</w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="4.4" punct="pt:8">m<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="306" place="7">a</seg>ill<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="402" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="5.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>t</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="P">à</seg></w> <w n="5.5" punct="vg:6">P<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="5.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="M">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>s</w> <w n="5.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.8" punct="pt:12">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="M">an</seg><pgtc id="3" weight="2" schema="CR">ç<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12" punct="pt">ai</seg>s</rhyme></pgtc></w>.</l>
					<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>r</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="6.4" punct="vg:6">pl<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r</w> <w n="6.6" punct="vg:8">b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="7.2" punct="vg:3">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="M">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="3" punct="vg">en</seg>s</w>, <w n="7.3" punct="vg:5">r<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg" mp="F">e</seg>s</w>, <w n="7.4" punct="vg:6">gr<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="7.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="7.6">l<seg phoneme="œ" type="vs" value="1" rule="406" place="10" mp="C">eu</seg>r</w> <w n="7.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg>c<pgtc id="3" weight="2" schema="CR">c<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>s</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>nt</w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="8.4">v<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ls<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">rh<seg phoneme="i" type="vs" value="1" rule="492" place="2">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">s<seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="M">au</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="9.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>rs</w> <w n="9.7">f<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>r<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>s</rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="10.1">Qu</w>'<w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="10.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w>-<w n="10.5" punct="vg:5">b<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="vg">a</seg>s</w>, <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="10.7" punct="vg:8">k<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rm<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="351" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="11.3">fl<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>ge<seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" caesura="1">e</seg>ts</w><caesura></caesura> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="11.6">cr<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>s</rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="12.3" punct="vg:4">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>s</w>, <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="12.5">gr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="12.6" punct="pt:8">c<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" punct="pe">O</seg>h</w> ! <w n="13.2">l</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="402" place="3">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.4">d<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="C">on</seg></w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.8" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="7" weight="2" schema="CR">ss<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="12" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="e" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="14.7" punct="dp:8">d<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="15.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="3">oi</seg>r</w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="15.5"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="C">i</seg>l</w> <w n="15.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="15.7">f<seg phoneme="o" type="vs" value="1" rule="317" place="9" mp="M">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>t</w> <w n="15.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="C">u</seg></w> <w n="15.9" punct="vg:12"><pgtc id="7" weight="2" schema="[CR">s<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg>g</rhyme></pgtc></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="16.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="16.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="16.6" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>cr<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>