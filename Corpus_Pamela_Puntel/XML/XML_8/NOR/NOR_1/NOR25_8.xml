<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR25" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="9((aa))" er_moy="1.11" er_max="2" er_min="0" er_mode="2(4/9)" er_moy_et="0.87">
				<head type="main">LE DÉFILÉ</head>
				<opener>
					<dateline>
						<date when="1871">Versailles, mai 1871.</date>
					</dateline>
				</opener>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">E</seg></w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg></w> <w n="1.4">v<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="1.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="1.6" punct="vg:8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" punct="vg" mp="F">e</seg>s</w>, <w n="1.7" punct="vg:10">s<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="1.8" punct="vg:12">h<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">T<seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2" punct="vg:3">n<seg phoneme="y" type="vs" value="1" rule="456" place="3" punct="vg">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="2.4" punct="vg:6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="M">am</seg>b<seg phoneme="o" type="vs" value="1" rule="314" place="6" punct="vg" caesura="1">eau</seg>x</w>,<caesura></caesura> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="8" mp="M">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg>s</w> <w n="2.6">d<seg phoneme="ø" type="vs" value="1" rule="397" place="10">eu</seg>x</w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="P">a</seg>r</w> <w n="2.8" punct="pt:12"><pgtc id="1" weight="2" schema="[CR">d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>ts</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="3.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="Lc">i</seg>x</w>-<w n="3.4">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>pt</w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6" caesura="1">an</seg>s</w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="3.7">vi<seg phoneme="e" type="vs" value="1" rule="382" place="8" mp="M">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>rds</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="3.9" punct="pv:12">s<seg phoneme="wa" type="vs" value="1" rule="419" place="11" mp="M">oi</seg>x<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="4.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="6" caesura="1">om</seg></w><caesura></caesura> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="4.6">b<seg phoneme="u" type="vs" value="1" rule="427" place="8" mp="M">ou</seg>ill<seg phoneme="ɔ" type="vs" value="1" rule="418" place="9">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="4.8">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>rm<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="466" place="2" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.4">cr<seg phoneme="ø" type="vs" value="1" rule="402" place="5" mp="M">eu</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="5.7" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="443" place="9" mp="M">o</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg>x</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="352" place="2" mp="M">e</seg>ffl<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="6.4">v<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="6.7">l</w>’<w n="6.8" punct="vg:12"><seg phoneme="wa" type="vs" value="1" rule="419" place="9" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Où</seg></w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="7.3">c<seg phoneme="o" type="vs" value="1" rule="414" place="4">ô</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="P">à</seg></w> <w n="7.5" punct="vg:6">c<seg phoneme="o" type="vs" value="1" rule="414" place="6" punct="vg" caesura="1">ô</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="7.7">s</w>’<w n="7.8"><seg phoneme="y" type="vs" value="1" rule="452" place="8" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="7.9">p<seg phoneme="ø" type="vs" value="1" rule="397" place="11" mp="Lc">eu</seg>t</w>-<w n="7.10" punct="vg:12"><pgtc id="4" weight="0" schema="[R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="C">a</seg></w> <w n="8.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>b<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="8.4">m<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>rt</w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="8.6">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.7">qu<seg phoneme="i" type="vs" value="1" rule="490" place="10">i</seg></w> <w n="8.8">v<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg></w> <w n="8.9" punct="pt:12">n<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>ls</w> <w n="9.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="365" place="2">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="9.3" punct="vg:6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="4" mp="M">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="9.4" punct="vg:8">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="M">on</seg>t<seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg>x</w>, <w n="9.5">b<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="9.7" punct="pv:12"><pgtc id="5" weight="1" schema="GR">y<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="Fm">e</seg>s</w>-<w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg>s</w> <w n="10.3" punct="vg:6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>t</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg></w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="10.7" punct="vg:12">v<pgtc id="5" weight="1" schema="GR">i<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">Ti<seg phoneme="ɛ" type="vs" value="1" rule="365" place="1">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="11.2">h<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="11.4" punct="vg:6">t<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6" punct="vg" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="11.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="188" place="7" punct="vg">e</seg>t</w>, <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="8">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="11.7">d</w>’<w n="11.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="10" mp="M">im</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg>d<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="12.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" mp="M">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="8">e</seg>c</w> <w n="12.5" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="13.2">f<seg phoneme="a" type="vs" value="1" rule="192" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="13.3" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg>x</w> <w n="13.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>rds</w> <w n="13.6" punct="vg:12">h<seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>b<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg><pgtc id="7" weight="2" schema="CR">t<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="12" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M">ou</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="365" place="2">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="14.3" punct="vg:6">bl<seg phoneme="e" type="vs" value="1" rule="352" place="5" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="14.4">r<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="M">u</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="9">en</seg>t</w> <w n="14.5">c<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>h<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg><pgtc id="7" weight="2" schema="CR">t<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg>s</rhyme></pgtc></w></l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" mp="C">un</seg></w> <w n="15.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d</w> <w n="15.4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="Lc">a</seg>r</w>-<w n="15.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="Lc">à</seg></w>-<w n="15.6" punct="vg:6">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg>cs</w>,<caesura></caesura> <w n="15.7">qu</w>’<w n="15.8"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="15.10" punct="pt:12">pr<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg><pgtc id="8" weight="2" schema="CR">t<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="16.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="16.3" punct="vg:6">c<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439" place="5" mp="M">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="362" place="6" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="16.4">v<seg phoneme="wa" type="vs" value="1" rule="439" place="7" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="397" place="10">eu</seg>x</w> <w n="16.7" punct="vg:12">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="11" mp="M">o</seg>r<pgtc id="8" weight="2" schema="CR">t<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">S</w>’<w n="17.2" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1" mp="M">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="17.4">tr<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="17.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="17.6">r<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="409" place="9">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="17.8">p<seg phoneme="a" type="vs" value="1" rule="339" place="11">a</seg>rt</w> <w n="17.9" punct="dp:12">l<pgtc id="9" weight="1" schema="GR">u<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="490" place="12" punct="dp">i</seg></rhyme></pgtc></w> :</l>
					<l n="18" num="1.18" lm="12" met="6+6">« <w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="18.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="3">à</seg></w> <w n="18.3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="18.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="7" mp="C">o</seg>s</w> <w n="18.5">t<seg phoneme="i" type="vs" value="1" rule="492" place="8" mp="M">y</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9">an</seg>s</w> <w n="18.6">d</w>’<w n="18.7"><seg phoneme="o" type="vs" value="1" rule="317" place="10" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="Lc">ou</seg>rd</w>’<w n="18.8" punct="pe:12">h<pgtc id="9" weight="1" schema="GR">u<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="490" place="12" punct="pe">i</seg></rhyme></pgtc></w> ! »</l>
				</lg>
			</div></body></text></TEI>