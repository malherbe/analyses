<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRK2" modus="cm" lm_max="10" metProfile="5+5" form="sonnet classique, prototype 1" schema="abba abba ccd eed" er_moy="0.57" er_max="2" er_min="0" er_mode="0(5/7)" er_moy_et="0.9">
				<head type="main">ÉCRASONS L’IMFAME</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="C">u</seg></w> <w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="1.4" punct="vg:5">tr<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="6">en</seg></w> <w n="1.6">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="1.7">j<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>rs</w> <w n="1.8">m<seg phoneme="o" type="vs" value="1" rule="317" place="9" mp="M">au</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>s</rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Où</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3" punct="vg:3">Dr<seg phoneme="wa" type="vs" value="1" rule="419" place="3" punct="vg">oi</seg>t</w>, <w n="2.4" punct="vg:5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="4" mp="M">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="449" place="5" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="2.5">g<seg phoneme="i" type="vs" value="1" rule="467" place="6">î</seg>t</w> <w n="2.6">bl<seg phoneme="e" type="vs" value="1" rule="352" place="7" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="P">a</seg>r</w> <w n="2.8" punct="vg:10"><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="3.2">f<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w> <w n="3.3" punct="vg:3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" punct="vg">e</seg></w>, <w n="3.4">s</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="6" mp="C">u</seg></w> <w n="3.7">cr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="3.9" punct="vg:10">V<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9" mp="M">o</seg>l<pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1" punct="vg:2">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="2" punct="vg">un</seg></w>, <w n="4.2" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="dp in">in</seg></w> : « <w n="4.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="4.6" punct="pe:10">h<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10" punct="pe">ai</seg>s</rhyme></pgtc></w> ! »</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="5.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="3">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" mp="M">in</seg>j<seg phoneme="y" type="vs" value="1" rule="449" place="5" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" mp="M">ai</seg>gu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="5.7" punct="vg:10">tr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10" punct="vg">ai</seg>ts</rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg></w> <w n="6.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="C">u</seg></w> <w n="6.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="6.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5" caesura="1">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="6.6">s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg></w> <w n="6.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8">en</seg></w> <w n="6.8">n</w>’<w n="6.9" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>l<pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="7.2">j<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.3" punct="vg:5"><seg phoneme="o" type="vs" value="1" rule="434" place="3" mp="M">o</seg>ppr<seg phoneme="i" type="vs" value="1" rule="466" place="4" mp="M">i</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="5" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" mp="C">on</seg></w> <w n="7.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-6" place="7">e</seg>r</w> <w n="7.6" punct="dp:10">c<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>c<pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">É</seg>cr<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" mp="M">In</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="317" place="6" mp="C">au</seg></w> <w n="8.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="C">u</seg></w> <w n="8.7" punct="pe:10">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="10" punct="pe">ai</seg>s</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">M<seg phoneme="e" type="vs" value="1" rule="352" place="1" mp="M">e</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="9.3">f<seg phoneme="ɛ" type="vs" value="1" rule="63" place="4">e</seg>r</w> <w n="9.4">ch<seg phoneme="o" type="vs" value="1" rule="317" place="5" caesura="1">au</seg>d</w><caesura></caesura> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6" mp="P">an</seg>s</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="9.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg></w> <w n="9.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="C">u</seg></w> <w n="9.9" punct="pt:10">s<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">B<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg>x</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="10.3" punct="vg:5">g<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="M">ou</seg>j<seg phoneme="a" type="vs" value="1" rule="339" place="5" punct="vg" caesura="1">a</seg>ts</w>,<caesura></caesura> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="7">en</seg></w> <w n="10.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="M">u</seg>rn<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1">H<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rs</w> <w n="11.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="C">u</seg></w> <w n="11.3">g<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="466" place="4" mp="M">i</seg>mm<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="u" type="vs" value="1" rule="425" place="6">où</seg></w> <w n="11.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>lli<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w> <w n="11.8" punct="pt:10">ch<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="10" met="5+5">— <w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="12.4">m<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="5" caesura="1">i</seg>s</w><caesura></caesura> <w n="12.5">j</w>’<w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg></w> <w n="12.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">em</seg>p<seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></w> <w n="12.8">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="12.9" punct="dp:10">r<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="466" place="10">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="13" num="4.2" lm="10" met="5+5"><w n="13.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="13.2">c<seg phoneme="œ" type="vs" value="1" rule="248" place="2">œu</seg>r</w> <w n="13.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="3">ein</seg></w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424" place="5" caesura="1">ou</seg>r</w><caesura></caesura> <w n="13.6">v<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>t</w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="13.8">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>rt</w> <w n="13.9">d<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="C">u</seg></w> <w n="13.10" punct="pe:10">cr<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="466" place="10">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="14" num="4.3" lm="10" met="5+5"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="14.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="14.4">c<seg phoneme="o" type="vs" value="1" rule="443" place="4" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="6">e</seg>st</w> <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7" mp="C">un</seg></w> <w n="14.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</w> <w n="14.8">d</w>’<w n="14.9" punct="pe:10"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>sp<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="10" punct="pe">oi</seg>r</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>