
 ▪	François COPPÉE [COP]

 	Liste des recueils de poésies
 	─────────────────────────────
	▫	FAIS CE QUE DOIS - ÉPISODE DRAMATIQUE EN UN ACTE, EN VERS	1871 [COP_10]
	▫	ÉCRIT PENDANT LE SIÈGE - édition partielle du recueil : LES HUMBLES (1891)	1870 [COP_9]
