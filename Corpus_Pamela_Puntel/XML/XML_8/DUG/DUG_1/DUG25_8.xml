<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG25" modus="cp" lm_max="12" metProfile="8, 6+6" form="strophe unique" schema="1(ababcdcd)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(4/4)" er_moy_et="0.0">
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="vg">on</seg>c</w>, <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="C">i</seg>l</w> <w n="1.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.4">f<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>t</w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="C">u</seg></w> <w n="1.6" punct="vg:6">bru<seg phoneme="i" type="vs" value="1" rule="490" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="1.7" punct="vg:8">P<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>s</w>, <w n="1.8">d<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="C">u</seg></w> <w n="1.9">bru<seg phoneme="i" type="vs" value="1" rule="490" place="10">i</seg>t</w> <w n="1.10">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>d</w> <w n="1.11" punct="vg:12">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="C">u</seg></w> <w n="2.2">bru<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>t</w> <w n="2.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="M">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="C">u</seg></w> <w n="2.5">bru<seg phoneme="i" type="vs" value="1" rule="490" place="8">i</seg>t</w> <w n="2.6" punct="pe:10">st<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="M">u</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="10" punct="pe ps">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="2.7" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="12" punct="vg">in</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">C</w>'<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="C">u</seg></w> <w n="3.4">bru<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg>t</w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="3.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="3.9">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.10">m<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>r</w> <w n="3.11" punct="pe:12">s<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg>pr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>d</w> <w n="4.3">t<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="C">u</seg></w> <w n="4.4">n</w>'<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>ds</w> <w n="4.6">pl<seg phoneme="y" type="vs" value="1" rule="449" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.8" punct="vg:9">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" punct="vg">on</seg></w>, <w n="4.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="4.10" punct="vg:12">t<seg phoneme="ɔ" type="vs" value="1" rule="438" place="11" mp="M">o</seg>cs<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="12" punct="vg">in</seg></rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="4">en</seg></w> <w n="5.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5" mp="M">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="5.5">t</w>'<w n="5.6" punct="vg:9"><seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="M">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="5.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="5.8">d<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>s<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="6.6" punct="vg:8">c<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="vg">ou</seg>p</rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="7.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">f<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="P">à</seg></w> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>t</w> <w n="7.6">pr<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>x</w><caesura></caesura> <w n="7.7">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="7" mp="C">e</seg>t</w> <w n="7.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="9" mp="M">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>l</w> <w n="7.9">v<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>c<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Qu</w>'<w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">in</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>t<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="8.3">m<seg phoneme="œ" type="vs" value="1" rule="150" place="4">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="5">eu</seg>r</w> <w n="8.4" punct="pe:8">P<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>sd<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="8" punct="pe ps">ou</seg>p</rhyme></pgtc></w> !…</l>
				</lg>
			</div></body></text></TEI>