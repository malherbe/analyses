<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">PENDANT L’ORAGE</title>
				<title type="sub_1">POÈMES NATIONAUX ET HISTORIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="PDE">
					<name>
						<forename>Joseph</forename>
						<surname>POISLE-DESGRANGES</surname>
					</name>
					<date from="1823" to="1879">1823-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>809 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">PDE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>PENDANT L’ORAGE : POÈMES NATIONAUX ET HISTORIQUES</title>
						<author>JOSEPH POISLE-DESGRANGES</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LEMERRE</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="PDE2">
				<head type="main">VENGEANCE !</head>
				<head type="sub_1">A L’ARMÉE FRANÇAISE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="1.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> ! <w n="1.4">c</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.7">n</w>’<w n="1.8"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="1.9">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="1.10">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.2">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="2.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> !</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="3.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.3">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="4.6">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>…</l>
					<l n="5" num="1.5"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="5.2">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="5.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">t<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.6">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> :</l>
					<l n="7" num="1.7"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.2">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.5"><seg phoneme="y" type="vs" value="1" rule="449">u</seg>hl<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w></l>
					<l n="8" num="1.8"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="8.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>ts</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="12"></space><w n="9.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">EN</seg>GE<seg phoneme="ɑ̃" type="vs" value="1" rule="310">AN</seg>C<seg phoneme="ə" type="ef" value="1" rule="e-5">E</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="10" num="3.1"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="11" num="3.2"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> <w n="11.4">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="3.3"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.6">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="13" num="3.4"><w n="13.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="13.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> : — <w n="13.6">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">EN</seg>GE<seg phoneme="ɑ̃" type="vs" value="1" rule="310">AN</seg>C<seg phoneme="ə" type="ef" value="1" rule="e-5">E</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="14" num="4.1"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="14.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="14.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.4">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="14.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="15" num="4.2"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="15.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> :</l>
					<l n="16" num="4.3"><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="16.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.6">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="17" num="4.4"><w n="17.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="17.3">pr<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="17.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="17.6">cr<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !…</l>
					<l n="18" num="4.5"><w n="18.1">L<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="18.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="18.3">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="18.5">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="19" num="4.6"><w n="19.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="19.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rts</w>,</l>
					<l n="20" num="4.7"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="383">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="20.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rts</w></l>
					<l n="21" num="4.8"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="21.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="21.3">bl<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.5">l</w>’<w n="21.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="22" num="5.1"><space unit="char" quantity="12"></space><w n="22.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">EN</seg>GE<seg phoneme="ɑ̃" type="vs" value="1" rule="310">AN</seg>C<seg phoneme="ə" type="ef" value="1" rule="e-5">E</seg></w> !</l>
				</lg>
				<lg n="6">
					<l n="23" num="6.1"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="23.5">d</w>’<w n="23.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="24" num="6.2"><w n="24.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> <w n="24.4">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="25" num="6.3"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="25.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="25.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.6">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="26" num="6.4"><w n="26.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="26.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="26.4">l</w>’<w n="26.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> : — <w n="26.6">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">EN</seg>GE<seg phoneme="ɑ̃" type="vs" value="1" rule="310">AN</seg>C<seg phoneme="ə" type="ef" value="1" rule="e-5">E</seg></w> !</l>
				</lg>
				<lg n="7">
					<l n="27" num="7.1"><w n="27.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.2">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="27.3">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>, <w n="27.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="27.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="27.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="27.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="28" num="7.2"><w n="28.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="28.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.5">g<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="28.6">d</w>’<w n="28.7"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343">ue</seg>il</w> !…</l>
					<l n="29" num="7.3"><w n="29.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.2">Str<seg phoneme="a" type="vs" value="1" rule="339">a</seg>sb<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w> <w n="29.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="29.4">br<seg phoneme="y" type="vs" value="1" rule="444">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="29.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="29.6">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="30" num="7.4"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="30.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>th<seg phoneme="e" type="vs" value="1" rule="408">é</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="30.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="30.6">d<seg phoneme="œ" type="vs" value="1" rule="405">eu</seg>il</w> !</l>
					<l n="31" num="7.5"><w n="31.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="31.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.4">l</w>’<w n="31.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="32" num="7.6"><w n="32.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="32.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="32.4">n<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>rs</w> <w n="32.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> :</l>
					<l n="33" num="7.7"><w n="33.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="33.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="33.3">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="33.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="33.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="33.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w></l>
					<l n="34" num="7.8"><w n="34.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="34.2">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w> <w n="34.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="34.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="34.5">l</w>’<w n="34.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<lg n="8">
					<l n="35" num="8.1"><space unit="char" quantity="12"></space><w n="35.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">EN</seg>GE<seg phoneme="ɑ̃" type="vs" value="1" rule="310">AN</seg>C<seg phoneme="ə" type="ef" value="1" rule="e-5">E</seg></w> !</l>
				</lg>
				<lg n="9">
					<l n="36" num="9.1"><w n="36.1">C</w>’<w n="36.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="36.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.4">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="36.5">d</w>’<w n="36.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="37" num="9.2"><w n="37.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.2">l</w>’<w n="37.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> <w n="37.4">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="37.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="37.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="38" num="9.3"><w n="38.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="38.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.3">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="38.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="38.6">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="39" num="9.4"><w n="39.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="39.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="39.4">l</w>’<w n="39.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg></w> : — <w n="39.6">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">EN</seg>GE<seg phoneme="ɑ̃" type="vs" value="1" rule="310">AN</seg>C<seg phoneme="ə" type="ef" value="1" rule="e-5">E</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">3 septembre 1870</date>
					</dateline>
				</closer>
			</div></body></text></TEI>