<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">DEVANT L'ENNEMI</title>
				<title type="sub">Poémes publiés dans la REVUE DES DEUX MONDES (1870)</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>124 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRB_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DEVANT L'ENNEMI</title>
						<author>AUGUSTE BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k870291</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>REVUE DES DEUX MONDES</title>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>REVUE DES DEUX MONDES</publisher>
									<date when="1870">1870</date>
								</imprint>
								<biblScope unit="tome">89</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEVANT L'ENNEMI</head><div type="poem" key="BRB54">
					<head type="main">LE FILS DES HUNS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="1.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="1.4"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="1.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>, <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="1.7">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="1.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="1.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">L<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="2.2"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="2.3">r<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="2.4">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="2.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>str<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ts</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.3">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="3.4">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.6">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="4.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.5">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Str<seg phoneme="a" type="vs" value="1" rule="339">a</seg>sb<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.3">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="5.4">cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> : — <w n="5.5">L<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="5.6">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.8">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="6.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w>, <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.5">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="6.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="6.7">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rps</w> <w n="6.8">pl<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="7.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="7.7">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.9">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="7.10">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ! —</l>
						<l n="8" num="2.4"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="8.2">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rds</w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="8.5">h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="8.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="8.7">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="9.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> : — <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rgn<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="4"></space><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.2">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rs</w> <w n="10.3">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">l</w>'<w n="10.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w>,</l>
						<l n="11" num="3.3"><w n="11.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.2">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>th<seg phoneme="e" type="vs" value="1" rule="408">é</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="11.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rc<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="4"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="12.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.4">fl<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="12.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> ! —</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">cr<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="13.4">W<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="13.5">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.7">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.2"><space unit="char" quantity="4"></space><w n="14.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.2">m<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> : — <w n="14.4">P<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w>, <w n="14.5">c</w>'<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.9">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w></l>
						<l n="15" num="4.3"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">j</w>'<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="15.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="15.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.9">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><space unit="char" quantity="4"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="16.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.3">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.4">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="240">e</seg>ds</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.6">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.7">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ! —</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="17.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="17.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.6">m<seg phoneme="a" type="vs" value="1" rule="342">a</seg>n<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="4"></space><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="18.4">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="18.5">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="18.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>, <w n="19.3">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="19.4">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>sp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="19.5">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>th<seg phoneme="e" type="vs" value="1" rule="408">é</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="4"></space><w n="20.1">J<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ds</w> <w n="20.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="20.7">n<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>rs</w> <w n="20.8">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="21.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.5">t<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="21.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="21.7">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="21.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.9">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.10">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="6.2"><space unit="char" quantity="4"></space><w n="22.1">N</w>'<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="22.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-42">e</seg></w>, <w n="22.4">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="22.5">c</w>'<w n="22.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="22.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="22.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w></l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="23.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="23.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="23.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.7">l</w>'<w n="23.8"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ls<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="6.4"><space unit="char" quantity="4"></space><w n="24.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="24.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="24.3"><seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="24.4">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd</w> <w n="24.5">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="24.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">fl<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">H<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> ! <w n="25.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="25.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="25.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="25.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="25.6">s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.7">qu</w>'<w n="25.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="25.9">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="26" num="7.2"><space unit="char" quantity="4"></space><w n="26.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>spr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="26.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="26.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="26.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="26.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="26.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="26.7">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>,</l>
						<l n="27" num="7.3"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="27.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="27.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="27.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="27.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.7">sc<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="377">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="28" num="7.4"><space unit="char" quantity="4"></space><w n="28.1">L</w>'<w n="28.2"><seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="28.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="28.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.6">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.7">d</w>'<w n="28.8"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>tt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</l>
					</lg>
				</div></body></text></TEI>