<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS D’EXIL</title>
				<title type="medium">Édition électronique</title>
				<author key="AME">
					<name>
						<forename>Ernest</forename>
						<surname>AMELINE</surname>
					</name>
					<date from="1825" to="1893">1825-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>504 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">AME_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>CHANTS D’EXIL. SOUVENIRS ARTISTIQUES</title>
						<author>Ernest Ameline</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>DENTU</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
				<p>La seconde partie "Souvenirs artistiques" n'est pas incluse dans cette édition.</p>				
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-12-10" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="AME6">
				<head type="main">A MON AMIE</head>
				<head type="form">ÉLÉGIE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="1.5">qu</w>’<w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.7">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="3.5">tr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.3">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.6">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1">N<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="5.2">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w> <w n="5.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.6">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>,</l>
					<l n="6" num="1.6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.2">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.6">ch<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.5">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> :</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="8.2">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> <w n="8.3">j<seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.5">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="9.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w>, <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="9.4">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="?" type="va" value="1" rule="33">er</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>,</l>
					<l n="10" num="2.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>, <w n="10.2">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="10.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="2.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="11.2">qu</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="11.4">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.6">m<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w> <w n="11.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w></l>
					<l n="12" num="2.4"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="12.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="12.4">j<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.6">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="13" num="2.5"><w n="13.1">S<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="13.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w></l>
					<l n="14" num="2.6"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="15" num="2.7"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.2"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="15.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.5"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>, <w n="15.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> :</l>
					<l n="16" num="2.8"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="16.2">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> <w n="16.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="16.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="17.2">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w>, <w n="17.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="17.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w>,</l>
					<l n="18" num="3.2"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="18.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="19" num="3.3"><w n="19.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="19.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="19.4">t<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>, <w n="19.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ts</w> !</l>
					<l n="20" num="3.4"><w n="20.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="20.3">f<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="20.4">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="20.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="21" num="3.5"><w n="21.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="21.3">j</w>’<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="21.5">p<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>…</l>
					<l n="22" num="3.6"><w n="22.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="22.2">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="22.3">s</w>’<w n="22.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="22.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="22.7">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="23" num="3.7"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="23.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="23.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> :</l>
					<l n="24" num="3.8"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="24.2">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> <w n="24.3">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="24.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.5">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">V<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="25.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">j</w>’<w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="25.5">qu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="25.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="25.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> !</l>
					<l n="26" num="4.2"><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">n</w>’<w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="26.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>… <w n="26.5">j</w>’<w n="26.6"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="26.7">v<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="26.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="26.9">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="27" num="4.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="27.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="27.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="27.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w></l>
					<l n="28" num="4.4"><w n="28.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.3">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="28.4">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.5">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="28.6">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="29" num="4.5"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="29.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="29.3">s</w>’<w n="29.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>,</l>
					<l n="30" num="4.6"><w n="30.1">Gu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.4">tr<seg phoneme="o" type="vs" value="1" rule="432">o</seg>p</w> <w n="30.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.6">v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="31" num="4.7"><w n="31.1">R<seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="31.2">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> !… <w n="31.3">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="31.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="31.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>,</l>
					<l n="32" num="4.8"><w n="32.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="32.2">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>s</w> <w n="32.3">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="32.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.5">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>