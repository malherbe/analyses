<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA PATRIE !</title>
				<title type="medium">Édition électronique</title>
				<author key="TST">
					<name>
						<forename>Tyrtée</forename>
						<surname>TASTET</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>392 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">TST_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA PATRIE !</title>
						<author>TYRTÉE TASTET</author>
						<imprint>
							<pubPlace>NANTES</pubPlace>
							<publisher>LIBRAIRIE MOREL</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="TST6">
				<head type="main">RÉPONSE. LE TALION</head>
				<head type="sub">LA FRANCE A LA PRUSSE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.2">p<seg phoneme="o" type="vs" value="1" rule="443">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.4">r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="1.5">s<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> ; <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="310">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.9">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>'<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.5">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="2.6">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">gl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.9">s</w>'<w n="2.10"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="3" num="1.3"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.2">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="3.4">l</w>'<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="3.6">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="3.9"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="3.10">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="4.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.7">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.8">m<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="496">Y</seg></w> <w n="5.2">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="5.3">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.6">t<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="6.3">Rh<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.5">l</w>'<w n="6.6"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>d<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.7">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="6.8">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.10">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">M<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>, <w n="7.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="7.3">t<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ! <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="7.7">c</w>'<w n="7.8"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="7.9">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.10">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.11">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.12">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="2.2"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.2">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="8.5">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.8">l<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="8.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="8.10">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="9" num="2.3"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.4">l</w>'<w n="9.5">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="9.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="9.7">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ls</w> <w n="9.8">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="9.9">d</w>'<w n="9.10"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>x<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
					<l n="10" num="2.4"><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>pt<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="10.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>, <w n="10.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.5">d<seg phoneme="ɥ" type="sc" value="0" rule="457">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="10.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="11" num="2.5"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="11.2">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">l</w>'<w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="11.7">r<seg phoneme="a" type="vs" value="1" rule="339">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.10">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="2.6"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="12.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.6">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="12.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="13.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg><seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="13.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rtr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.9">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>x<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="14" num="3.2"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.3">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ts</w> <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="14.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.8">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="3.3"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="15.6">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="15.7"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rc<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ;</l>
					<l n="16" num="3.4"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="16.2">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.4">B<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rl<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>, <w n="16.5">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="16.6">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="16.7">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="17" num="3.5"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="17.2">qu</w>'<w n="17.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="17.4">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="17.6">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="17.7"><seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="18" num="3.6"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="18.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="18.3">C<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="18.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.8">m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="18.9">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.</l>
				</lg>
				<closer>
					<signed>TYRTÉE.</signed>
					<note type="footnote" id="">A Dieu ne plaise que l'atrocité soit à l'ordre du jour entre les deux armées. Mais il peut n'être pas inutile de montrer à ces bêtes féroces qu'au besoin on leur appliquera résolument la loi du talion.</note>
				</closer>
			</div></body></text></TEI>