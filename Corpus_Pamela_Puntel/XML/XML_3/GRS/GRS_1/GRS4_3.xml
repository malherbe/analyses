<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'ANNÉE MAUDITE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="GRS">
					<name>
						<forename>Charles</forename>
						<surname>GRANDSARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2146 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'ANNÉE MAUDITE 1870-1871</title>
						<author>Charles Grandsard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61285325.r=GRANDSARD%20L%27ANN%C3%89E%20MAUDITE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'ANNÉE MAUDITE 1870-1871</title>
								<author>Charles Grandsard</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE DU PETIT JOURNAL</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRS4">
				<head type="main">L'AGONIE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="1.2">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="1.4">l</w>'<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> !</l>
				</lg>
				<lg n="2">
					<l n="2" num="2.1"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="2.2">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.4">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="2.5">d</w>'<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="3" num="2.2"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="3.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.4">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="240">e</seg>ds</w> <w n="3.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
					<l n="4" num="2.3"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.5">P<seg phoneme="i" type="vs" value="1" rule="492">y</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="5" num="2.4"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.2">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="5.6">N<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd</w> <w n="5.7">g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>.</l>
					<l n="6" num="2.5"><w n="6.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.2">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c</w> <w n="6.3">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="6.6">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="7" num="2.6"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="425">Où</seg></w> <w n="7.2">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="7.3">l</w>'<w n="7.4"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> <w n="7.5">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.7"><w n="8.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.2">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c</w> <w n="8.3">g<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="8.5">fl<seg phoneme="o" type="vs" value="1" rule="437">o</seg>ts</w> <w n="8.6">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rts</w> <w n="8.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="8.8">Rh<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ;</l>
					<l n="9" num="2.8"><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.2">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w>, <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="9.4">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="9.5">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ts</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.8">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="10" num="2.9"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="10.5">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="11" num="2.10"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="11.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="11.4">d</w>'<w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="12" num="3.1"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="12.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="12.5">r<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="13" num="3.2"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.3">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.4">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="13.5">j<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w></l>
					<l n="14" num="3.3"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="14.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.6">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="15" num="3.4"><w n="15.1">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ff<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="15.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="15.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>.</l>
					<l n="16" num="3.5"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="16.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="16.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="16.5">d</w>'<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="17" num="3.6"><w n="17.1">R<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="17.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="17.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>st<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="3.7"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="18.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.3">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="18.5">s</w>'<w n="18.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w>,</l>
					<l n="19" num="3.8"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="19.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="19.4">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="19.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="20" num="3.9"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.2">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="20.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.5">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="21" num="3.10"><w n="21.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="21.3">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w> !</l>
				</lg>
				<lg n="4">
					<l n="22" num="4.1"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="22.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="22.3">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.6">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="23" num="4.2"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="23.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>,</l>
					<l n="24" num="4.3"><w n="24.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="24.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="24.3">g<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="24.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.5">p<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="25" num="4.4"><w n="25.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="25.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.3">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="25.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="25.5">f<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>.</l>
					<l n="26" num="4.5"><w n="26.1">S<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="26.4">f<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="27" num="4.6"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="27.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.6">t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="28" num="4.7"><w n="28.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.3">r<seg phoneme="a" type="vs" value="1" rule="339">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="28.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
					<l n="29" num="4.8"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="29.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="29.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="29.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="30" num="4.9"><w n="30.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="30.3">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="30.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="30.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.7">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="31" num="4.10"><w n="31.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="31.2">p<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> <w n="31.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="31.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="31.5">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c</w> <w n="31.6">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="32" num="5.1"><w n="32.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="32.2">l</w>'<w n="32.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="32.4">v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="32.5">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="32.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="33" num="5.2"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="33.2">d</w>'<w n="33.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="33.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> <w n="33.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>,</l>
					<l n="34" num="5.3"><w n="34.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.2">l</w>'<w n="34.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="34.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="34.5"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="34.6">s</w>'<w n="34.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="35" num="5.4"><w n="35.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="35.2">v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="35.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="35.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="35.5">fl<seg phoneme="o" type="vs" value="1" rule="437">o</seg>t</w> <w n="35.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w>,</l>
					<l n="36" num="5.5"><w n="36.1">S<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>, <w n="36.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="36.3">bl<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.4">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="37" num="5.6"><w n="37.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>pp<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t</w> : <w n="37.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="37.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> <w n="37.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="38" num="5.7"><w n="38.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="38.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="38.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="38.4">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="38.6">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="38.7">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>,</l>
					<l n="39" num="5.8"><w n="39.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="39.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="39.3">h<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.4">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="40" num="5.9"><w n="40.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="40.2"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="40.3">l</w>’<w n="40.4"><seg phoneme="œ" type="vs" value="1" rule="285">œ</seg>il</w> <w n="40.5">c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="40.6">p<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="41" num="5.10"><w n="41.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="41.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="41.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="41.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="41.5">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> !</l>
				</lg>
				<lg n="6">
					<l n="42" num="6.1"><w n="42.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="42.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="42.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="42.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="42.5"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="43" num="6.2"><w n="43.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="43.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="43.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="43.4">gu<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> ;</l>
					<l n="44" num="6.3"><w n="44.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="44.2"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="44.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="44.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="45" num="6.4"><w n="45.1">L<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>-<w n="45.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="45.3">s</w>'<w n="45.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="45.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="45.6">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
					<l n="46" num="6.5"><w n="46.1">L<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>-<w n="46.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="46.3">d</w>'<w n="46.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="46.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="46.6">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="47" num="6.6"><w n="47.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="47.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="47.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c</w> <w n="47.5">l</w>'<w n="47.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="48" num="6.7"><w n="48.1">L</w>'<w n="48.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="48.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="48.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="48.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="492">y</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> ;</l>
					<l n="49" num="6.8"><w n="49.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="49.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="49.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="49.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="49.5">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="49.6">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="49.7">s<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="50" num="6.9"><w n="50.1">L<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>-<w n="50.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="50.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="50.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="50.5">bl<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="51" num="6.10"><w n="51.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="51.2"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="51.3">l</w>'<w n="51.4"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="51.5">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="51.6">s</w>'<w n="51.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>…</l>
				</lg>
				<lg n="7">
					<l n="52" num="7.1"><w n="52.1">Pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="52.2">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="52.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="52.4">l</w>'<w n="52.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> !</l>
				</lg>
			</div></body></text></TEI>