<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES MOBILES D'ILLE-ET-VILAINE</title>
				<title type="medium">Édition électronique</title>
				<author key="PLI">
					<name>
						<forename>Oscar</forename>
						<nameLink>de</nameLink>
						<surname>POLI</surname>
					</name>
					<date from="1838" to="1908">1838-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>32 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">PLI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES MOBILES D'ILLE-ET-VILAINE</title>
						<author>Oscar De Poli</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2719920/f1.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Le Figaro</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro : journal non politique.</publisher>
									<date when="1870">1870-09-24</date>
								</imprint>
								<biblScope unit="issue">267</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="PLI1">
				<head type="main">LES MOBILES D'ILLE-ET-VILAINE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="1.3">h<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w>, <w n="1.4">l</w>'<w n="1.5"><seg phoneme="œ" type="vs" value="1" rule="285">œ</seg>il</w> <w n="1.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="1.7">d</w>'<w n="1.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>rs</w>,</l>
					<l n="2" num="1.2"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.4">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.5">pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="3.4">j<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="3.6">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w>,</l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.2">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="5.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="5.3">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>, <w n="5.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.7">ch<seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>,</l>
					<l n="6" num="1.6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="7" num="1.7"><w n="7.1">H<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="7.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="8.2">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.3">d</w>'<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>-<w n="8.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w>-<w n="8.6">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="9.2">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="240">e</seg>ds</w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="9.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.5">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>,</l>
					<l n="10" num="2.2"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="10.3">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="10.7">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="11" num="2.3"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="11.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="11.6">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="11.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w>.</l>
					<l n="12" num="2.4"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="12.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="13" num="2.5"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="13.4">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>,</l>
					<l n="14" num="2.6"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="14.2">r<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="14.3">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.6">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="15" num="2.7"><w n="15.1">H<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="15.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>,</l>
					<l n="16" num="2.8"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="16.2">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.3">d</w>'<w n="16.4"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>-<w n="16.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w>-<w n="16.6">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="17.3">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="17.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>,</l>
					<l n="18" num="3.2"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="18.2">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.5">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="19" num="3.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="19.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="19.5">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>x</w></l>
					<l n="20" num="3.4"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="20.2">d</w>'<w n="20.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="21" num="3.5"><w n="21.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>, <w n="21.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.5">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="21.6">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>,</l>
					<l n="22" num="3.6"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="22.4">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="22.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="22.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="23" num="3.7"><w n="23.1">H<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="23.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="23.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="23.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>,</l>
					<l n="24" num="3.8"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="24.2">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="24.3">d</w>'<w n="24.4"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>-<w n="24.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w>-<w n="24.6">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="25.2">c</w>'<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="25.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="25.7">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w></l>
					<l n="26" num="4.2"><w n="26.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.3">Br<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="27" num="4.3"><w n="27.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="27.2">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="27.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="27.5">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="27.7">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="27.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w>,</l>
					<l n="28" num="4.4"><w n="28.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="28.2">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="28.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="28.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="29" num="4.5"><w n="29.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="29.2">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="29.3"><seg phoneme="y" type="vs" value="1" rule="449">u</seg>hl<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> :</l>
					<l n="30" num="4.6"><w n="30.1">L</w>'<w n="30.2">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="30.4">tr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="30.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
					<l n="31" num="4.7"><w n="31.1">H<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="31.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="31.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="31.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>,</l>
					<l n="32" num="4.8"><w n="32.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="32.2">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="32.3">d</w>'<w n="32.4"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>-<w n="32.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w>-<w n="32.6">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>