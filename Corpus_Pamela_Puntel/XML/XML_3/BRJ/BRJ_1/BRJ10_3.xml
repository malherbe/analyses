<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ10">
					<head type="number">X</head>
					<head type="main">RÉVEIL</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.6">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">d</w>'<w n="1.8"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.9">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.4">d</w>'<w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="2.6">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> ;</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="3.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>… <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="3.8">l</w>'<w n="3.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.10">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.4">l</w>'<w n="4.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.6">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="4.7">l</w>'<w n="4.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C</w>'<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.5">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="5.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">j</w>'<w n="5.9"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="5.10">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="5.11">r<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">C</w>'<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="6.3">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="6.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">j</w>'<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="6.7">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
						<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.3">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="7.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w>, <w n="7.6">j</w>'<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="7.8">n<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="7.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.10">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">J</w>'<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">j</w>'<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="8.7">n<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="9.3">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.7">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">cr<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="10.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">J</w>'<w n="11.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="11.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.5">f<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r</w> <w n="11.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="11.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.11">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="12.4">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">l</w>'<w n="12.7"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">J</w>'<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="13.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.6"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="13.7">g<seg phoneme="i" type="vs" value="1" rule="467">î</seg>t</w> <w n="13.8"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="14.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="14.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>,</l>
						<l n="15" num="4.3"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">m</w>'<w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="15.4">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">gl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="15.6">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="15.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.9">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="16.4">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="16.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Août 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>