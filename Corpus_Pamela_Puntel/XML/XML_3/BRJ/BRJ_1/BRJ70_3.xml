<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ70">
					<head type="number">LXX</head>
					<head type="main">LES TRICOTEUSES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="u" type="vs" value="1" rule="424">Ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.3">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.4">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.3">gu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="3.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="3.7">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">In</seg>s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.3">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>, <w n="5.4">f<seg phoneme="œ" type="vs" value="1" rule="303">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="6.2">Th<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">M<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="7.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="7.3">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="7.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="7.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.2"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="8.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="8.3"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>. <w n="8.4">Tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="377">en</seg>f<seg phoneme="œ" type="vs" value="1" rule="303">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="9" num="2.3"><w n="9.1">D</w>'<w n="9.2"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>il</w> <w n="9.5">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.6">l</w>'<w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="358">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>,</l>
						<l n="10" num="2.4"><w n="10.1">C<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w> <w n="10.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="10.5">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w></l>
						<l n="11" num="2.5"><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">n</w>'<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="11.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w></l>
						<l n="12" num="2.6"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.2">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.3">d</w>'<w n="12.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>'<w n="12.5">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">C</w>'<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.4">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="13.5">c</w>'<w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="13.7">l</w>'<w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="14" num="3.2"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.2">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="3.3"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="15.4">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w></l>
						<l n="16" num="3.4"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="16.2">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="16.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="16.4">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="16.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="17" num="3.5"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="17.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="17.3">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="17.4">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>gts</w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>, <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.7">l<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="3.6"><w n="18.1">V<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="18.3">v<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="18.5">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> !… <w n="19.3">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="4.2"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">ch<seg phoneme="a" type="vs" value="1" rule="339">â</seg>t<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>, <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.6">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="21" num="4.3"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="21.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="21.3">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="21.4">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">N</w>'<w n="22.2"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="22.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="22.4">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="22.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="23.3">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>g<seg phoneme="ɥi" type="vs" value="1" rule="273">ui</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="24" num="4.6"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="24.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w> !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>g<seg phoneme="ɥi" type="vs" value="1" rule="273">ui</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="25.5">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="25.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="26.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="27" num="5.3"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>cc<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="27.3">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.4">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="27.5">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w>,</l>
						<l n="28" num="5.4"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="28.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="28.4">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="28.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="29" num="5.5"><w n="29.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="29.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="29.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="29.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="29.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="30" num="5.6"><w n="30.1">L</w>'<w n="30.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="30.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="30.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w> <w n="30.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="31.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="31.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.5">s<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="31.6"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="6.2"><w n="32.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="32.2">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="32.3">d</w>'<w n="32.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="32.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.6"><seg phoneme="a" type="vs" value="1" rule="339">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="33" num="6.3"><w n="33.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">m<seg phoneme="o" type="vs" value="1" rule="437">o</seg>t</w> <w n="33.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="33.4">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
						<l n="34" num="6.4"><w n="34.1">V<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="34.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.4">v<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="35" num="6.5"><w n="35.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="35.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="35.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> : <w n="35.4">T<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="35.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="35.6">h<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="36" num="6.6"><w n="36.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="36.2">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> : <w n="36.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="36.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="36.5">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>