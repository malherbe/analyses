<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ33">
					<head type="number">XXXIII</head>
					<head type="main">LES CASQUES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.6">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> !…</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> : <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.10"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.11">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.12">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="3.2">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.6">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.2">m<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.7">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.9">c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.3">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>p<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w>, <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.5">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="6.6">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="7" num="2.3"><w n="7.1">S<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="7.2">l</w>'<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>cc<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>, <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w></l>
						<l n="8" num="2.4"><w n="8.1">T<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.2">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>… <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="8.8"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.9">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="9.3">n<seg phoneme="ɛ" type="vs" value="1" rule="307">aî</seg>t</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.5">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rt</w>, <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.8">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.10">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="10.3">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="10.7">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="10.10">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="11.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.4">d<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>, <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.7">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.9">n<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> ;</l>
						<l n="12" num="3.4"><w n="12.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.2">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">s<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w>, <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.8">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="12.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="12.10">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.11">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L</w>'<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.3"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.6">b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="13.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.9">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.10"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="13.11">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.12">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> !</l>
						<l n="14" num="4.2"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="14.4">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.6">b<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="14.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.9">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="15" num="4.3"><w n="15.1">S</w>'<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">t</w>'<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="15.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="15.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.8">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>,</l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="16.2">Gr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tch<seg phoneme="ɛ" type="vs" value="1" rule="24">e</seg>n</w>, <w n="16.3">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="16.4">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="16.6">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="16.7">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="16.8">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="16.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.10">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">L</w>'<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="449">u</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="17.3">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">d</w>'<w n="17.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w>, <w n="17.6">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="17.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.8">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="18.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="18.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="18.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.5">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ls</w>, <w n="18.6">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="18.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.8">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="19.2">m<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="19.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="19.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>,</l>
						<l n="20" num="5.4"><w n="20.1">S<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="20.3">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="20.5">d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="20.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="20.8">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="20.9">m<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg></w> <w n="20.10"><seg phoneme="a" type="vs" value="1" rule="339">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Gu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>, <w n="21.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="21.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.5"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="21.6">fr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>,</l>
						<l n="22" num="6.2">(<w n="22.1">J</w>'<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.5">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.6"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="22.7">v<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="22.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.9">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="22.10">f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>),</l>
						<l n="23" num="6.3"><w n="23.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="23.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="23.4">th<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="a" type="vs" value="1" rule="339">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="23.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="23.7">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c</w>, <w n="23.8">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.9">g<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> ;</l>
						<l n="24" num="6.4"><w n="24.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="24.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.4">m<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>… <w n="24.5">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="24.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="24.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="24.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="24.9">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>