<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ7">
				<head type="number">VI</head>
					<head type="main">EN AVANT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="1.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="1.3">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="2.3">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>, <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="2.7">N<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd</w> !</l>
						<l n="3" num="1.3"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.3">v<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="3.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.5">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="3.7">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w>. <w n="4.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">v<seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.8">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> !…</l>
						<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="5.2">ph<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.3">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
						<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">V<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="6.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="6.3">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="6.4">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.5">gl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ;</l>
						<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="7.2">Rh<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.4">gr<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! —</l>
						<l n="9" num="1.9"><space unit="char" quantity="6"></space><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="9.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="9.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="9.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="10" num="1.10"><space unit="char" quantity="6"></space><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="10.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="10.3">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ! <w n="10.4">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="10.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> !</l>
						<l n="11" num="1.11"><space unit="char" quantity="6"></space><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="1.12"><space unit="char" quantity="6"></space><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="12.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> !</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="13.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="13.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="13.6">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="2.2"><w n="14.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="14.2">s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="14.3">g<seg phoneme="o" type="vs" value="1" rule="317">au</seg>l<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="14.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> ;</l>
						<l n="15" num="2.3"><w n="15.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="15.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="300">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="2.4"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="16.3"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="16.4">s</w>'<w n="16.5"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.6">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="16.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w></l>
						<l n="17" num="2.5"><space unit="char" quantity="4"></space><w n="17.1">Qu</w>'<w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="17.3">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="17.4">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="17.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="17.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="2.6"><space unit="char" quantity="4"></space><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="18.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="18.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>, <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="18.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="18.6">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> !</l>
						<l n="19" num="2.7"><space unit="char" quantity="4"></space><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>, <w n="19.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="19.3">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="19.4">n</w>'<w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="19.7">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="2.8"><space unit="char" quantity="4"></space><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="20.5">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="20.6">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> ! —</l>
						<l n="21" num="2.9"><space unit="char" quantity="6"></space><w n="21.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="21.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="21.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="21.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="22" num="2.10"><space unit="char" quantity="6"></space><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="22.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="22.3">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ! <w n="22.4">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="22.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="22.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> !</l>
						<l n="23" num="2.11"><space unit="char" quantity="6"></space><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="23.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="23.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="2.12"><space unit="char" quantity="6"></space><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="24.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> !</l>
					</lg>
					<lg n="3">
						<l n="25" num="3.1"><w n="25.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t</w>, <w n="25.2">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>, <w n="25.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.5">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="26" num="3.2"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="26.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="26.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.6">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w> <w n="26.7">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="26.8">s</w>'<w n="26.9"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w></l>
						<l n="27" num="3.3"><w n="27.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="27.2">t</w>'<w n="27.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.4">ch<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="27.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="27.7">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.8">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="3.4"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="28.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="28.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.4">pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="28.5">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="28.6">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.7">l</w>'<w n="28.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
						<l n="29" num="3.5"><space unit="char" quantity="4"></space><w n="29.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="29.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="29.3">fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="29.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="30" num="3.6"><space unit="char" quantity="4"></space><w n="30.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>, <w n="30.3">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="30.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !</l>
						<l n="31" num="3.7"><space unit="char" quantity="4"></space><w n="31.1">P<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="31.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="457">u</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="31.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="31.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.5">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="3.8"><space unit="char" quantity="4"></space><w n="32.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="32.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.5">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ! —</l>
						<l n="33" num="3.9"><space unit="char" quantity="6"></space><w n="33.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="33.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="33.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="33.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="34" num="3.10"><space unit="char" quantity="6"></space><w n="34.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="34.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="34.3">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> ! <w n="34.4">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="34.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="34.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> !</l>
						<l n="35" num="3.11"><space unit="char" quantity="6"></space><w n="35.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="35.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="35.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="35.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.5">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="36" num="3.12"><space unit="char" quantity="6"></space><w n="36.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="36.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="36.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="36.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Août 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>