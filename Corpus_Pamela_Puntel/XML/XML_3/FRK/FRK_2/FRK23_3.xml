<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉPAVES</head><div type="poem" key="FRK23">
					<head type="main">DEVANT UNE BIÈRE</head>
					<opener>
						<salute>A la mémoire de M. Küss, Maire et Député de Strasbourg<ref target="1" type="noteAnchor">1</ref></salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">E</seg></w> <w n="1.2">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.3">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>, <w n="1.4">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.5">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.7">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.8">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.10">f<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="2" num="1.2"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.7">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="2.8">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> !</l>
						<l n="3" num="1.3"><w n="3.1">T<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="3.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sp<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="3.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.8">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="3.9">d</w>’<w n="3.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">S<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="4.2">pl<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.5">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.6">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="4.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">l</w>’<w n="4.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.10">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="4.11">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> !…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w>, <w n="5.2">c</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> ? <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w>’<w n="5.6">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="5.7">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="5.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.11">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.3">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> — <w n="6.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> — <w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="6.8">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.9">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="6.10">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> !</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.4">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="7.6">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="240">e</seg>ds</w> <w n="7.7">d</w>’<w n="7.8"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.9">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>d<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd</w> <w n="7.10"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="8.2"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="8.4">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> <w n="8.7">qu</w>’<w n="8.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="8.9">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w>, <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.3">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="9.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> : <w n="9.5">M<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tz</w>, <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.7">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="9.8">l<seg phoneme="o" type="vs" value="1" rule="434">o</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ls<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="10.4">Str<seg phoneme="a" type="vs" value="1" rule="339">a</seg>sb<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w>, <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.9">ch<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r</w> !…</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.6">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="11.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.9">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="304">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="3.4"><w n="12.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="12.3">m<seg phoneme="o" type="vs" value="1" rule="437">o</seg>t</w> <w n="12.4">c<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="12.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="12.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.9">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="12.10">ch<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="13.3">t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="13.4">l</w>’<w n="13.5">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w>, <w n="13.6">l</w>’<w n="13.7"><seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.10">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.11"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="14.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="14.4">p<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ds</w> <w n="14.5">s</w>’<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="14.8">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="14.9">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> :</l>
						<l n="15" num="4.3"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="o" type="vs" value="1" rule="443">o</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="15.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="15.8">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="15.9">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">Tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="16.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="16.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="16.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="16.7">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="16.8">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="17.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="17.3">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="17.5">tr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="17.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="17.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.8">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="18" num="5.2"><w n="18.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="18.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="18.3">t<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="18.4">l</w>’<w n="18.5"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>mm<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.6"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> ;</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="492">y</seg>rs</w> <w n="19.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="19.5">Dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w>, <w n="19.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="19.7">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="19.8">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="20" num="5.4"><w n="20.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="20.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="20.3">s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lcr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.4">n<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>rs</w> <w n="20.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="20.6">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.8">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="21.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">d</w>’<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ls<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="22" num="6.2"><w n="22.1">Qu</w>’<w n="22.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.3">l</w>’<w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>-<w n="22.6">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>, <w n="22.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="22.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.9">s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="22.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> :</l>
						<l n="23" num="6.3"><w n="23.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="23.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>, <w n="23.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="23.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="23.7">f<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="23.8">qu</w>’<w n="23.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="23.10">s<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="6.4"><w n="24.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="24.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="24.5"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="24.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="24.8">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="24.9">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">—<w n="25.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="25.2">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w>, <w n="25.3">s</w>’<w n="25.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="25.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w>, <w n="25.7">P<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="25.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.9">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="25.10">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="25.11">l<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="7.2"><w n="26.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="26.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="26.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="26.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.5">sp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ctr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.6"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="26.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.8">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="27.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="27.5"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="27.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="27.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.8">t<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="28" num="7.4"><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>gu<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="28.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="28.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="28.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="28.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="28.7">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="28.8">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="28.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="29.2">t</w>’<w n="29.3"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="29.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="29.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ! <w n="29.6">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="29.7">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="29.8">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="30" num="8.2"><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="30.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>… <w n="30.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="30.5">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="30.6">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ds</w> <w n="30.7"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="30.8">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w></l>
						<l n="31" num="8.3"><w n="31.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.2">l</w>’<w n="31.3">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">Str<seg phoneme="a" type="vs" value="1" rule="339">a</seg>sb<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w> <w n="31.6">t<seg phoneme="ɥ" type="sc" value="0" rule="457">u</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="31.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="31.8">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="32" num="8.4"><w n="32.1">N</w>’<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="32.3">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="32.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.6">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="32.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="32.8">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w> <w n="32.9">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="32.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>br<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> !</l>
					</lg>
					<closer>
						<note type="footnote" id="1">Mort à Bordeaux dans la nuit du Ier mars 1871, date de la ratification du traité portant cession de l’Alsace aux Prussiens.</note>
					</closer>
				</div></body></text></TEI>