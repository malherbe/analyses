<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉPAVES</head><head type="main_subpart">PATRIE</head><div type="poem" n="2" key="FRK19">
							<head type="number">II</head>
							<lg n="1">
								<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c</w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="1.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="1.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.7">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">f<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
								<l n="2" num="1.2"><w n="2.1">Tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.7">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.8">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.3">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="3.4">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> <w n="3.5">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="3.7">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ps</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
								<l n="4" num="1.4"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="4.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="4.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.9">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
							</lg>
							<lg n="2">
								<l n="5" num="2.1"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.2">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.5">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.9">l<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
								<l n="6" num="2.2"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="6.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
								<l n="7" num="2.3"><w n="7.1">Tr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="7.2">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="7.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> !… <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="7.5">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
								<l n="8" num="2.4"><w n="8.1">S<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>f</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> ! <w n="8.5">C</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.8">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.10">pl<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
							</lg>
							<lg n="3">
								<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="132">E</seg>h</w> <w n="9.2">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>, <w n="9.3">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w>… <w n="9.5">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="9.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="9.7">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.9">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.10">n<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
								<l n="10" num="3.2"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">Vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="10.7">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="10.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.9">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
								<l n="11" num="3.3"><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.5">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="11.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="11.8">d</w>’<w n="11.9"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.10"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> !</l>
							</lg>
							<lg n="4">
								<l n="12" num="4.1"><w n="12.1">L<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.4">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.7">pl<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.9">l</w>’<w n="12.10"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
								<l n="13" num="4.2"><w n="13.1">F<seg phoneme="o" type="vs" value="1" rule="317">au</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="13.2">d</w>’<w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="13.4">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="13.5">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="13.6">l</w>’<w n="13.7">h<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.9">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="14" num="4.3"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.4">fl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="14.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="14.6">B<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="14.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="14.9">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> !</l>
							</lg>
						</div></body></text></TEI>