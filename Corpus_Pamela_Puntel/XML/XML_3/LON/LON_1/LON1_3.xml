<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="LON">
					<name>
						<forename>Eugène</forename>
						<nameLink>de</nameLink>
						<surname>LONLAY</surname>
					</name>
					<date from="1815" to="1886">1815-1886</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>52 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">LON_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>>POÉSIES, ÉDITION ELZÉVIRIENNE</title>
						<author>EUGÈNE DE LONLAY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k65208385/f11.image.r=EUG%C3%88NE%20DE%20LONLAY</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES, ÉDITION ELZÉVIRIENNE</title>
								<author>EUGÈNE DE LONLAY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALCAN-LEVY</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="LON1">
				<head type="main">CHANT PATRIOTIQUE</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="1.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w>, <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="353">E</seg>x<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xpl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ts</w> :</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ccl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.3">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.6">l<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.3">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Gr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="7.4">d</w>'<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="424">Ou</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>-<w n="8.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="8.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="8.5">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>rs</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="9" num="1.1"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.3">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="9.6">l</w>'<w n="9.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="1.2"><w n="10.1">Gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="10.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="10.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w>,</l>
						<l n="11" num="1.3"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="11.2">l<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="11.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="1.4"><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.2">c<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.4">l<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="13.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="2.2"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ct<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>,</l>
						<l n="15" num="2.3"><w n="15.1">R<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.2">tr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="2.4"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="16.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.4">n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="17" num="1.1"><w n="17.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rc</w>-<w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w>-<w n="17.4">c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="1.2"><w n="18.1">L</w>'<w n="18.2">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.3">t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
						<l n="19" num="1.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="19.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="1.4"><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="20.4">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1"><w n="21.1">L</w>'<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="21.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="21.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="2.2"><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="22.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="22.3">d<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>,</l>
						<l n="23" num="2.3"><w n="23.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="2.4"><w n="24.1">L</w>'<w n="24.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="24.3">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="24.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199">om</seg></w> <w n="24.5">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>.</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">IV</head>
					<lg n="1">
						<l n="25" num="1.1"><w n="25.1">P<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="25.2">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="25.5">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="26" num="1.2"><w n="26.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="26.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="26.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="26.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> ;</l>
						<l n="27" num="1.3"><w n="27.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="27.3">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w> <w n="27.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="28" num="1.4"><w n="28.1">D<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="28.3">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="28.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>.</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1"><w n="29.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="29.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="29.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="30" num="2.2"><w n="30.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="30.2">l</w>'<w n="30.3">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="30.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
						<l n="31" num="2.3"><w n="31.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="31.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="31.3"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="31.4">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="31.5"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="32" num="2.4"><w n="32.1">V<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="32.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="32.3">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>.</l>
					</lg>
				</div>
				<div type="section" n="5">
					<head type="number">V</head>
					<lg n="1">
						<l n="33" num="1.1"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="33.2">l</w>'<w n="33.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="33.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="33.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="34" num="1.2"><w n="34.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>, <w n="34.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ds</w> <w n="34.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="34.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="34.5">v<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>x</w> :</l>
						<l n="35" num="1.3"><w n="35.1">D<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="35.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="35.3">l</w>'<w n="35.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="36" num="1.4"><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="36.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="36.3">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="36.4">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="37" num="2.1"><w n="37.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="37.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="37.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="37.4">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="37.5">d</w>'<w n="37.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="38" num="2.2"><w n="38.1">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="38.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="38.3">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w>,</l>
						<l n="39" num="2.3"><w n="39.1">Pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="39.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="39.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="40" num="2.4"><w n="40.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ll<seg phoneme="y" type="vs" value="1" rule="449">u</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="40.3">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>