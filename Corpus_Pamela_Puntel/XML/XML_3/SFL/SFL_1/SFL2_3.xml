<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DU SIÈGE DE PARIS</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="SFL">
					<name>
						<forename>Théobald</forename>
						<surname>SAINT-FÉLIX</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>161 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">SFL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>CHANTS DU SIÈGE DE PARIS. 1870-1871</title>
						<author>THÉOBALD SAINT-FÉLIX</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Imprimerie Ch. Schiller</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="SFL2">
				<head type="main">LES OISEAUX DU PROSCRIT</head>
				<head type="form">MÉLODIE</head>
				<opener>
					<salute>A VICTOR HUGO</salute>
				</opener>
				<div type="section" n="1">
					<head type="number">1</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="1.2"><seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w>, <w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="2.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d</w>, <w n="2.4">fr<seg phoneme="ɥ" type="sc" value="0" rule="454">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>,</l>
						<l n="3" num="1.3"><w n="3.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="3.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>ps</w>, <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="3.4">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="4" num="1.4"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="4.4">v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="4.5">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="4.7">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="4.8">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">P<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>, <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ppr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.6">v<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>x</w>, <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.8">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="6.9">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="7.3">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.3">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="8.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">2</head>
					<lg n="1">
						<l n="9" num="1.1"><w n="9.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>, <w n="9.2"><seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w>, <w n="9.3">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.4">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="9.5">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="10" num="1.2"><w n="10.1">B<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="10.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="10.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>…</l>
						<l n="11" num="1.3"><w n="11.1">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="11.2">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>, <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="11.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="12" num="1.4"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="12.2">j<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="12.3">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d</w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="12.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.6">v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><w n="13.1">P<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>, <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="13.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ppr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="2.2"><w n="14.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="14.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.6">v<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>x</w>, <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="14.8">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="14.9">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ;</l>
						<l n="15" num="2.3"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="15.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="15.3">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="2.4"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="16.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="16.3">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="16.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">3</head>
					<lg n="1">
						<l n="17" num="1.1"><w n="17.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="17.2"><seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w>, <w n="17.3">n</w>’<w n="17.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="17.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="17.7">b<seg phoneme="o" type="vs" value="1" rule="443">o</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="18" num="1.2"><w n="18.1">Cr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b</w>, <w n="18.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="18.5">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.7">v<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>,</l>
						<l n="19" num="1.3"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.3">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="19.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="19.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.6"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.7">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="20" num="1.4"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, <w n="20.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="20.4">v<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="20.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="20.6">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="20.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>.</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1"><w n="21.1">P<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>, <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="21.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="21.6"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ppr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="22" num="2.2"><w n="22.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="22.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="22.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.6">v<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>x</w>, <w n="22.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="22.8">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="22.9">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ;</l>
						<l n="23" num="2.3"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="23.3">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="2.4"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="24.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="24.3">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="24.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="24.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>.</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">4</head>
					<lg n="1">
						<l n="25" num="1.1"><w n="25.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="25.2"><seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w>, <w n="25.3">n</w>’<w n="25.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="25.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="25.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="25.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="1.2"><w n="26.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="26.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="26.5">d<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="26.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.7">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>. —</l>
						<l n="27" num="1.3"><w n="27.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="27.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="27.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="27.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="27.6">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="1.4"><w n="28.1">R<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="28.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="28.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="28.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="28.6">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> !</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1"><w n="29.1">P<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="29.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>, <w n="29.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="29.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="29.6"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ppr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="30" num="2.2"><w n="30.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="30.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w> <w n="30.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="30.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="30.6">v<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>x</w>, <w n="30.7"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="30.8">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="30.9">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ;</l>
						<l n="31" num="2.3"><w n="31.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="31.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="31.3">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="32" num="2.4"><w n="32.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="32.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="32.3">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="32.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="32.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w>.</l>
					</lg>
				</div>
				<closer>
					<note type="footnote" id="">N.B. — La musique, chant et piano, composée par M. Marfaing, est en vente chez M. Sylvain Saint-Étienne, éditeur, n°31 bis, Faubourg Montmartre, à Paris.</note>
				</closer>
			</div></body></text></TEI>