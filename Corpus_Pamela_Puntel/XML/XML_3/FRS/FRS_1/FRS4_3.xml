<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">AMERTUMES ET PAIN NOIR</title>
				<title type="sub">SIÈGE DE PARIS 1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="FRS">
					<name>
						<forename>Émile</forename>
						<surname>FRANÇOIS</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>487 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
						<author>ÉMILE FRANÇOIS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61012844.r=FRAN%C3%87OIS%20E.%2C%20AMERTUMES%20ET%20PAIN%20NOIR.?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
								<author>ÉMILE FRANÇOIS</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE A. LACROIX, VERBOECKHOVEN ET Cie</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-24" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRS4">
				<head type="main">L'ATTENTE</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">L</w>'<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !… <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>h</w> ! <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.5">p<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>r<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>rr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rqu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="3.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="4.2">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.5">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
					<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">l<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rd</w> <w n="6.4">p<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ds</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rps</w> !…</l>
					<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.3">m<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w>… <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="7.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w>, <w n="8.4">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.6">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="8.7">b<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rds</w> !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="411">Ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="9.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.2"><space unit="char" quantity="8"></space><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.4">m<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>ds</w> <w n="10.5">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rcl<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">f<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> !…</l>
					<l n="11" num="2.3"><space unit="char" quantity="8"></space><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="11.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rcl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="11.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="2.4"><space unit="char" quantity="8"></space><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="12.4">n</w>'<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="12.6">d</w>'<w n="12.7"><seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt</w></l>
					<l n="13" num="2.5"><space unit="char" quantity="8"></space><w n="13.1">Qu</w>'<w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="13.3"><seg phoneme="œ" type="vs" value="1" rule="285">œ</seg>il</w>-<w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>-<w n="13.5">b<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>f</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.7">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.8">v<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="2.6"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">In</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
					<l n="15" num="2.7"><space unit="char" quantity="8"></space><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.2">br<seg phoneme="ɥ" type="sc" value="0" rule="454">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>ts</w> <w n="15.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="15.4">v<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="365">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.7">r<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="2.8"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="16.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="16.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="16.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="16.5">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !…</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.2">br<seg phoneme="ɥ" type="sc" value="0" rule="454">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>ts</w>, <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>s</w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">p<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="17.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.9">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="3.2"><w n="18.1">V<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="18.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="18.5">d</w>'<w n="18.6"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w>, <w n="18.7">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.8"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.9"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>ph<seg phoneme="e" type="vs" value="1" rule="408">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="19" num="3.3"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="19.2">fl<seg phoneme="o" type="vs" value="1" rule="434">o</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rqu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="19.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c</w>, <w n="19.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rqu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="19.6">n<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>,</l>
					<l n="20" num="3.4"><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="20.2">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="20.3">gr<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w>, <w n="20.4">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="20.5">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="20.7">n</w>'<w n="20.8"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="20.9">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="20.10">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>,</l>
					<l n="21" num="3.5"><w n="21.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.2">bl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w> <w n="21.3">d</w>'<w n="21.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="21.5">c<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="3.6"><w n="22.1">L</w>'<w n="22.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="22.4">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.5"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rb<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="22.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="22.8">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="23" num="3.7"><w n="23.1">Qu</w>'<w n="23.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="23.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.6">n<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="23.8">qu</w>'<w n="23.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="23.10">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="23.11">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.12">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
					<l n="24" num="3.8"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="24.6">l</w>'<w n="24.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>x<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="24.8">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space unit="char" quantity="8"></space><w n="25.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="25.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="25.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="25.4">v<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="25.5">l</w>'<w n="25.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="25.7">v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w>, <w n="25.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="25.9">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="26" num="4.2"><space unit="char" quantity="8"></space><w n="26.1">N<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="26.2">h<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w>, <w n="26.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="26.4"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="26.5">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="26.6"><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>…</l>
					<l n="27" num="4.3"><space unit="char" quantity="8"></space><w n="27.1">R<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="27.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="27.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> !… <w n="27.4">V<seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="27.5">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="27.6">m<seg phoneme="ɥ" type="sc" value="0" rule="457">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
					<l n="28" num="4.4"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w>, <w n="28.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="28.3">l</w>'<w n="28.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="28.5">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg>vr<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w>,</l>
					<l n="29" num="4.5"><space unit="char" quantity="8"></space><w n="29.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="29.3">br<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="29.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="29.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="29.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="30" num="4.6"><space unit="char" quantity="8"></space><w n="30.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="30.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="30.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.4">m<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w>… <w n="30.5">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="30.6">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="30.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.8">p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>.</l>
					<l n="31" num="4.7"><space unit="char" quantity="8"></space><w n="31.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="31.3">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>… <w n="31.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.6">s</w>'<w n="31.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="32" num="4.8"><space unit="char" quantity="8"></space><w n="32.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="32.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> !… <w n="32.4"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="32.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> ; <w n="32.6">m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>… <w n="32.7">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>-<w n="32.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> ?</l>
				</lg>
			</div></body></text></TEI>