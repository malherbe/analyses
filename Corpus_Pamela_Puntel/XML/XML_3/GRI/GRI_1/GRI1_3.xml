<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">REVUE DE BRETAGNE ET DE VENDÉE</title>
				<title type="sub_2">QUATORZIÈME ANNÉE</title>
				<title type="sub_1">TROISIÈME SÉRIE — TOME VIII</title>
				<title type="medium">Édition électronique</title>
				<author key="GRI">
					<name>
						<forename>Émile</forename>
						<surname>GRIMAUD</surname>
					</name>
					<date from="1831" to="1901">1831-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>413 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">STROPHES PATRIOTIQUES</title>
						<author>ÉMILE GRIMAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k453762t.r=Revue%20de%20Bretagne%20et%20de%20Vend%C3%A9e%201870?rk=85837;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>REVUE DE BRETAGNE ET DE VENDÉE</title>
								<imprint>
									<pubPlace>NANTES</pubPlace>
									<publisher>REVUE DE BRETAGNE ET DE VENDÉE — ANNÉE 1870 — DEUXIÈME SEMESTRE</publisher>
									<date when="1870">OCTOBRE 1870</date>
								</imprint>
								<biblScope unit="tome">T. 28</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-21" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Octobre 1870</head><div type="poem" key="GRI1">
					<head type="main">LA MARSEILLAISE VENDÉENNE</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w>.</l>
							<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="3.3">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="3.4">gl<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">C</w>'<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="4.3">l</w>'<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="4.5">d</w>'<w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.7">t<seg phoneme="i" type="vs" value="1" rule="492">y</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w> <w n="4.8">m<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> !</l>
							<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">P<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>squ</w>'<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="5.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.4">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="5.5">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="5.6">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.7">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="6.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> <w n="6.3">m<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xp<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="6.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w>,</l>
							<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> !… <w n="7.2">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ds</w>,</l>
							<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="8.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342">a</seg><seg phoneme="j" type="sc" value="0" rule="474">ï</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="8.4">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
							<l n="9" num="1.9"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="9.3">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg>s</w> ! <w n="9.4">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="9.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
							<l n="10" num="1.10"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="10.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.4">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="10.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="10.7">l</w>'<w n="10.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="11" num="1.1"><space unit="char" quantity="8"></space><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>rr<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="11.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="11.5">s<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="12" num="1.2"><space unit="char" quantity="8"></space><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="12.3"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="12.6">v<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> !</l>
							<l n="13" num="1.3"><space unit="char" quantity="8"></space><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="13.4">s<seg phoneme="y" type="vs" value="1" rule="444">û</seg>r</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="13.7">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="14" num="1.4"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="14.2">c</w>'<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="14.4">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="14.6">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="14.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> !</l>
							<l n="15" num="1.5"><space unit="char" quantity="8"></space><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>cs</w> <w n="15.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
							<l n="16" num="1.6"><space unit="char" quantity="8"></space><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>cs</w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="16.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>ts</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="16.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="16.7">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> ;</l>
							<l n="17" num="1.7"><space unit="char" quantity="8"></space><w n="17.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="17.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="17.3">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="17.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="17.5">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>,</l>
							<l n="18" num="1.8"><space unit="char" quantity="8"></space><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="18.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="18.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="18.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.7">gl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="19" num="1.9"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="19.3">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg>s</w> ! <w n="19.4">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="19.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="19.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
							<l n="20" num="1.10"><w n="20.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="20.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="20.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.4">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="20.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="20.7">l</w>'<w n="20.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>

						<lg n="1">
							<l n="21" num="1.1"><space unit="char" quantity="8"></space><w n="21.1">L<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.2">l</w>'<w n="21.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>, <w n="21.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="22" num="1.2"><space unit="char" quantity="8"></space><w n="22.1">M<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="22.3">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="22.4">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rds</w> :</l>
							<l n="23" num="1.3"><space unit="char" quantity="8"></space><w n="23.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="23.2">l</w>'<w n="23.3"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="23.4">Str<seg phoneme="a" type="vs" value="1" rule="339">a</seg>sb<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rg</w> <w n="23.5">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="24" num="1.4"><space unit="char" quantity="8"></space><w n="24.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="24.2">t</w>'<w n="24.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="24.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="24.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="24.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rts</w>.</l>
							<l n="25" num="1.5"><space unit="char" quantity="8"></space><w n="25.1"><seg phoneme="w" type="sc" value="0" rule="430">Ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> ! <w n="25.2">c</w>'<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="25.4">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="25.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="25.8">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="26" num="1.6"><space unit="char" quantity="8"></space><w n="26.1">Br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="26.3">f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="26.4">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w></l>
							<l n="27" num="1.7"><space unit="char" quantity="8"></space><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="27.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="27.3">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="27.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.5">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w></l>
							<l n="28" num="1.8"><w n="28.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="28.3">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.4">r<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="29" num="1.9"><w n="29.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="29.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="29.3">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg>s</w> ! <w n="29.4">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="29.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="29.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
							<l n="30" num="1.10"><w n="30.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="30.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="30.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="30.4">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="30.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="30.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="30.7">l</w>'<w n="30.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<lg n="1">
							<l n="31" num="1.1"><space unit="char" quantity="8"></space><w n="31.1">D<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="31.2">c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="31.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="31.5">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="32" num="1.2"><space unit="char" quantity="8"></space><w n="32.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>ps</w>, <w n="32.2">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>th<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>, <w n="32.3">St<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w>,</l>
							<l n="33" num="1.3"><space unit="char" quantity="8"></space><w n="33.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="33.2">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="33.3"><seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="33.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="33.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="33.6">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="34" num="1.4"><space unit="char" quantity="8"></space><w n="34.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="34.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.3"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.5">br<seg phoneme="y" type="vs" value="1" rule="444">û</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w></l>
							<l n="35" num="1.5"><space unit="char" quantity="8"></space><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="35.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="35.5"><seg phoneme="o" type="vs" value="1" rule="414">ô</seg></w> <w n="35.6">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="36" num="1.6"><space unit="char" quantity="8"></space><w n="36.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="36.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="36.4">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="36.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="36.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> :</l>
							<l n="37" num="1.7"><space unit="char" quantity="8"></space><w n="37.1">Gu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="37.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="37.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ps</w>, <w n="37.4">gu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="37.5">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="37.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ;</l>
							<l n="38" num="1.8"><space unit="char" quantity="8"></space><w n="38.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="38.2">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="38.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="38.4">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="38.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="38.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
							<l n="39" num="1.9"><w n="39.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="39.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="39.3">V<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg>s</w> ! <w n="39.4">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="39.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="39.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="39.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
							<l n="40" num="1.10"><w n="40.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w>, <w n="40.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="40.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="40.4">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="40.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="40.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="40.7">l</w>'<w n="40.8"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !</l>
						</lg>
					</div>
					<closer>
						<dateline>
							<date when="1870">Nantes, 7 octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>