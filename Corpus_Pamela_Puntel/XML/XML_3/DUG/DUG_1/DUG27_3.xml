<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG27">
				<head type="main">LA PAIX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.2">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w>, <w n="1.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="1.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> ! <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.7">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w>, <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.9">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="1.10">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="1.11">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">T<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.4">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="3.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="3.5">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="3.8">tr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="3.9">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">B<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="4.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="4.3">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l</w>'<w n="4.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
					<l n="5" num="1.5"><w n="5.1">D<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="5.5">fl<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="5.7">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.3">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="6.5">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> !</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="352">E</seg>ff<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">m<seg phoneme="o" type="vs" value="1" rule="437">o</seg>t</w> <w n="7.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">l</w>'<w n="7.9">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>x</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="8.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="8.7">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>x</w> !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">— <w n="9.1">Qu</w>'<w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="9.3">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="9.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="9.9">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="9.10"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.11">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.2"><space unit="char" quantity="8"></space><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="10.3">m<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="396">eu</seg>rs</w>, <w n="10.4">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> :</l>
					<l n="11" num="2.3"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="11.5">ch<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="2.4"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="12.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="12.5">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> ! —</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="13.2">qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg></w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="13.6">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="13.7">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="13.8">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="13.9">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="13.10">s<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="3.2"><w n="14.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="14.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="14.4">B<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rk</w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.6">ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ffr<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="14.8">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">G<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="15.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="15.3">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>…</l>
					<l n="16" num="3.4"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="16.2">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="16.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">n<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="16.6">f<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r</w> <w n="16.7">l</w>'<w n="16.8"><seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="16.9">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="16.10">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.11">m<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="17" num="3.5"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">p<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="i" type="vs" value="1" rule="320">y</seg>s</w> <w n="17.4">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="17.5">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="17.6"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="17.7">Gr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="18.2">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d</w>'<w n="18.3">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.5">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> !…</l>
				</lg>
			</div></body></text></TEI>