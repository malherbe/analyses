<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG1">
				<head type="main">MON TITRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>-<w n="1.2">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="1.4">l</w>'<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="1.7">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ds</w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="2.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="3.3">Kr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>pp</w>, <w n="3.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="4.5">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="4.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
					<l n="5" num="1.5"><w n="5.1">Pl<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w> <w n="5.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="5.3">k<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.2">l</w>'<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">d</w>'<w n="6.5"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>…</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.3">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ct<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="8" num="2.2"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rts</w></l>
					<l n="9" num="2.3"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="9.2">g<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="10" num="2.4"><w n="10.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rts</w>,</l>
					<l n="11" num="2.5"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="11.7">f<seg phoneme="a" type="vs" value="1" rule="192">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="12" num="2.6"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>lc<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rs</w> !…</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">p<seg phoneme="o" type="vs" value="1" rule="443">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="413">ë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="3.2"><w n="14.1">S<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="14.2">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rg<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="14.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.6">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w></l>
					<l n="15" num="3.3"><w n="15.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="3.4"><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">n</w>'<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="16.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
					<l n="17" num="3.5"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.2">j<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.5">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="3.6"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="18.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="18.5">l</w>'<w n="18.6">h<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="19.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="4.2"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="20.6">n</w>'<w n="20.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t</w>,</l>
					<l n="21" num="4.3"><w n="21.1">J<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="21.2">l</w>'<w n="21.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">r<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="4.4"><w n="22.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.2">v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>l</w> <w n="22.3">n</w>'<w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="22.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="22.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> :</l>
					<l n="23" num="4.5"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="357">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="4.6"><w n="24.1">C</w>'<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="24.6">qu</w>'<w n="24.7"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t</w> !…</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="25.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>-<w n="25.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.5">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.7">v<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="26" num="5.2"><w n="26.1">Qu</w>'<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="26.5">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="26.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.7">c<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> !</l>
					<l n="27" num="5.3"><w n="27.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="27.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="27.4">qu</w>'<w n="27.5"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="28" num="5.4"><w n="28.1">P<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="28.3">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="28.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="28.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="28.6">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w></l>
					<l n="29" num="5.5"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="29.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="29.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="29.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>ts</w> <w n="29.5">qu</w>'<w n="29.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.7">f<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="30" num="5.6"><w n="30.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">Im</seg>pr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="30.3">st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>gm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> !…</l>
				</lg>
			</div></body></text></TEI>