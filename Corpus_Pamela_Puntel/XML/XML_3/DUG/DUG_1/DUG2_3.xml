<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG2">
				<head type="main">CHAUVIN</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w> <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">Rh<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> !</l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c</w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="2.3">d<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w>, <w n="2.4">Ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> !</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="3.3">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>.</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="4.2">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w>, <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w> <w n="4.4">b<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="5" num="1.5"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="5.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.3">gr<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="5.4">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> !</l>
					<l n="6" num="1.6"><w n="6.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="6.3">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w></l>
					<l n="7" num="1.7"><w n="7.1">F<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="7.2">r<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="7.3">l<seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="8.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="9.3">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> !…</l>
					<l n="10" num="2.2"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="10.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="11" num="2.3"><w n="11.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.3">r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="12" num="2.4"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.2">pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
					<l n="13" num="2.5"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="13.3">r<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
					<l n="14" num="2.6"><w n="14.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.2">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="14.3">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
					<l n="15" num="2.7"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="15.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="15.4">cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> :</l>
					<l n="16" num="2.8"><w n="16.1">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.3">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Qu<seg phoneme="wa" type="vs" value="1" rule="280">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.2">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg></w></l>
					<l n="18" num="3.2"><w n="18.1">V<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="18.2">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="18.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="18.4">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w>,</l>
					<l n="19" num="3.3"><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="19.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w> <w n="19.4">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w></l>
					<l n="20" num="3.4"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.3">p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="21" num="3.5"><w n="21.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">m<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="21.4">f<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
					<l n="22" num="3.6"><w n="22.1">Qu</w>'<w n="22.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="22.4">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="22.5">ch<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="22.6">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w></l>
					<l n="23" num="3.7"><w n="23.1"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="23.2">C<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w>, <w n="23.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="23.4">R<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
					<l n="24" num="3.8"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg></w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.3">R<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">T<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="25.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="25.4">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.5">p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w></l>
					<l n="26" num="4.2"><w n="26.1">Qu</w>'<w n="26.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="26.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="26.4"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w></l>
					<l n="27" num="4.3"><w n="27.1">Qu</w>'<w n="27.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="27.3">n<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.4">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w>…</l>
					<l n="28" num="4.4"><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="28.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.3">p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="29" num="4.5"><w n="29.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.2">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="29.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> !</l>
					<l n="30" num="4.6"><w n="30.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">gu<seg phoneme="i" type="vs" value="1" rule="490">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="30.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w>,</l>
					<l n="31" num="4.7"><w n="31.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w></l>
					<l n="32" num="4.8"><w n="32.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="32.2">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="32.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">F<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="33.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="33.3">b<seg phoneme="o" type="vs" value="1" rule="314">eau</seg>x</w> <w n="33.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>,</l>
					<l n="34" num="5.2"><w n="34.1">Rh<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="34.2"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="34.3">phr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> !</l>
					<l n="35" num="5.3"><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="35.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.3">bl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gu<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w></l>
					<l n="36" num="5.4"><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="36.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="37" num="5.5"><w n="37.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="37.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l</w> <w n="37.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="y" type="vs" value="1" rule="447">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
					<l n="38" num="5.6"><w n="38.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.2">m<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="38.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
					<l n="39" num="5.7"><w n="39.1"><seg phoneme="ɛ" type="vs" value="1" rule="198">E</seg>st</w> <w n="39.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="39.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.4">m<seg phoneme="o" type="vs" value="1" rule="443">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
					<l n="40" num="5.8"><w n="40.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg></w> <w n="40.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="40.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="40.4">g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !…</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><w n="41.1">K<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="41.2">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="41.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>,</l>
					<l n="42" num="6.2"><w n="42.1">S<seg phoneme="a" type="vs" value="1" rule="339">a</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="42.3">c<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w></l>
					<l n="43" num="6.3"><w n="43.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="43.2">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="43.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w>,</l>
					<l n="44" num="6.4"><w n="44.1">C<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="44.2">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="44.3">b<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="45" num="6.5"><w n="45.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="45.2">dr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="45.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="45.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
					<l n="46" num="6.6"><w n="46.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="46.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="46.3">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="46.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
					<l n="47" num="6.7"><w n="47.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.2">l</w>'<w n="47.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
					<l n="48" num="6.8"><w n="48.1">T<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="48.2">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="48.3"><seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1"><w n="49.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="49.2">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="49.3">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
					<l n="50" num="7.2"><w n="50.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.2">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="50.3">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
					<l n="51" num="7.3"><w n="51.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="51.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="51.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>p</w> <w n="51.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.5">bl<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
					<l n="52" num="7.4"><w n="52.1">Ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>, <w n="52.2">l</w>'<w n="52.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="52.4">t</w>'<w n="52.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="53" num="7.5"><w n="53.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="53.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="53.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> <w n="53.4">G<seg phoneme="o" type="vs" value="1" rule="317">au</seg>l<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>,</l>
					<l n="54" num="7.6"><w n="54.1">Gr<seg phoneme="a" type="vs" value="1" rule="339">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="54.2"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="54.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="54.4"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>xpl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>ts</w>,</l>
					<l n="55" num="7.7"><w n="55.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="55.2"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="55.3">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w></l>
					<l n="56" num="7.8"><w n="56.1">F<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="56.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="56.3">t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
			</div></body></text></TEI>