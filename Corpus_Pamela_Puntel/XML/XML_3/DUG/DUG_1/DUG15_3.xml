<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG15">
				<head type="main">LES BAVAROIS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="1.2">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">cr<seg phoneme="y" type="vs" value="1" rule="453">u</seg><seg phoneme="o" type="vs" value="1" rule="317">au</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>, <w n="1.5">d</w>'<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>st<seg phoneme="y" type="vs" value="1" rule="449">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">On</seg></w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.6">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>x</w> <w n="3.2">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="4.2">v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">Em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="4.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="4.6">R<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w>…</l>
					<l n="5" num="1.5"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.3">m<seg phoneme="e" type="vs" value="1" rule="352">e</seg>tt<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.5">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="5.7">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="6.2">l</w>'<w n="6.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.5">B<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> ?</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="361">en</seg>s</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">V<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> <w n="7.6">Th<seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn</w> <w n="7.7">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.2"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">L<seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">Cr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>,</l>
					<l n="9" num="2.3"><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.3">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.5">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="9.6">g<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.4"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="10.5">b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> ;</l>
					<l n="11" num="2.5"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">l</w>'<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="2.6"><w n="12.1">C</w>'<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="12.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>…</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.2">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.4">m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="14" num="3.2"><w n="14.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.3">p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.6">v<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> ;</l>
					<l n="15" num="3.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="15.2">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>pl<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="15.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="16" num="3.4"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>ss<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="466">i</seg>n<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w>, <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="339">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.3">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>,</l>
					<l n="17" num="3.5"><w n="17.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.2">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="17.3">f<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.4">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>d<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="18" num="3.6"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="18.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="18.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="18.4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ct<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="19.2">n<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="19.3">qu</w>'<w n="19.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="19.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="19.6">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>th<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="20" num="4.2"><w n="20.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="20.2">pr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="20.3">m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w></l>
					<l n="21" num="4.3"><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.2">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="22" num="4.4"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="22.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="22.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="22.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="22.6">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w></l>
					<l n="23" num="4.5"><w n="23.1">Cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>bl<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="23.2">d</w>'<w n="23.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="23.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="23.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="24" num="4.6"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="24.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="24.3"><seg phoneme="i" type="vs" value="1" rule="496">y</seg></w> <w n="24.4">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="24.6">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>…</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>h</w> ! <w n="25.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="25.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="26" num="5.2"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="26.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="26.4">l</w>'<w n="26.5"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>nn<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w></l>
					<l n="27" num="5.3"><w n="27.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="27.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="27.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="27.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="27.6">t<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="28" num="5.4"><w n="28.1">D<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="28.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="28.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w></l>
					<l n="29" num="5.5"><w n="29.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu</w>'<w n="29.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="29.3">l<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="29.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="29.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="29.6">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="30" num="5.6"><w n="30.1">L<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="30.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s</w> <w n="30.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="30.4">r<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="30.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g</w> !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="31.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.3">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w> <w n="31.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="31.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.6">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="32" num="6.2"><w n="32.1">Qu</w>'<w n="32.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="32.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="32.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="32.5">n<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l</w> <w n="32.6">n</w>'<w n="32.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.8">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>,</l>
					<l n="33" num="6.3"><w n="33.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.2">l</w>'<w n="33.3"><seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="33.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="34" num="6.4"><w n="34.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="34.3">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="34.4">chr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> <w n="34.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="34.6">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w></l>
					<l n="35" num="6.5"><w n="35.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="35.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="35.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="35.4"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="o" type="vs" value="1" rule="443">o</seg>n<seg phoneme="i" type="vs" value="1" rule="481">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="36" num="6.6"><w n="36.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="36.2">l</w>'<w n="36.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="36.4">Cr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>f<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> !…</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="37.2">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.3">tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>b<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="37.4">d</w>'<w n="37.5">h<seg phoneme="i" type="vs" value="1" rule="492">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="38" num="7.2"><w n="38.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="38.2">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="38.3"><seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.4">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>,</l>
					<l n="39" num="7.3"><w n="39.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="39.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="39.3">B<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rk</w> <w n="39.4">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="39.5">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="39.6">m<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="40" num="7.4"><w n="40.1">L<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="40.3">m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="40.4">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="40.5">f<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>,</l>
					<l n="41" num="7.5"><w n="41.1">N<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="41.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="41.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="41.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="42" num="7.6"><w n="42.1">D</w>'<w n="42.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="42.4">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="42.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="42.6">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w>…</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="43.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="43.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="43.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="43.5">cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="43.6">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="44" num="8.2"><w n="44.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.2">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="44.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="44.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.5">p<seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>,</l>
					<l n="45" num="8.3"><w n="45.1">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="45.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="45.3">W<seg phoneme="a" type="vs" value="1" rule="339">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="45.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.5">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="46" num="8.4"><w n="46.1">C<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="46.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rch<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="46.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="46.4">l</w>'<w n="46.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>,</l>
					<l n="47" num="8.5"><w n="47.1">N</w>'<w n="47.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="47.3">cr<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="47.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.5">t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="48" num="8.6"><w n="48.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="48.3">qu</w>'<w n="48.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="48.5">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="48.6">t</w>'<w n="48.7"><seg phoneme="o" type="vs" value="1" rule="434">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
				</lg>
			</div></body></text></TEI>