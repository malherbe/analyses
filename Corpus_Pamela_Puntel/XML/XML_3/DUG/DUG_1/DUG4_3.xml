<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG4">
				<head type="main">AVANT SARREBRUCK</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.2">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="1.3">f<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="1.4">P<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="2.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">s<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.7">m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="3.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="3.6">P<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">J</w>'<w n="4.2">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="381">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>m<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="5.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="5.3">f<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="6.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="6.5">P<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w></l>
					<l n="7" num="1.7"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.3">P<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="7.4">n</w>'<w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="8" num="1.8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w>, <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="8.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="9.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="9.5">b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sc<seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="362">en</seg></w></l>
					<l n="10" num="2.2"><w n="10.1">M<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="10.2">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.6">gu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="2.3"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="11.3">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="11.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="11.6">chr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w></l>
					<l n="12" num="2.4"><w n="12.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="12.2">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="12.3"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="13" num="2.5"><w n="13.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="13.2">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="13.3">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">v<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w>,</l>
					<l n="14" num="2.6"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="14.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="14.3">pl<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="14.5">l</w>'<w n="14.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w>,</l>
					<l n="15" num="2.7"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">n</w>'<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="15.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="15.6">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="15.7">d</w>'<w n="15.8"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="16" num="2.8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="16.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="16.3">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="17.2">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="17.5">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w></l>
					<l n="18" num="3.2"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="18.2">l</w>'<w n="18.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="18.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="18.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>s</w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="19" num="3.3"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">v<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="19.3">tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="19.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="19.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.6">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w></l>
					<l n="20" num="3.4"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="20.2">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>t<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="20.3">v<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="21" num="3.5"><w n="21.1"><seg phoneme="y" type="vs" value="1" rule="452">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.2">f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="21.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">m<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.6">br<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>,</l>
					<l n="22" num="3.6"><w n="22.1">P<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w> <w n="22.2">n</w>'<w n="22.3"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="22.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="22.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w></l>
					<l n="23" num="3.7"><w n="23.1">D</w>'<w n="23.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">pl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>str<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="23.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.6">v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="24" num="3.8"><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="24.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> ! <w n="24.4">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="25.2">l</w>'<w n="25.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">Im</seg>p<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="25.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t</w> <w n="25.6">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>,</l>
					<l n="26" num="4.2"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="317">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="26.2">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="402">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.4">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="26.5">l</w>'<w n="26.6"><seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="27" num="4.3"><w n="27.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">c<seg phoneme="ɛ" type="vs" value="1" rule="351">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="27.4">d</w>'<w n="27.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>pl<seg phoneme="o" type="vs" value="1" rule="443">o</seg>r<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="27.6">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>,</l>
					<l n="28" num="4.4"><w n="28.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">l</w>'<w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="28.5">g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="29" num="4.5"><w n="29.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="29.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="29.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ls</w> <w n="29.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="29.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="29.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="29.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>,</l>
					<l n="30" num="4.6"><w n="30.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="30.2">t</w>-<w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="30.5">pr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="30.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="30.7">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>,</l>
					<l n="31" num="4.7"><w n="31.1">Tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.2">b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>il</w> <w n="31.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="31.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?…</l>
					<l n="32" num="4.8"><w n="32.1">Cr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="32.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> : <w n="32.3">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="32.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="33.2">l<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="33.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.4">M<seg phoneme="œ" type="vs" value="1" rule="150">on</seg>s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="396">eu</seg>r</w> <w n="33.5"><seg phoneme="o" type="vs" value="1" rule="434">O</seg>ll<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w></l>
					<l n="34" num="5.2"><w n="34.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="34.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="34.3">c<seg phoneme="œ" type="vs" value="1" rule="248">œu</seg>r</w> <w n="34.4">n<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>j<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="34.6">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="35" num="5.3"><w n="35.1">L</w>'<w n="35.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="35.3">qu</w>'<w n="35.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="35.5">h<seg phoneme="a" type="vs" value="1" rule="339">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="35.7">tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>,</l>
					<l n="36" num="5.4"><w n="36.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="36.2">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="36.3">d</w>'<w n="36.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="36.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="37" num="5.5"><w n="37.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="37.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="37.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="37.5">ch<seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w>, <w n="37.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="37.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="37.8">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w>,</l>
					<l n="38" num="5.6"><w n="38.1">D<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="38.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="38.3">p<seg phoneme="o" type="vs" value="1" rule="317">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.4">P<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg></w>,</l>
					<l n="39" num="5.7"><w n="39.1">M</w>'<w n="39.2"><seg phoneme="ɛ" type="vs" value="1" rule="338">a</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="39.3">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="39.4">d<seg phoneme="œ" type="vs" value="1" rule="405">eu</seg>il</w> <w n="39.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="39.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="40" num="5.8"><w n="40.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="40.2">c</w>'<w n="40.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="40.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w>, <w n="40.5">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="40.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>