<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP1">
				<head type="main">L'INVASION</head>
				<head type="sub">1870-1871</head>
				<head type="number">I</head>
					<head type="main">PRÉLUDE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="1.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="466">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="1.5">f<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">p<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="2.7">r<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="3.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="3.4">fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">D</w>'<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.4">t<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="7.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.5">ch<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="8.2">R<seg phoneme="ɔ" type="vs" value="1" rule="440">o</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="8.4">Sp<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="9.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="9.3">j</w>'<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="9.5">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="9.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="9.7">h<seg phoneme="i" type="vs" value="1" rule="467">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="10.3">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="10.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> <w n="10.5">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>,</l>
					<l n="11" num="3.3"><w n="11.1">J</w>'<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="11.3">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="11.4">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="11.5">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="11.6">t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="11.7">gl<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="12.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.3">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>rs</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>cr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="13.4">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w>, <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="13.7">cr<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="14" num="4.2"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.3">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="14.4">t<seg phoneme="i" type="vs" value="1" rule="492">y</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="14.6">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>mm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w></l>
					<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w>, <w n="15.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.5">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="16" num="4.4"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="16.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>, <w n="17.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.5">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ps</w> <w n="17.6">d</w>'<w n="17.7"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ls</w> <w n="18.3">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="18.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">d<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ds</w>,</l>
					<l n="19" num="5.3"><w n="19.1">J</w>'<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg></w> <w n="19.3">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="19.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>p<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="5.4"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.3">l<seg phoneme="e" type="vs" value="1" rule="408">é</seg>gu<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="20.5">t<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="20.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Août-Octobre 1870.</date>
						<date when="1871">Octobre-Février 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>