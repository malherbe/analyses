<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP32">
				<head type="number">XXXII</head>
				<head type="main">LA CHARGE DES ZOUAVES PONTIFICAUX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Tr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="1.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> <w n="1.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="1.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.6">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="1.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="1.8">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="1.9">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w>.</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="2.4">qu</w>'<w n="2.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.6">n</w>'<w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="2.8">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="2.9">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="3.2">l</w>'<w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="3.4">n</w>'<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="3.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>t</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.8">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.9">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rtr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="4.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="4.4">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rs</w> <w n="5.4">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="5.5">z<seg phoneme="w" type="sc" value="0" rule="430">ou</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">f<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w>, <w n="6.6">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.8">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>, <w n="6.9">s<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="6.10">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.11">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w>,</l>
					<l n="7" num="1.7"><w n="7.1">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="7.3">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rge<seg phoneme="ɑ̃" type="vs" value="1" rule="310">an</seg>t</w> <w n="7.4">l</w>'<w n="7.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="8" num="1.8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.3">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">Un</seg></w> <w n="8.5">qu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt</w> <w n="8.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.9">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="9" num="1.9"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="9.2">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="9.5">qu</w>'<w n="9.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> ;</l>
					<l n="10" num="1.10"><w n="10.1">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> : <w n="10.5">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="10.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> !</l>
					<l n="11" num="1.11"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.3">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> ! <w n="11.5">n</w>'<w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="464">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="11.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="11.8">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="11.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.10">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="1.12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="12.2">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>t</w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.4">M<seg phoneme="o" type="vs" value="1" rule="317">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ts</w> <w n="12.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.7">qu</w>'<w n="12.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="12.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="13" num="1.13"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">En</seg></w> <w n="13.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="13.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="13.5">s</w>'<w n="13.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="13.8">c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w></l>
					<l n="14" num="1.14"><w n="14.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.2">qu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> <w n="14.4">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.7">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="14.8">m<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<l n="15" num="1.15"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="15.2">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="15.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.7">b<seg phoneme="a" type="vs" value="1" rule="342">a</seg><seg phoneme="j" type="sc" value="0" rule="474">ï</seg><seg phoneme="o" type="vs" value="1" rule="443">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<l part="I" n="16" num="1.16"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">s<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>r</w>, <w n="16.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="16.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="16.5">l</w>'<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> : <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>c<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="16.8">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> ! </l>
					<l part="F" n="16" num="1.16">— <w n="16.9">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l part="I" n="17" num="1.17">— <w n="17.1">Bl<seg phoneme="e" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w>. </l>
					<l part="M" n="17" num="1.17">— <w n="17.2">Tr<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>ss<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> ? </l>
					<l part="M" n="17" num="1.17">— <w n="17.3">M<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>. </l>
					<l part="F" n="17" num="1.17"><w n="17.4">Qu<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="17.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.6">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> <w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="18" num="1.18"><w n="18.1">S<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="18.2">qu<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>ts</w> : <w n="18.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="18.5">t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> ; <w n="18.6">c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r</w> <w n="18.7">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="18.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.9">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.10"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="18.11">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w>…</l>
					<l n="19" num="1.19"><w n="19.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="19.4">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="19.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="19.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.8"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="19.9">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="19.10">h<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w>, <w n="20.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="20.3">l</w>'<w n="20.4"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w> <w n="20.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="385">ein</seg></w> <w n="20.6">d</w>'<w n="20.7">h<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="20.8"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="20.9">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="20.10">s<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="21" num="1.21"><w n="21.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="21.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="21.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="21.6">c<seg phoneme="ɔ" type="vs" value="1" rule="418">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="21.8">v<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="21.9">cr<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>,</l>
					<l n="22" num="1.22"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="22.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t</w> <w n="22.4">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> <w n="22.5">chr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>sts</w> <w n="22.6">fl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>,</l>
					<l n="23" num="1.23"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="23.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">qu</w>'<w n="23.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="23.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="23.8">d</w>'<w n="23.9"><seg phoneme="o" type="vs" value="1" rule="317">AU</seg>TR<seg phoneme="ə" type="ef" value="1" rule="e-22">E</seg>S</w> <w n="23.10">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="23.11">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="23.12">D<seg phoneme="e" type="vs" value="1" rule="408">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="1.24"><w n="24.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>d</w> <w n="24.2">Ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="24.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="24.5">s<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> <w n="24.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="24.7">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="24.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="24.9">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">10 Février 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>