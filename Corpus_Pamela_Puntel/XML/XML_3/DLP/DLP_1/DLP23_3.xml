<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP23">
				<head type="number">XXIII</head>
				<head type="main">L'AMI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="1.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">l</w>'<w n="1.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="1.10">c<seg phoneme="o" type="vs" value="1" rule="434">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="2" num="2.1"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w>, <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> <w n="2.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="2.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="2.9">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="2.10">n<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w>,</l>
					<l n="3" num="2.2"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="3.4">n<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>rs</w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="3.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.7">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="3.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="2.3"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="408">É</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.3">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="4.6">cl<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="4.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="5" num="2.4"><w n="5.1">C</w>'<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="5.4">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="5.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>-<w n="5.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>s</w>-<w n="5.9">d<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>x</w>,</l>
					<l n="6" num="2.5"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="6.2">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="6.3">s</w>'<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.8">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="6.9">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>.</l>
					<l n="7" num="2.6"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="7.4">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.6">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="7.7">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="7.8">l</w>'<w n="7.9"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="7.10"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="7.11">l</w>'<w n="7.12"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="8" num="2.7"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>h</w> ! <w n="8.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.4">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="8.7">n<seg phoneme="o" type="vs" value="1" rule="414">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="9" num="2.8"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.3">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="9.4">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">pr<seg phoneme="e" type="vs" value="1" rule="408">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.8">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w>,</l>
					<l n="10" num="2.9"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">c<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="10.5">l</w>'<w n="10.6"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="y" type="vs" value="1" rule="449">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="10.8">d</w>'<w n="10.9"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.10">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rs</w> :</l>
					<l n="11" num="2.10"><w n="11.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="11.2">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="11.3">c</w>'<w n="11.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="11.5">C<seg phoneme="o" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b</w> : <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">m<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="11.8">c</w>'<w n="11.9"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="11.10">Sh<seg phoneme="a" type="vs" value="1" rule="339">a</seg>kesp<seg phoneme="i" type="vs" value="1" rule="">ea</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="12" num="2.11"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w>, <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="12.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.8">l<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="12.9">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="13" num="2.12"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="439">o</seg><seg phoneme="j" type="sc" value="0" rule="495">y</seg><seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="13.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="13.6">tr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>rs</w> <w n="13.7">l</w>'<w n="13.8"><seg phoneme="o" type="vs" value="1" rule="443">O</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg></w>,</l>
					<l n="14" num="2.13"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="14.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="14.8">n<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>.</l>
					<l n="15" num="2.14"><w n="15.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ls</w> <w n="15.2">pr<seg phoneme="o" type="vs" value="1" rule="443">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w> <w n="15.3">d</w>'<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="15.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="15.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>ts</w> <w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="16" num="2.15"><w n="16.1">C</w>'<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="16.3">b<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="374">en</seg></w> <w n="16.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w>, <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="16.6">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="16.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>s</w> <w n="16.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="16.9">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="17" num="2.16"><w n="17.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">c</w>'<w n="17.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="17.4">h<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="63">e</seg>r</w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> <w n="17.6">qu</w>'<w n="17.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="17.8">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.9">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="17.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w></l>
				</lg>
				<lg n="3">
					<l n="18" num="3.1"><w n="18.1">D</w>'<w n="18.2"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="18.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="18.7">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="18.8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg></w> <w n="18.9">d</w>'<w n="18.10"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> :</l>
					<l n="19" num="3.2"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">s<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>s</w> <w n="19.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.5">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="19.6"><seg phoneme="u" type="vs" value="1" rule="425">où</seg></w> <w n="19.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="443">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.8">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="19.9">c<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
				</lg>
				<lg n="4">
					<l n="20" num="4.1"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="443">O</seg></w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="20.3">D<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> ! <w n="20.4">qu</w>'<w n="20.5"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="20.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t</w> <w n="20.7">c<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="20.9">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="20.10">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.11">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 22 octobre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>