<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP4">
				<head type="number">IV</head>
				<head type="main">LA CHARGE DES CUIRASSIERS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg></w> <w n="1.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">d<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="1.9">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="2" num="1.2"><w n="2.1">R<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> <w n="2.2">n</w>'<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="2.4">p<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="2.6">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w>, <w n="2.7">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="2.8">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="189">e</seg>ts</w>, <w n="2.9">n<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="2.10">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w> <w n="3.2">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>ts</w> <w n="3.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="3.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="3.5"><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="3.7">fr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> :</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="4.3">r<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w> <w n="4.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="4.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="4.7">d</w>'<w n="4.8"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="4.9">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w>,</l>
					<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="5.6">m<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>rtr<seg phoneme="j" type="sc" value="0" rule="470">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">T<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>ls</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="6.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="6.5">qu</w>'<w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="6.8">sc<seg phoneme="y" type="vs" value="1" rule="449">u</seg>lpt<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> <w n="6.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="6.10">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="7" num="1.7"><w n="7.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="7.2">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="7.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>br<seg phoneme="e" type="vs" value="1" rule="408">é</seg></w> <w n="7.5">h<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>t</w> <w n="7.6">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.7">v<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367">en</seg>t</w>.</l>
				</lg>
				<lg n="2">
					<l n="8" num="2.1"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="8.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="8.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="370">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="372">en</seg>t</w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="8.7">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
					<l n="9" num="2.2"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w> <w n="9.3">l</w>'<w n="9.4"><seg phoneme="e" type="vs" value="1" rule="169">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="9.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="9.7">pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.9">f<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.3"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="10.3">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>g<seg phoneme="i" type="vs" value="1" rule="466">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="10.4">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="10.5">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="10.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="11.3">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="11.4">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="301">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg></w>, <w n="11.6">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="11.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465">in</seg>gt</w> !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.2">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> <w n="12.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="12.4">f<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg></w> <w n="12.5">p<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="12.7">f<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> : <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="12.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg></w> !</l>
					<l n="13" num="4.2"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>, <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rs</w>, <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>, <w n="13.4">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r</w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w>, <w n="13.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>s</w> <w n="13.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="13.9">pl<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.3"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="14.2">c<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="346">er</seg>s</w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="14.5"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.6">m<seg phoneme="y" type="vs" value="1" rule="449">u</seg>r<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="14.7">h<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="304">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="15" num="5.1"><w n="15.1">H<seg phoneme="e" type="vs" value="1" rule="408">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> ! <w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="15.3">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="15.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">d<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>t</w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="15.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> !</l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="16.2">j<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="408">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="198">e</seg>st</w> <w n="16.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="456">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> : <w n="16.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="16.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="16.8">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="16.9">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w>.</l>
					<l n="17" num="6.2"><w n="17.1">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>c</w>-<w n="17.2">M<seg phoneme="a" type="vs" value="1" rule="339">a</seg>h<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="17.3">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>t</w> : <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>ts</w>, <w n="17.5">n<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="17.6">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="17.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="307">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="18" num="6.3"><w n="18.1">J<seg phoneme="y" type="vs" value="1" rule="449">u</seg>squ</w>'<w n="18.2"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="18.3">b<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>t</w>, <w n="18.4">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="240">e</seg>d</w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="18.6">p<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="e" type="vs" value="1" rule="240">e</seg>d</w>, <w n="18.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>l</w> <w n="18.8">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w> <w n="18.9">l<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r</w> <w n="18.10">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="18.11">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="19" num="6.4"><w n="19.1">V<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w> <w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="410">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="459">u</seg><seg phoneme="i" type="vs" value="1" rule="490">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w>, <w n="19.4">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="e" type="vs" value="1" rule="408">é</seg>s</w> ? <w n="19.5">R<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>st<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442">o</seg>r</w> !</l>
					<l n="20" num="6.5"><w n="20.1">S<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w>-<w n="20.2">v<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>s</w>, <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="20.4">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.6">ch<seg phoneme="a" type="vs" value="1" rule="339">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.8">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="20.9">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> !</l>
				</lg>
				<lg n="7">
					<l n="21" num="7.1"><w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="424">ou</seg>r</w> <w n="21.2">s<seg phoneme="o" type="vs" value="1" rule="317">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">dr<seg phoneme="a" type="vs" value="1" rule="339">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314">eau</seg></w> <w n="21.5">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="21.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="y" type="vs" value="1" rule="449">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.7"><seg phoneme="e" type="vs" value="1" rule="188">e</seg>t</w> <w n="21.8">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="21.9">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="7.2"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>l</w> <w n="22.2">f<seg phoneme="o" type="vs" value="1" rule="317">au</seg>t</w>, <w n="22.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">s<seg phoneme="a" type="vs" value="1" rule="339">a</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="o" type="vs" value="1" rule="317">au</seg></w> <w n="22.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="416">oin</seg>g</w>, <w n="22.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="22.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>r</w> <w n="22.9"><seg phoneme="y" type="vs" value="1" rule="452">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="22.10">h<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="23" num="7.3"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.2">j<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w>, <w n="23.5">d<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="483">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="409">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="23.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w>,</l>
					<l n="24" num="7.4"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg></w> <w n="24.2">d<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>x</w> <w n="24.3">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="346">ez</seg></w> <w n="24.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="24.6">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.7">Pr<seg phoneme="y" type="vs" value="1" rule="449">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg>s</w> !</l>
				</lg>
				<lg n="8">
					<l n="25" num="8.1"><w n="25.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">g<seg phoneme="e" type="vs" value="1" rule="408">é</seg>n<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="a" type="vs" value="1" rule="339">a</seg>l</w> <w n="25.3">M<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>l</w> <w n="25.4">r<seg phoneme="e" type="vs" value="1" rule="408">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>d</w> : <w n="25.5">V<seg phoneme="i" type="vs" value="1" rule="467">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="25.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="26" num="8.2"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="26.2">l</w>'<w n="26.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg></w> <w n="26.4">n</w>'<w n="26.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>d</w> <w n="26.6">pl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="26.7">r<seg phoneme="j" type="sc" value="0" rule="480">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="376">en</seg></w> : <w n="26.8">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="26.9">l<seg phoneme="y" type="vs" value="1" rule="449">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.10">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="443">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="27" num="8.3"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="339">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345">e</seg>c</w> <w n="27.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="211">en</seg>t</w> <w n="27.3">b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>t<seg phoneme="a" type="vs" value="1" rule="306">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>s</w> <w n="27.4">qu<seg phoneme="i" type="vs" value="1" rule="490">i</seg></w> <w n="27.5">n</w>'<w n="27.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="27.7">p<seg phoneme="a" type="vs" value="1" rule="339">a</seg>s</w> <w n="27.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> !</l>
				</lg>
				<lg n="9">
					<l n="28" num="9.1"><w n="28.1">R<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w> <w n="28.2">Gu<seg phoneme="i" type="vs" value="1" rule="484">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="28.3">v<seg phoneme="wa" type="vs" value="1" rule="419">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="28.4">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="28.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> ! <w n="28.6">Qu</w>'<w n="28.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="28.8">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s</w>-<w n="28.9">t<seg phoneme="y" type="vs" value="1" rule="449">u</seg></w> ?</l>
				</lg>
				<lg n="10">
					<l part="I" n="29" num="10.1"><w n="29.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>ls</w> <w n="29.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="29.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417">on</seg>t</w> <w n="29.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w> <w n="29.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="29.6">n<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>f</w>… </l>
					<l part="F" n="29" num="10.1">— <w n="29.7">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.8">m</w>'<w n="29.9"><seg phoneme="a" type="vs" value="1" rule="339">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="30" num="10.2"><w n="30.1">L<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="30.2">m<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rt</w> <w n="30.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">c<seg phoneme="ɛ" type="vs" value="1" rule="160">e</seg>s</w> <w n="30.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339">a</seg>ts</w> <w n="30.6">p<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>t</w> <w n="30.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>t<seg phoneme="e" type="vs" value="1" rule="346">er</seg></w> <w n="30.8"><seg phoneme="œ̃" type="vs" value="1" rule="451">un</seg></w> <w n="30.9">p<seg phoneme="o" type="vs" value="1" rule="443">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="413">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="31" num="10.3"><w n="31.1">M<seg phoneme="wa" type="vs" value="1" rule="422">oi</seg></w>, <w n="31.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.3">br<seg phoneme="i" type="vs" value="1" rule="467">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.4">m<seg phoneme="a" type="vs" value="1" rule="339">a</seg></w> <w n="31.5">pl<seg phoneme="y" type="vs" value="1" rule="452">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.6"><seg phoneme="o" type="vs" value="1" rule="317">au</seg>x</w> <w n="31.7"><seg phoneme="e" type="vs" value="1" rule="352">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438">o</seg>rts</w> <w n="31.8">s<seg phoneme="y" type="vs" value="1" rule="449">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rfl<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w>,</l>
					<l n="32" num="10.4"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="188">E</seg>t</w> <w n="32.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">pl<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="32.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173">en</seg></w> <w n="32.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>t</w> <w n="32.6"><seg phoneme="a" type="vs" value="1" rule="341">à</seg></w> <w n="32.7">n<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="32.8">h<seg phoneme="e" type="vs" value="1" rule="408">é</seg>r<seg phoneme="o" type="vs" value="1" rule="437">o</seg>s</w> <w n="32.9">p<seg phoneme="ɛ" type="vs" value="1" rule="357">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="449">u</seg>s</w></l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Château de Pray, 10 août.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>