<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE CRI D'ALARME</title>
				<title type="sub">HOMMAGE A LA FRANCE EN DEUIL</title>
				<title type="medium">Édition électronique</title>
				<author key="FSF">
					<name>
						<forename>Fs.</forename>
						<surname>F.</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>175 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FSF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le cri d'alarme : hommage à la France en deuil</title>
						<author>Fs. F.</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k6208181z/f1.image.r=%22LE%20CRI%20D'ALARME%22</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le cri d'alarme : hommage à la France en deuil</title>
								<author>Fs. F.</author>
								<imprint>
									<pubPlace>Montpellier</pubPlace>
									<publisher>J. Calas</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FSF1">
				<head type="main">LE CRI D'ALARME</head>
				<head type="sub">HOMMAGE A LA FRANCE EN DEUIL</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Domine, salva nos, perimus !</quote>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Né d'un perfide azur, un ténébreux orage</l>
					<l n="2" num="1.2">Soudain sur notre barque a déchaîné ses coups.</l>
					<l n="3" num="1.3">Plus d'étoile. oh ! comment échapper au naufrage ?</l>
					<l n="4" num="1.4">Plus de mât. c'en est fait ! — « Maître réveillez-vous…</l>
					<l n="5" num="1.5"><space unit="char" quantity="8"/>Pitié, pitié ! tristes victimes,</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"/>Nous roulons au fond des abîmes.</l>
					<l n="7" num="1.7">« Seigneur, nous périssons ! sauvez-nous, sauvez-nous ! »</l>
				</lg>
				<lg n="2">
					<l n="8" num="2.1">Oui, l'orage est affreux ; oui, la nuit est profonde.</l>
					<l n="9" num="2.2">D'un naufrage inouï l'univers est témoin…</l>
					<l n="10" num="2.3">Pour la France livrée aux caprices de l'onde,</l>
					<l n="11" num="2.4">D'un miracle du Ciel il serait grand besoin !</l>
					<l n="12" num="2.5"><space unit="char" quantity="8"/>Implorons-le de sa clémence ;</l>
					<l n="13" num="2.6"><space unit="char" quantity="8"/>Dieu seul, en ce péril immense,</l>
					<l n="14" num="2.7">Dieu seul peut dire au flot : «Tu n'iras pas plus loin ! »</l>
				</lg>
				<lg n="3">
					<l n="15" num="3.1">Dans un piège attiré, notre généreux père</l>
					<l n="16" num="3.2">D'un rival implacable a subi le courroux.</l>
					<l n="17" num="3.3">Sans armes, sans amis, sans espoir, ô misère !</l>
					<l n="18" num="3.4">Il est là, mutilé !! « Maître, réveillez-vous ! »</l>
					<l n="19" num="3.5"><space unit="char" quantity="8"/>Pitié ! pitié ! tristes victimes,</l>
					<l n="20" num="3.6"><space unit="char" quantity="8"/>Nous roulons au fond des abîmes.</l>
					<l n="21" num="3.7">« Seigneur, nous périssons. Sauvez-nous, sauvez-nous ! »</l>
				</lg>
				<lg n="4">
					<l n="22" num="4.1">Tel qu'un paralytique étendu sur sa couche</l>
					<l n="23" num="4.2">Supplie en vain ses fils autour de lui penchés,</l>
					<l n="24" num="4.3">Oui, le pays, en proie à l'ennemi farouche,</l>
					<l n="25" num="4.4">Pleure en vain devant nous ses lambeaux arrachés.</l>
					<l n="26" num="4.5"><space unit="char" quantity="8"/>Du Ciel implorons la clémence.</l>
					<l n="27" num="4.6"><space unit="char" quantity="8"/>Dieu seul, en ce péril immense,</l>
					<l n="28" num="4.7">Pourrait lui dire encor : « Levez-vous et marchez ! »</l>
				</lg>
				<lg n="5">
					<l n="29" num="5.1">Aux flots insidieux d'une rive étrangère</l>
					<l n="30" num="5.2">Nos frères ont puisé défaillance et dégoûts.</l>
					<l n="31" num="5.3">Vidée à trop longs traits, la coupe mensongère</l>
					<l n="32" num="5.4">Va causer leur trépas. « Maître, réveillez-vous ! »</l>
					<l n="33" num="5.5"><space unit="char" quantity="8"/>Pitié ! pitié ! tristes victimes,</l>
					<l n="34" num="5.6"><space unit="char" quantity="8"/>Nous roulons au fond des abîmes…</l>
					<l n="35" num="5.7">Seigneur, nous périssons. Sauvez-nous, sauvez-nous ! »</l>
				</lg>
				<lg n="6">
					<l n="36" num="6.1">Oui, notre lèvre impie a des pures ivresses</l>
					<l n="37" num="6.2">Trop longtemps oublié le salutaire émoi,</l>
					<l n="38" num="6.3">Et, tel qu'un vil lépreux s'agite en ses détresses,</l>
					<l n="39" num="6.4">Notre corps social chancelle en désarroi.</l>
					<l n="40" num="6.5"><space unit="char" quantity="8"/>Du Ciel implorons la clémence ;</l>
					<l n="41" num="6.6"><space unit="char" quantity="8"/>Dieu seul, en ce malheur immense,</l>
					<l n="42" num="6.7">Pourrait lui dire encor : « Lazare, lève-toi !… »</l>
				</lg>
				<lg n="7">
					<l n="43" num="7.1">Lève-toi !… — Mais comment, naufragée et gisante,</l>
					<l n="44" num="7.2">Nef chérie, ô pays brisé dans tes fiertés,</l>
					<l n="45" num="7.3">Comment, ô ma patrie, hélas ! agonisante,</l>
					<l n="46" num="7.4">Pourriez-vous dans votre ombre aspirer aux clartés ? —</l>
					<l n="47" num="7.5"><space unit="char" quantity="8"/>Témoins et victimes des choses,</l>
					<l n="48" num="7.6"><space unit="char" quantity="8"/>Aux effets demandons leurs causes</l>
					<l n="49" num="7.7">Et du fond des erreurs montons aux vérités !</l>
				</lg>
				<lg n="8">
					<l n="50" num="8.1">Refoulant dans nos seins les angoisses de l'heure,</l>
					<l n="51" num="8.2">Comme un fils, à l'aspect imprévu d'un cercueil,</l>
					<l n="52" num="8.3">A sa mère en danger tout à coup songe et pleure,</l>
					<l n="53" num="8.4">Tremblant pour son trésor de tendresse et d'orgueil ;</l>
					<l n="54" num="8.5"><space unit="char" quantity="8"/>Fiers de son passé magnanime,</l>
					<l n="55" num="8.6"><space unit="char" quantity="8"/>Rendons, au bord de son abîme,</l>
					<l n="56" num="8.7">Un hommage pieux à la patrie en deuil !</l>
				</lg>
				<lg n="9">
					<l n="57" num="9.1">Ah ! si nous mesurions nos pleurs à ta souffrance,</l>
					<l n="58" num="9.2">Quels yeux plus que nos yeux devraient en contenir ?</l>
					<l n="59" num="9.3">Et, situ périssais, quel autre peuple, ô France !</l>
					<l n="60" num="9.4">Toi qui les guidais tous aux champs de l'avenir,</l>
					<l n="61" num="9.5"><space unit="char" quantity="8"/>Quel autre, ô reine de l'histoire,</l>
					<l n="62" num="9.6"><space unit="char" quantity="8"/>De quatorze siècles de gloire</l>
					<l n="63" num="9.7">Léguerait à ses fils l'auguste souvenir ?</l>
				</lg>
				<lg n="10">
					<l n="64" num="10.1">Si tu disparaissais, si ton étoile heureuse</l>
					<l n="65" num="10.2">S’éteignait dans ton ciel tout à coup embrumé ;</l>
					<l n="66" num="10.3">Si, tout à coup, l'écho de ta voix généreuse</l>
					<l n="67" num="10.4">Cessait de retentir dans le monde alarmé,</l>
					<l n="68" num="10.5"><space unit="char" quantity="8"/>Qui remplacerait ta lumière ?</l>
					<l n="69" num="10.6"><space unit="char" quantity="8"/>Et quelle autre voix sur la terre</l>
					<l n="70" num="10.7">Irait porter l'espoir au cœur de l'opprimé ?</l>
				</lg>
				<lg n="11">
					<l n="71" num="11.1">Sans toi, sans le bon goût que l'univers t'envie,</l>
					<l n="72" num="11.2">Qui soutiendrait l'éclat des travaux opulents ?</l>
					<l n="73" num="11.3">Sans toi, dans les beaux-arts, idéal de la vie,</l>
					<l n="74" num="11.4">Qui saurait susciter et sacrer les talents ?</l>
					<l n="75" num="11.5"><space unit="char" quantity="8"/>Qui donc, pour rapprocher les mondes</l>
					<l n="76" num="11.6"><space unit="char" quantity="8"/>A travers les monts ou les ondes,</l>
					<l n="77" num="11.7">De ton noble génie aurait les fiers élans ?</l>
				</lg>
				<lg n="12">
					<l n="78" num="12.1">Personne ! et, cependant, de cette gloire ardue</l>
					<l n="79" num="12.2">Tu peux te repentir en voyant tant d'ingrats.</l>
					<l n="80" num="12.3">Tu fus trop magnanime, et cela t'a perdue.</l>
					<l n="81" num="12.4">Tant de bienfaits aussi ne se pardonnent pas !</l>
					<l n="82" num="12.5"><space unit="char" quantity="8"/>Des sœurs qui te restent fidèles</l>
					<l n="83" num="12.6"><space unit="char" quantity="8"/>Compte le nombre, et par les zèles</l>
					<l n="84" num="12.7">Interprète les vœux qu'elles formaient tout bas…</l>
				</lg>
				<lg n="13">
					<l n="85" num="13.1">Une d'elles, surtout, au niveau de sa haine</l>
					<l n="86" num="13.2">Élevait sourdement son calcul infernal,</l>
					<l n="87" num="13.3">Et, comme tu marchais confiante et sereine,</l>
					<l n="88" num="13.4">Sondant le sol miné sous ton char triomphal,</l>
					<l n="89" num="13.5"><space unit="char" quantity="8"/>Elle épiait l'heure insolente,</l>
					<l n="90" num="13.6"><space unit="char" quantity="8"/>Qui devait dans sa main sanglante</l>
					<l n="91" num="13.7">Faire tomber enfin le sceptre impérial.</l>
				</lg>
				<lg n="14">
					<l n="92" num="14.1">Et te voilà brisée !!! et des noirs précipices</l>
					<l n="93" num="14.2">Ta chute fait trembler les grands et longs échos,</l>
					<l n="94" num="14.3">Et voilà,-Dieu puissant, détournez ces calices ! —</l>
					<l n="95" num="14.4">Voilà que l'anarchie, aiguisant ses couteaux,</l>
					<l n="96" num="14.5"><space unit="char" quantity="8"/>Veut, en des luttes fratricides,</l>
					<l n="97" num="14.6"><space unit="char" quantity="8"/>Achever de ses mains livides</l>
					<l n="98" num="14.7">L'affreux déchirement de ta robe en lambeaux !</l>
				</lg>
				<lg n="15">
					<l n="99" num="15.1">Dans nos cœurs désolés méditons ces images ;</l>
					<l n="100" num="15.2">Sans leçon, les regrets demeurent impuissants.</l>
					<l n="101" num="15.3">Si du monde la France a forcé les hommages,</l>
					<l n="102" num="15.4">C'est qu'elle fut jadis et lumière et bon sens.</l>
					<l n="103" num="15.5"><space unit="char" quantity="8"/>Hélas ! s'il n'en est plus de même,</l>
					<l n="104" num="15.6"><space unit="char" quantity="8"/>C'est que de système en système,</l>
					<l n="105" num="15.7">De ces dieux sans autel s'est retiré l'encens.</l>
				</lg>
				<lg n="16">
					<l n="106" num="16.1">Chaque heure maintenant proclame sa maxime ;</l>
					<l n="107" num="16.2">Tout se dit, tout s'écrit, tout brave l'examen ;</l>
					<l n="108" num="16.3">Tout se peut, tout se voit, rien n'est vertu ni crime ;</l>
					<l n="109" num="16.4">Le forçat d'aujourd'hui se fait juge demain,</l>
					<l n="110" num="16.5"><space unit="char" quantity="8"/>Et la plus cruelle ineptie,</l>
					<l n="111" num="16.6"><space unit="char" quantity="8"/>S'appuyant de notre inertie,</l>
					<l n="112" num="16.7">Poursuit impunément son funeste chemin.</l>
				</lg>
				<lg n="17">
					<l n="113" num="17.1">L'Utopie, aliment d'envieuses folies,</l>
					<l n="114" num="17.2">Dans la foule entretient les espoirs corrupteurs ;</l>
					<l n="115" num="17.3">Des doctrines sans Dieu, sans loi, de fiel remplies,</l>
					<l n="116" num="17.4">Brisent en les raillant tous les freins protecteurs.</l>
					<l n="117" num="17.5"><space unit="char" quantity="8"/>Une théorie homicide,</l>
					<l n="118" num="17.6"><space unit="char" quantity="8"/>Préconisant le régicide,</l>
					<l n="119" num="17.7">Recrute en plein soleil de sombres sectateurs ;</l>
				</lg>
				<lg n="18">
					<l n="120" num="18.1">Et l'on s'étonne après, — dérision amère !—</l>
					<l n="121" num="18.2">De voir se déchaîner de sinistres essaims</l>
					<l n="122" num="18.3">Dont la hache brutale et le fusil sommaire,</l>
					<l n="123" num="18.4">Supprimant toute voix contraire à leurs desseins,</l>
					<l n="124" num="18.5"><space unit="char" quantity="8"/>Osent,— trop fatale insolence, —</l>
					<l n="125" num="18.6"><space unit="char" quantity="8"/>Prétendre, par la violence,</l>
					<l n="126" num="18.7">Imposer au pays un ramas d'assassins !</l>
				</lg>
				<lg n="19">
					<l n="127" num="19.1">Amis, il n'est que temps. levons-nous ! — l'incendie,</l>
					<l n="128" num="19.2">Spectre hideux, sanglant, monte sur l'horizon ;</l>
					<l n="129" num="19.3">Pour comprimer l'essor de sa rage hardie,</l>
					<l n="130" num="19.4">Marchons armés de foi, de cœur et de raison.</l>
					<l n="131" num="19.5"><space unit="char" quantity="8"/>Voyez ! déjà de porche en porche,</l>
					<l n="132" num="19.6"><space unit="char" quantity="8"/>Secouant son horrible torche,</l>
					<l n="133" num="19.7">Le fléau niveleur envahit la maison !</l>
				</lg>
				<lg n="20">
					<l n="134" num="20.1">Déjà, semant partout la ruine et les larmes,</l>
					<l n="135" num="20.2">Le torrent en grondant se déborde et s'étend.</l>
					<l n="136" num="20.3">Français, plus de partis ! jetons le cri d'alarmes !!!</l>
					<l n="137" num="20.4">L'hydre au front des beffrois en hurlant se suspend…</l>
					<l n="138" num="20.5"><space unit="char" quantity="8"/>Voyez !… du fond des ses ténèbres,</l>
					<l n="139" num="20.6"><space unit="char" quantity="8"/>Ivre et dardant ses dards funèbres,</l>
					<l n="140" num="20.7">Sur nous, rouge et fangeux, s'élance le serpent !</l>
				</lg>
				<lg n="21">
					<l n="141" num="21.1">Ce serpent, de ses plis soudain serrant l'étreinte,</l>
					<l n="142" num="21.2">Siffle, s'enfle… il s'attaque à l'autel, au foyer.</l>
					<l n="143" num="21.3">Pères, défendons-nous ! Autour de l'arche sainte,</l>
					<l n="144" num="21.4">Chrétiens, au bon combat courons nous essayer !</l>
					<l n="145" num="21.5"><space unit="char" quantity="8"/>A la famille, au sanctuaire,</l>
					<l n="146" num="21.6"><space unit="char" quantity="8"/>Chacun doit son bras, sa prière ;</l>
					<l n="147" num="21.7">C'est la dette du cœur et de l'âme à payer.</l>
				</lg>
				<lg n="22">
					<l n="148" num="22.1">Mais, cette noble dette une fois acquittée,</l>
					<l n="149" num="22.2">Le chrétien ne croit pas avoir atteint le but :</l>
					<l n="150" num="22.3">Dès qu'elle est abattue, à la main irritée</l>
					<l n="151" num="22.4">Il tend sa main en frère ; il parle de salut,</l>
					<l n="152" num="22.5"><space unit="char" quantity="8"/>Et parfois une étreinte, heureuse</l>
					<l n="153" num="22.6"><space unit="char" quantity="8"/>De son étreinte généreuse,</l>
					<l n="154" num="22.7">Répond,—de repentir augure et doux tribut.</l>
				</lg>
				<lg n="23">
					<l n="155" num="23.1">Oui, tel sera le prix d'un ferme et tendre zèle.</l>
					<l n="156" num="23.2">Combattons l'anarchie avec la liberté ;</l>
					<l n="157" num="23.3">Au sceptique opposons notre raison fidèle ;</l>
					<l n="158" num="23.4">Au malheureux aigri, disons fraternité,</l>
					<l n="159" num="23.5"><space unit="char" quantity="8"/>Et de la divine lumière</l>
					<l n="160" num="23.6"><space unit="char" quantity="8"/>Nous relèverons la bannière,</l>
					<l n="161" num="23.7">A ce cri trois fois saint : espoir, foi, charité !</l>
				</lg>
				<lg n="24">
					<l n="162" num="24.1">Oh ! que chacun apporte une voix, une obole ;</l>
					<l n="163" num="24.2">Qu'au doux concert du bien tous brûlent d'être admis ;</l>
					<l n="164" num="24.3">Que le pinceau, le luth, la plume, la parole,</l>
					<l n="165" num="24.4">Que la science et l'art, s'honorant d'être amis,</l>
					<l n="166" num="24.5"><space unit="char" quantity="8"/>De purs rayons sèment leur voie,</l>
					<l n="167" num="24.6"><space unit="char" quantity="8"/>Heureux d'arracher quelque proie</l>
					<l n="168" num="24.7">Aux poisons que l'enfer sur la France a vomis.</l>
				</lg>
				<lg n="25">
					<l n="169" num="25.1">C'est ainsi que l'esprit, les mœurs, l'âme attendrie,</l>
					<l n="170" num="25.2">A leurs divines fins rappelant tes enfants,</l>
					<l n="171" num="25.3">Tu pourras de tes maux, ô ma mère ! ô patrie !</l>
					<l n="172" num="25.4">Oublier les douleurs dans nos bras réchauffants,</l>
					<l n="173" num="25.5"><space unit="char" quantity="8"/>Et que tu redevras, ô France !</l>
					<l n="174" num="25.6"><space unit="char" quantity="8"/>Un autre avenir d'espérance</l>
					<l n="175" num="25.7">Au cœur régénéré de tes fils triomphants.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Montpellier, le 27 mars 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>