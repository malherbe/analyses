<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA GUERRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FVL">
					<name>
						<forename>F.</forename>
						<surname>V.</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>304 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FVL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LA GUERRE</title>
						<author>F. V.</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb456064253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA GUERRE</title>
								<author>F. V.</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FVL1">
				<head type="main">LA GUERRE</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<head type="main">LES FAITS</head>
					<lg n="1">
						<l n="1" num="1.1">Les connaissez-vous pas un vallon solitaire,</l>
						<l n="2" num="1.2">Caressé par la brise, habité par les fleurs,</l>
						<l n="3" num="1.3">Où l'on puisse oublier le reste de la terre,</l>
						<l n="4" num="1.4">Le passé, le présent, la guerre et ses horreurs ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Où l'on puisse oublier ceux qu'on hait, ceux qu'on aime,</l>
						<l n="6" num="2.2">Les peuples et les rois, les fous et les méchants,</l>
						<l n="7" num="2.3">Les lâches, les héros, la patrie elle-même,</l>
						<l n="8" num="2.4">Sanglante, humiliée avec tous ses enfants ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Si vous le connaissez, oh ! dites, que j'y vole,</l>
						<l n="10" num="3.2">M'ensevelir vivant au tombeau de l'oubli :</l>
						<l n="11" num="3.3">Je ne puis du vainqueurs contempler l'auréole,</l>
						<l n="12" num="3.4">Dont l'éclat est pourtant par le crime affaibli.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Vous ne répondez pas ? Faut-il à sa mémoire</l>
						<l n="14" num="4.2">Malgré soi confier un pareil souvenir ?</l>
						<l n="15" num="4.3">Qu'une page sans nom s'ajoute à votre histoire,</l>
						<l n="16" num="4.4">Sans pouvoir la soustraire aux yeux de l'avenir ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Nos petits-fils, lisant cette page sanglante,</l>
						<l n="18" num="5.2">Croiront de leurs aïeux le bon sens obscurci.</l>
						<l n="19" num="5.3">Aux barbares du Nord, quoi ! la France expirante,</l>
						<l n="20" num="5.4">Après sic mois de lutte, a dû crier merci !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">La France ! jusqu'alors en héros si féconde,</l>
						<l n="22" num="6.2">Qui sous tous les climats a cueilli des lauriers,</l>
						<l n="23" num="6.3">Dont le drapeau vainqueur a fait le tour du monde,</l>
						<l n="24" num="6.4">La France n'a pas pu défendre ses foyers !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Elle a, depuis six mois, subi tous les outrages</l>
						<l n="26" num="7.2">D'un vainqueur sans pitié, qui s'avançait toujours,</l>
						<l n="27" num="7.3">Semant sur son chemin la mort et les ravages,</l>
						<l n="28" num="7.4">Sans que rien du torrent pût arrêter le cours !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Elle a vu, tour à tour, tomber ses places fortes :</l>
						<l n="30" num="8.2">Strasbourg, Tour, Metz, Belfort, ses premiers boulevards ;</l>
						<l n="31" num="8.3">Les obus et la faim partout forçant les portes</l>
						<l n="32" num="8.4">En tuant l'habitant sans toucher aux remparts !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Paris même, Paris, le rendez-vous du monde,</l>
						<l n="34" num="9.2">Le foyer du talent, de l'esprit et du goût,</l>
						<l n="35" num="9.3">Après quatre longs mois de misère profonde,</l>
						<l n="36" num="9.4">La ville aux douze forts n'a pu rester debout !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Sa chute a terminé cette lutte sauvage</l>
						<l n="38" num="10.2">Sans assouvir, hélas ! la soif de ces pillards :</l>
						<l n="39" num="10.3">Après s'être repus de sang et de carnage,</l>
						<l n="40" num="10.4">Ces vautours altérés s'abreuvent de milliards.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Il leur faut plus encore, il leur faut des provinces ;</l>
						<l n="42" num="11.2">De la France tombée il leur faut un lambeau :</l>
						<l n="43" num="11.3">Bismarck ne serait point rangé parmi les princes</l>
						<l n="44" num="11.4">S'il ne se distinguait par ce crime nouveau.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Ce n'était point assez d'entasser les ruines,</l>
						<l n="46" num="12.2">Contre le droit des gens d'écraser nos palais :</l>
						<l n="47" num="12.3">Il faut, pour couronner de honteuses rapines,</l>
						<l n="48" num="12.4">A nos frères du Rhin ravir le nom français !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Eh bien ! arrachez-nous la Lorraine et l'Alsace,</l>
						<l n="50" num="13.2">Frappez-nous bien au cœur, apaisez votre faim,</l>
						<l n="51" num="13.3">Monstres, qui voudriez qu'un seule besace</l>
						<l n="52" num="13.4">Nous restât pour aller mendier à Berlin !</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Guillaume, remplis bien de nos riches dépouilles</l>
						<l n="54" num="14.2">Ces nombreux chariots partant pour l'étranger :</l>
						<l n="55" num="14.3">Tu laisseras nubien plus, sur le sol que tu souilles,</l>
						<l n="56" num="14.4">De cœurs pour te haïr, de bras pour nous venger.</l>
					</lg>
				</div>
				<div type="section" n="2">
				<head type="number">II</head>
					<head type="main">GUILLAUME</head>
					<lg n="1">
						<l n="57" num="1.1">Qui donc a provoqué cette guerre incroyable,</l>
						<l n="58" num="1.2">Qui souleva d'horreur le monde tout entier ?</l>
						<l n="59" num="1.3">Qui donc devant l'histoire en sera responsable ?</l>
						<l n="60" num="1.4">Est-ce la France ou toi, Guillaume au cœur d'acier ?</l>
					</lg>
					<lg n="2">
						<l n="61" num="2.1">Écoute, dans les faits la réponse est écrite ;</l>
						<l n="62" num="2.2">Le masque du prétexte est enfin déchiré :</l>
						<l n="63" num="2.3">Les faits ont démenti ta parole hypocrite,</l>
						<l n="64" num="2.4">Et le crime revient à qui l'a préparé.</l>
					</lg>
					<lg n="3">
						<l n="65" num="3.1">La France était en paix avec l'Europe entière,</l>
						<l n="66" num="3.2">Vivait sans défiance auprès des Allemands,</l>
						<l n="67" num="3.3">Des libertés qu'elle aime attendait la lumière,</l>
						<l n="68" num="3.4">De la Prusse en amis accueillait les enfants.</l>
					</lg>
					<lg n="4">
						<l n="69" num="4.1">Aux progrès du travail appliquant sa richesse,</l>
						<l n="70" num="4.2">La France y conviait ses nombreux ouvriers,</l>
						<l n="71" num="4.3">Rêvant pour chacun d'eux une heureuse vieillesse,</l>
						<l n="72" num="4.4">Et ne croyant plus même aux combats meurtriers.</l>
					</lg>
					<lg n="5">
						<l n="73" num="5.1">La France y conviait tous les peuples du monde,</l>
						<l n="74" num="5.2">Aux rives de la Seine appelait leurs produits,</l>
						<l n="75" num="5.3">Établissait entre eux une lutte féconde,</l>
						<l n="76" num="5.4">Dont Berlin, comme nous, devait cueillir les fruits.</l>
					</lg>
					<lg n="6">
						<l n="77" num="6.1">Ne te souviens-il plus de ce tournoi splendide,</l>
						<l n="78" num="6.2">Unique jusqu'alors, visité par les rois,</l>
						<l n="79" num="6.3">Que toi-même, ô Guillaume, en ton âme perfide,</l>
						<l n="80" num="6.4">Tu voulus, comme un autre applaudir de la voix ?</l>
					</lg>
					<lg n="7">
						<l n="81" num="7.1">Tu vins pour écarter les soupçons de la France,</l>
						<l n="82" num="7.2">Pour jeter sur Paris ton regard d'épervier :</l>
						<l n="83" num="7.3">Car, de la Prusse à nous en voyant la distance,</l>
						<l n="84" num="7.4">Déjà tu souriais à tes canons d'acier.</l>
					</lg>
					<lg n="8">
						<l n="85" num="8.1">Au gré de tes désirs la France trop vantée,</l>
						<l n="86" num="8.2">Son commerce, ses arts, son passé, sa splendeur,</l>
						<l n="87" num="8.3">Dans les conseils des rois sa voix trop écoutée,</l>
						<l n="88" num="8.4">Déjà depuis longtemps avaient blessé ton cœur.</l>
					</lg>
					<lg n="9">
						<l n="89" num="9.1">Aussi que faisais-tu sur l'Elbe et la Baltique,</l>
						<l n="90" num="9.2">Avec ton vieux de Moltke et ton damné Bismarck,</l>
						<l n="91" num="9.3">Depuis qu'à Sadowa ta main diabolique</l>
						<l n="92" num="9.4">Avait brisé l'Autriche après le Danemark ?</l>
					</lg>
					<lg n="10">
						<l n="93" num="10.1">Cinquante ans de progrès n'ayant pu dans ton âme</l>
						<l n="94" num="10.2">Étouffer les ardeurs de ton ambition,</l>
						<l n="95" num="10.3">D'un complot fatal tu préparais la trame,</l>
						<l n="96" num="10.4">Pour tuer d'un seul coup la Grande Nation.</l>
					</lg>
					<lg n="11">
						<l n="97" num="11.1">Former un seul faisceau des peuples d'Allemagne ;</l>
						<l n="98" num="11.2">Leur montrer sur le Rhin un factice danger ;</l>
						<l n="99" num="11.3">Te poser à leurs yeux en nouveau Charlemagne,</l>
						<l n="100" num="11.4">Réveiller dans leurs cœurs des revers à venger ;</l>
					</lg>
					<lg n="12">
						<l n="101" num="12.1">Leur parler d'Iéna, d'Ulm et de Ratisbonne,</l>
						<l n="102" num="12.2">Pour rallumer en eux le désir des combats ;</l>
						<l n="103" num="12.3">Pour placer sur ton front une antique couronne,</l>
						<l n="104" num="12.4">De tous les Allemands faire autant de soldats ;</l>
					</lg>
					<lg n="13">
						<l n="105" num="13.1">Armer, discipliner tes cohortes, nombreuses</l>
						<l n="106" num="13.2">Comme les grains de sable au bord de l'Océan ;</l>
						<l n="107" num="13.3">Derrière elles lever ces colonnes fangeuses</l>
						<l n="108" num="13.4">De bandits et d'escrocs, dignes de Gengis-Khan ;</l>
					</lg>
					<lg n="14">
						<l n="109" num="14.1">Amasser des engins de mort et de ruine,</l>
						<l n="110" num="14.2">Fondre plus de canons que dix autres États,</l>
						<l n="111" num="14.3">En système ériger le vol et la rapine,</l>
						<l n="112" num="14.4">Décréter contre nous mille autres attentats ;</l>
					</lg>
					<lg n="15">
						<l n="113" num="15.1">De bandes d'espions empoisonner nos villes,</l>
						<l n="114" num="15.2">Serpents qui se glissent jusque sous l'humble toit ;</l>
						<l n="115" num="15.3">Colorer tes projets de prétextes habiles,</l>
						<l n="116" num="15.4">Proclamer que la force est au-dessus du droit ;</l>
					</lg>
					<lg n="16">
						<l n="117" num="16.1">Des autres nations acheter le silence,</l>
						<l n="118" num="16.2">Faire avec la Russie un pacte clandestin ;</l>
						<l n="119" num="16.3">Être insensible à tout, aux cris de la souffrance,</l>
						<l n="120" num="16.4">Aux sanglots de la veuve, aux pleurs de l'orphelin ;</l>
					</lg>
					<lg n="17">
						<l n="121" num="17.1">Enfin, quand tout fut prêt, de douze cent mille hommes</l>
						<l n="122" num="17.2">Inonder notre sol, surpris, épouvanté,</l>
						<l n="123" num="17.3">Sache-le bien, Guillaume, à l'époque où nous sommes,</l>
						<l n="124" num="17.4">C'est un crime, vois-tu, de lèse-humanité !!!</l>
					</lg>
					<lg n="18">
						<l n="125" num="18.1">Va. ce Dieu de bonté que ta bouche blasphème</l>
						<l n="126" num="18.2">Ne bénira jamais les mortels scélérats :</l>
						<l n="127" num="18.3">Ce Dieu, tu le sais bien, il commande qu'on s'aime,</l>
						<l n="128" num="18.4">Et pour s'entre-tuer on ne l'invoque pas !</l>
					</lg>
				</div>
				<div type="section" n="3">
				<head type="number">III</head>
					<head type="main">NAPOLÉON</head>
					<lg n="1">
						<l n="129" num="1.1">Toi, monarque tombé dans le sang et la boue,</l>
						<l n="130" num="1.2">Comme un ver écrasé sous le pied d'un Titan ;</l>
						<l n="131" num="1.3">Toi, sans remords au cœur, sans larmes sur la joue,</l>
						<l n="132" num="1.4">Qui t'éloignas maudit des plaines de Sedan ;</l>
					</lg>
					<lg n="2">
						<l n="133" num="2.1">Toi qui régnas vingt ans usr cette pauvre France,</l>
						<l n="134" num="2.2">Qui t'a, dans ses scrutins, par trois fois acclamé,</l>
						<l n="135" num="2.3">Qui ne t'a marchandé ni l'or ni la puissance,</l>
						<l n="136" num="2.4">Qui voyait dans ton fils son enfant bien-aimé ;</l>
					</lg>
					<lg n="3">
						<l n="137" num="3.1">Qu'as tu fait de ce nom, créé par le génie,</l>
						<l n="138" num="3.2">Qu'on lira désormais : lâcheté, trahison ?</l>
						<l n="139" num="3.3">Qu'as tu fait, réponds-moi, de ma chère patrie,</l>
						<l n="140" num="3.4">Dont les maux sont si grands qu'ils troublent sa raison ?</l>
					</lg>
					<lg n="4">
						<l n="141" num="4.1">Qu'as tu fait de son or, qu'as-tu fait de sa gloire,</l>
						<l n="142" num="4.2">De son sang généreux, de son intégrité ?</l>
						<l n="143" num="4.3">Va, ton règne est flétri ! l'inexorable histoire</l>
						<l n="144" num="4.4">Le montrera du doigt à la postérité !</l>
					</lg>
					<lg n="5">
						<l n="145" num="5.1">N'as-tu pas vu, dis-moi, de loin venir l'orage ?</l>
						<l n="146" num="5.2">N'as-tu pas vu la Prusse à tes côtés grandir ?</l>
						<l n="147" num="5.3">N'as-tu point entendu la voix prudente et sage</l>
						<l n="148" num="5.4">De Stoffel, te disant : « Prends garde, il faut agir » ?</l>
					</lg>
					<lg n="6">
						<l n="149" num="6.1">Pourtant de Sadowa tu sentis la menace :</l>
						<l n="150" num="6.2">A ce mot sur ton front ta couronne a frémi ;</l>
						<l n="151" num="6.3">Puis, comme au souvenir d'un rêve qui s'efface,</l>
						<l n="152" num="6.4">Tu t'es mis à sourire, et tu t'es rendormi !</l>
					</lg>
					<lg n="7">
						<l n="153" num="7.1">Et pendant qu'à Berlin on travaillait sans cesse,</l>
						<l n="154" num="7.2">Que de Moltke apprêtait ses nombreux bataillons,</l>
						<l n="155" num="7.3">Faisant étudier tes fautes, ta faiblesse,</l>
						<l n="156" num="7.4">Tu vivais à Paris dans tes illusions !</l>
					</lg>
					<lg n="8">
						<l n="157" num="8.1">Tu vivais rassuré par ce mot d'invincible</l>
						<l n="158" num="8.2">Dans lequel se drapaient ton orgueil et ton nom.</l>
						<l n="159" num="8.3">Pouvait-il à la peur se montrer accessible,</l>
						<l n="160" num="8.4">L'héritier des talents du grand Napoléon ?</l>
					</lg>
					<lg n="9">
						<l n="161" num="9.1">Tu vivais au milieu d'un cercle de mollesse,</l>
						<l n="162" num="9.2">De flatteurs achetés, d'agréables catins,</l>
						<l n="163" num="9.3">Cherchant à rajeunir ta précoce vieillesse,</l>
						<l n="164" num="9.4">Et croyant de la France accomplir les destins !</l>
					</lg>
					<lg n="10">
						<l n="165" num="10.1">Ton empire était creux de la base à la cime ;</l>
						<l n="166" num="10.2">Il n'avait qu'un éclat factice, mensonger ;</l>
						<l n="167" num="10.3">En aveugle il marchait sur le bord d'un abîme,</l>
						<l n="168" num="10.4">Et n'avait de talents que celui d'émarger.</l>
					</lg>
					<lg n="11">
						<l n="169" num="11.1">Du scandale d'en haut l'action délétère</l>
						<l n="170" num="11.2">De degrés en degrés arrivait jusqu'en bas.</l>
						<l n="171" num="11.3">Sans scrupule on puisait aux caisses de la guerre,</l>
						<l n="172" num="11.4">Sauf à diminuer l'effectif des combats.</l>
					</lg>
					<lg n="12">
						<l n="173" num="12.1">Mais Bismarck savait tout. En roué politique,</l>
						<l n="174" num="12.2">Il te prenait soudain dans un piège grossier :</l>
						<l n="175" num="12.3">En montrant à ton cœur un danger dynastique,</l>
						<l n="176" num="12.4">Il te fit dans l'arène élancer le premier</l>
					</lg>
					<lg n="13">
						<l n="177" num="13.1">Quelle aberration la puissance abusive</l>
						<l n="178" num="13.2">D'un monarque enivré ne produit-elle pas !</l>
						<l n="179" num="13.3">La France par ta voix devenait agressive,</l>
						<l n="180" num="13.4">Quand à peine elle avait trois cent mille soldats !</l>
					</lg>
					<lg n="14">
						<l n="181" num="14.1">Et c'est au lendemain d'un jour de confiance,</l>
						<l n="182" num="14.2">Quand le pays comptait que paix et liberté</l>
						<l n="183" num="14.3">D'un docile scrutin seraient la récompense,</l>
						<l n="184" num="14.4">Qu'aux hasards des combats tu l'as soudain jeté !</l>
					</lg>
					<lg n="15">
						<l n="185" num="15.1">Je sais bien que Guillaume était prêt à la lutte,</l>
						<l n="186" num="15.2">Qu'au prétexte bientôt un autre eût succédé ;</l>
						<l n="187" num="15.3">Mais doit-on de sa main accélérer sa chute,</l>
						<l n="188" num="15.4">Quand l'instant peut au moins en être retardé ?</l>
					</lg>
					<lg n="16">
						<l n="189" num="16.1">Doit-on précipiter son pays dans un gouffre,</l>
						<l n="190" num="16.2">Et plonger dans son cœur un homicide acier ?</l>
						<l n="191" num="16.3">Doit-on ?… Mais c'est assez, pour la France qui souffre,</l>
						<l n="192" num="16.4">Tu n'est plus qu'un bandit et qu'un aventurier !…</l>
					</lg>
				</div>
				<div type="section" n="4">
				<head type="number">IV</head>
					<head type="main">LES ROIS</head>
					<lg n="1">
						<l n="193" num="1.1">Ainsi toujours des rois la puissance funeste</l>
						<l n="194" num="1.2">De la France a causé la honte et le malheur</l>
						<l n="195" num="1.3">Quand une fois de plus un désastre l'atteste,</l>
						<l n="196" num="1.4">En conserverons-nous le souvenir au cœur ?</l>
					</lg>
					<lg n="2">
						<l n="197" num="2.1">Nous faudra-t-il encore un autre cataclysme</l>
						<l n="198" num="2.2">Pour dessiller nos yeux trop longtemps abusés ?</l>
						<l n="199" num="2.3">Il ne faudrait pourtant que du patriotisme</l>
						<l n="200" num="2.4">Pour reconnaître enfin que les rois sont usés.</l>
					</lg>
					<lg n="3">
						<l n="201" num="3.1">De leur dignité nos annales sont pleines :</l>
						<l n="202" num="3.2">Cupides, entêtés, quand, par l'agence affaiblis,</l>
						<l n="203" num="3.3">Sur leur tête ils ont vu s'accumuler nos haines,</l>
						<l n="204" num="3.4">Ils s'en vont le cœur vide et les coffres remplis.</l>
					</lg>
					<lg n="4">
						<l n="205" num="4.1">Pour tromper dans l'exil l'orgueil qui les dévore,</l>
						<l n="206" num="4.2">Ils s'en vont, entourés de quelques courtisans</l>
						<l n="207" num="4.3">Répétant chaque jour : « Sire, espérez encore,</l>
						<l n="208" num="4.4">En France nous avons de nombreux partisans. »</l>
					</lg>
					<lg n="5">
						<l n="209" num="5.1">Ils s'en vont de notre or faire un usage impie,</l>
						<l n="210" num="5.2">Contre notre repos sans cesse conspirer,</l>
						<l n="211" num="5.3">Et, pour être toujours traîtres à leur patrie,</l>
						<l n="212" num="5.4">Payer des malfaiteurs pour nous déshonorer.</l>
					</lg>
					<lg n="6">
						<l n="213" num="6.1">Oh ! oui, leur main est là dans nos moments de trouble ;</l>
						<l n="214" num="6.2">Dans l'ombre je la vois promener un tison.</l>
						<l n="215" num="6.3">Je les reconnais bien à leur conduite double,</l>
						<l n="216" num="6.4">Pour s'offrir en sauveurs allumant la maison.</l>
					</lg>
					<lg n="7">
						<l n="217" num="7.1">Les rois se comprenaient, du moins, au moyen âge.</l>
						<l n="218" num="7.2">Quand barons et seigneurs écrasaient de leur poids</l>
						<l n="219" num="7.3">Le peuple gémissant sous un dur esclavage,</l>
						<l n="220" num="7.4">Je comprends que la France ait acclamé ses rois :</l>
					</lg>
					<lg n="8">
						<l n="221" num="8.1">En s'appuyant sur elle, ils ont fait disparaître</l>
						<l n="222" num="8.2">L'hydre qui nous tenait dans ses plis enlacés.</l>
						<l n="223" num="8.3">La besogne achevée, ils n'ont plus raison d'être :</l>
						<l n="224" num="8.4">Qu'ils meurent en exil, nous en avons assez.</l>
					</lg>
					<lg n="9">
						<l n="225" num="9.1">Assez de fleurs de lis, d'aigles impériales ;</l>
						<l n="226" num="9.2">Assez de coqs gaulois, de symboles trompeurs,</l>
						<l n="227" num="9.3">De cortèges brillants, de majestés rivales,</l>
						<l n="228" num="9.4">Qui, sans nous rien donner, vivent de nos sueurs.</l>
					</lg>
					<lg n="10">
						<l n="229" num="10.1">Les rois déclassés depuis la grande époque</l>
						<l n="230" num="10.2">Où nous avons conquis franchises, liberté.</l>
						<l n="231" num="10.3">Ils n'ont vécu depuis que sur une équivoque :</l>
						<l n="232" num="10.4">Car un roi c'est un maître à perpétuité.</l>
					</lg>
					<lg n="11">
						<l n="233" num="11.1">Comment concilier ces erreurs d'un autre âge,</l>
						<l n="234" num="11.2">Les rois enfants, vieillards, restes du droit divin,</l>
						<l n="235" num="11.3">Avec le droit nouveau, l'universel suffrage,</l>
						<l n="236" num="11.4">Et de la nation le pouvoir souverain ?</l>
					</lg>
					<lg n="12">
						<l n="237" num="12.1">Quand un corps dangereux entre dans l'organisme,</l>
						<l n="238" num="12.2">Trouble les fonctions, dérange les ressorts,</l>
						<l n="239" num="12.3">La nature, attentive au jeu du mécanisme,</l>
						<l n="240" num="12.4">Pour expulser l'intrus combine ses efforts.</l>
					</lg>
					<lg n="13">
						<l n="241" num="13.1">Les rois sont des intrus dangereux pour la France,</l>
						<l n="242" num="13.2">Troublent le libre jeu des institutions,</l>
						<l n="243" num="13.3">Démontent les ressorts qui gênent leur puissance,</l>
						<l n="244" num="13.4">Et puis sont expulsés aux révolutions.</l>
					</lg>
					<lg n="14">
						<l n="245" num="14.1">Depuis Quatre-vingt-neuf, France, voilà leurs rôles ;</l>
						<l n="246" num="14.2">Et toi, souffrit par eux est ton unique lot,</l>
						<l n="247" num="14.3">Lorsque tu ne vas pas, chancelant sur tes pôles,</l>
						<l n="248" num="14.4">T'abîmer à Sedan ou bien à Waterloo.</l>
					</lg>
					<lg n="15">
						<l n="249" num="15.1">Pour te sauver encor des bras de l'anarchie,</l>
						<l n="250" num="15.2">O France, ne va pas demander leur retour !</l>
						<l n="251" num="15.3">Entre le despotisme et la démagogie,</l>
						<l n="252" num="15.4">Ne va pas osciller jusqu'à ton dernier jour !</l>
					</lg>
					<lg n="16">
						<l n="253" num="16.1">Car, pour cicatriser ta blessure profonde,</l>
						<l n="254" num="16.2">Il te faut de l'épargne et de la liberté,</l>
						<l n="255" num="16.3">Non celle qui détruit, mais celle qui féconde</l>
						<l n="256" num="16.4">Et répare les maux d'un règne détesté.</l>
					</lg>
				</div>
				<div type="section" n="5">
				<head type="number">V</head>
					<head type="main">LA RÉPARATION</head>
					<lg n="1">
						<l n="257" num="1.1">La réparation ! c'est le devoir suprême</l>
						<l n="258" num="1.2">S'imposant désormais à nos cœurs ulcérés ;</l>
						<l n="259" num="1.3">C'est le recueillement, c'est l'oubli de soi-même,</l>
						<l n="260" num="1.4">Pour ne voir que la France et ses flancs déchirés.</l>
					</lg>
					<lg n="2">
						<l n="261" num="2.1">C'est le travail pour tous, pour tous c'est la souffrance ;</l>
						<l n="262" num="2.2">C'est la privation assise à notre seuil</l>
						<l n="263" num="2.3">Pendant vingt ans peut-être ; et puis c'est la vengeance,</l>
						<l n="264" num="2.4">C'est la Prusse à nos pieds, ou la Prusse au cercueil.</l>
					</lg>
					<lg n="3">
						<l n="265" num="3.1">Français, nous le pouvons ; la tâche est grande et belle ;</l>
						<l n="266" num="3.2">Sous ce noble drapeau nous devons ranger,</l>
						<l n="267" num="3.3">Et, pour nous mettre à l’œuvre, apaiser la querelle</l>
						<l n="268" num="3.4">Qui nous souille encor plus que le joug étranger.</l>
					</lg>
					<lg n="4">
						<l n="269" num="4.1">Toi, Paris affolé, dépose tes colères,</l>
						<l n="270" num="4.2">Retourne à ton travail trop longtemps déserté ;</l>
						<l n="271" num="4.3">Et nous, épargnons-lui des larmes trop amères :</l>
						<l n="272" num="4.4">Inaugurons enfin l'ordre et la liberté.</l>
					</lg>
					<lg n="5">
						<l n="273" num="5.1">C'est là qu'est le salut. Sous cette double égide</l>
						<l n="274" num="5.2">Nous pouvons marcher droit au sentier du devoir.</l>
						<l n="275" num="5.3">L'autorité de tous est-elle moins solide</l>
						<l n="276" num="5.4">Que celle d'un monarque abusant du pouvoir ?</l>
					</lg>
					<lg n="6">
						<l n="277" num="6.1">De l'ordre social pour fondre les assises,</l>
						<l n="278" num="6.2">Livrons aux préjugés les plus rudes combats ;</l>
						<l n="279" num="6.3">Si du mal l'ignorance a permis les surprises,</l>
						<l n="280" num="6.4">Éclairons au plus tôt les assises d'en bas.</l>
					</lg>
					<lg n="7">
						<l n="281" num="7.1">Qu'une éducation virile, salutaire,</l>
						<l n="282" num="7.2">S'empare de l'enfant au sortir du berceau ;</l>
						<l n="283" num="7.3">Pour qu'il reste soumis à sa main tutélaire,</l>
						<l n="284" num="7.4">Avec soin de la femme élevons le niveau.</l>
					</lg>
					<lg n="8">
						<l n="285" num="8.1">Ainsi, le fils aura l'amour de la patrie ;</l>
						<l n="286" num="8.2">Il saura quel lui doit du cœur et des talents,</l>
						<l n="287" num="8.3">Et souffrant avec elle, et vivant de sa vie,</l>
						<l n="288" num="8.4">Pour la venger un jour, s'armera jusqu'aux dents.</l>
					</lg>
					<lg n="9">
						<l n="289" num="9.1">Alors, oh ! je te plains, Allemagne cruelle,</l>
						<l n="290" num="9.2">Qui n'as pas du vaincu ménagé la douleur :</l>
						<l n="291" num="9.3">Tu paieras de nos maux la dernière parcelle ;</l>
						<l n="292" num="9.4">D'un peuple libre et fort tu sauras la valeur.</l>
					</lg>
					<lg n="10">
						<l n="293" num="10.1">Nous n'avons succombé qu'à la force inouïe</l>
						<l n="294" num="10.2">Du nombre et des canons : nous les aurons alors,</l>
						<l n="295" num="10.3">Et nous aurons toujours cette antique furie</l>
						<l n="296" num="10.4">Dont tu connais le poids, si tu comptes tes morts.</l>
					</lg>
					<lg n="11">
						<l n="297" num="11.1">Oh ! oui, nous vengerons nos bourgades rasées,</l>
						<l n="298" num="11.2">Nos familles en deuil, nos villages détruits,</l>
						<l n="299" num="11.3">Nos frères sous le joug, nos villes écrasées,</l>
						<l n="300" num="11.4">Et comme des forçats nos prisonniers conduits.</l>
					</lg>
					<lg n="12">
						<l n="301" num="12.1">En attendant ce jour, pour soulager ma peine,</l>
						<l n="302" num="12.2">Je dirai ces forfaits à mes petits-enfants.</l>
						<l n="303" num="12.3">Moi qui prêchais l'amour, je prêcherai la haine,</l>
						<l n="304" num="12.4">La haine de la Prusse et celle des tyrans !</l>
					</lg>
				</div>
			</div></body></text></TEI>