<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">NOS RUINES</title>
				<title type="medium">Édition électronique</title>
				<author key="ANG">
					<name>
						<forename>Albert</forename>
						<surname>ANGOT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">ANG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>NOS RUINES</title>
						<author>ALBERT ANGOT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30021186v</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>NOS RUINES</title>
								<author>ALBERT ANGOT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DUNIOL ET CIE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANG11">
				<head type="main">L’ALLEMAGNE</head>
				<lg n="1">
					<l n="1" num="1.1">L’aigle portait au flanc une large blessure ;</l>
					<l n="2" num="1.2">L’incendie et le froid, les hommes, la nature</l>
					<l n="3" num="1.3">Paraissaient contre lui s’acharner à la fois.</l>
					<l n="4" num="1.4">Ainsi que des corbeaux qui croassent dans l’ombre,</l>
					<l n="5" num="1.5">Attentifs à sa perte et comptant sur leur nombre</l>
					<l n="6" num="1.6">S’agitaient nations et rois.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Et l’aigle pesamment revenait vers son aire</l>
					<l n="8" num="2.2">Voir si ses rejetons qu’il élevait naguère</l>
					<l n="9" num="2.3">Pour les rudes combats, — pouvaient le secourir,</l>
					<l n="10" num="2.4">Lorsque la bande noire, avec un cri sauvage,</l>
					<l n="11" num="2.5">Comme les tourbillons d’une grêle d’orage</l>
					<l n="12" num="2.6">Sur lui fondit pour l’assaillir.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Chacun voulait avoir sa part à la curée.</l>
					<l n="14" num="3.2">L’Europe était de sang à Leipsick altérée :</l>
					<l n="15" num="3.3">Prussiens et Bavarois, Wurtembergeois, Saxons,</l>
					<l n="16" num="3.4">Autrichiens, Weimariens, soldats de Westphalie,</l>
					<l n="17" num="3.5">L’Allemagne d’alors tant de fois assagie</l>
					<l n="18" num="3.6">Se ruait sur nos bataillons.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Malgré ses trahisons et la lutte inégale,</l>
					<l n="20" num="4.2">L’Allemagne accablant la France sa rivale</l>
					<l n="21" num="4.3">Avait un noble but, un but plein de grandeur :</l>
					<l n="22" num="4.4">Elle avait trop longtemps gémi sous notre chaîne ;</l>
					<l n="23" num="4.5">Elle la secouait le cœur rempli de haine</l>
					<l n="24" num="4.6">Et de fiel — contre l’oppresseur.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">C’était un grand réveil, comme une émeute immense ;</l>
					<l n="26" num="5.2">C’était ce sentiment, et cette impatience</l>
					<l n="27" num="5.3">Qu’éprouve un prisonnier, qui voit de sa prison</l>
					<l n="28" num="5.4">Un barreau — descellé par une main amie,</l>
					<l n="29" num="5.5">Et se prépare à fuir, quand la garde endormie</l>
					<l n="30" num="5.6">Permettra son évasion.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Un souffle de fierté mêlé d’indépendance,</l>
					<l n="32" num="6.2">Qu’elle avait recueilli jadis venant de France,</l>
					<l n="33" num="6.3">Animait ses enfants qui marchaient au combat.</l>
					<l n="34" num="6.4">Les Ruckert et les Arndt enflammaient leur courage ;</l>
					<l n="35" num="6.5">Chacun voulait être soldat.</l>
				</lg>
				<lg n="7">
					<l n="36" num="7.1">Les hymnes de Weber saluaient les bannières</l>
					<l n="37" num="7.2">Et faisaient tressaillir ces phalanges guerrières</l>
					<l n="38" num="7.3">Dont les dieux se nommaient jadis Bach et Mozart ;</l>
					<l n="39" num="7.4">On oubliait Schiller, le grand Goëthe lui-même</l>
					<l n="40" num="7.5">Qui semblait ignorer cette lutte suprême,</l>
					<l n="41" num="7.6">Perdu sur les sommets de l’art.</l>
				</lg>
				<lg n="8">
					<l n="42" num="8.1">Alors, à notre tour, nous vîmes la Patrie</l>
					<l n="43" num="8.2">Courber son noble front, par la douleur flétrie.</l>
					<l n="44" num="8.3">Notre sort était dur, mais non immérité.</l>
					<l n="45" num="8.4">Napoléon le Grand, ébloui par la Gloire,</l>
					<l n="46" num="8.5">Sur l’Europe abusait souvent de la victoire,</l>
					<l n="47" num="8.6">Et combattait la Liberté.</l>
				</lg>
				<lg n="9">
					<l n="48" num="9.1">Ah ! combien l’Allemagne, en son patriotisme</l>
					<l n="49" num="9.2">Poussé jusqu’à l’excès et jusqu’au fanatisme,</l>
					<l n="50" num="9.3">Était plus grand alors, qu’à cette heure où j’écris !</l>
					<l n="51" num="9.4">Ce n’est plus pour lever au ciel sa libre tête,</l>
					<l n="52" num="9.5">Mais bien pour obéir à l’esprit de conquête,</l>
					<l n="53" num="9.6">Qu’elle vient assiéger Paris.</l>
				</lg>
				<lg n="10">
					<l n="54" num="10.1">Autrefois, à Strasbourg, le drapeau tricolore</l>
					<l n="55" num="10.2">Dans le miroir du Rhin se reflétait encore,</l>
					<l n="56" num="10.3">Et la Pucelle de Metz qui fermait son corset,</l>
					<l n="57" num="10.4">Du haut de ses remparts regardant en silence</l>
					<l n="58" num="10.5">Les Prussiens et Blücher, ce type d’insolence,</l>
					<l n="59" num="10.6">Sentait son front qui rougissait.</l>
				</lg>
				<lg n="11">
					<l n="60" num="11.1">Plus heureuse aujourd’hui que Charles-Quint d’Espagne</l>
					<l n="61" num="11.2">Trouvant dans la famine une aide, — l’Allemagne</l>
					<l n="62" num="11.3">De la pudique Metz outrage la pudeur ;</l>
					<l n="63" num="11.4">Et le Prussien jaloux, entr’ouvrant sa ceinture,</l>
					<l n="64" num="11.5">Admire ses contours, et raille la figure</l>
					<l n="65" num="11.6">De Chevert — qu’elle a sur son cœur.</l>
				</lg>
				<lg n="12">
					<l n="66" num="12.1">Strasbourg ! — quels souvenirs ce nom seul ressuscite</l>
					<l n="67" num="12.2">Pour un cœur généreux, pour un cœur qui palpite !</l>
					<l n="68" num="12.3">Goëthe, n’est-ce pas là que tu venais t’asseoir,</l>
					<l n="69" num="12.4">Riche de l’avenir, et brillant de jeunesse,</l>
					<l n="70" num="12.5">Pour vider à longs traits la coupe de l’ivresse</l>
					<l n="71" num="12.6">De tout apprendre, et tout savoir ?</l>
				</lg>
				<lg n="13">
					<l n="72" num="13.1">Ah ! si tu revenais dans notre noble Alsace</l>
					<l n="73" num="13.2">Dont l’image enchantait ta mémoire vivace,</l>
					<l n="74" num="13.3">Lorsque tu recueillais tes souvenirs passés,</l>
					<l n="75" num="13.4">L’Alsace où tu connus l’aimable Frédérique,</l>
					<l n="76" num="13.5">Chaste apparition, dont l’ovale pudique</l>
					<l n="77" num="13.6">Brillait sous ses cheveux tressés,</l>
				</lg>
				<lg n="14">
					<l n="78" num="14.1">Certes, tu maudirais la Patrie allemande</l>
					<l n="79" num="14.2">Que tu voulais aussi voir un jour forte et grande,</l>
					<l n="80" num="14.3">En ne trouvant partout que cendres et débris !</l>
					<l n="81" num="14.4">L’Allemagne a passé !— La bombe incendiaire,</l>
					<l n="82" num="14.5">Qu’en ses canons lançait sa horde sanguinaire</l>
					<l n="83" num="14.6">N’a laissé que des murs noircis.</l>
				</lg>
				<lg n="15">
					<l n="84" num="15.1">Cette Bibliothèque, en richesses féconde,</l>
					<l n="85" num="15.2">Ces trésors précieux, où ton âme profonde</l>
					<l n="86" num="15.3">Aimait à se plonger, — tu la cherches en vain !</l>
					<l n="87" num="15.4">Werder, nouvel Omar, les jugeait inutiles :</l>
					<l n="88" num="15.5">Ce ne sont aujourd’hui que ruines stériles</l>
					<l n="89" num="15.6">Pour l’artiste et pour l’écrivain.</l>
				</lg>
				<lg n="16">
					<l n="90" num="16.1">Regarde un peu plus loin… ! — Ces fûts, ces colonnettes</l>
					<l n="91" num="16.2">Qui gisent en amas, ces fines statuettes</l>
					<l n="92" num="16.3">Paraissant regretter leurs fronts déshonorés,</l>
					<l n="93" num="16.4">Ce sont les noirs débris de cette Cathédrale</l>
					<l n="94" num="16.5">Qui levait dans les airs sa flèche triomphale</l>
					<l n="95" num="16.6">Fière de ses parvis sacrés.</l>
				</lg>
				<lg n="17">
					<l n="96" num="17.1">L’Allemagne a passé ! — La rosace gothique,</l>
					<l n="97" num="17.2">Les merveilleux vitraux, — l’horloge astronomique,</l>
					<l n="98" num="17.3">Tous ces chefs d’œuvre, tout, Goëthe, tout est brisé.</l>
					<l n="99" num="17.4">Tu n’entendrais plus l’orgue à la voix solennelle,</l>
					<l n="100" num="17.5">La bombe a complètement son œuvre criminelle.</l>
					<l n="101" num="17.6">Goëthe, l’Allemagne a passé — !</l>
				</lg>
				<lg n="18">
					<l n="102" num="18.1">Elle a passé sur nous, ainsi que la tempête,</l>
					<l n="103" num="18.2">Sourde à l’Humanité qui lui disait : — « Arrête ! — »</l>
					<l n="104" num="18.3">Implacable et brutale, aveugle en sa fureur,</l>
					<l n="105" num="18.4">Elle apprendra plus tard en lisant son histoire,</l>
					<l n="106" num="18.5">Que ces actes honteux ont terni sa victoire,</l>
					<l n="107" num="18.6">Et qu’elle a manqué de grandeur.</l>
				</lg>
				<lg n="19">
					<l n="108" num="19.1">Elle apprendra plus tard qu’elle a changé de chaîne.</l>
					<l n="109" num="19.2">Elle n’est plus Française, oui, mais elle est Prussienne ;</l>
					<l n="110" num="19.3">Elle n’est plus qu’un fief des rois de Brandebourg.</l>
					<l n="111" num="19.4">Son châtiment sera la crainte de la guerre,</l>
					<l n="112" num="19.5">Tant qu’il existera quelque Français sur terre</l>
					<l n="113" num="19.6">Pour venger le sort de Strasbourg ;</l>
				</lg>
				<lg n="20">
					<l n="114" num="20.1">Tant que Metz, subissant de honteuses caresses,</l>
					<l n="115" num="20.2">Nous enverra l’écho de ses mornes tristesses ;</l>
					<l n="116" num="20.3">Tant qu’il existera chez nous des arsenaux,</l>
					<l n="117" num="20.4">Des canons, des obus avec de la mitraille,</l>
					<l n="118" num="20.5">Des soldats, des marins, souhaitant la bataille,</l>
					<l n="119" num="20.6">Des flottes et des amiraux.</l>
				</lg>
				<lg n="21">
					<l n="120" num="21.1">Allez, bons Allemands, vous exercer aux armes,</l>
					<l n="121" num="21.2">Dans les camps des Prussiens pour voler aux alarmes ;</l>
					<l n="122" num="21.3">Vous aimes les combats ! vous en aurez demain.</l>
					<l n="123" num="21.4">Notre glaive d’acier tiré sur votre tête</l>
					<l n="124" num="21.5">Sera comme celui de l’Antiquité prête</l>
					<l n="125" num="21.6">A Damoclès, le Sicilien.</l>
				</lg>
				<lg n="22">
					<l n="126" num="22.1">Dites adieu, vassaux, à cette poésie,</l>
					<l n="127" num="22.2">A cette fraîche fleur aux parfums d’ambroisie,</l>
					<l n="128" num="22.3">A ces aimables arts, autrefois vos amours.</l>
					<l n="129" num="22.4">Vassaux, il faut partir : la Prusse vous appelle !</l>
					<l n="130" num="22.5">Soyez son bouclier, combattez devant elle,</l>
					<l n="131" num="22.6">Et pour elle mourez toujours.</l>
				</lg>
				<lg n="23">
					<l n="132" num="23.1">Pour vous plus de Lessing aux fécondes critiques,</l>
					<l n="133" num="23.2">De Goëthe, de Schiller aux drames historiques,</l>
					<l n="134" num="23.3">De Dürer, de Kaülbach, de Mozart, de Weber.</l>
					<l n="135" num="23.4">Vos grands hommes sont morts ; leur semence inféconde,</l>
					<l n="136" num="23.5">Au lieu des Meyerbeer, ne mettra plus au monde</l>
					<l n="137" num="23.6">Que des émules de Wagner.</l>
				</lg>
				<lg n="24">
					<l n="138" num="24.1">Vous n’aurez même plus pour animer vos âmes</l>
					<l n="139" num="24.2">Des Koërner et des Arndt aux paroles de flammes,</l>
					<l n="140" num="24.3">Car vous ne combattrez que pour ambition.</l>
					<l n="141" num="24.4">Le poëte-guerrier n’a d’hymne poétique,</l>
					<l n="142" num="24.5">Que s’il chante un sujet qui soit patriotique</l>
					<l n="143" num="24.6">Et digne de sa nation.</l>
				</lg>
				<lg n="25">
					<l n="144" num="25.1">Mais qu’entends-je ? Une voix vient se joindre à la mienne.</l>
					<l n="145" num="25.2">Est-ce un écho lointain que la brise ramène,</l>
					<l n="146" num="25.3">Et qui vient sur mon luth murmurer sourdement.</l>
					<l n="147" num="25.4">L’écho grandit… — Soudain une figure sombre</l>
					<l n="148" num="25.5">A mes côtés ricane, et m’approuve dans l’ombre :</l>
					<l n="149" num="25.6">C’est Heine, un Allemand.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">8 avril 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>