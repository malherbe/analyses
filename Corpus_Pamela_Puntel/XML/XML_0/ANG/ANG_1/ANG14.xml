<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">NOS RUINES</title>
				<title type="medium">Édition électronique</title>
				<author key="ANG">
					<name>
						<forename>Albert</forename>
						<surname>ANGOT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">ANG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>NOS RUINES</title>
						<author>ALBERT ANGOT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30021186v</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>NOS RUINES</title>
								<author>ALBERT ANGOT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DUNIOL ET CIE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANG14">
				<head type="main">SUR LA DÉMOLITION DE LA COLONNE DE LA PLACE VENDÔME</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">O grand Napoléon, quelle amère pensée</l>
						<l n="2" num="1.2">Doit régner aujourd’hui dans ta tête bronzée,</l>
						<l n="3" num="1.3">Au faîte du trône d’airain</l>
						<l n="4" num="1.4">Qu’autrefois a taillé ta droite colossale</l>
						<l n="5" num="1.5">Dans les mille canons que l’Europe vassale</l>
						<l n="6" num="1.6">Livrait à ton bras surhumain !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Quand tu le bâtissais ce bronze indélébile,</l>
						<l n="8" num="2.2">Tu disais : — « L’étranger et la guerre civile</l>
						<l n="9" num="2.3">« Viendront y briser leurs fureurs ;</l>
						<l n="10" num="2.4">« Mes vieux héros sculptés, l’orgueil de la patrie,</l>
						<l n="11" num="2.5">« Arrêteront les rois et le peuple en furie</l>
						<l n="12" num="2.6">« Qui craindront encor l’Empereur.”</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Oh ! combien était vaine, alors, ton espérance,</l>
						<l n="14" num="3.2">Rêve prodigieux d’un génie en démence,</l>
						<l n="15" num="3.3">Étincelante illusion !</l>
						<l n="16" num="3.4">Plus tard tu vis ici l’étranger apparaître ;</l>
						<l n="17" num="3.5">En lui tu reconnus jusqu’à deux fois ton maître :</l>
						<l n="18" num="3.6">Quelle amère déception !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Ce n’était point assez !… regarde dans a plaine !</l>
						<l n="20" num="4.2">Le Prussien est campé sur les bords de la Seine</l>
						<l n="21" num="4.3">Hélas ! pour la troisième fois ;</l>
						<l n="22" num="4.4">Pendant que sous tes pieds la populace armée,</l>
						<l n="23" num="4.5">Maîtresse de Paris, lutte contre l’armée</l>
						<l n="24" num="4.6">De la pauvre France aux abois.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Ce que Blücher ou Moltke, un jour, dans sa colère,</l>
						<l n="26" num="5.2">Devant Paris vaincu, jamais n’eût osé faire,</l>
						<l n="27" num="5.3">Ce peuple insensé le fera.</l>
						<l n="28" num="5.4">Quand le pays en deuil sous le Prussien succombe,</l>
						<l n="29" num="5.5">Il voudra, sous ses coups, que la Colonne tombe ;</l>
						<l n="30" num="5.6">Et ses débris il les vendra !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Un ramas d’étrangers, sans foyers, sans famille,</l>
						<l n="32" num="6.2">Rebut des nations, exploite la Guenille</l>
						<l n="33" num="6.3">Au nom de la Fraternité.</l>
						<l n="34" num="6.4">Que leur font, après tout, les gloires de la France,</l>
						<l n="35" num="6.5">Pourvu qu’avec ivresse, au soleil, leur démence</l>
						<l n="36" num="6.6">Étale sa lubricité !</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Que leur font ce faisceau de souvenirs épiques,</l>
						<l n="38" num="7.2">Nos guerriers chérissant les combats héroïques,</l>
						<l n="39" num="7.3">Le bruit du fer contre le fer,</l>
						<l n="40" num="7.4">Le fracas des canons, la clameur des cymbales,</l>
						<l n="41" num="7.5">Répondant par une hymne au sifflement des balles,</l>
						<l n="42" num="7.6">Avec Desaix, avec Kléber !</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Que leur font nos succès qui tiennent du prodige :</l>
						<l n="44" num="8.2">Et Jourdan sur la Sombre et Joubert sur l’Adige,</l>
						<l n="45" num="8.3">Gaëte pris par Masséna,</l>
						<l n="46" num="8.4">Pichegru s’emparant des flottes assiégées :</l>
						<l n="47" num="8.5">Du levant au couchant cent batailles rangées,</l>
						<l n="48" num="8.6">Thabor, Austerlitz, Iéna !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">« Oui, qu’importe ! Aujourd’hui les peuples sont tous frères</l>
						<l n="50" num="9.2">« A bas tous les tyrans ! A bas toutes les guerres !</l>
						<l n="51" num="9.3">« Le Peuple doit tout dominer.</l>
						<l n="52" num="9.4">« Brisons les monuments de toute tyrannie !</l>
						<l n="53" num="9.5">« Que la route par nos au Droit soit aplanie.</l>
						<l n="54" num="9.6">« C’est le Peuple qui doit régner.”</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">S’ils veulent renverser la Colonne immortelle,</l>
						<l n="56" num="10.2">C’est que, dans leurs excès, ils tremblent devant elle,</l>
						<l n="57" num="10.3">C’est qu’ils redoutent l’Empereur ;</l>
						<l n="58" num="10.4">C’est qu’ils ont peur de voir, un jour, leur conscience</l>
						<l n="59" num="10.5">Leur dire à son aspect : — « Tu is servir la France</l>
						<l n="60" num="10.6">« Au profit de ton déshonneur ! »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="61" num="1.1">Oh ! tu ne peux périr, Colonne trois fois sainte,</l>
						<l n="62" num="1.2">Où la foudre en tes flancs se cache à peine éteinte ;</l>
						<l n="63" num="1.3">Dis-moi que tu ne peux périr !</l>
						<l n="64" num="1.4">Dis que ton bronze est fort en sa longue spirale,</l>
						<l n="65" num="1.5">Qu’il saura résister à leur main infernale ;</l>
						<l n="66" num="1.6">Dis-moi que tu ne peux mourir !</l>
					</lg>
					<lg n="2">
						<l n="67" num="2.1">Dis-moi que de tes flancs l’héroïque sculpture</l>
						<l n="68" num="2.2">Bravera leurs marteaux, comme la vieille armure</l>
						<l n="69" num="2.3">D’un chevalier bardé de fer,</l>
						<l n="70" num="2.4">Qui, ferme comme un roc, au sein de la bataille,</l>
						<l n="71" num="2.5">Supportait tous les coups et d’estoc et de taille</l>
						<l n="72" num="2.6">Qui pleuvaient drus sur son haubert.</l>
					</lg>
					<lg n="3">
						<l n="73" num="3.1">Dis que c’est le géant de la Rome française</l>
						<l n="74" num="3.2">Qui tordit tout ensemble en sa vaste fournaise</l>
						<l n="75" num="3.3">Ton bronze et l’immortalité !</l>
						<l n="76" num="3.4">Oh ! dis-moi que ton moule est gardé par la gloire !</l>
						<l n="77" num="3.5">Qu’on peut le reconstruire, en consultant l’Histoire</l>
						<l n="78" num="3.6">Où notre nom est répété !</l>
					</lg>
					<lg n="4">
						<l n="79" num="4.1">Ah ! c’est qu’on a besoin, en ces jours de tristesses,</l>
						<l n="80" num="4.2">D’avoir un souvenir de nos vieilles prouesses,</l>
						<l n="81" num="4.3">Avant de nous pouvoir venger ;</l>
						<l n="82" num="4.4">Pour voir en ce moment, sans éprouver de honte,</l>
						<l n="83" num="4.5">Sans qu’un flot de rougeur à la face nous monte,</l>
						<l n="84" num="4.6">Sous nos murs camper l’étranger !</l>
					</lg>
					<lg n="5">
						<l n="85" num="5.1">Certes, on a besoin d’un monument sublime,</l>
						<l n="86" num="5.2">Quelque bronze vengeur, quelque trophée opime,</l>
						<l n="87" num="5.3">Pour montrer à ses fiers vainqueurs ;</l>
						<l n="88" num="5.4">Pour leur dire qu’aussi, lors de notre puissance,</l>
						<l n="89" num="5.5">Nous les vîmes plongés au sein de la souffrance,</l>
						<l n="90" num="5.6">Nous les vîmes verser des pleurs.</l>
					</lg>
					<lg n="6">
						<l n="91" num="6.1">Voilà pourquoi, Colonne, image de victoire,</l>
						<l n="92" num="6.2">Pourquoi je veux garder les fantômes de gloire</l>
						<l n="93" num="6.3">Qui se pressent à tes côtés,</l>
						<l n="94" num="6.4">Je veux voir tes soldats, ces héros d’un autre âge,</l>
						<l n="95" num="6.5">Sortir encor vainqueurs du nouvel esclavage</l>
						<l n="96" num="6.6">De ces Vandales détestés.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="97" num="1.1">Tremblez, vils étrangers et vile populace !</l>
						<l n="98" num="1.2">Dans le sein des Français la colère s’amasse,</l>
						<l n="99" num="1.3">Déjà votre règne est passé.</l>
						<l n="100" num="1.4">Tremblez ! J’entends déjà notre canon qui gronde</l>
						<l n="101" num="1.5">Dans le lointain ; et sans que le vôtre y réponde,</l>
						<l n="102" num="1.6">Le vrai Français s’est avancé.</l>
					</lg>
					<lg n="2">
						<l n="103" num="2.1">Tremblez ! déjà j’entends, avec de longs murmures,</l>
						<l n="104" num="2.2">Au sein du monument résonner des armures,</l>
						<l n="105" num="2.3">Et comme un bruit confus de pas.</l>
						<l n="106" num="2.4">On dirait que, soudain, renaissent de leur cendre,</l>
						<l n="107" num="2.5">Les vieux héros bronzés cherchent à redescendre</l>
						<l n="108" num="2.6">Pour combattre avec nos soldats.</l>
					</lg>
					<lg n="3">
						<l n="109" num="3.1">Vous avez réveillé ces ombres immortelles.</l>
						<l n="110" num="3.2">Les aigles, leurs gardiens, ont agité leurs ailes.</l>
						<l n="111" num="3.3">Craignez peut-être leur fureur !</l>
						<l n="112" num="3.4">Les foudres apaisées qu’ils portent dans leurs serres</l>
						<l n="113" num="3.5">Pourraient bien contre vous déchaînant leurs colères,</l>
						<l n="114" num="3.6">Vous renverser d’un trait vengeur.</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">ÉPILOGUE</head>
					<lg n="1">
						<l n="115" num="1.1">Et toi, Hugo, toi, qui célébrais la Colonne</l>
						<l n="116" num="1.2">Sur ton luth immortel, dans un vers qui résonne</l>
						<l n="117" num="1.3">Ainsi qu’une cloche d’airain ;</l>
						<l n="118" num="1.4">Toi, qui chantais jadis nos splendides annales,</l>
						<l n="119" num="1.5">Au sein des nations nos courses triomphales,</l>
						<l n="120" num="1.6">Le front pur comme un jour serein ;</l>
					</lg>
					<lg n="2">
						<l n="121" num="2.1">Oh ! que tu dois rougir de cette multitude</l>
						<l n="122" num="2.2">Que flattais avec trop de sollicitude,</l>
						<l n="123" num="2.3">De ces cruels démolisseurs !</l>
						<l n="124" num="2.4">S’il est bon de songer à clamer leur souffrance,</l>
						<l n="125" num="2.5">Il faut être assez fort pour braver leur licence,</l>
						<l n="126" num="2.6">Et pour enchaîner leurs erreurs.</l>
					</lg>
					<lg n="3">
						<l n="127" num="3.1">Démolir ta Colonne !… y songes-tu ?… quel crime !</l>
						<l n="128" num="3.2">Lève-toi donc, poëte !… à ta lyre sublime</l>
						<l n="129" num="3.3">Attache une corde de fer.</l>
						<l n="130" num="3.4">Car tu l’as dit :— »Malheur à qui dit à ses frères,</l>
						<l n="131" num="3.5">« Dans un temps tout rongé de haine et de misères :</l>
						<l n="132" num="3.6">« Je retourne dans le désert. »</l>
					</lg>
					<lg n="4">
						<l n="133" num="4.1">Me laisseras-tu seul, égoïste poëte,</l>
						<l n="134" num="4.2">Exhaler mes soupirs qu’emporte la tempête</l>
						<l n="135" num="4.3">Dans une rafale de vent ?</l>
						<l n="136" num="4.4">Viens à ma faible voix unir ta voix de flamme,</l>
						<l n="137" num="4.5">Pour dompter le courroux et le bruit de la lame,</l>
						<l n="138" num="4.6">Et faire taire l’ouragan.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">25 avril 1871.</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>