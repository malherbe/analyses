<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">NOS RUINES</title>
				<title type="medium">Édition électronique</title>
				<author key="ANG">
					<name>
						<forename>Albert</forename>
						<surname>ANGOT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">ANG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>NOS RUINES</title>
						<author>ALBERT ANGOT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30021186v</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>NOS RUINES</title>
								<author>ALBERT ANGOT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DUNIOL ET CIE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANG8">
				<head type="main">VERSAILLES</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">L’histoire a bien des railleries</l>
						<l n="2" num="1.2">Pour nos projets, nos passions,</l>
						<l n="3" num="1.3">Nos politiques rêveries.</l>
						<l n="4" num="1.4">Ce sont comme autant de leçons.</l>
						<l n="5" num="1.5">On sent alors mieux la faiblesse,</l>
						<l n="6" num="1.6">Et l’orgueil, et la folle ivresse</l>
						<l n="7" num="1.7">De notre pauvre humanité ;</l>
						<l n="8" num="1.8">De nos vœux, autant de chimères,</l>
						<l n="9" num="1.9">Et de nos œuvres éphémères</l>
						<l n="10" num="1.10">On sent la stérilité.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">La gloire fut à tire-d’aile :</l>
						<l n="12" num="2.2">Trônes, glaives des généraux,</l>
						<l n="13" num="2.3">Mîtres d’or roulent pêle-mêle</l>
						<l n="14" num="2.4">Avec la hache des bourreaux.</l>
						<l n="15" num="2.5">Dans la course vertigineuse,</l>
						<l n="16" num="2.6">Que poursuit sa vague par le Temps.</l>
						<l n="17" num="2.7">Rien ne flotte, rien ne surnage,</l>
						<l n="18" num="2.8">Pas même l’herbe du rivage.</l>
						<l n="19" num="2.9">Le temps, c’est le roi des torrents.</l>
					</lg>
					<lg n="3">
						<l n="20" num="3.1">Pourquoi, dis, dans ton impuissance,</l>
						<l n="21" num="3.2">Veux-tu combattre, homme insensé,</l>
						<l n="22" num="3.3">Cette force, cette puissance</l>
						<l n="23" num="3.4">Qui dans un jour t’aura brisé ?</l>
						<l n="24" num="3.5">N’élève plus dans ton délire,</l>
						<l n="25" num="3.6">Des colosses au front d’airain.</l>
						<l n="26" num="3.7">Ne taille plus dans le carrare</l>
						<l n="27" num="3.8">Tes traits que le cercueil avare</l>
						<l n="28" num="3.9">Viendra te disputer demain.</l>
					</lg>
					<lg n="4">
						<l n="29" num="4.1">Ton colosse a des pieds d’argile :</l>
						<l n="30" num="4.2">Il croule miné par le temps,</l>
						<l n="31" num="4.3">Et ta statue, œuvre débile,</l>
						<l n="32" num="4.4">N’offre plus que débris gisants.</l>
						<l n="33" num="4.5">Si, par hasard, elle subsiste,</l>
						<l n="34" num="4.6">Elle contemple toute triste,</l>
						<l n="35" num="4.7">Ton grand œuvre en morceaux brisé,</l>
						<l n="36" num="4.8">Ce grand œuvre que ton génie</l>
						<l n="37" num="4.9">Rêva tout rempli d’harmonie,</l>
						<l n="38" num="4.10">Et dur comme un lingot bronzé.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="39" num="1.1">Hier, Louis, toi que Turenne</l>
						<l n="40" num="1.2">Enrichit, après maints combats,</l>
						<l n="41" num="1.3">De l’Alsace et de la Lorraine ;</l>
						<l n="42" num="1.4">Grand Louis, ne voyais-tu pas,</l>
						<l n="43" num="1.5">Vainqueur dans plus de dix batailles,</l>
						<l n="44" num="1.6">L’Allemand railler dans Versailles</l>
						<l n="45" num="1.7">Ta gloire et tes rayons éteints ?</l>
						<l n="46" num="1.8">Il insultait ta belle France</l>
						<l n="47" num="1.9">Qu’un usurpateur sans puissance</l>
						<l n="48" num="1.10">Avait livré entre ses mains.</l>
					</lg>
					<lg n="2">
						<l n="49" num="2.1">Combats qui faisaient notre gloire,</l>
						<l n="50" num="2.2">Turckeim, Mergentheim et Salzbach,</l>
						<l n="51" num="2.3">Sont effacés de la mémoire</l>
						<l n="52" num="2.4">Par Wissembourg, Woërth et Forbach.</l>
						<l n="53" num="2.5">Honte ! un descendant des burgraves,</l>
						<l n="54" num="2.6">Foulant notre terre de braves</l>
						<l n="55" num="2.7">(Ils gémissaient dans le cercueil),</l>
						<l n="56" num="2.8">Au son des fanfares guerrières,</l>
						<l n="57" num="2.9">Se faisait, narguant nos bannières,</l>
						<l n="58" num="2.10">Sacrer dans ton palais en deuil !</l>
					</lg>
					<lg n="3">
						<l n="59" num="3.1">Né du suffrage populaire,</l>
						<l n="60" num="3.2">Aujourd’hui siège un Parlement,</l>
						<l n="61" num="3.3">Image de la France entière ;</l>
						<l n="62" num="3.4">Déjà ta demeure l’attend.</l>
						<l n="63" num="3.5">La même, où tu parlais en maître,</l>
						<l n="64" num="3.6">Dans un instant il va paraître.</l>
						<l n="65" num="3.7">Non ! non ! l’État ce n’est plus toi.</l>
						<l n="66" num="3.8">C’est le peuple et cette assemblée :</l>
						<l n="67" num="3.9">La royauté s’est écroulée.</l>
						<l n="68" num="3.10">Non ! l’État, ce n’est plus un roi !</l>
					</lg>
					<lg n="4">
						<l n="69" num="4.1">Louis Seize, en place de Grève,</l>
						<l n="70" num="4.2">Est mort frappé sur l’échafaud ;</l>
						<l n="71" num="4.3">Et sur une lointaine grève,</l>
						<l n="72" num="4.4">Qui lui servira de tombeau,</l>
						<l n="73" num="4.5">Proscrit, ton héritier unique,</l>
						<l n="74" num="4.6">Lassé par un destin inique,</l>
						<l n="75" num="4.7">Mange un pain arrosé de pleurs.</l>
						<l n="76" num="4.8">Quel contraste (ironie amère)</l>
						<l n="77" num="4.9">Entre ta gloire et sa misère,</l>
						<l n="78" num="4.10">Entre ta joie et ses douleurs !</l>
					</lg>
					<lg n="5">
						<l n="79" num="5.1">O Roi-Soleil, ton fuit de chasse</l>
						<l n="80" num="5.2">Et ton sceptre sont impuissants.</l>
						<l n="81" num="5.3">Dépose-les de bonne grâce,</l>
						<l n="82" num="5.4">Et de ton piédestal descends.</l>
						<l n="83" num="5.5">Ton coursier plein d’impatience</l>
						<l n="84" num="5.6">Se cabre avec trop d’arrogance,</l>
						<l n="85" num="5.7">Quand leur pouvoir n’existe plus :</l>
						<l n="86" num="5.8">Les provinces par toi conquises</l>
						<l n="87" num="5.9">Par les Allemands sont reprises ;</l>
						<l n="88" num="5.10">Les parlements sont revenus.</l>
					</lg>
					<lg n="6">
						<l n="89" num="6.1">Bien ! ici siège l’assemblée.</l>
						<l n="90" num="6.2">Ainsi qu’une troupe d’acteurs,</l>
						<l n="91" num="6.3">Sur plus d’une scène enrôlée,</l>
						<l n="92" num="6.4">Courant après des spectateurs,</l>
						<l n="93" num="6.5">Le parlement, de scène en scène,</l>
						<l n="94" num="6.6">De Bordeaux ici se promène</l>
						<l n="95" num="6.7">Pour faire ses seconds débuts.</l>
						<l n="96" num="6.8">C’est un grand drame qui se joue</l>
						<l n="97" num="6.9">Et dont l’intrigue se dénoue</l>
						<l n="98" num="6.10">Devant les peuples confondus.</l>
					</lg>
					<lg n="7">
						<l n="99" num="7.1">Ce n’est plus une Iphigénie,</l>
						<l n="100" num="7.2">Au spectacle de ses douleurs</l>
						<l n="101" num="7.3">Tracé par la main du génie,</l>
						<l n="102" num="7.4">Qui nous fera verser des pleurs.</l>
						<l n="103" num="7.5">L’héroïne est plus attachante,</l>
						<l n="104" num="7.6">Car c’est la France agonisante</l>
						<l n="105" num="7.7">Dont se déroulent les destins.</l>
						<l n="106" num="7.8">Le décor est immense et sombre :</l>
						<l n="107" num="7.9">Là-bas du sang, partout de l’ombre,</l>
						<l n="108" num="7.10">Et des horizons incertains.</l>
					</lg>
					<lg n="8">
						<l n="109" num="8.1">Malgré l’intérêt de la scène,</l>
						<l n="110" num="8.2">S’envole notre souvenir ;</l>
						<l n="111" num="8.3">Et pour un instant il ramène</l>
						<l n="112" num="8.4">Tout un passé qui va surgir.</l>
						<l n="113" num="8.5">Quelles ombres ! Quel personnages !</l>
						<l n="114" num="8.6">Et quels fantastiques mirages !</l>
						<l n="115" num="8.7">Oh ! quel tourbillon insensé !</l>
						<l n="116" num="8.8">Tout tourne, tout passe et repasse ;</l>
						<l n="117" num="8.9">En sans laisser la moindre trace,</l>
						<l n="118" num="8.10">Se mêle et se trouve effacé.</l>
					</lg>
					<lg n="9">
						<l n="119" num="9.1">Roi, reine, dauphins et dauphines,</l>
						<l n="120" num="9.2">Favorites et grands seigneurs ;</l>
						<l n="121" num="9.3">Belles dames, aux taille fines ;</l>
						<l n="122" num="9.4">Petits abbés et monseigneurs,</l>
						<l n="123" num="9.5">Ministres, galants, politiques,</l>
						<l n="124" num="9.6">Prosateurs, poëtes comiques.</l>
						<l n="125" num="9.7">C’est Luxembourg et Saint-Simon,</l>
						<l n="126" num="9.8">Louvois, Bossuet, et La Vallière,</l>
						<l n="127" num="9.9">Colbert, Henriette d’Angleterre,</l>
						<l n="128" num="9.10">Villars, Racine et Fénelon.</l>
					</lg>
					<lg n="10">
						<l n="129" num="10.1">Cordons bleus et robes bouffantes,</l>
						<l n="130" num="10.2">Perruques, dentelles, canons,</l>
						<l n="131" num="10.3">Paniers et gazes transparentes,</l>
						<l n="132" num="10.4">Jabots, seins blancs et pieds mignons,</l>
						<l n="133" num="10.5">Élégances, galanteries,</l>
						<l n="134" num="10.6">Aveux d’amour, mutineries,</l>
						<l n="135" num="10.7">Intrigues et frivolités,</l>
						<l n="136" num="10.8">Tout disparaît (rêve éphémère !)</l>
						<l n="137" num="10.9">Dans un flot de poudre légère</l>
						<l n="138" num="10.10">Au souffle des réalités.</l>
					</lg>
					<lg n="11">
						<l n="139" num="11.1">Nos députés, tête baissée,</l>
						<l n="140" num="11.2">Préoccupés, tout soucieux,</l>
						<l n="141" num="11.3">Viennent promener leur pensée</l>
						<l n="142" num="11.4">Sous les grands arbres, près des Dieux,</l>
						<l n="143" num="11.5">Ou près des bassins de Latone</l>
						<l n="144" num="11.6">Et de Neptune qui s’étonne</l>
						<l n="145" num="11.7">De ces hôtes nouveau-venus ;</l>
						<l n="146" num="11.8">Près des sylvains et des naïades,</l>
						<l n="147" num="11.9">Des faunes, des hamadryades</l>
						<l n="148" num="11.10">Ouvrant de grand yeux éperdus.</l>
					</lg>
					<lg n="12">
						<l n="149" num="12.1">Alors, du fond de ma poitrine</l>
						<l n="150" num="12.2">Je sens s’exhaler un soupir ;</l>
						<l n="151" num="12.3">Et mon esprit que tout chagrine</l>
						<l n="152" num="12.4">Cherche à pénétrer l’avenir.</l>
						<l n="153" num="12.5">Efforts superflus ! Tout est sombre…</l>
						<l n="154" num="12.6">Pauvre pays !… Ah ! puisse l’ombre</l>
						<l n="155" num="12.7">Des Condé, Louvois, des Colbert,</l>
						<l n="156" num="12.8">Errants au sein de la verdure</l>
						<l n="157" num="12.9">Guider, pour panser ta blessure.</l>
						<l n="158" num="12.10">Nos ministres nommées hier !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">12 Mars 1871</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>