<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">NOS RUINES</title>
				<title type="medium">Édition électronique</title>
				<author key="ANG">
					<name>
						<forename>Albert</forename>
						<surname>ANGOT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">ANG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>NOS RUINES</title>
						<author>ALBERT ANGOT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30021186v</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>NOS RUINES</title>
								<author>ALBERT ANGOT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DUNIOL ET CIE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANG1">
				<head type="main">LE SIÈCLE</head>
				<head type="form">PRÉLUDE</head>
				<lg n="1">
					<l n="1" num="1.1">Ce siècle dégénère, un vil instinct le mène ;</l>
					<l n="2" num="1.2">En maîtresse partout règne la passion :</l>
					<l n="3" num="1.3">Prolétaire, bourgeois, ministre, capitaine,</l>
					<l n="4" num="1.4">Chacun veut obéir à son ambition.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Pareille à l’édifice aux colonnes usées,</l>
					<l n="6" num="2.2">La société tremble à l’haleine du vent ;</l>
					<l n="7" num="2.3">Ses plus fermes piliers étaient nos mœurs passées :</l>
					<l n="8" num="2.4">Le respect du foyer par le père et l’enfant.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Comme un Soleil d’hiver, derrière une ruine,</l>
					<l n="10" num="3.2">Baissant son pâle front au lointain horizon,</l>
					<l n="11" num="3.3">Le flambeau de la foi d’heure en heure décline</l>
					<l n="12" num="3.4">Et cache sa lueur derrière la Raison.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Partout, dans les cités, et dans les solitudes,</l>
					<l n="14" num="4.2">On sent régner dans l’air un souffle empoisonné.</l>
					<l n="15" num="4.3">Dans l’esprit corrompu des âpres multitudes,</l>
					<l n="16" num="4.4">Dieu, respect et devoir, tout est déraciné.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">L’ouragan a passé sur les têtes humaines ;</l>
					<l n="18" num="5.2">C’est l’ouragan du doute et de l’impiété :</l>
					<l n="19" num="5.3">Il a semé partout de détestables graines,</l>
					<l n="20" num="5.4">Et par lui le bon grain, hélas ! est emporté.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">L’Avenir est un gouffre : il donne le vertige ;</l>
					<l n="22" num="6.2">Quand sur son fond béant on se penche pour voir,</l>
					<l n="23" num="6.3">On recule effaré : le regard qui s’afflige</l>
					<l n="24" num="6.4">N’a vu qu’un lit fangeux où sommeille un flot noir.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Comme un vaste Océan sur ses immenses plages,</l>
					<l n="26" num="7.2">Aujourd’hui nous voyons avec impunité</l>
					<l n="27" num="7.3">La Licence monter d’étages en étages,</l>
					<l n="28" num="7.4">Et noyer dans ses flots l’Auguste Liberté.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Autrefois les penseurs et les puissants poëtes</l>
					<l n="30" num="8.2">Voyaient dans notre peuple un généreux lion,</l>
					<l n="31" num="8.3">A la faute crinière, à l’œil plein de tempêtes,</l>
					<l n="32" num="8.4">Fier après le combat, accessible au pardon.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Aujourd’hui, ce n’est plus qu’un tigre sanguinaire</l>
					<l n="34" num="9.2">Égorgeant à plaisir les troupeaux qu’il atteint,</l>
					<l n="35" num="9.3">Jusqu’à ce qu’épuisé de rage et de colère,</l>
					<l n="36" num="9.4">Malgré lui, le sommeil ferme son œil éteint.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Voilà quel est le peuple ; et l’émeute grondante,</l>
					<l n="38" num="10.2">Au bruit le plus léger sait bien se réveiller.</l>
					<l n="39" num="10.3">Chez celui qui vous tend une main implorante,</l>
					<l n="40" num="10.4">On voit les yeux reluire et la haine briller.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Que m’importent les Arts et la Vapeur ardente !</l>
					<l n="42" num="11.2">Qu’importent le progrès, les éléments domptés,</l>
					<l n="43" num="11.3">Si le Sombre Avenir sans cesse m’épouvante,</l>
					<l n="44" num="11.4">Si mes fils doivent voir des jours plus attristés ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Si, malgré leurs efforts, ma France est abaissée</l>
					<l n="46" num="12.2">Par de fiers ennemis et la corruption,</l>
					<l n="47" num="12.3">Si de lâches tyrans sur sa tête brisée,</l>
					<l n="48" num="12.4">Peuple ou bien empereur, sèment la trahison !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Que ne puis-je, mon Dieu ! moi, le pauvre poëte,</l>
					<l n="50" num="13.2">Comme autrefois Hugo, comme Goëthe et Schiller,</l>
					<l n="51" num="13.3">Attacher à mon luth, comme un signe de fête,</l>
					<l n="52" num="13.4">En saluant ce Siècle un léger rameau vert !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Que ne puis-je, en mes chants, ramener l’espérance,</l>
					<l n="54" num="14.2">Annoncer des beaux jours dorés par le Soleil,</l>
					<l n="55" num="14.3">Un Avenir nouveau pour notre noble France,</l>
					<l n="56" num="14.4">Pour la Société le moment du réveil !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Travaillons ! C’est le mot, si j’ai bonne mémoire,</l>
					<l n="58" num="15.2">Que répétait jadis un empereur romain,</l>
					<l n="59" num="15.3">Fier du bonheur de tous, amoureux de la gloire ;</l>
					<l n="60" num="15.4">Que ce mot nous rallie à l’instant et demain.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Travaillons, travaillons à reformer les âmes,</l>
					<l n="62" num="16.2">Malgré la calomnie, et malgré les dédains.</l>
					<l n="63" num="16.3">Que la foi dans les cœurs porte ses saintes flammes,</l>
					<l n="64" num="16.4">Et nous verrons meilleurs tous les tristes humains.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Rappelons le respect, l’amour de la famille</l>
					<l n="66" num="17.2">Dans la terre d’exil aujourd’hui gémissants.</l>
					<l n="67" num="17.3">Disons que c’est le phare en la nuit qui scintille</l>
					<l n="68" num="17.4">Pour guider vers le port et montrer les brisants.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Unissons pour couver ces vivifiants germes</l>
					<l n="70" num="18.2">Tous les cœurs vertueux, tous les cœurs inspirés,</l>
					<l n="71" num="18.3">Tous les cœurs vraiment purs, tous les cœurs vraiment fermes,</l>
					<l n="72" num="18.4">Par les rayons divins dans ce siècle éclairés.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">La main à l’œuvre, amis… ! Ramons avec courage</l>
					<l n="74" num="19.2">Pour conduire la nef portant la vérité</l>
					<l n="75" num="19.3">Sur les flots de la foule et l’ancrer au rivage,</l>
					<l n="76" num="19.4">Et verser ses trésors à la Société.</l>
				</lg>
			</div></body></text></TEI>