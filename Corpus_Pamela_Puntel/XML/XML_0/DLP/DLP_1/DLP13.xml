<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP13">
				<head type="number">XIII</head>
				<head type="main">LE DERNIER JOUR D'UNE CITÉ</head>
				<head type="main">A STRASBOURG</head>
				<lg n="1">
					<l n="1" num="1.1">On n'entend que le bruit du canon dans les rues :</l>
					<l n="2" num="1.2">Par la flamme et le fer incessamment accrues,</l>
					<l n="3" num="1.3">La ruine et la mort se sont donné la main :</l>
					<l n="4" num="1.4">Hommes, femmes, vieillards, enfants, tout être humain</l>
					<l n="5" num="1.5">Se débat écrasé par l'effort qui le brise,</l>
					<l n="6" num="1.6">Sous l'étreinte suprême où Strasbourg agonise.</l>
					<l n="7" num="1.7">Ce qui ne brûle pas encore va brûler :</l>
					<l n="8" num="1.8">De temps en temps on sent la terre s'ébranler…</l>
					<l n="9" num="1.9">Ce n'est rien : ce ne sont que vingt maisons qui tombent,</l>
					<l n="10" num="1.10">A travers les sanglots des blessés qui succombent :</l>
					<l n="11" num="1.11">Un boulet passe et va frapper un bataillon,</l>
					<l n="12" num="1.12">Fauchant des rangs entiers dans son large sillon :</l>
					<l n="13" num="1.13">On enlève les morts, et le feu recommence.</l>
				</lg>
				<lg n="2">
					<l n="14" num="2.1">Oh ! qui raconterait cette bataille immense !</l>
				</lg>
				<lg n="3">
					<l n="15" num="3.1">Un colonel de ligne arrive du dehors.</l>
					<l n="16" num="3.2">Tous les soldats vivants sont entrés dans le corps</l>
					<l n="17" num="3.3">Qu'il ramène brisé par trente heures de lutte :</l>
					<l n="18" num="3.4">Le reste est mort, ou va décroissant par minute ;</l>
					<l part="I" n="19" num="3.5">Uhrich est là :</l>
					<l part="M" n="19" num="3.5">— Comment sont vos hommes ?</l>
					<l part="F" n="19" num="3.5">— Très-mal.</l>
					<l part="I" n="20" num="3.6">— Combien en avez-vous ?</l>
					<l part="F" n="20" num="3.6">— Dix mille, général.</l>
					<l part="I" n="21" num="3.7">— Combien de Prussiens devant vous ?</l>
					<l part="F" n="21" num="3.7">— Deux cent mille.</l>
					<l part="I" n="22" num="3.8">— Chargez !</l>
					<l part="F" n="22" num="3.8">Le colonel sort encor de la ville.</l>
					<l n="23" num="3.9">Uhrich court aux remparts. Quinze cents artilleurs,</l>
					<l n="24" num="3.10">Pendant que les soldats vont les défendre ailleurs,</l>
					<l n="25" num="3.11">Lancent sur l'ennemi les boulets et les flammes :</l>
					<l n="26" num="3.12">Auprès d'eux sont couchés les enfants et les femmes</l>
					<l n="27" num="3.13">Qui leur ont apporté de la poudre et du pain,</l>
					<l n="28" num="3.14">Car toujours les canons et les hommes ont faim !</l>
				</lg>
				<lg n="4">
					<l n="29" num="4.1">— Général, dit un vieux, la poudre diminue.</l>
				</lg>
				<lg n="5">
					<l part="I" n="30" num="5.1">Uhrich montre la plaine et lui dit :</l>
					<l part="F" n="30" num="5.1">— Continue.</l>
					<l n="31" num="5.2">Plus loin, un artilleur tombe, couvert de sang.</l>
				</lg>
				<lg n="6">
					<l part="I" n="32" num="6.1">— Un homme pour mourir ! dit-il.</l>
					<l part="F" n="32" num="6.1">Il en vient cent.</l>
				</lg>
				<lg n="7">
					<l n="33" num="7.1">Alors le général se tourne vers les autres :</l>
					<l n="34" num="7.2">— Le poste doit rester au plus ancien des vôtres,</l>
					<l n="35" num="7.3">Mes enfants : prends, l'ami : le canon t'appartient !</l>
				</lg>
				<lg n="8">
					<l n="36" num="8.1">Et l'artilleur, pondant que le feu se soutient</l>
					<l n="37" num="8.2">Toujours plus écrasant de la ville à la plaine,</l>
					<l n="38" num="8.3">Fendant l'air enflammé de sa bruyante haleine,</l>
					<l n="39" num="8.4">Décharge le canon, et tombe. Il était mort.</l>
				</lg>
				<lg n="9">
					<l n="40" num="9.1">— Général, les boulets vont nous manquer encor,</l>
					<l n="41" num="9.2">Dit un sergent, penché sur l'affût qui tressaille.</l>
				</lg>
				<lg n="10">
					<l part="I" n="42" num="10.1">Le général répond :</l>
					<l part="F" n="42" num="10.1">— Ça ne fait rien : travaille !</l>
					<l n="43" num="10.2">Il faut tirer sur eux si longtemps qu'on pourra :</l>
					<l n="44" num="10.3">Quand nous n'en aurons plus, eh bien ! on en fera !</l>
				</lg>
				<lg n="11">
					<l n="45" num="11.1">Il s'éloigne, et le feu double de violence.</l>
				</lg>
				<lg n="12">
					<l n="46" num="12.1">Dans la ville, la flamme a gagné l'ambulance :</l>
					<l n="47" num="12.2">Alors tous ces héros, que jamais rien n'abat,</l>
					<l n="48" num="12.3">Après avoir été dos lions au combat,</l>
					<l n="49" num="12.4">Courent pour arracher sa proie à l'incendie</l>
					<l n="50" num="12.5">Par la bise du Nord à chaque instant grandie :</l>
					<l n="51" num="12.6">Ils posent une échelle au mur de la maison</l>
					<l n="52" num="12.7">Où la mort va faucher sa terrible moisson,</l>
					<l n="53" num="12.8">Et sous l'écrasement des boulets et des bombes</l>
					<l n="54" num="12.9">Emportent ces blessés dont se creusaient les tombes.</l>
				</lg>
				<lg n="13">
					<l n="55" num="13.1">Uhrich prend sa lorgnette et regarde au lointain</l>
				</lg>
				<lg n="14">
					<l n="56" num="14.1">— Allons ! dit-il, voilà l'ennemi, c'est certain :</l>
					<l n="57" num="14.2">Nos soldats terrassés ont dû battre on retraite !</l>
				</lg>
				<lg n="15">
					<l n="58" num="15.1">En effet, tout couvert de sang jusqu'à la tête,</l>
					<l n="59" num="15.2">Un jeune lieutenant accourt, trois fois blessé :</l>
				</lg>
				<lg n="16">
					<l part="I" n="60" num="16.1">— Eh bien, le colonel ?</l>
					<l part="F" n="60" num="16.1">— Mort ! Je l'ai remplacé.</l>
					<l part="I" n="61" num="16.2">— Mais, et le commandant ?</l>
					<l part="M" n="61" num="16.2">— Mort !</l>
					<l part="F" n="61" num="16.2">— Et le capitaine ?</l>
					<l part="I" n="62" num="16.3">— Mort !</l>
					<l part="M" n="62" num="16.3">— Que vous reste-t-il d'hommes ?</l>
					<l part="F" n="62" num="16.3">Deux mille à peine.</l>
				</lg>
				<lg n="17">
					<l n="63" num="17.1">Alors le général réunit ses soldats.</l>
					<l n="64" num="17.2">— Il ne faut pas nous rendre encore, n'est-ce pas ?</l>
					<l part="I" n="65" num="17.3">Chargeons !</l>
					<l part="F" n="65" num="17.3">Et les soldats partent, Uhrich en tête !</l>
				</lg>
				<lg n="18">
					<l n="66" num="18.1">Mais non plus cette fois pour venger la défaite,</l>
					<l n="67" num="18.2">Non plus pour délivrer la ville qu'il défend,</l>
					<l n="68" num="18.3">Et revenir encor dans ses murs, triomphant,</l>
					<l n="69" num="18.4">Après avoir sauvé la grande citadelle,</l>
					<l n="70" num="18.5">Mais pour lui faire au moins une mort digne d'elle,</l>
					<l n="71" num="18.6">Et puisqu'il faut tomber, tomber en lui faisant</l>
					<l n="72" num="18.7">Un sépulcre taillé dans la chair et le sang !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Cependant des remparts tonne l'artillerie</l>
					<l n="74" num="19.2">Toujours à chaque instant plus forte et mieux nourrie !</l>
					<l n="75" num="19.3">A travers la fumée on voit l'énorme effort</l>
					<l n="76" num="19.4">De tous ces artilleurs, forgerons de la mort,</l>
					<l n="77" num="19.5">Forgeant des corps humains quand le canon s'allume,</l>
					<l n="78" num="19.6">Comme un morceau de fer qui bondit sous l'enclume !</l>
				</lg>
				<lg n="20">
					<l n="79" num="20.1">Quelle fournaise rouge au milieu de ces champs !</l>
					<l n="80" num="20.2">Aux pères fatigués succèdent les enfants ;</l>
					<l n="81" num="20.3">Chacun fait à son tour la terrible besogne,</l>
					<l n="82" num="20.4">Pas un, pas un d'entr'eux que la rage n'empoigne,</l>
					<l n="83" num="20.5">Pas un qui pour mourir ne se soit apprêté,</l>
					<l n="84" num="20.6">Devant ce grand combat où tombe une cité !</l>
				</lg>
				<lg n="21">
					<l n="85" num="21.1">Le sergent de ses mains se fait une lorgnette :</l>
					<l n="86" num="21.2">— Crénom ! grognonne-t-il en remuant la tête,</l>
					<l n="87" num="21.3">Ces Maudits vont tomber sur nous comme des chiens !</l>
				</lg>
				<lg n="22">
					<l n="88" num="22.1">Tout à coup il entend crier : Les Prussiens !</l>
					<l n="89" num="22.2">En effet, l'ennemi vient de couper les nôtres.</l>
					<l n="90" num="22.3">Pendant qu'Uhrich chargeait cent bataillons, les autres</l>
					<l n="91" num="22.4">Sont venus par derrière et nous coupent en deux :</l>
					<l n="92" num="22.5">Les Français épuisés sont pris entre trois feux !</l>
				</lg>
				<lg n="23">
					<l n="93" num="23.1">Un colonel accourt et regarde la plaine,</l>
					<l n="94" num="23.2">Où tous sont si mêlés que l'on distingue à peine</l>
					<l n="95" num="23.3">Sous le ciel qui se couvre et la nuit qui descend,</l>
					<l n="96" num="23.4">Qui des deux ennemis est vainqueur à présent.</l>
					<l n="97" num="23.5">Le feu s'arrête, et tous regardent en silence.</l>
					<l n="98" num="23.6">Qui sait de quel côté va pencher la balance ?</l>
					<l n="99" num="23.7">Muets, les artilleurs regardent sans rien voir…</l>
					<l n="100" num="23.8">Voici la nuit ; le ciel, la plaine, tout est noir…</l>
					<l n="101" num="23.9">Dieu ! que sont devenus nos soldats ?… On ignore</l>
					<l n="102" num="23.10">Ceux qui de ces héros restent vivants encore !</l>
					<l n="103" num="23.11">On ne sait rien, mais rien ! Sont-ils morts ou vainqueurs ?</l>
					<l n="104" num="23.12">Outre le doute affreux l'angoisse étreint les cœurs !…</l>
				</lg>
				<lg n="24">
					<l n="105" num="24.1">Tout à coup on entend un cri de sentinelle,</l>
					<l n="106" num="24.2">Et puis c'est tout ! Plus loin, le cri se renouvelle,</l>
					<l n="107" num="24.3">Puis une troisième fois un qui-vive lointain</l>
					<l part="I" n="108" num="24.4">Auquel les arrivants répondent !…</l>
					<l part="F" n="108" num="24.4">C'est certain !</l>
				</lg>
				<lg n="25">
					<l n="109" num="25.1">Ce sont eux ! Ils ont pu trouer cette muraille</l>
					<l n="110" num="25.2">De corps humains jetés à travers la bataille</l>
					<l n="111" num="25.3">Pour couper la retraite à nos soldats brisés !</l>
					<l part="I" n="112" num="25.4">Ce sont eux !</l>
					<l part="F" n="112" num="25.4">Mais hélas ! presque tous sont blessés…</l>
					<l n="113" num="25.5">Deux mille sont partis, cent cinquante reviennent…</l>
					<l n="114" num="25.6">Oh ! que toujours nos cœurs de ceux-là se souviennent,</l>
					<l n="115" num="25.7">Qui pour lutter pour nous de partout sont venus,</l>
					<l n="116" num="25.8">Vécurent ignorés et sont morts inconnus !</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<l n="117" num="25.9">Il ne restait plus rien dans la ville affamée,</l>
					<l n="118" num="25.10">Plus de fer, de boulets, de poudre, plus d'armée</l>
					<l n="119" num="25.11">Plus rien ! Le désespoir avait surgi partout :</l>
					<l n="120" num="25.12">Pas un de ses créneaux n'était resté debout ;</l>
					<l n="121" num="25.13">Elle avait noblement succombé toute entière,</l>
					<l n="122" num="25.14">Sans vouloir un seul jour baisser sa tête altière,</l>
					<l n="123" num="25.15">Regardant sa ruine avec sérénité…</l>
				</lg>
				<lg n="26">
					<l n="124" num="26.1"><space unit="char" quantity="2"/>En France, c'est ainsi que meurt une cité !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 17 octobre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>