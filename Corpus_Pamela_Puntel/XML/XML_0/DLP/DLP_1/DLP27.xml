<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP27">
				<head type="number">XXVII</head>
				<head type="main">LE VOLONTAIRE</head>
				<lg n="1">
					<l n="1" num="1.1">— Chère femme, je viens te lire un gros mystère :</l>
					<l n="2" num="1.2">Ce matin je me suis engagé volontaire.</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1">— Tu pars ? Voyons, voyons, je ne comprends pas bien…</l>
					<l part="I" n="4" num="2.2">Tu pars ?</l>
					<l part="M" n="4" num="2.2">— Oui.</l>
					<l part="M" n="4" num="2.2">— Quand ?</l>
					<l part="F" n="4" num="2.2">— Demain. Je ne te disais rien,</l>
					<l n="5" num="2.3">Parce que je voulais reculer la souffrance ;</l>
					<l part="I" n="6" num="2.4">Mais…</l>
					<l part="M" n="6" num="2.4">— Et pourquoi pars-tu ?</l>
					<l part="F" n="6" num="2.4">— Pour défendre la France,</l>
					<l part="I" n="7" num="2.5">Parbleu !</l>
					<l part="F" n="7" num="2.5">— Non, j'entends mal ce que tu dis, je croi</l>
					<l part="I" n="8" num="2.6">Tu pars… comme soldat ? Mais qui t'y force ?</l>
				</lg>
				<lg n="3">
					<l part="F" n="8">— Moi…</l>
					<l n="9" num="3.1">— Mais moi, mais ton enfant ? nous quitter ? et sans cause ?</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1">— Tu le trompes : je pars, et c'est pour quelque chose !</l>
					<l n="11" num="4.2">Je pars pour accomplir notre devoir à tous !</l>
					<l n="12" num="4.3">Vois-tu, le temps n'est plus de ne songer qu'à nous :</l>
					<l n="13" num="4.4">Au-dessus de l'amour des enfants et des femmes,</l>
					<l n="14" num="4.5">Il est un mot sacré qui fait vibrer nos âmes :</l>
					<l n="15" num="4.6">Un mol que nous avons bafoué trop longtemps,</l>
					<l n="16" num="4.7">Mais qu'il faut relever, s'il en est encor temps !</l>
				</lg>
				<lg n="5">
					<l part="I" n="17" num="5.1">— Je ne te comprends pas…</l>
					<l part="F" n="17" num="5.1">— Écoute, ma chérie :</l>
					<l n="18" num="5.2">Je viens de découvrir que j'aimais ma pairie !…</l>
					<l n="19" num="5.3">Ma foi, c'est vrai, j'étais incrédule et railleur ;</l>
					<l n="20" num="5.4">C'est mon pays vaincu qui m'a rendu meilleur,</l>
					<l n="21" num="5.5">C'est pourquoi j'ai pleuré dans le fond de moi-même,</l>
					<l n="22" num="5.6">Comme si je perdais un des êtres que j'aime :</l>
					<l n="23" num="5.7">Je m'étais endormi ne croyant plus à rien…</l>
					<l n="24" num="5.8">Au réveil, je me suis relevé citoyen !</l>
					<l n="25" num="5.9">— Des mots que tout cela ! des phrases de poëte !</l>
					<l n="26" num="5.10">Quelque rhéteur obscur t'aura monté la tête !</l>
					<l n="27" num="5.11">Ta patrie est ici ; c'est ton enfant, c'est moi !</l>
					<l n="28" num="5.12">Le reste ? que me fait le reste, excepté toi ?</l>
					<l n="29" num="5.13">Pourquoi donc vouloir faire une tâche plus grande</l>
					<l n="30" num="5.14">Que celle que la loi du peuple te demande ?</l>
					<l n="31" num="5.15">N'es-tu pas marié, n'es-tu pas père enfin ?</l>
					<l part="I" n="32" num="5.16">Reste ! tu dois rester !</l>
					<l part="F" n="32" num="5.16">— Oh ! c'est trop à la fin !</l>
					<l n="33" num="5.17">El tu ne comprends pas ! Que veux-tu que je dise</l>
					<l n="34" num="5.18">Alors ? Mais c'est à nous que l'invasion brise,</l>
					<l n="35" num="5.19">A nous dont elle vient menacer le foyer,</l>
					<l n="36" num="5.20">D'être une légion qui se lève en entier !</l>
					<l n="37" num="5.21">Comment ! le prolétaire irait pour les défendre,</l>
					<l n="38" num="5.22">Lui qui n'a rien a perdre, eux dont on peut tout prendre !</l>
					<l n="39" num="5.23">Comment ! étant époux, je suis moins citoyen,</l>
					<l n="40" num="5.24">Et la France en danger je ne lui dois plus rien !</l>
					<l n="41" num="5.25">Tiens ! écoute une voix qui parle haut à lame ?</l>
					<l part="I" n="42" num="5.26">Entends-tu le canon qui tonne ?</l>
					<l part="F" n="42" num="5.26">Oh ! pauvre femme !</l>
					<l part="I" n="43" num="5.27">Pauvre mère !</l>
					<l part="F" n="43" num="5.27">Il en est qui tombent aujourd'hui,</l>
					<l n="44" num="5.28">Qui, le pays mourant, se sont levés pour lui,</l>
					<l n="45" num="5.29">Pour payer de leur sang ta défense et la nôtre !</l>
					<l n="46" num="5.30">Et je n'oserais pas me battre comme un autre !</l>
					<l n="47" num="5.31">Et je resterais là, bras croisés, sans rien voir,</l>
					<l n="48" num="5.32">Quand il n'en est pas un qui n'ait fait son devoir !</l>
					<l n="49" num="5.33">Car tu le veux ainsi, toi, l'une des meilleures !</l>
					<l n="50" num="5.34">Car tu me vois remplir mon devoir, et tu pleures !</l>
					<l n="51" num="5.35">Et tu ne m'as pas mis le fusil dans la main !</l>
					<l n="52" num="5.36">Et quand après cinq mois de jours sans lendemain,</l>
					<l n="53" num="5.37">Quand la France est debout, tout entière enfiévrée,</l>
					<l n="54" num="5.38">Je me lève à mon tour pour la cause sacrée,</l>
					<l n="55" num="5.39">Qui de chacun de nous eût dû faire un martyr,</l>
					<l n="56" num="5.40">Tu dis que je suis fou de songer à partir !</l>
				</lg>
				<lg n="6">
					<l n="57" num="6.1">Mais tu ne sens donc pas quel courant nous entraîne ?</l>
					<l n="58" num="6.2">Mois tu ne sens donc pas que l'heure est souveraine,</l>
					<l n="59" num="6.3">Et qu'il faut à présent oublier un passé</l>
					<l n="60" num="6.4">Que tout le sang d'un peuple aura vite effacé ?</l>
					<l n="61" num="6.5">Toi, Française, au moment où la tempête monte,</l>
					<l n="62" num="6.6">Tu te mets froidement du parti de la honte !</l>
				</lg>
				<lg n="7">
					<l n="63" num="7.1">Des mots, patriotisme, honneur ? — En vérité !</l>
					<l n="64" num="7.2">C'est avec ces mots-là qu'on fait l'humanité !</l>
					<l n="65" num="7.3">Et si je dois mourir en défendant ma cause,</l>
					<l n="66" num="7.4">Je serai mort au moins pour sauver quelque chose !</l>
					<l n="67" num="7.5">Mais tu baisses la tête, et tu comprends aussi…</l>
				</lg>
				<lg n="8">
					<l part="I" n="68" num="8.1">— Oui ! j'étais lâche ! Tiens… Va te battre !</l>
					<l part="F" n="68" num="8.1">— Merci !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">1er Janvier 1871</date>
					</dateline>
				</closer>
			</div></body></text></TEI>