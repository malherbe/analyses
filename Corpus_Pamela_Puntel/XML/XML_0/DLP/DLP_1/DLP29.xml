<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP29">
				<head type="number">XXIX</head>
				<head type="main">LES ÉTRENNES DE PARIS</head>
				<lg n="1">
					<l n="1" num="1.1">Allons ! pille, assassine, arrache, égorge encore,</l>
					<l n="2" num="1.2">O Temps inassouvi dont la faux nous dévore !</l>
					<l n="3" num="1.3">Entasse, dans tes jours plus longs qu'un siècle entier,</l>
					<l n="4" num="1.4">Les ruines sans nom que fait le Hun altier !</l>
					<l n="5" num="1.5">Va ! va ! poursuis ton vol au milieu de nos plaines,</l>
					<l n="6" num="1.6">Où l'invasion monte en tempêtes humaines !</l>
					<l n="7" num="1.7">Fais couler de la ville au pays du labour</l>
					<l n="8" num="1.8">Le sang de Woerth après le sang de Wissembourg !</l>
					<l n="9" num="1.9">Fais tomber cet espoir qui toujours se redonne</l>
					<l n="10" num="1.10">De Sedan qu'on trahit à Metz qu'on abandonne !</l>
					<l n="11" num="1.11">Va toujours ! viens cerner Paris entre ses forts ;</l>
					<l n="12" num="1.12">Fais-lui comme ceinture un vaste champ de morts</l>
					<l n="13" num="1.13">Tombés pour son orgueil et pour sa délivrance ;</l>
					<l n="14" num="1.14">Enfin amasse tout, deuils, sanglots et souffrance,</l>
					<l n="15" num="1.15">Tu n'empêcheras point que nous, vaincus d'hier,</l>
					<l n="16" num="1.16">Debout sous le grand ciel qui luit joyeux et clair,</l>
					<l n="17" num="1.17">Nous ne venions, du fond de la ville cernée,</l>
				</lg>
				<lg n="2">
					<l n="18" num="2.1">Te souhaiter, O France, une superbe année !</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1">Écoute, nous avons une étrenne à t'offrir :</l>
					<l n="20" num="3.2">Trois cent mille soldais qui sont prêts à mourir !</l>
					<l n="21" num="3.3">Et nous avons souffert, va, les uns et les autres,</l>
					<l n="22" num="3.4">Car la neige et la faim ont frappé bien des nôtres !</l>
					<l n="23" num="3.5">Le froid est dur pendant les grand'gardes de nuit…</l>
					<l n="24" num="3.6">Mais ta sainte pensée est là qui nous conduit,</l>
					<l n="25" num="3.7">Mais ton nom bien-aimé résonne à notre oreille,</l>
					<l n="26" num="3.8">Et tout cela nous rend la chaleur sans pareille,</l>
					<l n="27" num="3.9">Mystérieux accord appris par la douleur,</l>
					<l n="28" num="3.10">Qui fait monter le sang plus chaud à notre cœur !</l>
				</lg>
				<lg n="4">
					<l n="29" num="4.1">C'est un beau jour de l'an dans la ville assiégée !</l>
				</lg>
				<lg n="5">
					<l n="30" num="5.1">Les maudits qui la croient de désespoir rongée,</l>
					<l n="31" num="5.2">En proie aux factions des traîtres et des fous,</l>
					<l n="32" num="5.3">Oh ! s'ils pouvaient nous voir unis, résolus, tous !</l>
					<l n="33" num="5.4">Oh ! s'ils pouvaient la voir, notre armée aguerrie,</l>
					<l n="34" num="5.5">Légions qu'enfanta l'appel de la patrie !</l>
					<l n="35" num="5.6">Tous ces dormeurs d'hier réveillés à ton nom,</l>
					<l n="36" num="5.7">Et qu'a déjà brunis le souffle du canon !</l>
				</lg>
				<lg n="6">
					<l n="37" num="6.1">Et tout cela pour toi, France, mère adorée !</l>
				</lg>
				<lg n="7">
					<l n="38" num="7.1">Chacun a bien compris que l'heure était sacrée,</l>
					<l n="39" num="7.2">Et qu'il fallait lutter jusqu'à la mort ici</l>
					<l n="40" num="7.3">Pour que l'on pût là-bas se relever aussi !</l>
				</lg>
				<lg n="8">
					<l n="41" num="8.1">N'est-ce pas que l'étrenne est belle ? — On te l'envoie !</l>
					<l n="42" num="8.2">Sache que nous souffrons ce qu'on souffre avec joie ;</l>
					<l n="43" num="8.3">Sache que tous ont mis les douleurs en commun,</l>
					<l n="44" num="8.4">Et que le désespoir cherche encore quelqu'un !</l>
					<l n="45" num="8.5">Car pour tout oublier, larmes, craintes, prières,</l>
					<l n="46" num="8.6">Et tout le sang des fils, et tous les pleurs des mères,</l>
					<l n="47" num="8.7">El tous ceux qui partis ne sont pas revenus,</l>
					<l n="48" num="8.8">Pour nous faire oublier ces tourments inconnus</l>
					<l n="49" num="8.9">De la faim, du danger, du froid, de l'ignorance,</l>
					<l n="50" num="8.10">Il suffit qu'on se dise un seul mot : pour la France !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="9">
					<l n="51" num="9.1">Allons ! pille, assassine, arrache, égorge encore,</l>
					<l n="52" num="9.2">O Temps inassouvi dont la faux nous dévore !</l>
					<l n="53" num="9.3">Tu n'as pas empêché quo les vaincus d'hier,</l>
					<l n="54" num="9.4">Debout sous le grand ciel qui luit joyeux et clair,</l>
					<l n="55" num="9.5">Ne soient venus du fond de ta ville cernée,</l>
				</lg>
				<lg n="10">
					<l n="56" num="10.1">Te souhaiter, ô France, une superbe année !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">1er Janvier 1871</date>
					</dateline>
				</closer>
			</div></body></text></TEI>