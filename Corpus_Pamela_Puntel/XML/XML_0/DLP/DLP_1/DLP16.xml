<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP16">
				<head type="number">XVI</head>
				<head type="main">LA PETITE VILLE</head>
				<head type="main">A PHALSBOURG</head>
				<lg n="1">
					<l n="1" num="1.1">Contre un ils étaient venus mille !…</l>
					<l n="2" num="1.2">Mais combien d'entr'eux sont restés</l>
					<l n="3" num="1.3">Sous tes murs, ô petite ville,</l>
					<l n="4" num="1.4">Qu'Homère ou Dante auraient chantés ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Combien sont tombés sous tes balles,</l>
					<l n="6" num="2.2">Frappés au cœur et sans souiller</l>
					<l n="7" num="2.3">Tes forteresses virginales</l>
					<l n="8" num="2.4">Que le Maudit veut violer ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Combien reverront leur patrie</l>
					<l n="10" num="3.2">Pour raconter à leurs enfants</l>
					<l n="11" num="3.3">Comment deux mois, toujours meurtrie,</l>
					<l n="12" num="3.4">Toujours debout tu te défends ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Tu n'étais qu'un, ils étaient mille,</l>
					<l n="14" num="4.2">De flamme et de fer hérissés ;</l>
					<l n="15" num="4.3">Tu n'avais, ô petite ville,</l>
					<l n="16" num="4.4">Que ton cœur, et ce fut assez !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Oh ! tant que vivra notre France,</l>
					<l n="18" num="5.2">Oh ! tant qu'elle ira l’œil fixé</l>
					<l n="19" num="5.3">Vers l'avenir comme espérance,</l>
					<l n="20" num="5.4">Comme regret vers le passé ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Tant qu'elle pourra voir le monde,</l>
					<l n="22" num="6.2">Graviter docile à la voix</l>
					<l n="23" num="6.3">Qui sort de sa gorge profonde</l>
					<l n="24" num="6.4">Pour enseigner peuples et rois ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Tant qu'elle régnera tranquille</l>
					<l n="26" num="7.2">Sous sa couronne de clarté ;</l>
					<l n="27" num="7.3">Si longtemps, ô petite ville,</l>
					<l n="28" num="7.4">Ton nom partout sera cité !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Les peuples poursuivront la route</l>
					<l n="30" num="8.2">Où le destin les a poussés,</l>
					<l n="31" num="8.3">En écoutant comme on écoute</l>
					<l n="32" num="8.4">L'enseignement des jours passés ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Les générations humaines</l>
					<l n="34" num="9.2">Disparaîtront dans leurs tombeaux,</l>
					<l n="35" num="9.3">Ainsi qu'un amas d'ombres vaines</l>
					<l n="36" num="9.4">Que la Mort mène par troupeaux,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Et toujours on lira de même,</l>
					<l n="38" num="10.2">Les soirs d'hiver, à son foyer,</l>
					<l n="39" num="10.3">Cette Illiade sans poëme</l>
					<l n="40" num="10.4">Dont le chantre : est un peuple entier !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . </ab>
				<lg n="11">
					<l n="41" num="11.1">Contre un ils' étaient venus mille !…</l>
					<l n="42" num="11.2">Mais combien d'entr'eux sont restés</l>
					<l n="43" num="11.3">Sous tes murs, ô petite ville,</l>
					<l n="44" num="11.4">Qu'Homère ou Dante auraient chantés !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, le 20 septembre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>