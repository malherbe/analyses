<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP2">
				<head type="number">II</head>
				<head type="main">LA LÉGENDE DU DRAPEAU</head>
				<lg n="1">
					<l n="1" num="1.1">On se battait depuis cinq heures du matin,</l>
					<l n="2" num="1.2">Et nos soldats pliaient, vaincus par le destin ;</l>
					<l n="3" num="1.3">Mais tels qu'un aigle altier accroupi dans son aire,</l>
					<l n="4" num="1.4">Ils voulaient regarder en face le tonnerre.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Les Prussiens étaient quatre fois plus nombreux :</l>
					<l n="6" num="2.2">La mitraille de fer qu'ils vomissaient contre eux,</l>
					<l n="7" num="2.3">Fauchait les rangs français comme en juillet l'orage</l>
					<l n="8" num="2.4">Courbe les épis d'or debout sur son passage…</l>
					<l n="9" num="2.5">Rien n'y faisait : toujours, froidement, pas à pas,</l>
					<l n="10" num="2.6">Ces glorieux vaincus qu'on n'épouvante pas,</l>
					<l n="11" num="2.7">Pour sauver la retraite où reculaient les nôtres,</l>
					<l n="12" num="2.8">Calmes, se regardaient mourir les uns les autres.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Ils allaient, sachant bien qu'ils étaient condamnés.</l>
				</lg>
				<lg n="4">
					<l n="14" num="4.1">Tout à coup un conscrit dit : — Nous sommes cernés !</l>
					<l n="15" num="4.2">En effet, parallèle à notre infanterie,</l>
					<l n="16" num="4.3">Les Prussiens avaient mis leur artillerie,</l>
					<l n="17" num="4.4">Afin de nous couper la retraite du pont !</l>
					<l n="18" num="4.5">En tordant sa moustache un commandant répond :</l>
					<l n="19" num="4.6">— Va bien ! Allons toujours, enfants, c'est la consigne.</l>
					<l n="20" num="4.7">Ils vont : et dans les rangs, pas un cri, pas un signe,</l>
					<l n="21" num="4.8">Qui montre que ces gens décimés par la mort ,</l>
					<l n="22" num="4.9">Vaincus, aient abjuré l'espoir de vaincre encor.</l>
				</lg>
				<lg n="5">
					<l n="23" num="5.1">Un petit lieutenant de dix-neuf ans à peine</l>
					<l part="I" n="24" num="5.2">Dit :</l>
					<l part="F" n="24" num="5.2">— Commandant ! j'en vois dix mille dans la plaine !</l>
					<l n="25" num="5.3">Et le commandant dit une seconde fois :</l>
					<l n="26" num="5.4">— Va bien ! Allons toujours : je vois ce que tu vois…</l>
				</lg>
				<lg n="6">
					<l part="I" n="27" num="6.1">Ils vont.</l>
				</lg>
				<lg n="7">
					<l part="F" n="27">Les Prussiens redoublent la mitraille,</l>
					<l n="28" num="7.1">Croyant pouvoir d'un coup terminer la bataille,</l>
					<l n="29" num="7.2">Quand un vieux capitaine, un ancien de l’Alma,</l>
					<l n="30" num="7.3">Dont la poudre a bruni la peau qu'elle enflamma,</l>
					<l part="I" n="31" num="7.4">Dit :</l>
					<l part="F" n="31" num="7.4">— Commandant, ils vont nous prendre par derrière !</l>
					<l part="I" n="32" num="7.5">La commandant répond :</l>
					<l part="F" n="32" num="7.5">— Va bien ! qu'y veux-tu faire ?</l>
					<l part="I" n="33" num="7.6">Allons toujours !…</l>
					<l part="M" n="33" num="7.6">Ils vont.</l>
					<l part="F" n="33" num="7.6">Le canon ennemi</l>
					<l n="34" num="7.7">Fait sa trouée énorme et les fauche à demi.</l>
					<l n="35" num="7.8">Tout à coup, au lointain, viennent au pas de charge</l>
					<l n="36" num="7.9">Dix régiments, tenant mille mètres de large,</l>
					<l n="37" num="7.10">Et faisant sur la droite un obstacle contre eux.</l>
				</lg>
				<lg n="8">
					<l n="38" num="8.1">Les Prussiens étaient douze fois plus nombreux.</l>
				</lg>
				<lg n="9">
					<l n="39" num="9.1">C'était comme une mer d'hommes et de fumée</l>
					<l n="40" num="9.2">Se resserrant toujours autour de notre armée.</l>
					<l n="41" num="9.3">Alors le commandant lorgne les alentours,</l>
					<l part="I" n="42" num="9.4">Et dit tout bas :</l>
					<l part="F" n="42" num="9.4">Va mal ! — N'importe !… allons toujours !…</l>
					<l part="I" n="43" num="9.5">Ils vont.</l>
					<l part="F" n="43" num="9.5">Mais cette fois ils retournent la tête,</l>
					<l n="44" num="9.6">Et, chargeant en avant avec la baïonnette,</l>
					<l n="45" num="9.7">Cherchent à se frayer un passage sanglant</l>
					<l n="46" num="9.8">A travers ce réseau de fer étincelant.</l>
				</lg>
				<lg n="10">
					<l n="47" num="10.1">Oh ! les lions français terribles et superbes !</l>
				</lg>
				<lg n="11">
					<l n="48" num="11.1">Comme le vent qui fait courber les hautes herbes,</l>
					<l n="49" num="11.2">A travers les boulets, les obus et le fer</l>
					<l n="50" num="11.3">Qui tombent sur leur front avec un bruit d'enfer,</l>
					<l n="51" num="11.4">Ils vont, amoncelant les morts sur les ruines,</l>
					<l n="52" num="11.5">Pour creuser un sillon à travers des poitrines !</l>
				</lg>
				<lg n="12">
					<l n="53" num="12.1">Tout à coup, au milieu du terrible chemin,</l>
					<l n="54" num="12.2">Un cri sort, effrayant, de ce charnier humain :</l>
					<l n="55" num="12.3">C'est le drapeau français qui tombe, et qu'on menace…</l>
					<l n="56" num="12.4">Non ! un jeune conscrit s'élance, et le ramasse…</l>
					<l n="57" num="12.5">Une balle le tue ! — un deuxième le prend…</l>
					<l part="I" n="58" num="12.6">Un biscayen l'écrase !…</l>
					<l part="F" n="58" num="12.6">— Alors, de rang en rang,</l>
					<l n="59" num="12.7">Et toujours en chargeant en avant, tête basse,</l>
					<l n="60" num="12.8">Toujours de main en main le drapeau français passe,</l>
					<l n="61" num="12.9">Prenant pour défenseurs ceux qui veulent s'offrir :</l>
					<l n="62" num="12.10">Après celui qui meurt, celui qui va mourir !</l>
				</lg>
				<lg n="13">
					<l n="63" num="13.1">Trois frères étaient là. Pour défendre leur France</l>
					<l n="64" num="13.2">Ils s'étaient engagés, n'ayant d'autre espérance</l>
					<l n="65" num="13.3">Que de mourir pour elle en faisant leur devoir :</l>
					<l n="66" num="13.4">Vraiment, on aurait dit trois enfants à les voir.</l>
					<l n="67" num="13.5">Le plus vieux a vingt ans, le plus jeune en a seize.</l>
					<l n="68" num="13.6">L'aîné prend le drapeau dans ses mains et le baise,</l>
					<l n="69" num="13.7">Puis, regardant le ciel comme un martyr chrétien,</l>
					<l n="70" num="13.8">Il dit, en élevant le bras qui le soutient :</l>
					<l part="I" n="71" num="13.9">— Dieu me garde ! en avant !</l>
					<l part="M" n="71" num="13.9">Il est tué.</l>
					<l part="F" n="71" num="13.9">Son frère,</l>
					<l n="72" num="13.10">Fait le signe de croix, une courte prière,</l>
					<l part="I" n="73" num="13.11">Et le prend à son tour en disant :</l>
					<l part="F" n="73" num="13.11">— En avant !</l>
					<l part="I" n="74" num="13.12">Il est tué.</l>
					<l part="F" n="74" num="13.12">Derrière, arme au poing, le suivant,</l>
					<l n="75" num="13.13">Le troisième relève avec sa main meurtrie</l>
					<l n="76" num="13.14">Ce chiffon glorieux, âme de la patrie,</l>
					<l part="I" n="77" num="13.15">Et répète :</l>
					<l part="M" n="77" num="13.15">— En avant !</l>
					<l part="M" n="77" num="13.15">Il est tué.</l>
					<l part="F" n="77" num="13.15">Grand Dieu !</l>
					<l n="78" num="13.16">Sous cette pluie ardente où l'ondée est du feu,</l>
					<l n="79" num="13.17">Toujours pour relever le drapeau qui frissonne,</l>
					<l n="80" num="13.18">Toujours quelqu'un, avant qu'il n'y ait plus personne !</l>
					<l n="81" num="13.19">Le conscrit volontaire ou le vieux vétéran</l>
					<l n="82" num="13.20">Tour à tour le relève et le sauve en mourant :</l>
					<l n="83" num="13.21">Vingt-huit fois le drapeau qui tombe, se redresse,</l>
					<l n="84" num="13.22">Agitant dans ses plis son ombre vengeresse !</l>
					<l n="85" num="13.23">On nous parle beaucoup des vieux Léonidas :</l>
					<l n="86" num="13.24">Qu'ont-ils fait de plus beau que ces vingt-huit soldats !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Château de Pray, 12 août.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>