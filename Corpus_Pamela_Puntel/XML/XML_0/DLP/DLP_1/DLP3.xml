<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP3">
				<head type="number">III</head>
				<head type="main">LA HONTE</head>
				<lg n="1">
					<l n="1" num="1.1">Il est midi : le ciel est brillant de gaieté</l>
					<l n="2" num="1.2">Sous les doux chatoiements d'un beau soleil d'été ;</l>
					<l n="3" num="1.3">La brise est douce, et va, parfumant la campagne,</l>
					<l n="4" num="1.4">Du hêtre de la plaine au pin de la montagne ;</l>
					<l n="5" num="1.5">L'alouette s'élève en chantant sa chanson</l>
					<l n="6" num="1.6">Fraîche comme la fleur qui croît sur un buisson ;</l>
					<l n="7" num="1.7">Vous voyez ce tableau fait d'ombre et de lumière :</l>
					<l n="8" num="1.8">Dans le fond, la forêt, dont la sombre lisière</l>
					<l n="9" num="1.9">Borde légèrement la route tout au long,</l>
					<l n="10" num="1.10">Comme un mantelet brun jeté sur le vallon ;</l>
					<l n="11" num="1.11">Plus bas, le ruisseau clair coulant son eau tranquille,</l>
					<l n="12" num="1.12">Et plus loin, les maisons d'une petite ,ville</l>
					<l n="13" num="1.13">Toute blanche, au milieu de ce beau jour d'été</l>
					<l n="14" num="1.14">Qui respire l'amour, la vie et la gaieté !</l>
				</lg>
				<lg n="2">
					<l n="15" num="2.1">La ville, c'est Sedan ; le jour, le Deux Septembre.</l>
				</lg>
				<lg n="3">
					<l part="I" n="16" num="3.1">Comprenez-vous cela, voyons !</l>
					<l part="F" n="16" num="3.1">Dans cette chambre,</l>
					<l n="17" num="3.2">Un homme, un empereur, a jeté dans un coin</l>
					<l n="18" num="3.3">Comme un hochet usé dont on n'a plus besoin</l>
					<l n="19" num="3.4">Et qu'on brise d'un coup sur un pan de muraille</l>
					<l n="20" num="3.5">Son arme, vierge encor des feux de la bataille !</l>
					<l n="21" num="3.6">Il est parti, disant : C'est moi le général !</l>
					<l n="22" num="3.7">Bah ! pour lui c'est assez de monter à cheval</l>
					<l n="23" num="3.8">Et d'aller en parade en tête d'une armée !</l>
					<l n="24" num="3.9">Mais que viennent les coups de fusil, la fumée</l>
					<l n="25" num="3.10">Du canon, les obus, le râle des soldats,</l>
					<l n="26" num="3.11">Tous ces héros obscurs que l'on ne connaît pas,</l>
					<l n="27" num="3.12">Cet homme, frissonnant devant cette tempête,</l>
					<l n="28" num="3.13">Rentrera son épée et baissera la tête,</l>
					<l n="29" num="3.14">Pendant que ses soldats qu'il fuit avec terreur,</l>
					<l n="30" num="3.15">Tomberont tous au cri de : Vive. l'Empereur !</l>
				</lg>
				<lg n="4">
					<l part="I" n="31" num="4.1">Quelqu'un vient et lui dit :</l>
					<l part="F" n="31" num="4.1">— La bataille est perdue.</l>
					<l n="32" num="4.2">La ligne jusqu'au bout s'est en vain défendue :</l>
					<l part="I" n="33" num="4.3">Ils étaient vingt contre un !… Que faire ?</l>
					<l part="F" n="33" num="4.3">— Rendez-vous.</l>
					<l n="34" num="4.4">— Nous rendre ! Nous avons l'ennemi devant nous,</l>
					<l n="35" num="4.5">Chargeons encore, et si lu moitié de nous tombe,</l>
					<l n="36" num="4.6">La moitié passera sur eux comme une trombe !</l>
					<l part="I" n="37" num="4.7">— Rendez-vous.</l>
				</lg>
				<lg n="5">
					<l part="F" n="37">— Quoi ! nous rendre ! Et l'honneur du drapeau ?</l>
				</lg>
				<lg n="6">
					<l n="38" num="6.1">Et la France par nous morte et mise au tombeau ?</l>
					<l n="39" num="6.2">Et la honte d'aller, nous, quatre-vingt mille hommes,</l>
					<l n="40" num="6.3">Dos soldats, des Français, armés comme nous sommes,</l>
					<l n="41" num="6.4">Oublieux du passé, nous jeter à genoux…</l>
					<l part="I" n="42" num="6.5">Impossible ! Nous rendre ! Allons donc !</l>
					<l part="F" n="42" num="6.5">— Rendez-vous.</l>
				</lg>
				<lg n="7">
					<l n="43" num="7.1">— Nous rendre ! Mais le monde est là qui nous regarde !</l>
					<l n="44" num="7.2">Mais la France à ses fils a confié sa garde !</l>
					<l n="45" num="7.3">Comme nous, notre épée est vivante, elle aussi !</l>
					<l n="46" num="7.4">Nous no pouvons aller nous rendre à leur merci !</l>
					<l n="47" num="7.5">Humilier devant ces Huns et ces Vandales</l>
					<l n="48" num="7.6">Qui sur nos fronts courbés essuieraient leurs sandales,</l>
					<l n="49" num="7.7">Vingt siècles de grandeur dont le monde est jaloux !</l>
					<l part="I" n="50" num="7.8">Sire ! devant le ciel, que faire ?</l>
					<l part="F" n="50" num="7.8">— Rendez-vous.</l>
				</lg>
				<lg n="8">
					<l n="51" num="8.1">— Sire ! nous pouvons tout sauver, même la honte !</l>
					<l n="52" num="8.2">Nous avons des héros avec lesquels on compte,</l>
					<l n="53" num="8.3">Les dragons, les hussards et ceux dos cuirassiers</l>
					<l n="54" num="8.4">De Reischoffen sont là, sabre au poing ; — essayez !</l>
					<l n="55" num="8.5">Sire ! ne perdez pas l'honneur de la patrie !</l>
					<l n="56" num="8.6">Sire ! voyez la France avilie et meurtrie</l>
					<l n="57" num="8.7">Qui tord ses bras maigris à force de souffrir,</l>
					<l n="58" num="8.8">Et qui nous dit de vaincre, ou sinon de mourir !</l>
					<l n="59" num="8.9">Sire ! nous devons compte à l'éternelle histoire</l>
					<l n="60" num="8.10">De nous, de nos soldats, de notre vieille gloire,</l>
					<l n="61" num="8.11">De nos aïeux pensifs qui nous regardent tous !</l>
					<l part="I" n="62" num="8.12">Sire ! ne perdez pas la France !</l>
					<l part="F" n="62" num="8.12">— Rendez-vous.</l>
				</lg>
				<lg n="9">
					<l n="63" num="9.1">Sacredieu ! pas un seul de tous ceux qu'on renomme,</l>
					<l n="64" num="9.2">Pas un ! n'osa casser la tête de cet homme !</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<l n="65" num="9.3">Ils ont capitulé ! C'est fini, bien fini !</l>
					<l n="66" num="9.4">De tout ce grand passé que n'ont jamais terni</l>
					<l n="67" num="9.5">Ni les jours de succès, ni les jours d'infortune,</l>
					<l n="68" num="9.6">Restent des légions jetant, une par une,</l>
					<l n="69" num="9.7">Le fusil qu'à ses fils la France avait donné,</l>
					<l n="70" num="9.8">Aux pieds d'un caporal prussien couronné !</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<l n="71" num="9.9">Il est minuit : le ciel, étoiles impassibles,.</l>
					<l n="72" num="9.10">Éclaire les coteaux endormis et paisibles :</l>
					<l n="73" num="9.11">Le rossignol des nuits gazouille sa chanson</l>
					<l n="74" num="9.12">« Fraîche comme la fleur qui croît sur un buisson ;</l>
					<l n="75" num="9.13">Plus bas, le ruisseau clair coule son eau tranquille,</l>
					<l n="76" num="9.14">Et plus loin les maisons d'une petite ville</l>
					<l n="77" num="9.15">Toute blanche, au milieu de cette nuit d'été,</l>
					<l n="78" num="9.16">Qui respire l'amour, la vie et la gaieté…</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Neufchâteau, 7 septembre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>