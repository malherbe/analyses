<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP11">
				<head type="number">XI</head>
				<head type="main">SURSUM CORDA</head>
				<div type="section" n="1">
					<head type="number"> I</head>
					<lg n="1">
						<l n="1" num="1.1">Allons ! n'hésitons plus, car l'heure est solennelle.</l>
						<l n="2" num="1.2">Puisque le malheur Vient-nous fouetter de son aile,</l>
						<l n="3" num="1.3">Puisque l'heure est venue où nous devons souffrir,</l>
						<l n="4" num="1.4">Soyons dignes de vivre en sachant bien mourir !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Eh ! quoi ! parce qu'un homme a vendu notre France,</l>
						<l n="6" num="2.2">Parce qu'il a jeté le pays sans défense</l>
						<l n="7" num="2.3">Aux mains de deux bandits nés du sang d'Attila,</l>
						<l n="8" num="2.4">Sans résister encor nous en resterions là !</l>
						<l part="I" n="9" num="2.5">Nous verrons !</l>
						<l part="F" n="9" num="2.5">Ils prendront Paris ? Soit, c'est possible !</l>
						<l n="10" num="2.6">Il ne restera plus qu'une cendre insensible</l>
						<l n="11" num="2.7">De la ville où le monde apprenait à penser ;</l>
						<l n="12" num="2.8">Sur son emplacement l'herbe pourra pousser,</l>
						<l n="13" num="2.9">Et l'on pourra semer, comme aux siècles antiques,</l>
						<l n="14" num="2.10">Du chanvre où s'élevaient ses créneaux héroïques ;</l>
						<l n="15" num="2.11">Mais après, nous aurons pour lever nos drapeaux</l>
						<l n="16" num="2.12">Quatorze millions de combattants nouveaux,</l>
						<l n="17" num="2.13">De l'or, du pain, du plomb, et le fer qui terrasse</l>
						<l n="18" num="2.14">Ceux qui pensent d'un coup frapper toute une race !</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1">Quand Paris sera mort, il nous restera Tours !</l>
						<l n="20" num="3.2">Où les Valois dressaient leurs châteaux aux cent tours ;</l>
						<l n="21" num="3.3">Tous les siècles passés inclinés sur la Loire</l>
						<l n="22" num="3.4">Refléteront d'un coup notre superbe histoire !</l>
					</lg>
					<lg n="4">
						<l n="23" num="4.1">Tours brûlé, nous aurons Bordeaux vivante encor !</l>
						<l n="24" num="4.2">Dans la vieille cité que brunit son ciel d'or,</l>
						<l n="25" num="4.3">Et que la main de Dieu pour jamais a bénie,</l>
						<l n="26" num="4.4">Nous irons enfouir la France et son génie !</l>
					</lg>
					<lg n="5">
						<l n="27" num="5.1">Quand ils l'auront brûlée et pillée, elle aussi,</l>
						<l n="28" num="5.2">Avant que nul de nous n'ait demandé merci,</l>
						<l n="29" num="5.3">Nous lutterons encor notre lutte impuissante,</l>
						<l n="30" num="5.4">Et Brest accueillera la France agonisante !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Lorsque Brest tombera, quand tout sera perdu,</l>
						<l n="32" num="6.2">Quand Dieu de notre appel n'aura rien entendu,</l>
						<l n="33" num="6.3">Quand nous sortirons morts de la fournaise immense</l>
						<l n="34" num="6.4">Où le monde à jamais finit et recommence,</l>
						<l n="35" num="6.5">Oh ! alors, n'ayant plus ni poudre, ni canon,</l>
						<l n="36" num="6.6">N'ayant plus du passé qu'un regret, et qu'un nom</l>
						<l n="37" num="6.7">De la plus grande encor des histoires humaines,</l>
						<l n="38" num="6.8">N'ayant plus d'âme au corps et de sang dans les veines,</l>
						<l n="39" num="6.9">Pour trouver un tombeau digne de son néant,</l>
						<l n="40" num="6.10">La France ira d'un bond sombrer dans l'Océan !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="41" num="1.1">Mais nous verrons plus tôt finir notre épopée !</l>
						<l n="42" num="1.2">La France ne fait plus reluire son épée</l>
						<l n="43" num="1.3">Pour défendre un tyran qu'elle ne voulait point !…</l>
						<l n="44" num="1.4">O nos fiers conquérants, nous n'irons pas si loin !</l>
						<l n="45" num="1.5">Reichsoffen et Sedan, vos semblants de victoire,</l>
						<l n="46" num="1.6">Vont crouler sous ces murs qu'illustrera l'histoire</l>
					</lg>
					<lg n="2">
						<l n="47" num="2.1">Paris va devenir votre immense tombeau !…</l>
					</lg>
					<lg n="3">
						<l n="48" num="3.1">Et plus tard, quand sera venu l'âge nouveau,</l>
						<l n="49" num="3.2">Où l'homme recueilli dans le passé remonte</l>
						<l n="50" num="3.3">Pour juger froidement l'héroïsme ou la honte ;</l>
						<l n="51" num="3.4">Lorsque le voyageur égaré dans nos champs</l>
						<l n="52" num="3.5">Trouvera vos débris gardés par nos enfants,</l>
						<l n="53" num="3.6">Et qu'il demandera le nom de cette armée</l>
						<l n="54" num="3.7">Qui sous nos coups puissants s'est un jour abîmée,</l>
						<l n="55" num="3.8">Et le nom de son roi dans l'ombre enseveli,</l>
						<l n="56" num="3.9">On répondra, mêlant de dédain et d'oubli</l>
						<l n="57" num="3.10">Le nom de ce tyran au nom de son royaume :</l>
					</lg>
					<lg n="4">
						<l n="58" num="4.1">— On ne sait plus qui c'est… Genséric ou Guillaume !…</l>
					</lg>
				</div>
				<closer>
					<dateline>
						<date when="1870">Paris, 23 septembre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>