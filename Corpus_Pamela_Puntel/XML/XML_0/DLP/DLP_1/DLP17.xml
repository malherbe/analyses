<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP17">
				<head type="number">XVII</head>
				<head type="main">APRÈS LE COMBAT</head>
				<lg n="1">
					<l part="I" n="1" num="1.1">Ce village là-bas c'est Frechwiller.</l>
					<l part="F" n="1" num="1.1">La nuit</l>
					<l n="2" num="1.2">Est arrivée, avec le repos qui la suit,</l>
					<l n="3" num="1.3">Couvrant d'ombres la plaine ou fut ce grand carnage</l>
					<l n="4" num="1.4">Qui pourrait rappeler les combats d'un autre âge,</l>
					<l n="5" num="1.5">Répandant au travers de ces champs désolés</l>
					<l n="6" num="1.6">Des cadavres humains partout amoncelés,</l>
					<l part="I" n="7" num="1.7">C'est navrant.</l>
					<l part="F" n="7" num="1.7">Los soldats qu'a fauchés la mitraille</l>
					<l n="8" num="1.8">Sont tombés l'un sur l'autre, en ordre de bataille,</l>
					<l n="9" num="1.9">Sans bouger de leur poste au suprême moment :</l>
					<l n="10" num="1.10">Auprès d'un régiment un autre régiment,</l>
					<l n="11" num="1.11">Près du général mort l'officier impassible ;</l>
					<l n="12" num="1.12">Et tous, fusil au poing, le front encor terrible,</l>
					<l n="13" num="1.13">N'ayant pas à la mort hésité de s'offrir,</l>
					<l n="14" num="1.14">Tels qu'ils avaient lutté se sont laissés mourir.</l>
				</lg>
				<lg n="2">
					<l n="15" num="2.1">Çà et là des canons encloués sur la terre,</l>
					<l n="16" num="2.2">Tordant leur affût noir étonné de se taire ;</l>
					<l n="17" num="2.3">Plus loin des chevaux morts, le poitrail rouge encor,</l>
					<l n="18" num="2.4">Partout le sang, partout le deuil, partout la mort !</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1">Avançons : le massacre en tous lieux se ressemble.</l>
				</lg>
				<lg n="4">
					<l n="20" num="4.1">Ici des grenadiers au panache qui tremble,</l>
					<l n="21" num="4.2">Là des soldats de ligne et des turcos couchés,</l>
					<l n="22" num="4.3">Rencontrés par la mort qui les avaient cherchés :</l>
					<l n="23" num="4.4">Nul n'a plié devant la trombe meurtrière ;</l>
					<l n="24" num="4.5">Pas de fuyards : aucun n'a regardé derrière !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Et sur ces morts qu'a faits la volonté d'un seul,</l>
					<l n="26" num="5.2">Le silence des nuits jeté comme un linceul.</l>
				</lg>
				<lg n="6">
					<l n="27" num="6.1">Oh ! qui pourrait savoir, oh ! qui pourrait connaître</l>
					<l n="28" num="6.2">Les bonheurs à venir qui dorment là peut-être !</l>
					<l n="29" num="6.3">Qui dirait ce que Dieu gardait à ces soldats</l>
					<l n="30" num="6.4">Que deux tyrans maudits ont immolés là-bas,</l>
					<l n="31" num="6.5">Pour leur ambition qui réclamait ses proies !</l>
					<l n="32" num="6.6">Qui dirait ce que Dieu leur réservait de joies !</l>
					<l n="33" num="6.7">Qui dirait l'avenir qui les attendait tous !</l>
					<l n="34" num="6.8">A celui-ci, l'enfant qu'on tient sur ses genoux</l>
					<l n="35" num="6.9">Et qui paie un baiser d'une douce caresse ;</l>
					<l n="36" num="6.10">A celui-là l'amour béni d'une maîtresse,</l>
					<l n="37" num="6.11">A cet autre qui dort pour ne plus s'éveiller,</l>
					<l n="38" num="6.12">La gloire que sa mort n'a même pu payer !</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Dijon, 20 août.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>