<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES DE GUERRE</title>
				<title type="sub_2">Poèmes publiés dans LA REVUE DES DEUX MONDES (1871)</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES DE GUERRE</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.m.wikisource.org/wiki/Po%C3%A9sies_de_guerre</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>REVUE DES DEUX MONDES</title>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>REVUE DES DEUX MONDES</publisher>
									<date when="1871">1871</date>
								</imprint>
								<biblScope unit="tome">91</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP38">
			<head type="number">III. — L’ORPHELIN</head>
				<lg n="1">
					<l n="1" num="1.1">La mère est accoudée à la table de chêne ;</l>
					<l n="2" num="1.2">À ses pieds, l’enfant tient son écheveau de laine.</l>
					<l n="3" num="1.3">Et joue en souriant d’un sourire charmant.</l>
					<l n="4" num="1.4">Un peintre esquisserait ce tableau-là gaîment :</l>
					<l n="5" num="1.5">La mère est jeune, elle a la beauté qui rayonne.</l>
					<l n="6" num="1.6">Lorsque les yeux sont doux, et lorsque l’âme est bonne.</l>
					<l n="7" num="1.7">Elle est seule à présent, car son mari se bat,</l>
					<l n="8" num="1.8">Et voilà deux cents jours que dure le combat !</l>
					<l n="9" num="1.9">Hier encore elle a pu lire une chère lettre,</l>
					<l n="10" num="1.10">Alors elle s’est dit : Il reviendra peut-être !…</l>
					<l n="11" num="1.11">Il reviendra, c’est sûr, car mon cœur me l’a dit !…</l>
				</lg>
				<lg n="2">
					<l n="12" num="2.1">Hélas ! voyez le soir qui vient, un soir maudit !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">On remet à la mère une dépêche… Elle ouvre…</l>
					<l n="14" num="3.2">Pourquoi ce deuil ? pourquoi ce voile qui la couvre ?</l>
					<l n="15" num="3.3">Elle dit à l’enfant qu’elle tient dans ses bras :</l>
					<l n="16" num="3.4">« Lorsque tu seras grand, tu ne te battras pas ! »</l>
				</lg>
				<lg n="4">
					<l n="17" num="4.1">Mère, tu comprends mal la tâche qui t’incombe :</l>
					<l n="18" num="4.2">Parle au fils au berceau du père dans la tombe !</l>
					<l n="19" num="4.3">Fais pendant quatorze ans bondir ce jeune cœur,</l>
					<l n="20" num="4.4">Sans cesse, en lui disant ce qu’a fait le vainqueur !</l>
					<l n="21" num="4.5">Pour qu’il rêve à son tour une horrible victoire,</l>
					<l n="22" num="4.6">Déroule devant lui notre sanglante histoire !</l>
					<l n="23" num="4.7">Dans cette âme que Dieu fit naître pour aimer.</l>
					<l n="24" num="4.8">C’est la haine contre eux qu’il faut faire germer !</l>
					<l n="25" num="4.9">Parle-lui du passé, parle-lui de la honte,</l>
					<l n="26" num="4.10">Et qu’il veuille effacer tout ce qu’on lui raconte,</l>
					<l n="27" num="4.11">Pour qu’il ne reste rien de leurs crimes anciens.</l>
					<l n="28" num="4.12">Par le sang du maudit et par le sang des siens !</l>
					<l n="29" num="4.13">La mère d’aujourd’hui doit s’inspirer de Rome :</l>
					<l n="30" num="4.14">Ils l’ont fait orphelin ? À toi d’en faire un homme !</l>
				</lg>
			</div></body></text></TEI>