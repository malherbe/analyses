<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA COLÈRE D’UN FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="MEN">
					<name>
						<forename>Catulle</forename>
						<surname>MENDÈS</surname>
					</name>
					<date from="1841" to="1909">1841-1909</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>124 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MEN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA COLÈRE D’UN FRANC-TIREUR</title>
						<author>CATULLE MENDÈS</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LEMERRE</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="MEN44">
				<head type="main">LA COLÈRE D’UN FRANC-TIREUR</head>
				<head type="sub_2">Poème Dit par M. COQUELIN de la Comédie-Française</head>
				<lg n="1">
					<l n="1" num="1.1">Major, je n’entends rien à votre médecine.</l>
					<l n="2" num="1.2">La tisane m’assomme et le lit m’assassine.</l>
					<l n="3" num="1.3">Si je ne suis guéri demain, à mon réveil,</l>
					<l n="4" num="1.4">Morbleu ! traînant ma jambe avec votre appareil,</l>
					<l n="5" num="1.5">Je rejoindrai, boiteux ou non, les camarades.</l>
					<l n="6" num="1.6">Je réclame mon lot de gloire et de bourrades.</l>
					<l n="7" num="1.7">S’il faut saigner là-bas sous quelque obus prussien,</l>
					<l n="8" num="1.8">Tant mieux ! un nouveaux mal guérira de l’ancien,</l>
					<l n="9" num="1.9">(Vous nommerez cela de l’homœopathie),</l>
					<l n="10" num="1.10">Et, si l’on meurt, je veux être de la partie.</l>
					<l n="11" num="1.11">Je puis mourir, n’ayant ni femme ni marmots.</l>
					<l n="12" num="1.12">Ma fureur te surprend, major ? En quatre mots</l>
					<l n="13" num="1.13">Voici pourquoi je veux quitter cette paillasse.</l>
				</lg>
				<lg n="2">
					<l n="14" num="2.1">Nous marchions. Nous étions quatre cents, tous d’Alsace.</l>
					<l n="15" num="2.2">Comme on était parti dès le soleil levant,</l>
					<l n="16" num="2.3">Nul n’aurait pu, le soir, faire un pas en avant</l>
					<l n="17" num="2.4">Sans le clairon hardi qui chante et qui réveille ;</l>
					<l n="18" num="2.5">Ce bruit-là, c’est du rhum que l’on boit par l’oreille.</l>
					<l n="19" num="2.6">Il fallut s’arrêter pourtant, dormant déjà.</l>
					<l n="20" num="2.7">Près d’une roche un bouc passait, on le mangea,</l>
					<l n="21" num="2.8">Tandis qu’autour de nous, pour des scènes funèbres,</l>
					<l n="22" num="2.9">Comme de noirs décors s’élevaient les ténèbres.</l>
				</lg>
				<lg n="3">
					<l n="23" num="3.1">Connaissez-vous l’opaque et tenace sommeil</l>
					<l n="24" num="3.2">Qui résiste à la pluie, au jour, au cri vermeil</l>
					<l n="25" num="3.3">Des trompettes sonnant la diane éclatante,</l>
					<l n="26" num="3.4">Le sommeil harassé du soldat sous la tente ?</l>
					<l n="27" num="3.5">C’est lui qui me coucha près d’un arbre, à l’écart.</l>
					<l n="28" num="3.6">Je vis confusément dans un dernier regard</l>
					<l n="29" num="3.7">Mes compagnons autour d’un feu de feuilles sèches,</l>
					<l n="30" num="3.8">Et la plaine, et, pareils à des faisceaux de flèches,</l>
					<l n="31" num="3.9">Les peupliers perçant le vide aérien,</l>
					<l n="32" num="3.10">Et des coteaux, là-bas, et de l’ombre ; puis, rien.</l>
				</lg>
				<lg n="4">
					<l n="33" num="4.1">Quand j’ouvris l’œil, au bord du ciel naissait l’aurore.</l>
					<l n="34" num="4.2">Les membres lourds, l’esprit plein de brumes encor,</l>
					<l n="35" num="4.3">Pour secouer le froid, invisible linceul,</l>
					<l n="36" num="4.4">Je me levai, cherchant les autres. J’étais seul.</l>
					<l n="37" num="4.5">Seul ! — Sans doute, éveillés par de brusques alarmes,</l>
					<l n="38" num="4.6">En courant, en criant, ils avaient pris les armes,</l>
					<l n="39" num="4.7">Mais moi, dans le silence et dans l’ombre perdu,</l>
					<l n="40" num="4.8">Stupide, je n’avais rien vu, rien entendu :</l>
					<l n="41" num="4.9">Je dormais ! — A présent, c’est clair, j’étais un lâche,</l>
					<l n="42" num="4.10">J’étais le vil goujat qui se sauve ou se cache</l>
					<l n="43" num="4.11">A l’heure de l’alerte et du danger commun ;</l>
					<l n="44" num="4.12">Et, peut-être, guettant le moment opportun,</l>
					<l n="45" num="4.13">Le cœur chaud, le bras fort, l’arme bien épaulée,</l>
					<l n="46" num="4.14">Mes amis s’embusquaient là-bas, dans la vallée,</l>
					<l n="47" num="4.15">Et disaient en parlant du camarade enfui :</l>
					<l n="48" num="4.16">« Tiens, je n’aurais jamais pensé cela de lui ! »</l>
					<l part="I" n="49" num="4.17">Enfer !</l>
				</lg>
				<lg n="5">
					<l part="F" n="49">Soudain, j’entends des coups de feu. J’écoute,</l>
					<l n="50" num="5.1">Collant l’oreille à terre. On se bat, plus de doute,</l>
					<l n="51" num="5.2">Mais un peu loin, vers l’est, entre des mamelons.</l>
					<l n="52" num="5.3">N’importe ! En avant, marche ! au pas de course ! allons !</l>
					<l n="53" num="5.4">Et dans leur sac que font sauter mes bonds farouches</l>
					<l n="54" num="5.5">J’entends se remuer, joyeuses, mes cartouches !</l>
				</lg>
				<lg n="6">
					<l n="55" num="6.1">En courant, je glissai. Bête brute ! animal !</l>
					<l n="56" num="6.2">Sur un caillou. Je crus ne pas m’être fait mal.</l>
					<l n="57" num="6.3">Mais quatre pas plus loin, — oh ! le diable t’emporte,</l>
					<l n="58" num="6.4">Os maudit ! — cette jambe autrefois droite et forte,</l>
					<l n="59" num="6.5">Cette jambe, — tenez, coupez-la-moi, major ! —</l>
					<l n="60" num="6.6">En s’affaissant sous moi, me fit tomber encor,</l>
					<l n="61" num="6.7">Avec ce cri de rage et de douleur « cassée ! »</l>
					<l n="62" num="6.8">Là-bas la fusillade éclatait, plus pressée,</l>
					<l n="63" num="6.9">Disant : « Viens, tes amis t’appellent, ce sont eux</l>
					<l n="64" num="6.10">Qui luttent ! » Je restais couché, comme un goutteux.</l>
					<l n="65" num="6.11">Tandis qu’ils combattaient, beaux d’une âpre furie,</l>
					<l n="66" num="6.12">Je tâtais, en geignant, ma chair endolorie,</l>
					<l n="67" num="6.13">Blessé, rampant. Blessé ? pas même. Estropié !</l>
					<l n="68" num="6.14">De sorte qu’en ce jour si longtemps épié,</l>
					<l n="69" num="6.15">Jour de combat ! le fier serment et l’espérance</l>
					<l n="70" num="6.16">De mourir pour ta vie et pour ta gloire, ô France !</l>
					<l n="71" num="6.17">Et mon père, vieillard délaissé sans amis,</l>
					<l n="72" num="6.18">Et les larmes de celle à qui j’étais promis,</l>
					<l n="73" num="6.19">Et ma jeunesse avec son adresse et sa force,</l>
					<l n="74" num="6.20">Tout cela n’était rien, à cause d’une entorse !</l>
					<l n="75" num="6.21">Tout mon espoir s’était brisé contre un caillou !</l>
				</lg>
				<lg n="7">
					<l n="76" num="7.1">Major, êtes-vous sûr que je ne sois pas fou ?</l>
					<l n="77" num="7.2">J’ai dû le devenir dans ce moment atroce.</l>
				</lg>
				<lg n="8">
					<l n="78" num="8.1">« Je marcherai ! » me dis-je. Alors, fichant la crosse</l>
					<l n="79" num="8.2">De mon fusil dans l’herbe humide, je parvins</l>
					<l n="80" num="8.3">A me dresser. Ainsi qu’un homme entre deux vins,</l>
					<l n="81" num="8.4">J’avançai, par saccade, et, vers la terre moite</l>
					<l n="82" num="8.5">Me courbant, j’avais l’air d’un animal qui boite.</l>
					<l n="83" num="8.6">Mais le bruit du combat, plus proche, et le clairon</l>
					<l n="84" num="8.7">Me donnaient dans le cœur de grands coups d’éperon,</l>
					<l n="85" num="8.8">Et, bien que la douleur dans cette jambe infâme</l>
					<l n="86" num="8.9">Fût telle que je crus mille fois rendre l’âme,</l>
					<l n="87" num="8.10">Je marchais, sans relâche, oubliant de souffrir,</l>
					<l n="88" num="8.11">Et devant d’arriver assez tôt pour mourir !</l>
				</lg>
				<lg n="9">
					<l n="89" num="9.1">J’atteignis une côte. Au-delà, dans la plaine,</l>
					<l n="90" num="9.2">On se battait. Que faire, inerte, hors d’haleine ?</l>
					<l n="91" num="9.3">« Allons, monte, perclus !» Impossible ! trop haut !</l>
					<l n="92" num="9.4">Ah ! j’en pleurais. « Cela se peut, puisqu’il le faut ! »</l>
					<l n="93" num="9.5">Et, couché dans un lit de torrent qui serpente</l>
					<l n="94" num="9.6">Presque à pic et pierreux, tout le long de la pente,</l>
					<l n="95" num="9.7">En m’aidant du genou, de l’ongle et du menton,</l>
					<l n="96" num="9.8">Je grimpai ! j’entendais les feux du peloton !</l>
					<l n="97" num="9.9">Mes mains, mes bras, saignaient sur les épines vertes,</l>
					<l n="98" num="9.10">Je portais mon fusil entre mes dents ouvertes,</l>
					<l n="99" num="9.11">Des pointes déchiraient mon ventre à chaque effort,</l>
					<l n="100" num="9.12">Et ma jambe, pareille à la jambe d’un mort,</l>
					<l n="101" num="9.13">Lamentable fardeau, me tirait en arrière :</l>
					<l n="102" num="9.14">Je grimpais ! mon fusil tomba, dans une ornière,</l>
					<l n="103" num="9.15">Parmi des gazons ras qu’avait roussis l’hiver.</l>
					<l n="104" num="9.16">Mon bon fusil ! j’avais encor mon revolver,</l>
					<l n="105" num="9.17">Et je grimpais toujours ! têtu ! de roche en roche !</l>
					<l n="106" num="9.18">Et quand, les yeux hagards, je vis la cime proche,</l>
					<l n="107" num="9.19">Fou d’espoir, sur l’épine et les cailloux bourrus,</l>
					<l n="108" num="9.20">Lourd, déchiré, sanglant, n’importe ! je courus !</l>
					<l n="109" num="9.21">Et bientôt, m’élevant sur mes deux poings, robuste,</l>
					<l n="110" num="9.22">Joyeux, je dominai le mont de tout mon buste.</l>
				</lg>
				<lg n="10">
					<l n="111" num="10.1">Oh ! quel cri je poussai ! Car je vis, oui, je vis</l>
					<l n="112" num="10.2">Les Français triomphants, les autres poursuivis,</l>
					<l n="113" num="10.3">Et, soulevant mon arme entre mes deux mains jointes,</l>
					<l n="114" num="10.4">Discernant les Prussiens, grâce aux casques à pointes,</l>
					<l n="115" num="10.5">Dans la confusion des corps-à-corps étroits,</l>
					<l n="116" num="10.6">Calme, j’en visai six et j’en vis tomber trois.</l>
					<l n="117" num="10.7">Puis, mourant, je roulai, la tête la première,</l>
					<l part="I" n="118" num="10.8">Dans le combat.</l>
				</lg>
				<lg n="11">
					<l part="F" n="118">Hier, j’ai revu la lumière,</l>
					<l n="119" num="11.1">Stupidement couché dans ce lit d’hôpital.</l>
					<l n="120" num="11.2">— Ah ! major, coupe, taille, ampute, sois brutal,</l>
					<l n="121" num="11.3">Mais sois prompt ! le canon résonne ! et la Victoire,</l>
					<l n="122" num="11.4">Qui redevient française et nous rend notre gloire,</l>
					<l n="123" num="11.5">De Prussiens culbutés va faire un tel abus</l>
					<l n="124" num="11.6">Que, si je tarde encore, « il n’en restera plus ! »</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">3 Décembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>