<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA PRUSSIADE</title>
				<title type="sub_2">OU</title>
				<title type="sub_1">LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
				<title type="sub">1870-71</title>
				<title type="sub">Douze poëmes par un Suisse</title>
				<title type="medium">Édition électronique</title>
				<author key="VCL">
					<name>
						<forename>Henri</forename>
						<surname>VALLON-COLLEY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>846 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VCL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA PRUSSIADE OU LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
						<author>HENRI M. VALLON-COLLEY</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LACHAUD</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VCL1">
				<head type="main">LA MORT DES FIANCES<lb/>OU<lb/>BOMBARDEMENT PAR LES PRUSSIENS D’UN TRAIN DE BLESSES</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Le 31 janvier, un train de 1200 blessés part <lb/>
								pour Lyon. A Byans, pendant que la machine <lb/>
								faisait de l’eau, cinq obus percent le train, qui <lb/>
								est criblé de projectiles.
							</quote>
						</cit>
					</epigraph>
				</opener>
				<p>

				</p>
				<div type="section" n="1">
					<lg n="1">
						<l n="1" num="1.1">Des rives du Niémen jusqu’à celles du Rhin,</l>
						<l n="2" num="1.2">Chaque Allemand savait que jadis à Berlin</l>
						<l n="3" num="1.3">Pendant longtemps flotta le drapeau de la France ;</l>
						<l n="4" num="1.4">Ils voulaient se venger. Quelle fut leur vengeance ?</l>
						<l n="5" num="1.5">Horrible, indigne, hélas ! d’un peuple qui se croit</l>
						<l n="6" num="1.6">Du monde le premier, qui dit avoir le droit</l>
						<l n="7" num="1.7">De s’appeler savant, cultivé, poétique,</l>
						<l n="8" num="1.8">Magnanime, moral, surtout philanthropique.</l>
						<l n="9" num="1.9">Et pourtant, des Teutons lorsqu’ils foulaient le sol,</l>
						<l n="10" num="1.10">Les Français, vrais soldats, ne dirent pas : Le vol</l>
						<l n="11" num="1.11">Est notre ordre du jour ; on ne les vit pas faire</l>
						<l n="12" num="1.12">Aux gens inoffensifs une implacable guerre.</l>
						<l n="13" num="1.13">Non ! Ah ! cruels Germains, redoutez l’avenir,</l>
						<l n="14" num="1.14">Car les Français, un jour, voudront se souvenir !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">I</head>
					<lg n="1">
						<l n="15" num="1.1">C’est au déclin du jour. Au coin d’un cimetière,</l>
						<l n="16" num="1.2">Une femme à genoux récite une prière.</l>
						<l n="17" num="1.3">« Chers enfants, ici-bas je ne vous verrai plus,</l>
						<l n="18" num="1.4">Dit-elle en finissant ; mais j’espère en Jésus,</l>
						<l n="19" num="1.5">J’espère en l’Éternel, en la Vierge Marie !</l>
						<l n="20" num="1.6">Je vous retrouverai dans une autre patrie.»</l>
						<l n="21" num="1.7">Un prêtre, en ce moment, passe dans le chemin,</l>
						<l n="22" num="1.8">S’approche de la femme et, lui prenant la main :</l>
						<l n="23" num="1.9">« Pauvre mère, dit-il, votre deuil est immense ;</l>
						<l n="24" num="1.10">Mais, puisque vous mettez en Dieu votre espérance,</l>
						<l n="25" num="1.11">Il vous soulagera. Je suis son serviteur.</l>
						<l n="26" num="1.12">Contez-moi vos chagrins, ouvrez-moi votre cœur.</l>
						<l n="27" num="1.13">— Bon monsieur le curé, vous êtes charitable !</l>
						<l n="28" num="1.14">Ce que vous entendrez sera bien lamentable.</l>
						<l n="29" num="1.15">A quelques pas d’ici se trouve ma maison :</l>
						<l n="30" num="1.16">Allons-y : la nuit vient, et froide est la saison. »</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">II</head>
					<lg n="1">
						<l n="31" num="1.1">Près d’un fourneau de fer, sur un vieux banc de chêne,</l>
						<l n="32" num="1.2">Le prêtre s’est assis. Une lampe avec peine</l>
						<l n="33" num="1.3">Entoure un crucifix d’une faible clarté ;</l>
						<l n="34" num="1.4">Tous les autres objets sont dans l’obscurité.</l>
						<l n="35" num="1.5">La femme a commencé de raconter l’histoire</l>
						<l n="36" num="1.6">De ses derniers malheurs. « Oui, vous pouvez me croire,</l>
						<l n="37" num="1.7">Mon père, c’est ici qu’ils se dirent adieu.</l>
						<l n="38" num="1.8">Ah ! c’était un garçon comme l’on en voit peu !</l>
						<l n="39" num="1.9">Marguerite, ma fille, était sa bien-aimée ;</l>
						<l n="40" num="1.10">Il voulait l’épouser au sortir de l’armée.</l>
						<l n="41" num="1.11">Soudain la guerre éclate ; il part, le cœur serré.</l>
						<l n="42" num="1.12">Depuis ce moment-là nous avons bien pleuré.</l>
						<l n="43" num="1.13">Après un mois de lutte, hélas ! peu favorable,</l>
						<l n="44" num="1.14">Arrive de Sedan le jour épouvantable.</l>
						<l n="45" num="1.15">Ça fait frémir ! Enfin, mon fils des Allemands</l>
						<l n="46" num="1.16">Se trouve prisonnier, mais ne l’est pas longtemps.</l>
						<l n="47" num="1.17">Avec quelques amis, sur les bords de la Loire</l>
						<l n="48" num="1.18">Il va combattre encor et se couvre de gloire.</l>
						<l n="49" num="1.19">Il échappe aux boulets. Mais le facteur, un soir,</l>
						<l n="50" num="1.20">Sonne à ma fille un pli portant un cachet noir.</l>
						<l n="51" num="1.21">Elle l’ouvre en tremblant, lit, puis se précipite</l>
						<l n="52" num="1.22">Dans me bras en disant : « O mère, partons vite,</l>
						<l n="53" num="1.23">« Ne perdons pas de temps. Mon brave fiancé</l>
						<l n="54" num="1.24">« Est près de Montbéliard, grièvement blessé. »</l>
						<l n="55" num="1.25">Ah ! bien coupables sont les auteurs d’une guerre</l>
						<l n="56" num="1.26">Avez-vous jamais vu l’hôpital militaire,</l>
						<l n="57" num="1.27">Bon monsieur le curé ? Là, les pauvres soldats</l>
						<l n="58" num="1.28">Dans d’atroces douleurs attendent le trépas.</l>
						<l n="59" num="1.29">Ceux qu’épargne la mort retournent au village,</l>
						<l n="60" num="1.30">C’est vrai, mais peuvent-ils reprendre leur ouvrage ?</l>
						<l n="61" num="1.31">Lorsqu’on est impotent l’on ne peut pas bêcher ;</l>
						<l n="62" num="1.32">Avec un déni-bras comment pouvoir faucher ?</l>
						<l n="63" num="1.33">Ma fille trouve donc, assis sur de la paille,</l>
						<l n="64" num="1.34">Son valeureux promis. Un éclat de mitraille</l>
						<l n="65" num="1.35">L’a privé des deux mains. Touchant est le revoir.</l>
						<l n="66" num="1.36">Mais pour moi, cependant, il est pénible à voir.</l>
						<l n="67" num="1.37">Je pense et ne dis rien. Elle pleure, soupire.</l>
						<l n="68" num="1.38">Lui, pour la consoler, s’efforce de sourire.</l>
						<l n="69" num="1.39">Tout à coup un docteur s’arrête devant nous :</l>
						<l n="70" num="1.40">« Ah ! dit-il au blessé, cous êtes aimé, vous !</l>
						<l n="71" num="1.41">« Ici plus d’un guerrier a quitté cette vie</l>
						<l n="72" num="1.42">« Sans avoir près de lui quelque personne amie,</l>
						<l n="73" num="1.43">« Sans avoir entendu, sortant du fond du cœur,</l>
						<l n="74" num="1.44">« Au moment de la mort, un mot consolateur.»</l>
						<l n="75" num="1.45">S’adressant à ma fille : « Armez-vous de courage.</l>
						<l n="76" num="1.46">« Ce soir vous reprendrez le chemin du village.</l>
						<l n="77" num="1.47">« S’il a deux mains de moins vous avez son amour :</l>
						<l n="78" num="1.48">« Cela doit vous suffire. Adieu ! Heureux retour ! »</l>
						<l n="79" num="1.49">Au milieu de la nuit, le train, un train immense,</l>
						<l n="80" num="1.50">Plein de soldats blessés, s’arrête. Alors commence</l>
						<l n="81" num="1.51">Un massacre inouï. Du coté des wagons</l>
						<l n="82" num="1.52">Les artilleurs prussiens ont braqué leurs canons.</l>
						<l n="83" num="1.53">Parmi ces pauvres gens les projectiles tombent.</l>
						<l n="84" num="1.54">En voulant se sauver, plusieurs d’entre eux succombent.</l>
						<l n="85" num="1.55">Il fait noir. D’un obus la sinistre lueur</l>
						<l n="86" num="1.56">Éclaire par instants cette scène d’horreur.</l>
						<l n="87" num="1.57">Marguerite, d’un saut, hors du wagon s’élance ;</l>
						<l n="88" num="1.58">Elle veut m’entrainer, mais je perds connaissance.</l>
						<l n="89" num="1.59">Le lendemain matin, sur la neige, et glacés,</l>
						<l n="90" num="1.60">Je retrouve les corps des pauvres fiancés.</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="2">
						<l n="91" num="2.1">J’ai fini. N’est-ce pas, c’est une triste histoire ?</l>
						<l n="92" num="2.2">— Lorsqu’on est malheureux, il est bien doux de croire, »</l>
						<l n="93" num="2.3">Répond l’excellent prêtre. Et, pendant qu’au dehors</l>
						<l n="94" num="2.4">La neige tombe, il dit la prière des Morts.</l>
					</lg>
				</div>
			</div></body></text></TEI>