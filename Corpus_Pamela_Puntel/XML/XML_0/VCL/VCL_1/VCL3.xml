<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA PRUSSIADE</title>
				<title type="sub_2">OU</title>
				<title type="sub_1">LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
				<title type="sub">1870-71</title>
				<title type="sub">Douze poëmes par un Suisse</title>
				<title type="medium">Édition électronique</title>
				<author key="VCL">
					<name>
						<forename>Henri</forename>
						<surname>VALLON-COLLEY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>846 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VCL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA PRUSSIADE OU LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
						<author>HENRI M. VALLON-COLLEY</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LACHAUD</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VCL3">
				<head type="main">LE COSAQUE DU RHIN À CHATEAUDUN</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Far dearer the grave or the prison,<lb/>
								Illumed by one patriot name,<lb/>
								Than the trophies of all, who have risen<lb/>
								On liberty’s ruins to fame.
							</quote>
							<bibl>
								<name>MOORE</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Des sauvages, les Huns, portèrent autrefois</l>
						<l n="2" num="1.2">L’épouvante et la mort sur le sol des Gaulois.</l>
						<l n="3" num="1.3">Avec raison aussi, l’inexorable histoire</l>
						<l n="4" num="1.4">De leur barbare chef a flétri la mémoire.</l>
						<l n="5" num="1.5">Au dix-neuvième siècle, après mille ans et plus,</l>
						<l n="6" num="1.6">Un moderne Attila, se parant de vertus</l>
						<l n="7" num="1.7">Qu’il ne possède pas, au nom de la patrie</l>
						<l n="8" num="1.8">Et de la liberté, se rue avec furie,</l>
						<l n="9" num="1.9">Sans honte, sans remords, sur un peuple épuisé.</l>
						<l n="10" num="1.10">Oui, Guillaume premier, ce roi civilisé,</l>
						<l n="11" num="1.11">Bat des mains en voyant des villes enflammées,</l>
						<l n="12" num="1.12">Applaudit aux méfaits commis par ses armées,</l>
						<l n="13" num="1.13">Donne à ses généraux des croix, des dignités,</l>
						<l n="14" num="1.14">S’ils se sont distinguées par des atrocités.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="15" num="1.1">Triste descend la nuit. Des efforts héroïques,</l>
						<l n="16" num="1.2">Des élans de lion, trente charges épiques,</l>
						<l n="17" num="1.3">N’ont pu sauver, hélas ! la petite cité</l>
						<l n="18" num="1.4">Dont le nom brillera dans la postérité :</l>
						<l n="19" num="1.5">Châteaudun est conquis ! Un incendie immense</l>
						<l n="20" num="1.6">D’un sanguinaire roi proclame la démence.</l>
						<l n="21" num="1.7">Dans la rue, où la mort se montre à chaque pas,</l>
						<l n="22" num="1.8">Les poutres des maisons tombent avec fracas.</l>
						<l n="23" num="1.9">Ce qu’épargne le feu par la horde sauvage</l>
						<l n="24" num="1.10">Est souillé, renversé, détruit, mis au pillage.</l>
						<l n="25" num="1.11">Le commandant en chef, drapé dans son manteau,</l>
						<l n="26" num="1.12">Fume, et de temps en temps murmure : « C’est bien beau ! »</l>
						<l n="27" num="1.13">Triste descend la nuit ! Au porche d’une église</l>
						<l n="28" num="1.14">Un drapeau français flotte ; il porte pour devise :</l>
						<l n="29" num="1.15">« Indépendance ou mort. » Sur la maison de Dieu</l>
						<l n="30" num="1.16">Aussitôt cent soldats en blasphémant font feu,</l>
						<l n="31" num="1.17">Déchirent l’oriflamme, et, ne rêvant que crimes,</l>
						<l n="32" num="1.18">Dans l’édifice saint vont chercher des victimes.</l>
						<l n="33" num="1.19">Mais là règne la paix : seuls quelques défenseurs</l>
						<l n="34" num="1.20">Du pauvre Châteaudun sont soignés par des sœurs.</l>
						<l n="35" num="1.21">Près de l’autel, couché sur une froide dalle,</l>
						<l n="36" num="1.22">L’un de ces malheureux, sanglant, mutilé, râle ;</l>
						<l n="37" num="1.23">A genoux devant lui, le regard vers le ciel,</l>
						<l n="38" num="1.24">Sa bien-aimée attend le moment solennel.</l>
						<l n="39" num="1.25">Un Cosaque du Rhin, à la mine farouche,</l>
						<l n="40" num="1.26">S’approche du blessé, de sa botte le touche,</l>
						<l n="41" num="1.27">Disant : « Par Belzébuth ! c’est le chef infernal</l>
						<l n="42" num="1.28">Qui dans nos rangs a fait un si terrible mal.</l>
						<l n="43" num="1.29">J’ai cru que je l’avais, par un coup formidable</l>
						<l n="44" num="1.30">De ma longue rapière, envoyé chez le diable.</l>
						<l n="45" num="1.31">Le chien respire encor, mais cette fois je veux,</l>
						<l n="46" num="1.32">En bon soldat royal, le tuer un peu mieux. »</l>
						<l n="47" num="1.33">Et le Teuton, avec sa botte éperonnée,</l>
						<l n="48" num="1.34">Éventre le blessé pour finir la journée.</l>
						<l n="49" num="1.35">Alors tous ces pillards, qui n’ont plus rien d’humain,</l>
						<l n="50" num="1.36">Souillent le lieu sacré par un affreux festin.</l>
						<l n="51" num="1.37">En étal de boucher la nef est transformée ;</l>
						<l n="52" num="1.38">La chaire est la tribune où l’on boit à l’armée ;</l>
						<l n="53" num="1.39">L’autel où, devant Dieu, s’agenouillait, le soir,</l>
						<l n="54" num="1.40">Le pécheur pénitent, n’est plus qu’un abattoir.</l>
						<l n="55" num="1.41">Pendant toute la nuit se prolonge l’orgie,</l>
						<l n="56" num="1.42">Et lorsque l’aube éteint la dernière bougie,</l>
						<l n="57" num="1.43">Notre sanglant héros, le Cosaque du Rhin,</l>
						<l n="58" num="1.44">Gorgé de viande crue, et de champagne plein,</l>
						<l n="59" num="1.45">S’endort paisiblement devant la sacristie</l>
						<l n="60" num="1.46">Par ses hideux amis en dortoir convertie.</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="2">
						<l n="61" num="2.1">Parmi les Allemands qui prirent Châteaudun</l>
						<l n="62" num="2.2">Combien de vrais soldats se trouvait-il ? Pas un.</l>
					</lg>
				</div>
			</div></body></text></TEI>