<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA PRUSSIADE</title>
				<title type="sub_2">OU</title>
				<title type="sub_1">LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
				<title type="sub">1870-71</title>
				<title type="sub">Douze poëmes par un Suisse</title>
				<title type="medium">Édition électronique</title>
				<author key="VCL">
					<name>
						<forename>Henri</forename>
						<surname>VALLON-COLLEY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>846 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VCL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA PRUSSIADE OU LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
						<author>HENRI M. VALLON-COLLEY</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LACHAUD</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VCL8">
				<head type="main">PETITE VENGEANCE PRUSSIENNE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Sous prétexte que les trains qui transportent<lb/>
								des troupes allemandes déraillent à chaque instant<lb/>
								(ces prétendus déraillements sont attribués à<lb/>
								des Français), l’autorité militaire prussienne<lb/> 
								a cru devoir forcer quelques notables de la ville<lb/>
								de N… de prendre place près du chauffeur.<lb/>
								Dans le nombre se trouve un octogénaire. Sa<lb/>
								petite-fille l’accompagne.
							</quote>
						</cit>
					</epigraph>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Dans la salle à manger d’une maison princière,</l>
						<l n="2" num="1.2">Des officiers prussiens, à la tournure altière,</l>
						<l n="3" num="1.3">Au regard fanfaron, impertinent, hautain,</l>
						<l n="4" num="1.4">D’un hymne belliqueux fredonnent le refrain :</l>
						<l n="5" num="1.5">« C’est assez chantonné. Messieurs, veuillez vous taire</l>
						<l n="6" num="1.6">Dit l’un en remplissant de vieux bordeaux son verre.</l>
						<l n="7" num="1.7">Oublions les lauriers, buvons à nos amours,</l>
						<l n="8" num="1.8">Et demandons aux dieux qu’ils fleurissent toujours.</l>
						<l n="9" num="1.9">Sous ce fortuné toit est l’objet de ma flamme,</l>
						<l n="10" num="1.10">Celle qui deviendra mon amante, ma femme ;</l>
						<l n="11" num="1.11">Elle est belle, elle est jeune, elle est riche surtout :</l>
						<l n="12" num="1.12">Or je confesse avoir pour les écus du goût.</l>
						<l n="13" num="1.13">Le maître de ces lieux, son vénérable père,</l>
						<l n="14" num="1.14">Comme tout vrai Français, n’a pas de caractère ;</l>
						<l n="15" num="1.15">Il sera très-heureux de former un lien</l>
						<l n="16" num="1.16">Entre sa fille et moi, bien que je sois Prussien.</l>
						<l n="17" num="1.17">Lorsque nous aurons pris la cité de la Seine,</l>
						<l n="18" num="1.18">Et ‘heure de sa chute avance, elle est prochaine.</l>
						<l n="19" num="1.19">Si vous êtes vivants, mes amis, de bon cœur</l>
						<l n="20" num="1.20">Je vous inviterai pour fêter mon bonheur. »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="21" num="1.1">Le cupide Allemand, avec beaucoup d’emphase,</l>
						<l n="22" num="1.2">Vient d’arriver au bout d’une pompeuse phrase.</l>
						<l n="23" num="1.3">Alors un beau vieillard, à l’œil profond et bleu,</l>
						<l n="24" num="1.4">Où brille, malgré l’âge, un juvénile feu,</l>
						<l n="25" num="1.5">Quitte son vieux fauteuil, et, d’une voix sévère,</l>
						<l n="26" num="1.6">Dit : « Monsieur l’officier, c’est par trop téméraire,</l>
						<l n="27" num="1.7">Par trop présomptueux, de vous produire ici,</l>
						<l n="28" num="1.8">Dans ma chambre à coucher, et de parler ainsi.</l>
						<l n="29" num="1.9">Vous désirez avoir pour épouse ma fille,</l>
						<l n="30" num="1.10">Devenir mon beau-fils, membre de ma famille,</l>
						<l n="31" num="1.11">Cela dans un moment où vos brutaux amis</l>
						<l n="32" num="1.12">Ravagent sans pitié mon malheureux pays !!!</l>
						<l n="33" num="1.13">Entre ma fille et vous se trouve un vaste abîme.</l>
						<l n="34" num="1.14">En comblant vos désirs, je commettrais un crime,</l>
						<l n="35" num="1.15">Je serais méprisé, entré partout au doigt ;</l>
						<l n="36" num="1.16">De m’appeler Français je n’aurais pas le droit.</l>
						<l n="37" num="1.17">Ici le caractère est quelque peu mobile,</l>
						<l n="38" num="1.18">J’en conviens franchement, mais l’âme n’est pas vile.</l>
						<l n="39" num="1.19">Grâce à Dieu, le Français, quand il s’agit d’honneur,</l>
						<l n="40" num="1.20">Sait être sérieux, vous le voyez, vainqueur. »</l>
						<l n="41" num="1.21">Le vaillant officier répond par un sourire</l>
						<l n="42" num="1.22">Ironique et méchant, s’incline et se retire,</l>
						<l n="43" num="1.23">En se promettant bien de punir, sans retard,</l>
						<l n="44" num="1.24">Originalement, l’intraitable vieillard.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="45" num="1.1">Sur des bourgeois français, rassemblés dans la rue</l>
						<l n="46" num="1.2">Principale d’un bourg, un escadron se rue.</l>
						<l n="47" num="1.3">« Retirez-vous, manants ! faites place, sinon</l>
						<l n="48" num="1.4">Vous serez écharpés,» rugit un gros Teuton.</l>
						<l n="49" num="1.5">L’inoffensive foule en murmurant se range,</l>
						<l n="50" num="1.6">L’escadron germain passe. Alors la scène change :</l>
						<l n="51" num="1.7">Deux caporaux saxons, plusieurs sergents badois,</l>
						<l n="52" num="1.8">Cent soldats de Guillaume, un major bavarois,</l>
						<l n="53" num="1.9">Escortent, en jurant comme à Berlin l’on jure,</l>
						<l n="54" num="1.10">Un homme aux cheveux gris, près duquel, calme, pure,</l>
						<l n="55" num="1.11">Une idéale enfant marche les yeux baissés.</l>
						<l n="56" num="1.12">C’est un père et sa fille ; ils ont été chassés</l>
						<l n="57" num="1.13">Impitoyablement, ce jour-là, de bonne heure,</l>
						<l n="58" num="1.14">Par quelques estafiers, de leur propre demeure.</l>
						<l n="59" num="1.15">Quel crime ont-ils commis pour être ainsi traités ?</l>
						<l n="60" num="1.16">Aucun. Mais, quand il peut faire des lâchetés,</l>
						<l n="61" num="1.17">Mettre une ville à sac, fondre sur une proie,</l>
						<l n="62" num="1.18">Le soldat prussien est au comble de la joie.</l>
						<l n="63" num="1.19">Le village franchi, fantassins, cavaliers,</l>
						<l n="64" num="1.20">Près du chemin de fer font halte. Aux prisonniers</l>
						<l n="65" num="1.21">Un soudard dit alors : « Sur la locomotive</l>
						<l n="66" num="1.22">Vous devez prendre place, afin que s’il arrive —</l>
						<l n="67" num="1.23">Cela est déjà vu — quelque petit malheur,</l>
						<l n="68" num="1.24">D’être parmi les morts vous ayez le bonheur.»</l>
						<l n="69" num="1.25">Les victimes, jugeant que ce serait folie</l>
						<l n="70" num="1.26">De ne pas obéir, boivent jusqu’à la lie</l>
						<l n="71" num="1.27">Le calice infernal ; et, lorsque vers la nuit</l>
						<l n="72" num="1.28">Retentit le sifflet, l’officier éconduit,</l>
						<l n="73" num="1.29">S’approchant du vieillard, murmure : « C’est dommage,</l>
						<l n="74" num="1.30">Mais vous l’avez voulu. Donc, Monsieur, bon voyage ! »</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="2">
						<l n="75" num="2.1">Ces civilisateurs, ces Germains tant vantés,</l>
						<l n="76" num="2.2">Hélas ! sont bien petits s’ils sont désappointés.</l>
					</lg>
				</div>
			</div></body></text></TEI>