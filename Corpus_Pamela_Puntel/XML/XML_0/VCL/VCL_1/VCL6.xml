<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA PRUSSIADE</title>
				<title type="sub_2">OU</title>
				<title type="sub_1">LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
				<title type="sub">1870-71</title>
				<title type="sub">Douze poëmes par un Suisse</title>
				<title type="medium">Édition électronique</title>
				<author key="VCL">
					<name>
						<forename>Henri</forename>
						<surname>VALLON-COLLEY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>846 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VCL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA PRUSSIADE OU LES HAUTS FAITS DE GUILLAUME Ier ET DE SES ALLIES EN FRANCE 1870-1871</title>
						<author>HENRI M. VALLON-COLLEY</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LACHAUD</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VCL6">
				<head type="main">LES VANDALES MODERNES A ORLÉANS</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								A Orléans et dans les environs, les Bavarois<lb/>
								ont fait une ample moisson de pendules, de<lb/>
								montres, de bijoux et de robes. Les officiers<lb/>
								donnaient l’exemple.
							</quote>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Le monarque prussiens, ce sinistre vainqueur,</l>
					<l n="2" num="1.2">Qui s’est fait octroyer un manteau d’empereur,</l>
					<l n="3" num="1.3">A souvent répété dans ses accès mystiques :</l>
					<l n="4" num="1.4">« Un jour j’accomplirai de grands faits historiques. »</l>
					<l n="5" num="1.5">Ces faits, on les connaît : ils ne sont tant vantés</l>
					<l n="6" num="1.6">Que par quelques sabreurs arrogants, exaltés ;</l>
					<l n="7" num="1.7">Mais pour toutes les sœurs, mais pour toutes les mères</l>
					<l n="8" num="1.8">Qui pleurent en silence et des fils et des frères</l>
					<l n="9" num="1.9">Qu’elles ne verront plus, loin d’être glorieux,</l>
					<l n="10" num="1.10">Guillaume, tes exploits sont cruels, odieux.</l>
				</lg>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="11" num="1.1">D’une riche cité le glas funèbre sonne,</l>
						<l n="12" num="1.2">Aux portes d’Orléans le canon prussien tonne ;</l>
						<l n="13" num="1.3">La nuit est avancée, et de tous les côtés</l>
						<l n="14" num="1.4">Les femmes, les enfants, courent épouvantés.</l>
						<l n="15" num="1.5">Ah ! c’est que des soldats qui composent l’armée</l>
						<l n="16" num="1.6">Du boucher couronné sombre est la renommée :</l>
						<l n="17" num="1.7">De l’habitant des champs ils volent les troupeaux,</l>
						<l n="18" num="1.8">Et sans nécessité bombardent les hameaux ;</l>
						<l n="19" num="1.9">A la ville, au vallon, partout sur leur passage</l>
						<l n="20" num="1.10">L’on voit du feu, du sang, des traces de carnage ;</l>
						<l n="21" num="1.11">Dans ses lugubres plis, leur drapeau blanc et noir,</l>
						<l n="22" num="1.12">Symbole de la mort, porte le désespoir.</l>
						<l n="23" num="1.13">Hurrah ! Hurrah ! ce cri veut dire qu’à la Loire</l>
						<l n="24" num="1.14">Les coursiers allemands vont enfin pouvoir boire.</l>
						<l n="25" num="1.15">Braves, comme toujours, par des masses chargés,</l>
						<l n="26" num="1.16">Les Français sont vaincus, mais non découragés.</l>
						<l n="27" num="1.17">Alors l’infernal chef de l’infernale bande,</l>
						<l n="28" num="1.18">Le général de Thann permet, même commande,</l>
						<l n="29" num="1.19">Que le pillage ait lieu. Trente mille voleurs,</l>
						<l n="30" num="1.20">Gradés et non gradés, commettent des horreurs</l>
						<l n="31" num="1.21">Dignes du grand Mandrin et du fameux Cartouche,</l>
						<l n="32" num="1.22">Et cela froidement, le sourire à la bouche.</l>
						<l n="33" num="1.23">Bientôt les havre-sacs regorgent de bijoux,</l>
						<l n="34" num="1.24">D’argent, de crucifix et même de joujoux ;</l>
						<l n="35" num="1.25">Dans les fourgons royaux s’entassent pêle-mêle</l>
						<l n="36" num="1.26">Le somptueux velours, la soie et la dentelle.</l>
						<l n="37" num="1.27">A la cave l’on hurle, et le vin coule à flots ;</l>
						<l n="38" num="1.28">Au premier, l’on plaisante en faisant des ballots ;</l>
						<l n="39" num="1.29">Au second, un vieillard blanchi par les années</l>
						<l n="40" num="1.30">Cherche à sauver l’honneur de deux infortunées ;</l>
						<l n="41" num="1.31">Plus haut, non loin du toit, dans un obscur réduit,</l>
						<l n="42" num="1.32">C’est un vieux mobilier qu’avec rage on détruit ;</l>
						<l n="43" num="1.33">La vieille qui longtemps en fut propriétaire,</l>
						<l n="44" num="1.34">Assise dans un coin, sanglote et laisse faire.</l>
						<l n="45" num="1.35">Siècle civilisé, qu’en dis-tu ? C’est affreux.</l>
						<l n="46" num="1.36">Voici pourtant encor quelque chose de mieux :</l>
						<l n="47" num="1.37">Le vent est glacial, le jour se lève pâle,</l>
						<l n="48" num="1.38">Cinquante grenadiers de la garde royale</l>
						<l n="49" num="1.39">Vont dans les hôpitaux, en chassent les blessés,</l>
						<l n="50" num="1.40">Qui sont par des Germains à l’instant remplacés.</l>
						<l n="51" num="1.41">Les Français, eux, sont mis dans un local humide,</l>
						<l n="52" num="1.42">Où l’air est corrompu, nauséabond, fétide.</l>
						<l n="53" num="1.43">Sans lumière, sans feu, privés de tout secours,</l>
						<l n="54" num="1.44">Sans boire, sans manger, ils restent là deux jours.</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="2">
						<l n="55" num="2.1">Comme le plus petit, malgré ton diadème,</l>
						<l n="56" num="2.2">O Guillaume le Grand, devant le roi suprême,</l>
						<l n="57" num="2.3">Tu paraitras un jour. Alors, peut-être, aux cieux</l>
						<l n="58" num="2.4">Tu ne seras trouvé ni grand, ni glorieux.</l>
					</lg>
				</div>
			</div></body></text></TEI>