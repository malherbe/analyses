<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DU SIÈGE DE PARIS</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="SFL">
					<name>
						<forename>Théobald</forename>
						<surname>SAINT-FÉLIX</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>161 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">SFL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>CHANTS DU SIÈGE DE PARIS. 1870-1871</title>
						<author>THÉOBALD SAINT-FÉLIX</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Imprimerie Ch. Schiller</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="SFL3">
				<head type="main">LE MOBILE BRETON</head>
				<head type="form">CHANSON</head>
				<opener>
					<salute>AUX MOBILES DE LA BRETAGNE</salute>
				</opener>
				<div type="section" n="1">
					<head type="number">1</head>
					<lg n="1">
						<l n="1" num="1.1">Je suis Jeannic, garde mobile,</l>
						<l n="2" num="1.2">Petit moblot du Morbihan,</l>
						<l n="3" num="1.3">Venu dans votre grande ville,</l>
						<l n="4" num="1.4">Moitié pleurant, moitié riant.</l>
						<l n="5" num="1.5">Je pleurais en quittant mon père,</l>
						<l n="6" num="1.6">Ma sœur et nos champs de genêts.</l>
						<l n="7" num="1.7">Tristement j’embrassais ma mère,</l>
						<l n="8" num="1.8">Mais en chemin je souriais ;</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Car, à la voix de la patrie,</l>
						<l n="10" num="2.2">Le brave breton bretonnant</l>
						<l n="11" num="2.3">Donne avec joie à la France chérie</l>
						<l n="12" num="2.4">Son bras, sa vie, et son cœur et son sang.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">2</head>
					<lg n="1">
						<l n="13" num="1.1">Un jour, on dit sur la falaise</l>
						<l n="14" num="1.2">Que Guillaume, le roi prussien,</l>
						<l n="15" num="1.3">Voulait prendre tout à son aise</l>
						<l n="16" num="1.4">Votre beau pays parisien.</l>
						<l n="17" num="1.5">Et soudain, de toutes nos âmes,</l>
						<l n="18" num="1.6">Un cri de douleur s’éleva…</l>
						<l n="19" num="1.7">Tout fut debout : hommes et femmes :</l>
						<l n="20" num="1.8">La Bretagne se souleva !…</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1">Car, à la voix de la patrie,</l>
						<l n="22" num="2.2">Le brave breton bretonnant</l>
						<l n="23" num="2.3">Donne avec joie à la France chérie</l>
						<l n="24" num="2.4">Son bras, sa vie, et son cœur et son sang.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">3</head>
					<lg n="1">
						<l n="25" num="1.1">Notre-Dame de délivrance,</l>
						<l n="26" num="1.2">Mère d’amour et de bonté,</l>
						<l n="27" num="1.3">Va rendre à notre belle France</l>
						<l n="28" num="1.4">Et sa gloire et sa liberté…</l>
						<l n="29" num="1.5">Puis, du Morbihan le mobile,</l>
						<l n="30" num="1.6">Le cœur plein de joie et d’espoir,</l>
						<l n="31" num="1.7">Quittera votre grande ville</l>
						<l n="32" num="1.8">Après avoir fait son devoir…</l>
					</lg>
					<lg n="2">
						<l n="33" num="2.1">Car, à la voix de la patrie,</l>
						<l n="34" num="2.2">Le brave breton bretonnant</l>
						<l n="35" num="2.3">Donne avec joie à la France chérie</l>
						<l n="36" num="2.4">Son bras, sa vie, et son cœur et son sang.</l>
					</lg>
				</div>
				<closer>
					<note type="footnote" id="">N.B. — La musique, chant et piano, composé par M. F. Bossière, est en vente chez M. Sylvain Saint-Étienne, éditeur, n° 31 bis, Faubourg-Montmartre, à Paris.</note>
				</closer>
			</div></body></text></TEI>