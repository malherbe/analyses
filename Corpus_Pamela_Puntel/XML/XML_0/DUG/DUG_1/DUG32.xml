<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG32">
				<head type="main">AMOUR !</head>
				<lg n="1">
					<l n="1" num="1.1">Quand règne au fond d'un cœur sincère</l>
					<l n="2" num="1.2">L'amour, ce sentiment divin,</l>
					<l n="3" num="1.3">Exil, maladie ou misère,</l>
					<l n="4" num="1.4">Pour l'en arracher tout est vain ;</l>
					<l n="5" num="1.5">Plus le mur croule et plus le lierre</l>
					<l n="6" num="1.6">Fidelle au débris qu'il enserre</l>
					<l n="7" num="1.7">S'y colle avec force et le serre</l>
					<l n="8" num="1.8">Dans un embrassement sans fin !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Balzac a peint dans un beau livre</l>
					<l n="10" num="2.2">Cet amour immense et fatal</l>
					<l n="11" num="2.3">Qui peut obstinément survivre</l>
					<l n="12" num="2.4">Aux vaines étreintes du mal,</l>
					<l n="13" num="2.5">Et, quand on la croit abattue</l>
					<l n="14" num="2.6">Par ce qui blesse et ce qui tue,</l>
					<l n="15" num="2.7">Replace la chère statue</l>
					<l n="16" num="2.8">Sur son éternel piédestal !…</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">— Armand, oubliez-moi ! la tombe</l>
					<l n="18" num="3.2">M'entrouvre ses flancs ténébreux !</l>
					<l n="19" num="3.3">Le cilice sur moi retombe</l>
					<l n="20" num="3.4">Et j'ai coupé mes longs cheveux !…</l>
					<l n="21" num="3.5">Je ne suis plus jeune ni belle,</l>
					<l n="22" num="3.6">L'air manque à ma poitrine frêle,</l>
					<l n="23" num="3.7">C'est la mort seule que j'appelle !…</l>
					<l n="24" num="3.8">Armand répondit : — Je te veux !…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Oui, je te veux flétrie et pâle</l>
					<l n="26" num="4.2">Et mourante, car mon amour</l>
					<l n="27" num="4.3">Pour tes grands yeux cerclés d'opale</l>
					<l n="28" num="4.4">Est pur comme le premier jour…</l>
					<l n="29" num="4.5">Car risquant révolte et blasphème,</l>
					<l n="30" num="4.6">S'il fallait ce défi suprême,</l>
					<l n="31" num="4.7">Je t'arracherais à Dieu même</l>
					<l n="32" num="4.8">Comme la colombe au vautour ! —</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Ainsi, ma France endolorie,</l>
					<l n="34" num="5.2">Corps saignant et cœur ulcéré,</l>
					<l n="35" num="5.3">Étreignant sur ta chair meurtrie</l>
					<l n="36" num="5.4">Les plis du drapeau déchiré,</l>
					<l n="37" num="5.5">Je t'aime encore davantage !</l>
					<l n="38" num="5.6">Avec frénésie ! avec rage !</l>
					<l n="39" num="5.7">Et le lien qui nous engage</l>
					<l n="40" num="5.8">Par tes malheurs devient sacré !…</l>
				</lg>
			</div></body></text></TEI>