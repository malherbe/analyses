<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG17">
				<head type="main">L'ODEUR ALLEMANDE</head>
				<lg n="1">
					<l n="1" num="1.1">Pillant, brûlant selon l'usage,</l>
					<l n="2" num="1.2">Les Allemands ont passé là,</l>
					<l n="3" num="1.3">Et jamais les Huns d'Attila</l>
					<l n="4" num="1.4">N'ont mieux fait sentir leur passage</l>
					<l n="5" num="1.5">Que ces Badois, Mecklembourgeois,</l>
					<l n="6" num="1.6">Hessois, tous noms rimant en ois ;</l>
					<l n="7" num="1.7"><space unit="char" quantity="8"/>Ceux qu'on appelle</l>
					<l n="8" num="1.8">Silésiens et Poméraniens,</l>
					<l n="9" num="1.9">D'autres encor rimant à chiens ;</l>
					<l n="10" num="1.10">En un mot toute la séquelle</l>
					<l n="11" num="1.11"><space unit="char" quantity="10"/>Par laquelle</l>
					<l n="12" num="1.12">La terre et l'air sont corrompus…</l>
					<l n="13" num="1.13">Qu'on fasse rougir une pelle</l>
					<l n="14" num="1.14">Pour brûler du sucre dessus !…</l>
				</lg>
				<lg n="2">
					<l n="15" num="2.1">C'est que les peuplades vassales</l>
					<l n="16" num="2.2">Que Guillaume attache à ses pas</l>
					<l n="17" num="2.3">D'eau propre ne se servent pas…</l>
					<l n="18" num="2.4">Elles sont cruelles mais sales…</l>
					<l n="19" num="2.5">Et la punaise entre les draps,</l>
					<l n="20" num="2.6">La sueur ignoble des bras,</l>
					<l n="21" num="2.7"><space unit="char" quantity="8"/>Dalle à vaisselle,</l>
					<l n="22" num="2.8">Gueule d'égout, tas de fumier</l>
					<l n="23" num="2.9">Et vieux linges de l'infirmier</l>
					<l n="24" num="2.10">N'ont pas une odeur comme celle</l>
					<l n="25" num="2.11"><space unit="char" quantity="10"/>Que recèle</l>
					<l n="26" num="2.12">Cette engeance aux crânes obtus…</l>
					<l n="27" num="2.13">Qu'on fasse rougir une pelle</l>
					<l n="28" num="2.14">Pour brûler du sucre dessus !…</l>
				</lg>
				<lg n="3">
					<l n="29" num="3.1">C'est une odeur particulière</l>
					<l n="30" num="3.2">A cette espèce de bétail :</l>
					<l n="31" num="3.3">Je n'entreprends point le travail</l>
					<l n="32" num="3.4">De la dépeindre tout entière ;</l>
					<l n="33" num="3.5">Villon et Marot, ces Gaulois,</l>
					<l n="34" num="3.6">Et maître Rabelais, tous trois,</l>
					<l n="35" num="3.7"><space unit="char" quantity="8"/>Dans leur cervelle</l>
					<l n="36" num="3.8">Ne trouveraient pas de dicton</l>
					<l n="37" num="3.9">Pour définir l'affreux poison ;</l>
					<l n="38" num="3.10">Gargantua même et sa fidelle</l>
					<l n="39" num="3.11"><space unit="char" quantity="10"/>Gargamelle</l>
					<l n="40" num="3.12">N'auraient pas de mots assez crûs…</l>
					<l n="41" num="3.13">Qu'on fasse rougir une pelle</l>
					<l n="42" num="3.14">Pour brûler du sucre dessus !…</l>
				</lg>
				<lg n="4">
					<l n="43" num="4.1">Mais ce qui dans l'ignoble guerre</l>
					<l n="44" num="4.2">Au dégoût ajoute l'horreur,</l>
					<l n="45" num="4.3">Ce qui nous soulève le cœur</l>
					<l n="46" num="4.4">Plus que la puanteur vulgaire</l>
					<l n="47" num="4.5">Qu'exhalent ces gueux en passant,</l>
					<l n="48" num="4.6">C'est que l'affreuse odeur du sang</l>
					<l n="49" num="4.7"><space unit="char" quantity="8"/>Toujours s'y mêle</l>
					<l n="50" num="4.8">A l'acre senteur des brasiers,</l>
					<l n="51" num="4.9">Débris de villages entiers</l>
					<l n="52" num="4.10">Fumants sous leur main criminelle !</l>
					<l n="53" num="4.11"><space unit="char" quantity="8"/>Haine éternelle</l>
					<l n="54" num="4.12">Aux Allemands qui sont venus !…</l>
					<l n="55" num="4.13">Qu'on fasse rougir une pelle</l>
					<l n="56" num="4.14">Pour brûler du sucre dessus !…</l>
				</lg>
			</div></body></text></TEI>