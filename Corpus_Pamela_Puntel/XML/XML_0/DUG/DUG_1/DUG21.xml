<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG21">
				<lg n="1">
					<l n="1" num="1.1">Allons ! vous êtes les plus forts,</l>
					<l n="2" num="1.2">Et l'Allemagne est triomphante !</l>
					<l n="3" num="1.3">Vous avez broyé nos efforts</l>
					<l n="4" num="1.4">Sous votre lourdeur étouffante !…</l>
					<l n="5" num="1.5">Déchus des gloires d'autrefois,</l>
					<l n="6" num="1.6">Domptés pour la première fois,</l>
					<l n="7" num="1.7">Nous endurons depuis six mois</l>
					<l n="8" num="1.8">Tous les maux que la guerre enfante !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Si vous avez été vainqueurs,</l>
					<l n="10" num="2.2">Ce n'est pas avec le courage ;</l>
					<l n="11" num="2.3">De vos bras, comme de vos cœurs,</l>
					<l n="12" num="2.4">L'algèbre seule aida la rage ;</l>
					<l n="13" num="2.5">Vos froids calculs nous ont surpris,</l>
					<l n="14" num="2.6">C'est la faim qui livra Paris,</l>
					<l n="15" num="2.7">Et vous ne l'auriez jamais pris</l>
					<l n="16" num="2.8">En vous exposant au carnage !…</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Pillant nos blés, volant notre or,</l>
					<l n="18" num="3.2">Rançonnant nos champs et nos villes,</l>
					<l n="19" num="3.3">Vous paralysez notre essor</l>
					<l n="20" num="3.4">Par les embûches les plus viles ;</l>
					<l n="21" num="3.5">Mais ce que vous ne prendrez pas,</l>
					<l n="22" num="3.6">Malgré vos cartes, vos compas,</l>
					<l n="23" num="3.7">Vos Krupp au stupide fracas,</l>
					<l n="24" num="3.8">Je vous en défie, imbéciles…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">C'est notre vieil esprit gaulois</l>
					<l n="26" num="4.2">Dont la pétillante étincelle</l>
					<l n="27" num="4.3">Ne pourrait trouer les parois</l>
					<l n="28" num="4.4">De votre tudesque cervelle…</l>
					<l n="29" num="4.5">L'Allemagne est très-forte !… elle a</l>
					<l n="30" num="4.6">Le poids de l'or qu'elle vola !</l>
					<l n="31" num="4.7">Elle est très-riche !… mais, voilà…</l>
					<l n="32" num="4.8">Elle n'est pas spirituelle !…</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Et ce que ne pourront avoir,</l>
					<l n="34" num="5.2">Non plus, vos épaisses Prussiennes,</l>
					<l n="35" num="5.3">C'est le charme, divin pouvoir</l>
					<l n="36" num="5.4">De nos frêles Parisiennes !</l>
					<l n="37" num="5.5">Bref, malgré ses adversités,</l>
					<l n="38" num="5.6">N'enviant pas vos qualités,</l>
					<l n="39" num="5.7">La France, que vous détestez,</l>
					<l n="40" num="5.8">Vous défend de toucher aux siennes !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">Soyez victorieux ! puissants !</l>
					<l n="42" num="6.2">De lauriers couronnez vos têtes !</l>
					<l n="43" num="6.3">A Bismark prodiguez l'encens !</l>
					<l n="44" num="6.4">Par les vols comptez vos conquêtes !</l>
					<l n="45" num="6.5">Mais, quoi que vous fassiez vraiment,</l>
					<l n="46" num="6.6">Votre nom de peuple allemand</l>
					<l n="47" num="6.7">Vous condamne éternellement</l>
					<l n="48" num="6.8">A rester lourds, grossiers et bêtes !…</l>
				</lg>
			</div></body></text></TEI>