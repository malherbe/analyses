<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG18">
				<lg n="1">
					<l n="1" num="1.1">Eh bien, oui, c'est la haine avec toute sa rage,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"/>Sans ménagement, sans pitié,</l>
					<l n="3" num="1.3">Heureuse de jeter les crachats au visage</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"/>Et prodigue de coups de pié !</l>
					<l n="5" num="1.5">La haine qui, parfois injuste, exagérée,</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"/>Reste légitime toujours,</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Car la Patrie est là réclamant la curée '</l>
					<l n="8" num="2.2"><space unit="char" quantity="8"/>Des tigres comme des vautours !</l>
					<l n="9" num="2.3">Et puis en fait d'excès, en fait de violences,</l>
					<l n="10" num="2.4"><space unit="char" quantity="8"/>Tout est bon quand on se défend</l>
					<l n="11" num="2.5">Contre tant d'obusiers, de pétrole et de lances</l>
					<l n="12" num="2.6"><space unit="char" quantity="8"/>Aux mains du Prussien triomphant !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Les délicats que froisse un pli même des roses,</l>
					<l n="14" num="3.2"><space unit="char" quantity="8"/>Qu'un mot trop vif fait rejimber,</l>
					<l n="15" num="3.3">Trouveront qu'on me voit, gazant trop peu les choses,</l>
					<l n="16" num="3.4"><space unit="char" quantity="8"/>Dans des vulgarités tomber ;</l>
					<l n="17" num="3.5">Mais la muse n'a plus le droit d'être bégueule</l>
					<l n="18" num="3.6"><space unit="char" quantity="8"/>En face d'un tel ennemi,</l>
					<l n="19" num="3.7">Et sans se reprocher d'être si forte en gueule</l>
					<l n="20" num="3.8"><space unit="char" quantity="8"/>Ne s'emporte pas à demi !…</l>
					<l n="21" num="3.9">C'est qu'elle a mis aussi l'uniforme de guerre !</l>
					<l n="22" num="3.10"><space unit="char" quantity="8"/>Jupon court, cigarette au bec</l>
					<l n="23" num="3.11">Et par les francs rimeurs prise pour vivandière,</l>
					<l n="24" num="3.12"><space unit="char" quantity="8"/>Elle bat la charge, boit sec,</l>
					<l n="25" num="3.13">Fait le coup de fusil crânement, la luronne…</l>
					<l n="26" num="3.14"><space unit="char" quantity="8"/>Et narguant le mauvais destin,</l>
					<l n="27" num="3.15">Emprunte volontiers au général Cambronne</l>
					<l n="28" num="3.16"><space unit="char" quantity="8"/>L'énergique mot de la fin !…</l>
				</lg>
			</div></body></text></TEI>