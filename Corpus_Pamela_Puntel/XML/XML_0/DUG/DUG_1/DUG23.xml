<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES ÉCLATS D'OBUS</title>
				<title type="medium">Édition électronique</title>
				<author key="DUG">
					<name>
						<forename>Ferdinand</forename>
						<surname>DUGUÉ</surname>
					</name>
					<date from="1816" to="1913">1816-1913</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1590 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUG_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES ÉCLATS D'OBUS</title>
						<author>FERDINAND DUGUÉ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5863441t.r=DUGU%C3%89%20LES%20%C3%89CLATS%20D%27OBUS%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES ÉCLATS D'OBUS</title>
								<author>FERDINAND DUGUÉ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-18" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUG23">
				<head type="main">MONSIEUR LE DUC</head>
				<lg n="1">
					<l n="1" num="1.1">C'est un fleur delysé qui porte un nom sonore…</l>
					<l n="2" num="1.2">Conseiller général et maire, puis encore</l>
					<l n="3" num="1.3">Duc de je ne sais quoi, grand ami du clergé,</l>
					<l n="4" num="1.4">D'une fortune immense ici-bas affligé,</l>
					<l n="5" num="1.5">Il goûtait les douceurs de l'existence humaine,</l>
					<l n="6" num="1.6">L'hiver dans son hôtel, l'été dans son domaine ;</l>
					<l n="7" num="1.7">Admirable manoir que le sien ! Des tableaux</l>
					<l n="8" num="1.8">D'ancêtres cuirassés, des grands arbres, des eaux</l>
					<l n="9" num="1.9">Jaillissantes ; enfin une de ces merveilles</l>
					<l n="10" num="1.10">Où le poëte peut évoquer dans ses veilles</l>
					<l n="11" num="1.11">Les ombres d'Henri quatre et de François premier,</l>
					<l n="12" num="1.12">Avec le blanc panache au faîte du cimier !…</l>
				</lg>
				<lg n="2">
					<l n="13" num="2.1">Brave homme au demeurant, quoique pauvre cervelle,</l>
					<l n="14" num="2.2">Toujours préoccupé d'une ferme modèle</l>
					<l n="15" num="2.3">Dont le sol torturé par de nouveaux engrais</l>
					<l n="16" num="2.4">Reçoit de nouveaux grains qui ne poussent jamais ;</l>
					<l n="17" num="2.5">Cependant, pour ses porcs comme pour ses génisses,</l>
					<l n="18" num="2.6">Médaillé par faveur dans deux ou trois comices,</l>
					<l n="19" num="2.7">Il préfère aux exploits de ses nobles aïeux</l>
					<l n="20" num="2.8">Sa gloire d'éleveur qu'il prend au sérieux :</l>
					<l n="21" num="2.9">Brave homme, je l'ai dit, affable, fort honnête,</l>
					<l n="22" num="2.10">N'étant point tout à fait ce qu'on nomme une bête…</l>
					<l n="23" num="2.11">Avec de grands dehors, œil bleu, nez aquilin,</l>
					<l n="24" num="2.12">Et toujours sur la lèvre un sourire bénin…</l>
				</lg>
				<lg n="3">
					<l n="25" num="3.1">Bref, tenant à sa vie autant qu'à sa fortune,</l>
					<l n="26" num="3.2">Et poltron, oh ! mais, là ! poltron comme la lune !…</l>
					<l n="27" num="3.3">La guerre le surprit dans des combinaisons</l>
					<l n="28" num="3.4">Pour améliorer l'espèce des dindons…</l>
					<l n="29" num="3.5">Et devant l'ennemi, que fait mon gentilhomme ?</l>
					<l n="30" num="3.6">Il déserte, emportant une très-forte somme</l>
					<l n="31" num="3.7">Toute en beaux louis d'or, et puis… sauvé, mon Dieu !</l>
					<l n="32" num="3.8">Il lire, comme on dit, son épingle du jeu !…</l>
				</lg>
				<lg n="4">
					<l n="33" num="4.1">Lui parti, que feront ces paysans qu'il laisse,</l>
					<l n="34" num="4.2">Abdiquant les devoirs de l'antique noblesse</l>
					<l n="35" num="4.3">Et les devoirs encor plus grands du citoyen,</l>
					<l n="36" num="4.4">Défendre avec leur sang jusqu'à son propre bien…</l>
					<l n="37" num="4.5">Que feront-ils ?… eh, mais, ils l'enverront d'emblée</l>
					<l n="38" num="4.6">Pour les représenter à la haute assemblée</l>
					<l n="39" num="4.7">Que Bismark nous permet d'élire en quelques jours…</l>
					<l n="40" num="4.8">Certes, ce n'est pas lui qui fera longs discours ;</l>
					<l n="41" num="4.9">Mais, suivant du passé la politique étroite,</l>
					<l n="42" num="4.10">Plus poltron que jamais, il prendra place à droite…</l>
					<l n="43" num="4.11">Et, par peur, préférant aux Brutus les Tarquins,</l>
					<l n="44" num="4.12">Il voudra dévorer tous les Républicains !</l>
				</lg>
			</div></body></text></TEI>