<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">FAIS CE QUE DOIS</title>
				<title type="sub">ÉPISODE DRAMATIQUE EN UN ACTE, EN VERS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>318 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">COP_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>FAIS CE QUE DOIS. ÉPISODE DRAMATIQUE EN UN ACTE, EN VERS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k408037p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Oeuvres complètes de François Coppée</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
								<biblScope unit="tome">Théâtre, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>édition partielle de la partie : ECRITS PENDANT LE SIÈGE</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="drama" key="COP217">
				<head type="main">FAIS CE QUE DOIS</head>
				<head type="form">ÉPISODE DRAMATIQUE EN UN ACTE, EN VERS</head>
				<head type="sub_1">Représentée pour la première fois<lb/>sur le théâtre de l’Odéon le 21 octobre 1871</head>
				<div type="body">
					<castList>
						<head>PERSONNAGES</head>
						<castItem>
							<role>DANIEL,</role>
							<roleDesc>maître d’école.</roleDesc>
							<actor>M. Dumaine</actor>
						</castItem>
						<castItem>
							<role>MARTHE,</role>
							<roleDesc>veuve d’un officier.</roleDesc>
							<actor>Mlle Sarah Bernhardt</actor>
						</castItem>
						<castItem>
							<role>HENRI,</role>
							<roleDesc>son jeune fils.</roleDesc>
							<actor>Mlle Jeanne Bernhardt</actor>
						</castItem>
					</castList>
					<set>Dans un port de mer en 1871.</set>
					<set>
						La terrasse d’un hôtel meublé, dans un port. Au fond, par une galerie
						à jour on aperçoit des mâts de navire et l’horizon de la mer. —
						Au lever du rideau, Marthe en grand deuil, est assise. Son fils
						Henri garçon de quatorze ans environ, en deuil aussi, se tient debout
						auprès d’elle.
					</set>
					<div type="act" n="1">
						<div type="scene" n="1">
							<head type="main">SCÈNE PREMIÈRE</head>
							<stage type="cast">
								<name>MARTHE</name>,
								<name>HENRI</name>
							</stage>
							<sp n="1">
								<speaker>HENRI.</speaker>
								<l part="I" n="1">Ainsi nous émigrons.</l>
							</sp>
							<sp n="2">
								<speaker>MARTHE.</speaker>
								<l part="F" n="1">Oui, nous quittons la France.</l>
							</sp>
							<sp n="3">
								<speaker>HENRI.</speaker>
								<l part="I" n="2">Voyager, quel bonheur !</l>
							</sp>
							<sp n="4">
								<speaker>MARTHE.</speaker>
								<l part="F" n="2">C’est assez de souffrance.</l>
								<l n="3">Ces quelques mois me font plus vieille de dix ans.</l>
								<l n="4">Nous avons des moyens de vivre suffisants,</l>
								<l n="5">Et nous nous embarquons, ce soir, pour l’Amérique.</l>
								<l n="6">Non, je ne forme pas un espoir chimérique</l>
								<l n="7">En croyant que là-bas tu feras ton chemin.</l>
								<l n="8">Mais ici, j’ai vraiment trop peur du lendemain</l>
								<l part="I" n="9">Nous partons.</l>
							</sp>
							<sp n="5">
								<speaker>HENRI.</speaker>
								<l part="M" n="9">Tu seras heureuse ?</l>
							</sp>
							<sp n="6">
								<speaker>MARTHE.</speaker>
								<l part="F" n="9">Je l’espère.</l>
								<stage>L’enfant s’éloigne et va regarder l’Océan ; elle le suit des yeux.</stage>
								<l n="10">Cette guerre maudite ! elle m’a pris ton père,</l>
								<l n="11">Et je ne connais pas l’endroit de son tombeau.</l>
								<l n="12">Et toi, mon bien-aimé, toi, si pur et si beau,</l>
								<l n="13">On te réserverait la même destinée.</l>
								<l n="14">— O France que j’aimais, patrie où je suis née,</l>
								<l n="15">Dont le langage est doux à mes lèvres toujours,</l>
								<l n="16">Car enfin c’est celui de mes jeunes amours</l>
								<l n="17">Et celui dans lequel ce fils m’a dit : Ma mère,</l>
								<l n="18">Hélas ! je devais donc t’accuser d’être amère,</l>
								<l n="19">Trouver ton ciel funeste et ton air étouffant.</l>
								<l n="20">Mais tu m’as faite veuve, et je n’ai qu’un enfant.</l>
							</sp>
							<sp n="7">
								<speaker>HENRI.</speaker>
								<l n="21">Comme c’est beau, la mer ! et comme un long voyage,</l>
								<l n="22">Ce doit être amusant. Mais vois donc ce nuage</l>
								<l part="I" n="23">De fumée et ce grand vaisseau.</l>
							</sp>
							<sp n="8">
								<speaker>MARTHE.</speaker>
								<l part="F" n="23">C’est un steamer</l>
								<l part="I" n="24">Qui revient de là-bas.</l>
							</sp>
							<sp n="9">
								<speaker>HENRI.</speaker>
								<l part="F" n="24">Comme c’est beau la mer !</l>
								<l n="25">Tantôt, maman, j’ai vu notre trois-mâts qu’on charge.</l>
								<l n="26">Un matelot disait : Le vent souffle du large.</l>
								<l n="27">Cela faisait flotter, ainsi que des rubans,</l>
								<l n="28">Les joyeux pavillons pavoisant les haubans.</l>
								<l n="29">Un mulâtre, tout noir sous la blancheur du linge,</l>
								<l n="30">Passait ; un petit mousse, agile comme un singe,</l>
								<l n="31">Descendait d’une vergue, et, tout le long des quais,</l>
								<l n="32">Au milieu des ballots, des fruits, des perroquets,</l>
								<l n="33">De l’odeur du goudron et du frisson des voiles,</l>
								<l n="34">Enchanté, je lisais, peints en noir sur des toiles,</l>
								<l n="35">Ces noms clairs et légers comme des cris d’oiseau :</l>
								<l n="36">Le Brésil, la Plata, Lima, Valparaiso.</l>
								<l n="37">Oh ! partir sur la mer ! — Et puis j’ai du courage.</l>
								<l n="38">J’ai réfléchi, Tant pis si nous faisons naufrage.</l>
								<l n="39">Comment ! J’aimerais mieux que la mer écumât,</l>
								<l n="40">Car je te sauverais sur un débris de mât.</l>
								<l n="41">Je sais mon Robinson par cœur. Que tu e veuilles</l>
								<l n="42">Ou non, je te ferais une maison de feuilles,</l>
								<l n="43">Sur une plage d’or, devant les flots nombreux,</l>
								<l n="44">Et là nous resterions tout seuls et très-heureux,</l>
								<l n="45">Bien plus, chère maman, qu’ici nous ne le sommes ;</l>
								<l n="46">Car ne te vois-je pas triste parmi les hommes ?</l>
							</sp>
							<sp n="10">
								<speaker>MARTHE.</speaker>
								<l part="I" n="47">Enfant !</l>
								<stage>A part.</stage>
								<l part="F" n="47">Comme à cet âge on sait vite oublier !</l>
								<stage>Haut.</stage>
								<l n="48">Allons ! va voir un peu jusqu’à notre voilier ;</l>
								<l n="49">Je crains que l’on n’ait pas inscrit notre passage.</l>
							</sp>
							<sp n="11">
								<speaker>HENRI.</speaker>
								<l part="I" n="50">J’y cours.</l>
							</sp>
							<sp n="12">
								<speaker>MARTHE.</speaker>
								<l part="F" n="50">Embrasse-moi, mon mignon, et sois sage.</l>
								<stage>Henri l’embrasse et sort.</stage>
							</sp>
						</div>
						<div type="scene" n="2">
							<head type="main">SCÈNE II</head>
							<sp n="13">
								<speaker>MARTHE.</speaker>
								<l n="51">Non, si je n’étais pas heureuse dans l’exil,</l>
								<l n="52">Du moins ce pauvre cher petit le sera-t-il.</l>
								<l n="53">La patrie, après tout, un préjugé vulgaire,</l>
								<l n="54">Qui me prendrait cet ange à la prochaine guerre</l>
								<l n="55">Et qui le jetterait en pâture au canon.</l>
								<l n="56">Et cependant, ô France ! il prononçait ton nom,</l>
								<l n="57">Ce héros que j’aimais, tombé dans la mêlée.</l>
								<l n="58">—Mon Dieu, s’il pouvait voir que je m’en suis allée</l>
								<l n="59">Du village de France où nous fûmes heureux,</l>
								<l n="60">Et qu’en deuil, à travers le monde aventureux,</l>
								<l n="61">J’emmène son enfant pour tenter la fortune ;</l>
								<l n="62">Si tout sanglant… Ce songe horrible m’importune.</l>
								<l n="63">Mais je suis mère, et j’ai bien fait comme je fis.</l>
								<l n="64">Je n’ai d’autre devoir que de sauver mon fils.</l>
								<l n="65">Mon âme interrogée a confiance en elle ;</l>
								<l n="66">Elle doit écouter sa crainte maternelle.</l>
								<l n="67">Tout autre sentiment dans mon cœur est tari.</l>
								<stage>Daniel parait au fond.</stage>
								<l n="68">Ah ! Daniel le vieil ami de mon mari.</l>
							</sp>
						</div>
						<div type="scene" n="3">
							<head type="main">SCÈNE III</head>
							<stage type="cast">
								<name>MARTHE</name>,
								<name>DANIEL</name>
							</stage>
							<sp n="14">
								<speaker>DANIEL.</speaker>
								<l part="I" n="69">Vous partez ?</l>
							</sp>
							<sp n="15">
								<speaker>MARTHE.</speaker>
								<l part="M" n="69">Ce soir même.</l>
							</sp>
							<sp n="16">
								<speaker>DANIEL.</speaker>
								<l part="M" n="69">Et l’enfant ?</l>
							</sp>
							<sp n="17">
								<speaker>MARTHE.</speaker>
								<l part="F" n="69">M’accompagne.</l>
							</sp>
							<sp n="18">
								<speaker>DANIEL.</speaker>
								<l n="70">Écoutez. Dans la pauvre école de campagne</l>
								<l n="71">Où j’apprends l’alphabet aux petits paysans,</l>
								<l n="72">Je n’ai là que des cœurs bons et peu médisants ;</l>
								<l n="73">Mais lorsqu’ils ont appris que, pour un long voyage,</l>
								<l n="74">Avec leur jeune ami vous quittiez le village,</l>
								<l n="75">Que, devant l’avenir sombre et plein de danger,</l>
								<l n="76">Leur petit compagnon fuyait à l’étranger,</l>
								<l n="77">O MARTHE. ils ont trouvé le mot qui déconcerte</l>
								<l n="78">Et, comme d’un soldat, ils ont dit : Il déserte.</l>
							</sp>
							<sp n="19">
								<speaker>MARTHE.</speaker>
								<l part="I" n="79">Mon ami…</l>
							</sp>
							<sp n="20">
								<speaker>DANIEL.</speaker>
								<l part="F" n="79">Votre fils, c’est vrai, n’est qu’un enfant,</l>
								<l n="80">Vous disposez de lui ; mais l’honneur vous défend</l>
								<l n="81">De l’entraîner si loin, avant qu’il y consente.</l>
								<l n="82">Avez-vous éclairé sa jeune âme innocente ?</l>
								<l n="83">De vous, pauvre affolée, a-t-il bien pu savoir</l>
								<l n="84">Ce qu’est une patrie et quel est son devoir ?</l>
								<l n="85">Connaît-il cette guerre infâme et notre haine ?</l>
								<l n="86">Sait-il qu’on nous a pris l’Alsace et la Lorraine,</l>
								<l n="87">Que Metz et que Strasbourg ont dû courber leurs fronts</l>
								<l n="88">Sous le joug allemand, et que nous en souffrons</l>
								<l n="89">Comme un soldat, pendant sa vieillesse attristée,</l>
								<l n="90">Souffre encor dans sa jambe autrefois amputée ?</l>
								<l n="91">Sait-il que dans nos mains on a brisé le fer</l>
								<l n="92">Et sait-il que son père est mort à Frœschwiller ?</l>
							</sp>
							<sp n="21">
								<speaker>MARTHE.</speaker>
								<l n="93">Oui, mais il sait encore et surtout que je l’aime,</l>
								<l n="94">Qu’il est toute ma vie et mon espoir suprême,</l>
								<l n="95">Et, s’il fallait le perdre enfin, que j’en mourrais.</l>
							</sp>
							<sp n="22">
								<speaker>DANIEL.</speaker>
								<l part="I" n="96">Marthe !</l>
							</sp>
							<sp n="23">
								<speaker>MARTHE.</speaker>
								<l part="F" n="96">Rappelez-vous le soir où je pleurais,</l>
								<l n="97">Près de vous, au début de l’affreuse campagne,</l>
								<l n="98">Lorsque cet officier, captif en Allemagne,</l>
								<l n="99">M’envoya cette croix d’honneur de mon mari</l>
								<l n="100">Et ces mots par lesquels je sais qu’il a péri.</l>
								<l n="101">Rappelez-vous. C’était une nuit de septembre.</l>
								<l n="102">M’agenouillant alors du côté de la chambre</l>
								<l n="103">Où se trouvait le lit de mon fils endormi,</l>
								<l n="104">Ardemment j’ai prié devant vous, mon ami,</l>
								<l n="105">Disant : — Conservez-le, Seigneur plein d’indulgence,</l>
								<l part="I" n="106">Pour mon amour.</l>
							</sp>
							<sp n="24">
								<speaker>DANIEL.</speaker>
								<l part="F" n="106">Et j’ai songé : Pour la vengeance.</l>
								<l n="107">O Marthe au nom du sang, au nom des pleurs versés…</l>
							</sp>
							<sp n="25">
								<speaker>MARTHE.</speaker>
								<l n="108">Non. La France m’a pris mon époux ; c’est assez.</l>
							</sp>
							<sp n="26">
								<speaker>DANIEL.</speaker>
								<l part="I" n="109">Vous ne pouvez partir.</l>
							</sp>
							<sp n="27">
								<speaker>MARTHE.</speaker>
								<l part="F" n="109">Dès ce soir, je l’emmène.</l>
							</sp>
							<sp n="28">
								<speaker>DANIEL.</speaker>
								<l part="I" n="110">Lâcheté !</l>
							</sp>
							<sp n="29">
								<speaker>MARTHE.</speaker>
								<l part="F" n="110">Je n’ai pas l’âme d’une Romaine.</l>
							</sp>
							<sp n="30">
								<speaker>DANIEL.</speaker>
								<l n="111">Mais vous regretterez demain ce moment-ci.</l>
							</sp>
							<sp n="31">
								<speaker>MARTHE.</speaker>
								<l part="I" n="112">Je suis mère.</l>
							</sp>
							<sp n="32">
								<speaker>DANIEL.</speaker>
								<l part="F" n="112">La France est une mère aussi.</l>
							</sp>
							<sp n="33">
								<speaker>MARTHE.</speaker>
								<l n="113">Une mère qui veut quoi s’égorge pour elle.</l>
							</sp>
							<sp n="34">
								<speaker>DANIEL.</speaker>
								<l n="114">Nous lui devons nos bras pour venger sa querelle.</l>
							</sp>
							<sp n="35">
								<speaker>MARTHE.</speaker>
								<l n="115">Et vous vous déchirez entre vous aujourd’hui</l>
							</sp>
							<sp n="36">
								<speaker>DANIEL.</speaker>
								<l part="I" n="116">Oh ! Marthe ! votre époux vous entend !</l>
							</sp>
							<sp n="37">
								<speaker>MARTHE.</speaker>
								<l part="F" n="116">Oui, c’est lui</l>
								<l n="117">Dont la voix dit : — Va t’en ! tout bas à mon oreille</l>
							</sp>
							<sp n="38">
								<speaker>DANIEL.</speaker>
								<l part="I" n="118">Vous blasphémez !</l>
							</sp>
						</div>
						<div type="scene" n="4">
							<head type="main">SCÈNE IV</head>
							<stage type="cast">
								<name>MARTHE</name>,
								<name>DANIEL</name>,
								<name>HENRI</name>
							</stage>
							<sp n="39">
								<speaker>HENRI.</speaker>
								<l part="F" n="118">Maman, le navire appareille,</l>
								<l n="119">Et ses voiles déjà palpitent dans le ciel.</l>
								<l n="120">Partons vite, partons !… Ah ! monsieur DANIEL.</l>
							</sp>
							<sp n="40">
								<speaker>DANIEL.</speaker>
								<l part="I" n="121">Henri…</l>
							</sp>
							<sp n="41">
								<speaker>MARTHE.</speaker>
								<l part="F" n="121">N’écoute pas cet homme, il va te dire,</l>
								<l n="122">Enfant, qu’il ne faut pas monter sur ce navire.</l>
								<l n="123">Il va t’épouvanter du voyage lointain,</l>
								<l n="124">Des dangers inconnus et du but incertain.</l>
								<l n="125">Puis il prononcera bien haut le nom de France ;</l>
								<l n="126">Il voudra re donner sa menteuse espérance.</l>
								<l n="127">Il prédira des temps meilleurs, des jours plus beaux,</l>
								<l n="128">Un souffle glorieux passant dans les drapeaux</l>
								<l n="129">Et les joyeux soldats, marchant à la frontière.</l>
								<l n="130">N’écoute pas cet homme, enfant ! ta vie entière,</l>
								<l n="131">Il la sacrifierait à son rêve trompeur.</l>
								<l n="132">Il fera résonner les grands mots qui font peur,</l>
								<l n="133">Évoquant le passé sombre et les morts eux-mêmes !</l>
								<l n="134">— Enfant, n’écoute pas cet homme, si tu m’aimes.</l>
							</sp>
							<sp n="42">
								<speaker>DANIEL.</speaker>
								<l n="135">Marthe vous vous trompez, et je ne doute pas</l>
								<l n="136">Du calme et vrai bonheur qui vous attend là-bas.</l>
								<l n="137">Vous me connaissez trop pour croire que je mente.</l>
								<l n="138">Partez. Le ciel est pur et la mer et clémente.</l>
								<l n="139">Vous avez le bon vent et le flot régulier.</l>
								<l n="140">Partez. Le Nouveau Monde, au sol hospitalier,</l>
								<l n="141">Où vous irez, conduits par la brise docile,</l>
								<l n="142">Vous garde ses déserts immenses pour asile,</l>
								<l n="143">Qui, dans la solitude, au soleil assoupis,</l>
								<l n="144">N’attendent qu’un colon pour se charger d’épis,</l>
								<l n="145">Et ses plaines sans fin et jamais parcourues</l>
								<l n="146">Où l’on trouve de l’or au sillon des charrues.</l>
								<l n="147">C'est là qu'est le bonheur. Aussi je vous le dis</l>
								<l n="148">Partez. Vous trouverez là-bas un paradis.</l>
								<l n="149">— Pour un homme pratique, et qui compte, et qui s’aime,</l>
								<l n="150">La patrie est le champ qu’on laboure et qu’on sème,</l>
								<l n="151">Et c’est un sentiment très-stupide et très-vieux</l>
								<l n="152">De s’attacher au sol où dorment les aïeux.</l>
								<l n="153">Et puis, que quittez-vous ? Une France frappée,</l>
								<l n="154">Qui saigne en s’appuyant sur un tronçon d’épée.</l>
								<l n="155">Fuyez. Vous resterez ici dans un enfer.</l>
								<stage>Avec une profonde tristesse.</stage>
								<l n="156">Nous sommes arrivés à notre âge de fer,</l>
								<l n="157">Et ce pays descend une fatale pente.</l>
								<l n="158">Espérer qu’il s’arrête un jour et se repente,</l>
								<l n="159">Nourrir cette sublime et folle illusion</l>
								<l n="160">Qu’il redevienne encor la grande nation,</l>
								<l n="161">Qu’il se relève enfin, je ne lose plus guère.</l>
								<l n="162">Hélas ! ce que j’ai vu dans la dernière guerre</l>
								<l n="163">M’a souvent fait penser que j’avais trop vécu,</l>
								<l n="164">Et, dusse-je irriter ta rage de vaincu,</l>
								<l n="165">Peuple qui dans l’orgueil et le mal persévères,</l>
								<l n="166">Tes fils sauront de moi les vérités sévères.</l>
								<l n="167">Oui, lorsque dans l’école ils viendront se ranger</l>
								<l n="168">Et sur nos grands malheurs d’hier m’interroger,</l>
								<l n="169">Il faudra que leur maître accablé leur raconte</l>
								<l n="170">Qu’il a pleuré du sang et sué de la honte.</l>
								<l n="171">Il faudra qu’il distingue, en sa ferme équité,</l>
								<l n="172">De ce qui fut fatal, ce qui fut mérité ;</l>
								<l n="173">Qu’il leur dise quel vent d’incroyable folie</l>
								<l n="174">Souffla pendant six mois sur la France envahie ;</l>
								<l n="175">Ces chefs et ces soldats se jetant sans raison</l>
								<l n="176">Le mot de lâcheté, le mot de trahison ;</l>
								<l n="177">Les factieux, malgré le danger de la ville,</l>
								<l n="178">Réservant leurs fusils pour la guerre civile,</l>
								<l n="179">Les aboiements des clubs, les efforts des partis</l>
								<l n="180">Par le malheur public à peine ralentis,</l>
								<l n="181">La foule se grisant de journaux et d’affiches,</l>
								<l n="182">La chasse aux croix d’honneur, des gens devenus riches</l>
								<l n="183">En volant sur le pain et l’habit du soldat ;</l>
								<l n="184">Et, dernier déshonneur et suprême attentat !</l>
								<l n="185">A l’heure de profond désespoir et de larmes</l>
								<l n="186">Où Paris épuisé dut déposer les armes,</l>
								<l n="187">A l’heure où, sous ses murs, ceux qui l’avaient vaincu,</l>
								<l n="188">Tristes que le géant eût encor survécu,</l>
								<l n="189">N’osaient trop s’approcher et se disaient : Il bouge ;</l>
								<l n="190">L’émeute parricide et folle, au drapeau rouge,</l>
								<l n="191">L’émeute des instincts, sans patrie et sans Dieu,</l>
								<l n="192">Ensanglantant la ville et la livrant au feu,</l>
								<l n="193">Devant les joyeux toasts portés à nos ruines</l>
								<l n="194">Par cent mille Allemands debout sur les collines !</l>
							</sp>
							<sp n="43">
								<speaker>HENRI.</speaker>
								<l n="195">O maître, finissez. Vous me faites rougir.</l>
							</sp>
							<sp n="44">
								<speaker>DANIEL.</speaker>
								<l n="196">Non, enfant, il est temps encor de réagir.</l>
								<l n="197">Parfois la guérison est prompte après la crise.</l>
								<l n="198">Oui, je veux appliquer le fer qui cautérise,</l>
								<l n="199">Sur le mauvais orgueil dans ces jeunes esprits.</l>
								<l n="200">Mais lorsque je verrais qu’ils m’ont enfin compris</l>
								<l n="201">Et qu’ils courbent le front sous ma sombre parole,</l>
								<l n="202">Alors je leur tiendrais le discours qui console.</l>
								<l n="203">— Je leur dirai qu’il fut encore des héros</l>
								<l n="204">Chez nos pauvres soldats arrachés aux hameaux,</l>
								<l n="205">Lorsque nous inonda cette effroyable armée ;</l>
								<l n="206">Comme on a bien souffert dans la ville affamée</l>
								<l n="207">Où pas un ne parlait de se rendre, pas un,</l>
								<l n="208">Et comme on a bien su mourir à Châteaudun !</l>
								<l n="209">Je leur dirai comment, dans Paris qu’on assiège</l>
								<l n="210">Et dans les camps lointain dispersés sur la neige,</l>
								<l n="211">On lutta de son mieux et l’on fit son devoir ;</l>
								<l n="212">Comment ceux-ci voyant toujours l’horizon noir,</l>
								<l n="213">Ceux-là croyant toujours, ô France ! à ton étoile,</l>
								<l n="214">Mangèrent le pain dur, dormirent sur la toile</l>
								<l n="215">Et tombèrent, vaincus, mais frappés par devant ;</l>
								<l n="216">Je leur raconterai ces histoires, enfant ;</l>
								<l n="217">Je les enivrerai de haine et de souffrance,</l>
								<l n="218">Et je préparerai des vengeurs à la France.</l>
							</sp>
							<sp n="45">
								<speaker>HENRI.</speaker>
								<l part="I" n="219">Des vengeurs !</l>
							</sp>
							<sp n="46">
								<speaker>MARTHE.</speaker>
								<l part="F" n="219">Daniel, Daniel, songez-y.</l>
								<l n="220">Vous le savez, je n’ai que ce pauvre enfant-ci.</l>
								<l n="221">Vous savez quelle fut la mort affreuse et lente</l>
								<l n="222">De son père, couché sur la paille sanglante,</l>
								<l n="223">Au milieu des hourras vainqueurs des ennemis.</l>
								<l n="224">Vous même convenez que le doute est permis,</l>
								<l n="225">Que cette nation est peut-être perdue.</l>
								<l n="226">Daniel répondez. Faut-il qu’on me le tue</l>
								<l n="227">Pour un dernier effort inutile, pour rien ?</l>
								<l part="I" n="228">Oh ! je n’ai plus d’espoir !</l>
							</sp>
							<sp n="47">
								<speaker>DANIEL.</speaker>
								<l part="F" n="228">Marthe, écoutez-moi bien.</l>
								<l n="229">Je suis simple d’esprit et n’ai rien d’un prophète,</l>
								<l n="230">Et pourtant, malgré tout, malgré notre défaite,</l>
								<l n="231">Je crois que nous pouvons encore être sauvés.</l>
							</sp>
							<sp n="48">
								<speaker>MARTHE.</speaker>
								<l part="I" n="232">Mais un enfant ?…</l>
							</sp>
							<sp n="49">
								<speaker>DANIEL.</speaker>
								<l part="F" n="232">Enfants, c’est vous qui le pouvez.</l>
								<l n="233">Car pour notre revanche, hélas, trop peu certaine,</l>
								<l n="234">Nous n’osons entrevoir qu’une date lointaine.</l>
								<l n="235">L’œuvre doit être longue et patiente ; et nous,</l>
								<l n="236">Nous qui vous aurons fait monter sur nos genoux</l>
								<l n="237">Afin de vous parler plus près des représailles,</l>
								<l n="238">Lorsque vous partirez, enfant, pour les batailles,</l>
								<l n="239">Nos cheveux déjà gris seront tout à fait blancs,</l>
								<l n="240">Et nous vous bénirons avec des bras tremblants.</l>
							</sp>
							<sp n="50">
								<speaker>MARTHE.</speaker>
								<l n="241">Vous doutez cependant de ce pays frivole ?</l>
							</sp>
							<sp n="51">
								<speaker>DANIEL.</speaker>
								<l n="242">Nous le transformerons, nous, les maîtres d’école.</l>
								<l n="243">Donnez vos fils ; ils sont ardents et belliqueux.</l>
								<l n="244">Donnez. Nous sauverons la patrie avec eux.</l>
								<l part="I" n="245">— Si nous le voulons bien…</l>
							</sp>
							<sp n="52">
								<speaker>MARTHE.</speaker>
								<l part="F" n="245">La revanche ! Chimère !</l>
								<l part="I" n="246">Vain rêve, œuvre impossible !</l>
							</sp>
							<sp n="53">
								<speaker>HENRI.</speaker>
								<l part="F" n="246">Écoutons-le, ma mère.</l>
							</sp>
							<sp n="54">
								<speaker>DANIEL.</speaker>
								<l n="247">Oui, si ce peuple veut et si tout son passé</l>
								<l n="248">De folie et d’erreur est un jour effacé,</l>
								<l n="249">Si de son ignorance enfin il se délivre,</l>
								<l n="250">S’il apprend à choisir la parole et le livre,</l>
								<l n="251">S’il cherche le progrès logique et régulier,</l>
								<l n="252">S’il se plie à la loi, s’il sait répudier</l>
								<l n="253">La révolution dont le monde s’effraie</l>
								<l n="254">Et, prenant le chemin de la liberté vraie,</l>
								<l n="255">Qui n’est que le respect de soi-même et d’autrui,</l>
								<l n="256">S’il répare et maudit ses fautes d’aujourd’hui,</l>
								<l n="257">Il reprendra sa place à la tête du monde.</l>
								<l n="258">Certe, avant de fonder la paix bonne et féconde,</l>
								<l n="259">Il lui faudra combattre encore, il lui faudra</l>
								<l n="260">Une guerre où l’Europe entière tremblera.</l>
								<l n="261">Car il n’est pas de joug enfin qu’on ne secoue,</l>
								<l n="262">Il ne peut pas garder ce soufflet sur la joue.</l>
								<l n="263">Mais pour cette œuvre sainte il n’a qu’un seul moyen,</l>
								<l n="264">C’est de faire un soldat de chaque citoyen,</l>
								<l n="265">De la patrie entière une famille armée</l>
								<l n="266">Et du seul sentiment du devoir enflammée,</l>
								<l n="267">Où le riche bourgeois coudoiera l’artisan,</l>
								<l n="268">Où le noble sera l’égal du paysan.</l>
								<l n="269">Car dans le régiment la nation se mêle ;</l>
								<l n="270">On partage la tente, on mange à la gamelle,</l>
								<l n="271">On se voit, on se parle et l’on devient amis.</l>
								<l n="272">Et quand tous ces soldats, à de vrais chefs soumis,</l>
								<l n="273">S’estimant et montrant, dans le même service,</l>
								<l n="274">Un même dévouement, un même sacrifice,</l>
								<l n="275">Contents du travail fait et du fusil porté,</l>
								<l n="276">Unis par les liens de la fraternité,</l>
								<l n="277">Marcheront dans le rang, calmes, forts, sans murmure,</l>
								<l n="278">O mon pays en deuil, la chose sera mûre,</l>
								<l n="279">Et, poussant vers le ciel ton cri de conquérant,</l>
								<l n="280">Tu pourras les répandre alors comme un torrent,</l>
								<l n="281">Et planter, glorieux, les trois couleurs altières</l>
								<l n="282">De notre vieux drapeau sur nos vieilles frontières !</l>
							</sp>
							<sp n="55">
								<speaker>MARTHE.</speaker>
								<l n="283">Et si nous succombons encore ? Si, vainqueur,</l>
								<l n="284">Le fer de l’Allemand nous entre jusqu’au cœur ?</l>
								<l n="285">Si Paris voit encore autour de ses murailles ?…</l>
							</sp>
							<sp n="56">
								<speaker>DANIEL.</speaker>
								<l n="286">Femme, nul ne connait le destin des batailles,</l>
								<l n="287">Mais s’il doit les revoir couvrir son horizon,</l>
								<l n="288">Que Paris cette fois songe à son vieux blason.</l>
								<stage>Avec enthousiasme.</stage>
								<l n="289">O navire ! voilà bien longtemps que la houle</l>
								<l n="290">Sur le morne Océan te harcèle et te roule,</l>
								<l n="291">Et que le rude assaut des lames et des vents</l>
								<l n="292">Fait craquer ta carène et grincer tes haubans.</l>
								<l n="293">Nous t’avons vu souvent, sous l’effort de l’orage,</l>
								<l n="294">Courir vers les écueils et voler au naufrage,</l>
								<l n="295">O vaisseau qui du grand Paris portes le nom !</l>
								<l n="296">Dans l’ouragan hurlant plus haut que le canon</l>
								<l n="297">Nous t’avons vu souvent t’abîmer sous la brume ;</l>
								<l n="298">Mais tu te relevais toujours, couvert d’écume,</l>
								<l n="299">Superbe et vomissant l’eau par les écubiers.</l>
								<l n="300">Donc, s’il faut qu’à la fin, Français, vous succombiez,</l>
								<l n="301">Dans un combat suprême, écrasés par le nombre,</l>
								<l n="302">Si Paris doit périr, si c’est bien l’heure sombre</l>
								<l n="303">D’amener pavillon ou de couler à pic,</l>
								<l n="304">Souviens-toi de Jean-Bart et de Du Couëdic,</l>
								<l n="305">Navire, souviens-toi de Villaret-Joyeuse !</l>
								<l n="306">Lorsqu’après la bataille atroce et furieuse,</l>
								<l n="307">Rouge de sang, n’ayant plus de mâts, plus d’agrès,</l>
								<l n="308">Tu verras ces maudits face à face, tout près,</l>
								<l n="309">Et te jetant déjà les chaînes de l’esclave,</l>
								<l n="310">Meurs en volcan pour les engloutir sous ta lave,</l>
								<l n="311">Et que le monde entier convienne avec effroi,</l>
								<l n="312">Que le sort du Vengeur est seul digne de toi !</l>
							</sp>
							<sp n="57">
								<speaker>HENRI.</speaker>
								<l n="313">O mère, il a raison. C’est un conseil funeste</l>
								<l part="I" n="314">Que te donnait tout bas ton désespoir.</l>
								<stage>A Daniel.</stage>
								<l part="F" n="314">Je reste.</l>
							</sp>
							<sp n="58">
								<speaker>MARTHE.à DANIEL.</speaker>
								<l part="I" n="315">Hélas ! qu’avez-vous fait ?</l>
							</sp>
							<sp n="59">
								<speaker>DANIEL.</speaker>
								<l part="F" n="315">Le devoir est ici.</l>
							</sp>
							<sp n="60">
								<speaker>MARTHE.à HENRI.</speaker>
								<l part="I" n="316">Tu l’exiges de moi, cruel enfant ?</l>
							</sp>
							<sp n="61">
								<speaker>HENRI.se jetant à son cou.</speaker>
								<l part="F" n="316">Merci !</l>
							</sp>
							<sp n="62">
								<speaker>MARTHE.</speaker>
								<l n="317">Soit, je cède, et je mets au ciel mon espérance.</l>
								<l part="I" n="318">Dieu, protège mon fils !</l>
							</sp>
							<sp n="63">
								<speaker>DANIEL.</speaker>
								<l part="F" n="318">Dieu, protège la France !</l>
							</sp>
						</div>
					</div>
				</div>
			</div></body></text></TEI>