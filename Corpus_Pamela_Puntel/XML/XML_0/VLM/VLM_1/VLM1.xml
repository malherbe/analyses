<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE BAISER DE L'ALSACIENNE</title>
				<title type="medium">Édition électronique</title>
				<author key="VLM">
					<name>
						<forename>Germain</forename>
						<surname>GIRARD</surname>
						<addName type="pen_name">VILLEMER</addName>
					</name>
					<date from="1842" to="1892">1842-1892</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>138 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VLM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE BAISER DE L'ALSACIENNE</title>
						<author>VILLEMER</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Eveillard et Jacquot, Editeurs</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VLM1">
				<head type="main">LE BAISER DE L'ALSACIENNE</head>
				<head type="form">Dit par M. Delaunay de la Comédie Française</head>
				<lg n="1">
					<l n="1" num="1.1">Le soleil de Juillet illuminait l' Alsace.</l>
					<l n="2" num="1.2">Les appels du clairon éclataient dans l'espace…</l>
					<l n="3" num="1.3">Tout le jour, défilaient, en chantant, pleins d 'entrain,</l>
					<l n="4" num="1.4">Des régiments français s'en allant vers le Rhin…</l>
					<l n="5" num="1.5">Sur la place publique, auprès de la fontaine,</l>
					<l n="6" num="1.6">De son regard rêveur une jeune Alsacienne</l>
					<l n="7" num="1.7">Depuis un long moment suivait avec amour</l>
					<l n="8" num="1.8">Les zouaves préparant leur campement d'un jour :</l>
					<l n="9" num="1.9">Quand l'un d'eux, un sergent à la mine éveillée,</l>
					<l n="10" num="1.10">Se dirigea vers la fontaine ensoleillée :</l>
					<l n="11" num="1.11">— Dites, la belle enfant, fit-il, portant la main</l>
					<l n="12" num="1.12">A son turban tout gris des poudres du chemin,</l>
					<l n="13" num="1.13">D'un peu d 'eau, s'il vous plait, me ferez-vous la grâce ?</l>
				</lg>
				<lg n="2">
					<l n="14" num="2.1">A cette question la blonde enfant d'Alsace</l>
					<l part="I" n="15" num="2.2">Sourit et lui tendit son vase :</l>
				</lg>
				<lg n="3">
					<l part="F" n="15">« Assurément,</l>
					<l n="16" num="3.1">Dit-elle, j'en aurais pour tout le régiment.</l>
					<l n="17" num="3.2">Buvez sans crainte l'eau de la source française,</l>
					<l n="18" num="3.3">Buvez, petit sergent, buvez tout à votre aise ! »</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Mais quoiqu'elle insistât le sergent ne but pas,</l>
					<l n="20" num="4.2">Et les yeux dans ses yeux, il ajouta plus bas :</l>
					<l n="21" num="4.3">« Non, vous d'abord, buvez… Ma soif n'est pas pressée…</l>
					<l n="22" num="4.4">Buvez ! Je voudrais tant savoir votre pensée ! »</l>
				</lg>
				<lg n="5">
					<l n="23" num="5.1">Riant de son caprice, elle but lentement</l>
					<l n="24" num="5.2">Et de nouveau lui dit : — Buvez donc maintenant…</l>
				</lg>
				<lg n="6">
					<l n="25" num="6.1">Il étancha sa soif, il but avec ivresse</l>
					<l n="26" num="6.2">Et puis la regardant soudain avec tendresse :</l>
					<l n="27" num="6.3">« Je ne sais rien, dit-il, de ce que vous pensez ;</l>
					<l n="28" num="6.4">Mais si vous permettez, voilà ce que je sais.</l>
					<l n="29" num="6.5">Je voudrais bien, avant d' affronter la bataille,</l>
					<l n="30" num="6.6">Pouvoir dans mes dix doigts enlacer votre taille,</l>
					<l n="31" num="6.7">Et vous prendre un baiser, un seul, là, mais bien doux !</l>
					<l n="32" num="6.8">Dites , la belle enfant, dites, permettez-vous ! »</l>
				</lg>
				<lg n="7">
					<l n="33" num="7.1">Elle se défendit : C 'est un peu trop d'audace,</l>
					<l n="34" num="7.2">Dit-elle en souriant, pour un sergent qui passe !</l>
					<l n="35" num="7.3">Nos garçons ne vont pas si vite par ici…</l>
					<l n="36" num="7.4">Bah ! c'est qu'on est pressée, dit-il, riant aussi…</l>
					<l part="I" ana="unanalyzable" n="37" num="7.5">Et puis… , voilà je suis parisien !</l>
				</lg>
				<lg n="8">
					<l part="F" ana="unanalyzable" n="37">L' Alsacienne</l>
					<l ana="unanalyzable" n="38" num="8.1">......................................................................................</l>
				</lg>
				<lg n="9">
					<l n="39" num="9.1">Mais vous êtes français, dit-elle, c'est assez !</l>
					<l n="40" num="9.2">Je sens que tout en moi frémit quand vous passez…</l>
					<l n="41" num="9.3">Faites votre devoir, marchez à la victoire…</l>
					<l n="42" num="9.4">Et moi, je vous promets, et vous pouvez m'en croire,</l>
					<l n="43" num="9.5">Si loin que le combat puisse entrainer vos pas,</l>
					<l n="44" num="9.6">Qu 'au retour mon baiser ne vous manquera pas. »</l>
				</lg>
				<lg n="10">
					<l n="45" num="10.1">Le sergent s'éloigna rêveur. Cette fillette</l>
					<l n="46" num="10.2">A la parole grave avait troublé sa tête…</l>
					<l n="47" num="10.3">Il n 'avait qu'un désir, lui parler, la revoir…</l>
					<l n="48" num="10.4">Sous sa fenêtre, il vint encor rêver le soir ;</l>
					<l n="49" num="10.5">Quand le clairon sonna le départ à l'aurore,</l>
					<l n="50" num="10.6">En repliant sa tente, il rêvait d'elle encore…</l>
					<l n="51" num="10.7">Mais comme il s'éloignait, suivant le régiment,</l>
					<l n="52" num="10.8">Soudain elle apparut, souriant doucement</l>
					<l n="53" num="10.9">Et de vergiss-mein-nicht lui jetant une gerbe,</l>
					<l n="54" num="10.10">Elle semblait lui dire en un geste superbe :</l>
					<l n="55" num="10.11">« Va ! sergent ! souviens-toi ! moi je me souviendrai !</l>
					<l n="56" num="10.12">Mérite mon baiser, je te l'apporterai ! »</l>
				</lg>
				<lg n="11">
					<l n="57" num="11.1">Wissembourg ! Wissembourg ! L'Allemagne insolent</l>
					<l n="58" num="11.2">Pourra nous reprocher cette tache sanglante !</l>
					<l n="59" num="11.3">Lorsqu'à vingt allemands tout le jour un français</l>
					<l n="60" num="11.4">Tient tête, sans faiblir, c'est encor le succès.</l>
					<l n="61" num="11.5">Furent-ils des vainqueurs ces soldats qui dans l'ombre,</l>
					<l n="62" num="11.6">Foudroyaient à coup sûr, confiants dans leur nombre !</l>
					<l n="63" num="11.7">Non ! le vainqueur du jour, ce fut ce beau martyr,</l>
					<l n="64" num="11.8">Douai, qui ne pouvant plus vaincre, sut mourir…</l>
					<l n="65" num="11.9">Ce furent ces héros, luttant cent contre mille,</l>
					<l n="66" num="11.10">Qui sans espoir, sachant tout effort inutile,</l>
					<l n="67" num="11.11">S' en allèrent, le rire aux dents, sous le drapeau,</l>
					<l n="68" num="11.12">Dans les rangs allemands se creuser un tombeau !</l>
					<l n="69" num="11.13">Wissembourg ! Wissembourg ! Laisse donc l'Allemagne</l>
					<l n="70" num="11.14">Célébrer ce début de l'horrible campagne :</l>
					<l n="71" num="11.15">Les tombes des héros fleuriront, quelque-jour,</l>
					<l n="72" num="11.16">Et les vaincus auront la victoire à leur tour !</l>
				</lg>
				<lg n="12">
					<l n="73" num="12.1">C'est fini. La journée, hélas ! est consommée,</l>
					<l n="74" num="12.2">Les cadavres sanglants de tout un corps d'armée</l>
					<l n="75" num="12.3">Dans des ruisseaux de sang sommeillent pour jamais</l>
					<l n="76" num="12.4">Et Wissembourg n'est plus qu'un cercueil désormais.</l>
					<l n="77" num="12.5">La ferme où tout riait la veille est morne et sombre,</l>
					<l n="78" num="12.6">Et ses vieux murs troués par des boulets sans nombre,</l>
					<l n="79" num="12.7">Ouverts comme une porte immense jusqu'au toit,</l>
					<l n="80" num="12.8">Semblent dire la mort : Entre comme chez toi !</l>
					<l n="81" num="12.9">Le vieux moulin s'est tû. Son aile pend meurtrie,</l>
					<l n="82" num="12.10">Et du ruisseau qui court à travers la prairie,</l>
					<l n="83" num="12.11">Le sang a coloré l'eau si pure jadis.</l>
					<l n="84" num="12.12">Sur sa rive, parmi les doux myosotis</l>
					<l n="85" num="12.13">Sous le grand saule où le soleil couchant se joue,</l>
					<l n="86" num="12.14">La poitrine sanglante et la mort à la joue</l>
					<l n="87" num="12.15">Le sergent agonise. Il a fait son devoir</l>
					<l n="88" num="12.16">Et payé son tribut à l'affreux désespoir.</l>
					<l n="89" num="12.17">Au moment d 'expirer, il ouvre sa tunique :</l>
					<l n="90" num="12.18">Sa main y cherche encor une chère relique,</l>
					<l n="91" num="12.19">Les fleurs de l'Alsacienne. Hélas ! il espérait</l>
					<l n="92" num="12.20">Dans ce doux souvenir. Sans rien dire, en secret,</l>
					<l n="93" num="12.21">Avant de s'élancer dans la mêlée horrible,</l>
					<l n="94" num="12.22">Il pensait en son cœur : Non ! ce n'est pas possible !</l>
					<l n="95" num="12.23">Je ne peux pas mourir ! Je garde cet espoir,</l>
					<l n="96" num="12.24">Elle me l'a promis et je dois la revoir !</l>
				</lg>
				<lg n="13">
					<l n="97" num="13.1">Pourtant il va mourir ! Sur ses lèvres glacées,</l>
					<l n="98" num="13.2">C'est vainement qu'il tient les chères fleurs pressées.</l>
					<l n="99" num="13.3">L'Alsacienne, échappant à son rêve éperdu,</l>
					<l n="100" num="13.4">Ne vient pas lui donner le baiser attendu…</l>
					<l n="101" num="13.5">Tout à coup un appel a frappé son oreille…</l>
					<l n="102" num="13.6">Aux portes de la mort le mourant se réveille</l>
					<l n="103" num="13.7">Quelqu'un vient… Une voix soupire en le nommant :</l>
					<l n="104" num="13.8">« O Dieu ! permettez-moi de tenir mon serment ! »</l>
					<l n="105" num="13.9">C'est elle… Elle le cherche… Elle approche… Elle arrive,</l>
					<l n="106" num="13.10">Un noir pressentiment lui fait suivre la rive</l>
					<l n="107" num="13.11">Où les vergiss-mein-nicht ouvrent leurs doux yeux bleus.</l>
					<l n="108" num="13.12">Soudain elle le voit, il est là, sous ses yeux,</l>
					<l n="109" num="13.13">Sanglant, expirant, mort… Un suprême sourire</l>
					<l n="110" num="13.14">Sur sa lèvre glacée erre et semble lui dire :</l>
					<l n="111" num="13.15">« J'attendais ton baiser… Oh ! pourquoi m'oublier ! »</l>
				</lg>
				<lg n="14">
					<l n="112" num="14.1">L' Alsacienne en pleurant vient de s'agenouiller,</l>
					<l n="113" num="14.2">Elle lave le sang qui coule des blessures,</l>
					<l n="114" num="14.3">Mais en vain sa douleur se répand en murmures,</l>
					<l n="115" num="14.4">En vain sa douce voix s'épuise à répéter :</l>
					<l n="116" num="14.5">« C'est moi ! c'est mon baiser que je viens t'apporter ! »</l>
					<l n="117" num="14.6">Le petit sergent dort dans la paix éternelle.</l>
				</lg>
				<lg n="15">
					<l n="118" num="15.1">Alors, d'un long baiser l'alsacienne fidèle</l>
					<l n="119" num="15.2">Ferme ses yeux éteints, et puis, se redressant,</l>
					<l n="120" num="15.3">Frémissante, la lèvre encor rouge de sang :</l>
					<l n="121" num="15.4">« Prussiens ! souvenez-vous comme moi, cria-t-elle,</l>
					<l n="122" num="15.5">Vous venez de sceller l'union immortelle.</l>
					<l n="123" num="15.6">De cet hymen sanglant la haine doit germer,</l>
					<l n="124" num="15.7">Et je vous hais autant que j'aurais dû l'aimer ! »</l>
				</lg>
				<lg n="16">
					<l n="125" num="16.1">Sur la place publique, auprès de la fontaine</l>
					<l n="126" num="16.2">On voit depuis ce jour revenir l'Alsacienne,</l>
					<l n="127" num="16.3">Ses cheveux blonds noués d'un large ruban noir…</l>
					<l n="128" num="16.4">Sur son front est écrit : Je suis le désespoir !</l>
					<l n="129" num="16.5">Là-bas, les paysans qui savent son épreuve,</l>
					<l n="130" num="16.6">La saluant bien bas, murmurent : C 'est la veuve !</l>
					<l n="131" num="16.7">Les prussiens ont tué son sergent, et depuis,</l>
					<l n="132" num="16.8">Vouée à sa vengeance, elle attend près du puits</l>
					<l n="133" num="16.9">Des régiments français la prochaine arrivée…</l>
					<l n="134" num="16.10">Ah ! sonne, heure bénie ! heure ardemment rêvée !</l>
					<l n="135" num="16.11">Soleil de la revanche, éclate au fond des cieux !</l>
					<l n="136" num="16.12">Debout, français ! frappons sans pitié, furieux,</l>
					<l n="137" num="16.13">Et qu'aux veuves en deuil rendant enfin justice,</l>
					<l n="138" num="16.14">Chaque puits Alsacien de sang prussien s 'emplisse !</l>
				</lg>
			</div></body></text></TEI>