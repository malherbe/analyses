<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES MOBILES D'ILLE-ET-VILAINE</title>
				<title type="medium">Édition électronique</title>
				<author key="PLI">
					<name>
						<forename>Oscar</forename>
						<nameLink>de</nameLink>
						<surname>POLI</surname>
					</name>
					<date from="1838" to="1908">1838-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>32 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">PLI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES MOBILES D'ILLE-ET-VILAINE</title>
						<author>Oscar De Poli</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2719920/f1.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Le Figaro</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro : journal non politique.</publisher>
									<date when="1870">1870-09-24</date>
								</imprint>
								<biblScope unit="issue">267</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="PLI1">
				<head type="main">LES MOBILES D'ILLE-ET-VILAINE</head>
				<lg n="1">
					<l n="1" num="1.1">Le front haut, l'œil chargé d'éclairs,</l>
					<l n="2" num="1.2">Pour chasser les hordes prussiennes,</l>
					<l n="3" num="1.3">Ils ont quitté, joyeux et fiers,</l>
					<l n="4" num="1.4">La terre couverte de chênes.</l>
					<l n="5" num="1.5">Ils vont mourir, ces fils des chouans,</l>
					<l n="6" num="1.6">Pour la France républicaine !</l>
					<l n="7" num="1.7">Honneur aux héros de vingt ans,</l>
					<l n="8" num="1.8">Aux mobiles d'Ille-et-Vilaine !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Aux pieds du prêtre bénissant,</l>
					<l n="10" num="2.2">Ils ont juré sur le saint livre</l>
					<l n="11" num="2.3">De verser là bas tout leur sang.</l>
					<l n="12" num="2.4">De vaincre ou de ne point survivre !</l>
					<l n="13" num="2.5">Devant ces soldats paysans,</l>
					<l n="14" num="2.6">O roi, ta déroute est certaine !</l>
					<l n="15" num="2.7">Honneur aux héros de vingt ans,</l>
					<l n="16" num="2.8">Aux mobiles d'Ille-et-Vilaine !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Comme leurs pères, autrefois,</l>
					<l n="18" num="3.2">Ils portent la sainte médaille</l>
					<l n="19" num="3.3">Et font un grand signe de croix</l>
					<l n="20" num="3.4">Avant d'aborder la mitraille,</l>
					<l n="21" num="3.5">On les voit, ces pieux géants,</l>
					<l n="22" num="3.6">Les derniers au feu dans la plaine !</l>
					<l n="23" num="3.7">Honneur aux héros de vingt ans,</l>
					<l n="24" num="3.8">Aux mobiles d'Ille-et-Vilaine !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Oui, c'est la guerre des géants</l>
					<l n="26" num="4.2">Que la Bretagne recommence ;</l>
					<l n="27" num="4.3">Son bras est fort comme au vieux temps,</l>
					<l n="28" num="4.4">Son cœur est le cœur de la France !</l>
					<l n="29" num="4.5">Tremblez, misérables uhlans :</l>
					<l n="30" num="4.6">L'heure du triomphe est prochaine !…</l>
					<l n="31" num="4.7">Honneur aux héros de vingt ans,</l>
					<l n="32" num="4.8">Aux mobiles d'Ille-et-Vilaine !</l>
				</lg>
			</div></body></text></TEI>