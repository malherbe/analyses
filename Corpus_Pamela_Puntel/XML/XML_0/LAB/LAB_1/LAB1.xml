<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA FRANCE À GARIBALDI</title>
				<title type="sub_1">ODE</title>
				<title type="medium">Édition électronique</title>
				<author key="LAB">
					<name>
						<forename>Gustave</forename>
						<surname>LABOURT</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>48 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">LAB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LA FRANCE À GARIBALDI, ODE</title>
						<author>GUSTAVE LABOURT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>La Sentinelle Mentonnaise</publisher>
						<pubPlace>Menton</pubPlace>
						<idno type="URI">www.basesdocumentaires-cg06.fr/archives</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA FRANCE À GARIBALDI, ODE</title>
								<author>GUSTAVE LABOURT</author>
								<imprint>
									<pubPlace>Menton</pubPlace>
									<publisher>La Sentinelle Mentonnaise, Journal politique et Littéraire</publisher>
									<date when="1871">28 mars 1900</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="LAB1">
				<head type="main">LA FRANCE À GARIBALDI</head>
				<head type="form">ODE</head>
				<head type="sub_2">
					La Sentinelle Mentonnaise, Journal politique et Littéraire, 28 mars 1900<lb/>
					(republication à l'occasion de l'inauguration du monument à Garibaldi)
				</head>
				<head type="sub_1">POÉSIE DITE AU THÉÂTRE FRANÇAIS DE NICE, LE 28 MARS 1871</head>
				<lg n="1">
					<l n="1" num="1.1">Toi qui viens de saisir ta brave et fière épée,</l>
					<l n="2" num="1.2">Pour tâcher de venger la France ensanglantée…</l>
					<l n="3" num="1.3">Tu fis pour la sauver un effort surhumain !</l>
					<l n="4" num="1.4">Et semblable à ce noble et sublime romain…</l>
					<l n="5" num="1.5">Lorsque ton dévouement nous devient inutile,</l>
					<l n="6" num="1.6">Méprisant les honneurs tu rentres dans ton île</l>
					<l n="7" num="1.7">Laissant les orgueilleux rêver d'ambitions.</l>
					<l n="8" num="1.8">Ton rêve, à toi, ce fut le bonheur des nations.</l>
					<l n="9" num="1.9">Oui ! tu rêvais la paix, tu détestais la guerre.</l>
					<l n="10" num="1.10">Va, nous le savons tous… Tu voulais sur la terre</l>
					<l n="11" num="1.11">Voir cesser la discorde ! et voir l'humanité</l>
					<l n="12" num="1.12">Ne former qu'un grand peuple… où la fraternité</l>
					<l n="13" num="1.13">Pût régner seule ! Enfin tu voulais, bon génie,</l>
					<l n="14" num="1.14">Sous ton talon de fer briser la tyrannie !!</l>
					<l n="15" num="1.15">Garibaldi n'est pas un vulgaire soldat,</l>
					<l n="16" num="1.16">Car ce n'est qu'aux tyrans qu'il a livré combat !</l>
					<l n="17" num="1.17">Un seul mot doit le peindre, et je n'en veux pas d'autre !</l>
					<l n="18" num="1.18">On dit c'est un héros ! Je dis, c’est un apôtre !</l>
					<l n="19" num="1.19">Son cri de ralliement le voici : Dieu puissant</l>
					<l n="20" num="1.20">Donne la liberté ! mais sans verser de sang !</l>
					<l n="21" num="1.21">N'est-il pas incroyable à l'époque où nous sommes</l>
					<l n="22" num="1.22">De penser que l'on peut faire tuer des hommes ?</l>
					<l n="23" num="1.23">Où donc est la science ? où donc est le progrès ?</l>
					<l n="24" num="1.24">Si nous nous égorgeons entre nous sans regrets !</l>
					<l n="25" num="1.25">Laissons à d'autres temps un moyen si sauvage</l>
					<l n="26" num="1.26">Et par l'intelligence écrasons l'esclavage !</l>
					<l n="27" num="1.27">Merci Garibaldi ! Merci ! C'est notre France</l>
					<l n="28" num="1.28">Qui te dit par ma voix, que sa reconnaissance</l>
					<l n="29" num="1.29">Est éternelle ! e pas un Français n'oubliera</l>
					<l n="30" num="1.30">Le nom du grand héros qui vit à Caprera !</l>
					<l n="31" num="1.31">C'est un enfant de Nice, à Nice il a grandi !</l>
					<l n="32" num="1.32">C'est un enfant de Nice, à Nice il a grandi !</l>
					<l n="33" num="1.33">Elle a droit d'être fière ! Une aussi grande gloire !</l>
					<l n="34" num="1.34">Est une belle page au livre de l'histoire !</l>
					<l n="35" num="1.35">Ta vie avec ton sang, tu nous l'offrais, grand cœur,</l>
					<l n="36" num="1.36">Pour sauver notre France et venger notre honneur !</l>
					<l n="37" num="1.37">Oui ! nous devons l'aimer jusqu'à l'idolâtrie !</l>
					<l n="38" num="1.38">Le pays qu'on écrase est toujours ta patrie !</l>
					<l n="39" num="1.39">Tu sauves l'opprimé, tu combats les tyrans</l>
					<l n="40" num="1.40">Et tes braves soldats s'appellent tes enfants !</l>
					<l n="41" num="1.41">Puis quand on veut payer ton sublime courage</l>
					<l n="42" num="1.42">Tu vas cancer ta gloire au fond d'un hermitage,</l>
					<l n="43" num="1.43">Tu n'entends plus là-bas retentir le canon.</l>
					<l n="44" num="1.44">Mais des échos de gloire ont répété ton nom !</l>
					<l n="45" num="1.45">Il n'est plus désormais qu'un cri dans notre France.</l>
					<l n="46" num="1.46">Cri parti de nos cœurs pleins de reconnaissance</l>
					<l n="47" num="1.47">Et qui retentira du nord jusqu'au midi.</l>
					<l n="48" num="1.48">Vive la liberté ! Vive Garibaldi.</l>
				</lg>
				<p>
					P.S. –– Aujourd'hui hélas ! Ricciotti Garibaldi, au lieu de lutter pour la liberté,
					combat avec la tyrannie. Les temps sont bien changés !
				</p>
			</div></body></text></TEI>