<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'ANNÉE MAUDITE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="GRS">
					<name>
						<forename>Charles</forename>
						<surname>GRANDSARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2146 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'ANNÉE MAUDITE 1870-1871</title>
						<author>Charles Grandsard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61285325.r=GRANDSARD%20L%27ANN%C3%89E%20MAUDITE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'ANNÉE MAUDITE 1870-1871</title>
								<author>Charles Grandsard</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE DU PETIT JOURNAL</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRS1">
				<head type="main">L'ANNÉE MAUDITE</head>
				<lg n="1">
					<l n="1" num="1.1">Laissez-moi remuer la sanglante poussière</l>
					<l n="2" num="1.2">Où gît notre grandeur, prestige évanoui,</l>
					<l n="3" num="1.3">Où naguère tomba notre illustre bannière,</l>
					<l n="4" num="1.4">Qui brilla si longtemps sur le monde ébloui !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Laissez-moi feuilleter page à page l'histoire</l>
					<l n="6" num="2.2">De nos affronts hideux, de nos cuisants malheurs,</l>
					<l n="7" num="2.3">Tordre mon cœur saignant, victime expiatoire,</l>
					<l n="8" num="2.4">Et vider devant tous la coupe des douleurs !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Car la douleur n'est pas ce que pense le lâche,</l>
					<l n="10" num="3.2">Un acide mortel, un dévorant poison</l>
					<l n="11" num="3.3">Qui, dans l'âme versé, la mine sans relâche,</l>
					<l n="12" num="3.4">Et ronge tout : honneur, énergie et raison !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Sans doute, en l'effleurant d'une lèvre amollie,</l>
					<l n="14" num="4.2">On n'y sent rien, d'abord, qu'une amère liqueur ;</l>
					<l n="15" num="4.3">Mais, dans le fond du vase, on trouve sous la lie</l>
					<l n="16" num="4.4">Un suc fortifiant qui relève le cœur !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">On y trouve le don de souffrir, et de taire</l>
					<l n="18" num="5.2">L'angoisse dont la griffe à la gorge nous prend,</l>
					<l n="19" num="5.3">Et ce puissant amour pour le devoir austère</l>
					<l n="20" num="5.4">Qui tient lieu du bonheur et seul fait l'homme grand !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">On y trouve un mépris absolu, sans limite,</l>
					<l n="22" num="6.2">Pour tout frivole éclat, pour toute vanité,</l>
					<l n="23" num="6.3">Et cette dignité mâle que rien n'imite,</l>
					<l n="24" num="6.4">Qui s'attache au malheur noblement accepté !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Ah ! si nous recouvrons dans l'odieux supplice</l>
					<l n="26" num="7.2">Ces virils sentiments, trop longtemps désappris.,</l>
					<l n="27" num="7.3">Nous pourrons sans regret vider l'amer calice :</l>
					<l n="28" num="7.4">Nous ne les aurons pas payés d'un trop haut prix !</l>
				</lg>
			</div></body></text></TEI>