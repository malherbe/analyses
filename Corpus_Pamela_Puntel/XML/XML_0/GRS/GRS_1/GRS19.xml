<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'ANNÉE MAUDITE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="GRS">
					<name>
						<forename>Charles</forename>
						<surname>GRANDSARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2146 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'ANNÉE MAUDITE 1870-1871</title>
						<author>Charles Grandsard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61285325.r=GRANDSARD%20L%27ANN%C3%89E%20MAUDITE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'ANNÉE MAUDITE 1870-1871</title>
								<author>Charles Grandsard</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE DU PETIT JOURNAL</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRS19">
				<head type="main">LA GRANDE MÈRE</head>
				<lg n="1">
					<l n="1" num="1.1">« Eh quoi ! me disait-on, laissez-vous la souffrance</l>
					<l n="2" num="1.2">Broyer ainsi votre âme entre ses dents de fer ?</l>
					<l n="3" num="1.3">Faut-il, pour un malheur qui tombe sur la France,</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Faire de sa vie un enfer ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">« La terre a-t-elle donc, depuis l'heure fatale,</l>
					<l n="6" num="2.2">Couvert d'un voile noir sa fraîcheur, sa beauté ?</l>
					<l n="7" num="2.3">Et n'est-ce plus pour nous que la prodigue étale</l>
					<l n="8" num="2.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Les dons de sa fécondité ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">« Les épis aux grains d'or, que la faucille coupe,</l>
					<l n="10" num="3.2">Vont-ils nous refuser leur savoureux froment ?</l>
					<l n="11" num="3.3">Le cep ne fait-il plus couler dans notre coupe</l>
					<l n="12" num="3.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Les flots de son sang écumant ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">« Croyez-nous : le pays, guérissant sa blessure,</l>
					<l n="14" num="4.2">De ses prospérités va, reprendre le cours ;</l>
					<l n="15" num="4.3">Savourez les loisirs que la paix nous assure ;</l>
					<l n="16" num="4.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Jouissez : nos instants sont courts ! »</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Ainsi les insensés, caressant leur chimère,</l>
					<l n="18" num="5.2">Sur un monde en débris se couronnaient de fleurs,</l>
					<l n="19" num="5.3">Et rêvaient au plaisir, quand le corps de leur mère</l>
					<l n="20" num="5.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Gisait sur son lit de douleurs !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Mais je le savais bien, moi, que mon existence</l>
					<l n="22" num="6.2">A la tienne tenait par un lien puissant,</l>
					<l n="23" num="6.3">Que ma substance, à moi, n'était que ta substance,</l>
					<l n="24" num="6.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Que j'étais ta chair et ton sang !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Que l'une à l'autre, en toi, nos fibres enlacées</l>
					<l n="26" num="7.2">Ensemble éprouvaient tout : calme ou tressaillements ;</l>
					<l n="27" num="7.3">Qu'à ton esprit le mien empruntait ses pensées,</l>
					<l n="28" num="7.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Mon cœur, au tien, ses battements !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Aussi, dans tes beaux jours, quand par l'Europe entière</l>
					<l n="30" num="8.2">Ta force exubérante à flots se répandait,</l>
					<l n="31" num="8.3">Ma tête, à mon insu, se relevait, altière,</l>
					<l n="32" num="8.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Et la vie en moi débordait !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Mais depuis tes malheurs, depuis que sur la terre</l>
					<l n="34" num="9.2">Tu gîs, le flanc percé, le visage flétri,</l>
					<l n="35" num="9.3">Mon front blêmi se courbe, et mon sang, dans l'artère</l>
					<l n="36" num="9.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Avec lenteur coule, appauvri !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Oui ! le cœur de l'enfant, sur celui de la mère,</l>
					<l n="38" num="10.2">Palpite à l'unisson, triomphant ou navré ;</l>
					<l n="39" num="10.3">Je souffre, je languis de ton angoisse amère ;</l>
					<l n="40" num="10.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Si jamais tu meurs, je mourrai !</l>
				</lg>
			</div></body></text></TEI>