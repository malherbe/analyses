<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'ANNÉE MAUDITE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="GRS">
					<name>
						<forename>Charles</forename>
						<surname>GRANDSARD</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2146 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'ANNÉE MAUDITE 1870-1871</title>
						<author>Charles Grandsard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61285325.r=GRANDSARD%20L%27ANN%C3%89E%20MAUDITE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'ANNÉE MAUDITE 1870-1871</title>
								<author>Charles Grandsard</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE DU PETIT JOURNAL</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRS8">
				<head type="main">MALÉDICTION</head>
				<lg n="1">
					<l n="1" num="1.1">Parole sur celui qui dégrada la France !</l>
				</lg>
				<lg n="2">
					<l n="2" num="2.1">Parce qu'il a trompé la naïve espérance</l>
					<l n="3" num="2.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Que ce peuple avait mise en lui,</l>
					<l n="4" num="2.3">Et qu'il l'a saturé de honte et de souffrance,</l>
					<l n="5" num="2.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Quand il lui devait son appui ;</l>
				</lg>
				<lg n="3">
					<l n="6" num="3.1">Parce qu'il l'a poussé, faible et chargé d'entraves,</l>
					<l n="7" num="3.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Devant un ennemi si fort,</l>
					<l n="8" num="3.3">Que ce peuple guerrier, brave entre les plus braves,</l>
					<l n="9" num="3.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Fut vaincu presque sans effort ;</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1">Parce qu'il attira le massacre sauvage</l>
					<l n="11" num="4.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Sur les populeuses cités,</l>
					<l n="12" num="4.3">Et qu'il fit promener la flamme et le ravage</l>
					<l n="13" num="4.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Dans nos sillons partout vantés ;</l>
				</lg>
				<lg n="5">
					<l n="14" num="5.1">Parce que, grâce à lui, dans la grande patrie</l>
					<l n="15" num="5.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Le fer tranche un large lambeau,</l>
					<l n="16" num="5.3">Et que, dès ce moment, mutilée et meurtrie,</l>
					<l n="17" num="5.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Elle est mûre pour le tombeau ;</l>
				</lg>
				<lg n="6">
					<l n="18" num="6.1">Parce qu'il l'a plongée avec indifférence</l>
					<l n="19" num="6.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Si bas dans la honte et l'affront,</l>
					<l n="20" num="6.3">Que cette nation, qui s'appelle la France,</l>
					<l n="21" num="6.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Ose à peine lever le front,</l>
				</lg>
				<lg n="7">
					<l n="22" num="7.1">Qu'il soit maudit ! Qu'il aille en vagabond sur terre !</l>
					<l n="23" num="7.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Et lui, jadis le chef des preux,</l>
					<l n="24" num="7.3">Qu'il suive avec les siens son chemin solitaire,</l>
					<l n="25" num="7.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Objet d'horreur comme un lépreux !</l>
				</lg>
				<lg n="8">
					<l n="26" num="8.1">Que sa grande victime, en chacun de ses rêves,</l>
					<l n="27" num="8.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Surgisse, fantôme sanglant,</l>
					<l n="28" num="8.3">Entr'ouvre son linceul, et lui montre sans trêves,</l>
					<l n="29" num="8.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Du doigt, sa large plaie au flanc !</l>
				</lg>
				<lg n="9">
					<l n="30" num="9.1">Dans ses fastes vengeurs, que l'implacable histoire</l>
					<l n="31" num="9.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Le marque d'un fer rouge au front ;</l>
					<l n="32" num="9.3">Et que de l'avenir l'arrêt expiatoire</l>
					<l n="33" num="9.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Le range au dessous de Néron !</l>
				</lg>
				<lg n="10">
					<l n="34" num="10.1">Enfin, qu'il lui soit fait comme au doge parjure !</l>
					<l n="35" num="10.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Sur l'image de Faliero</l>
					<l n="36" num="10.3">On mit un crêpe, avec cette sanglante injure :</l>
					<l n="37" num="10.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Mort sous la hache du bourreau ! »</l>
				</lg>
				<lg n="11">
					<l n="38" num="11.1">Lui, qu'on mette à son rang parmi les rois de France,</l>
					<l n="39" num="11.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Au lieu de son buste proscrit,</l>
					<l n="40" num="11.3">Un drap noir, monument de deuil et de souffrance ;</l>
					<l n="41" num="11.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Et qu'au dessous il soit écrit :</l>
				</lg>
				<lg n="12">
					<l n="42" num="12.1">« Napoléon… celui qui dégrada la France ! »</l>
				</lg>
			</div></body></text></TEI>