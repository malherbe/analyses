<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES DOULEURS DE LA GUERRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VRM">
					<name>
						<forename>Louis-Lucien</forename>
						<surname>VERMEIL</surname>
					</name>
					<date from="1833" to="1901">1833-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>620 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VRM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES DOULEURS DE LA GUERRE</title>
						<author>LOUIS-LUCIEN VERMEIL</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=l7xDAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES DOULEURS DE LA GUERRE</title>
								<author>LOUIS-LUCIEN VERMEIL</author>
								<imprint>
									<pubPlace>LAUSANNE</pubPlace>
									<publisher>LIBRAIRIE BLANC IMER et LEBET</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VRM3">
				<head type="number">III</head>
				<head type="main">LE BIVAC</head>
				<lg n="1">
					<l n="1" num="1.1">C'est l'heure du bivac ; quelque poutre enflammée</l>
					<l n="2" num="1.2">Réchauffe les soldats de cette grande armée.</l>
					<l n="3" num="1.3">Un bon feu fait du bien. On parle des combats ;</l>
					<l n="4" num="1.4">Pas un mot des absents, il faudrait parler bas.</l>
					<l n="5" num="1.5">Les hauts faits, les exploits, les actes de bravoure :</l>
					<l n="6" num="1.6">Il suffit de conter,… un cercle vous entoure.</l>
					<l n="7" num="1.7">On s'anime au récit de ce bon vétéran</l>
					<l n="8" num="1.8">Qui met l'entrain partout et le silence au rang.</l>
					<l n="9" num="1.9">En l'écoutant ainsi plus d'un conscrit s'enflamme ;</l>
					<l n="10" num="1.10">La Vaillance et l'ardeur renaissent dans son âme.</l>
					<l n="11" num="1.11">« Ceux qui tûront le mieux et ne broncheront pas</l>
					<l n="12" num="1.12">« Auront la croix d'honneur !… Conscrits, marchez au pas ! »</l>
					<l n="13" num="1.13">On boit, on s'étourdit et la chanson joyeuse</l>
					<l n="14" num="1.14">Vient dérider les fronts et rend l'âme oublieuse.</l>
					<l n="15" num="1.15">On plaisante et l'on rit ; mais la mère et l'enfant</l>
					<l n="16" num="1.16">Pleurent sous leur vieux toit, de sanglots étouffant !</l>
					<l n="17" num="1.17">On plaisante et l'on rit ; mais là dans les coins sombres</l>
					<l n="18" num="1.18">Gisent des corps sanglants au milieu des décombres !</l>
					<l n="19" num="1.19">On plaisante et l'on rit ; — mais peut être demain</l>
					<l n="20" num="1.20">D'autres riront pour vous !Sort barbare, inhumain !</l>
				</lg>
			</div></body></text></TEI>