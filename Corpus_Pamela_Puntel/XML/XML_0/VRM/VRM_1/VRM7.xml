<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES DOULEURS DE LA GUERRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VRM">
					<name>
						<forename>Louis-Lucien</forename>
						<surname>VERMEIL</surname>
					</name>
					<date from="1833" to="1901">1833-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>620 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VRM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES DOULEURS DE LA GUERRE</title>
						<author>LOUIS-LUCIEN VERMEIL</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=l7xDAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES DOULEURS DE LA GUERRE</title>
								<author>LOUIS-LUCIEN VERMEIL</author>
								<imprint>
									<pubPlace>LAUSANNE</pubPlace>
									<publisher>LIBRAIRIE BLANC IMER et LEBET</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VRM7">
				<head type="number">VII</head>
				<head type="main">LES HORREURS DE LA GUERRE</head>
				<lg n="1">
					<l n="1" num="1.1">O siècle de progrès ! ô siècle de lumière !</l>
					<l n="2" num="1.2">Qui voulais rapprocher, sous ta grande bannière,</l>
					<l n="3" num="1.3">Les peuples réunis par les liens étroits</l>
					<l n="4" num="1.4">De la paix bienveillante et du respect des droits !</l>
					<l n="5" num="1.5">O toi, qui construisais un superbe édifice</l>
					<l n="6" num="1.6">Et qui gravais ces mots sur son haut frontispice :</l>
					<l n="7" num="1.7">« Honorons le travail, aimons la liberté,</l>
					<l n="8" num="1.8">» Et tous nous grandirons dans la fraternité ! »</l>
					<l n="9" num="1.9">Ton édifice, ô siècle, est tombé dans la poudre,</l>
					<l n="10" num="1.10">Détruit et consumé par des éclats de foudre.</l>
					<l n="11" num="1.11">Et l'on voit aujourd'hui, vers le tison fumant,</l>
					<l n="12" num="1.12">Tes enfants consternés d'un tel embrasement !</l>
				</lg>
				<lg n="2">
					<l n="13" num="2.1">O siècle vaniteux, fier de tes découvertes,</l>
					<l n="14" num="2.2">De tes inventions, de tes prodiges ! — Certes,</l>
					<l n="15" num="2.3">Tu ne t'attendais pas aux spectacles affreux</l>
					<l n="16" num="2.4">Que la guerre nous offre en ces jours malheureux.</l>
					<l n="17" num="2.5">Ce n'est pas seulement les mortelles alarmes,</l>
					<l n="18" num="2.6">L'épouvante, l'effroi, le sort cruel des armes,</l>
					<l n="19" num="2.7">La défaite, la honte, ou la captivité,</l>
					<l n="20" num="2.8">Le pillage et le vol… c'est la férocité !</l>
					<l n="21" num="2.9">La guerre, n'est-ce pas ? cruautés sans pareilles,</l>
					<l n="22" num="2.10">Des femmes, des enfants que l'on tue à Bazeilles ?</l>
					<l n="23" num="2.11">Des hommes fusillés, là, derrière un buisson,</l>
					<l n="24" num="2.12">Pour avoir défendu leur grange, leur maison ?</l>
				</lg>
				<lg n="3">
					<l n="25" num="3.1">Quand parmi les débris de quelque infanterie</l>
					<l n="26" num="3.2">Vient passer au galop la lourde artillerie,</l>
					<l n="27" num="3.3">Les chevaux bondissant, malgré tous leurs écarts,</l>
					<l n="28" num="3.4">Sont forcés de marcher sur des membres épars !</l>
					<l n="29" num="3.5">Que de gémissements étouffés sous la roue</l>
					<l n="30" num="3.6">Des caissons, des affûts, qu'on traîne dans la boue !</l>
					<l n="31" num="3.7">Oui, mon Dieu, des mourans, des blessés écrasés,</l>
					<l n="32" num="3.8">Des cadavres, enfin, sous des toits embrasés !</l>
				</lg>
				<lg n="4">
					<l n="33" num="4.1">Que la guerre, ô mon Dieu ! que la guerre est horrible !</l>
					<l n="34" num="4.2">Ah ! de tous les fléaux, c'est bien le plus terrible.</l>
					<l n="35" num="4.3">Il endurcit les cœurs et transforme nos mains</l>
					<l n="36" num="4.4">En instruments maudits, odieux, inhumains !</l>
					<l n="37" num="4.5">La guerre est un tissu d'ignoble barbarie,</l>
					<l n="38" num="4.6">De monstruosités et de sauvagerie.</l>
					<l n="39" num="4.7">La médaille ou la croix qu'on donne aux vétérans</l>
					<l n="40" num="4.8">Est un jeton menteur frappé par les tyrans !</l>
				</lg>
			</div></body></text></TEI>