<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES DOULEURS DE LA GUERRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VRM">
					<name>
						<forename>Louis-Lucien</forename>
						<surname>VERMEIL</surname>
					</name>
					<date from="1833" to="1901">1833-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>620 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VRM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES DOULEURS DE LA GUERRE</title>
						<author>LOUIS-LUCIEN VERMEIL</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=l7xDAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES DOULEURS DE LA GUERRE</title>
								<author>LOUIS-LUCIEN VERMEIL</author>
								<imprint>
									<pubPlace>LAUSANNE</pubPlace>
									<publisher>LIBRAIRIE BLANC IMER et LEBET</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VRM11">
				<head type="number">XI</head>
				<head type="main">LE SAUVEUR</head>
				<lg n="1">
					<l n="1" num="1.1">Depuis le sombre jour où Caïn sur la terre</l>
					<l n="2" num="1.2">Osa près d'un autel verser le sang d'un frère,</l>
					<l n="3" num="1.3">Oh ! que n'as-tu marqué d'un stigmate maudit</l>
					<l n="4" num="1.4">Le front du meurtrier, ou soldat, ou bandit !</l>
					<l n="5" num="1.5">Pourquoi le crime, ô Ciel ! pourquoi la haine atroce ?</l>
					<l n="6" num="1.6">Pourquoi la guerre, enfin, qui rend l'homme féroce ?</l>
					<l n="7" num="1.7">Pourquoi le fils d'Adam, dans un combat brutal,</l>
					<l n="8" num="1.8">Du tigre ou du lion se montre-t-il l'égal ?</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="2">
					<l n="9" num="2.1">L'homme altéré de sang est-il ta créature ?</l>
					<l n="10" num="2.2">Ou bien est-il un monstre au sein de la nature ?</l>
					<l n="11" num="2.3">L'as-tu fait, ô Seigneur, si méchant, si cruel,</l>
					<l n="12" num="2.4">Qu'on doute en le voyant d'un Dieu bon, paternel ?</l>
					<l n="13" num="2.5">Sont-ce bien là tes traits ? — Est-ce là ton image</l>
					<l n="14" num="2.6">Que ce guerrier farouche au milieu du carnage ? —</l>
					<l n="15" num="2.7">Réponds, réponds, Seigneur, à mes graves soupçons ;</l>
					<l n="16" num="2.8">Recueilli devant toi, j'écoute tes leçons.</l>
					<l n="17" num="2.9">L'obscurité se fait maintenant sur ma route :</l>
					<l n="18" num="2.10">Viens dissiper, mon Dieu, les ténèbres du doute !</l>
					<l n="19" num="2.11">Ta Parole le dit, ô Seigneur, Tout-Puissant :</l>
					<l n="20" num="2.12">« Ils ont les pieds légers pour répandre le sang ;</l>
					<l n="21" num="2.13">« D'ailleurs leur cœur est plein de ruse, d'injustice</l>
					<l n="22" num="2.14">« Et de méchanceté, de meurtres, d'artifice ;</l>
					<l n="23" num="2.15">« Leur esprit est couvert d'un voile très épais ;</l>
					<l n="24" num="2.16">« Car ils n'ont point connu le chemin de la paix ! »</l>
				</lg>
				<lg n="3">
					<l n="25" num="3.1">Ta Parole dit vrai, notre Dieu, notre Père !</l>
					<l n="26" num="3.2">Mais en toi maintenant toute mon âme espère.</l>
					<l n="27" num="3.3">Jésus a dû briser la tête du serpent,</l>
					<l n="28" num="3.4">Hydre horrible et toujours sur la terre rampant ;</l>
					<l n="29" num="3.5">Et bientôt le péché, la guerre et la souillure,</l>
					<l n="30" num="3.6">Soudain disparaîtront du sein de la nature.</l>
					<l n="31" num="3.7">Et l'on verra le Christ triomphant, glorieux,</l>
					<l n="32" num="3.8">Régner sur cette terre et sous ces nouveaux cieux,</l>
					<l n="33" num="3.9">Où doivent séjourner la paix et la justice !…</l>
					<l n="34" num="3.10">C'est le prix attendu de son grand sacrifice !</l>
					<l n="35" num="3.11">Espérons fermement en ce règne d'amour ;</l>
					<l n="36" num="3.12">Le Seigneur l'a promis ; il en connaît le jour !</l>
				</lg>
				<lg n="4">
					<l n="37" num="4.1">Ce jour, dès à présent j'en aperçois l'aurore,</l>
					<l n="38" num="4.2">Une faible clarté bien indécise encore ;</l>
					<l n="39" num="4.3">Mais qui grandit toujours, quand, humbles, nous croyons</l>
					<l n="40" num="4.4">En Jésus, le Sauveur ; ô les divins rayons !</l>
				</lg>
			</div></body></text></TEI>