<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES DOULEURS DE LA GUERRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VRM">
					<name>
						<forename>Louis-Lucien</forename>
						<surname>VERMEIL</surname>
					</name>
					<date from="1833" to="1901">1833-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>620 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VRM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES DOULEURS DE LA GUERRE</title>
						<author>LOUIS-LUCIEN VERMEIL</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=l7xDAAAAcAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES DOULEURS DE LA GUERRE</title>
								<author>LOUIS-LUCIEN VERMEIL</author>
								<imprint>
									<pubPlace>LAUSANNE</pubPlace>
									<publisher>LIBRAIRIE BLANC IMER et LEBET</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VRM10">
				<head type="number">X</head>
				<head type="main">L'HOMME ET DIEU</head>
				<lg n="1">
					<l n="1" num="1.1">Mon Dieu ! que d'orphelins, de veuves, de victimes,</l>
					<l n="2" num="1.2">De deuils et de douleurs, de souffrances intimes !</l>
					<l n="3" num="1.3">Devant un tel fléau, le premier des bienfaits,</l>
					<l n="4" num="1.4">Pour les deux nations, c'est aujourd'hui la paix !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">En toi, Dieu des humains, tout notre espoir se fonde ;</l>
					<l n="6" num="2.2">Ah ! sous des flots de sang, laisserais-tu ce monde ?…</l>
					<l n="7" num="2.3">Viens à nous, sans tarder, après tant de vautours,</l>
					<l n="8" num="2.4">Colombe de la paix, pour consoler nos jours !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">O vous, anges des cieux, un voile de tristesse</l>
					<l n="10" num="3.2">N'a-t-il pas obscurci votre sainte allégresse ?</l>
					<l n="11" num="3.3">Avez-Vous pu chanter, sans des pleurs dans la Voix,</l>
					<l n="12" num="3.4">Vos cantiques divins au pied du Roi des rois ?</l>
					<l n="13" num="3.5">Avez-vous pu chanter Vos louanges sublimes,</l>
					<l n="14" num="3.6">Quand triomphaient ici les haines et les crimes ?</l>
					<l n="15" num="3.7">Quand la terre est en deuil, le ciel est-il joyeux ?</l>
					<l n="16" num="3.8">Avez-vous pu chanter la gloire des saints lieux ?…</l>
					<l n="17" num="3.9">Je le demande encor, vous les justes, les anges,</l>
					<l n="18" num="3.10">Et vous les Séraphins des célestes phalanges,</l>
					<l n="19" num="3.11">Devant tant de malheurs, n'étiez-Vous pas émus ?</l>
					<l n="20" num="3.12">N'avez-vous pas pleuré, comme autrefois Jésus ?</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="4">
					<l n="21" num="4.1">Aux demeures d'en-haut, la nombreuse assemblée</l>
					<l n="22" num="4.2">Par le cri des humains ne peut être troublée.</l>
					<l n="23" num="4.3">Dans les splendeurs du ciel tous les sens sont ravis,</l>
					<l n="24" num="4.4">Et le bruit des combats n'atteint pas ses parvis !</l>
					<l n="25" num="4.5">Il faut chanter d'ailleurs pour accueillir les frères</l>
					<l n="26" num="4.6">Tombés sous les drapeaux des luttes meurtrières.</l>
					<l n="27" num="4.7">Il faut chanter pour eux les hymnes de la paix</l>
					<l n="28" num="4.8">Auprès du Dieu d'amour qu'on bénit à jamais !</l>
					<l n="29" num="4.9">Si l'on chante déjà pour les âmes glanées,</l>
					<l n="30" num="4.10">On doit chanter bien plus pour celles moissonnées !</l>
					<l n="31" num="4.11">Qu'elle est douce la paix après tant de combats,</l>
					<l n="32" num="4.12">Et que sa palme est belle en la main des soldats !!!</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">O Dieu de charité, d'amour et de justice,</l>
					<l n="34" num="5.2">O Toi, qui ne veux pas que l'orphelin périsse,</l>
					<l n="35" num="5.3">Toi, le Dieu de la veuve et du pauvre l'appui,</l>
					<l n="36" num="5.4">Écoute qui t'invoque et l'exauce aujourd'hui !</l>
					<l n="37" num="5.5">Vois la mère qui prie et son enfant qui pleure</l>
					<l n="38" num="5.6">Près du sombre foyer de leur triste demeure ;</l>
					<l n="39" num="5.7">Oh ! laisses-toi fléchir par leurs gémissements ;</l>
					<l n="40" num="5.8">Ils sont à tes genoux, apaise leurs tourments !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">L'obscurité se fait sur cette pauvre terre,</l>
					<l n="42" num="6.2">Quand la bonté de Dieu devient un grand mystère,</l>
					<l n="43" num="6.3">Quand pour le pénétrer notre esprit se confond</l>
					<l n="44" num="6.4">Et qu'on se penche en vain pour en trouver le fond.</l>
					<l n="45" num="6.5">L'obscurité se fait quand soudain, dans notre âme,</l>
					<l n="46" num="6.6">S'éteint en vacillant une divine flamme,</l>
					<l n="47" num="6.7">La flamme de la foi, l'amour de l'infini,</l>
					<l n="48" num="6.8">Sans lequel, ici-bas, notre cœur est terni.</l>
					<l n="49" num="6.9">L'obscurité se fait, quand une main fatale</l>
					<l n="50" num="6.10">Abuse du pouvoir de la force brutale ;</l>
					<l n="51" num="6.11">Quand l'orgueil du vainqueur et le crime odieux</l>
					<l n="52" num="6.12">Se suivent le front haut sous la Voûte des cieux !</l>
					<l n="53" num="6.13">Brillez au firmament ! brillez pures étoiles !</l>
					<l n="54" num="6.14">Car la nuit du péché répand sur nous ses voiles.</l>
					<l n="55" num="6.15">Pour que l'astre scintille, il faut l'obscurité,</l>
					<l n="56" num="6.16">Et le ciel aussitôt resplendit de clarté !</l>
				</lg>
			</div></body></text></TEI>