<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TRIOLETS</title>
				<title type="sub_1">Poème publié dans le journal LE FIGARO (1870-1871)</title>
				<title type="medium">Édition électronique</title>
				<author key="MIL">
					<name>
						<forename>Albert</forename>
						<surname>MILLAUD</surname>
					</name>
					<date from="1844" to="1892">1844-1892</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>200 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>UN VIEUX PRUSSIEN À UN JEUNE CONSCRIT</title>
						<author>ALBERT MILLAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2720209/f2.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Figaro : journal non politique</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro</publisher>
									<date when="1870">22 octobre 1870</date>
								</imprint>
								<biblScope unit="issue">295</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TRIOLETS</title>
						<author>ALBERT MILLAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k272023f/f2.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Figaro : journal non politique</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro</publisher>
									<date when="1870">25 octobre 1870</date>
								</imprint>
								<biblScope unit="issue">298</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TRIOLETS</title>
						<author>ALBERT MILLAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI"> https://gallica.bnf.fr/ark:/12148/bpt6k272090q.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Figaro : journal non politique</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro</publisher>
									<date when="1870">1er janvier 1871</date>
								</imprint>
								<biblScope unit="issue">1</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1870-1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="MIL1">
				<head type="main">UN VIEUX PRUSSIEN À UN JEUNE CONSCRIT</head>
				<lg n="1">
					<l n="1" num="1.1">« O meine liebe Karl écoute,</l>
					<l n="2" num="1.2">Écoute et ne sois point surpris.</l>
					<l n="3" num="1.3">Ça ne fait plus l'ombre d'un doute,</l>
					<l n="4" num="1.4">Nous allons entres dans Paris.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">» J'y suis entré déjà moi-même</l>
					<l n="6" num="2.2">En mil huit cent quinze autrefois.</l>
					<l n="7" num="2.3">C'est un songe ! c'est un poëme !</l>
					<l n="8" num="2.4">Ouvre l'oreille : écoute et vois.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">» A quelques pas des Tuileries</l>
					<l n="10" num="3.2">Je te mènerai, pour régal,</l>
					<l n="11" num="3.3">Tout d'abord, voir les galeries</l>
					<l n="12" num="3.4">De bois, dans le Palais-Royal.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">» Sous ces arcades colossales</l>
					<l n="14" num="4.2">Qui s'ouvrent sur des frais jardins,</l>
					<l n="15" num="4.3">Nous fumerons quelques cigales</l>
					<l n="16" num="4.4">En observant les muscadins.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">» Nous y verrons, lèvre mi-close,</l>
					<l n="18" num="5.2">Des femmes chanter sans émoi</l>
					<l n="19" num="5.3">Sur la harpe : « Bouton de rose !</l>
					<l n="20" num="5.4">Tu seras plus heureux que moi !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">» C'est un air qu'on chante en famille,</l>
					<l n="22" num="6.2">Après quoi tu visiteras</l>
					<l n="23" num="6.3">L'éléphant noir de la Bastille :</l>
					<l n="24" num="6.4">— Il est en bois et plein de rats !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">» Nous verrons encor d'autres femmes</l>
					<l n="26" num="7.2">A Tivoli, bal vénéré.</l>
					<l n="27" num="7.3">Ah ! quels cancans nous y pinçames</l>
					<l n="28" num="7.4">Avec la reine Pomaré !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">» Cherchant des loisirs moins folâtres,</l>
					<l n="30" num="8.2">Nous irons, si ça t'est égal</l>
					<l n="31" num="8.3">Voir jouer dans les grands théâtres</l>
					<l n="32" num="8.4">Les ouvrages de Lancival.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">» Je me souviens de quelque bribe</l>
					<l n="34" num="9.2">Des vieilles chansons qu'inventait</l>
					<l n="35" num="9.3">A cette époque, un nommé Scribe,</l>
					<l n="36" num="9.4">Jeune écrivain qui promettait.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">» Tes souliers sont très ridicules :</l>
					<l n="38" num="10.2">Nous irons donc chez Sakowski,</l>
					<l n="39" num="10.3">Et nous verrons aux Funambules</l>
					<l n="40" num="10.4">Travailler madame Saqui.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">» Nous ferons avec du champagne</l>
					<l n="42" num="11.2">Chez Ramponneau plus d'un festin,</l>
					<l n="43" num="11.3">Et nous irons à la campagne</l>
					<l n="44" num="11.4">Par le coucou du Plat-d’étain.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">» Tu verras, ô jeune recrue,</l>
					<l n="46" num="12.2">Tu verras certaine beauté</l>
					<l n="47" num="12.3">Que je courtisais dans la rue</l>
					<l n="48" num="12.4">Du Doyenné… O volupté !…</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">» Amours ! voluptés et rasades !</l>
					<l n="50" num="13.2">C'était le deux août ! Je m'entend…</l>
					<l n="51" num="13.3">— Nous sommes dix-sept camarades</l>
					<l n="52" num="13.4">Qui pouvons tous t'en dire autant… »</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">— Ainsi parla la vieille bête…</l>
					<l n="54" num="14.2">Le jeune conscrit, tout surpris,</l>
					<l n="55" num="14.3">Répondit à la chansonnette</l>
					<l n="56" num="14.4">En tendant le doigt vers Paris :</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">» Et ce rempart qui nous menace,</l>
					<l n="58" num="15.2">Et ces canons qu'on voit là-bas,</l>
					<l n="59" num="15.3">Et qu'on charge par la culasse,</l>
					<l n="60" num="15.4">Sergent, tu ne m'en parles pas !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">» Et ces marins au tir habiles,</l>
					<l n="62" num="16.2">Ces clous perçants, au fer pointu,</l>
					<l n="63" num="16.3">Et ces gardes, et ces mobiles,</l>
					<l n="64" num="16.4">Dis-moi, soldat, t'en souviens-tu ?</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">» Dans l'histoire que tu m'as faite,</l>
					<l n="66" num="17.2">Tout cela n'est donc pas compris ?</l>
					<l n="67" num="17.3">— C'est vrai, reprit la vieille bête,</l>
					<l n="68" num="17.4">On m'a changé mon vieux Paris ! »</l>
				</lg>
				<closer>
					<signed>Albert Millaud.</signed>
				</closer>
			</div></body></text></TEI>