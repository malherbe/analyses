<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TRIOLETS</title>
				<title type="sub_1">Poème publié dans le journal LE FIGARO (1870-1871)</title>
				<title type="medium">Édition électronique</title>
				<author key="MIL">
					<name>
						<forename>Albert</forename>
						<surname>MILLAUD</surname>
					</name>
					<date from="1844" to="1892">1844-1892</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>200 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>UN VIEUX PRUSSIEN À UN JEUNE CONSCRIT</title>
						<author>ALBERT MILLAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2720209/f2.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Figaro : journal non politique</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro</publisher>
									<date when="1870">22 octobre 1870</date>
								</imprint>
								<biblScope unit="issue">295</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TRIOLETS</title>
						<author>ALBERT MILLAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k272023f/f2.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Figaro : journal non politique</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro</publisher>
									<date when="1870">25 octobre 1870</date>
								</imprint>
								<biblScope unit="issue">298</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TRIOLETS</title>
						<author>ALBERT MILLAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI"> https://gallica.bnf.fr/ark:/12148/bpt6k272090q.item</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>Figaro : journal non politique</title>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Le Figaro</publisher>
									<date when="1870">1er janvier 1871</date>
								</imprint>
								<biblScope unit="issue">1</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1870-1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="MIL2">
				<head type="main">TRIOLETS</head>
				<lg n="1">
					<l n="1" num="1.1">Quel fantaisiste que Nadar</l>
					<l n="2" num="1.2">Aéronaute ou photographe !</l>
					<l n="3" num="1.3">Il est aussi fort que Godard :</l>
					<l n="4" num="1.4">Quel fantaisiste que Nadar !</l>
					<l n="5" num="1.5">Mais, entre nous, Nadar n'a d'art</l>
					<l n="6" num="1.6">Que pour écrire son paraphe…</l>
					<l n="7" num="1.7">Quel fantaisiste quo Nadar</l>
					<l n="8" num="1.8">Aéronaute ou photographe !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Pour vous enlever le ballon</l>
					<l n="10" num="2.2">Plein de délice est son hélice.</l>
					<l n="11" num="2.3">Il a bien tombé l'Aquilon,</l>
					<l n="12" num="2.4">Pour vous enlever le ballon.</l>
					<l n="13" num="2.5">Avec sa casquette à gallon</l>
					<l n="14" num="2.6">Il rappelle l'amiral suisse.</l>
					<l n="15" num="2.7">Pour vous enlever le ballon</l>
					<l n="16" num="2.8">Plein de délice est son hélice !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Dans le royaume des oiseaux</l>
					<l n="18" num="3.2">Il est fort bon aéronaute.</l>
					<l n="19" num="3.3">Si Gambetta (Je plains ses os)</l>
					<l n="20" num="3.4">Dans le royaume des oiseaux</l>
					<l n="21" num="3.5">Ne se rompit pas en morceaux,</l>
					<l n="22" num="3.6">De Nadar ce n'est point la faute…</l>
					<l n="23" num="3.7">Dans le royaume des oiseaux,</l>
					<l n="24" num="3.8">Il est fort bon aéronaute.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">A Ferrière, au-dessus du parc,</l>
					<l n="26" num="4.2">Le voici qui franchit l'espace.</l>
					<l n="27" num="4.3">On dirait la flèche d'un arc</l>
					<l n="28" num="4.4">A Ferrière au-dessus du parc,</l>
					<l n="29" num="4.5">Et Guillaume dit à Bismarck :</l>
					<l n="30" num="4.6">— Tais-toi ! voici Nadar qui passe.</l>
					<l n="31" num="4.7">A Ferrière, au-dessus du parc,</l>
					<l n="32" num="4.8">Le voici qui franchit l'espace.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Toi, plus crépu que Clodion,</l>
					<l n="34" num="5.2">O toi qui vas porter ma lettre :</l>
					<l n="35" num="5.3">Nous ferons, par souscription,</l>
					<l n="36" num="5.4">Toi, plus crépu que Clodion,</l>
					<l n="37" num="5.5">Ta statue en collodion,</l>
					<l n="38" num="5.6">Au-dessus d'un grand gazomètre.</l>
					<l n="39" num="5.7">Toi, plus crépu que Clodion,</l>
					<l n="40" num="5.8">O toi qui vas porter ma lettre !</l>
				</lg>
				<closer>
					<signed>Albert Millaud.</signed>
				</closer>
			</div></body></text></TEI>