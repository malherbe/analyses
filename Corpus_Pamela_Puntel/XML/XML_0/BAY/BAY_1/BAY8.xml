<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA FRONTIÈRE</title>
				<title type="sub">ESSAIS DE POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BAY">
					<name>
						<forename>Hippolyte</forename>
						<surname>BAYE</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>864 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BAY_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FRONTIÈRE. ESSAIS DE POÉSIES</title>
						<author>BAYE, HIPPOLYTE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54640834.r=BAYE%2C%20HIPPOLYTE%20ESSAIS%20DE%20PO%C3%89SIES%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA FRONTIÈRE. ESSAIS DE POÉSIES</title>
								<author>BAYE, HIPPOLYTE</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE LACROIX, VERBOECKHOVËN ET CIE ÉDITEURS</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAY8">
				<head type="main">LA VOIX DES RUINES</head>
				<lg n="1">
					<l n="1" num="1.1">Des murs flottants comme des ombres ;</l>
					<l n="2" num="1.2">Des poutres, des fers, des tronçons ;</l>
					<l n="3" num="1.3">Le silence sur ces décombres :</l>
					<l n="4" num="1.4">Plus de meunier, plus de chansons.</l>
					<l n="5" num="1.5">Là-bas, une ronce ennemie</l>
					<l n="6" num="1.6">Enlace l'ailette endormie</l>
					<l n="7" num="1.7">Et-sur les monceaux va grimpant</l>
					<l n="8" num="1.8">Au lieu du babil de la roue,</l>
					<l n="9" num="1.9">Un filet d'eau, rauque, s'enroue,</l>
					<l n="10" num="1.10">Troublé par le lézard rampant.</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1"><space unit="char" quantity="4"/>Moulin, meunier, meunière,</l>
					<l n="12" num="2.2"><space unit="char" quantity="4"/>Qui donc vous a fait taire</l>
					<l n="13" num="2.3"><space unit="char" quantity="4"/>Pour toujours à la fois ?</l>
					<l n="14" num="2.4"><space unit="char" quantity="4"/>C'est encore la guerre,</l>
					<l n="15" num="2.5"><space unit="char" quantity="4"/>Invention des rois.</l>
				</lg>
				<lg n="3">
					<l n="16" num="3.1">Deux princes, sinistres comètes,</l>
					<l n="17" num="3.2">Ici, se heurtèrent un jour.</l>
					<l n="18" num="3.3">Le chaos, roulant sur nos têtes,</l>
					<l n="19" num="3.4">Ébranla cet obscur séjour.</l>
					<l n="20" num="3.5">Voués à la grande hécatombe,</l>
					<l n="21" num="3.6">Les époux dans la même tombe</l>
					<l n="22" num="3.7">Tombèrent frappés du canon ;</l>
					<l n="23" num="3.8">Et lorsque s'exhalait leur âme,</l>
					<l n="24" num="3.9">Chez eux, par le fer et la flamme,</l>
					<l n="25" num="3.10">Un conquérant gravait son nom.</l>
				</lg>
				<lg n="4">
					<l n="26" num="4.1"><space unit="char" quantity="4"/>Ce fléau séculaire</l>
					<l n="27" num="4.2"><space unit="char" quantity="4"/>Dans sa seule colère</l>
					<l n="28" num="4.3"><space unit="char" quantity="4"/>Puise en tout temps ses droits.</l>
					<l n="29" num="4.4"><space unit="char" quantity="4"/>Tout broyer par la guerre,</l>
					<l n="30" num="4.5"><space unit="char" quantity="4"/>C'est le plaisir des rois.</l>
				</lg>
				<lg n="5">
					<l n="31" num="5.1">Quelle demeure hospitalière !</l>
					<l n="32" num="5.2">De la main qui les nourrissait,</l>
					<l n="33" num="5.3">Des oiseaux l'aile familière</l>
					<l n="34" num="5.4">Ne craignait fusil ni lacet.</l>
					<l n="35" num="5.5">Où donc es-tu, troupe infidèle ?</l>
					<l n="36" num="5.6">Quoi ! déjà la tendre hirondelle</l>
					<l n="37" num="5.7">A d'autres murs porte son nid.</l>
					<l n="38" num="5.8">Oui, la meunière inconsolée</l>
					<l n="39" num="5.9">Ne Verrait plus dans la vallée</l>
					<l n="40" num="5.10">Les pigeons qu'elle y réunit.</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><space unit="char" quantity="4"/>Cette tribu légère,</l>
					<l n="42" num="6.2"><space unit="char" quantity="4"/>De la paix messagère,</l>
					<l n="43" num="6.3"><space unit="char" quantity="4"/>Se plonge au fond des bois.</l>
					<l n="44" num="6.4"><space unit="char" quantity="4"/>Car elle hait la guerre</l>
					<l n="45" num="6.5"><space unit="char" quantity="4"/>Et les foudres des rois.</l>
				</lg>
				<lg n="7">
					<l n="46" num="7.1">Mon enfance, qui fut voisine</l>
					<l n="47" num="7.2">Du chaume plus que du château,</l>
					<l n="48" num="7.3">Reçut du meunier sans lésine</l>
					<l n="49" num="7.4">Fraîches cerises, blanc gâteau.</l>
					<l n="50" num="7.5">Comment payer ma vieille joie ?</l>
					<l n="51" num="7.6">Paix est due à celui qui choie</l>
					<l n="52" num="7.7">Tout être humain faible ou petit.</l>
					<l n="53" num="7.8">Mais quand la force partout gronde,</l>
					<l n="54" num="7.9">Le bien semé parmi le monde</l>
					<l n="55" num="7.10">Ne sauve ni ne garantit.</l>
				</lg>
				<lg n="8">
					<l n="56" num="8.1"><space unit="char" quantity="4"/>Ils sont là sous la terre</l>
					<l n="57" num="8.2"><space unit="char" quantity="4"/>De ce pré solitaire</l>
					<l n="58" num="8.3"><space unit="char" quantity="4"/>Où se penche une croix.</l>
					<l n="59" num="8.4"><space unit="char" quantity="4"/>C'est l’œuvre de la guerre,</l>
					<l n="60" num="8.5"><space unit="char" quantity="4"/>C'est l'ouvrage des rois !</l>
				</lg>
				<lg n="9">
					<l n="61" num="9.1">Pauvres gens ! l'histoire abandonne</l>
					<l n="62" num="9.2">Vos noms à l'oubli détesté,</l>
					<l n="63" num="9.3">Elle qui tresse une couronne</l>
					<l n="64" num="9.4">Aux bourreaux de la Liberté.</l>
					<l n="65" num="9.5">Votre meurtre, nul ne l'expie.</l>
					<l n="66" num="9.6">Aux sons d'une fanfare impie,</l>
					<l n="67" num="9.7">Vos mânes frémissent encor.</l>
					<l n="68" num="9.8">— Juste Dieu ! cessez donc d'absoudre</l>
					<l n="69" num="9.9">Les criminels qui, noirs de poudre,</l>
					<l n="70" num="9.10">Tachent de sang leurs trônes d'or !</l>
				</lg>
				<lg n="10">
					<l n="71" num="10.1"><space unit="char" quantity="4"/>Faites sur notre sphère,</l>
					<l n="72" num="10.2"><space unit="char" quantity="4"/>Aux éclats du tonnerre,</l>
					<l n="73" num="10.3"><space unit="char" quantity="4"/>Descendre cette voix :</l>
					<l n="74" num="10.4"><space unit="char" quantity="4"/>« Maudite soit la guerre !</l>
					<l n="75" num="10.5"><space unit="char" quantity="4"/>« Et maudits tous les rois !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Novembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>