<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES PIGEONS DE LA RÉPUBLIQUE</title>
				<title type="sub">édition partielle du recueil : PENDANT LA GUERRE (1872)</title>
				<title type="medium">Édition électronique</title>
				<author key="MAN">
					<name>
						<forename>Eugène</forename>
						<surname>MANUEL</surname>
					</name>
					<date from="1823" to="1901">1823-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>690 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES PIGEONS DE LA RÉPUBLIQUE</title>
						<author>EUGÈNE MANUEL</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k6483232z.r=eug%C3%A8ne%20manuel?rk=128756;0</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PENDANT LA GUERRE</title>
								<author>EUGÈNE MANUEL</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>MICHEL LEVY FRERES, ÉDITEURS</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>édition partielle du recueil de poésies</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="MAN4">
				<head type="main">HENRI REGNAULT</head>
				<head type="form">POÉSIE</head>
				<head type="sub">Récitée à la Comédie-Française par M. COQUELIN<lb/>le 27 janvier 1871</head>
				<opener>
					<salute>
						A GEORGES CLAIRIN <lb/>
						PEINTRE <lb/>
						AMI DE HENRI REGNAULT <lb/>
						et son compagnon d'armes au combat de Buzenval <lb/>
						le 19 janvier 1871. <lb/>
						E. M.
					</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Ils lui disaient : « Allons ! viens ! quittons cette place !</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"/>Le clairon nous rallie en bas !</l>
					<l n="3" num="1.3">Contre ce mur d’airain que veux-tu que l’on fasse ?</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"/>Ils sont trop forts : on ne peut pas !</l>
					<l n="5" num="1.5">La retraite a sonné ; rentrons ! sur cette pente,</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"/>Assez de morts dorment ce soir.</l>
					<l n="7" num="1.7">La brume est plus épaisse, et la boue est sanglante :</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"/>Nous avons fait notre devoir !»</l>
					<l n="9" num="1.9">Mais lui, distrait et sombre, absorbé dans un rêve,</l>
					<l n="10" num="1.10"><space unit="char" quantity="8"/>A peine il entend ses amis.</l>
					<l n="11" num="1.11">« Partez ! laissez-moi seul, dit-il d’une voix brève.</l>
					<l n="12" num="1.12"><space unit="char" quantity="8"/>Je reviendrai : je l’ai promis… »</l>
					<l n="13" num="1.13">Il sent bondir en lui le cœur de la patrie,</l>
					<l n="14" num="1.14"><space unit="char" quantity="8"/>Et dans ses veines le sang bout.</l>
					<l n="15" num="1.15">Résolu, sans bravade et sans forfanterie,</l>
					<l n="16" num="1.16"><space unit="char" quantity="8"/>Il veut demeurer jusqu’au bout.</l>
					<l n="17" num="1.17">La rage sourde emplit son âme généreuse ;</l>
					<l n="18" num="1.18"><space unit="char" quantity="8"/>Un vague éclair sort de ses yeux ;</l>
					<l n="19" num="1.19">Et, pressant son fusil d’une étreinte fiévreuse,</l>
					<l n="20" num="1.20"><space unit="char" quantity="8"/>Il s’écarte silencieux.</l>
				</lg>
				<lg n="2">
					<l n="21" num="2.1">Lentement il gravit la pelouse, et, farouche,</l>
					<l n="22" num="2.2"><space unit="char" quantity="8"/>Sondant la profondeur des bois,</l>
					<l n="23" num="2.3">Il saisit à regret sa dernière cartouche</l>
					<l n="24" num="2.4"><space unit="char" quantity="8"/>Pour tirer encore une fois.</l>
					<l n="25" num="2.5">Ils l’appellent en vain : leurs voix jeunes et franches</l>
					<l n="26" num="2.6"><space unit="char" quantity="8"/>Se perdent le long du chemin ;</l>
					<l n="27" num="2.7">Les balles ont sifflé de nouveau dans les branches :</l>
					<l n="28" num="2.8"><space unit="char" quantity="8"/>Quelqu’un manquait le lendemain !</l>
				</lg>
				<lg n="3">
					<l n="29" num="3.1">Quelqu’un ! — Le plomb stupide et la mitraille infâme</l>
					<l n="30" num="3.2"><space unit="char" quantity="8"/>Pourraient faucher un siècle encor,</l>
					<l n="31" num="3.3">Avant de nous ravir deux fois une telle âme,</l>
					<l n="32" num="3.4"><space unit="char" quantity="8"/>Et deux fois un pareil trésor !</l>
					<l n="33" num="3.5">Qui que tu sois, posté derrière u tronc de chêne,</l>
					<l n="34" num="3.6"><space unit="char" quantity="8"/>Ou qu’un mur crénelé masquait,</l>
					<l n="35" num="3.7">Vainqueur obscur, qui tins une minute à peine</l>
					<l n="36" num="3.8"><space unit="char" quantity="8"/>Sa tête au bout de ton mousquet ;</l>
					<l n="37" num="3.9">Toi qui n’auras été qu’une inepte matière,</l>
					<l n="38" num="3.10"><space unit="char" quantity="8"/>Un aveugle instrument de mort,</l>
					<l n="39" num="3.11">Sans quoi l’éternité, — sache-le,— tout entière</l>
					<l n="40" num="3.12"><space unit="char" quantity="8"/>Serait trop peu pour ton remord ;</l>
					<l n="41" num="3.13">Maudit sois-tu, soldat, toi, ton peuple et la guerre,</l>
					<l n="42" num="3.14"><space unit="char" quantity="8"/>Et ton vieux roi tout le premier,</l>
					<l n="43" num="3.15">Puisqu’il n’aura fallu qu’un paysan vulgaire,</l>
					<l n="44" num="3.16"><space unit="char" quantity="8"/>Fils de l’étable et du fumier,</l>
					<l n="45" num="3.17">Quelque bouvier pétri pour les œuvres serviles,</l>
					<l n="46" num="3.18"><space unit="char" quantity="8"/>Marchant sous la crosse et les coups,</l>
					<l n="47" num="3.19">Un balayeur peut-être échappé de nos villes,</l>
					<l n="48" num="3.20"><space unit="char" quantity="8"/>Encor puant de nos égouts,</l>
					<l n="49" num="3.21">Pour trouver au hasard, bêtement, cette face,</l>
					<l n="50" num="3.22"><space unit="char" quantity="8"/>Comme par un défi moqueur,</l>
					<l n="51" num="3.23">Pour trancher dans sa sève abondante et vivace</l>
					<l n="52" num="3.24"><space unit="char" quantity="8"/>Tout ce génie et tout ce cœur,</l>
					<l n="53" num="3.25">Étouffer à son aube une lueur si pure,</l>
					<l n="54" num="3.26"><space unit="char" quantity="8"/>Éteindre un tel rayonnement,</l>
					<l n="55" num="3.27">Que la France mourante en ressent la blessure</l>
					<l n="56" num="3.28"><space unit="char" quantity="8"/>Jusque dans cet écoulement !</l>
				</lg>
				<lg n="4">
					<l n="57" num="4.1">Sais-tu ce que ton doigt, lâchant cette détente,</l>
					<l n="58" num="4.2"><space unit="char" quantity="8"/>A frappé dans l’ombre ? Sais-tu</l>
					<l n="59" num="4.3">Ce que ta main détruit de poésie ardente,</l>
					<l n="60" num="4.4"><space unit="char" quantity="8"/>D’intelligence et de vertu ?</l>
					<l n="61" num="4.5">Ah ! soyez donc de ceux que Dieu choisit lui-même,</l>
					<l n="62" num="4.6"><space unit="char" quantity="8"/>Et qu’il a marqués de son sceau ;</l>
					<l n="63" num="4.7">Que l’artiste charmé vous admire et vous aime ;</l>
					<l n="64" num="4.8"><space unit="char" quantity="8"/>Rendez fameux vote pinceau ;</l>
					<l n="65" num="4.9">Soyez plus qu’un espoir et plus qu’une promesse ;</l>
					<l n="66" num="4.10"><space unit="char" quantity="8"/>Ayez la force et la beauté,</l>
					<l n="67" num="4.11">Ayez toute la grâce et toute la jeunesse,</l>
					<l n="68" num="4.12"><space unit="char" quantity="8"/>Et tout l’avenir enchanté,</l>
					<l n="69" num="4.13">Pour qu’un soir il suffise à la brutale envie</l>
					<l n="70" num="4.14"><space unit="char" quantity="8"/>D’un goujat qui sait son métier,</l>
					<l n="71" num="4.15">De faire des : du coup il supprime une vie</l>
					<l n="72" num="4.16"><space unit="char" quantity="8"/>Qui va manquer au monde entier !</l>
					<l n="73" num="4.17">Pauvre enfant, il rêvait encor la délivrance ;</l>
					<l n="74" num="4.18"><space unit="char" quantity="8"/>Nos vœux brûlants étaient les siens ;</l>
					<l n="75" num="4.19">Et voilà pour adieu ce que te laisse, ô France,</l>
					<l n="76" num="4.20"><space unit="char" quantity="8"/>Le dernier plomb de ce Prussiens !</l>
				</lg>
				<lg n="5">
					<l n="77" num="5.1">Oh ! qu’il fut triste et noir le jour des funérailles !</l>
					<l n="78" num="5.2"><space unit="char" quantity="8"/>Va, tu fais bien d’être endormi</l>
					<l n="79" num="5.3">C’était l’heure où la faim désarmait nos murailles,</l>
					<l n="80" num="5.4"><space unit="char" quantity="8"/>Et nous courbait sous l’ennemi !</l>
					<l n="81" num="5.5">Paris était venu, près de ta fiancée,</l>
					<l n="82" num="5.6"><space unit="char" quantity="8"/>Au grave et sombre rendez-vous :</l>
					<l n="83" num="5.7">Chaque regard cachait une morne pensée,</l>
					<l n="84" num="5.8"><space unit="char" quantity="8"/>Faite de honte et de courroux.</l>
					<l n="85" num="5.9">Tous, les jeunes, les vieux, dans la foi, dans le doute,</l>
					<l n="86" num="5.10"><space unit="char" quantity="8"/>Nous méditions, le cœur navré ;</l>
					<l n="87" num="5.11">Et le De profundis qui montait vers la voûte</l>
					<l n="88" num="5.12"><space unit="char" quantity="8"/>Jamais n’avait ainsi pleuré ;</l>
					<l n="89" num="5.13">Car, en couvant des yeux cette bière drapée,</l>
					<l n="90" num="5.14"><space unit="char" quantity="8"/>Nous conduisions un autre deuil :</l>
					<l n="91" num="5.15">La patrie avec toi, du même coup frappée,</l>
					<l n="92" num="5.16"><space unit="char" quantity="8"/>Dormait aussi dans ton cercueil !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Paris, 25, janvier 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>