<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES PIGEONS DE LA RÉPUBLIQUE</title>
				<title type="sub">édition partielle du recueil : PENDANT LA GUERRE (1872)</title>
				<title type="medium">Édition électronique</title>
				<author key="MAN">
					<name>
						<forename>Eugène</forename>
						<surname>MANUEL</surname>
					</name>
					<date from="1823" to="1901">1823-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>690 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES PIGEONS DE LA RÉPUBLIQUE</title>
						<author>EUGÈNE MANUEL</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k6483232z.r=eug%C3%A8ne%20manuel?rk=128756;0</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PENDANT LA GUERRE</title>
								<author>EUGÈNE MANUEL</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>MICHEL LEVY FRERES, ÉDITEURS</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>édition partielle du recueil de poésies</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="MAN5">
				<head type="main">LES ABSENTS</head>
				<head type="form">POESIE</head>
				<head type="sub_2">Recitée par mademoiselle MARIE DELAPORTE<lb/>au festival donné à Paris, au bénéfice des victimes<lb/>de la guerre, le 6 août 1871.</head>
				<opener>
					<dateline>
						<date when="1871">AOÛT 1871.</date>
					</dateline>
					<salute> A MADEMOISELLE MARIE DELAPORTE.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Si vous les connaissez encore,</l>
					<l n="2" num="1.2">Tous ces absents, je n'en sais rien !</l>
					<l n="3" num="1.3">Ceux qui s'en vont, — je le crains bien, —</l>
					<l n="4" num="1.4">L'ombre les couvre : on les ignore !</l>
					<l n="5" num="1.5">La mémoire est faite d'oublis,</l>
					<l n="6" num="1.6">Et les souvenirs affaiblis</l>
					<l n="7" num="1.7">Sont comme un trajet de navire,</l>
					<l n="8" num="1.8">Sous les flots si vite effacé</l>
					<l n="9" num="1.9">Que nul regard ne saurait dire</l>
					<l n="10" num="1.10">Par où le sillage a passé !</l>
					<l n="11" num="1.11">Mais les voyageurs se souviennent,</l>
					<l n="12" num="1.12">Et des fils puissants les retiennent</l>
					<l n="13" num="1.13">Au doux pays qu'ils ont quitté.</l>
					<l n="14" num="1.14">Au loin, de l'invisible rive,</l>
					<l n="15" num="1.15">Chaque bruit sourd qui leur arrive</l>
					<l n="16" num="1.16">Est avidement écouté ;</l>
					<l n="17" num="1.17">Et si ce bruit, dont on tressaille,</l>
					<l n="18" num="1.18">C'est le canon de la bataille.</l>
					<l n="19" num="1.19">Même au foyer de l'étranger</l>
					<l n="20" num="1.20">Qui pour vous élargit la place,</l>
					<l n="21" num="1.21">On sent un frisson qui vous glace.</l>
					<l n="22" num="1.22">Et l'on voudrait tout partager,</l>
					<l n="23" num="1.23">L'honneur, la honte et le danger !</l>
				</lg>
				<lg n="2">
					<l n="24" num="2.1">Dans vos heures désespérées.</l>
					<l n="25" num="2.2">Songiez-vous, en luttant ici,</l>
					<l n="26" num="2.3">O pauvres âmes torturées,</l>
					<l n="27" num="2.4">Que vos absents souffraient aussi ?…</l>
					<l n="28" num="2.5">C'est leur offrande que j'apporte :</l>
					<l n="29" num="2.6">Et je voudrais ma voix plus forte,</l>
					<l n="30" num="2.7">Et mon geste plus douloureux,</l>
					<l n="31" num="2.8">Et ma plainte plus enflammée,</l>
					<l n="32" num="2.9">Pour te dire, ô ma mère aimée,</l>
					<l n="33" num="2.10">Quand mouraient tes fils généreux,</l>
					<l n="34" num="2.11">Combien de pleurs versés sur eux !</l>
				</lg>
				<lg n="3">
					<l n="35" num="3.1">Ah ! comme la France était fière,</l>
					<l n="36" num="3.2">Aux jours où pour nous la frontière</l>
					<l n="37" num="3.3">A l'horizon disparaissait !</l>
					<l n="38" num="3.4">Sa renommée encore entière</l>
					<l n="39" num="3.5">Dans son passé resplendissait !</l>
					<l n="40" num="3.6">Comme on la croyait forte et saine !</l>
					<l n="41" num="3.7">Qui nous eût dit que des uhlans</l>
					<l n="42" num="3.8">Les coursiers bientôt, dans la Seine,</l>
					<l n="43" num="3.9">Plongeraient leurs naseaux brûlants ;</l>
					<l n="44" num="3.10">Que, par d'étranges représailles,</l>
					<l n="45" num="3.11">On entendrait, ô vieux Versailles,</l>
					<l n="46" num="3.12">A nos désastres inouïs</l>
					<l n="47" num="3.13">Frémir l'écho du Jeu de paume ;</l>
					<l n="48" num="3.14">Et sur les marbres de Louis</l>
					<l n="49" num="3.15">Sonner l'éperon de Guillaume !</l>
				</lg>
				<lg n="4">
					<l n="50" num="4.1">Ces feuilles, qui portent au loin</l>
					<l n="51" num="4.2">Les cent bruits confus de la guerre,</l>
					<l n="52" num="4.3">Hélas ! si superbes naguère,</l>
					<l n="53" num="4.4">Nous les maudissions sans témoin !</l>
					<l n="54" num="4.5">Je m'abîmais longtemps, rêveuse,</l>
					<l n="55" num="4.6">Sur les récits de ces combats,</l>
					<l n="56" num="4.7">Qu'avait froissés ma main nerveuse !</l>
					<l n="57" num="4.8">J'essayais de n'y croire pas !</l>
				</lg>
				<lg n="5">
					<l n="58" num="5.1">Si loin de la terre natale,</l>
					<l n="59" num="5.2">Attendre ! attendre, sans savoir</l>
					<l n="60" num="5.3">La fin de la lutte fatale !</l>
					<l n="61" num="5.4">Se dire, en s'endormant, le soir,</l>
					<l n="62" num="5.5">Dans un sommeil plein de fantômes,</l>
					<l n="63" num="5.6">Durant des nuits qui sont des mois :</l>
					<l n="64" num="5.7">« Que font nos soldats dans les bois,</l>
					<l n="65" num="5.8">Et nos paysans sous les chaumes ?</l>
					<l n="66" num="5.9">Que font nos tristes prisonniers ?</l>
					<l n="67" num="5.10">Que font tant d'amis et de frères ?</l>
					<l n="68" num="5.11">Les champs, là-bas, sont des charniers</l>
					<l n="69" num="5.12">Tout semés de croix funéraires ! »</l>
				</lg>
				<lg n="6">
					<l n="70" num="6.1">On a pourtant bien combattu</l>
					<l n="71" num="6.2">Pour mériter la délivrance !</l>
					<l n="72" num="6.3">O ma France ! ma pauvre France !</l>
					<l n="73" num="6.4">Oh ! réponds-moi, que deviens-tu ?</l>
				</lg>
				<lg n="7">
					<l n="74" num="7.1">Vivre ainsi, non, ce n'est pas vivre !</l>
					<l n="75" num="7.2">Mais parlez donc !… On la délivre ?…</l>
					<l n="76" num="7.3">— Non, pas encore… — Ah ! cette fois</l>
					<l n="77" num="7.4">La chute serait trop profonde !</l>
					<l n="78" num="7.5">L'Europe a pris sa grande voix</l>
					<l n="79" num="7.6">Pour rendre l'équilibre au monde ?…</l>
					<l n="80" num="7.7">— Non ! L'Europe attendra la fin.</l>
					<l n="81" num="7.8">— Mais Paris meurt, Paris a faim,</l>
					<l n="82" num="7.9">Paris sent, ainsi qu'une serre,</l>
					<l n="83" num="7.10">S'enfoncer autour de son cou</l>
					<l n="84" num="7.11">L'ongle du terrible adversaire</l>
					<l n="85" num="7.12">Qui sur son corps tient le genou !</l>
					<l n="86" num="7.13">Paris va, d'un effort suprême,</l>
					<l n="87" num="7.14">Se redresser sur son cercueil,</l>
					<l n="88" num="7.15">Et, tout sanglant, et déjà blême,</l>
					<l n="89" num="7.16">Renverser l'ennemi du seuil ?</l>
					<l n="90" num="7.17">— Non ! — Mais tu gardes ton courage.</l>
					<l n="91" num="7.18">Après tant de rudes assauts,</l>
					<l n="92" num="7.19">France ? On doit redouter ta rage ?</l>
					<l n="93" num="7.20">Si ton épée est en morceaux,</l>
					<l n="94" num="7.21">Tu peux encore en faire usage ?</l>
				</lg>
				<lg n="8">
					<l n="95" num="8.1">Tous, citadins et campagnards,</l>
					<l n="96" num="8.2">Ils sauront, de leur main crispée,</l>
					<l n="97" num="8.3">Saisir, comme autant de poignards.</l>
					<l n="98" num="8.4">Les tronçons de ta vieille épée ?</l>
					<l n="99" num="8.5">— Non !… car le flot va grossissant ;</l>
					<l n="100" num="8.6">Le flot sanglant, de plaine en plaine,</l>
					<l n="101" num="8.7">Monte toujours, engloutissant</l>
					<l n="102" num="8.8">Devant lui la moisson humaine !</l>
					<l n="103" num="8.9">Inexorables bulletins !</l>
					<l n="104" num="8.10">Pour celle qu'on crut invincible,</l>
					<l n="105" num="8.11">Quoi ! pas un retour des destins ?</l>
					<l n="106" num="8.12">Non ? toujours non ? C'est impossible !</l>
				</lg>
				<lg n="9">
					<l n="107" num="9.1">J'ai compris combien je t'aimais :</l>
					<l n="108" num="9.2">Nul de vous ne saura jamais,</l>
					<l n="109" num="9.3">Ici, de quelle pointe aiguë</l>
					<l n="110" num="9.4">Au loin les cœurs sont traversés,</l>
					<l n="111" num="9.5">Quand on vous dit, les yeux baissés :</l>
					<l n="112" num="9.6">« Vous savez ?. La France est vaincue ! »</l>
				</lg>
				<lg n="10">
					<l n="113" num="10.1">Donnez-moi mes robes de deuil !</l>
					<l n="114" num="10.2">Un voile noir ! un voile sombre !</l>
					<l n="115" num="10.3">A nos hôtes fermez ce seuil :</l>
					<l n="116" num="10.4">Il faut se confiner dans l'ombre !</l>
					<l n="117" num="10.5">O douleurs feintes, taisez-vous !</l>
					<l n="118" num="10.6">Quand mon pays sous de tels coups</l>
					<l n="119" num="10.7">Tombe, et laisse échapper ses armes,</l>
					<l n="120" num="10.8">A lui seul mon culte sacré !</l>
					<l n="121" num="10.9">J'ai bien souvent versé des larmes,</l>
					<l n="122" num="10.10">Mais jamais je n'ai tant pleuré.</l>
				</lg>
				<lg n="11">
					<l n="123" num="11.1">Chère patrie au cœur blessée,</l>
					<l n="124" num="11.2">Comme on voudrait par la pensée</l>
					<l n="125" num="11.3">Accourir, impuissant soutien ;</l>
					<l n="126" num="11.4">Baiser ton front, quand tout s'écroule :</l>
					<l n="127" num="11.5">Homme, mêler son sang au tien,</l>
					<l n="128" num="11.6">Femme, laver ton sang qui coule !</l>
				</lg>
				<lg n="12">
					<l n="129" num="12.1">Ah ! partons, je veux la revoir !</l>
					<l n="130" num="12.2">Elle souffre : je veux moi-même</l>
					<l n="131" num="12.3">Auprès d'elle humblement m'asseoir,</l>
					<l n="132" num="12.4">Comme au chevet de ceux qu'on aime.</l>
					<l n="133" num="12.5">J'ai tout quitté pour le départ ;</l>
					<l n="134" num="12.6">J'ai franchi les monts et les fleuves,</l>
					<l n="135" num="12.7">Et de ses mortelles épreuves</l>
					<l n="136" num="12.8">Mon cœur aussi voulait sa part !</l>
					<l n="137" num="12.9">Pour toucher la terre sacrée,</l>
					<l n="138" num="12.10">Dieu ! qu'il paraît long, le chemin !</l>
					<l n="139" num="12.11">Comme la vue est attirée</l>
					<l n="140" num="12.12">Par delà l'horizon germain !</l>
					<l n="141" num="12.13">Comme on aspire au lendemain,</l>
					<l n="142" num="12.14">Sur ces plaines que l’œil dévore !</l>
				</lg>
				<lg n="13">
					<l n="143" num="13.1">« France ! France ! enfin, te voilà !</l>
					<l n="144" num="13.2">Salut, Rhin, que le soleil dore !… »</l>
					<l n="145" num="13.3">Soudain, mon regard se voila,</l>
					<l n="146" num="13.4">Quand on me dit : « Non, pas encore ! »</l>
					<l n="147" num="13.5">Pourtant, je répétai son nom,</l>
					<l n="148" num="13.6">Plus bas, avec quelle souffrance !</l>
					<l n="149" num="13.7">« Ah ! cette fois, c'est elle ! » — « Non ! » —</l>
					<l n="150" num="13.8">Et cependant, c'était la France !</l>
				</lg>
				<lg n="14">
					<l n="151" num="14.1">Salut enfin, salut, Paris !</l>
					<l n="152" num="14.2">Au flanc de toutes tes collines,</l>
					<l n="153" num="14.3">J'ai vu les funèbres débris,</l>
					<l n="154" num="14.4">J'ai vu la cendre et les ruines.</l>
					<l n="155" num="14.5">J'ai vu, dans la sombre cité,</l>
					<l n="156" num="14.6">Après tant de stériles peines,</l>
					<l n="157" num="14.7">L'effroyable complicité</l>
					<l n="158" num="14.8">Du désespoir et de la haine.</l>
					<l n="159" num="14.9">La France, lambeau par lambeau,</l>
					<l n="160" num="14.10">Tombait vaincue, — et criminelle :</l>
					<l n="161" num="14.11">Je l'ai vue au bord du tombeau,</l>
					<l n="162" num="14.12">Et j'ai cru mourir avec elle !</l>
				</lg>
				<lg n="15">
					<l n="163" num="15.1">Aujourd'hui, l'on peut repartir :</l>
					<l n="164" num="15.2">La guérison nous paraît sûre ;</l>
					<l n="165" num="15.3">Dieu même semble consentir</l>
					<l n="166" num="15.4">A cicatriser la blessure !</l>
					<l n="167" num="15.5">A nos absents qui sont là-bas,</l>
					<l n="168" num="15.6">Et dont l'âme vibre à distance,</l>
					<l n="169" num="15.7">J'irai reparler d'espérance ;</l>
					<l n="170" num="15.8">Je leur dirai : « Ne pleurez pas ! »</l>
					<l n="171" num="15.9">Oui, la France a quitté la couche</l>
					<l n="172" num="15.10">Où son sang coulait sans tarir,</l>
					<l n="173" num="15.11">Où son regard fixe et farouche</l>
					<l n="174" num="15.12">Disait tout ce qu'on peut souffrir ;</l>
					<l n="175" num="15.13">Elle a, faible encor de sa fièvre,</l>
					<l n="176" num="15.14">Essuyé sur sa pâle lèvre</l>
					<l n="177" num="15.15">La honte mêlée au dégoût ;</l>
					<l n="178" num="15.16">Elle a, des yeux, cherché son glaive ;</l>
					<l n="179" num="15.17">Elle sourit, elle se lève,</l>
					<l n="180" num="15.18">Elle est levée : — elle est debout !</l>
				</lg>
			</div></body></text></TEI>