<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">PENDANT L’INVASION</title>
				<title type="medium">Édition électronique</title>
				<author key="SOU">
					<name>
						<forename>Joséphin</forename>
						<surname>SOULARY</surname>
					</name>
					<date from="1815" to="1891">1815-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>650 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">SOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>PENDANT L’INVASION</title>
						<author>JOSÉPHIN SOULARY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=_IuxR_m8ok8C</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PENDANT L’INVASION</title>
								<author>JOSÉPHIN SOULARY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="SOU3">
				<head type="main">LE RÉACTIONNAIRE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>La réaction lève la tête !…</quote>
							<bibl>
								<name>(Cliché banal.)</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Lyon, après Marseille, aura<lb/>
								sa tache de sang. La réaction<lb/>
								doit être contente. 
							</quote>
							<bibl>
								<name>(Cliché spécial.)</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">C'est fini ! D'Artagnan dans sa tombe repose</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Avec Aramis et Porthos.</l>
					<l n="3" num="1.3">A leur place ont paru les Jourdain de la prose</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Et les Eschyle du pathos.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">On ne jonglera plus, aux cadences du nombre,</l>
					<l n="6" num="2.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Avec les fleurs d'une chanson ;</l>
					<l n="7" num="2.3">Car au beau pays bleu passe un fantôme sombre</l>
					<l n="8" num="2.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Qui donne aux Muses le frisson.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ce monstre au prix de qui l'Ogre était débonnaire</l>
					<l n="10" num="3.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>A mis le rire en interdit.</l>
					<l n="11" num="3.3">On l’appelle tout bas : LE RÉACTIONNAIRE !</l>
					<l n="12" num="3.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Ce nom seul n'est-il pas maudit ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">C'est lui le grand coupable et le bouc émissaire</l>
					<l n="14" num="4.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Chargé des crimes d'Israël !</l>
					<l n="15" num="4.3">En lui s'est incarné ce fléau nécessaire :</l>
					<l n="16" num="4.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>« Le conspirateur éternel ! »</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Il conspire depuis que les voleurs de terre</l>
					<l n="18" num="5.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>De leur ombre se sont émus.</l>
					<l n="19" num="5.3">Quand Romulus fondait sa borne autoritaire,</l>
					<l n="20" num="5.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Il conspirait avec Rémus.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Il surgit tout armé dans les heures de crise</l>
					<l n="22" num="6.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Dès chausse-trapes du trottoir,</l>
					<l n="23" num="6.3">Ainsi qu'on voit sortir d'une boîte à surprise</l>
					<l n="24" num="6.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Une tête de diable noir.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Nul au juste ne sait les mœurs, la façon d'être</l>
					<l n="26" num="7.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>De ce formidable assassin ;</l>
					<l n="27" num="7.3">Mais on croit l’avoir vu sous la forme d'un prêtre,</l>
					<l n="28" num="7.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>D'un chimiste ou d'un médecin.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Nul ne pourrait jurer qu'on ait surpris l'infâme</l>
					<l n="30" num="8.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>La torche aux mains, le fer aux dents ;</l>
					<l n="31" num="8.3">Mais qu'un maçon se tue ou qu'un toit prenne flamme,</l>
					<l n="32" num="8.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Le monstre a trempé là-dedans !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Qu'un financier, touché des plaintes amoureuses</l>
					<l n="34" num="9.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Qu'on soupire à son coffre-fort,</l>
					<l n="35" num="9.3">Porte en Suisse le nid de ses valeurs peureuses,</l>
					<l n="36" num="9.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Le monstre est dans son passe-port !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Nos combattants, si fiers de leur mise soignée,</l>
					<l n="38" num="10.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Voient-ils, dès le premier bouton,</l>
					<l n="39" num="10.3">Leurs tuniques partir en toile d’araignée,</l>
					<l n="40" num="10.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Et leurs souliers fondre en carton ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Ce n'est là qu'une ruse au monstre familière ;</l>
					<l n="42" num="11.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Et c'est encore un de ses tours,</l>
					<l n="43" num="11.3">Lorsque nos bataillons avancent en arrière,</l>
					<l n="44" num="11.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Toujours battant, battus toujours.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Si la cité n'est plus qu'une immense caserne ;</l>
					<l n="46" num="12.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Si, pour garder on ne sait quoi,</l>
					<l n="47" num="12.3">Tout un camp de bourgeois, la nuit venue, hiverne</l>
					<l n="48" num="12.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>En plein nord, loin du doux chez-soi ;</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Si Prudhomme, en public, roule, risible Alcide,</l>
					<l n="50" num="13.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Des yeux qui voudraient être craints ;</l>
					<l n="51" num="13.3">S'il laisse fièrement sur sa face placide</l>
					<l n="52" num="13.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Germer une barbe à tous crins ;</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">La poitrine en avant, le doigt sur la couture,</l>
					<l n="54" num="14.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>S'il va, raide comme un épi,</l>
					<l n="55" num="14.3">Visiter ses clients, le sabre à la ceinture,</l>
					<l n="56" num="14.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Et s'il couche avec, son képi ;</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Si Bébé même est pris de vaillance mutine ;</l>
					<l n="58" num="15.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Si, laissant carlins et coucous,</l>
					<l n="59" num="15.3">Le bonhomme Noël a mis dans sa bottine</l>
					<l n="60" num="15.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Un revolver à douze coups ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">C'est qu'on a dit : « Le monstre a redressé la tête !</l>
					<l n="62" num="16.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>« Veillons bien ! sus au réprouvé ! »</l>
					<l n="63" num="16.3">Or Prudhomme, bon père et citoyen honnête,</l>
					<l n="64" num="16.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Croit toujours que c'est arrivé.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Je veux vous confier un secret qui me pèse.</l>
					<l n="66" num="17.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Sommes-nous seuls ? baissons la voix.</l>
					<l n="67" num="17.3">Eh bien, j’ai vu le monstre !… oui, vu, ne vous déplaise,</l>
					<l n="68" num="17.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>En plein jour, comme je vous vois !</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Sachez tous les forfaits de ce Croquemitaine !</l>
					<l n="70" num="18.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Bravant frimas, neige et glaçons,</l>
					<l n="71" num="18.3">Le lâche ! il arborait des gants chauds de futaine, '</l>
					<l n="72" num="18.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Un cache-nez et des chaussons !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Corrompant jusqu’au fisc, ce suppôt des despotes</l>
					<l n="74" num="19.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Revenait allègre et furtif</l>
					<l n="75" num="19.3">De solder au Trésor le montant de ses cotes</l>
					<l n="76" num="19.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Doublé par ordre impératif !</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">A chaque pas semant son or, — le misérable !</l>
					<l n="78" num="20.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>On le voyait, les yeux baissés,</l>
					<l n="79" num="20.3">Sournoisement glisser, dans un tronc charitable,</l>
					<l n="80" num="20.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Son offrande pour les blessés !</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Comme il est coutumier d’audaces merveilleuses,</l>
					<l n="82" num="21.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>On insinuait quelque part</l>
					<l n="83" num="21.3">Qu'il aurait, — le brigand, — fourni deux mitrailleuses</l>
					<l n="84" num="21.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>A notre légion qui part !</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Mille indices font voir ses féroces pensées :</l>
					<l n="86" num="22.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Il met parfois du linge blanc ;</l>
					<l n="87" num="22.3">Il déteste la foule, — à cause des poussées,</l>
					<l n="88" num="22.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Et le ronge, — a cause du sang.</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Si le pays, parmi ses sauveurs de tout grade,</l>
					<l n="90" num="23.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Ne l’a pas vu se faufiler,</l>
					<l n="91" num="23.3">C'est qu'il juge, à part lui, bien malade un malade</l>
					<l n="92" num="23.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Que tant de Purgon font aller.</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Sur tout chef-d’œuvre il a l'incroyable manie</l>
					<l n="94" num="24.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>De noter quelques errata ;</l>
					<l n="95" num="24.3">A cela près, il donne un bon point de génie</l>
					<l n="96" num="24.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Au lyrisme de Gambetta.</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Envers tous citoyens, riches ou pauvres hères,</l>
					<l n="98" num="25.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Il professe un profond respect ;</l>
					<l n="99" num="25.3">Même il les nommerait volontiers « très-chers frères »,</l>
					<l n="100" num="25.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Si ce titre n'était suspect.</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Il admet le crayon narquois jetant sa gourme</l>
					<l n="102" num="26.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>En des dessins d'un trait gaillard ;</l>
					<l n="103" num="26.3">Mais il aimerait l'Art sentant moins la chiourme</l>
					<l n="104" num="26.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Et respirant un peu plus l’art.</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1">Doux par tempérament, pour les fureurs d'Oreste</l>
					<l n="106" num="27.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Il a des partions indulgens ;</l>
					<l n="107" num="27.3">Seulement il voudrait le voir un peu moins leste</l>
					<l n="108" num="27.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>A fusiller les braves gens.</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1">Il lui plaît qu'à son gré chacun se règle en somme</l>
					<l n="110" num="28.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Sur Machiavel ou Proudhon ;</l>
					<l n="111" num="28.3">Mais il ose avouer que le Christ est son homme,</l>
					<l n="112" num="28.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Et que l'Évangile a du bon.</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1">« Arrêtez ! me dit-on ; nous prenez-vous pour d’autres ?</l>
					<l n="114" num="29.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>« Mais ce monstre est de nos cousins ;</l>
					<l n="115" num="29.3">« Dans ses gestes et faits vous racontez les nôtres,</l>
					<l n="116" num="29.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>« Et même ceux de nos voisins. »</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1">Il est vrai. Ce gredin, ce gueux, cet être immonde</l>
					<l n="118" num="30.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Qu'écorcher vif serait trop doux,</l>
					<l n="119" num="30.3">Lecteur, c'est vous, c'est moi, c'est lui, c'est tout le monde !</l>
					<l n="120" num="30.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>— Je m'en doutais ; embrassons-nous.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">5 janvier 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>