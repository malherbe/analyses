<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR18">
				<head type="main">JOUR DE LA CAPITULATION</head>
				<opener>
					<dateline>
						<date when="1871">Janvier 1871.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">AINSI tout est fini. — Sous le vent de tempête,</l>
					<l n="2" num="1.2">O France, ô mon pays, il faut courber la tête,</l>
					<l n="3" num="1.3">Sous la fatalité baisser ton front d’airain ;</l>
					<l n="4" num="1.4">L’ennemi triomphant a brisé ta couronne ;</l>
					<l n="5" num="1.5">La Fortune te hait, l’univers t’abandonne,</l>
					<l n="6" num="1.6">Et sur tes bras meurtris pèse le fer germain.</l>
					</lg>
					<lg n="2">
					<l n="7" num="2.1">En vain, grande d‘espoir et folle de souffrance,</l>
					<l n="8" num="2.2">De tes puissants poumons as-tu crié : Vengeance !</l>
					<l n="9" num="2.3">En vain as-tu levé ton regard vers les cieux :</l>
					<l n="10" num="2.4">Le cri s'est arrêté dans ta gorge oppressée,</l>
					<l n="11" num="2.5">Et, comme tu rêvais de ta gloire passée,</l>
					<l n="12" num="2.6">Des larmes de douleur ont obscurci tes yeux.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Ah ! les temps ne sont plus Où l'Europe, inquiète,</l>
					<l n="14" num="3.2">Dans chacun de tes coups trouvant une défaite,</l>
					<l n="15" num="3.3">Pour se venger plus tard implorait ton appui ;</l>
					<l n="16" num="3.4">Avec les temps nouveaux vient la nouvelle guerre ;</l>
					<l n="17" num="3.5">Ils frappent maintenant, ceux qu'on frappait naguère :</l>
					<l n="18" num="3.6">Les vaincus d’autrefois sont vainqueurs aujourd’hui.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Mais va ! ne rougis pas, car, s’il est une honte,</l>
					<l n="20" num="4.2">Qu’à l'auteur de la guerre entière elle remonte ;</l>
					<l n="21" num="4.3">Sedan l'a condamné, ton crime le punit ;</l>
					<l n="22" num="4.4">Pour lui le rouge au front et le regret à l’âme,</l>
					<l n="23" num="4.5">Pour lui les noirs remords, pour lui le nom d’infâme,</l>
					<l n="24" num="4.6">Pour lui tout ce qui venge et tout ce qui maudit !</l>
					</lg>
					<lg n="5">
					<l n="25" num="5.1">Mais toi, tu peux braver l’ennemi qui te tue.</l>
					<l n="26" num="5.2">Avec trop de valeur, France, tu t’es battue</l>
					<l n="27" num="5.3">Pour qu’aujourd’hui ton front s’abaisse en rougissant.</l>
					<l n="28" num="5.4">Tu fus brave toujours, si tu fus malheureuse,</l>
					<l n="29" num="5.5">Et jamais une paix ne peut être honteuse</l>
					<l n="30" num="5.6">Quand la main qui la signe est couverte de sang.</l>
				</lg>
			</div></body></text></TEI>