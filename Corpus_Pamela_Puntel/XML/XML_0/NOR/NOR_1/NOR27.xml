<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR27">
				<head type="main">A L‘ÉGLISE</head>
				<opener>
					<dateline>
						<date when="1871">Mai 1871</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">LA rue est roide, étroite, et le pavé crotté.</l>
					<l n="2" num="1.2">Entre deux croque-morts rudement ballotté,</l>
					<l n="3" num="1.3">Un petit cercueil blanc chemine vers l’église.</l>
					<l n="4" num="1.4">Derrière, un homme pâle, à la moustache grise,</l>
					<l n="5" num="1.5">Marche, les yeux au sol, triste, le front penché.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Sur le seuil de l’église, un insurgé couché</l>
					<l n="7" num="2.2">Fume sa pipe et voit le convoi qui s'avance :</l>
					<l n="8" num="2.3">« Halte-là, citoyen ! et qu’on passe à distance !</l>
					<l n="9" num="2.4">On n'entre pas ici ! — Je viens pour mon enfant…</l>
					<l n="10" num="2.5">— Impossible ! — Pourtant ?… — La consigne défend</l>
					<l n="11" num="2.6">De te laisser entrer : et puis d'ailleurs, regarde,</l>
					<l n="12" num="2.7">Depuis hier l’église est faite corps de garde.</l>
					<l n="13" num="2.8">Nous avons remplacé ces calotins damnés.</l>
					<l n="14" num="2.9">— Pourtant je ne peux pas laisser… vous comprenez…</l>
					<l n="15" num="2.10">Mon pauvre enfant chéri sans messe, sans prière…</l>
					<l n="16" num="2.11">— Des messes, citoyen ! des messes ! pourquoi faire ?</l>
					<l n="17" num="2.12">Si ton petit est mort, les messes n’y font rien.</l>
					<l n="18" num="2.13">Il faut se consoler comme un vrai citoyen,</l>
					<l n="19" num="2.14">Et, laissant de côté curés et patenôtres ,</l>
					<l n="20" num="2.15">Quand on perd un enfant, en fabriquer deux autres. »</l>
					<l n="21" num="2.16">Le père devint rouge et son poing se roidit.</l>
					<l n="22" num="2.17">Il voulut insister ; mais, hélas ! qu’eût-il dit ?</l>
					<l part="I" n="23" num="2.18">Qu’eût-il fait ?</l>
					<l part="F" n="23" num="2.18">Il partit, et vers le cimetière</l>
					<l n="24" num="2.19">Marcha, les yeux fixés sur la petite bière,</l>
					<l n="25" num="2.20">Le cœur gonflé de pleurs,— mais pensant qu’en haut lieu</l>
					<l n="26" num="2.21">Il est une justice, et qu’on l’appelle : Dieu !</l>
				</lg>
			</div></body></text></TEI>