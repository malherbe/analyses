<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR1">
				<head type="main">LES FRANCS-FILEURS</head>
				<opener>
					<dateline>
						<date when="1870">Saint-Maux, septembre 1870.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Le régiment des francs-fileurs</l>
					<l n="2" num="1.2">Est un vrai régiment modèle ;</l>
					<l n="3" num="1.3">Et ses soldats sont les meilleurs</l>
					<l n="4" num="1.4">Pour s’élancer à tire-d’aile</l>
					<l n="5" num="1.5"><space unit="char" quantity="12"/><space unit="char" quantity="12"/>Ailleurs.</l>
					<l n="6" num="1.6">Quand, pour investir notre ville,</l>
					<l n="7" num="1.7">L'Allemand lança son filet,</l>
					<l n="8" num="1.8">On vit comme — bouche inutile —</l>
					<l n="9" num="1.9">Plus d’un franc-fileur qui filait.</l>
					<l n="10" num="1.10">L'un s’en allait pour sa famille,</l>
					<l n="11" num="1.11">Qu’il ne pouvait laisser ici ;</l>
					<l n="12" num="1.12">Pour ses enfants, sa grande fille,</l>
					<l n="13" num="1.13">Qui lui causait bien du souci ;</l>
					<l n="14" num="1.14">L’autre, pour défendre la Loire</l>
					<l n="15" num="1.15">Et mettre le pays debout ;</l>
					<l n="16" num="1.16">Tel autre partait… pour la gloire,</l>
					<l n="17" num="1.17">Et tel autre… pour rien du tout.</l>
					<l n="18" num="1.18">C’est ainsi qu‘avec une entente,</l>
					<l n="19" num="1.19">Un ensemble des plus parfaits,</l>
					<l n="20" num="1.20">Ces messieurs plièrent la tente</l>
					<l n="21" num="1.21">Et préparèrent leurs paquets.</l>
				</lg>
				<lg n="2">
					<l n="22" num="2.1">Étant d’intelligence rare,</l>
					<l n="23" num="2.2">Ils avaient très-bien su prévoir</l>
					<l n="24" num="2.3">Qu’au jour où la Prusse barbare</l>
					<l n="25" num="2.4">Autour de nous viendrait s’asseoir,</l>
					<l n="26" num="2.5">On pourrait faire maigre chère,</l>
					<l n="27" num="2.6">Manger du perdreau rarement.</l>
					<l n="28" num="2.7">Du bœuf, fort peu ; – du mouton, guère ; —</l>
					<l n="29" num="2.8">Du pain même — modérément.</l>
					<l n="30" num="2.9">Là-bas, c’était manger et vivre ;</l>
					<l n="31" num="2.10">Mais ici, vivre sans manger.</l>
					<l n="32" num="2.11">Quel embarras ! quel parti suivre ?</l>
					<l n="33" num="2.12">Le déshonneur ou le danger ?</l>
					<l n="34" num="2.13">Bah ! la gloire est chose tentante,</l>
					<l n="35" num="2.14">Mais les estomacs délicats</l>
					<l n="36" num="2.15">Ont besoin de viande saignante…</l>
					<l n="37" num="2.16">J'aime mieux m’en aller là-bas.</l>
				</lg>
				<lg n="3">
					<l n="38" num="3.1">Et puis, plus que la nourriture,</l>
					<l n="39" num="3.2">Ils craignaient,— et non sans raison,—</l>
					<l n="40" num="3.3">Que les Prussiens (malice pure !)</l>
					<l n="41" num="3.4">Ne se servissent du canon.</l>
					<l n="42" num="3.5">Or la nuit, tandis qu’on sommeille,</l>
					<l n="43" num="3.6">Est-il rien de plus ennuyeux</l>
					<l n="44" num="3.7">Que ce fracas qui vous réveille</l>
					<l n="45" num="3.8">Et vous fait entr’ouvrir les yeux ?</l>
					<l n="46" num="3.9">Et puis les blessés… car sans doute</l>
					<l n="47" num="3.10">On en verra plus d’un passer,</l>
					<l n="48" num="3.11">Qui sait ? Peut-être sur la route</l>
					<l n="49" num="3.12">Forcera-t-on à les panser ?</l>
					<l n="50" num="3.13">Ah ! cette crainte est la dernière :</l>
					<l n="51" num="3.14">Et puis la viande de cheval…</l>
					<l n="52" num="3.15">Non ! je ne puis être infirmière,</l>
					<l n="53" num="3.16">Car l’aspect du sang me fait mal.</l>
				</lg>
				<lg n="4">
					<l n="54" num="4.1">Outre cela, que va-t-on faire</l>
					<l n="55" num="4.2">Pendant si longtemps à Paris ?</l>
					<l n="56" num="4.3">Le Bois tout entier est par terre !</l>
					<l n="57" num="4.4">Plus d’Opéra les mercredis !</l>
					<l n="58" num="4.5">Pour sûr, au train dont vont les choses.</l>
					<l n="59" num="4.6">Cet hiver on recevra peu :</l>
					<l n="60" num="4.7">Mes deux nouvelles robes roses</l>
					<l n="61" num="4.8">Quand donc les mettrai-je, mon Dieu ?</l>
					<l n="62" num="4.9">Mais ce sera la mort d’avance</l>
					<l n="63" num="4.10">Que de passer l’hiver ainsi :</l>
					<l n="64" num="4.11">J’en tremble déjà quand j’y pense.</l>
					<l n="65" num="4.12">Non ! je ne puis rester ici.</l>
					<l n="66" num="4.13">Moi qui suis si bien aux lumières !</l>
					<l n="67" num="4.14">Mais j’oubliais ! il faut partir :</l>
					<l n="68" num="4.15">Le docteur me disait naguères</l>
					<l n="69" num="4.16">Que l’ennui me ferait maigrir.</l>
				</lg>
				<lg n="5">
					<l n="70" num="5.1">Quand, après la guerre finie,</l>
					<l n="71" num="5.2">Frais et gaillards ils reviendront,</l>
					<l n="72" num="5.3">Ceux qui restaient pour la patrie,</l>
					<l n="73" num="5.4">Quand ils partaient, se souviendront.</l>
					<l n="74" num="5.5">Lorgnon dans l’œil, mine riante,</l>
					<l n="75" num="5.6">Tranquilles comme Alis-Babas,</l>
					<l n="76" num="5.7">De la ville encor palpitante</l>
					<l n="77" num="5.8">Ils visiteront les dégâts.</l>
					<l n="78" num="5.9">En nous rencontrant sur leur route,</l>
					<l n="79" num="5.10">Tendant vers nous leurs doigts gantés.</l>
					<l n="80" num="5.11">Ils nous demanderont sans doute</l>
					<l n="81" num="5.12">Des nouvelles de nos santés.</l>
					<l n="82" num="5.13">Nous alors, sans vouloir leur rendre</l>
					<l n="83" num="5.14">Leurs joyeux serrements de main,</l>
					<l n="84" num="5.15">De façon qu’ils puissent l’entendre,</l>
					<l n="85" num="5.16">Nous fredonnerons ce refrain :</l>
					<l n="86" num="5.17">Le régiment des francs-fileurs</l>
					<l n="87" num="5.18">Est un vrai régiment modèle,</l>
					<l n="88" num="5.19">Et ses soldats sont les meilleurs</l>
					<l n="89" num="5.20">Pour s’élancer à tire-d’aile</l>
					<l n="90" num="5.21"><space unit="char" quantity="12"/><space unit="char" quantity="12"/>Ailleurs.</l>
				</lg>
			</div></body></text></TEI>