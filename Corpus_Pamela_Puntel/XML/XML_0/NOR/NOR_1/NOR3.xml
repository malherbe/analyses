<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">TABLETTES D’UN MOBILE</title>
				<title type="sub">1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="NOR">
					<name>
						<forename>Jacques</forename>
						<surname>NORMAND</surname>
					</name>
					<date from="1848" to="1931">1848-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1350 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">NOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>TABLETTES D’UN MOBILE 1870-1871</title>
						<author>JACQUES NORMAND</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books?id=BWt4N2w5RnYC</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>TABLETTES D’UN MOBILE 1870-1871</title>
								<author>JACQUES NORMAND</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>E. LACHAUD ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="NOR3">
				<head type="main">L’EXILÉE</head>
				<opener>
					<dateline>
						<date when="1870">Septembre 1870.</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">POURQUOI pleures-tu, pauvre femme,</l>
					<l n="2" num="1.2">Assise au bord de ce chemin ?</l>
					<l n="3" num="1.3">Ton visage, où se lit ton âme,</l>
					<l n="4" num="1.4">Porte l’empreinte du chagrin ;</l>
					<l n="5" num="1.5">Tes beaux yeux ont perdu leur flamme,</l>
					<l n="6" num="1.6">Et des pleurs roulent sur ta main.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Est-ce l’amour qui t’a blessée ?</l>
					<l n="8" num="2.2">As-tu ressenti la douleur</l>
					<l n="9" num="2.3">De te voir soudain repoussée</l>
					<l n="10" num="2.4">Par un geste froid et moqueur ?</l>
					<l n="11" num="2.5">T’a-t-il lâchement repoussée</l>
					<l n="12" num="2.6">Celui-là qui te prit ton cœur ?</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Est-ce ton enfant que tu pleures ?</l>
					<l n="14" num="3.2">Est-ce ton frère, ton ami ?</l>
					<l n="15" num="3.3">Seule, loin de toutes demeures,</l>
					<l n="16" num="3.4">Grelottante et morte à demi,</l>
					<l n="17" num="3.5">Pourquoi marcher de longues heures</l>
					<l n="18" num="3.6">Sans jamais chercher un abri ?</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">De quel pays es-tu venue ?</l>
					<l n="20" num="4.2">Quel chagrin a pu t’alarmer ?</l>
					<l n="21" num="4.3">Va, dis-le moi : rien qu’à ta vue</l>
					<l n="22" num="4.4">Mon être s’est laissé charmer,</l>
					<l n="23" num="4.5">Et, sans t’avoir jamais connue,</l>
					<l n="24" num="4.6">Il me semble déjà t’aimer.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Viens : jusqu’à la ferme prochaine</l>
					<l n="26" num="5.2">Accepte mon bras pour soutien ;</l>
					<l n="27" num="5.3">Raconte-moi toute ta peine ;</l>
					<l n="28" num="5.4">Tu trouveras, sans craindre rien,</l>
					<l n="29" num="5.5">Une main pour serrer la tienne,</l>
					<l n="30" num="5.6">Un cœur pour soulager le tien.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">— Ami, si longue est la distance</l>
					<l n="32" num="6.2">Qu’il me faut encor parcourir</l>
					<l n="33" num="6.3">Que j’ai perdu toute espérance</l>
					<l n="34" num="6.4">D’y pouvoir jamais réussir :</l>
					<l n="35" num="6.5">Ami, si grande est ma souffrance</l>
					<l n="36" num="6.6">Que rien ne saurait la guérir.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Tu me demandes qui je pleure ?</l>
					<l n="38" num="7.2">Hélas ! qui pourrait les compter</l>
					<l n="39" num="7.3">Tous ceux-là que la mort effleure,</l>
					<l n="40" num="7.4">Ou que la mort vient d’emporter,</l>
					<l n="41" num="7.5">Foule nombreuse que chaque heure,</l>
					<l n="42" num="7.6">Chaque moment, peut augmenter !</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Je fuis devant mon ennemie,</l>
					<l n="44" num="8.2">La Guerre au bras ensanglanté :</l>
					<l n="45" num="8.3">Je n’ai ni foyer ni patrie ;</l>
					<l n="46" num="8.4">Mon nom est partout rejeté</l>
					<l n="47" num="8.5">Comme celui d’une bannie :</l>
					<l n="48" num="8.6">On m’appelle l’Humanité.</l>
				</lg>
			</div></body></text></TEI>