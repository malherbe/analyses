<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA REDOUTE DE MONTRETOUT</title>
				<title type="medium">Édition électronique</title>
				<author key="PTE">
					<name>
						<forename>Edmond</forename>
						<surname>POTIER</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>187 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">PTE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LA REDOUTE DE MONTRETOUT</title>
						<author>EDMOND POTIER</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>DENTU</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="PTE1">
				<head type="main">LA REDOUTE DE MONTRETOU</head>
				<div type="section" n="1">
				<head type="number">I</head>
					<head type="main">LE DÉPART</head>
					<lg n="1">
						<l n="1" num="1.1">C'EST LE DIX-HUIT JANVIER. Le matin, à dix heures,</l>
						<l n="2" num="1.2">La Garde sédentaire occupe le rempart ;</l>
						<l n="3" num="1.3">Les bataillons de marche ont quitté leurs demeures ;</l>
						<l n="4" num="1.4">Sac au dos ! A midi ! C'est l'ordre du départ.</l>
						<l n="5" num="1.5">Le lieu de réunion est la rue Lafayette,</l>
						<l n="6" num="1.6">Et sur tout son parcours, le second régiment</l>
						<l n="7" num="1.7">A formé ses faisceaux, fusil et baïonnette ;</l>
						<l n="8" num="1.8">On n'attend plus alors qu'un seul commandement.</l>
						<l n="9" num="1.9">Une heure ! une demie ! Enfin deux heures sonnent !</l>
						<l n="10" num="1.10">Le colonel est là… Vite il prend les devant.</l>
						<l n="11" num="1.11">Aussitôt les tambours et les clairons résonnent.</l>
						<l n="12" num="1.12">Voici Martin du Nord ! Bataillons, en avant !</l>
						<l n="13" num="1.13">Nous voilà donc partis ! Père et mère à la suite ;</l>
						<l n="14" num="1.14">Les femmes, les enfants, nous pressant les talons,</l>
						<l n="15" num="1.15">Riant, chantant, pleurant, nous font brin de conduite,</l>
						<l n="16" num="1.16">Sans savoir plus que nous l'endroit où nous allons.</l>
						<l n="17" num="1.17">Un long cordon de troupe à nos yeux se dévoile,</l>
						<l n="18" num="1.18">Mobiles et lignards, mitrailleuses, canons ;</l>
						<l n="19" num="1.19">Nous venons d'arriver barrière de l’Étoile.</l>
						<l n="20" num="1.20">Adieu parents, amis ! Nous suivons nos fanions.</l>
						<l n="21" num="1.21">Droit, toujours droit vers nous, c'est toujours de la troupe,</l>
						<l n="22" num="1.22">Et le flot s'en grossit lorsque nous avançons ;</l>
						<l n="23" num="1.23">Gardes nationaux, lignards font un seul groupe</l>
						<l n="24" num="1.24">Sur le pont de Neuilly qu'alors nous traversons.</l>
						<l n="25" num="1.25">Le régiment entier se perd en conjecture :</l>
						<l n="26" num="1.26">Cette fois, se dit-il, on ne sort pas pour rien,</l>
						<l n="27" num="1.27">Et nous aurons demain bien de la tablature</l>
						<l n="28" num="1.28">Car nous allons, je crois, droit au mont Valérien.</l>
						<l n="29" num="1.29">Une heure l'on fait halte au bout de Courbevoie ;</l>
						<l n="30" num="1.30">Nous mettons sac à terre, encor frais et dispos.</l>
						<l n="31" num="1.31">Certe ! il en était temps ! Embourbés sur la voie,</l>
						<l n="32" num="1.32">Pour aller en avant, il fallait du repos.</l>
						<l n="33" num="1.33">A cinq heures sonnant, l'on se remet en marche</l>
						<l n="34" num="1.34">Sans cesse pataugeant dans les chemins boueux.</l>
						<l n="35" num="1.35">Allons-nous à Nanterre, à Buzenval, à Garche ?</l>
						<l n="36" num="1.36">C'est au mont Valérien, cela n'est plus douteux.</l>
						<l n="37" num="1.37">Du gigantesque fort nous franchissons l'enceinte ;</l>
						<l n="38" num="1.38">Comme nouveaux soldats, avec étonnement,</l>
						<l n="39" num="1.39">Nous tournons, contournons l'immense labyrinthe.</l>
						<l n="40" num="1.40">Enfin, là, nous trouvons notre cantonnement.</l>
						<l n="41" num="1.41">Par escouade de douze, à grand'peine on s'installe.</l>
						<l n="42" num="1.42">Pour dîner dans la mienne, un chétif hareng saur</l>
						<l n="43" num="1.43">Fait les frais du repas. Sur la planche on s'étale,</l>
						<l n="44" num="1.44">Vaincus par la fatigue, aussitôt l'on s'endort.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<head type="main">L'ACTION</head>
					<lg n="1">
						<l n="45" num="1.1">C'EST LE DIX-NEUF JANVIER. Nous le savions la veille…</l>
						<l n="46" num="1.2">Quatre heures du matin ; la Diane nous réveille.</l>
						<l n="47" num="1.3">On absorbe au galop le café dans son quart ;</l>
						<l n="48" num="1.4">On laisse là son sac, puis à l'instant on part,</l>
						<l n="49" num="1.5">Grignotant le biscuit sous nos dents, dans nos bouches ;</l>
						<l n="50" num="1.6">Chacun prend son fusil et ses cent vingt cartouches.</l>
						<l n="51" num="1.7">A peine on se voyait, comme aux jours les plus courts ;</l>
						<l n="52" num="1.8">Pour la seconde fois nous suivons les contours</l>
						<l n="53" num="1.9">De l'invincible fort, ce géant de vaillance</l>
						<l n="54" num="1.10">Qui devait soutenir si bien notre défense.</l>
						<l n="55" num="1.11">Pressés l'un contre l'autre, de temps en temps courbés,</l>
						<l n="56" num="1.12">S'avançant pas à pas et toujours embourbés.</l>
						<l n="57" num="1.13">Des signaux de couleur, sous forme de fusées,</l>
						<l n="58" num="1.14">Portant en haut des airs leurs flammes divisées,</l>
						<l n="59" num="1.15">Avertissent alors les forts des alentours,</l>
						<l n="60" num="1.16">Qui répondent bientôt par le même recours.</l>
						<l n="61" num="1.17">Nous longeons le chemin bordant la Tuilerie ;</l>
						<l n="62" num="1.18">C'est là que se trouvait un parc d'artillerie.</l>
						<l n="63" num="1.19">Nous y vîmes aussi de nombreux régiments</l>
						<l n="64" num="1.20">En réserve tout prêts pour les événements…</l>
						<l n="65" num="1.21">Sept heures du matin ! Enfin le jour va poindre.</l>
						<l n="66" num="1.22">Des bataillons de ligne à nous viennent se joindre.</l>
						<l n="67" num="1.23">Nous entendons crier : Alerte ! En tirailleurs !</l>
						<l n="68" num="1.24">Chacun vient se cacher aux endroits les meilleurs ;</l>
						<l n="69" num="1.25">Étendus à plat ventre aux côtés de la route,</l>
						<l n="70" num="1.26">On charge son fusil, en silence on écoute.</l>
						<l n="71" num="1.27">En ce moment alors, l'obus vint à ronfler.</l>
						<l n="72" num="1.28">Et les balles aussi commençaient à siffler.</l>
						<l n="73" num="1.29">Devant nous, des troupiers à la culotte rouge</l>
						<l n="74" num="1.30">Ont l'ordre d'avancer, mais aucun d'eux ne bouge ;</l>
						<l n="75" num="1.31">En vain leur commandant cherche à les entrainer…</l>
						<l n="76" num="1.32">Marchez ! Nationaux ! seuls, vous devez donner.</l>
						<l n="77" num="1.33">En avant ! en avant ! Et chacun s'encourage.</l>
						<l n="78" num="1.34">Au loin nous entendons la lutte qui s'engage :</l>
						<l n="79" num="1.35">Feux d'file et peloton semblent se succéder ;</l>
						<l n="80" num="1.36">Trois pièces de canon viennent nous seconder.</l>
						<l n="81" num="1.37">Elles tonnent ! Pan ! pan ! Quelles sont leurs portées ?</l>
						<l n="82" num="1.38">Je l'ignore. A l'instant elles sont démontées.</l>
						<l n="83" num="1.39">Il nous fallut de suite aller, à pas pressés,</l>
						<l n="84" num="1.40">Dételer leurs chevaux sur la route blessés.</l>
						<l n="85" num="1.41">De plus en plus terrible était la fusillade ;</l>
						<l n="86" num="1.42">Chez nous, pas un blessé, personne de malade !</l>
						<l n="87" num="1.43">En avant ! Deux cents pas, puis, toujours en avant,</l>
						<l n="88" num="1.44">Nous traversons les champs, les vignes ; tout bravant.</l>
						<l n="89" num="1.45">Neuf heures du matin. Au pied d'une redoute,</l>
						<l n="90" num="1.46">Nous arrivons ainsi sans que chacun s'en doute.</l>
						<l n="91" num="1.47">Baïonnette au canon ! A l'assaut ! à l'assaut !</l>
						<l n="92" num="1.48">A ce cri répété, nous grimpons saut par saut.</l>
						<l n="93" num="1.49">Le clairon nous sonna l'entrainant chant de gloire</l>
						<l n="94" num="1.50">Qui pour nous jusqu'alors voulait dire : Victoire !</l>
						<l n="95" num="1.51">Car soixante ennemis furent cernés partout ;</l>
						<l n="96" num="1.52">Martin du Nord en chef occupait MONTRETOUT.</l>
						<l n="97" num="1.53">Brandissant à la main un échalas de vigne</l>
						<l n="98" num="1.54">En guise de drapeau, nous le vîmes bien digne</l>
						<l n="99" num="1.55">De tout son régiment, et certe il l'a prouvé,</l>
						<l n="100" num="1.56">Son bataillon chéri ne fut trop éprouvé.</l>
						<l n="101" num="1.57">Les balles continuaient à siffler sur nos têtes,</l>
						<l n="102" num="1.58">De la redoute alors nous occupions les crêtes.</l>
						<l n="103" num="1.59">Notre oreille entendit la terrible musique</l>
						<l n="104" num="1.60">De la balle chantant en gamme chromatique :</l>
						<l n="105" num="1.61">Psi ! psi !! piou ! ou I ou !! ou !!! puis tombant morte, flac !</l>
						<l n="106" num="1.62">Ou comme en un carton, mortelle, faisant plac !</l>
						<l n="107" num="1.63">C'est un homme tué ! Venez donc, ambulance !</l>
						<l n="108" num="1.64">Mais venez donc, docteur ! car son trépas s'avance !</l>
						<l n="109" num="1.65">Il est mort !<ref type="noteAnchor">*</ref> On le met sur deux fusils, croisé ;</l>
						<l n="110" num="1.66">Tandis que la croix rouge errait dans l'Élysé.</l>
						<l n="111" num="1.67">L'ennemi nous voyait, leur canon Krupp se braque</l>
						<l n="112" num="1.68">Pour pointer à coups sûrs une pauvre baraque.</l>
						<l n="113" num="1.69">Près de trois cents obus, pour sa destruction</l>
						<l n="114" num="1.70">En poudre ont plus coûté que sa construction,</l>
						<l n="115" num="1.71">Quatre heures bombardés ! Nous eûmes ce spectacle,</l>
						<l n="116" num="1.72">Et des éclats d'obus épargnés par miracle.</l>
						<l n="117" num="1.73">Tous ne peuvent le dire ! Oh ! malheureusement,</l>
						<l n="118" num="1.74">Des victimes gisaient là douloureusement !</l>
						<l n="119" num="1.75">Nous avions avec nous les francs-tireurs des Ternes</l>
						<l n="120" num="1.76">Semblables à des lions sortant de leurs cavernes,</l>
						<l n="121" num="1.77">Ils soutenaient le feu, tirant à coups comptés ;</l>
						<l n="122" num="1.78">Leur brave lieutenant eut les pieds emportés.</l>
						<l n="123" num="1.79">Il passa devant nous, et malgré sa souffrance :</l>
						<l n="124" num="1.80">Vengez-moi ! cria t-il, Adieu ! Vive la France !</l>
						<l n="125" num="1.81">Nous étions tout au plus encor près de trois cents.</l>
						<l n="126" num="1.82">Il fallait nous aider ; c'est du simple bon sens ;</l>
						<l n="127" num="1.83">Mais le chemin partout devint impraticable,</l>
						<l n="128" num="1.84">Et chevaux et canons s'enfonçaient dans le sable.</l>
						<l n="129" num="1.85">Leur frayer un passage à cela résistant,</l>
						<l n="130" num="1.86">Ce fut du temps perdu quoique fait à l'instant.</l>
						<l n="131" num="1.87">L'on nous met à l'abri dessous la casemate,</l>
						<l n="132" num="1.88">Tandis qu'autour de nous l'obus encor éclate.</l>
						<l n="133" num="1.89">Nous avions faim et soif, ce qu'on ne peut braver,</l>
						<l n="134" num="1.90">Et les vivres non plus ne pouvaient arriver.</l>
						<l n="135" num="1.91">Un ordre fut donné vers cette heure attardée :</l>
						<l n="136" num="1.92">Partez ! La position est maintenant gardée.</l>
						<l n="137" num="1.93">Serrez tous vos bidons, vos quarts ; sans feu, sans bruit :</l>
						<l n="138" num="1.94">Épaule contre épaule, en silence ! Il fait nuit !</l>
						<l n="139" num="1.95">Le fort nous protégea près de son avancée,</l>
						<l n="140" num="1.96">De plus en plus la route en boue était tracée ;</l>
						<l n="141" num="1.97">Nous ne faisions qu'un pas ; ferré comme un roulier,</l>
						<l n="142" num="1.98">A peine aurait-on pu retirer son soulier ;</l>
						<l n="143" num="1.99">Et nous avons bien mis, serrés masse par masse,</l>
						<l n="144" num="1.100">Quatre heures pour rentrer au fort à notre place,</l>
						<l n="145" num="1.101">Tellement imprévu devint l'encombrement</l>
						<l n="146" num="1.102">De canons, de troupiers et de tout régiment.</l>
						<l n="147" num="1.103">Nous eûmes pour manger viande et soupe salée,</l>
						<l n="148" num="1.104">Et pas d'eau pour mouiller notre bouche altérée ;</l>
						<l n="149" num="1.105">Il nous en vint pourtant, et bientôt raffermis,</l>
						<l n="150" num="1.106">Tous aussitôt couchés, nous fûmes endormis,</l>
					</lg>
					<closer>
						<note type="footnote" id="*">
							Donjon, volontaire marié, décoré de la médaille militaire de Crimée et d'Italie.
						</note>
					</closer>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<head type="main">LE RETOUR</head>
					<lg n="1">
						<l n="151" num="1.1">C'EST LE VINGT DE JANVIER. Huit heures du matin.</l>
						<l n="152" num="1.2">Venez Nationaux ! venez ! C'est une fête !</l>
						<l n="153" num="1.3">C'est notre Général, l'étendard à la main,</l>
						<l n="154" num="1.4">Clément-Thomas, heureux, fier d'être à votre tête :</l>
						<l n="155" num="1.5">« Vous êtes des soldats ! Vous nous l'avez appris.</l>
						<l n="156" num="1.6">« Vous venez de sauver l'honneur de notre France !</l>
						<l n="157" num="1.7">« Vous serez les héros du siège de Paris ;</l>
						<l n="158" num="1.8">« Vous avez combattu tous pour sa délivrance,</l>
						<l n="159" num="1.9">« Armistice ! Deux jours, par ordre demandés,</l>
						<l n="160" num="1.10">« Pour enterrer les morts tombés par plus de mille</l>
						<l n="161" num="1.11">« Nous sont de part et d'autre à l'instant accordés</l>
						<l n="162" num="1.12">« Et ce soir, sac au dos, vous rentrerez en ville.</l>
						<l n="163" num="1.13">« L'OFFICIEL a cité votre puissant secours :</l>
						<l n="164" num="1.14">« Vous n'êtes pas battus ! Le fait est historique.</l>
						<l n="165" num="1.15">« Bravo ! Nationaux ! » Bravos à ce discours.</l>
						<l n="166" num="1.16">« Vive Clément-Thomas ! Vive la République ! »</l>
						<l n="167" num="1.17">A midi nous partons. Adieux ! adieux au fort !</l>
						<l n="168" num="1.18">Comme au char embourbé, les traces sur la roue</l>
						<l n="169" num="1.19">Indiquent qu'il s'en tire après un grand effort,</l>
						<l n="170" num="1.20">Nous revenons ainsi tout cuirassés de boue.</l>
						<l n="171" num="1.21">Notre retour se fit par le même chemin</l>
						<l n="172" num="1.22">D'où nous étions venus ; mais la route était belle,</l>
						<l n="173" num="1.23">Le soleil avait lui ! Pour nous serrer la main</l>
						<l n="174" num="1.24">Des amis accouraient, demandant la nouvelle :</l>
						<l n="175" num="1.25">— Eh bien ! et Montretout ! on vous l'a donc repris ?</l>
						<l n="176" num="1.26">— Pas du tout ! disons-nous, maitres de la redoute !</l>
						<l n="177" num="1.27">— Mais non ! — Mais si ! Nous voilà bien surpris.</l>
						<l n="178" num="1.28">Montretout est à nous ! — Bien sûr ? — Sans aucun doute.</l>
						<l n="179" num="1.29">— Mais qu'est-ce que l'on dit ?…</l>
						<l n="180" num="1.30">Dans les journaux du jour,</l>
						<l n="181" num="1.31">De Chanzy, de Faidherbe on lisait la défaite…</l>
						<l n="182" num="1.32">La sédentaire vint fêter notre retour...</l>
						<l n="183" num="1.33">Nous l'avons su trop tard ! nous battions en retraite.</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="2">
						<l n="184" num="2.1">Peut-être en mon récit s'aiguise la critique ;</l>
						<l n="185" num="2.2">J'ai dit la vérité sur ce que j'ai pu voir.</l>
						<l n="186" num="2.3">L'histoire mettra fin à toute polémique.</l>
						<l n="187" num="2.4">Bravo ! Nationaux ! C'était votre devoir.</l>
					</lg>
				</div>
				<closer>
					<signed>EDMOND POTIER<lb/>Caporal, 3e compagnie de guerre, 7e bataillon</signed>
				</closer>
			</div></body></text></TEI>