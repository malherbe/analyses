<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">PENDANT L’ORAGE</title>
				<title type="sub_1">POÈMES NATIONAUX ET HISTORIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="PDE">
					<name>
						<forename>Joseph</forename>
						<surname>POISLE-DESGRANGES</surname>
					</name>
					<date from="1823" to="1879">1823-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>809 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">PDE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>PENDANT L’ORAGE : POÈMES NATIONAUX ET HISTORIQUES</title>
						<author>JOSEPH POISLE-DESGRANGES</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>LEMERRE</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="PDE7">
				<head type="main">ÉPÎTRE À BISMARK</head>
				<head type="sub_1">AUX MANES DES VICTIMES DE LA GUERRE <lb/>ET DU BOMBARDEMENT DE PARIS</head>
				<lg n="1">
					<l n="1" num="1.1">Conseiller d’un roi fou, serviteur de Guillaume,</l>
					<l n="2" num="1.2">Toi que Mars favorise à son grand jeu de paume,</l>
					<l n="3" num="1.3">Bismark, toi qui t’es fait le valet d’un bourreau</l>
					<l n="4" num="1.4">Pour mieux tirer la corde où pend l’affreux couteau,</l>
					<l n="5" num="1.5">Je m’empresse en ce jour de t’offrir une épître ;</l>
					<l n="6" num="1.6">Car cet honneur t’est dû, Bismark, à plus d’un titre :</l>
					<l n="7" num="1.7">N’es-tu pas l’échanson de Guillaume ? Au festin,</l>
					<l n="8" num="1.8">Dans sa coupe tu sais mêler le sang au vin.</l>
					<l n="9" num="1.9">N’est-il pas l’écolier, toi le maître d’école ?</l>
					<l n="10" num="1.10">Professer à la cour fut de tout temps ton rôle,</l>
					<l n="11" num="1.11">Et j’aime à t’applaudir dans l’un ou l’autre emploi ;</l>
					<l n="12" num="1.12">Car tu grises Guillaume, et le bourreau c’est toi !</l>
					<l n="13" num="1.13">Oui, c’est toi, vil serpent qui sus, par ton astuce,</l>
					<l n="14" num="1.14">Sur la France attirer la haine de la Prusse.</l>
					<l n="15" num="1.15">Tu peux être assuré, sans entrer à Paris,</l>
					<l n="16" num="1.16">Qu’on te rend dent pour dent et mépris pour mépris.</l>
					<l n="17" num="1.17">Du mépris, c’est trop peu pour l’homme sanguinaire</l>
					<l n="18" num="1.18">Qui veut de mon pays faire un vaste ossuaire,</l>
					<l n="19" num="1.19">Qui veut anéantir la cité des beaux-arts</l>
					<l n="20" num="1.20">En lançant des obus par-dessus nos remparts.</l>
					<l n="21" num="1.21">Voilà plus de six mois, misérable vampire,</l>
					<l n="22" num="1.22">Que le sang coule, hélas ! que ta bouche l’aspire.</l>
					<l n="23" num="1.23">Pour te complaire au crime et dormir sans remord,</l>
					<l n="24" num="1.24">Tu dus faire en secret un pacte avec la mort :</l>
					<l n="25" num="1.25">N’est-ce pas toi, Satan, qui transportas Guillaume</l>
					<l n="26" num="1.26">Sur un mont élevé, non loin de son royaume ?</l>
					<l n="27" num="1.27">Tu lui disais : « Vois-tu ce pays florissant ?</l>
					<l n="28" num="1.28">C’est la France !… Elle craint ton bras ferme et puissant.</l>
					<l n="29" num="1.29">Tu peux la ravager ; j’y puis lancer la flamme ;</l>
					<l n="30" num="1.30">A Satan livre-toi ! Livre ton corps, ton âme !</l>
					<l n="31" num="1.31">Laisse pour guerroyer ta femme, ton château,</l>
					<l n="32" num="1.32">Et la France est à toi, je t’en fais le cadeau !</l>
					<l n="33" num="1.33">Avec tes fils suis-moi vers les champs de la gloire ;</l>
					<l n="34" num="1.34">Qu’ils traversent le Rhin, qu’ils traversent la Loire !</l>
					<l n="35" num="1.35">Tout ton peuple est armé, qu’il subisse les lois</l>
					<l n="36" num="1.36">De la guerre !… En avant ! les Saxons, les Badois,</l>
					<l n="37" num="1.37">Pour former une armée immense, colossale !</l>
					<l n="38" num="1.38">Contemple donc Strasbourg !… Vois-tu sa cathédrale ?</l>
					<l n="39" num="1.39">— Strasbourg !… si je l’avais !… il me la faut, morbleu !…</l>
					<l n="40" num="1.40">— Guillaume, tu l’auras vivant ou bien en feu.</l>
					<l n="41" num="1.41">Je puis donner au roi la moitié de la terre ;</l>
					<l n="42" num="1.42">Aujourd’hui prends la France !… A demain l’Angleterre !…»</l>
				</lg>
				<lg n="2">
					<l n="43" num="2.1">Et Guillaume suivit tous tes conseils, Satan.</l>
					<l n="44" num="2.2">Sur un cheval fougueux il partit à l’instant :</l>
					<l n="45" num="2.3">Son regard était fauve ; il avait la moustache</l>
					<l n="46" num="2.4">Épaisse comme un bois qui n’a point vu la hache.</l>
					<l n="47" num="2.5">Partout, à son approche, on reculait d’horreur ;</l>
					<l n="48" num="2.6">Car il portait partout le sabre et la terreur.</l>
					<l n="49" num="2.7">Les vierges à ses pieds tombaient échevelées,</l>
					<l n="50" num="2.8">Les vieillards chancelants, le mères désolées,</l>
					<l n="51" num="2.9">Nous subissaient l’outrage… Et toi, Bismark, et toi,</l>
					<l n="52" num="2.10">Joyeux tu laissais prendre un bain de sang au roi…</l>
				</lg>
				<lg n="3">
					<l n="53" num="3.1">Un bain ne calme pas la fureur souveraine ;</l>
					<l n="54" num="3.2">Il prit des bains de sang en Alsace, en Lorraine,</l>
					<l n="55" num="3.3">Et plus il s’y plongea, plus le cœur lui brûlait ;</l>
					<l n="56" num="3.4">Ce n’était plus un cœur, non, c’était un boulet !…</l>
					<l n="57" num="3.5">Et ce boulet de fer, qu’un tison rouge allume,</l>
					<l n="58" num="3.6">C’est toi qui l’as forgé, Bismark, sur ton enclume.</l>
					<l n="59" num="3.7">Tu le verras faillir ; il touche à son déclin ;</l>
					<l n="60" num="3.8">Il n’est point de boulet roulant toujours sans fin.</l>
					<l n="61" num="3.9">Puisse-t-il rencontrer ta tête sur la route</l>
					<l n="62" num="3.10">Et la broyer d’un choc !… Mais pas encore !… Écoute,</l>
					<l n="63" num="3.11">Des soldats expirants ont proféré ton nom :</l>
					<l n="64" num="3.12">« Bismarck n’aura jamais les honneurs du canon.</l>
					<l n="65" num="3.13">S’il meurt, lui qui sema les horreurs de la guerre,</l>
					<l n="66" num="3.14">Que sa mort soit des plus horribles sur la terre !</l>
					<l n="67" num="3.15">Le sol boit notre sang ; il entend nos clameurs ;</l>
					<l n="68" num="3.16">Nos mères ont versé jusqu’à leurs derniers pleurs ;</l>
					<l n="69" num="3.17">On a su les réduire, avec la violence,</l>
					<l n="70" num="3.18">A servir des Prussiens qui brandissent la lance ;</l>
					<l n="71" num="3.19">Et pas un seul de nous n’est là pour protéger</l>
					<l n="72" num="3.20">Celle qui crie : « A moi !… Mon fils, viens me venger !… »</l>
				</lg>
				<lg n="4">
					<l n="73" num="4.1">« Moi, Prussien, on m’a dit : Le maître du royaume</l>
					<l n="74" num="4.2">T’ordonne de quitter ton toit couvert de chaume.</l>
					<l n="75" num="4.3">Il laissa sa famille, il faut en faire autant.</l>
					<l n="76" num="4.4">— Mon fils est au berceau. — Dieu veille sur l’enfant !</l>
					<l n="77" num="4.5">— Mais ma femme m’étreint en me couvrant de larmes,</l>
					<l n="78" num="4.6">Comment l’abandonner ? — En saisissant tes armes.</l>
					<l n="79" num="4.7">Le roi veut des guerriers… Allons ! drôle, partons !… »</l>
					<l n="80" num="4.8">Et j’ai suivi Guillaume avec ses bataillons…</l>
					<l n="81" num="4.9">Je ressentis bientôt ce qu’un soldat endure,</l>
					<l n="82" num="4.10">La faim, hélas ! la faim sur une couche dure.</l>
					<l n="83" num="4.11">« Cherche ton pain ! m’a dit un vieux sergent rétif :</l>
					<l n="84" num="4.12">On n’a rien en dormant ; montre-toi plus actif.</l>
					<l n="85" num="4.13">Ce n’est pas au soleil qui naquit l’émeraude ;</l>
					<l n="86" num="4.14">Elle est au fond du sol. Va donc à la maraude !</l>
					<l n="87" num="4.15">Le pillage est ton droit, le meurtre est un besoin ;</l>
					<l n="88" num="4.16">Bismarck a tout permis quand nous serions au loin… »</l>
				</lg>
				<lg n="5">
					<l n="89" num="5.1">Le mal qu’on nous conseille, on l’apprend bien sans livre,</l>
					<l n="90" num="5.2">Je l’ai commis tout seul ; le crime m’a fait vivre.</l>
					<l n="91" num="5.3">J’ai même eu du plaisir à voir souffrir autrui,</l>
					<l n="92" num="5.4">En riant du fléau qui me frappe aujourd’hui.</l>
					<l n="93" num="5.5">Oh ! la guerre ! la guerre ! ignominie atroce !</l>
					<l n="94" num="5.6">Elle fait l’assassin et la bête féroce.</l>
					<l n="95" num="5.7">La guerre m’enseigna les crimes les plus grands :</l>
					<l n="96" num="5.8">J’ai tué les vieillards ; j’ai tué les enfants ;</l>
					<l n="97" num="5.9">J’ai pillé les autels et, chose plus infâme,</l>
					<l n="98" num="5.10">J’ai !… Devinez le mot pour l’honneur de ma femme…</l>
				</lg>
				<lg n="6">
					<l n="99" num="6.1">Enivrés par la poudre, et bravant le trépas,</l>
					<l n="100" num="6.2">Tous les Prussiens marchaient vers Paris à grands pas.</l>
					<l n="101" num="6.3">« Un héros ne meurt point ! disais-je en mon délire !</l>
					<l n="102" num="6.4">Mais un scélérat tombe, on peut le lui prédire. »</l>
					<l n="103" num="6.5">Frappé par une balle, on me vit chanceler,</l>
					<l n="104" num="6.6">Me roidir sur la neige, et tout bas appeler…</l>
					<l n="105" num="6.7">Nul n’entendait ma voix… Passait une corneille :</l>
					<l n="106" num="6.8">Elle vint… et son bec me déchira l’oreille.</l>
					<l n="107" num="6.9">Resté seul, près de moi s’élevaient des tombeaux</l>
					<l n="108" num="6.10">Sur le tertre desquels planaient d’affreux corbeaux.</l>
					<l n="109" num="6.11">Et je les comparais, tous ces oiseaux voraces,</l>
					<l n="110" num="6.12">Aux monarques unis pour détruire les races…</l>
					<l n="111" num="6.13">Je vis un homme en deuil… un second et puis trois ;</l>
					<l n="112" num="6.14">Ils semblaient murmurer contre le jeu des rois.</l>
					<l n="113" num="6.15">C’étaient des gens âgés, d’assez forte encolure,</l>
					<l n="114" num="6.16">N’ayant pour tout habit qu’une robe de bure<ref type="noteAnchor" target="1">1</ref>.</l>
					<l n="115" num="6.17">Ils enterraient les morts avec un saint respect,</l>
					<l n="116" num="6.18">Et moi, vil criminel, tremblant à leur aspect,</l>
					<l n="117" num="6.19">J’évitais leurs regards… L’un d’eux me dit : « Mon frère,</l>
					<l n="118" num="6.20">Au péril nous venons doucement vous soustraire ;</l>
					<l n="119" num="6.21">Laissez-moi vous panser… » J’eus un moment d’émoi ;</l>
					<l n="120" num="6.22">Car j’étais étonné qu’on eut pitié de moi,</l>
					<l n="121" num="6.23">De moi qui, l’avant-veille et par un temps de bise,</l>
					<l n="122" num="6.24">Avais pris aux blessés tout, jusqu’à leur chemise…</l>
				</lg>
				<lg n="7">
					<l n="123" num="7.1">Bismark, je te maudis ! Guillaume, je te hais !</l>
					<l n="124" num="7.2">Vous avez diffamé tous deux le cœur français.</l>
					<l n="125" num="7.3">Il est bon, généreux et rempli de clémence.</l>
					<l n="126" num="7.4">On ne fusille point les prisonniers en France.</l>
					<l n="127" num="7.5">J’ai vu les brancardiers, ils étaient plus d’un cent,</l>
					<l n="128" num="7.6">Ramasser les blessés sous un feu menaçant.</l>
					<l n="129" num="7.7">En Prusse, on est cruel ; du mal on est complice.</l>
					<l n="130" num="7.8">S’il faut prochainement que ma mort s’accomplisse</l>
					<l n="131" num="7.9">Au profit de Guillaume et du nouveau Tristan</l>
					<l n="132" num="7.10">Qu’on appelle Bismark, je lui dirai : « Satan,</l>
					<l n="133" num="7.11">Je veux par les deux pieds que le peuple te pende !</l>
					<l n="134" num="7.12">Si tu pousses des cris, qu’un vautour les entende !</l>
					<l n="135" num="7.13">Qu’il te crève chaque œil pour en chasser l’éclair !</l>
					<l n="136" num="7.14">Qu’il t’arrache le foie et s’en repaisse en l’air !</l>
					<l n="137" num="7.15">Qu’il revienne t’ôter les entrailles fumantes !</l>
					<l n="138" num="7.16">Que tout ton corps, Bismark, soit de chairs palpitantes !</l>
					<l n="139" num="7.17">Et pour les consumer, qu’un enfer dévorant</l>
					<l n="140" num="7.18">S’entr’ouvre avec fureur !… C’est le vœu d’un mourant… »</l>
				</lg>
				<closer>
					<note type="footnote" id="1">Les Frères de la doctrine chrétienne dont la conduite et le dévouement ont mérité les plus grands éloges.</note>
					<dateline>
						<date when="1871">Janvier 1871</date>
					</dateline>
				</closer>
			</div></body></text></TEI>