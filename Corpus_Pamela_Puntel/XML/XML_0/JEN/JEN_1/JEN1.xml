<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">APRÈS LA BATAILLE</title>
				<title type="medium">Édition électronique</title>
				<author key="JEN">
					<name>
						<forename>Marie</forename>
						<surname>JENNA</surname>
					</name>
					<date from="1834" to="1887">1834-1887</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>125 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">JEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>APRÈS LA BATAILLE</title>
						<author>MARIE JENNA</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb31195782v</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>APRÈS LA BATAILLE</title>
								<author>MARIE JENNA</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="JEN1">
				<head type="main">APRÈS LA BATAILLE</head>
				<lg n="1">
					<head type="main">UN BLESSÉ</head>
					<l n="1" num="1.1">Si je pouvais enfin me lever !… vain effort !</l>
					<l n="2" num="1.2">Ah ! ce qu'ils m'ont mis là, je le sens, c'est la mort.</l>
					<l n="3" num="1.3">Au cœur un poids m'étouffe et la fière m'altère…</l>
					<l n="4" num="1.4">Viendront-ils ?… Comme elle est dure et froide, la terre !</l>
					<l n="5" num="1.5">Au loin le canon gronde… oh ! qu'ont-ils fait là-bas !</l>
					<l n="6" num="1.6">Ils sont vainqueurs peut-être… et je ne le sais pas !</l>
				</lg>
				<lg n="2">
					<head type="main">UN CŒUR D'ANGES</head>
					<l n="7" num="2.1">Le vent qui traverse la plaine</l>
					<l n="8" num="2.2">A du poison dans son haleine,</l>
					<l n="9" num="2.3">Et du fond de l'immense arène</l>
					<l n="10" num="2.4">Montent des plaintes et des cris.</l>
					<l n="11" num="2.5">Sur le sol gisent les débris</l>
					<l n="12" num="2.6">D'un gigantesque sacrifice.</l>
					<l n="13" num="2.7">Ah ! quel souffle a passé dans l'air ?</l>
					<l n="14" num="2.8">Du Seigneur est-ce la justice ?</l>
					<l n="15" num="2.9">Est-ce la haine de l'enfer ?</l>
				</lg>
				<lg n="3">
					<head type="main">LE BLESSÉ</head>
					<l n="16" num="3.1">Que je souffre, mon Dieu ! mon sein brûle… Oh ! la guerre !</l>
					<l n="17" num="3.2">Quoi ! mourir sans secours ! mourir ici ! ma mère !…</l>
					<l n="18" num="3.3">Tu partirais, je crois, sans attendre un instant,</l>
					<l n="19" num="3.4">Mère, si tu savais qu'il est là, ton enfant.</l>
				</lg>
				<lg n="4">
					<head type="main">LES ANGES</head>
					<l n="20" num="4.1">Restons, car on souffre où nous sommes.</l>
					<l n="21" num="4.2">O lieu funeste, horrible champ !</l>
					<l n="22" num="4.3">Ses moissons sont des membres d'hommes</l>
					<l n="23" num="4.4">Et son fleuve coule du sang.</l>
					<l n="24" num="4.5">Des frissons, des luttes étranges</l>
					<l n="25" num="4.6">Au fond s'agitent par instant,</l>
					<l n="26" num="4.7">Et la sérénité des anges</l>
					<l n="27" num="4.8">Se troublerait en le voyant.</l>
				</lg>
				<lg n="5">
					<head type="main">LE BLESSÉ</head>
					<l n="28" num="5.1">Oh ! tu me guérirais ! que n'as-tu pu me suivre ?</l>
					<l n="29" num="5.2">Mourir à vingt-cinq ans ! j'aurais tant voulu vivre !</l>
					<l n="30" num="5.3">Vous qui par charité relevez les mourants,</l>
					<l n="31" num="5.4">Emportez-moi… je souffre… est-ce vous que j'entends ?</l>
				</lg>
				<lg n="6">
					<head type="main">LES ANGES</head>
					<l n="32" num="6.1">Descendons, soulageons leur peine,</l>
					<l n="33" num="6.2">Faisons monter tous les soupirs.</l>
					<l n="34" num="6.3">De ces mourants brisons la chaîne ;</l>
					<l n="35" num="6.4">Sous les monceaux de chair humaine</l>
					<l n="36" num="6.5">Cherchons les âmes des martyrs !</l>
				</lg>
				<lg n="7">
					<head type="main">LE BLESSÉ</head>
					<l n="37" num="7.1">Un peu d'eau… j'ai bien soif ! un lit.… tout m'abandonne.</l>
					<l n="38" num="7.2">Il faut mourir ici… Mourir !… ô mon Yvonne !</l>
					<l n="39" num="7.3">Sur le bord du chemin tu m'attendras longtemps…</l>
					<l n="40" num="7.4">Et nous aurions été dans cinq mois si contents !</l>
					<l n="41" num="7.5">L'autre jour en tremblant tu soulevais mes armes…</l>
					<l n="42" num="7.6">Ah ! tes yeux, tes beaux yeux en verseront des larmes,</l>
					<l n="43" num="7.7">Quand tu verras mon nom dans la liste des morts.</l>
					<l n="44" num="7.8">On nous a vus partir si braves et si forts !</l>
				</lg>
				<lg n="8">
					<head type="main">LES ANGES</head>
					<l n="45" num="8.1">Si la félicité passée</l>
					<l n="46" num="8.2">Pouvait lui faire illusion !</l>
					<l n="47" num="8.3">Berçons un instant sa pensée</l>
					<l n="48" num="8.4">D'une lointaine vision.</l>
				</lg>
				<lg n="9">
					<head type="main">LE BLESSÉ</head>
					<l n="49" num="9.1">O ma lande fleurie, ô mes champs, mon village !</l>
					<l n="50" num="9.2">Jardin que j'ai planté plein de fleurs et d'ombrage,</l>
					<l n="51" num="9.3">Mes bœufs et mes brebis… ô le repas du soir,</l>
					<l n="52" num="9.4">Quand après la fatigue il fait si bon s'asseoir !</l>
					<l n="53" num="9.5">Les récits du foyer, les cloches du dimanche,</l>
					<l n="54" num="9.6">Dans l'église aux grands jours Yvonne en robe blanche,</l>
					<l n="55" num="9.7">Les compagnons joyeux… et ma petite sœur</l>
					<l n="56" num="9.8">Qui d'un coquelicot me fit la croix d'honneur !</l>
					<l n="57" num="9.9">Ma vie eût été belle, et la voilà finie.</l>
					<l n="58" num="9.10">Cloches de mon pays, sonnez mon agonie…</l>
				</lg>
				<lg n="10">
					<head type="main">LES ANGES</head>
					<l n="59" num="10.1">Du grand sommeil il va dormir,</l>
					<l n="60" num="10.2">A vous, Jésus, faites qu'il pense,</l>
					<l n="61" num="10.3">Et que la céleste espérance</l>
					<l n="62" num="10.4">Le console enfin de mourir.</l>
				</lg>
				<lg n="11">
					<head type="main">LE BLESSÉ</head>
					<l n="63" num="11.1">Mon Dieu qui me voyez, mon seigneur et mon maître,</l>
					<l n="64" num="11.2">A votre jugement bientôt je vais paraître,</l>
					<l n="65" num="11.3">Et je vous oubliais quand je n'ai plus que vous.</l>
					<l n="66" num="11.4">Hélas ! j'ai tant péché ! mais vous m'avez absous.</l>
					<l n="67" num="11.5">Ouvrez-moi votre sein… Je meurs pour ma patrie !</l>
					<l n="68" num="11.6">Ma mère brûle un cierge à la vierge Marie ;</l>
					<l n="69" num="11.7">La médaille d' Yvonne, elle est là… sur mon cœur.</l>
					<l n="70" num="11.8">Mêlez mon sang qui coule au sang de mon Sauveur.</l>
				</lg>
				<lg n="12">
					<head type="main">L'ANGE DE LA FRANCE</head>
					<l n="71" num="12.1">Jéhovah ! Dieu de la victoire,</l>
					<l n="72" num="12.2">De la force unique soutien,</l>
					<l n="73" num="12.3">Vous aviez couronné de gloire</l>
					<l n="74" num="12.4">Votre royaume très-chrétien.</l>
					<l n="75" num="12.5">Son bras puissant, son âme fière</l>
					<l n="76" num="12.6">Étaient soumis a votre loi,</l>
					<l n="77" num="12.7">Et des anges l'armée entière</l>
					<l n="78" num="12.8">Aux cieux s'inclinait devant moi.</l>
				</lg>
				<lg n="13">
					<l n="79" num="13.1">Oh ! la France, qu'elle était belle,</l>
					<l n="80" num="13.2">Parcourant la rive infidèle</l>
					<l n="81" num="13.3">Ainsi qu'un lion qui bondit !</l>
					<l n="82" num="13.4">Semant sur les plages lointaines</l>
					<l n="83" num="13.5">Son or et le sang de ses veines</l>
					<l n="84" num="13.6">Pour le tombeau de Jésus-Christ !</l>
					<l n="85" num="13.7">Puis, à l'heure de la prière,</l>
					<l n="86" num="13.8">Humble et douce comme un enfant,</l>
					<l n="87" num="13.9">Inclinant sur l'auguste Pierre</l>
					<l n="88" num="13.10">Son front blessé mais triomphant.</l>
					<l n="89" num="13.11">Par saint Louis je vous implore !</l>
				</lg>
				<lg n="14">
					<l n="90" num="14.1">Mon Dieu, qu'elle était belle encore</l>
					<l n="91" num="14.2">Quand, se levant de ses douleurs,</l>
					<l n="92" num="14.3">Et mettant son vainqueur en fuite,</l>
					<l n="93" num="14.4">Elle s'élançait à la suite</l>
					<l n="94" num="14.5">De la vierge de Vaucouleurs !</l>
					<l n="95" num="14.6">Par Jeanne d'Arc, pitié pour elle !</l>
					<l n="96" num="14.7">Oh ! la France, qu'elle était belle</l>
					<l n="97" num="14.8">Lorsque dans sa fidélité,</l>
					<l n="98" num="14.9">Défiant Pilate et Caïphe,</l>
					<l n="99" num="14.10">De Rome et de son roi-pontife</l>
					<l n="100" num="14.11">Elle abritait la majesté !</l>
				</lg>
				<lg n="15">
					<l n="101" num="15.1">Jéhovah, Dieu de la victoire,</l>
					<l n="102" num="15.2">De la force unique soutien,</l>
					<l n="103" num="15.3">Vous aviez couronné de gloire</l>
					<l n="104" num="15.4">Votre royaume très-chrétien.</l>
					<l n="105" num="15.5">Son bras puissant, son âme fière</l>
					<l n="106" num="15.6">Étaient soumis à votre loi,</l>
					<l n="107" num="15.7">Et des anges l'armée entière</l>
					<l n="108" num="15.8">Aux cieux s'inclinait devant moi.</l>
				</lg>
				<lg n="16">
					<head type="main">LE BLESSÉ</head>
					<l n="109" num="16.1">Où suis-je ?… Ah ! l'ennemi… le tambour… il s'avance !</l>
					<l n="110" num="16.2">Présent, mon général ! marchons ! vive la France !</l>
					<l n="111" num="16.3">Je me lève… attendez… Guillaume, gare à toi !</l>
					<l n="112" num="16.4">Attendez je ne puis… qui me tient ?… laissez-moi !</l>
					<l n="113" num="16.5">Mais non ! je vais mourir et c'est Dieu qui m'appelle.</l>
					<l n="114" num="16.6">Mes amis, vous prierez pour moi dans la chapelle.</l>
					<l n="115" num="16.7">Je ne vous verrai plus… ma mère… Yvonne… adieu !</l>
					<l n="116" num="16.8">Là-haut mon heure sonne, et me voici, mon Dieu !</l>
				</lg>
				<lg n="17">
					<head type="main">LES ANGES</head>
					<l n="117" num="17.1">Oui, Dieu l'appelle… en sa présence</l>
					<l n="118" num="17.2">Le vaincu monte glorieux.</l>
					<l n="119" num="17.3">Que dans l'éternelle balance</l>
					<l n="120" num="17.4">Où le Ciel a pesé la France</l>
					<l n="121" num="17.5">Soit versé ce sang généreux.</l>
					<l n="122" num="17.6">Et pour qu'un rayon salutaire</l>
					<l n="123" num="17.7">La prépare à son triste sort,</l>
					<l n="124" num="17.8">Allons dire à la pauvre mère</l>
					<l n="125" num="17.9">Que peut-être son fils est mort.</l>
				</lg>
			</div></body></text></TEI>