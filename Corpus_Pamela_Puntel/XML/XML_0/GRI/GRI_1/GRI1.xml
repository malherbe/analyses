<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">REVUE DE BRETAGNE ET DE VENDÉE</title>
				<title type="sub_2">QUATORZIÈME ANNÉE</title>
				<title type="sub_1">TROISIÈME SÉRIE — TOME VIII</title>
				<title type="medium">Édition électronique</title>
				<author key="GRI">
					<name>
						<forename>Émile</forename>
						<surname>GRIMAUD</surname>
					</name>
					<date from="1831" to="1901">1831-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>413 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">STROPHES PATRIOTIQUES</title>
						<author>ÉMILE GRIMAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k453762t.r=Revue%20de%20Bretagne%20et%20de%20Vend%C3%A9e%201870?rk=85837;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>REVUE DE BRETAGNE ET DE VENDÉE</title>
								<imprint>
									<pubPlace>NANTES</pubPlace>
									<publisher>REVUE DE BRETAGNE ET DE VENDÉE — ANNÉE 1870 — DEUXIÈME SEMESTRE</publisher>
									<date when="1870">OCTOBRE 1870</date>
								</imprint>
								<biblScope unit="tome">T. 28</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-21" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Octobre 1870</head><div type="poem" key="GRI1">
					<head type="main">LA MARSEILLAISE VENDÉENNE</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><space unit="char" quantity="8"/>Quelle tempête furieuse !</l>
							<l n="2" num="1.2"><space unit="char" quantity="8"/>Le monde entier en retentit.</l>
							<l n="3" num="1.3"><space unit="char" quantity="8"/>France, autrefois si glorieuse !</l>
							<l n="4" num="1.4"><space unit="char" quantity="8"/>C'est l'adieu d'un tyran maudit !</l>
							<l n="5" num="1.5"><space unit="char" quantity="8"/>Puisqu'il nous faut payer ses crimes,</l>
							<l n="6" num="1.6"><space unit="char" quantity="8"/>En six mois expier vingt ans,</l>
							<l n="7" num="1.7"><space unit="char" quantity="8"/>Debout !… Nous devons être grands,</l>
							<l n="8" num="1.8"><space unit="char" quantity="8"/>Quand nos aïeux furent sublimes.</l>
							<l n="9" num="1.9">Aux armes, Vendéens ! La France est en danger !</l>
							<l n="10" num="1.10">Marchons, fils des géants, et chassons l'étranger !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="11" num="1.1"><space unit="char" quantity="8"/>Arrière, ô barbare, qui souilles</l>
							<l n="12" num="1.2"><space unit="char" quantity="8"/>Le sol où nous avons vécu !</l>
							<l n="13" num="1.3"><space unit="char" quantity="8"/>Tu le crois sûr de nos dépouilles,</l>
							<l n="14" num="1.4"><space unit="char" quantity="8"/>Et c'est toi qui seras vaincu !</l>
							<l n="15" num="1.5"><space unit="char" quantity="8"/>Tout le pays des Francs se lève :</l>
							<l n="16" num="1.6"><space unit="char" quantity="8"/>Les Francs sont prêts à tout souffrir ;</l>
							<l n="17" num="1.7"><space unit="char" quantity="8"/>Jusqu'au dernier plutôt mourir,</l>
							<l n="18" num="1.8"><space unit="char" quantity="8"/>Que de nous courber sous ton glaive !</l>
							<l n="19" num="1.9">Aux armes, Vendéens ! La France est en danger !</l>
							<l n="20" num="1.10">Marchons, fils des géants, et chassons l'étranger !</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>

						<lg n="1">
							<l n="21" num="1.1"><space unit="char" quantity="8"/>Lance l'obus, lance la bombe,</l>
							<l n="22" num="1.2"><space unit="char" quantity="8"/>Mitraille enfants, femmes, vieillards :</l>
							<l n="23" num="1.3"><space unit="char" quantity="8"/>Si l'immortel Strasbourg succombe,</l>
							<l n="24" num="1.4"><space unit="char" quantity="8"/>Paris t'attend sous ses remparts.</l>
							<l n="25" num="1.5"><space unit="char" quantity="8"/>Oui ! c'est là que la main divine</l>
							<l n="26" num="1.6"><space unit="char" quantity="8"/>Brisera tes fiers bataillons</l>
							<l n="27" num="1.7"><space unit="char" quantity="8"/>Et nous, travailleurs des sillons</l>
							<l n="28" num="1.8">Nous achèverons ta ruine</l>
							<l n="29" num="1.9">Aux armes, Vendéens ! La France est en danger !</l>
							<l n="30" num="1.10">Marchons, fils des géants, et chassons l'étranger !</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<lg n="1">
							<l n="31" num="1.1"><space unit="char" quantity="8"/>Du ciel inspirez à notre âme,</l>
							<l n="32" num="1.2"><space unit="char" quantity="8"/>Bonchamps, Cathelineau, Stofflet,</l>
							<l n="33" num="1.3"><space unit="char" quantity="8"/>Pour cet œuvre inspirez la flamme ;</l>
							<l n="34" num="1.4"><space unit="char" quantity="8"/>Dont votre âme antique brûlait</l>
							<l n="35" num="1.5"><space unit="char" quantity="8"/>Et que notre égide, ô Marie,</l>
							<l n="36" num="1.6"><space unit="char" quantity="8"/>Nous couvre au milieu des combats :</l>
							<l n="37" num="1.7"><space unit="char" quantity="8"/>Guidez nos coups, guidez nos pas ;</l>
							<l n="38" num="1.8"><space unit="char" quantity="8"/>Par nos mains sauvez la patrie !…</l>
							<l n="39" num="1.9">Aux armes, Vendéens ! La France est en danger !</l>
							<l n="40" num="1.10">Marchons, fils des géants, et chassons l'étranger !</l>
						</lg>
					</div>
					<closer>
						<dateline>
							<date when="1870">Nantes, 7 octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>