<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">REVUE DE BRETAGNE ET DE VENDÉE</title>
				<title type="sub_2">QUATORZIÈME ANNÉE</title>
				<title type="sub_1">TROISIÈME SÉRIE — TOME VIII</title>
				<title type="medium">Édition électronique</title>
				<author key="GRI">
					<name>
						<forename>Émile</forename>
						<surname>GRIMAUD</surname>
					</name>
					<date from="1831" to="1901">1831-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>413 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">GRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">STROPHES PATRIOTIQUES</title>
						<author>ÉMILE GRIMAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k453762t.r=Revue%20de%20Bretagne%20et%20de%20Vend%C3%A9e%201870?rk=85837;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>REVUE DE BRETAGNE ET DE VENDÉE</title>
								<imprint>
									<pubPlace>NANTES</pubPlace>
									<publisher>REVUE DE BRETAGNE ET DE VENDÉE — ANNÉE 1870 — DEUXIÈME SEMESTRE</publisher>
									<date when="1870">OCTOBRE 1870</date>
								</imprint>
								<biblScope unit="tome">T. 28</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-21" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Décembre 1870</head><div type="poem" key="GRI5">
					<head type="main">LES DEUX ÉPÉES</head>
					<lg n="1">
						<l n="1" num="1.1">Paris est submergé par les vagues de l'ombre ;</l>
						<l n="2" num="1.2">Sur son front endormi les étoiles sans nombre</l>
						<l n="3" num="1.3">Agitent leurs traits d'or dans les cieux clairs et froids ;</l>
						<l n="4" num="1.4">Un vent âpre poursuit sa plainte monotone,</l>
						<l n="5" num="1.5">Et nul bruit ne s'y joint, que le canon qui tonne,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"/>Ou l'heure vibrant aux beffrois.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Drapés dans des manteaux que soulève la bise,</l>
						<l n="8" num="2.2">Deux soldats ont ouvert la porte d'une église ;</l>
						<l n="9" num="2.3">Dés le seuil même ils sont frappés d'un saint respect :</l>
						<l n="10" num="2.4">Ils sortent de la nuit, du silence des tombes ;</l>
						<l n="11" num="2.5">Les voilà transportés au temps des catacombes…</l>
						<l n="12" num="2.6"><space unit="char" quantity="8"/>Pour eux quel saisissant aspect !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Dans le champ de la nef tout est sombre et sans formes ;</l>
						<l n="14" num="3.2">Les piliers vaguement dressent leurs fûts énormes ;</l>
						<l n="15" num="3.3">C'est d'un bois, à minuit, l'épaisse obscurité ;</l>
						<l n="16" num="3.4">Mais là-bas leur regard, mais leur âme ravie</l>
						<l n="17" num="3.5">S'élance : au fond du cœur tout est splendeur et vie,</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"/>Humaine et mystique clarté.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Parmi les fleurs de neige et les rayons des cierges,</l>
						<l n="20" num="4.2">La Mère de Jésus, la Vierge entre les vierges,</l>
						<l n="21" num="4.3">Est debout et sourit en son chaste maintien.</l>
						<l n="22" num="4.4">Broyant de ses pieds nus le tentateur immonde,</l>
						<l n="23" num="4.5">Elle abaisse ses yeux, que la douceur inonde,</l>
						<l n="24" num="4.6"><space unit="char" quantity="8"/>Vers la terre, vers le chrétien.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">— Recours des affligés, ô clémente Marie,</l>
						<l n="26" num="5.2">Vous qu'une humble oraison a toujours attendrie,</l>
						<l n="27" num="5.3">De vos deux serviteurs, prosternés à genoux,</l>
						<l n="28" num="5.4">Ah ! secondez les vœux et la vive prière ;</l>
						<l n="29" num="5.5">Ils vous disent : « Pendant la lutte meurtrière,</l>
						<l n="30" num="5.6"><space unit="char" quantity="8"/>» Reine du ciel, protégez-nous ! »</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Pourriez-vous les entendre avec indifférence ?</l>
						<l n="32" num="6.2">Si vous régnez aux cieux, vous régnez sur la France ;</l>
						<l n="33" num="6.3">Nous vous avons voué d'innombrables autels…</l>
						<l n="34" num="6.4">Notre vaisseau se tord sous les flots, sous l'orage :</l>
						<l n="35" num="6.5">L'abandonnerez-vous en proie à tant de rage ?</l>
						<l n="36" num="6.6"><space unit="char" quantity="8"/>Ces coups sont-ils des coups mortels ?</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Étoile de la mer, le péril est extrême !</l>
						<l n="38" num="7.2">Le jour luira bientôt… c'est notre jour suprême !</l>
						<l n="39" num="7.3">Que nous apporte-t-il : le triomphe ou l'écueil ?</l>
						<l n="40" num="7.4">Du haut de nos destins nous fera-t-il descendre ?</l>
						<l n="41" num="7.5">No va-t-il subsister de nous qu'un peu de cendre,</l>
						<l n="42" num="7.6"><space unit="char" quantity="8"/>Qu'un souvenir dans un cercueil ?</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Nous avons irrité le doux Sauveur des hommes ;</l>
						<l n="44" num="8.2">Intercédez pour nous, fils des Francs, nous qui sommes,</l>
						<l n="45" num="8.3">Nous qui voulons rester de Dieu les vrais soldats,</l>
						<l n="46" num="8.4">Et poursuivre jamais notre rôle sublime :</l>
						<l n="47" num="8.5">Il est temps ! nous roulons au plus bas de l'abîme ;</l>
						<l n="48" num="8.6"><space unit="char" quantity="8"/>A vos enfants tendez les bras !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Vous qui par Jeanne d'Arc avez sauvé la France,</l>
						<l n="50" num="9.2">Devant ces deux guerriers, notre unique espérance,</l>
						<l n="51" num="9.3">Mendiants à vos pieds courbant leurs fronts soumis,</l>
						<l n="52" num="9.4">Gémissant et pleurant des pleurs expiatoires,</l>
						<l n="53" num="9.5">Envoyez saint Michel, ô Dame-des-Victoires,</l>
						<l n="54" num="9.6"><space unit="char" quantity="8"/>Et c'en est fait des ennemis !</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">Trochu ! Ducrot ! voilà des mains, des âmes pures :</l>
						<l n="56" num="10.2">Par eux, Mère, fermez nos horribles blessures ;</l>
						<l n="57" num="10.3">Du sang de votre Fils voyez-les se nourrir.</l>
						<l n="58" num="10.4">A leur fourreau le prêtre a rendu les épées,</l>
						<l n="59" num="10.5">De divines vertus abondamment trempées…</l>
						<l n="60" num="10.6"><space unit="char" quantity="8"/>— Allez, héros, vaincre ou mourir !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Nantes, 8 décembre 1870, fête de l'Immaculée-Conception.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>