<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA FRANCE NE MEURT PAS !!</title>
				<title type="sub">ODE PATRIOTIQUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DEF">
					<name>
						<forename>Paul</forename>
						<surname>DEFER</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DEF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LA FRANCE NE MEURT PAS !!, ODE PATRIOTIQUE</title>
						<author>PAUL DEFER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k6220878z.r=DEFER%20LA%20FRANCE%20NE%20MEURT%20PAS?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA FRANCE NE MEURT PAS !!, ODE PATRIOTIQUE</title>
								<author>PAUL DEFER</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DENTU</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DEF1">
				<head type="main">LA FRANCE NE MEURT PAS !!</head>
				<lg n="1">
					<l n="1" num="1.1">Les verrons-nous enfin, ces Barbares du Nord,</l>
					<l n="2" num="1.2">Vils esclaves maudits d'un Roi plus vil encor ?</l>
					<l n="3" num="1.3">Les verrons-nous enfin ces conquérants sans gloire,</l>
					<l n="4" num="1.4">Qui dans le nombre seul ont trouvé la victoire ?</l>
					<l n="5" num="1.5">Potentat plein d'orgueil, ministre sans pudeur,</l>
					<l n="6" num="1.6">Guerriers dont le grand jour excite la frayeur,</l>
					<l n="7" num="1.7">Qui combattez la nuit, rampant dans les broussailles,</l>
					<l n="8" num="1.8">Ou le matin, blottis derrière vos murailles,</l>
					<l n="9" num="1.9">Montrez-vous au soleil ! face à face, un contre un !</l>
					<l n="10" num="1.10">Un contre un ? c'est trop peu, car nous avons chacun</l>
					<l n="11" num="1.11">Des frères à venger. Sortez de vos repaires ;</l>
					<l n="12" num="1.12">Affrontez le danger, despotes sanguinaires ;</l>
					<l n="13" num="1.13">LA POPULACE est prête, elle attend le combat !</l>
					<l n="14" num="1.14">Paris n'est pas encor le peuple qu'on abat</l>
					<l n="15" num="1.15">En lui criant famine ; il sait prendre les armes</l>
					<l n="16" num="1.16">Pour sauver son honneur, et s'il verse des larmes,</l>
					<l n="17" num="1.17">S'il pleure un fils aimé par la mort ennobli,</l>
					<l n="18" num="1.18">Qu'importe sa douleur, il n'est point affaibli.</l>
					<l n="19" num="1.19">Il a dans son histoire une page sublime</l>
					<l n="20" num="1.20">Qu'il n'a pas oubliée ; et si parfois l'abîme</l>
					<l n="21" num="1.21">Entr'ouvert à ses pieds doit l'engloutir bientôt,</l>
					<l n="22" num="1.22">Il ne cédera pas, il périra plutôt.</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="2">
					<l n="23" num="2.1">Alors, Prince insensé, dans la Ville aux cent portes,</l>
					<l n="24" num="2.2">Tu ferais chevaucher tes vaillantes cohortes ;</l>
					<l n="25" num="2.3">Tu foulerais aux pieds nos souvenirs pieux ;</l>
					<l n="26" num="2.4">Tu serais Maître et Roi, presque l'égal des dieux !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="3">
					<l n="27" num="3.1">Cela ne sera pas, cela ne doit pas être !</l>
					<l n="28" num="3.2">Après Paris, vieillard, il te faudrait peut-être</l>
					<l n="29" num="3.3">Nos ports et nos cités ? La France entière encor,</l>
					<l n="30" num="3.4">Pour mettre un terme enfin à ton beau rêve d'or !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="4">
					<l n="31" num="4.1">Aux ARMES, CITOYENS ! donnons tous notre vie,</l>
					<l n="32" num="4.2">Mais de ce joug sanglant délivrons la patrie ;</l>
					<l n="33" num="4.3">Frères, levons-nous tous, le moment a sonné.</l>
					<l n="34" num="4.4">Il faut vaincre, et montrer au vieux monde étonné</l>
					<l n="35" num="4.5">Qu'un Peuple libre est fort, et que ses destinées</l>
					<l n="36" num="4.6">Dépendent de lui-même, et non de vingt années</l>
					<l n="37" num="4.7">D'un règne avilissant. Sentons battre nos cœurs !</l>
					<l n="38" num="4.8">Sus aux soldats heureux qui parlent en vainqueurs !</l>
					<l n="39" num="4.9">Sus à ce roi débile, oublié de la Parque !</l>
					<l n="40" num="4.10">A ce grand chancelier, digne de son monarque !</l>
					<l n="41" num="4.11">A ces hommes sans honte, à ces princes sans foi,</l>
					<l n="42" num="4.12">Qui croient briser la France en nous dictant leur loi !</l>
					<l n="43" num="4.13">Ils semblent de nos faits n'avoir plus la mémoire :</l>
					<l n="44" num="4.14">LA FRANCE NE MEURT PAS ! qu'ils consultent l'histoire.</l>
					<l n="45" num="4.15">Ils y verront écrits, gravés par le burin,</l>
					<l n="46" num="4.16">Pour s'immortaliser sur des tables d'airain :</l>
					<l n="47" num="4.17">Dix siècles de grandeurs, de succès et de luttes ;</l>
					<l n="48" num="4.18">Ils verront un grand Peuple, écrasé dans ses chutes,</l>
					<l n="49" num="4.19">Se relever toujours et toujours triompher ;</l>
					<l n="50" num="4.20">Ils verront ce Paris, qu'ils voudraient posséder,</l>
					<l n="51" num="4.21">Résister aux Normands, les chasser et les vaincre.</l>
					<l n="52" num="4.22">Qu'ils lisent jusqu'au bout, s'ils veulent se convaincre ;</l>
					<l n="53" num="4.23">Ils souriront peut-être aux portraits de ces rois,</l>
					<l n="54" num="4.24">Despotes fainéants, qui nous donnaient des lois.</l>
					<l n="55" num="4.25">Plus tard n'avons-nous pas, sous un règne plus sage,</l>
					<l n="56" num="4.26">Affranchi la commune, aboli l'esclavage ?</l>
					<l n="57" num="4.27">N'avons-nous pas enfin brisé le joug puissant</l>
					<l n="58" num="4.28">Des princes féodaux qui pillaient le passant ?</l>
					<l n="59" num="4.29">Lisez, lisez toujours, ô fils de Germanie ;</l>
					<l n="60" num="4.30">Oui, vous verrez encor notre chère patrie,</l>
					<l n="61" num="4.31">Livrée aux factieux, subir l'invasion</l>
					<l n="62" num="4.32">Des cruels chevaliers de la fière Albion ;</l>
					<l n="63" num="4.33">Mais, le Seigneur veillait, une femme bénie</l>
					<l n="64" num="4.34">Se levait parmi nous, et, bienfaisant génie,</l>
					<l n="65" num="4.35">Chassait les étrangers par ses nobles élans !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="5">
					<l n="66" num="5.1">Jeanne de Domrémy, Pucelle d'Orléans,</l>
					<l n="67" num="5.2">Martyre des Anglais, ton nom est à l'histoire !</l>
					<l n="68" num="5.3">Et l'histoire des faits sait garder la mémoire.</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="6">
					<l n="69" num="6.1">Le peuple avait souffert, était-il abattu ?</l>
					<l n="70" num="6.2">Non, il ne l'était pas, et sa mâle vertu</l>
					<l n="71" num="6.3">Toujours a triomphé ; toujours de grandes âmes</l>
					<l n="72" num="6.4">Ont surgi de son sein, hommes, vieillards ou femmes !</l>
					<l n="73" num="6.5">Lisez, lisez toujours : quelques années après,</l>
					<l n="74" num="6.6">C'est une femme encor qui délivrait Beauvais.</l>
					<l n="75" num="6.7">Les hommes n'étaient plus !. Et ces nobles otages,</l>
					<l n="76" num="6.8">Au siège de Calais, qui, pour vaincre les rages</l>
					<l n="77" num="6.9">D’Édouard d'Angleterre, acceptaient seuls la mort,</l>
					<l n="78" num="6.10">C'était le peuple aussi ! Quel était le plus fort</l>
					<l part="I" n="79" num="6.11">Du peuple ou du tyran ?</l>
					<l part="F" n="79" num="6.11">Je ne saurais m'étendre.</l>
					<l n="80" num="6.12">Pour ne rien oublier, il faudrait entreprendre</l>
					<l n="81" num="6.13">L'histoire toute entière, et suivre pas à pas</l>
					<l n="82" num="6.14">Tous les siècles passés ; je ne le pourrais pas…</l>
					<l n="83" num="6.15">Écartons de nos yeux les grandeurs éphémères</l>
					<l n="84" num="6.16">De ces fils d'Henri Quatre, et les longues misères</l>
					<l n="85" num="6.17">Du peuple qui souffrait du froid et de la faim,</l>
					<l n="86" num="6.18">Et voyait semer l'or quand il manquait de pain.</l>
					<l n="87" num="6.19">J'ai hâte d'arriver aux grandes épopées</l>
					<l n="88" num="6.20">Qui pour vaincre le monde ont trouvé tant d'épées :</l>
					<l n="89" num="6.21">Avons-nous succombé ? rois, de vos droits épris,</l>
					<l n="90" num="6.22">Quand cette POPULACE, objet de vos mépris,</l>
					<l n="91" num="6.23">Se levait rugissante, implacable, acharnée,</l>
					<l n="92" num="6.24">Et disait à son tour à l'Europe étonnée :</l>
					<l n="93" num="6.25">Tremblez, tremblez, tyrans ! car il n'est plus de Roi,</l>
					<l n="94" num="6.26">Il n'y a plus qu'un Peuple, et ce Peuple, c'est moi !</l>
					<l n="95" num="6.27">Avons-nous succombé ? quand la Patrie entière,</l>
					<l n="96" num="6.28">Pour vous écraser tous, courait à la frontière ?</l>
					<l n="97" num="6.29">O races d'Allemagne ! ô vaincus d'Iéna !</l>
					<l n="98" num="6.30">Aux portes de Paris vous arriviez déjà,</l>
					<l n="99" num="6.31">Quand sonna le réveil de la France endormie ;</l>
					<l n="100" num="6.32">Quand nos jeunes héros, au valeureux génie,</l>
					<l n="101" num="6.33">En vous brisant, volaient à l'immortalité,</l>
					<l n="102" num="6.34">Guidés par ces deux mots : Patrie et Liberté !</l>
					<l n="103" num="6.35">Ils étaient nos aïeux ! eh bien ! dites encore</l>
					<l n="104" num="6.36">Que la France a vécu : elle est à son aurore !</l>
					<l n="105" num="6.37">LA POPULACE attend ; saluez-la bien bas.</l>
					<l n="106" num="6.38">Avec elle, Germains, LA FRANCE NE MEURT PAS !!!</l>
				</lg>
				<ab type="dash">─────────────────────────────</ab>
				<lg n="7">
					<l n="107" num="7.1">Amis, encore un mot : on parle de traités,</l>
					<l n="108" num="7.2">D'armistice, de paix ; sommes-nous consultés ?</l>
					<l n="109" num="7.3">Devons-nous accepter cette honte nouvelle ?</l>
					<l n="110" num="7.4">Briser la Liberté sans combattre pour elle !</l>
					<l n="111" num="7.5">Briser la Liberté serait là notre sort ?</l>
					<l n="112" num="7.6">Mais la paix, aujourd'hui c'est pire que la mort.</l>
					<l n="113" num="7.7">Quoi ! ce roi conquérant nous ferait ses esclaves,</l>
					<l n="114" num="7.8">Et nous mettrait aux pieds des fers et des entraves ?</l>
					<l n="115" num="7.9">Nous ne serions plus rien, rien qu'un peuple abaissé,</l>
					<l n="116" num="7.10">Indigne du grand nom qu'on nous avait laissé ?</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="8">
					<l n="117" num="8.1">O mon pauvre Pays ! ô chère République !</l>
					<l n="118" num="8.2">A ces ambitieux donne donc la réplique ;</l>
					<l n="119" num="8.3">Dis-leur que le bon droit triomphe pas à pas ;</l>
					<l n="120" num="8.4">Qu'un Français doit mourir, qu'il ne s'avilit pas !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 3 novembre 1870</date>
					</dateline>
				</closer>
			</div></body></text></TEI>