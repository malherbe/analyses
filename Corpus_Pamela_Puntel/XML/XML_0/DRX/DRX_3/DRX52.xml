<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES PAROLES DU VAINCU</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>126 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DRX_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES PAROLES DU VAINCU</title>
						<author>LÉON DIERX</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k58363472.r=DIERX%20%20LES%20PAROLES%20DU%20VAINCU%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES PAROLES DU VAINCU</title>
								<author>LÉON DIERX</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX52">
				<head type="main">LES PAROLES DU VAINCU</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Tu rêvais paix universelle !</l>
						<l n="2" num="1.2">Tu disais : « Qu'importe un ruisseau ?</l>
						<l n="3" num="1.3">Pourquoi le globe qu'on morcelle ?</l>
						<l n="4" num="1.4">La terre immense est mon berceau ! »</l>
						<l n="5" num="1.5">A présent, tu dis : « Hors la gaîne,</l>
						<l n="6" num="1.6">Le glaive à deux mains des aïeux !</l>
						<l n="7" num="1.7">Hors des cœurs, le sang furieux !</l>
						<l n="8" num="1.8">Et vous, autour de notre haine,</l>
						<l n="9" num="1.9">Rangez-vous, impassibles Dieux ! »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="10" num="1.1">Ils tombèrent, enfin, ces braves !</l>
						<l n="11" num="1.2">Par blocs massifs aux trous béants.</l>
						<l n="12" num="1.3">Le soir vint grandir ces géants,</l>
						<l n="13" num="1.4">Ces vaincus effrayants et graves !</l>
						<l n="14" num="1.5">L'un surtout, son buste d'acier</l>
						<l n="15" num="1.6">Droit sur l'arçon, semblait attendre !</l>
						<l n="16" num="1.7">La nuit, on croit toujours l'entendre ;</l>
						<l n="17" num="1.8">Car la mort n'a point osé prendre</l>
						<l n="18" num="1.9">Son âme, à ce grand cuirassier !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="19" num="1.1">Ceux de l'Argonne et de Valmy</l>
						<l n="20" num="1.2">Sont vêtus de pourpre éclatante.</l>
						<l n="21" num="1.3">Ils souriaient, fiers, dans l'attente,</l>
						<l n="22" num="1.4">Nous criant : Sus à l'ennemi ! —</l>
						<l n="23" num="1.5">Mais toujours passaient les Barbares !</l>
						<l n="24" num="1.6">Et les vieux sonneurs de fanfares</l>
						<l n="25" num="1.7">Criaient en vain : « Debout ! les Morts !</l>
						<l n="26" num="1.8">Redonnez-nous, ô Dieux avares !</l>
						<l n="27" num="1.9">Du sang qui coule dans des corps ! »</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">IV</head>
					<lg n="1">
						<l n="28" num="1.1">Dans les soleils couchants je vois</l>
						<l n="29" num="1.2">Des ruines au nom sonore,</l>
						<l n="30" num="1.3">Dont la gloire sur nous encore</l>
						<l n="31" num="1.4">Flambe, et croule, comme autrefois !</l>
						<l n="32" num="1.5">Dans les soleils fondants j'admire,</l>
						<l n="33" num="1.6">O Paris ! les reines d'orgueil.</l>
						<l n="34" num="1.7">J'ouvre, éperdu, longtemps, mon œil.</l>
						<l n="35" num="1.8">Et je vais, criant, l'âme en deuil :</l>
						<l n="36" num="1.9">Ninive ! Ecbatane ! Palmyre !</l>
					</lg>
				</div>
				<div type="section" n="5">
					<head type="number">V</head>
					<lg n="1">
						<l n="37" num="1.1">Plus d'une fois, ta noble épée,</l>
						<l n="38" num="1.2">O Patrie ! a, de son revers,</l>
						<l n="39" num="1.3">Quelque part, fait tomber leurs fers !</l>
						<l n="40" num="1.4">De ton sang fraternel trempée,</l>
						<l n="41" num="1.5">Plus d'une plaine était en fleur,</l>
						<l n="42" num="1.6">Où l'on riait de ton malheur !</l>
						<l n="43" num="1.7">Ah ! pour que rien ne te flétrisse,</l>
						<l n="44" num="1.8">Toi, l'unique Libératrice,</l>
						<l n="45" num="1.9">Oublie aussi ; pardonnons-leur !</l>
					</lg>
				</div>
				<div type="section" n="6">
				<head type="number">VI</head>
					<lg n="1">
						<l n="46" num="1.1">Vous, enfants, conçus dans l'année</l>
						<l n="47" num="1.2">Aux ciels éclaboussés de sang !</l>
						<l n="48" num="1.3">Fils des veuves au lait puissant !</l>
						<l n="49" num="1.4">O vous, dont l'âme est condamnée</l>
						<l n="50" num="1.5">A rêver, de meurtre en naissant !</l>
						<l n="51" num="1.6">Irritez nos soifs éphémères !</l>
						<l n="52" num="1.7">Répétez-nous les cris perdus</l>
						<l n="53" num="1.8">Que dans le ventre de vos mères</l>
						<l n="54" num="1.9">Vous jetaient les mourants vaincus !</l>
					</lg>
				</div>
				<div type="section" n="7">
				<head type="number">VII</head>
					<lg n="1">
						<l n="55" num="1.1">Un long fantôme avec la nuit</l>
						<l n="56" num="1.2">Revient, angoisse inévitable !</l>
						<l n="57" num="1.3">Un spectre illustre, à chaque table,</l>
						<l n="58" num="1.4">S'assied muet. Son sang reluit !</l>
						<l n="59" num="1.5">Un grand linceul, au coin des bornes,</l>
						<l n="60" num="1.6">Barre la route au citoyen !</l>
						<l n="61" num="1.7">Dans chaque rue un être, ancien,</l>
						<l n="62" num="1.8">L'aïeule auguste, aux grands yeux mornes,</l>
						<l n="63" num="1.9">Nous suit dans l'ombre, et ne dit rien !</l>
					</lg>
				</div>
				<div type="section" n="8">
					<head type="number">VIII</head>
					<lg n="1">
						<l n="64" num="1.1">Qu'ils sont gras, les corbeaux, mon frère !</l>
						<l n="65" num="1.2">Les corbeaux de notre pays !</l>
						<l n="66" num="1.3">Ah ! la chair des héros trahis</l>
						<l n="67" num="1.4">Alourdit leur vol funéraire !</l>
						<l n="68" num="1.5">Quand ils regagnent, vers le soir,</l>
						<l n="69" num="1.6">Leurs bois déserts, hantés des goules,</l>
						<l n="70" num="1.7">Frère, aux clochers on peut les voir,</l>
						<l n="71" num="1.8">Claquant du bec, par bandes soûles,</l>
						<l n="72" num="1.9">Flotter comme un lourd drapeau noir !</l>
					</lg>
				</div>
				<div type="section" n="9">
				<head type="number">IX</head>
					<lg n="1">
						<l n="73" num="1.1">Dévore la honte et l'outrage !</l>
						<l n="74" num="1.2">Ne dis plus, toi, le fils des preux :</l>
						<l n="75" num="1.3">« Ces renards étaient trop nombreux. »</l>
						<l n="76" num="1.4">Tais-toi ! Couve en ton cœur ta rage !</l>
						<l n="77" num="1.5">Attends ! prépare, un jour, pour eux,</l>
						<l n="78" num="1.6">Sans répit, l'heure expiatoire.</l>
						<l n="79" num="1.7">Laisse-les nous voler l'histoire,</l>
						<l n="80" num="1.8">Ces porteurs d'étendards affreux</l>
						<l n="81" num="1.9">Déshonorés par la victoire !</l>
					</lg>
				</div>
				<div type="section" n="10">
					<head type="number">X</head>
					<lg n="1">
						<l n="82" num="1.1">Sous la lune au sanglant brouillard</l>
						<l n="83" num="1.2">Court la nature ensorcelée.</l>
						<l n="84" num="1.3">— Tu regardes dans la vallée ;</l>
						<l n="85" num="1.4">Que vois-tu ? dis-le-nous, vieillard !</l>
						<l n="86" num="1.5">— Le vétéran dit : « Je regarde</l>
						<l n="87" num="1.6">Ces peupliers rangés là-bas !</l>
						<l n="88" num="1.7">Je crois revoir la vieille garde,</l>
						<l n="89" num="1.8">Haute et droite, avec la cocarde,</l>
						<l n="90" num="1.9">Courant au nord, pour les combats ! »</l>
					</lg>
				</div>
				<div type="section" n="11">
				<head type="number">XI</head>
					<lg n="1">
						<l n="91" num="1.1">Battez le fer, ô forgerons !</l>
						<l n="92" num="1.2">Pour y pendre un jour leurs entrailles !</l>
						<l n="93" num="1.3">Fondez le plomb pour les mitrailles,</l>
						<l n="94" num="1.4">Quand, un jour, nous les chasserons !</l>
						<l n="95" num="1.5">L'odeur des morts emplit la brume.</l>
						<l n="96" num="1.6">Dans la plaine et sur le coteau</l>
						<l n="97" num="1.7">Que l'espoir sacré se rallume !</l>
						<l n="98" num="1.8">Que la vengeance soit l'enclume,</l>
						<l n="99" num="1.9">Et la haine, le dur manteau !</l>
					</lg>
				</div>
				<div type="section" n="12">
					<head type="number">XII</head>
					<lg n="1">
						<l n="100" num="1.1">Car là-bas, en riant de nous,</l>
						<l n="101" num="1.2">Ils font sonner leurs lourdes crosses ;</l>
						<l n="102" num="1.3">Car là-bas, sous leurs mains atroces</l>
						<l n="103" num="1.4">Ils ont mis nos sœurs à genoux !</l>
						<l n="104" num="1.5">Ah ! l'honneur est un mort rebelle</l>
						<l n="105" num="1.6">Qui dort trop mal pour rester coi !</l>
						<l n="106" num="1.7">Il n'attend pas qu'un Dieu l'appelle.</l>
						<l n="107" num="1.8">N'entends-tu rien, mon frère, en toi,</l>
						<l n="108" num="1.9">Qui hurle : « Allons ! réveille-moi ! »</l>
					</lg>
				</div>
				<div type="section" n="13">
				<head type="number">XIII</head>
					<lg n="1">
						<l n="109" num="1.1">Le vent qui passe nous apporte</l>
						<l n="110" num="1.2">Un bruit de fifre et de tambour.</l>
						<l n="111" num="1.3">Il ne nous parle plus d'amour,</l>
						<l n="112" num="1.4">Le vent qui souffle à notre porte !</l>
						<l n="113" num="1.5">Le vent qui chante vient du Rhin</l>
						<l n="114" num="1.6">Où rit et boit le Hun rapace !</l>
						<l n="115" num="1.7">Il poursuit en mer le marin,</l>
						<l n="116" num="1.8">Sous le ciel clair, ou sous le grain,</l>
						<l n="117" num="1.9">Le rire affreux du vent qui passe !</l>
					</lg>
				</div>
				<div type="section" n="14">
					<head type="number">XIV</head>
					<lg n="1">
						<l n="118" num="1.1">Dans les aurores, les vois-tu,</l>
						<l n="119" num="1.2">Montrant, l'une, sa noire flèche,</l>
						<l n="120" num="1.3">L'autre, ses murs toujours sans brèche,</l>
						<l n="121" num="1.4">Nos deux sœurs, ivres de vertu ?</l>
						<l n="122" num="1.5">Les vois-tu sortir dans l'aurore</l>
						<l n="123" num="1.6">Des bras dénoués du Germain,</l>
						<l n="124" num="1.7">L'une, allongeant sa maigre main,</l>
						<l n="125" num="1.8">L'autre, vierge farouche encore,</l>
						<l n="126" num="1.9">Nos sœurs, après l'horrible hymen !</l>
					</lg>
				</div>
			</div></body></text></TEI>