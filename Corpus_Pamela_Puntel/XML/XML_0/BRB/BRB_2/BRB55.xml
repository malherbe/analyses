<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">DEVANT L'ENNEMI</title>
				<title type="sub">Poémes publiés dans la REVUE DES DEUX MONDES (1870)</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>124 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRB_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DEVANT L'ENNEMI</title>
						<author>AUGUSTE BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k870291</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>REVUE DES DEUX MONDES</title>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>REVUE DES DEUX MONDES</publisher>
									<date when="1870">1870</date>
								</imprint>
								<biblScope unit="tome">89</biblScope>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEVANT L'ENNEMI</head><div type="poem" key="BRB55">
					<head type="main">AUX ALLEMANDS</head>
					<lg n="1">
						<l n="1" num="1.1">Qu'as tu fait, Allemagne ? En ce conflit nouveau,</l>
						<l n="2" num="1.2"><space unit="char" quantity="12"/>Tu t'es mise à la suite</l>
						<l n="3" num="1.3">D'un féroce ministre et de son roi dévot,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"/>Bombardeur hypocrite !</l>
						<l n="5" num="1.5">Toi que l'on estimait parfum d'honnêteté</l>
						<l n="6" num="1.6"><space unit="char" quantity="12"/>Et fleur de poésie,</l>
						<l n="7" num="1.7">Tu n'avais dans le cœur, sous masque de bonté,</l>
						<l n="8" num="1.8"><space unit="char" quantity="12"/>Que basse jalousie !</l>
						<l n="9" num="1.9">Servante du Prussien, tu lui prêtas tes bras</l>
						<l n="10" num="1.10"><space unit="char" quantity="12"/>Quand sa troupe sauvage,</l>
						<l n="11" num="1.11">S'épandant sur nos champs, y porta le trépas,</l>
						<l n="12" num="1.12"><space unit="char" quantity="12"/>La flamme et le ravage ;</l>
						<l n="13" num="1.13">Tu mêlas ton épée aux glaives assassins</l>
						<l n="14" num="1.14"><space unit="char" quantity="12"/>De ces hardis Vandales,</l>
						<l n="15" num="1.15">Et pris secrète part à tous les noirs desseins</l>
						<l n="16" num="1.16"><space unit="char" quantity="12"/>Des bandes féodales !</l>
						<l n="17" num="1.17">Et pourquoi ? Dans l'espoir qu'au vil démembrement</l>
						<l n="18" num="1.18"><space unit="char" quantity="12"/>De la France éventrée</l>
						<l n="19" num="1.19">Tes petits rois vautours seraient tous simplement</l>
						<l n="20" num="1.20"><space unit="char" quantity="12"/>Admis à la curée !</l>
						<l n="21" num="1.21">Tes républicains même, ivres de la beauté</l>
						<l n="22" num="1.22"><space unit="char" quantity="12"/>De cette boucherie,</l>
						<l n="23" num="1.23">Muets presque tous, ont à peine protesté</l>
						<l n="24" num="1.24"><space unit="char" quantity="12"/>Contre la barbarie !</l>
						<l n="25" num="1.25">Ah ! que le temps s'écoule, il n'effacera pas</l>
						<l n="26" num="1.26"><space unit="char" quantity="12"/>Cette action coupable ;</l>
						<l n="27" num="1.27">Elle marque ton front entre tous les états</l>
						<l n="28" num="1.28"><space unit="char" quantity="12"/>D'une tache effroyable.</l>
						<l n="29" num="1.29">Pour des siècles sans nombre elle nous laisse au cœur</l>
						<l n="30" num="1.30"><space unit="char" quantity="12"/>Une peine infinie</l>
						<l n="31" num="1.31">Dont nulle douce paix n'amoindrira l'ardeur,</l>
						<l n="32" num="1.32"><space unit="char" quantity="12"/>Perfide Germanie !</l>
						<l n="33" num="1.33">Mais va, ton châtiment s'avance, car après</l>
						<l n="34" num="1.34"><space unit="char" quantity="12"/>Cette horrible campagne</l>
						<l n="35" num="1.35">Le venin de la Prusse en toi reste à jamais,</l>
						<l n="36" num="1.36"><space unit="char" quantity="12"/>Et morte est l'Allemagne.</l>
					</lg>
				</div></body></text></TEI>