<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES SAINTES COLÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="SIE">
					<name>
						<forename>Louisa</forename>
						<surname>SIEFERT</surname>
					</name>
					<date from="1845" to="1877">1845-1877</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>226 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">SIE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES SAINTES COLÈRES</title>
						<author>LOUISA SIEFERT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5742349p.r=LOUISA%20SIEFERT%20%20%20LES%20SAINTES%20COL%C3%88RES?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES SAINTES COLÈRES</title>
								<author>LOUISA SIEFERT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-23" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="SIE2">
				<head type="main">LES SAINTES COLÈRES</head>
				<div n="1" type="section">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">L’automne ! la voilà plus belle que jamais,</l>
						<l n="2" num="1.2">Avec sa douceur calme et son moite sourire.</l>
						<l n="3" num="1.3">Tous ces enchantements sont bien ceux que j’aimais,</l>
						<l n="4" num="1.4">Que, si souvent déjà, j’ai tenté de décrire.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Les rayons du matin glissent dans la vapeur</l>
						<l n="6" num="2.2">Qui reste prise aux doigts plus grêles des ramures ;</l>
						<l n="7" num="2.3">Le vent léger s’en va comme s’il avait peur</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>D’ôter un grain aux grappes mûres.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Le soir se fait plus grave et plus religieux,</l>
						<l n="10" num="3.2">L’étoile y luit plus tôt d’une flamme moins rose ;</l>
						<l n="11" num="3.3">Et par les monts, les bois, les prés, les eaux, les cieux,</l>
						<l n="12" num="3.4">O jours de l’an passé ! c’est bien la même chose.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">— Non, non, dans tous les cœurs l’hymne se change en cri,</l>
						<l n="14" num="4.2">La terre sous nos pieds brûle, gronde, tressaille,</l>
						<l n="15" num="4.3">Car de coups de canon l’horizon est meurtri :</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>La France est le champ de bataille !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Dieu ! qui pourrait songer à ses propres douleurs,</l>
						<l n="18" num="5.2">Quand la Patrie est là déchirée et sanglante ?</l>
						<l n="19" num="5.3">Pour une autre souffrance où donc trouver des pleurs ?</l>
						<l n="20" num="5.4">Que dire, qu’appeler la revanche trop lente ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">O France ! ils sont venus nombreux et triomphants,</l>
						<l n="22" num="6.2">Ils t’ont visée au cœur du bout de leur épée,</l>
						<l n="23" num="6.3">Ils veulent maintenant te voler tes enfants,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>T’avilir comme ils t’ont frappée.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Debout ! relève-toi de ces derniers vingt ans,</l>
						<l n="26" num="7.2">Souviens-toi de l’Argonne et de quatre-vingt-douze,</l>
						<l n="27" num="7.3">A tous ces ennemis, ô France, il en est temps,</l>
						<l n="28" num="7.4">Sache donc te montrer de ton honneur jalouse.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">De ta robuste main reprends ton vieux drapeau,</l>
						<l n="30" num="8.2">Déroules-en les plis dans le vent héroïque,</l>
						<l n="31" num="8.3">Pour qu’au moins nous mourions comme Hoche ou Marceau</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>En acclamant la République !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">15 août 1870.</date>
						</dateline>
					</closer>
				</div>
				<div n="2" type="section">
					<head type="number">II</head>
					<lg n="1">
						<l n="33" num="1.1"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>C’est horrible. La terre crie,</l>
						<l n="34" num="1.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Ainsi qu’un pressoir trop chargé ;</l>
						<l n="35" num="1.3"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Le cellier devient boucherie,</l>
						<l n="36" num="1.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Et le vin en sang est changé.</l>
					</lg>
					<lg n="2">
						<l n="37" num="2.1"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Par les âmes des morts qui passent,</l>
						<l n="38" num="2.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>On dirait le ciel obscurci ;</l>
						<l n="39" num="2.3"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Ces vents qui d’un frisson nous glacent,</l>
						<l n="40" num="2.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Ont apporté leur râle ici. ’</l>
					</lg>
					<lg n="3">
						<l n="41" num="3.1"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Partout les villes bombardées</l>
						<l n="42" num="3.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Fument dans la rougeur des soirs ;</l>
						<l n="43" num="3.3"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Plaines, forêts sont débordées</l>
						<l n="44" num="3.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>De soldats blonds, de chasseurs noirs.</l>
					</lg>
					<lg n="4">
						<l n="45" num="4.1"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>La cuve est pleine, elle est immense.</l>
						<l n="46" num="4.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Le ferment bout avec fureur.</l>
						<l n="47" num="4.3"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>— Ne viendras-tu pas voir, ô France,</l>
						<l n="48" num="4.4"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Les vendanges de l’empereur ?</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">20 août 1870.</date>
						</dateline>
					</closer>
				</div>
				<div n="3" type="section">
					<head type="number">III</head>
					<lg n="1">
						<l n="49" num="1.1">Vivat et Te Deum ! c’est le couronnement</l>
						<l n="50" num="1.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>De cet admirable édifice.</l>
						<l n="51" num="1.3">Le rapt a commencé, purs vient l'égorgement.</l>
						<l n="52" num="1.4"><space unit="char" quantity="12"/><space quantity="12" unit="char"/>— « Il faut qu’on en finisse ? »</l>
					</lg>
					<lg n="2">
						<l n="53" num="2.1">L’esclave, après vingt ans, s’éveillait et vivait ;</l>
						<l n="54" num="2.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Pensive, elle disait : — « Je souffre ! »</l>
						<l n="55" num="2.3">Pour en avoir raison, cette fois on devait</l>
						<l n="56" num="2.4"><space unit="char" quantity="12"/><space quantity="12" unit="char"/>La jeter dans le gouffre.</l>
					</lg>
					<lg n="3">
						<l n="57" num="3.1">Avec un peu de gloire on tenait le moyen,</l>
						<l n="58" num="3.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>(Gloire ou gloriole, n’importe ! )</l>
						<l n="59" num="3.3">Et l’on se promettait de l’en griser si bien,</l>
						<l n="60" num="3.4"><space unit="char" quantity="12"/><space quantity="12" unit="char"/>Qu’elle en fût ivre-morte.</l>
					</lg>
					<lg n="4">
						<l n="61" num="4.1">Alors en la berçant de sonores discours,</l>
						<l n="62" num="4.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Comme cette folle en écoute,</l>
						<l n="63" num="4.3">On la lierait de nœuds souples, fermes et courts,</l>
						<l n="64" num="4.4"><space unit="char" quantity="12"/><space quantity="12" unit="char"/>Qui la livreraient toute.</l>
					</lg>
					<lg n="5">
						<l n="65" num="5.1">On l’enterrerait vive, et pendant qu’elle dort</l>
						<l n="66" num="5.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>On rebâtirait sur sa tombe</l>
						<l n="67" num="5.3">L’édifice ébranlé par le dernier effort</l>
						<l n="68" num="5.4"><space unit="char" quantity="12"/><space quantity="12" unit="char"/>Auquel elle succombe.</l>
					</lg>
					<lg n="6">
						<l n="69" num="6.1">— Mais le pied du bandit a glissé ; mais sa main</l>
						<l n="70" num="6.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Tâtonne ; mais sa voix s’enroue ;</l>
						<l n="71" num="6.3">Mais devant lui le sang qui remplit le chemin</l>
						<l n="72" num="6.4"><space unit="char" quantity="12"/><space quantity="12" unit="char"/>En a fait de la boue.</l>
					</lg>
					<lg n="7">
						<l n="73" num="7.1">Mais derrière lui, sombre et fatal, son passé</l>
						<l n="74" num="7.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Le repousse dans cette lutte,</l>
						<l n="75" num="7.3">Et son dernier exploit tant de fois annoncé,</l>
						<l n="76" num="7.4"><space unit="char" quantity="12"/><space quantity="12" unit="char"/>Précipite sa chute.</l>
					</lg>
					<lg n="8">
						<l n="77" num="8.1">Et la France regarde avec un œil d’effroi</l>
						<l n="78" num="8.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Ce charnier aux terreurs funèbres,</l>
						<l n="79" num="8.3">Dont il voulait lui faire à jamais sous sa loi</l>
						<l n="80" num="8.4"><space unit="char" quantity="12"/><space quantity="12" unit="char"/>Un lit dans les ténèbres.</l>
					</lg>
					<lg n="9">
						<l n="81" num="9.1">— Oh ! n’est-ce pas qu’enfin tu te rebelleras,</l>
						<l n="82" num="9.2"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Fière, superbe et si meurtrie,</l>
						<l n="83" num="9.3">Et qu’à la liberté tu vas rouvrir tes bras,</l>
						<l n="84" num="9.4"><space unit="char" quantity="12"/><space quantity="12" unit="char"/>O ma mère, ô Patrie !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">25 août 1870.</date>
						</dateline>
					</closer>
				</div>
				<div n="4" type="section">
					<head type="number">IV</head>
					<lg n="1">
						<l n="85" num="1.1">Cet homme était assis au bord de la rivière ;</l>
						<l n="86" num="1.2">Huit jours auparavant sa contenance fière,</l>
						<l n="87" num="1.3">Son langage énergique en parlant de ses fils,</l>
						<l n="88" num="1.4">Tous deux soldats, m’avaient frappée ; et je lui fis,</l>
						<l n="89" num="1.5">Croyant voir à son front des rides plus cruelles,</l>
						<l n="90" num="1.6">Tout bas cette demande : « Avez-vous des nouvelles ? »</l>
						<l n="91" num="1.7">Il me répondit : — « Non ! » d’un ton qui me glaça.</l>
						<l n="92" num="1.8">Le flot clair devant nous prit sa course, passa,</l>
						<l n="93" num="1.9">Et me sembla tomber au loin comme une larme.</l>
						<l n="94" num="1.10">Autour de nous l’automne empruntait plus de charme</l>
						<l n="95" num="1.11">Au matin mi-voilé de gris-rose et de bleu.</l>
						<l n="96" num="1.12">Le vent en soupirant s’élevait peu à peu,</l>
						<l n="97" num="1.13">Et le cœur se serrait à ce doux paysage.</l>
						<l n="98" num="1.14">Alors mon compagnon détournant son visage,</l>
						<l n="99" num="1.15">Évitant tout regard qui le pouvait troubler,</l>
						<l n="100" num="1.16">Lentement, sourdement se mit à me parler :</l>
					</lg>
					<lg n="2">
						<l n="101" num="2.1">« Oui, j’ai deux fils là-bas, dit-il. Cette semaine</l>
						<l n="102" num="2.2">J’ai su que le second va bien. Dieu le ramène !</l>
						<l n="103" num="2.3">Moi, je ne l’attends plus depuis que l’autre est mort.</l>
						<l n="104" num="2.4">Oh ! ne m’allez pas dire : On ne sait pas son sort ;</l>
						<l n="105" num="2.5">Peut-être est-il blessé, prisonnier ?— Non, madame.</l>
						<l n="106" num="2.6">Je connais mon garçon, c’est une bonne lame,</l>
						<l n="107" num="2.7">Qui sait qu’on ne doit pas se rendre. Les meilleurs</l>
						<l n="108" num="2.8">Disaient : « Il nous vaut tous ! » J ’en étais fier. D’ailleurs</l>
						<l n="109" num="2.9">Le journal l’a bien dit, et je l’ai lu moi-même,</l>
						<l n="110" num="2.10">Son régiment était à Woerth. C’est le deuxième,</l>
						<l n="111" num="2.11">Celui dont il n’est rien resté — que le tombeau !</l>
						<l n="112" num="2.12">Vive Dieu ! quel lancier c’était, et brave, et beau !</l>
						<l n="113" num="2.13">Perdre un enfant pareil, voyez-vous, ça vous ronge.</l>
						<l n="114" num="2.14">Le cinq août, la veille (on dit songe, mensonge,</l>
						<l n="115" num="2.15">N’est-il pas vrai ? pourtant, écoutez donc ceci ) :</l>
						<l n="116" num="2.16">Il est venu vers moi la nuit, il m’a saisi</l>
						<l n="117" num="2.17">Dans ses bras : « Adieu, père ! » — Il est sorti tout pâle.</l>
						<l n="118" num="2.18">Au jour, me rappelant cette scène fatale,</l>
						<l n="119" num="2.19">L’embrassade, le cri, j’ai compris la leçon</l>
						<l n="120" num="2.20">Qu’il me fallait comprendre, et j’ai dit : Mon garçon</l>
						<l n="121" num="2.21">Est flambé, c’est fini ! — Depuis, j’ai su l’affaire,</l>
						<l n="122" num="2.22">Et j’ai récrit trois fois. Mais rien ! Allez, un père</l>
						<l n="123" num="2.23">Ne peut pas s’y tromper. Il était si gentil ;</l>
						<l n="124" num="2.24">Pour plume il aurait pris le bout de son fusil ;</l>
						<l n="125" num="2.25">Avec ça, doux, rangé, soigneux comme une fille…</l>
						<l n="126" num="2.26">Que voulez-vous ? On est ainsi dans la famille.</l>
						<l n="127" num="2.27">Notre sang aime à voir le grand soleil. Il faut</l>
						<l n="128" num="2.28">Qu’on se batte. De père en fils c’est le défaut.</l>
						<l n="129" num="2.29">Le bisaïeul que j’ai connu dans mon enfance</l>
						<l n="130" num="2.30">Et centenaire, était à Demain. Notre France</l>
						<l n="131" num="2.31">Fut envahie alors comme aujourd’hui. — Le roi</l>
						<l n="132" num="2.32">Vaut l’empereur. — L’aïeul était à Fontenoy.</l>
						<l n="133" num="2.33">— Je crois qu’on s’en voulait pour de la politique. —</l>
						<l n="134" num="2.34">Le père était partout durant la République :</l>
						<l n="135" num="2.35">Sur le Rhin, en Hollande, — et c’était le grand temps !</l>
						<l n="136" num="2.36">Vint le premier empire, et j’avais dix-sept ans</l>
						<l n="137" num="2.37">Quand j’ai fait à mon tour ma première campagne.</l>
						<l n="138" num="2.38">L’ennemi nous passa sur le corps en Champagne</l>
						<l n="139" num="2.39">Pour entrer dans Paris, où jamais jusque-là</l>
						<l n="140" num="2.40">Nul n’avait pu venir. — Il nous coûtait cela,</l>
						<l n="141" num="2.41">Napoléon ! — Après mes douze ans de service,</l>
						<l n="142" num="2.42">Et beaucoup de travail pour peu de bénéfice,</l>
						<l n="143" num="2.43">Je me suis marié, les enfants ont grandi ;</l>
						<l n="144" num="2.44">Quand je les regardais, j’étais ragaillardi.</l>
						<l n="145" num="2.45">J’aimais a voir aussi près d’eux leur pauvre mère ;</l>
						<l n="146" num="2.46">Elle est morte, les fils sont partis pour la guerre,</l>
						<l n="147" num="2.47">Et je me suis fait vieux pour mourir le dernier.</l>
						<l n="148" num="2.48">L’aîné me ressemblait : l’autre, le pontonnier,</l>
						<l n="149" num="2.49">Tenait de ma défunte, il était blond comme elle.</l>
						<l n="150" num="2.50">Vous voyez cette eau bleue ? on dirait sa prunelle.</l>
						<l n="151" num="2.51">Quelque chose me dit qu’il restera là-bas,</l>
						<l n="152" num="2.52">Et celui-là non plus, je ne le verrai pas.</l>
						<l n="153" num="2.53">Plus de famille alors, plus de nom, plus de race.</l>
						<l n="154" num="2.54">La maison est à qui la veut ; car, à la place</l>
						<l n="155" num="2.55">Où leur sang a fumé, la terre le boira,</l>
						<l n="156" num="2.56">Et leur souvenir même avec moi s’éteindra.</l>
						<l n="157" num="2.57">— Oh ! je suis déjà vieux, et j’ai la tête blanche,</l>
						<l n="158" num="2.58">Mais si, trouvant enfin l’heure de la revanche,</l>
						<l n="159" num="2.59">Je tenais d’une main ces Prussiens haïs</l>
						<l n="160" num="2.60">Qui deux fois dans ma vie ont souillé mon pays,</l>
						<l n="161" num="2.61">Qui changent notre France en un champ de bataille ;</l>
						<l n="162" num="2.62">Et si, dans l’autre main, j’avais cette canaille</l>
						<l n="163" num="2.63">D’empereur, qui nous vole et nous égorge après,</l>
						<l n="164" num="2.64">Oh ! des deux mains, d’un coup, je les écraserais !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">28 août 1870.</date>
						</dateline>
					</closer>
				</div>
				<div n="5" type="section">
					<head type="number">V</head>
					<lg n="1">
						<l n="165" num="1.1">Ah ! parce qu’ils sont forts, et qu’ils sont en grand nombre,</l>
						<l n="166" num="1.2">Qu’ils se sont préparés dans le silence et l’ombre</l>
						<l n="167" num="1.3"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Comme des renards ou des loups ;</l>
						<l n="168" num="1.4">Parce qu’ils ont surpris notre France endormie,</l>
						<l n="169" num="1.5">Qu’ils ont mis leur poing lourd sur sa bouche blémie,</l>
						<l n="170" num="1.6"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Et sur sa gorge leurs genoux,</l>
					</lg>
					<lg n="2">
						<l n="171" num="2.1">Ils ont crié victoire, et dit qu’elle était morte !</l>
						<l n="172" num="2.2">Mais le torrent de sang qu’ils font couler, emporte</l>
						<l n="173" num="2.3"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Son dernier rêve et son sommeil ;</l>
						<l n="174" num="2.4">Et leur glaive, inhabile à bien servir leur haine,</l>
						<l n="175" num="2.5">A tout d’abord frappé, tordu, brisé sa chaîne :</l>
						<l n="176" num="2.6"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Elle est libre pour le réveil !</l>
					</lg>
					<lg n="3">
						<l n="177" num="3.1">Comment n’ont-ils pas vu l’œuvre de leur démence ?</l>
						<l n="178" num="3.2">Ne comprennent-ils pas que la guerre commence,</l>
						<l n="179" num="3.3"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Et, devant la Patrie en deuil</l>
						<l n="180" num="3.4">Qui presse de la main sa blessure béante,</l>
						<l n="181" num="3.5">N’ont-ils pas frissonné de honte et d’épouvante ?</l>
						<l n="182" num="3.6"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Comment donc ont-ils tant d’orgueil ?</l>
					</lg>
					<lg n="4">
						<l n="183" num="4.1">Est-ce qu’ils ont si mal appris leur propre histoire ?</l>
						<l n="184" num="4.2">Nous faut-il de nouveau leur remettre en mémoire</l>
						<l n="185" num="4.3"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Les fastes de la liberté ?</l>
						<l n="186" num="4.4">Et quoiqu’ils aient gagné les premières étapes,</l>
						<l n="187" num="4.5">Ressusciterons-nous Valmy, Fleurus, Jemmapes,</l>
						<l n="188" num="4.6"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Pour confondre leur vanité ?</l>
					</lg>
					<lg n="5">
						<l n="189" num="5.1">Sans doute ils sont puissants, et leur audace est grande,</l>
						<l n="190" num="5.2">Il se peut même enter qu’au droit elle commande</l>
						<l n="191" num="5.3"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Aujourd’hui, peut-être demain ;</l>
						<l n="192" num="5.4">Il se peut que le sort trompé notre courage,</l>
						<l n="193" num="5.5">Que jusqu’au lieu marqué pour laver tant d’outrage,</l>
						<l n="194" num="5.6"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>Nous devions faire un long chemin ;</l>
					</lg>
					<lg n="6">
						<l n="195" num="6.1">Mais plus lente elle vient, plus la justice est sûre.</l>
						<l n="196" num="6.2">Passent les jours, les mois, les ans ! L’heure future</l>
						<l n="197" num="6.3"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>A déjà sonné sur nos fronts.</l>
						<l n="198" num="6.4">Nous saurons bien l’attendre et prendre patience ;</l>
						<l n="199" num="6.5">— Et s’il n’est plus qu’un cri, celui de la vengeance,</l>
						<l n="200" num="6.6"><space unit="char" quantity="8"/><space quantity="8" unit="char"/>O France, nous le pousserons !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">25 novembre 1870.</date>
						</dateline>
					</closer>
				</div>
				<div n="6" type="section">
					<head type="number">VI</head>
					<lg n="1">
						<l n="201" num="1.1">On disait : — Il est mort, foulé sur la grand’route</l>
						<l n="202" num="1.2">Par ceux dont il voulait arrêter la déroute.</l>
						<l n="203" num="1.3">Cet autre, au coin d’un bois, tomba seul. Celui-ci,</l>
						<l n="204" num="1.4">Plutôt que de céder, s’est fait tuer ici.</l>
						<l n="205" num="1.5">Celui-là fut broyé sous tant de projectiles,</l>
						<l n="206" num="1.6">Et tous ces dévouements étaient bien inutiles !</l>
					</lg>
					<lg n="2">
						<l n="207" num="2.1">Et je pensais : jamais dévouement n’est perdu ;</l>
						<l n="208" num="2.2">Ce germe est immortel, et le sang répandu</l>
						<l n="209" num="2.3">Consacre le principe au nom duquel il coule.</l>
						<l n="210" num="2.4">Ces braves ne sont pas grains de sable à la houle :</l>
						<l n="211" num="2.5">Ils sont grains de froment au sillon large et droit ;</l>
						<l n="212" num="2.6">Où sema le devoir, moissonnera le droit.</l>
					</lg>
				<closer>
						<dateline>
							<date when="1870">28 novembre 1870.</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>