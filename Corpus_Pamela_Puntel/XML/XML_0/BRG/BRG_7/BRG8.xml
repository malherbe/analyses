<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POËMES DE LA GUERRE</title>
				<title type="sub">1870-1871</title>
				<title type="sub">Édition partielle</title>
				<title type="medium">Édition électronique</title>
				<author key="BRG">
					<name>
						<forename>Émile</forename>
						<surname>BERGERAT</surname>
					</name>
					<date from="1845" to="1923">1845-1923</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2140 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRG_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poëmes de la guerre, 1870-1871</title>
						<author>Émile Bergerat</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https://archive.org/details/poemesdelaguerre00berg/page/n122</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poëmes de la guerre, 1870-1871</title>
								<author>Émile Bergerat</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALPHONSE LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Edition partielle.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRG8">
				<head type="main">SAINT-CLOUD</head>
				<head type="form">ÉGLOGUE FUNAMBULESQUE</head>
				<opener>
					<salute> A mon ami ERNEST COQUELIN, <lb/>dit Coquelin cadet, <lb/>de la Comédie-Française.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Je chante ces enfants d'Obus</l>
					<l n="2" num="1.2">Qui, par le secours du pétrole,</l>
					<l n="3" num="1.3">Ont fait cuire sans casserole</l>
					<l n="4" num="1.4">L'Hydre Saint-Cloud, fils de Phœbus !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">– Ce Saint-Cloud était, dans son antre,</l>
					<l n="6" num="2.2">Un monstre horrible et malfaisant !</l>
					<l n="7" num="2.3">Il avait,— c'est rare à présent ! —</l>
					<l n="8" num="2.4">Un œil rond au milieu du ventre.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Accoudé le jour et la nuit</l>
					<l n="10" num="3.2">Sur le penchant d'une colline,</l>
					<l n="11" num="3.3">Il jouait de la mandoline !</l>
					<l n="12" num="3.4">–Sorte de guitare qui nuit. —</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Il chantait la faridondaine !</l>
					<l n="14" num="4.2">– Laisser-aller inconvenant ,</l>
					<l n="15" num="4.3">Il permettait à tout venant</l>
					<l n="16" num="4.4">De lui monter sur la bedaine.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Ses pieds pendaient au fil de l'eau,</l>
					<l n="18" num="5.2">Et, tendant une jambe obscène,</l>
					<l n="19" num="5.3">Il faisait un pont sur la Seine</l>
					<l n="20" num="5.4">A ceux qu'attirait ce tableau !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Ce cyclope était ventriloque</l>
					<l n="22" num="6.2">Et contrefaisait sous les bois</l>
					<l n="23" num="6.3">De bons amoureux qui parfois</l>
					<l n="24" num="6.4">Grâce à lui battaient la breloque.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Il recevait de noirs tritons,</l>
					<l n="26" num="7.2">Gens de rien, demi-dieux quelconques,</l>
					<l n="27" num="7.3">Qui s'époumonaient dans des conques</l>
					<l n="28" num="7.4">Qu'on nomme aux enfers : Mirlitons !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Il hébergeait des saltimbanques,</l>
					<l n="30" num="8.2">Et même des pitres forains</l>
					<l n="31" num="8.3">Qui trompaient tous les riverains</l>
					<l n="32" num="8.4">Par les mensonges de leurs banques !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Le monstre se faisait croupier</l>
					<l n="34" num="9.2">De macarons, ainsi qu'à Bade !</l>
					<l n="35" num="9.3">Et saignait, ô naïf troubade,</l>
					<l n="36" num="9.4">Ton escarcelle de troupier !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Même il donnait la comédie,</l>
					<l n="38" num="10.2">Dans quel costume ! en caleçons !</l>
					<l n="39" num="10.3">Pleine d'immorales leçons</l>
					<l n="40" num="10.4">Auxquelles rien ne remédie !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Il criait : Boum boum ! et zing zing !</l>
					<l n="42" num="11.2">Il crevait des tambours de basques !</l>
					<l n="43" num="11.3">Il s'affublait d'horribles casques</l>
					<l n="44" num="11.4">Moitié papier et moitié zinc !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Il organisait des quadrilles,</l>
					<l n="46" num="12.2">Vrais gymnases de cachuchas,</l>
					<l n="47" num="12.3">Où l'on voyait de petits chats</l>
					<l n="48" num="12.4">Entre les bras de gros mandrilles.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Il lançait d'infâmes pétards</l>
					<l n="50" num="13.2">Dans les jambes du pauvre monde,</l>
					<l n="51" num="13.3">Et sur la surface de l'onde</l>
					<l n="52" num="13.4">Faisait remonter les têtards !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Voyait-il passer dans leurs yoles</l>
					<l n="54" num="14.2">Des bourgeois en fin drap d'Elbeuf,</l>
					<l n="55" num="14.3">Il soufflait dessus comme un bœuf</l>
					<l n="56" num="14.4">Pour les induire en cabrioles ;</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Et par ce procédé cruel</l>
					<l n="58" num="15.2">Les menant à résipiscence</l>
					<l n="59" num="15.3">Débarquait leur concupiscence</l>
					<l n="60" num="15.4">A ses bals de Pantagruel !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Dirai-je les crimes encore</l>
					<l n="62" num="16.2">De cet Hydre ? de ce Cacus ?</l>
					<l n="63" num="16.3">Fils incestueux de Bacchus</l>
					<l n="64" num="16.4">Et de sa fille Terpsychore ?</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Grâce aux conseils de ses volants</l>
					<l n="66" num="17.2">Le petit garçon en jaquette</l>
					<l n="67" num="17.3">Courtisait avec la raquette</l>
					<l n="68" num="17.4">La petite fille en volants !</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Il encourageait la guimbarde</l>
					<l n="70" num="18.2">Et le pistolet de salon !</l>
					<l n="71" num="18.3">Que dis-je ? il usait du ballon</l>
					<l n="72" num="18.4">Pour montrer comment on bombarde !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Le simoun et le sirocco</l>
					<l n="74" num="19.2">Font des oasis dans la gorge</l>
					<l n="75" num="19.3">A côté de ses sucres d'orge</l>
					<l n="76" num="19.4">A l'absinthe et de son coco !</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Que dire de ses croquignoles,</l>
					<l n="78" num="20.2">De ses gaufres ? des tombolas</l>
					<l n="79" num="20.3">Où des rois pseudobamboulas,</l>
					<l n="80" num="20.4">Dans la langue des Batignolles,</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Faisaient aux dévotes d'Auteuil</l>
					<l n="82" num="21.2">Cette injure que nous sentîmes</l>
					<l n="83" num="21.3">De leur donner pour dix centimes</l>
					<l n="84" num="21.4">Un pot de chambre orné d'un œil !</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Car sa licence était sans bornes</l>
					<l n="86" num="22.2">Et ne redoutait nul affront !</l>
					<l n="87" num="22.3">Les moindres boutons sur un front</l>
					<l n="88" num="22.4">Avec lui devenaient des cornes !</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Dès que l'on tombait sous sa main</l>
					<l n="90" num="23.2">Il fallait rire, aimer, de force !</l>
					<l n="91" num="23.3">Il vous rabotait comme écorce</l>
					<l n="92" num="23.4">Têtes et cœurs de parchemin !</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Ce drôle de la pire espèce</l>
					<l n="94" num="24.2">Prenait un méchant air bénin</l>
					<l n="95" num="24.3">Pour vous infuser le venin</l>
					<l n="96" num="24.4">De sa grosse allégresse épaisse !</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Il arrosait les Magistrats,</l>
					<l n="98" num="25.2">Les Sénateurs, les Diplomates,</l>
					<l n="99" num="25.3">De je ne sais quels aromates</l>
					<l n="100" num="25.4">A surexciter des castrats ;</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Et c'était à vous rendre jaunes</l>
					<l n="102" num="26.2">Comme des citrons indignés,</l>
					<l n="103" num="26.3">De les voir bondir, résignés,</l>
					<l n="104" num="26.4">Parmi les couples de béjaunes !</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1">Scandale à dresser les cheveux</l>
					<l n="106" num="27.2">Des personnes indifférentes !</l>
					<l n="107" num="27.3">Les oncles lui lâchaient des rentes</l>
					<l n="108" num="27.4">Qu'ils doivent encore aux neveux !</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1">Et ce bacchanal de Silène</l>
					<l n="110" num="28.2">S'en allait, par delà les bois,</l>
					<l n="111" num="28.3">Agiter le cœur et les doigts</l>
					<l n="112" num="28.4">Des peintresses en porcelaine !</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1">Les fesses rejoignaient les pieds !</l>
					<l n="114" num="29.2">Les baisers rattrapaient les lèvres ;</l>
					<l n="115" num="29.3">Bref on n'entendait jusqu'à Sèvres</l>
					<l n="116" num="29.4">Qu'amoureux et qu'estropiés !</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1">Et s'en allait à la malheure</l>
					<l n="118" num="30.2">Tout ce qu'on respecte ici-bas !</l>
					<l n="119" num="30.3">On rajustait jusqu'à ses bas</l>
					<l n="120" num="30.4">Devant le monde ! — Moi, j'en pleure !</l>
				</lg>
				<lg n="31">
					<l n="121" num="31.1">Les carapaces de homard</l>
					<l n="122" num="31.2">Fleurissaient sur les aubépines !</l>
					<l n="123" num="31.3">Enlèvement des Proserpines</l>
					<l n="124" num="31.4">Avec les bouchons de pomard !</l>
				</lg>
				<lg n="32">
					<l n="125" num="32.1">On s'allongeait des estocades</l>
					<l n="126" num="32.2">De demoiselles à messieurs !</l>
					<l n="127" num="32.3">On se laissait glisser plusieurs</l>
					<l n="128" num="32.4">Afin d'imiter les cascades !</l>
				</lg>
				<lg n="33">
					<l n="129" num="33.1">On fuyait à deux en disant :</l>
					<l n="130" num="33.2">« Voyons un peu si l'on m'attrape ! »</l>
					<l n="131" num="33.3">Et c'était une chausse-trape</l>
					<l n="132" num="33.4">Où tombait l’œil du médisant</l>
				</lg>
				<lg n="34">
					<l n="133" num="34.1">D'affreux plaisants, de branche en branche,</l>
					<l n="134" num="34.2">Parodiaient la voix du coq !</l>
					<l n="135" num="34.3">Et l'on faisait du Paul de Kock</l>
					<l n="136" num="34.4">Au lieu de lire Malebranche !</l>
				</lg>
				<lg n="35">
					<l n="137" num="35.1">Du bout élastique des sticks</l>
					<l n="138" num="35.2">Au bord des tuyaux acoustiques,</l>
					<l n="139" num="35.3">On contrefaisait les moustiques,</l>
					<l n="140" num="35.4">Antique tic de vieux loustics !…</l>
				</lg>
				<lg n="36">
					<l n="141" num="36.1">J'espère bien qu'un La Bruyère</l>
					<l n="142" num="36.2">Doublé d'un ardent Juvénal</l>
					<l n="143" num="36.3">Peindra le désordre vénal</l>
					<l n="144" num="36.4">De cet hydre, ivre de gruyère !</l>
				</lg>
				<lg n="37">
					<l n="145" num="37.1">Que Saint-Cloud n'échappera pas</l>
					<l n="146" num="37.2">Aux lanières de la satire !</l>
					<l n="147" num="37.3">Qu'on dira combien ce Satyre</l>
					<l n="148" num="37.4">Mangeait de melons par repas !</l>
				</lg>
				<lg n="38">
					<l n="149" num="38.1">J'espère bien que les crevettes</l>
					<l n="150" num="38.2">Auront leur tour, sombres douleurs !</l>
					<l n="151" num="38.3">Châtiment ! c'est de tes couleurs</l>
					<l n="152" num="38.4">Qu'il faudra que tu le revêtes,</l>
				</lg>
				<lg n="39">
					<l n="153" num="39.1">Ce monstre ! Il aurait tes oublis ?</l>
					<l n="154" num="39.2">Mais on dirait que nous l'aidâmes</l>
					<l n="155" num="39.3">A manger ses plaisirs-mesdames,</l>
					<l n="156" num="39.4">A défeuilleter ses oublis !</l>
				</lg>
				<lg n="40">
					<l n="157" num="40.1">Eh quoi ! l'innocente écrevisse,</l>
					<l n="158" num="40.2">La sardine, le goujon frais,</l>
					<l n="159" num="40.3">Dix-huit ans auraient fait les frais</l>
					<l n="160" num="40.4">Des séductions de novice !</l>
				</lg>
				<lg n="41">
					<l n="161" num="41.1">Quoi ! l'omelette et le radis</l>
					<l n="162" num="41.2">Auront assisté près du beurre</l>
					<l n="163" num="41.3">Aux détournements de mineure,</l>
					<l n="164" num="41.4">Pour qu'il l'emporte en paradis !</l>
				</lg>
				<lg n="42">
					<l n="165" num="42.1">Quoi ! des balançoires d'Armide,</l>
					<l n="166" num="42.2">Images du parfait hymen,</l>
					<l n="167" num="42.3">Auront rapproché le chemin</l>
					<l n="168" num="42.4">Du loup à la brebis timide !</l>
				</lg>
				<lg n="43">
					<l n="169" num="43.1">Quoi ! de naïfs chevaux de bois</l>
					<l n="170" num="43.2">Auront fait naître des furoncles,</l>
					<l n="171" num="43.3">A ces choses que chez les oncles</l>
					<l n="172" num="43.4">Les neveux voilaient autrefois !</l>
				</lg>
				<lg n="44">
					<l n="173" num="44.1">Ainsi la toupie hollandaise,</l>
					<l n="174" num="44.2">Le jeu de boule, les billards,</l>
					<l n="175" num="44.3">Accéléraient des corbillards</l>
					<l n="176" num="44.4">Aux ingrats batifolants d'aise !</l>
				</lg>
				<lg n="45">
					<l n="177" num="45.1">Ainsi ces bals et ces lampions,</l>
					<l n="178" num="45.2">Ces cymbales, ces saxophones,</l>
					<l n="179" num="45.3">Servaient à réveiller les faunes</l>
					<l n="180" num="45.4">Cachés sous l'habit des Scipions !</l>
				</lg>
				<lg n="46">
					<l n="181" num="46.1">Et, satisfait, la nuit venue,</l>
					<l n="182" num="46.2">L'Hydre Saint-Cloud, fils de Bacchus,</l>
					<l n="183" num="46.3">Aurait fait sauter ses écus,</l>
					<l n="184" num="46.4">Ainsi qu'un jongleur, sous la nue ;</l>
				</lg>
				<lg n="47">
					<l n="185" num="47.1">Et jusqu'au retour du festin</l>
					<l n="186" num="47.2">Reprenant sa pose et sa flûte,</l>
					<l n="187" num="47.3">Il aurait dit au travail : flûte !</l>
					<l n="188" num="47.4">Il aurait dit zut : au destin ! ! !</l>
				</lg>
				<lg n="48">
					<l n="189" num="48.1">Non ! non ! l'exécrable vampire</l>
					<l n="190" num="48.2">Ne retardait que son Léthé !</l>
					<l n="191" num="48.3">Il avait dansé tout l'été ;</l>
					<l n="192" num="48.4">Il avait chanté tout l'Empire !…</l>
				</lg>
				<lg n="49">
					<l n="193" num="49.1">Les enfants d'Obus sont venus</l>
					<l n="194" num="49.2">Venger la morale outragée ! —</l>
					<l n="195" num="49.3">Bons jeunes gens ! doux ! ingénus !</l>
					<l n="196" num="49.4">Un miel ! un sucre ! une dragée !</l>
				</lg>
				<lg n="50">
					<l n="197" num="50.1">Ils ont cerné l'Hydre Saint-Cloud</l>
					<l n="198" num="50.2">Qui, les voyant sur la colline,</l>
					<l n="199" num="50.3">Prenait déjà sa mandoline</l>
					<l n="200" num="50.4">Depuis six mois pendue au clou,</l>
				</lg>
				<lg n="51">
					<l n="201" num="51.1">Et, débordant, par leurs écluses,</l>
					<l n="202" num="51.2">Sur l'affreux joueur de syrinx,</l>
					<l n="203" num="51.3">Ils lui coupèrent le larynx</l>
					<l n="204" num="51.4">Et toutes les chansons incluses !</l>
				</lg>
				<lg n="52">
					<l n="205" num="52.1">Puis ils lui crevèrent son œil</l>
					<l n="206" num="52.2">Afin d'en occuper le centre,</l>
					<l n="207" num="52.3">Et pénétrèrent dans son ventre</l>
					<l n="208" num="52.4">Tapissé de nids d'écureuil ! —</l>
				</lg>
				<lg n="53">
					<l n="209" num="53.1">« Mes enfants, hurlait le Cyclope,</l>
					<l n="210" num="53.2">« Vous ne me remettez donc pas ?</l>
					<l n="211" num="53.3">« Je vous ai fait danser des pas</l>
					<l n="212" num="53.4">« Que vous divulguiez à l'Europe !</l>
				</lg>
				<lg n="54">
					<l n="213" num="54.1">« Je suis votre ménétrier !</l>
					<l n="214" num="54.2">« Quand vous êtes partis en guerre</l>
					<l n="215" num="54.3">« Vous avez tous chez moi naguère</l>
					<l n="216" num="54.4">« Sablé le coup de l'étrier !</l>
				</lg>
				<lg n="55">
					<l n="217" num="55.1">« Vous faites erreur, je suppose,</l>
					<l n="218" num="55.2">« Sur une identité de noms :</l>
					<l n="219" num="55.3">« Vous prenez pour de vrais canons</l>
					<l n="220" num="55.4">« Ceux que je bois quand je compose !</l>
				</lg>
				<lg n="56">
					<l n="221" num="56.1">« De mon joyeux petit bedon</l>
					<l n="222" num="56.2">« Le soleil fait mûrir la poire ;</l>
					<l n="223" num="56.3">« Si je suis gendre de Grégoire</l>
					<l n="224" num="56.4">« C'est grâce au curé de Meudon !</l>
				</lg>
				<lg n="57">
					<l n="225" num="57.1">« Je suis un ogre très-bon zigue !</l>
					<l n="226" num="57.2">« Quand vient l'hiver, au coin du feu,</l>
					<l n="227" num="57.3">« Avec les astres du vrai Dieu</l>
					<l n="228" num="57.4">« Je fais mon cinq cents de bezigue !</l>
				</lg>
				<lg n="58">
					<l n="229" num="58.1">« Je laisse à l'amiral Napier</l>
					<l n="230" num="58.2">« Toutes les mers, jusqu'aux Antilles !</l>
					<l n="231" num="58.3">« Et ne lance que de gentilles</l>
					<l n="232" num="58.4">« Petites flottes en papier !</l>
				</lg>
				<lg n="59">
					<l n="233" num="59.1">« Je laisse à Krupp ses mécaniques</l>
					<l n="234" num="59.2">« Aux arguments déterminants !</l>
					<l n="235" num="59.3">« Et n'ai que des pois fulminants</l>
					<l n="236" num="59.4">« En fait de choses volcaniques !</l>
				</lg>
				<lg n="60">
					<l n="237" num="60.1">« Je ne gêne Bismark, Cavour,</l>
					<l n="238" num="60.2">« Gortschakoff, de Beust ni Gladstone !</l>
					<l n="239" num="60.3">« En gaîté, je suis autochthone,</l>
					<l n="240" num="60.4">« Je décentralise en amour !</l>
				</lg>
				<lg n="61">
					<l n="241" num="61.1">« Pourquoi m'égorgez-vous ? C'est bête !</l>
					<l n="242" num="61.2">« Moi mort, avec qui rirez-vous ?</l>
					<l n="243" num="61.3">« Vous devriez doter les fous</l>
					<l n="244" num="61.4">« Dans ce monde où l'homme s'embête !</l>
				</lg>
				<lg n="62">
					<l n="245" num="62.1">« Où le fils de Teut sent en lui</l>
					<l n="246" num="62.2">« Ce mal tourner à la souffrance !</l>
					<l n="247" num="62.3">« Vous voulez donc venger la France</l>
					<l n="248" num="62.4">« En mourant, chez elle, d'ennui ?— »</l>
				</lg>
				<lg n="63">
					<l n="249" num="63.1">Ce fut son mensonge du cygne !</l>
					<l n="250" num="63.2">Les timides enfants d'Obus</l>
					<l n="251" num="63.3">Ne comprirent rien au rébus</l>
					<l n="252" num="63.4">De cette impénitence insigne !</l>
				</lg>
				<lg n="64">
					<l n="253" num="64.1">Ils approchèrent des barils</l>
					<l n="254" num="64.2">D'une liqueur bitumineuse</l>
					<l n="255" num="64.3">Que dans leur valeur lumineuse</l>
					<l n="256" num="64.4">Ils ne destinaient qu'à Paris</l>
				</lg>
				<lg n="65">
					<l n="257" num="65.1">Et, l'ayant d'abord enflammée</l>
					<l n="258" num="65.2">Selon le procédé du punch,</l>
					<l n="259" num="65.3">Ils invitèrent à ce lunch</l>
					<l n="260" num="65.4">Définitif la Renommée !</l>
				</lg>
				<lg n="66">
					<l n="261" num="66.1">Elle vint, admira d'abord</l>
					<l n="262" num="66.2">L'auto-da-fé du méchant Hydre,</l>
					<l n="263" num="66.3">Arrêta l'heure à sa clepsydre,</l>
					<l n="264" num="66.4">Et fit l'inventaire du mort.</l>
				</lg>
				<lg n="67">
					<l n="265" num="67.1">Mais ne trouvant plus dans l'armoire</l>
					<l n="266" num="67.2">Ni le linge ni les couverts,</l>
					<l n="267" num="67.3">Mais rien que les tiroirs ouverts,</l>
					<l n="268" num="67.4">Mit ce secret dans sa mémoire ! —</l>
				</lg>
				<lg n="68">
					<l n="269" num="68.1">J'ai chanté ces enfants d'Obus</l>
					<l n="270" num="68.2">Qui, par le secours du pétrole,</l>
					<l n="271" num="68.3">Ont fait cuire sans casserole</l>
					<l n="272" num="68.4">L'Hydre Saint-Cloud, fils de Phœbus !</l>
				</lg>
			</div></body></text></TEI>