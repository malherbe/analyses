<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES CARRIÈRES DE JAUMONT</title>
				<title type="sub">OU VENGEANCE DE QUATRE PAYSANS</title>
				<title type="sub">SOUVENIR ÉPISODIQUE DE LA GUERRE ACTUELLE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLN">
					<name>
						<forename>Gabriel</forename>
						<surname>DELAUNAY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>52 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES CARRIÈRES DE JAUMONT</title>
						<author>Gabriel Delaunay</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54429491/f4.image.r=LES%20CARRI%C3%88RES%20DE%20JAUMONT</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES CARRIÈRES DE JAUMONT</title>
								<author>Gabriel Delaunay</author>
								<imprint>
									<pubPlace>BORDEAUX</pubPlace>
									<publisher>FERET</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLN1">
				<head type="main">LES CARRIÈRES DE JAUMONT</head>
				<lg n="1">
					<l n="1" num="1.1">Vengeance ! — avaient-ils dit, dans leur âpre douleur.</l>
					<l n="2" num="1.2">Leurs yeux restèrent secs, mais la haine en leur cœur</l>
					<l n="3" num="1.3">Avec ses doigts d'acier fit une déchirure,</l>
					<l n="4" num="1.4">Et les pleurs détournés inondaient leur blessure !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="2">
					<l n="5" num="2.1">Satan avait dû voir déborder ce torrent ;</l>
					<l n="6" num="2.2">Satan leur mit au cœur la rage du serpent.</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="3">
					<l n="7" num="3.1">Ah ! c'est qu'il est venu le temps des représailles :</l>
					<l n="8" num="3.2">La vengeance a des droits en dehors des batailles,</l>
					<l n="9" num="3.3">Elle peut sous les fleurs cacher des échafauds</l>
					<l n="10" num="3.4">Quand les soldats déchus ne sont que des bourreaux,</l>
					<l n="11" num="3.5">D'infâmes suborneurs, effroi de nos familles,</l>
					<l n="12" num="3.6">Qui, jusque chez les morts, vont insulter aux filles !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="4">
					<l n="13" num="4.1">Donc, quatre paysans, par l'outrage éprouvés,</l>
					<l n="14" num="4.2">Avaient juré la mort des soldats réprouvés :</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="5">
					<l n="15" num="5.1">De la pitié la haine a franchi les barrières !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="6">
					<l n="16" num="6.1">Jaumont à leurs pensers vient offrir des carrières</l>
					<l n="17" num="6.2">Sous un crâne poudreux aux profonds creusements</l>
					<l n="18" num="6.3">Zébrés de ça, de là, par des soutènements.</l>
					<l n="19" num="6.4">Soudain des offensés le projet redoutable</l>
					<l n="20" num="6.5">Est de vanter la place heureuse, inexpugnable,</l>
					<l n="21" num="6.6">Aux avides Teutons, voulant sur les Latins</l>
					<l n="22" num="6.7">Fondre de ces hauteurs en parfaits assassins :</l>
					<l n="23" num="6.8">Ils s'élancent hardis sur la scène mouvante ;</l>
					<l n="24" num="6.9">Des Latins prévenus l'attitude imposante</l>
					<l n="25" num="6.10">Attend avec stupeur un ordre solennel :</l>
					<l n="26" num="6.11">« Feu ! » nous crie une voix… oh ! spectacle éternel,</l>
					<l n="27" num="6.12">Si je ferme les yeux qu'anime ma pensée !!!</l>
					<l n="28" num="6.13">Des ennemis par nous la masse est repoussée,</l>
					<l n="29" num="6.14">Plus loin elle recule et plus proche est la mort.</l>
					<l n="30" num="6.15">Une attaque opposée a décidé son sort :</l>
					<l n="31" num="6.16">Dans l'excavation l'airain fatal résonne,</l>
					<l n="32" num="6.17">Le gouffre^qui s'émeut va n'épargner personne,</l>
					<l n="33" num="6.18">Il palpite, il s’entr’ouvre, et les caveaux béants</l>
					<l n="34" num="6.19">S'abreuvent à loisir de milliers d'Allemands !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="7">
					<l n="35" num="7.1">Une seule clameur, déchirante, indicible,</l>
					<l n="36" num="7.2">S'élève vers le ciel comme un remords horrible,</l>
					<l n="37" num="7.3">Sortant du précipice où, canons et chevaux,</l>
					<l n="38" num="7.4">Se confondent au sein d'insondables tombeaux !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="8">
					<l n="39" num="8.1">Les Teutons, sans nul doute, eussent à notre place</l>
					<l n="40" num="8.2">D'un hurlement féroce, inhérent à leur race,</l>
					<l n="41" num="8.3">Salué le succès de ce fait glorieux !…</l>
					<l n="42" num="8.4">Mais nous… muets, glacés, les larmes dans les yeux</l>
					<l n="43" num="8.5">Nous mêlions à leurs voix nos ferventes prières,</l>
					<l n="44" num="8.6">Afin qu'un prompt repos vînt fermer leurs paupières.</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="9">
					<l n="45" num="9.1">Vengeance ! avaient-ils dit, dans leur âpre douleur,</l>
					<l n="46" num="9.2">Leurs yeux restèrent secs, mais la haine en leur cœur</l>
					<l n="47" num="9.3">Avec ses doigts d'acier fit une déchirure,</l>
					<l n="48" num="9.4">Et les pleurs détournés inondaient leur blessure !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				<lg n="10">
					<l n="49" num="10.1">Ils n'y furent survivre, et pour n'en plus souffrir,</l>
					<l n="50" num="10.2">Parmi les Allemands ils ont voulu mourir.</l>
					<l n="51" num="10.3">Heureux d'être plus près témoins de leur supplice,</l>
					<l n="52" num="10.4">De leur vie ils ont fait l'immortel sacrifice !!!</l>
				</lg>
			</div></body></text></TEI>