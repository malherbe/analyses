<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES HÉROS DE WISSEMBOURG</title>
				<title type="medium">Édition électronique</title>
				<author key="DLN">
					<name>
						<forename>Gabriel</forename>
						<surname>DELAUNAY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>194 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES HÉROS DE WISSEMBOURG</title>
						<author>Gabriel Delaunay</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30314954g</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES HÉROS DE WISSEMBOURG</title>
								<author>Gabriel Delaunay</author>
								<imprint>
									<pubPlace>BORDEAUX</pubPlace>
									<publisher>FERET</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="drama" key="DLN2">
				<head type="main">LES HÉROS DE WISSEMBOURG</head>
				<head type="form">A-PROPOS PATRIOTIQUE EN UN ACTE ET EN VERS</head>
				<div type="body">
					<castList>
						<head>PERSONNAGES</head>
						<castItem>
							<role>UN LIEUTENANT D’UN RÉGIMENT QUELCONQUE</role>
						</castItem>
						<castItem>
							<role>LA VICTOIRE EN TUNIQUE ROMAINE</role>
						</castItem>
						<castItem>
							<role>DEUX SOLDATS FRANÇAIS</role>
						</castItem>
						<castItem>
							<role>QUATRE SOLDATS PRUSSIENS</role>
						</castItem>
					</castList>
					<set>
						Le théâtre représente une petite cour d’une ambulance prussienne,
						en Alsace, s’ouvrant sur une forêt. Une table à droite et à gauche
						eu premier plan. Au-dessus d’une porte, à gauche, on lit : Ambulance.
						Le drapeau blanc à croix-rouge flotte à côté. Les soldats prussiens
						jouent aux échecs sur la table de droite, et les français à la table
						de gauche, viennent de boire. 
					</set>
					<div type="act" n="1">
						<div type="scene" n="1">
							<head type="number">SCÈNE I</head>
							<stage type="cast"/>
							<sp n="1">
								<speaker>UN SOLDAT FRANÇAIS, blessé au bras gauche, après avoir voulu verser à boire à son ami qui a refusé d’un geste et retiré son verre.</speaker>
								<l n="1">Tu préfères le vin ? Tu n’est pas dégoûté.</l>
								<l n="2">Selon l’occasion, je suis moins entêté.</l>
								<l n="3">Tu pourrais, et pour cause, aimer plus tard la bière…</l>
							</sp>
							<sp n="2">
								<speaker>DEUXIÈME SOLDAT FRANÇAIS.</speaker>
								<l n="4">Celle qui portera le roi Guillaume en terre.</l>
								<l part="I" n="5">Je ne dis pas.</l>
							</sp>
							<sp n="3">
								<speaker>PREMIER SOLDAT FRANÇAIS.</speaker>
								<l part="F" n="5">Tudieu ! quel affreux calembourg !</l>
								<l part="I" n="6">A quoi penses-tu donc ?</l>
							</sp>
							<sp n="4">
								<speaker>DEUXIÈME SOLDAT FRANÇAIS.</speaker>
								<l part="F" n="6">Je pense à Wissembourg !</l>
								<l n="7">Pour l’honneur du drapeau, c’est une flétrissure !</l>
							</sp>
							<sp n="5">
								<speaker>PREMIER SOLDAT FRANÇAIS.</speaker>
								<l n="8">Plus égoïste, hélas ! je pense à ma blessure.</l>
								<l n="9">Et puis, bah ! je pressens que nous serons vengés.</l>
								<l n="10">Je crois les voir bondir comme des enragés</l>
								<l n="11">Et sus, aux Prussiens, combattre à l’arme blanche.</l>
								<l n="12">La belle fera tout, passe donc pour la manche.</l>
								<stage>(Voyant venir LE LIEUTENANT.)</stage>
								<l n="13">Voici venir à nous notre bon lieutenant,</l>
								<l n="14">Toujours triste, pensif et toujours maugréant.</l>
							</sp>
						</div>
						<div type="scene" n="2">
							<head type="number">SCÈNE II</head>
							<sp n="6">
								<speaker>LE LIEUTENANT. Il entre du deuxième plan à droite et vient lentement dire son monologue au public sur le milieu de la scène. Il est censé se parler à lui-même ; sa parole est entrecoupée. L’orchestre joue en sourdine un air mélancolique. Les Prussiens ont cessé leur jeu et se sont levés en chuchotant.</speaker>
								<l n="15">C’en est fait ! la victoire, à nos rangs infidèle,</l>
								<l n="16">Dans les camps ennemis va déployer son aile !</l>
								<l n="17">Et de mes frères morts, dont les corps mutilés</l>
								<l n="18">Assourdissaient les pas des coursiers affolés,</l>
								<l n="19">Dont le funèbre amas protégeait la retraite,</l>
								<l n="20">Des… fuyards ont-ils dit, essuyant leur défaite,</l>
								<l n="21">Elle a laissé sans prix l’immortel dévouement !</l>
								<l n="22">O France ! dans ton sein que nul dissentiment</l>
								<l n="23">Ne s’oppose à ta marche, active et mugissante,</l>
								<l n="24">Pour chasser des Teutons la horde envahissante.</l>
								<l n="25">Puissent tes fils, goûtant les bienfaits d’un accord,</l>
								<l n="26">A tes mâles accents se ruer vers le Nord,</l>
								<l n="27">Y planter les jalons de leur indépendance,</l>
								<l n="28">Et faire entendre enfin ton cri de délivrance !</l>
							</sp>
							<sp n="7">
								<speaker>PREMIER PRUSSIEN.</speaker>
								<l n="29">Il faut en convenir, ce sont de fiers soldats,</l>
								<l n="30">Intrépides guerriers, se jouant des combats.</l>
							</sp>
							<sp n="8">
								<speaker>DEUXIÈME PRUSSIEN.</speaker>
								<l n="31">Ce que j’admire en eux, c’est leur air plein d’audace.</l>
							</sp>
							<sp n="9">
								<speaker>TROISIÈME PRUSSIEN.</speaker>
								<l n="32">Moi, leur agilité, qui tue ou qui fracasse</l>
								<stage>Il a montré tour à tour son œil poché et sa jambe malade.</stage>
							</sp>
							<sp n="10">
								<speaker>QUATRIÈME PRUSSIEN.</speaker>
								<l n="33">Je m’en souviens, de reste, ils m’ont fait brêche-dent.</l>
								<l n="34">Puis j’ai devant les yeux, grouillant comme un serpent,</l>
								<l n="35">Un mâtin de turco qui m’a fait tomber… pile</l>
								<l n="36">Après s’être, par moi, garé d’un projectile.</l>
							</sp>
							<sp n="11">
								<speaker>PREMIER PRUSSIEN.</speaker>
								<l n="37">Les zouzous m’ont joué ce tour-là plusieurs fois,</l>
								<l part="I" n="38">Aussi suis-je manchot</l>
							</sp>
							<sp n="12">
								<speaker>DEUXIÈME SOLDAT FRANÇAIS.(à part).</speaker>
								<l part="F" n="38">Beaucoup jambes de bois.</l>
								<l n="39">Provision d’hiver pour chauffer l’ambulance.</l>
							</sp>
							<sp n="13">
								<speaker>PREMIER SOLDAT FRANÇAIS.</speaker>
								<l part="I" n="40">Tu comprends l’allemand ?</l>
							</sp>
							<sp n="14">
								<speaker>DEUXIÈME SOLDAT FRANÇAIS.</speaker>
								<l part="F" n="40">Un brin ; dans mon enfance,</l>
								<l n="41">Mon père, un vieux troupier de l’empire dernier,</l>
								<l n="42">Sous lequel il fut fait, comme nous, prisonnier,</l>
								<l n="43">Eut le temps d’essayer d’apprendre ce langage.</l>
								<l n="44">J’y goûtai quelque peu, c’est tout mon héritage.</l>
								<stage>Les Prussiens sont sortis depuis le commencement de ce dialogue.</stage>
							</sp>
						</div>
						<div type="scene" n="3">
							<head type="number">SCÈNE III</head>
							<sp n="15">
								<speaker>LE LIEUTENANT. animé, qui est allé s’asseoir à la table des Français après leur avoir serré la main, dès qu’il a eu dit son monologue.</speaker>
								<l n="45">Il a dû te léguer autre chose, mon vieux :</l>
								<l n="46">L’invincible désir de venger nos aïeux !</l>
							</sp>
							<sp n="16">
								<speaker>DEUXIÈME SOLDAT FRANÇAIS.</speaker>
								<l n="47">Oui ; mais comme un éclair qu’abandonne la foudre</l>
								<l n="48">Il menace impuissant ! Ce sont là feux de poudre.</l>
							</sp>
							<sp n="17">
								<speaker>PREMIER SOLDAT FRANÇAIS.</speaker>
								<l n="49">Plus que jamais, hélas ! ils consument nos cœurs !</l>
							</sp>
						</div>
						<div type="scene" n="4">
							<head type="number">SCÈNE IV</head>
							<sp n="18">
								<speaker>LA VICTOIRE, couronnée de laurier (musique).</speaker>
								<l n="50">Héros de Wissembourg, ne vers plus de pleurs.</l>
								<l n="51">Notre mère a sonné l’heure de la vengeance.</l>
								<stage>(Au lieutenant)</stage>
								<l n="52">Tu m’accusais à tort, ami, dans ta souffrance.</l>
								<l n="53">Je voulais apparaitre et ne le pouvais pas.</l>
								<l n="54">La justice, toujours, doit diriger mes pas.</l>
								<l n="55">Elle ordonne, et j’accours ; je ne vis que pour elle.</l>
								<l n="56">Si parfois, sans pitié, comme une criminelle,</l>
								<l n="57">Elle me fait subir une étrange merci,</l>
								<l n="58">C’est quelle a ses projets et qu’il le faut ainsi.</l>
								<l n="59">Lorsque des Prussiens la force quintuplée</l>
								<l n="60">Surprit un défilé de ta vaillante armée,</l>
								<l n="61">J’allai de la justice implorer le secours :</l>
								<l n="62">“ Il faut que cet échec pour la France ait son cours,”</l>
								<l n="63">Me dit-elle, d’un œil rempli de quiétude.</l>
								<l n="64">“ Son retour à la gloire est ma suprême étude.</l>
								<l n="65">Trop inégaux pour vaincre avant d’autres renforts,</l>
								<l n="66">Ses soldats tomberont ; mais devant qu’ils soient morts,</l>
								<l n="67">Ils auront ébranlé l’ennemi dans la lutte,</l>
								<l n="68">Et vingt pour un mourront entraînés dans leur chute.</l>
								<l n="69">Ils apprendront du moins à ces fiers Allemands</l>
								<l n="70">Qu’aux héros succombé survivront des géants !”</l>
							</sp>
							<sp n="19">
								<speaker>LE LIEUTENANT.</speaker>
								<l n="71">Que pouvons-nous sans toi si tu ne nous protèges ?</l>
							</sp>
							<sp n="20">
								<speaker>LA VICTOIRE.</speaker>
								<l n="72">Sans ce dessein, ami, pourquoi t’apparaitrais-je ?</l>
								<l n="73">Courage, et sur ton front le paisible olivier</l>
								<l n="74">Mêlera sa ramure à celle du laurier.</l>
								<stage>(Au deuxième soldat)</stage>
								<l n="75">Toi, joins à ta valeur une hypocrite audace,</l>
								<l part="I" n="76">Sache d’un ennemi…</l>
							</sp>
							<sp n="21">
								<speaker>DEUXIÈME SOLDAT FRANÇAIS</speaker>
								<l part="F" n="76">Saisir le carapace ?</l>
							</sp>
							<sp n="22">
								<speaker>LA VICTOIRE, qui a fait un signe affirmatif.</speaker>
								<l n="77">De plus, à la faveur d’un accent bavarois,</l>
								<l n="78">Traverse sans entrave et les camps et les bois ;</l>
								<l n="79">Dénonce des tyrans l’orgueilleuse tactique.</l>
								<l n="80">Ils sont tous espions, sois-le par politique.</l>
								<l n="81">Rien ne doit répugner contre la trahison</l>
								<l n="82">Dont la ruse et l’épée, et le vol le blason !</l>
							</sp>
							<sp n="23">
								<speaker>DEUXIÈME SOLDAT.</speaker>
								<l n="83">Ah ! puisque ton concours assure notre gloire,</l>
								<l n="84">Pour mériter de toi nous doterons l’histoire !</l>
							</sp>
							<stage>
								(Les soldats sortent pendant que LE LIEUTENANT qui traverse lentement le théâtre va<lb/>
								s’asseoir en tombant sur la chaise où il était assis à la table de gauche.<lb/>
								Musique douce.)
							</stage>
						</div>
						<div type="scene" n="5">
							<head type="number">SCÈNE V</head>
							<sp n="24">
								<speaker>LE LIEUTENANT.</speaker>
								<l n="85">La gloire est un vain mot pour ceux qui ne sont plus !</l>
							</sp>
							<sp n="25">
								<speaker>LA VICTOIRE, après une pause.</speaker>
								<l n="86">N’exhale pas, enfant, de regrets superflus.</l>
								<l n="87">Va, quand un noble cœur bat dans une poitrine,</l>
								<l n="88">La vie est un roseau que la tourmente incline,</l>
								<l n="89">Que l’ouragan déplace et qu’il fait dépérir.</l>
								<l n="90">Le vent poussait au Nord, ils y devaient courir</l>
								<l n="91">Affronter le trépas pour sauver la patrie !</l>
								<l n="92">Mais au delà crois-tu l’espérance flétrie ?</l>
								<l n="93">De la nuit des tombeaux que t’est-il revenu ?</l>
								<l n="94">De son but éternel Dieu t’a-t-il prévenu ?</l>
								<l n="95">Le doute, je le sais, peut désoler ton âme,</l>
								<l n="96">Mais, crois-moi, la raison est un puissant dictame,</l>
								<l n="97">Qui mène au droit sentier l’esprit le plus pervers,</l>
								<l n="98">Quelque égaré qu’il soit à l’aspect des revers.</l>
								<l n="99">L’amour de la patrie est un lien suprême</l>
								<l n="100">Dont l’instinct vit chez l’homme à l’insu de lui-même.</l>
								<l n="101">S’il déserte rêveur, épris d’un ciel vermeil,</l>
								<l n="102">Au loin il chantera sa terre et son soleil.</l>
								<l n="103">De l’honneur du pays il se rend solidaire,</l>
								<l n="104">Ni l’or, ni les grandeurs ne l’en sauraient distraire ;</l>
								<l n="105">Jaloux de le défendre on le voit accourir,</l>
								<l n="106">En coutât-il pour vaincre aussi bien d’en mourir.</l>
								<l n="107">Si donc, pour comprimer cet élan qui l’oppresse,</l>
								<l n="108">Qui te conduit peut-être à la pente traîtresse</l>
								<l n="109">Tu ne sens en ton cœur, en face de la mort,</l>
								<l n="110">Qu’un entraînant besoin de céder à ton sort,</l>
								<l n="111">Il faut qu’il soit prescrit par une loi divine</l>
								<l n="112">L’âme, c’est le moteur ; le corps, c’est la machine.</l>
								<l n="113">L’âme prend son essor sous le souffle des cieux,</l>
								<l n="114">En imprimant au corps son élan glorieux !</l>
								<l n="115">Dieu le voulant ainsi, tu n’as plus rien à craindre,</l>
								<l n="116">La mort en dit pas tout, cesse, enfant, de te plaindre !</l>
							</sp>
							<sp n="26">
								<speaker>LE LIEUTENANT. se levant.</speaker>
								<l part="I" n="117">Mais la cause !…</l>
							</sp>
							<sp n="27">
								<speaker>LA VICTOIRE, vivement.</speaker>
								<l part="F" n="117">Elle est sainte, il n’en faut pas douter</l>
								<l n="118">On insulte au drapeau, fais-le donc respecter.</l>
								<l n="119">On envahit la France, accours pour la défendre,</l>
								<l n="120">Combats pour la sauver, dût la mort te surprendre !</l>
								<l n="121">C’est un pieux devoir, poursuis-le jusqu’au bout :</l>
								<l n="122">Dieu l’exige, il est juste, et tient compte de tout.</l>
							</sp>
							<sp n="28">
								<speaker>LE LIEUTENANT.</speaker>
								<l n="123">Si le destin voulait qu’elle nous fût conquise ?…</l>
							</sp>
							<sp n="29">
								<speaker>LA VICTOIRE.</speaker>
								<l n="124">Les rois peuvent, sans doute, en parler à leur guise,</l>
								<l n="125">Mais les peuples tenant des pouvoirs de plus haut</l>
								<l n="126">Savent sauvegarder leur précieux dépôt.</l>
								<l n="127">L’équilibre ébranlé mène à la décadence :</l>
								<l n="128">Le Nord boit à prix d’or le doux vin de la France</l>
								<l n="129">Et celle-ci reçoit ses produits liquoreux.</l>
								<l n="130">Le Sud importe au loin ses filaments soyeux.</l>
								<l n="131">Si, pour Flore, Bacchus daigne égrainer sa treille,</l>
								<l n="132">De bouquets en retour, elle emplit sa corbeille.</l>
								<l n="133">Pomone, dans l’Ouest, cultive se vergers.</l>
								<l n="134">Flore, vers l’Orient, sème ses orangers.</l>
								<l n="135">Aux quatre points, Cérès, de ses blondes aigrettes,</l>
								<l n="136">A doré nos sillons, ennemis des disettes.</l>
								<l n="137">Quoique l’Est ait reçu ses dons à plans mains,</l>
								<l n="138">Elle a jonché de blé les bords de nos chemins ;</l>
								<l n="139">Diane a, chez les czars, protégé les hermines ;</l>
								<l n="140">Et l’avare Midas, dont l’or est dans les mines,</l>
								<l n="141">A doté pour sa part les arides climats.</l>
								<l n="142">L’équilibre fait donc le bonheur des États,</l>
								<l n="143">Puisque sur le négoce ils basent leur fortune,</l>
								<l n="144">L’invasion n’est point simplement importune,</l>
								<l n="145">Mais criminelle encore. Il faut châtier.</l>
								<l n="146">Et d’avides félons, délivrer le foyer !</l>
								<stage>
									(Depuis un moment on entend jouer en sourdine l’air de la Marseillaise qui se mêle<lb/>
									à des coups de canon. La Victoire se dirige vers le deuxième plan à droite)
								</stage>
								<l n="147">Entends-tu ? du canon la voix retentissante</l>
								<l n="148">Jette enfin vers le Nord, le deuil et l’épouvante.</l>
								<l n="149">D’innombrables soldats, comme des flots vengeurs,</l>
								<l n="150">Vont submerger les rangs de vos envahisseurs.</l>
								<l n="151">Guerre aux tyrans ! suis-moi, ta cause est noble et sage,</l>
								<l n="152">Rendons meurtre pour meurtre, outrage pour outrage.</l>
							</sp>
						</div>
						<div type="scene" n="6">
							<head type="number">SCÈNE VI</head>
							<stage>
								Des soldats apparaissent en grand nombre par toutes les trouées.<lb/>
								Le Deuxième soldat français porte l’épée du lieutenant et le premier<lb/>
								porte le drapeau français. La Victoire a pris l’épée des mains du<lb/>
								deuxième soldat pour la remettre au lieutenant.
							</stage>
							<sp n="30">
								<speaker>LA VICTOIRE.</speaker>
								<l n="153">Les voici tous, partons et de ton glaive armé</l>
								<l n="154">Fais mouvoir le fléau de colère enflammé !!!</l>
							</sp>
							<sp n="31">
								<speaker>LE LIEUTENANT. l’épée à la main, s’avance sur la scène et dit d’une voix sourde qui éclate peu à peu :</speaker>
								<l n="155">Eh bien ! puisque le sol sacré de ma patrie</l>
								<l n="156">Fut souillé par les pas d’une horde ennemie ;</l>
								<l n="157">Puisque mes frères morts, de larmes et de sang</l>
								<l n="158">Ont baigné le terrain qui fut son premier banc ;</l>
								<l n="159">Puisque le laurier vit des humides rosées ;</l>
								<l n="160">Puisque le palmier croit sous les chaudes ondées ;</l>
								<l n="161">Puisque de Wissembourg les près et les sillons</l>
								<l n="162">Reçurent pour engrais nos sanglants bataillons ;</l>
								<l n="163">Puisqu’au bord du torrent la terre est plus féconde ;</l>
								<l n="164">Puisque le roc altier se redresse dans l’onde,</l>
								<l n="165">Et puisque sur sa crête un vautour furieux</l>
								<l n="166">Dévora Prométhée aux délices des dieux,</l>
								<l n="167">Que le laurier béni, gage de la victoire,</l>
								<l n="168">Aux yeux de l’univers annonce notre gloire !</l>
								<l n="169">Que le vaste palmier, symbole du martyr,</l>
								<l n="170">Élève au ciel sa gerbe au nom du repentir !</l>
								<l n="171">Que nos sillons, nourris par d’humaines matières,</l>
								<l n="172">Produisent de héros des légions guerrières</l>
								<l n="173">Et revendent les pleurs dont s’abreuva le sol !</l>
								<l n="174">Que le vautour rongeur arrête enfin son vol</l>
								<l n="175">Pour broyer les tyrans sur le roc qui s’écroule.</l>
								<l n="176">Qu’ils soient tous engloutis et roulés par la houle !!!</l>
								<stage>(Il remonte la scène)</stage>
								<l n="177">Je me retrouve enfin, amis, sus aux Judas.</l>
								<l n="178">Ils sont revenus en France, ils n’en sortiront pas !</l>
							</sp>
							<sp n="32">
								<speaker>LA VICTOIRE, qui s’est emparée du drapeau que tenait le premier soldat.</speaker>
								<l n="179">Non, car tout est soldat pour sauver la patrie,</l>
								<l n="180">Car on aime sa mère avec idolâtrie,</l>
								<l n="181">Car la France est la vôtre et qu’on la veut flétrir,</l>
								<l n="182">Car vous avez juré de vaincre ou de mourir.</l>
							</sp>
							<stage>
								(A ce moment tous tendent la main pour faire un serment et des flammes
								de plusieurs nuances s’allument dans le fond du théâtre.)
							</stage>
							<sp n="33">
								<speaker>LE LIEUTENANT.</speaker>
								<l n="183">Et nous jurons encor par nos illustres pères,</l>
								<l n="184">Par la vertu souillé et les pleurs de nos mères,</l>
								<l n="185">Par ceux auxquels l’outrage inflige une douleur,</l>
								<l n="186">Par nos soldats martyrs, tombés au champ d’honneur,</l>
								<l n="187">Que tous, vieillards, enfants échappés aux mamelles,</l>
								<l n="188">Femmes, noble et vilain, abjurant leurs querelles,</l>
								<l n="189">Animés par la haine et le même désir,</l>
								<l n="190">Vont combattre en ton nom jusqu’au dernier soupir !</l>
								<l n="191">Car tu n’aurais rien dit ni fait pour nous, Victoire,</l>
								<l n="192">S’il manquait à nos forts, à notre territoire,</l>
								<l n="193">Un vestige, un lambeau conquis par nos aïeux,</l>
								<l n="194">Si nous n’étions leurs fils et ne mourrions comme eux !</l>
							</sp>
							<stage>
								(L’orchestre, qui jouait en trémolo, fait fortement entendre le refrain
								de la Marseillaise, et de violents coups de canon résonnent dans le
								lointain. Tableau.)
							</stage>
						</div>
					</div>
				</div>
			</div></body></text></TEI>