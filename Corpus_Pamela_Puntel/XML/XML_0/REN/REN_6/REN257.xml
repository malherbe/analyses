<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">AU BRUIT DU CANON</title>
				<title type="medium">Édition électronique</title>
				<author key="REN">
					<name>
						<forename>Armand</forename>
						<surname>RENAUD</surname>
					</name>
					<date from="1836" to="1895">1836-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">REN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>AU BRUIT DU CANON</title>
						<author>ARMAND RENAUD</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https://archive.org/details/posiesrecueili00renauoft/page/n321</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>AU BRUIT DU CANON</title>
								<author>ARMAND RENAUD</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="REN257">
				<head type="main">A UN PROPHÈTE ALLEMAND</head>
				<head type="sub_2">Poëme récité sur le théâtre <lb/>de la Comédie-Française par Mlle FAVART</head>
				<lg n="1">
					<l n="1" num="1.1">Poëte de la blonde et rêveuse Allemagne</l>
					<l n="2" num="1.2">Où vont les fiancés, à travers la campagne,</l>
					<l n="3" num="1.3">Voir reposer la lune au cœur des lys fleuris,</l>
					<l n="4" num="1.4">Toi qui, haussent ta lèvre au clairon du prophète</l>
					<l n="5" num="1.5">Et couronnant ton luth comme pour une fête,</l>
					<l n="6" num="1.6">As prédit la ruine et la mort de Paris,</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Poëte, tu dis vrai : le temps viendra sans doute</l>
					<l n="8" num="2.2">Où la lueur qu’on suit et la voix qu’on écoute,</l>
					<l n="9" num="2.3"><space unit="char" quantity="8"/>Le grand Paris ne sera plus ;</l>
					<l n="10" num="2.4">Où du sable onduleux, tacheté de broussaille,</l>
					<l n="11" num="2.5">Recouvrira sans bruit l’océan qui tressaille</l>
					<l n="12" num="2.6"><space unit="char" quantity="8"/>Dans ce vivant flux et reflux.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Poëte, tu dis vrai : sur la Seine déserte,</l>
					<l n="14" num="3.2">Le pluvier seul fendra la flottante herbe verte,</l>
					<l n="15" num="3.3">Pour atteindre l’insecte à l’homme survivant.</l>
					<l n="16" num="3.4">Nos flambeaux feront place à l’ombre sous les astres ;</l>
					<l n="17" num="3.5">Et comme nos splendeurs, s’en iront nos désastres</l>
					<l n="18" num="3.6">Dans l’éclat du soleil et la plainte du vent.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Mais en ces jours futurs que l’inconnu dérobe,</l>
					<l n="20" num="4.2">Si quelque nouveau peuple existe sur le globe,</l>
					<l n="21" num="4.3"><space unit="char" quantity="8"/>Le grand peuple des jours meilleurs,</l>
					<l n="22" num="4.4">Le peuple fait d’amour, de lumière, de joie,</l>
					<l n="23" num="4.5">Où nul ne sera plus le bourreau ni la proie,</l>
					<l n="24" num="4.6"><space unit="char" quantity="8"/>Où tous seront les travailleurs ;</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><space unit="char" quantity="8"/>S’il vient jamais, le peuple libre,</l>
					<l n="26" num="5.2"><space unit="char" quantity="8"/>Le peuple beau, pur, fraternel,</l>
					<l n="27" num="5.3"><space unit="char" quantity="8"/>Dont l’harmonieux équilibre</l>
					<l n="28" num="5.4"><space unit="char" quantity="8"/>Rayonnera comme le ciel,</l>
					<l n="29" num="5.5"><space unit="char" quantity="8"/>Et qui, sans combat ni victime,</l>
					<l n="30" num="5.6"><space unit="char" quantity="8"/>De gravir le progrès sublime,</l>
					<l n="31" num="5.7"><space unit="char" quantity="8"/>Infiniment, de cime en cime,</l>
					<l n="32" num="5.8"><space unit="char" quantity="8"/>Fera son bonheur éternel !</l>
				</lg>
				<lg n="6">
					<l n="33" num="6.1">Les savants de ce peuple iront par nos collines,</l>
					<l n="34" num="6.2">Fouillant le sable triste où gisent les ruines</l>
					<l n="35" num="6.3">De la ville, jadis colosse radieux,</l>
					<l n="36" num="6.4">Et resteront muets de pieuse surprise</l>
					<l n="37" num="6.5">Devant un arc de pierre, un haut portail d’église,</l>
					<l n="38" num="6.6">Un fronton par lequel les grands hommes sont dieux</l>
				</lg>
				<lg n="7">
					<l n="39" num="7.1">Or, dans ce siècle-là, ne crois pas, ô poëte,</l>
					<l n="40" num="7.2">Qu’une Europe sera sans Paris, corps sans tête,</l>
					<l n="41" num="7.3"><space unit="char" quantity="8"/>Que les Allemands survivront.</l>
					<l n="42" num="7.4">La Béotie est morte en même temps qu’Athènes.</l>
					<l n="43" num="7.5">Les rivaux d’à présent, ombres alors lointaines,</l>
					<l n="44" num="7.6"><space unit="char" quantity="8"/>Au même linceul dormiront.</l>
				</lg>
				<lg n="8">
					<l n="45" num="8.1">Et comme on aimera l’idée et non le glaive,</l>
					<l n="46" num="8.2">Paris, pour l’avenir que pressentit son rêve,</l>
					<l n="47" num="8.3">Paris, pour ses frissons dont le monde fut plein,</l>
					<l n="48" num="8.4">Pour son rugissement contre les tyrannies,</l>
					<l n="49" num="8.5">Rayonnera parmi les poussières bénies…</l>
					<l n="50" num="8.6">— Mais nul ne cherchera la place où fut Berlin.</l>
				</lg>
			</div></body></text></TEI>