<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES GRAINS DE POUDRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VDS">
					<name>
						<forename>Ali-Joseph-Augustin</forename>
						<surname>VIAL DE SABLIGNY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>604 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VDS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES GRAINS DE POUDRE</title>
						<author>ALI VIAL DE SABLIGNY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5612402j.r=VIAL%20DE%20SABLIGNY%20ALI-JOSEPH-AUGUSTIN%20LES%20GRAINS%20DE%20POUDRE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES GRAINS DE POUDRE</title>
								<author>ALI VIAL DE SABLIGNY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DESCHAMPS, PAPETIER-LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VDS9">
				<head type="main">ALSACE LORRAINE</head>
				<lg n="1">
					<l n="1" num="1.1">Chers et bons habitants d'Alsace et de Lorraine,</l>
					<l n="2" num="1.2">C'est en vain qu'à la Prusse un traité vous enchaîne ;</l>
					<l n="3" num="1.3">C'est en vain qu'à nos bras on vient vous arracher,</l>
					<l n="4" num="1.4">La France, de ses fils ne peut se détacher.</l>
					<l n="5" num="1.5">Vainement vous tombez en des mains étrangères,</l>
					<l n="6" num="1.6">Nous ne cesserons pas de voir en vous des frères.</l>
					<l n="7" num="1.7">Au vôtre notre cœur sera toujours uni,</l>
					<l n="8" num="1.8">Et toujours votre nom, d'un charme indéfini</l>
					<l n="9" num="1.9">S'entourera chez nous. Toujours notre pensée</l>
					<l n="10" num="1.10">S'envolera vers vous doucement caressée,</l>
					<l n="11" num="1.11">Pour causer avec vous de notre beau pays</l>
					<l n="12" num="1.12">Nous franchirons l'espace, infortunés amis !</l>
					<l n="13" num="1.13">Nos mains iront presser les vôtres dans un rêve</l>
					<l n="14" num="1.14">Jusqu'au jour où le Ciel à nos maux fera trêve.</l>
				</lg>
				<lg n="2">
					<l n="15" num="2.1">Non, il n'est pas permis d 'oublier un instant</l>
					<l n="16" num="2.2">L'ardeur avec laquelle on vous à vus luttant</l>
					<l n="17" num="2.3">Contre cet ennemi dont vous êtes la proie,</l>
					<l n="18" num="2.4">Et qui sous son talon vous écrase et vous broie.</l>
					<l n="19" num="2.5">Non non ! noble Strasbourg, héroïque cité,</l>
					<l n="20" num="2.6">Dont la gloire vivra toute l'éternité,</l>
					<l n="21" num="2.7">Tu n'auras pas, lionne à la fauve crinière,</l>
					<l n="22" num="2.8">Combattu fièrement jusqu'à l'heure dernière ;</l>
					<l n="23" num="2.9">Tu n'auras pas d'un siège enduré les tourments</l>
					<l n="24" num="2.10">Et supporté le feu des canons allemands,</l>
					<l n="25" num="2.11">Pour que l'on t'abandonne en la crise fatale</l>
					<l n="26" num="2.12">Qui te jette au pouvoir de la force brutale.</l>
					<l n="27" num="2.13">Nous entendons ta voix et tes gémissements,</l>
					<l n="28" num="2.14">Tes larmes, les soupirs et tes déchirements.</l>
					<l n="29" num="2.15">Tu nous dis : Sauvez-moi ! sauvez-moi de l'abîme !</l>
					<l n="30" num="2.16">Oui, nous le sauverons, malheureuse victime ;</l>
					<l n="31" num="2.17">Nous saurons le soustraire à ton horrible sort,</l>
					<l n="32" num="2.18">Dussions-nous pour cela braver cent fois la mort.</l>
					<l n="33" num="2.19">Nous faisons le serment d'accomplir cette tâche :</l>
					<l n="34" num="2.20">Quiconque y manquerait serait un traître, un lâche !</l>
				</lg>
				<lg n="3">
					<l n="35" num="3.1">Eh quoi ! ces vastes champs couverts de blonds épis,</l>
					<l n="36" num="3.2">Ces coteaux, ces vallons aux verdoyants lapis,</l>
					<l n="37" num="3.3">Par le pied d'un vainqueur tout bouffi d'arrogance,</l>
					<l n="38" num="3.4">Seraient longtemps foulés ? Non, non ! c'est trop d'offense !</l>
					<l n="39" num="3.5">Que ce peuple odieux ne trouve que du fiel ;</l>
					<l n="40" num="3.6">Pour tes fils, sol fécond, garde en ton sein le miel.</l>
					<l n="41" num="3.7">Que pour eux seulement soient l'ombre des grands chênes,</l>
					<l n="42" num="3.8">Le toit de tes hameaux et l'onde des fontaines,</l>
					<l n="43" num="3.9">Ces trésors sont les leurs, ils leur furent volés ;</l>
					<l n="44" num="3.10">Mais avant que vingt ans, au plus, soient écoulés,</l>
					<l n="45" num="3.11">Ils seront retournés à leurs seuls et vrais maîtres,</l>
					<l n="46" num="3.12">Nous aurons reconquis le bien de nos ancêtres,</l>
					<l n="47" num="3.13">Ne nous arrêtant plus que ne soit effacé</l>
					<l n="48" num="3.14">Le sillon que le crime en tous lieux a tracé.</l>
					<l n="49" num="3.15">Qu'enfin ne soient vengés les affronts, les outrages,</l>
					<l n="50" num="3.16">Et que ne soit rendu le repos aux bocages.</l>
					<l n="51" num="3.17">Quelle indicible joie et quelle fête, alors !</l>
					<l n="52" num="3.18">Quelle vive allégresse et quels heureux transports,</l>
					<l n="53" num="3.19">Quand il aura brillé, ce jour semé d'ivresse,</l>
					<l n="54" num="3.20">Dont l'aurore viendra dissiper la tristesse</l>
					<l n="55" num="3.21">Qui voile notre front, ô regrettés absents ?</l>
					<l n="56" num="3.22">Nous chanterons en chœur l'hymne aux divins accents</l>
					<l n="57" num="3.23">Que savent les échos : l'hymne de la patrie,</l>
					<l n="58" num="3.24">Et l'amour cueillera les fleurs de la prairie.</l>
				</lg>
				<lg n="4">
					<l n="59" num="4.1">Ainsi, nobles martyrs, ne perdez pas l'espoir,</l>
					<l n="60" num="4.2">Nous ne vous disons pas adieu, mais au revoir.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">2 avril 1871</date>
					</dateline>
				</closer>
			</div></body></text></TEI>