<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES GRAINS DE POUDRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VDS">
					<name>
						<forename>Ali-Joseph-Augustin</forename>
						<surname>VIAL DE SABLIGNY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>604 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VDS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES GRAINS DE POUDRE</title>
						<author>ALI VIAL DE SABLIGNY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5612402j.r=VIAL%20DE%20SABLIGNY%20ALI-JOSEPH-AUGUSTIN%20LES%20GRAINS%20DE%20POUDRE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES GRAINS DE POUDRE</title>
								<author>ALI VIAL DE SABLIGNY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DESCHAMPS, PAPETIER-LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VDS3">
				<head type="main">AUX MESSAGERS DE LA PATRIE</head>
				<head type="sub_2">Poésie récitée par Mme Eugénie PETIT</head>
				<head type="sub_2">HOMMAGE A M. LE BARON LARREY</head>
				<lg n="1">
					<l n="1" num="1.1">Venez, ô chers pigeons, messagers du Ciel pur,</l>
					<l n="2" num="1.2"><space unit="char" quantity="12"/>Ouvrez, ouvrez vos ailes !</l>
					<l n="3" num="1.3">Élancez-vous légers au milieu de l'azur,</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"/>Apportez des nouvelles !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Paris n'a plus, hélas ! qu'un étroit horizon !</l>
					<l n="6" num="2.2"><space unit="char" quantity="12"/>Et pour calmer ses craintes,</l>
					<l n="7" num="2.3">Il n'espère qu'en vous ! Venez de sa prison</l>
					<l n="8" num="2.4"><space unit="char" quantity="12"/>Adoucir les étreintes.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Paris ne sait rien, lui ! tandis que vous, là-bas,</l>
					<l n="10" num="3.2"><space unit="char" quantity="12"/>Vous avez vu nos frères :</l>
					<l n="11" num="3.3">La province se lève et marche, n'est-ce pas,</l>
					<l n="12" num="3.4"><space unit="char" quantity="12"/>Le cœur plein de colères ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Elle va nous aider à chasser les Prussiens,</l>
					<l n="14" num="4.2"><space unit="char" quantity="12"/>Peuple infâme et barbare</l>
					<l n="15" num="4.3">Tel qu'on n'en vit jamais dans les siècles anciens,</l>
					<l n="16" num="4.4"><space unit="char" quantity="12"/>Et que l'orgueil égare.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Dites, charmants pigeons, pour nos vaillants guerriers</l>
					<l n="18" num="5.2"><space unit="char" quantity="12"/>Déjà couverts de gloire,</l>
					<l n="19" num="5.3">Revenez-vous chargés d'innombrables lauriers,</l>
					<l n="20" num="5.4"><space unit="char" quantity="12"/>Doux fruit de la victoire ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Que vous serez heureux de regagner vos nids !</l>
					<l n="22" num="6.2"><space unit="char" quantity="12"/>Vos fils viennent d'éclore,</l>
					<l n="23" num="6.3">Et vous pourrez soigner la mère et les petits</l>
					<l n="24" num="6.4"><space unit="char" quantity="12"/>Que votre cœur adore.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Vous ne vous doutez pas quand vous quittez le sol</l>
					<l n="26" num="7.2"><space unit="char" quantity="12"/>Et planez dans la nue,</l>
					<l n="27" num="7.3">Que la mort, pour briser l'élan de votre vol,</l>
					<l n="28" num="7.4"><space unit="char" quantity="12"/>Guette votre venue !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">C'est à vos jours qu'en veut du farouche épervier</l>
					<l n="30" num="8.2"><space unit="char" quantity="12"/>La redoutable serre !</l>
					<l n="31" num="8.3">Une fois dans nos murs vous pourrez défier</l>
					<l n="32" num="8.4"><space unit="char" quantity="12"/>Ce dangereux corsaire.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Vous serez garantis à l'ombre des remparts,</l>
					<l n="34" num="9.2"><space unit="char" quantity="12"/>Armés et formidables.</l>
					<l n="35" num="9.3">Vous verra-t-on un jour peints sur nos étendards,</l>
					<l n="36" num="9.4"><space unit="char" quantity="12"/>Sauveurs incomparables ?</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Nous saurons vénérer comme un saint souvenir</l>
					<l n="38" num="10.2"><space unit="char" quantity="12"/>Votre mission d'ange,</l>
					<l n="39" num="10.3">Et puis, vous entendrez un peuple vous bénir,</l>
					<l n="40" num="10.4"><space unit="char" quantity="12"/>Gracieuse phalange !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Nous voudrions avoir, pour vous le consacrer,</l>
					<l n="42" num="11.2"><space unit="char" quantity="12"/>Un Capitole, un Temple,</l>
					<l n="43" num="11.3">Car , Rome, dans l'Histoire est seule à nous montrer</l>
					<l n="44" num="11.4"><space unit="char" quantity="12"/>Un aussi noble exemple.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">O pigeons-voyageurs ! que votre sort est beau !</l>
					<l n="46" num="12.2"><space unit="char" quantity="12"/>Pour vous ici l'on prie !</l>
					<l n="47" num="12.3">Et l'an soixante-dix vous aura fait l'oiseau</l>
					<l n="48" num="12.4"><space unit="char" quantity="12"/>Sacré de la Patrie.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 4 décembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>