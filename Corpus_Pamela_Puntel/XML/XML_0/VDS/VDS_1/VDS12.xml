<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LES GRAINS DE POUDRE</title>
				<title type="medium">Édition électronique</title>
				<author key="VDS">
					<name>
						<forename>Ali-Joseph-Augustin</forename>
						<surname>VIAL DE SABLIGNY</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>604 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">VDS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES GRAINS DE POUDRE</title>
						<author>ALI VIAL DE SABLIGNY</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5612402j.r=VIAL%20DE%20SABLIGNY%20ALI-JOSEPH-AUGUSTIN%20LES%20GRAINS%20DE%20POUDRE?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES GRAINS DE POUDRE</title>
								<author>ALI VIAL DE SABLIGNY</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>DESCHAMPS, PAPETIER-LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="VDS12">
				<head type="main">AIMONS-NOUS</head>
				<lg n="1">
					<l n="1" num="1.1">Repoussons désormais toute guerre civile,</l>
					<l n="2" num="1.2">Mes frères, aimons nous d'un saint attachement ;</l>
					<l n="3" num="1.3">De nos cœurs à chacun rendons l'accès facile</l>
					<l n="4" num="1.4">Et qu'un commun accord nous lie étroitement !</l>
					<l n="5" num="1.5">On ne peut rien fonder par le sang et les larmes :</l>
					<l n="6" num="1.6">Unissons nos efforts pour marcher en avant ;</l>
					<l n="7" num="1.7">Le travail, le progrès, sont les meilleures armes,</l>
					<l n="8" num="1.8">Pour atteindre le but que l'on va poursuivant.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Confondons tous les rangs dans un même principe,</l>
					<l n="10" num="2.2">La noblesse de l'âme avec celle du nom ;</l>
					<l n="11" num="2.3">Devant l'égalité que tout fiel se dissipe,</l>
					<l n="12" num="2.4">Du lien fraternel soyons tous un chaînon.</l>
					<l n="13" num="2.5">Que le même flambeau nous guide et nous éclaire,</l>
					<l n="14" num="2.6">Laissons parler en nous la voix de la raison,</l>
					<l n="15" num="2.7">Suivons, suivons le cours du bon-sens populaire.</l>
					<l n="16" num="2.8">De l'arbre du bonheur hâtons la floraison.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Nous ne devons former qu'une même famille ;</l>
					<l n="18" num="3.2">Au foyer de l'amour réchauffons-nous un peu :</l>
					<l n="19" num="3.3">Vivre sous un ciel pur où resplendit et brille</l>
					<l n="20" num="3.4">Le soleil de la paix, doit être notre vœu.</l>
					<l n="21" num="3.5">Préparons la moisson, fils de la République,</l>
					<l n="22" num="3.6">Faisons régner partout la justice et l'amour,</l>
					<l n="23" num="3.7">Et ce que nous fit perdre un souverain inique,</l>
					<l n="24" num="3.8">Nous l'aurons retrouvé, mes frères, en un jour.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Paris, 30 juin 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>