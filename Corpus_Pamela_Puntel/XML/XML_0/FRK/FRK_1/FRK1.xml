<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA HORDE ALLEMANDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>136 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LA HORDE ALLEMANDE</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61238688.r=FRANK%20F.%2C%20%20LA%20HORDE%20ALLEMANDE%2C?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LA HORDE ALLEMANDE</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>ALPHONSE LEMERRE</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-24" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
				<change when="2019-12-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-12-05" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRK1">
				<head type="main">LA HORDE ALLEMANDE</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Entendez-vous le bruit de la Horde allemande</l>
						<l n="2" num="1.2">Qui se rue au pillage avec des bras sanglants ?…</l>
						<l n="3" num="1.3">Amis, ne craignons rien : la Patrie est plus grande</l>
						<l n="4" num="1.4">Que la Horde sauvage attachée à ses flancs !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">La Patrie est sublime, ayant en elle une âme</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Qui lutte, au cri de liberté,</l>
						<l n="7" num="2.3">Pour le Droit immortel, non pour un lucre infâme —</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Prix d'un carnage illimité !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">O fous ! nous respections la vieille bonhomie</l>
						<l n="10" num="3.2">Louée avec tant d'art par leurs maîtres chanteurs,</l>
						<l n="11" num="3.3">Qui recouvrait en eux la pensée ennemie</l>
						<l n="12" num="3.4">Et semblait rayonner dans leurs regards menteurs :</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Des bandits ont vécu chez nous,-pleins de menaces,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Guetteurs cachés du camp germain ;</l>
						<l n="15" num="4.3">Et nous, l'esprit hanté d'illusions tenaces,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Nous leur avons tendu la main !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Espions sans vergogne, ils trahissaient leur hôte :</l>
						<l n="18" num="5.2">Un jour, ils ont quitté nos loyales maisons,</l>
						<l n="19" num="5.3">Pour revenir en masse, armés, la tête haute,</l>
						<l n="20" num="5.4">Disant : « — J'ai passé là… Rions, tuons, brisons !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« Brisons la porte ouverte et le dernier asile,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Et jetons-en la cendre au vent !…</l>
						<l n="23" num="6.3">« Nous reconnais-tu bien ? C'est nous, Peuple imbécile ! —</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Déshonneur et mort, en avant ! »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Et nous n'y pouvions croire ! Et, l'âme encore emplie</l>
						<l n="26" num="7.2">Du souvenir sacré des géants d'autrefois,</l>
						<l n="27" num="7.3">Luther, Goethe, Schiller,—sous qui tout orgueil plie, —</l>
						<l n="28" num="7.4">Nous leur avons crié : « — Ce n'est pas votre voix !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">« L'homme qui nous perdait, César du Bas-Empire,</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Despote ici, traître là-bas,</l>
						<l n="31" num="8.3">« Veut en vain, dans l'opprobre où sa fortune expire,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Trafiquer du sort des combats !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">« Voyez : hors de l'étreinte impure de cet homme,</l>
						<l n="34" num="9.2">« La France, calme et fière, offre au monde la paix…</l>
						<l n="35" num="9.3">« Ce n'est pas votre voix qui la raille et nous somme</l>
						<l n="36" num="9.4">« De ramper avilis entre vos rangs épais !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">« Cet exécrable cri ne sort pas de vos bouches :</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« — Écraser la France et Paris ! —</l>
						<l n="39" num="10.3">« Sommes-nous donc au temps des Nibelungs farouches,</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Et le monde est-il en débris ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">« La paix serait trop chère au prix de tant de honte !</l>
						<l n="42" num="11.2">« Le fer et la famine et la mort coûtent moins ;</l>
						<l n="43" num="11.3">« La mitraille demain soldera notre compte :</l>
						<l n="44" num="11.4">« Nous aurons, au grand jour, les peuples pour témoins ! »</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Or ils ont répondu : « — La justice, qu'importe ?</l>
						<l n="46" num="12.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Le droit de la guerre est grossier :</l>
						<l n="47" num="12.3">« Nous avons, pour broyer la raison la plus forte,</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Des canons de bronze et d'acier !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">« Des bords de la Baltique et du fond des bois sombres</l>
						<l n="50" num="13.2">« Nous sommes accourus au funèbre festin :</l>
						<l n="51" num="13.3">« De péril en péril, à travers les décombres,</l>
						<l n="52" num="13.4">« Nous épions l'instant marqué par le Destin,</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">« Et vous serez broyés, rêveurs à tête folle,</l>
						<l n="54" num="14.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Qui raisonnez sous le soufflet !</l>
						<l n="55" num="14.3">« Notre philosophie est un mot qui s'envole,</l>
						<l n="56" num="14.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Laissant la réplique au boulet ! »</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Ainsi, Kant et Mozart nous masquaient leurs boutiques,</l>
						<l n="58" num="15.2">Leurs ateliers de meurtre et leur culte du mal !</l>
						<l n="59" num="15.3">O France, ô ma patrie, à ces soudards mystiques</l>
						<l n="60" num="15.4">Cesse de faire appel au nom de l'Idéal !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Car si, croisant les bras, tu t'avançais pensive,</l>
						<l n="62" num="16.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Songeant aux Maîtres bien-aimés,</l>
						<l n="63" num="16.3">L'Allemagne rirait, et sa hache massive</l>
						<l n="64" num="16.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>S'abattrait sur tes bras fermés !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">O reîtres diplômés ! docteurs en fourberie !</l>
						<l n="66" num="17.2">Vous devez devant nous vous incliner bien bas,</l>
						<l n="67" num="17.3">Car nous demeurons droits jusqu'en notre furie !</l>
						<l n="68" num="17.4">Car nous avons du cœur et vous n'en avez pas !…</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">Pour moi qui m'enivrais de leurs pures idées,</l>
						<l n="70" num="18.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Moi, naïf, qui les admirais,</l>
						<l n="71" num="18.3">— Sachant par quel ressort leurs âmes sont-guidées,</l>
						<l n="72" num="18.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Je les repousse et je les hais !</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">Mesure qui voudra son esprit à leur toise !</l>
						<l n="74" num="19.2">J'aimerais mieux avoir, serf éternellement,</l>
						<l n="75" num="19.3">A mendier mon pain sur la terre gauloise,</l>
						<l n="76" num="19.4">Que d'être couronné sur le sol allemand !</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1">Depuis que dans Strasbourg tes trésors, ô Pensée,</l>
						<l n="78" num="20.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>N'ont pu trouver grâce à leurs yeux,</l>
						<l n="79" num="20.3">Je sens, je sens monter en mon âme offensée</l>
						<l n="80" num="20.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Un mur qui me sépare d'eux !</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1">Je sais tout ce que vaut l’œuvre d'hypocrisie</l>
						<l n="82" num="21.2">Dont ils avaient couvert la fange de leurs cœurs :</l>
						<l n="83" num="21.3">Leur suprême vertu, leur fleur de poésie,</l>
						<l n="84" num="21.4">Ils en ont fait litière en se croyant vainqueurs !</l>
					</lg>
					<lg n="22">
						<l n="85" num="22.1">Sans doute on les verrait, s'ils gardaient la victoire,</l>
						<l n="86" num="22.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Contempler d'un œil languissant</l>
						<l n="87" num="22.3">Le bleu vergiss-mein-nicht, en savourant leur gloire,</l>
						<l n="88" num="22.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Les pieds dans un ruisseau de sang !</l>
					</lg>
					<lg n="23">
						<l n="89" num="23.1">Comme le cri brutal de la Ménade antique,</l>
						<l n="90" num="23.2">Sans doute on entendrait sortir du sein des bois</l>
						<l n="91" num="23.3">Le refrain des goujats entonnant leur cantique,</l>
						<l n="92" num="23.4">Pour insulter de loin l'héroïsme aux abois :</l>
					</lg>
					<lg n="24">
						<l n="93" num="24.1">—« La honte n'est qu'un mot, et le droit n'est qu'une ombre !</l>
						<l n="94" num="24.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Et comme d'inertes moutons,</l>
						<l n="95" num="24.3">« Les peuples sont tremblants : sous la force et le nombre</l>
						<l n="96" num="24.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« L'Europe va mourir… Chantons !</l>
					</lg>
					<lg n="25">
						<l n="97" num="25.1">« Hurrah ! la France est morte, et de sa main crispée,</l>
						<l n="98" num="25.2">« Qui ne portera plus le glaive ou l'étendard,</l>
						<l n="99" num="25.3">« Nos gantelets de fer ont fait tomber l'épée !</l>
						<l n="100" num="25.4">« Hurrah ! pour nous ravir l'empire, il est trop tard !</l>
					</lg>
					<lg n="26">
						<l n="101" num="26.1">« Tous les fiers combattants qui s'agitaient naguère</l>
						<l n="102" num="26.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Ont roulé dans les grandes eaux,</l>
						<l n="103" num="26.3">« Dans le rouge océan de la dernière guerre,</l>
						<l n="104" num="26.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>« Et nous n'avons plus de rivaux ! »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="105" num="1.1">Or, quand le jour maudit de l'affreuse agonie</l>
						<l n="106" num="1.2">Sonnerait pour la France et pour le monde entier ;</l>
						<l n="107" num="1.3">Quand ton peuple sans âme, ô lourde Germanie,</l>
						<l n="108" num="1.4">Des peuples disparus se dirait héritier ;</l>
					</lg>
					<lg n="2">
						<l n="109" num="2.1">Quand il croirait avoir écrasé sous sa meule</l>
						<l n="110" num="2.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Fierté, Justice et Vérité, —</l>
						<l n="111" num="2.3">Même en ces jours d'horreur, et la nuit régnant seule</l>
						<l n="112" num="2.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Où régna jadis la clarté,</l>
					</lg>
					<lg n="3">
						<l n="113" num="3.1">Tout ne serait pas mort dans l'étendue immense :</l>
						<l n="114" num="3.2">Il te faudrait compter, race au stupide orgueil,</l>
						<l n="115" num="3.3">Avec cet inconnu qui toujours recommence,</l>
						<l n="116" num="3.4">Et l'indomptable Droit sortirait du cercueil !</l>
					</lg>
					<lg n="4">
						<l n="117" num="4.1">Tu tremblerais encore, et la voix de la France,</l>
						<l n="118" num="4.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Massacrée au coin du chemin,</l>
						<l n="119" num="4.3">Te poursuivrait partout d'un long cri d'espérance</l>
						<l n="120" num="4.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Au nom d'un autre genre humain !</l>
					</lg>
					<lg n="5">
						<l n="121" num="5.1">Du sein des archipels, du fond des mers lointaines,</l>
						<l n="122" num="5.2">Des peuples surgiraient qu'on ignorait hier,</l>
						<l n="123" num="5.3">Ramenant les soleils de Paris et d'Athènes,</l>
						<l n="124" num="5.4">Et leur âme vaincrait ton armure de fer !</l>
					</lg>
					<lg n="6">
						<l n="125" num="6.1">Et la ville au grand cœur, et la cité brisée</l>
						<l n="126" num="6.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Dont tu redoutes jusqu'au nom,</l>
						<l n="127" num="6.3">Triompherait de toi, nation méprisée,</l>
						<l n="128" num="6.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Gisant sous ton dernier canon !</l>
					</lg>
					<lg n="7">
						<l n="129" num="7.1">Mais l'âme de Paris, dans sa force sublime,</l>
						<l n="130" num="7.2">Ne se laissera pas assassiner par toi :</l>
						<l n="131" num="7.3">Tu n'auras, Allemagne, en retour de ton crime,</l>
						<l n="132" num="7.4">Que le fleuve de sang où se noîra ton roi !</l>
					</lg>
					<lg n="8">
						<l n="133" num="8.1">Et nous te défions à la face du monde,</l>
						<l n="134" num="8.2"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Jurant, si l'on veut notre mort,</l>
						<l n="135" num="8.3">De creuser dans nos murs une fosse profonde</l>
						<l n="136" num="8.4"><space unit="char" quantity="8"/><space unit="char" quantity="8"/>Pour tous les égorgeurs du Nord !</l>
					</lg>
				</div>
					<closer>
						<dateline>
							<date when="1870">Paris, 20 octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>