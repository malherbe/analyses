<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRK28">
				<head type="main">VIVE LA RÉPUBLIQUE !</head>
				<lg n="1">
					<l n="1" num="1.1">BIEN qu’au Peuple dans la stupeur,</l>
					<l n="2" num="1.2">Cherchant des yeux sa chaîne ancienne,</l>
					<l n="3" num="1.3">—Avant que la raison lui vienne,—</l>
					<l n="4" num="1.4">Ce mot éclatant fasse peur,</l>
					<l n="5" num="1.5">Lorsqu’il donne aux rois la réplique…</l>
					<l n="6" num="1.6"><space unit="char" quantity="4"/>Vive la République !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Liberté ! Cri jeune et vibrant</l>
					<l n="8" num="2.2">Sorti de l’âme universelle !</l>
					<l n="9" num="2.3">Poursuis le tyran qui chancelle ;</l>
					<l n="10" num="2.4">Qu’il te maudisse en expirant !—</l>
					<l n="11" num="2.5">Peuple, pour que ta loi s’applique,</l>
					<l n="12" num="2.6"><space unit="char" quantity="4"/>Vive la République !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Égalité ! Que ton niveau,</l>
					<l n="14" num="3.2">Pur triangle au tranchant de hache,</l>
					<l n="15" num="3.3">Pour fonder la Cité sans tache</l>
					<l n="16" num="3.4">S’imposant au monde nouveau,</l>
					<l n="17" num="3.5">Sauve le peuple famélique :</l>
					<l n="18" num="3.6"><space unit="char" quantity="4"/>Vive la République !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Fraternité ! Marchons unis !</l>
					<l n="20" num="4.2">Aimons-nous, pour que tout nous aide !</l>
					<l n="21" num="4.3">Pour que le monde entier nous cède,</l>
					<l n="22" num="4.4">Quand les despotes sont punis,</l>
					<l n="23" num="4.5">Chassons la haine à l’œil oblique :</l>
					<l n="24" num="4.6"><space unit="char" quantity="4"/>Vive la République !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Patrie ! œuvre de nos aïeux !</l>
					<l n="26" num="5.2">Patrie, ô famille agrandie,</l>
					<l n="27" num="5.3">Honte au fils qui te répudie !</l>
					<l n="28" num="5.4">Sur ton étendard glorieux</l>
					<l n="29" num="5.5">Je lis, sous le fer de la pique :</l>
					<l n="30" num="5.6"><space unit="char" quantity="4"/>Vive la République !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Au nom de la sainte équité,</l>
					<l n="32" num="6.2">Place !… Pour le siècle où nous sommes,</l>
					<l n="33" num="6.3">Pour tous les temps, pour tous les hommes,</l>
					<l n="34" num="6.4">Pour l’immense Postérité,</l>
					<l n="35" num="6.5">—Quand tout meurt, trône et basilique—</l>
					<l n="36" num="6.6"><space unit="char" quantity="4"/>Vive la République !</l>
				</lg>
			</div></body></text></TEI>