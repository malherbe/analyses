<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">HAINE AUX BARBARES</title>
				<title type="sub_1">CHANTS PATRIOTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="MDC">
					<name>
						<forename>Victor</forename>
						<nameLink>de</nameLink>
						<surname>MÉRI DE LA CANORGUE</surname>
					</name>
					<date from="1805" to="1875">1805-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>272 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MDC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">HAINE AUX BARBARES. CHANTS PATRIOTIQUES</title>
						<author>VICTOR DE MÉRI DE LA CANORGUE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30929677r.public</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>HAINE AUX BARBARES. CHANTS PATRIOTIQUES</title>
								<author>VICTOR DE MÉRI DE LA CANORGUE</author>
								<imprint>
									<pubPlace>MARSEILLE</pubPlace>
									<publisher>CAMOIN LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="MDC1">
				<head type="main">UN CRI VENGEUR</head>
				<head type="form">1er CHANT</head>
				<lg n="1">
					<l n="1" num="1.1">Toi, la mère des Preux, ma noble patrie !</l>
					<l n="2" num="1.2">Sous le poids des revers, vas-tu tomber flétrie ?</l>
					<l n="3" num="1.3">Vas-tu laisser ton sol souillé par l'étranger,</l>
					<l n="4" num="1.4">Sans saisir l'insolent, le vaincre et te venger,</l>
					<l n="5" num="1.5">Sans faire resplendir, sur le champ de bataille,</l>
					<l n="6" num="1.6">L'honneur de ton drapeau, broyé par la mitraille ?</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Les barbares sont là sur tes champs dévastés ;</l>
					<l n="8" num="2.2">Arme-toi de ta faulx, et, d'un élan superbe,</l>
					<l n="9" num="2.3">Les courbant sous tes pieds, fauche-les comme l'herbe</l>
					<l n="10" num="2.4">Tes bataillons surpris n'ont point été domptés :</l>
					<l n="11" num="2.5">Dans les rangs ennemis, ces lions pleins d'audace</l>
					<l n="12" num="2.6">Sont tombés en laissant une sanglante trace.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Il n'est pas une ville, il n'est pas un hameau</l>
					<l n="14" num="3.2">Qui de nos ennemis ne creuse le tombeau,</l>
					<l n="15" num="3.3">La France provoquée a frémi de colère,</l>
					<l n="16" num="3.4">Et je la vois déjà, dans sa fureur guerrière,</l>
					<l n="17" num="3.5">Coucher dans les sillons de ses champs désolés,</l>
					<l n="18" num="3.6">Ces ravageurs hideux, h sa rage immolés ;</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Faire partout la chasse à ces incendiaires</l>
					<l n="20" num="4.2">Qui brûlent les châteaux, les villes, les chaumières,</l>
					<l n="21" num="4.3">Et laissent sur leurs pas des larmes et du sang.</l>
					<l n="22" num="4.4">O ! France ! étouffe-les dans un effort puissant ;</l>
					<l n="23" num="4.5">Agrandis leurs tombeaux, jettes-y leurs cohortes,</l>
					<l n="24" num="4.6">Et du pays sauvé ferme sur eux les portes !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">L'étranger qui te foule, à chacun de ses pas,</l>
					<l n="26" num="5.2">Fait surgir des héros qui ne reculent pas,</l>
					<l n="27" num="5.3">Qui vengent ton honneur, ô France bien-aimée !</l>
					<l n="28" num="5.4">Dont le cœur héroïque, à son dernier soupir,</l>
					<l n="29" num="5.5">En s'éteignant pour toi palpite de plaisir.</l>
					<l n="30" num="5.6">N'es-tu pas des héros la terre renommée ?</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Qui pourrait t'asservir, mon vaillant pays ?</l>
					<l n="32" num="6.2">Tous les bras sont armés pour venger ta querelle ;</l>
					<l n="33" num="6.3">Quand tu cours des dangers, tous les cœurs sont unis,</l>
					<l n="34" num="6.4">Non, tu ne mourras point, ô patrie immortelle !</l>
					<l n="35" num="6.5">Depuis quinze cents ans aux siècles tu survis,</l>
					<l n="36" num="6.6">Et ton Dieu, dans le Ciel, est le Dieu de Clovis.</l>
				</lg>
			</div></body></text></TEI>