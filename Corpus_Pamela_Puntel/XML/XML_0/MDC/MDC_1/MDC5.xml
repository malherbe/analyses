<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">HAINE AUX BARBARES</title>
				<title type="sub_1">CHANTS PATRIOTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="MDC">
					<name>
						<forename>Victor</forename>
						<nameLink>de</nameLink>
						<surname>MÉRI DE LA CANORGUE</surname>
					</name>
					<date from="1805" to="1875">1805-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>272 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">MDC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">HAINE AUX BARBARES. CHANTS PATRIOTIQUES</title>
						<author>VICTOR DE MÉRI DE LA CANORGUE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30929677r.public</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>HAINE AUX BARBARES. CHANTS PATRIOTIQUES</title>
								<author>VICTOR DE MÉRI DE LA CANORGUE</author>
								<imprint>
									<pubPlace>MARSEILLE</pubPlace>
									<publisher>CAMOIN LIBRAIRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="MDC5">
				<head type="main">A LA FRANCE</head>
				<lg n="1">
					<l n="1" num="1.1">Quel sort sera le tien, ô France bien-aimée !</l>
					<l n="2" num="1.2">Vas-tu servir de proie la horde affamée</l>
					<l n="3" num="1.3">Qui déchire ton sein, s'abreuve de ton sang,</l>
					<l n="4" num="1.4">Et laisse sur ton sol sa souillure, en passant ?</l>
					<l n="5" num="1.5">Ou de Charles-Martel, reprenant la massue,</l>
					<l n="6" num="1.6">Vas-tu broyer encor, comme au jour de Poitiers,</l>
					<l n="7" num="1.7">Une race maudite et dans l'enfer conçue,</l>
					<l n="8" num="1.8">Et convertir ainsi tes cyprès en lauriers ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Quoi ! tu ne réponds rien, ô ma chère mourante,</l>
					<l n="10" num="2.2">Et la terre h tes maux demeure indifférente !</l>
					<l n="11" num="2.3">Hélas ! autour de toi quel silence effrayant :</l>
					<l n="12" num="2.4">Tout tremble, tout se tait et l'abîme est béant !</l>
					<l n="13" num="2.5">Pourtant tu n'es point morte, ô France que j'adore !</l>
					<l n="14" num="2.6">Ton cœur, quoique saignant, ton cœur palpite encore.</l>
					<l n="15" num="2.7">Nul peuple, nul héros ne vient te secourir,</l>
					<l n="16" num="2.8">Et, seule, sans secours te faudra-t-il mourir ?</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Ton barbare ennemi, contre toi plein de rage,</l>
					<l n="18" num="3.2">Te foule sous ses pieds, te dépouille et t'outrage :</l>
					<l n="19" num="3.3">Il a juré ta mort, le poignard sur ton sein,</l>
					<l n="20" num="3.4">Et de roi qu'il était il s'est fait assassin !</l>
					<l n="21" num="3.5">Qui donc viendra panser tes blessures profondes ?</l>
					<l n="22" num="3.6">J'interroge le Ciel, et la terre, et les ondes ;</l>
					<l n="23" num="3.7">A défaut des vivants pour chasser ton bourreau,</l>
					<l n="24" num="3.8">Si j'évoquais soudain les morts de leur tombeau !…</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Vous, guerriers d'autrefois, sortez donc de la tombe,</l>
					<l n="26" num="4.2">Et ne permettez pas que la France succombe !</l>
					<l n="27" num="4.3">Vous, Bayard, Duguesclin, et Lahire, et Dunois,</l>
					<l n="28" num="4.4">Armez-vous du drapeau que portaient nos vieux rois ;</l>
					<l n="29" num="4.5">Jeanne d'Arc et Clisson, accourez aux batailles .</l>
					<l n="30" num="4.6">Et chassez l'ennemi debout sur nos murailles ;</l>
					<l n="31" num="4.7">Vous, Turenne et Condé, Catinat et Villars</l>
					<l n="32" num="4.8">Faites fuir sous vos coups ces bandes de pillards.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Point de Pape Léon, de Geneviève sainte</l>
					<l n="34" num="5.2">Qui de nos murs sacrés sauvegarde l'enceinte,</l>
					<l n="35" num="5.3">Qui fasse reculer ce moderne Attila</l>
					<l n="36" num="5.4">Que Dieu, pour nous punir, de nos jours appela.</l>
					<l n="37" num="5.5">De notre capitale il franchit la barrière,</l>
					<l n="38" num="5.6">Et point de bras vengeur qui le tienne en arrière,</l>
					<l n="39" num="5.7">Qui détourne de nous ce barbare oppresseur</l>
					<l n="40" num="5.8">Qui traîne sur ses pas le carnage et l'horreur !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">Vous, les vaillants, les forts, vous qui vivez encore,</l>
					<l n="42" num="6.2">Vous que la gloire appelle et que l'honneur décore ;</l>
					<l n="43" num="6.3">Et vous, fiers Vendéens, vous, les fils des Croisés,</l>
					<l n="44" num="6.4">Que déjà cent combats ont immortalisés,</l>
					<l n="45" num="6.5">Laisserez-vous ainsi notre vieille patrie</l>
					<l n="46" num="6.6">Pencher vers sa ruine, et s'abimer flétrie,</l>
					<l n="47" num="6.7">Et ne ferez-vous point à ce noble pays</l>
					<l n="48" num="6.8">Quelque nouveau rempart couvert de vos débris ?</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1">Mais vous, mon Dieu, mais vous, laisserez-vous la France</l>
					<l n="50" num="7.2">S'affaisser sous le poids de sa rude souffrance ?</l>
					<l n="51" num="7.3">N'a-t-elle point encore épuisé devant vous,</l>
					<l n="52" num="7.4">Tout son amer calice, et de votre courroux</l>
					<l n="53" num="7.5">Doit-elle ressentir les suites effroyables</l>
					<l n="54" num="7.6">Sans toucher votre cœur ? Ses accents lamentables</l>
					<l n="55" num="7.7">Sont montés jusqu'au Ciel, ne la repoussez pas,</l>
					<l n="56" num="7.8">Ouvrez-lui votre sein et tendez-lui les bras !</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1">Pitié, mon Dieu, pitié ! Seigneur, pardon pour elle !</l>
					<l n="58" num="8.2">Rendez-lui, désormais, une vie immortelle.</l>
					<l n="59" num="8.3">Venez à son secours, vous, maître tout-puissant !</l>
					<l n="60" num="8.4">Elle s'est épurée des torrents de sang,</l>
					<l n="61" num="8.5">Et parmi les combats subissant leur martyre,</l>
					<l n="62" num="8.6">Ses héros invoquaient, dans leur pieux délire,</l>
					<l n="63" num="8.7">Le nom de votre Fils ; de leur mourante voix,</l>
					<l n="64" num="8.8">Ils priaient comme lui, tout en baisant sa croix.</l>
				</lg>
				<lg n="9">
					<l n="65" num="9.1">Et je priais ainsi, lorsque du sein des ombres</l>
					<l n="66" num="9.2">Qui couvraient mon pays, couché sur ses décombres ;</l>
					<l n="67" num="9.3">Le soleil s'élança, brillant et radieux,</l>
					<l n="68" num="9.4">Et j'aperçus là-haut, dans la splendeur des cieux,</l>
					<l n="69" num="9.5">Le Christ qui demandait notre grâce à son Père ;</l>
					<l n="70" num="9.6">Et la paix aussitôt, divine messagère,</l>
					<l n="71" num="9.7">Apparut à nos yeux, ravis de ce bonheur,</l>
					<l n="72" num="9.8">Conduisant par la main notre unique Sauveur.</l>
				</lg>
				<lg n="10">
					<l n="73" num="10.1">La France, sous ses lois, et forte, et rajeunie,</l>
					<l n="74" num="10.2">Reprenait, grâce lui, sa force et son génie ;</l>
					<l n="75" num="10.3">Reniant du passé les fatales erreurs,</l>
					<l n="76" num="10.4">Elle ne livrait plus sa vie aux empereurs,</l>
					<l n="77" num="10.5">A ces hideux Césars qui, montés sur le trône,</l>
					<l n="78" num="10.6">Sur leur front sans pudeur attachaient la couronne,</l>
					<l n="79" num="10.7">Dans la boue et le sang se vautraient à plaisir,</l>
					<l n="80" num="10.8">Et, nous faisant tuer, se gardaient de mourir.</l>
				</lg>
			</div></body></text></TEI>