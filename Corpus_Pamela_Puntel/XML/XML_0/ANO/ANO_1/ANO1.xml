<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LA GRAND MÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>201 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">ANO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LA GRAND MÈRE</title>
						<author>(ANONYME)</author>
					</titleStmt>
					<publicationStmt>
						<publisher/>
						<idno type="URI"/>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<series>
								<title>LA GRAND MÈRE</title>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE ARTISTIQUE</publisher>
									<date when="1871">1871</date>
								</imprint>
								<biblScope unit="tome"/>
							</series>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO1">
				<head type="main">LA GRAND MÈRE</head>
				<lg n="1">
					<l n="1" num="1.1">L'armistice conclu, — front bas et cœur navré,</l>
					<l n="2" num="1.2">Quand il fallut quitter Abbeville livré</l>
					<l n="3" num="1.3">Sans lutte à l'ennemi, — j'eus l'ordre de me rendre,</l>
					<l n="4" num="1.4">Avec mon bataillon, sur la Canche et de prendre</l>
					<l n="5" num="1.5">Dans les hameaux voisins mes quartiers de printemps.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Pauvre était le pays, pauvres les habitants.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">Comme nous arrivions dans un petit village,</l>
					<l n="8" num="3.2">Un de ces nids perdus l'été sous le feuillage,</l>
					<l n="9" num="3.3">— Misérables abris dont l'hiver irrité</l>
					<l n="10" num="3.4">Découvre aux vents du nord l'aride nudité, —</l>
					<l n="11" num="3.5">Sur la route une vieille apparut la première,</l>
					<l n="12" num="3.6">Se trainant avec peine au seuil de sa chaumière.</l>
					<l n="13" num="3.7">Je lui criai de loin : « Hé bien, comment va-t-on,</l>
					<l n="14" num="3.8">La mère ? » — Elle approcha, s'aidant de son bâton,</l>
					<l n="15" num="3.9">Mit doucement sa main sur mon bras sans rien dire</l>
					<l part="I" n="16" num="3.10">Et pleura.</l>
				</lg>
				<lg n="4">
					<l part="F" n="16">Je compris qu'il ne faillait pas rire.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Son corps maigre et chétif tremblait sous ses haillons.</l>
					<l n="18" num="5.2">Ses yeux étaient bordés de rouge. Des sillons,</l>
					<l n="19" num="5.3">Creusés par la douleur encor plus que par l'âge,</l>
					<l n="20" num="5.4">Sans nombre parcouraient sa gorge et son visage.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Comme il venait par là deux braves paysans,</l>
					<l n="22" num="6.2">Ils me dirent : « la veille a quatre-vingt-douze ans.</l>
					<l n="23" num="6.3">» Son petit-fils est mort. Il était militaire</l>
					<l n="24" num="6.4">» Comme vous. Elle avait trois mesures de terre,</l>
					<l n="25" num="6.5">Mais elle a tout perdu par suite d'accidents.</l>
					<l n="26" num="6.6">» C'est qu'on en voit beaucoup, quand on vit trop longtemps !</l>
					<l n="27" num="6.7">» Sitôt qu'elle aperçoit un mobile elle pleure.</l>
					<l n="28" num="6.8">« C'est le chagrin. — Cela va passer tout à l'heure. »</l>
				</lg>
				<lg n="7">
					<l n="29" num="7.1">Honteux comme un soldat qui se sent émouvoir,</l>
					<l n="30" num="7.2">De ce spectacle, en somme, assez pénible à voir,</l>
					<l n="31" num="7.3">Je voulus détourner mes yeux et ma pensée.</l>
					<l part="I" n="32" num="7.4">Je m'éloignai.</l>
				</lg>
				<lg n="8">
					<l part="F" n="32">Mais non. L'image, en vain chassée,</l>
					<l n="33" num="8.1">Reparaissait cruelle et sombre devant moi.</l>
					<l n="34" num="8.2">C'était comme un remords, un reproche… — Pourquoi</l>
					<l n="35" num="8.3">Me sentais-je saisi d'une pitié profonde ?</l>
					<l n="36" num="8.4">La misère ? est-il rien de moins rare en ce monde ?</l>
					<l n="37" num="8.5">La malheur ? on le foule aux pieds à chaque pas !</l>
					<l n="38" num="8.6">Le deuil ? Nous le portons ! et nous n'y songeons pas !</l>
				</lg>
				<lg n="9">
					<l n="39" num="9.1">D'où vient que ce tableau d'une obscure souffrance</l>
					<l n="40" num="9.2">En moi n'ait pas laissé la même indifférence ?</l>
				</lg>
				<lg n="10">
					<l n="41" num="10.1">C'est que chez les vieillards immense est la douleur.</l>
					<l n="42" num="10.2">Il n'en est pas qui soit comparable à la leur.</l>
				</lg>
				<lg n="11">
					<l n="43" num="11.1">Pour nos autres, enfants, le chagrin n'est qu'une ombre</l>
					<l n="44" num="11.2">Qu'un rayon d'espérance efface ou fait moins sombre,</l>
					<l n="45" num="11.3">Mais, sur le front des vieux, l'ombre s'ajoute au noir,</l>
					<l n="46" num="11.4">Avec notre misère ils ont leur désespoir,</l>
					<l n="47" num="11.5">Hébreux à qui le ciel ne verse plus sa manne.</l>
				</lg>
				<lg n="12">
					<l n="48" num="12.1">Ma foi ! le lendemain j'étais dans la cabane.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">— Je me souviens qu'avant d'entrer, comme un amant</l>
					<l n="50" num="13.2">Craintif, j'avais poussé la porte… doucement…</l>
					<l n="51" num="13.3">La vieille était assise à l'angle obscur de l'âtre.</l>
					<l n="52" num="13.4">La flamme, en s'endormant sous la tourbe grisâtre,</l>
					<l n="53" num="13.5">Lui jetait ces lueurs indécises qui font</l>
					<l n="54" num="13.6">Plus pâle le visage avec l'œil plus profond.</l>
				</lg>
				<lg n="14">
					<l n="55" num="14.1">Et je regardais, vers la cendre penchée,</l>
					<l n="56" num="14.2">Attisant son feu mort de sa main desséchée.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Tout un siècle était là. — Quel destin ! Quel séjour !</l>
					<l n="58" num="15.2">Elle me reconnut et murmura : bonjour.</l>
				</lg>
				<lg n="16">
					<l n="59" num="16.1">Je m'assis sur la chaise enfumée et brunie,</l>
					<l n="60" num="16.2">— Car on n'exige pas plus de cérémonie</l>
					<l n="61" num="16.3">Au village. — Elle dit : « Vous ne voudriez pas</l>
					<l n="62" num="16.4">Boire un verre de cidre ? » — Elle ajouta plus bas,</l>
					<l n="63" num="16.5">En soulevant sa main : « Quel bonheur vous me faites !</l>
					<l n="64" num="16.6">» Quelle guerre, mon Dieu !… Sans doute que vous êtes</l>
					<l n="65" num="16.7">» Un capitaine, un chef… Votre habit est plus beau…</l>
					<l n="66" num="16.8">» Et puis c'est plus doré dessus votre chapeau…</l>
					<l n="67" num="16.9">» Il faudra punir Jean ! — C'est drôle tout de même</l>
					<l part="I" n="68" num="16.10">» Qu'il ne vient point diner. »</l>
				</lg>
				<lg n="17">
					<l part="F" n="68">J'avais un mal extrême,</l>
					<l n="69" num="17.1">— Car la vieille était sourde — à suivre l'entretien.</l>
					<l part="I" n="70" num="17.2">— Qu'est-ce que Jean ? lui dis-je.—</l>
				</lg>
				<lg n="18">
					<l part="F" n="70">« Hé ! vous le savez bien,</l>
					<l n="71" num="18.1">« C'est un de vos soldats ! C'est ici qu'il demeure.</l>
					<l n="72" num="18.2">» Ah ! je le soigne bien, allez ! Mais voici l'heure</l>
					<l n="73" num="18.3">» Encore bien passée… il ne vient pas manger ! »</l>
					<l n="74" num="18.4">— « Quoi ! le maire vous donne un mobile à loger ? »</l>
				</lg>
				<lg n="19">
					<l n="75" num="19.1">— « Oui, oui, je l'ai voulu. Tenez, voilà sa chambre…</l>
					<l n="76" num="19.2">» Celle de mon p'tit fieu… mort au mois de décembre…</l>
					<l n="77" num="19.3">» C'est cette peine-là qui me fera périr !… »</l>
					<l n="78" num="19.4">Et sa voix se brisa ne pouvant s'attendrir.</l>
				</lg>
				<lg n="20">
					<l n="79" num="20.1">Et puis, — geste navrant ! — de ses deux mains flétries,</l>
					<l n="80" num="20.2">Longtemps elle pressa ses paupières taries…</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">A quatre-vingt-douze ans les pleurs ne coulent plus.</l>
				</lg>
				<lg n="22">
					<l n="82" num="22.1">— « Ah ! que Dieu le reçoive au sein de ses élus ! »</l>
					<l n="83" num="22.2">Reprit-elle, en laissant sa tête résignée</l>
					<l n="84" num="22.3">Tomber comme un rameau qu'a touché la cognée.</l>
				</lg>
				<lg n="23">
					<l n="85" num="23.1">O misère ! Ô leçon ! Ainsi, non-seulement</l>
					<l n="86" num="23.2">Cette veuve au passant donnait son logement,</l>
					<l n="87" num="23.3">Elle donnait son pain — sans qu'elle y fût forcée.</l>
					<l n="88" num="23.4">Je voulus le défendre, elle parut blessée :</l>
					<l n="89" num="23.5">— « Quand on loge quelqu'un il faut bien le nourrir,</l>
					<l n="90" num="23.6">» Ça se doit, voyez-vous, et puis ça fait plaisir. »</l>
				</lg>
				<lg n="24">
					<l n="91" num="24.1">— « Mais vous n'êtes pas riche et cette charge est telle… »</l>
				</lg>
				<lg n="25">
					<l n="92" num="25.1">— « Je ne demande rien à personne, dit-elle,</l>
					<l n="93" num="25.2">» Pour le peu qu'il m'en faut, j'irai bien jusqu'au bout !</l>
					<l n="94" num="25.3">» Ne me reprenez pas mon mobile surtout !</l>
					<l n="95" num="25.4">» Médor couche avec lui, croyant que c'est Baptiste… »</l>
					<l n="96" num="25.5">— Elle sourit ! Hélas ! Sourire encore plus triste</l>
					<l part="I" n="97" num="25.6">Et navrant que ses pleurs !</l>
				</lg>
				<lg n="26">
					<l part="F" n="97">Elle reprit alors :</l>
					<l n="98" num="26.1">« Est-ce drôle que Jean soit si longtemps dehors ?</l>
				</lg>
				<lg n="27">
					<l n="99" num="27.1">— » Mais ne l'attendez pas ! je l'exige ! à votre âge,</l>
					<l part="I" n="100" num="27.2">» Attendre c'est mauvais.</l>
				</lg>
				<lg n="28">
					<l part="F" n="100">Non, ce serait dommage !</l>
					<l n="101" num="28.1">» J'ai du si bon bouillon ! » — J'aperçus en effet</l>
					<l n="102" num="28.2">Quelque chose à mes pieds comme un pot qui chauffait.</l>
				</lg>
				<lg n="29">
					<l n="103" num="29.1">— « C'est de la bonne viande et des pommes de terre.</l>
					<l n="104" num="29.2">» Du bouillon, pour le corps, c'est sain, c'est salutaire !</l>
					<l n="105" num="29.3">» Et puis j'ai fait un bon petit café… pour lui !</l>
					<l n="106" num="29.4">» Si Baptiste était là quelle fête aujourd'hui !…</l>
					<l n="107" num="29.5">» Vous, sans doute que c'est au château que vous êtes ?</l>
					<l n="108" num="29.6">» Il fait beau là-dedans ! et c'est tous les jours fêtes !</l>
					<l n="109" num="29.7">» Bien sûr qu'on vous y donne aussi tous vos repas ? »</l>
				</lg>
				<lg n="30">
					<l n="110" num="30.1">— « Non, je loge au château mais je n'y dine pas. »</l>
				</lg>
				<lg n="31">
					<l n="111" num="31.1">Soudain le chien se mit à japper. — Sur la route</l>
					<l n="112" num="31.2">Un pas se fit entendre. — « Ah ! le voilà sans doute !</l>
					<l n="113" num="31.3">» Surtout ne dites rien… Ne soyez pas méchant ! »</l>
				</lg>
				<lg n="32">
					<l n="114" num="32.1">La porte enfin s'ouvrit. C'était donc Monsieur Jean !</l>
				</lg>
				<lg n="33">
					<l n="115" num="33.1">Devant le bol fumant de faïence fossile,</l>
					<l n="116" num="33.2">Il s'assit tout honteux comme un grand imbécile,</l>
					<l n="117" num="33.3">Sans songer seulement à nettoyer son plat.</l>
					<l n="118" num="33.4">Tandis qu'il s'attablait, tout seul, comme un prélat,</l>
					<l n="119" num="33.5">Sous le nez patient de Médor à la diète,</l>
					<l n="120" num="33.6">Sur ses genoux, l'aïeule ajustait son assiette</l>
					<l n="121" num="33.7">Et, dans l'ombre, faisait le signe de la croix.</l>
				</lg>
				<lg n="34">
					<l part="I" n="122" num="34.1">Il faut croire à l'attrait du malheur.</l>
					<l part="F" n="122" num="34.1">Bien de fois,</l>
					<l n="123" num="34.2">Et même chaque jour, — étant célibataire</l>
					<l n="124" num="34.3">Je ne vois pas pourquoi j'en ferais un mystère —</l>
					<l n="125" num="34.4">J'allai faire ma cour à ma vieille Suzon.</l>
					<l n="126" num="34.5">Pour moi, tous les chemins menaient à sa maison.</l>
				</lg>
				<lg n="35">
					<l n="127" num="35.1">Comme d'une façon galante et généreuse</l>
					<l n="128" num="35.2">Il convient de mener une intrigue amoureuse,</l>
					<l n="129" num="35.3">Du cabaret voisin je faisais apporter</l>
					<l n="130" num="35.4">Les petits douceurs qui pouvaient la tenter…</l>
					<l n="131" num="35.5">Tant et si bien qu'enfin, dépouillant toute honte,</l>
					<l ana="incomplete" where="I" n="132" num="35.6">tous, les soirs, à mon compte,</l>
					<l ana="unanalyzable" n="133" num="35.7">Nous prenions du</l>
					<l ana="unanalyzable" n="134" num="35.8">Chez elle. — Et le hameau de faire</l>
					<l rhyme="none" n="135" num="35.9">Des étrangers amours du chef de bataillon.</l>
					<l n="136" num="35.10">Heureusement que Jean, se trouvant de la fête,</l>
					<l n="137" num="35.11">De tout propos malin sauvait nos tête-à-tête.</l>
				</lg>
				<lg n="36">
					<l n="138" num="36.1">Hélas ! ces rendez-vous étaient souvent troublés</l>
					<l n="139" num="36.2">Par d'amers souvenirs, sans cesse rappelés !</l>
					<l n="140" num="36.3">Tantôt, d'un vieux panier, reliquaire fidèle</l>
					<l n="141" num="36.4">Où ses pauvres trésors dormaient à côté d'elle,</l>
					<l n="142" num="36.5">On tirait un papier mille fois déplié,</l>
					<l n="143" num="36.6">Renfermant le portrait par Baptiste envoyé.</l>
					<l n="144" num="36.7">Et l'aïeule baisait cette image pâlie</l>
					<l n="145" num="36.8">Que ne distinguait plus une prunelle affaiblie.</l>
				</lg>
				<lg n="37">
					<l n="146" num="37.1">Tantôt elle exhumait le message brutal</l>
					<l n="147" num="37.2">Disant : Baptiste est mort… mort dans un hôpital !</l>
					<l n="148" num="37.3">— Caprice respecté de la douleur qui dure,</l>
					<l n="149" num="37.4">Il me fallait vingt fois en faire la lecture</l>
					<l n="150" num="37.5">Et vingt fois m'arrêter aux détails déchirants</l>
					<l n="151" num="37.6">Que ramène toujours l'histoire des mourants.</l>
				</lg>
				<lg n="38">
					<l n="152" num="38.1">Et puis, venaient encore maints récits sur l'enfance</l>
					<l n="153" num="38.2">De Baptiste, récits que l'on connait d'avance</l>
					<l n="154" num="38.3">Quand à la mère en deuil on parle de son fils !</l>
					<l n="155" num="38.4">Il était brave et doux, docile aux bons avis,</l>
					<l n="156" num="38.5">Auprès d'elle le soir il faisait sa prière…</l>
				</lg>
				<lg n="39">
					<l n="157" num="39.1">— Mais Dieu la punissait pour avoir été fière !</l>
					<l n="158" num="39.2">Et sa voix murmurait : « pardon ! pardon ! pardon ! »</l>
				</lg>
				<lg n="40">
					<l n="159" num="40.1">Pour la centième fois, me montrant son bâton,</l>
					<l n="160" num="40.2">Elle me répétait : « Vous voyez cette branche,</l>
					<l n="161" num="40.3">» C'est du noyer. C'est lui qui l'a faite un dimanche</l>
					<l n="162" num="40.4">» Pour moi… c'est le dernier cadeau de mon p'tit fieu ! »</l>
					<l n="163" num="40.5">Et, comprimant son cœur, elle disait : «Mon Dieu,</l>
					<l n="164" num="40.6">» Que votre volonté soit faite sur la terre</l>
					<l part="I" n="165" num="40.7">» Comme au ciel ! »</l>
				</lg>
				<lg n="41">
					<l part="F" n="165">D'autre fois l'aïeule solitaire,</l>
					<l n="166" num="41.1">Pour me remercier de savoir l'écouter,</l>
					<l n="167" num="41.2">Me faisait des présents qu'il fallait accepter.</l>
					<l n="168" num="41.3">— On ne refuse pas l'humble offrande craintive. —</l>
					<l n="169" num="41.4">Elle me présentait quelque pomme chétive,</l>
					<l n="170" num="41.5">Conservée avec soin, c'était le dernier fruit</l>
					<l n="171" num="41.6">Que le pommier planté par baptiste eût produit.</l>
				</lg>
				<lg n="42">
					<l n="172" num="42.1">Toujours quelque surprise, à loisir préparée,</l>
					<l n="173" num="42.2">Attendait ma venue et fêtait mon entrée :</l>
					<l n="174" num="42.3">Une petite croix, quatre pieds de tabac,</l>
					<l n="175" num="42.4">Pour le soldat gardés au retour du combat ;</l>
					<l n="176" num="42.5">Un méchant petit sac de noisettes trouvées</l>
					<l n="177" num="42.6">Par elle dans la haie et pour lui conservées !</l>
				</lg>
				<lg n="43">
					<l part="I" n="178" num="43.1">O générosité du pauvre !</l>
				</lg>
				<lg n="44">
					<l part="F" n="178">A tout roman,</l>
					<l n="179" num="44.1">Triste ou gai, faux ou vrai, faut-il dénouement ?</l>
					<l n="180" num="44.2">La paix vint. Cette paix que la mère eut bénie,</l>
					<l n="181" num="44.3">Mais que son deuil changeait en cruelle ironie !</l>
				</lg>
				<lg n="45">
					<l n="182" num="45.1">L'ordre étant venu de quitter le vallon,</l>
					<l n="183" num="45.2">On mit le sac au dos, l'adieu ne fut pas long.</l>
				</lg>
				<lg n="46">
					<l n="184" num="46.1">La vieille est là, debout, chancelante, débile.</l>
					<l n="185" num="46.2">Elle cherche des yeux le chef… puis le mobile,</l>
					<l n="186" num="46.3">Adressant au hasard des signes de la main…</l>
					<l part="I" n="187" num="46.4">Le bataillon passa.</l>
				</lg>
				<lg n="47">
					<l part="F" n="187">Sur le bord du chemin,</l>
					<l n="188" num="47.1">Comme à notre arrivée elle était la première,</l>
					<l n="189" num="47.2">Je la vis au départ demeurer la dernière,</l>
					<l n="190" num="47.3">Et, quand tout se voila pour ses regards éteints,</l>
					<l n="191" num="47.4">Chercher encor l'écho de nos tambours lointains.</l>
				</lg>
				<lg n="48">
					<l n="192" num="48.1">Oh ! la pauvre chaumière isolée et déserte !</l>
					<l n="193" num="48.2">Bien souvent, je revois, par la porte entrouverte,</l>
					<l n="194" num="48.3">La malheureuse vieille à son morne foyer</l>
					<l n="195" num="48.4">Et tenant dans sa main ce bâton de noyer…</l>
					<l part="I" n="196" num="48.5">Légué par son enfant !</l>
				</lg>
				<lg n="49">
					<l part="F" n="196">Alors, moi qui naguère</l>
					<l n="197" num="49.1">M'enivrant dans mon cœur des fureurs de la guerre,</l>
					<l n="198" num="49.2">Adressais à la mort tant de lâches défis,</l>
					<l n="199" num="49.3">Ah ! je songe à ma mère un instant oubliée,</l>
					<l n="200" num="49.4">Et je rends grâce à Dieu, d'une âme humiliée,</l>
					<l n="201" num="49.5">De l'avoir épargnée en lui gardant son fils.</l>
				</lg>
				<ab type="star">◇ FIN</ab>
				<closer>
					<dateline>
						<date when="1871">Hesmond, mars 1871</date>
					</dateline>
				</closer>
			</div></body></text></TEI>