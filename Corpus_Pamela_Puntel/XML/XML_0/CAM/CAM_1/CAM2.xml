<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM2">
				<head type="main">HYMNE RELIGIEUX SUR LE TRAVAIL</head>
				<lg n="1">
					<l n="1" num="1.1">Dieu rend à nos labeurs les miracles faciles.</l>
					<l n="2" num="1.2">La matière partout cède à nos volontés.</l>
					<l n="3" num="1.3">Comme de fiers coursiers à nos rênes dociles,</l>
					<l n="4" num="1.4">Les fougueux éléments frémissent, mais domptés.</l>
					<l n="5" num="1.5">Nos mains de la nature ont déchiré les voiles,</l>
					<l n="6" num="1.6">Et l’âme, mer profonde, a livré ses secrets ;</l>
					<l n="7" num="1.7">Nos regards dans l’espace ont suivi les étoiles ;</l>
					<l n="8" num="1.8">Du Très-Haut dans l’histoire ils lisent les décrets.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Travail, force vivante et libre,</l>
					<l n="10" num="2.2">Tu révèles l’Esprit humain.</l>
					<l n="11" num="2.3">C’est toi qui, sur les bords du Tibre,</l>
					<l n="12" num="2.4">Enfantas le peuple romain.</l>
					<l n="13" num="2.5">Par toi, près des mers d’Ionie,</l>
					<l n="14" num="2.6">Naquirent ces fleurs du génie,</l>
					<l n="15" num="2.7">L’Iliade et le Parthénon.</l>
					<l n="16" num="2.8">Héritière de l’ancien monde,</l>
					<l n="17" num="2.9">Par toi, par ton ardeur féconde,</l>
					<l n="18" num="2.10">La France a conquis son grand nom.</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1">A l’œuvre, enfants ! L’œuvre est divine.</l>
					<l n="20" num="3.2">Les lâches seuls veulent s’asseoir.</l>
					<l n="21" num="3.3">L’œil ne voit pas, le cœur devine :</l>
					<l n="22" num="3.4">Il pressent l’aube dans le soir.</l>
					<l n="23" num="3.5">Dans un cercle affreux enfermée,</l>
					<l n="24" num="3.6">Bientôt notre patrie aimée</l>
					<l n="25" num="3.7">Le rompra d’un puissant essor.</l>
					<l n="26" num="3.8">Les débris dont elle est couverte</l>
					<l n="27" num="3.9">Porteront une forêt verte</l>
					<l n="28" num="3.10">Où vous cueillerez des fruits d’or.</l>
				</lg>
				<lg n="4">
					<l n="29" num="4.1">Fils du Maître des cieux, imitez votre Père.</l>
					<l n="30" num="4.2">Il accomplit sans trêve un infini dessein ;</l>
					<l n="31" num="4.3">Sur l’immense étendue où se meut chaque sphère,</l>
					<l n="32" num="4.4">Il verse les trésors échappés de son sein.</l>
					<l n="33" num="4.5">La beauté, l’harmonie ont en lui leur principe ;</l>
					<l n="34" num="4.6">Par les chœurs de soleils son nom est attesté.</l>
					<l n="35" num="4.7">Qu’à l’ordre universel votre âme participe ;</l>
					<l n="36" num="4.8">Sa double aile est l’amour avec la liberté.</l>
				</lg>
				<lg n="5">
					<l n="37" num="5.1">Que Dieu soit votre unique règle.</l>
					<l n="38" num="5.2">Montez jusqu’aux cimes d’azur,</l>
					<l n="39" num="5.3">D’où s’élevant plus haut que l’aigle,</l>
					<l n="40" num="5.4">L’Idéal plane d’un vol sûr.</l>
					<l n="41" num="5.5">Aspirez à ce qui fait l’homme,</l>
					<l n="42" num="5.6">Non au vain éclat que renomme</l>
					<l n="43" num="5.7">Le vil esclave de ses sens.</l>
					<l n="44" num="5.8">Dans l’horreur de nos catastrophes,</l>
					<l n="45" num="5.9">Des poètes, des philosophes</l>
					<l n="46" num="5.10">Méditez les nobles accents.</l>
				</lg>
				<lg n="6">
					<l n="47" num="6.1">La Justice est la loi première,</l>
					<l n="48" num="6.2">Qu’elle brille sur vos travaux.</l>
					<l n="49" num="6.3">Tous les peuples à sa lumière</l>
					<l n="50" num="6.4">Marcheront dans les temps nouveaux.</l>
					<l n="51" num="6.5">Gardez-en vous sa sainte image.</l>
					<l n="52" num="6.6">Qu’elle ne soit d’aucun nuage</l>
					<l n="53" num="6.7">Voilée à vos jeunes esprits.</l>
					<l n="54" num="6.8">Guerre, enfants, aux instincts rebelles !</l>
					<l n="55" num="6.9">Luttez : Les victoires sont belles.</l>
					<l n="56" num="6.10">Vertus et gloire en sont le prix.</l>
				</lg>
			</div></body></text></TEI>