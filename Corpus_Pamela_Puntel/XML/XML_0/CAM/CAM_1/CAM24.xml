<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM24">
				<head type="main">L’ARCHEVÊQUE ROGER ET LE ROI GUILLAUME</head>
				<head type="main">OU LA TOUR DE LA FAIM</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Ministre du Dieu bon, mais ministre parjure,</l>
						<l n="2" num="1.2">Prêtre infidèle au crucifix,</l>
						<l n="3" num="1.3">Roger Ubaldini, pour venger son injure,</l>
						<l n="4" num="1.4">Saisit Ugolin et ses fils,</l>
						<l n="5" num="1.5">Dans une épaisse tour, sous une bonne escorte,</l>
						<l n="6" num="1.6">Les traîna rudement bouclés,</l>
						<l n="7" num="1.7">Et du simple donjon ayant cloué la porte,</l>
						<l n="8" num="1.8">Dans le fleuve on jeta les clés.</l>
						<l n="9" num="1.9">Que fut le dénoûment de ce lugubre drame ?</l>
						<l n="10" num="1.10">Des prisonniers quel fut le sort ?</l>
						<l n="11" num="1.11">Le barde florentin le dévoile, et notre âme</l>
						<l n="12" num="1.12">En ressent un frisson de mort.</l>
						<l n="13" num="1.13">Ineffables douleurs ! Les enfants disaient : «Père,</l>
						<l n="14" num="1.14">Tu nous as vêtus de ces chairs.</l>
						<l n="15" num="1.15">Mange-les. Tu le peux. « Et lui se désespère</l>
						<l n="16" num="1.16">De voir souffrir ces fils si chers.</l>
						<l n="17" num="1.17">Longtemps quand gémissait ou le vent ou l’orfraie ;</l>
						<l n="18" num="1.18">On croit ouïr : «du pain… Je meurs»</l>
						<l n="19" num="1.19">Et longtemps Mise, aux bords de l’Arno qui s’effraie,</l>
						<l n="20" num="1.20">Recueillit ces tristes clameurs.</l>
						<l n="21" num="1.21">Dante dit qu’aux enfers, accroupi dans la glace,</l>
						<l n="22" num="1.22">Ugolin brise de Roger</l>
						<l n="23" num="1.23">Le crâne sous ses dents, et jamais ne se lasse</l>
						<l n="24" num="1.24">De le mordre et de le ronger.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="25" num="1.1">Ubaldini teuton, pieuse bête fauve,</l>
						<l n="26" num="1.2">O Guillaume, Tartufe-roi,</l>
						<l n="27" num="1.3">Oses-tu l’invoquer le rédempteur, qui sauve,</l>
						<l n="28" num="1.4">Toi qui foules aux pieds sa loi ?</l>
						<l n="29" num="1.5">Oses-tu bien prier que le ciel illumine</l>
						<l n="30" num="1.6">Ton Augusta, reine, et les tiens,</l>
						<l n="31" num="1.7">Toi qui veux, dans Paris, tuer par la famine</l>
						<l n="32" num="1.8">Ses deux millions de chrétiens ?</l>
						<l n="33" num="1.9">Ta condamnation dans les cieux est écrite.</l>
						<l n="34" num="1.10">Du Christ tu déchires le corps.</l>
						<l n="35" num="1.11">Tu prends des bains de sang, ô vieillard hypocrite,</l>
						<l n="36" num="1.12">Est tu convoites nos trésors.</l>
						<l n="37" num="1.13">Noël ! Noël ! Voici le nouveau Charlemagne !</l>
						<l n="38" num="1.14">Meurtre, rapine, embrasement,</l>
						<l n="39" num="1.15">C’est ce qui le devance, et ce qui l’accompagne :</l>
						<l n="40" num="1.16">Dons du joyeux avènement !</l>
						<l n="41" num="1.17">Tes canons ont choisi pour leurs points de repère</l>
						<l n="42" num="1.18">Nos monuments hospitaliers.</l>
						<l n="43" num="1.19">Ton palais d’empereur devrait être un repaire</l>
						<l n="44" num="1.20">Dans les plus sauvages halliers.</l>
						<l n="45" num="1.21">Quoi ! la grande cité, dans tout son périmètre,</l>
						<l n="46" num="1.22">Serait une tour de la faim !</l>
						<l n="47" num="1.23">Son vaillant peuple, ô doux vainqueur, gracieux maître,</l>
						<l n="48" num="1.24">N’aurait que tes obus pour pain !</l>
						<l n="49" num="1.25">Ah ! puisses-tu périr dans nos plaintes suantes</l>
						<l n="50" num="1.26">Du sang des Germains égorgés !</l>
						<l n="51" num="1.27">Par nos loups, nos vautours, puissent tes chairs puantes</l>
						<l n="52" num="1.28">Et ton vieux crâne être rongés !</l>
					</lg>
				</div>
			</div></body></text></TEI>