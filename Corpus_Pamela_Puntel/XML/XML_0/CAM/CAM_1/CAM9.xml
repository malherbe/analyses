<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM9">
				<head type="main">LA STATUE DE KLEBER</head>
				<lg n="1">
					<l n="1" num="1.1">La nuit règne ; elle est sans étoiles.</l>
					<l n="2" num="1.2">Les bombes se suivent dans l’air,</l>
					<l n="3" num="1.3">Et, jetant un sinistre éclair,</l>
					<l n="4" num="1.4">De l’ombre déchirent les voiles.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Contre nous valeureux soldats</l>
					<l n="6" num="2.2">La trombe de fer est hurlante.</l>
					<l n="7" num="2.3">La Cathédrale chancelante</l>
					<l n="8" num="2.4">Sonne d’elle-même les glas.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">La mort prend d’effrayantes formes ;</l>
					<l n="10" num="3.2">Femmes, enfants sont écrasés ;</l>
					<l n="11" num="3.3">Les édifices embrasés</l>
					<l n="12" num="3.4">S’écroulent en débris énormes.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ton cœur dans le bronze, ô Kléber,</l>
					<l n="14" num="4.2">Tressaille, ton regard s’effare</l>
					<l n="15" num="4.3">Devant ce spectacle barbare,</l>
					<l n="16" num="4.4">Devant ces horreurs de l’enfer.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Tu rêvais, ô héros du Caire,</l>
					<l n="18" num="5.2">Que le vertige de l’effroi</l>
					<l n="19" num="5.3">Courbait, frissonnants, devant toi</l>
					<l n="20" num="5.4">Et Mameluck et Janissaire.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Voilà que, sur ton piédestal</l>
					<l n="22" num="6.2">La chute de Strasbourg t’éveille.</l>
					<l n="23" num="6.3">Ce cri de douleur, ô merveille !</l>
					<l n="24" num="6.4">Sort de tes lèvres de métal :</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">« Que vois-je ? Quoi ! Strasbourg est une immense tombe.</l>
					<l n="26" num="7.2">Je songeais au passé sous mon masque d’airain.</l>
					<l n="27" num="7.3">Mes songes glorieux coulaient comme le Rhin.</l>
					<l n="28" num="7.4">Ils s’effacent au bruit de ma cité qui tombe.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">« Qu’êtes-vous devenus, beaux jours évanouis,</l>
					<l n="30" num="8.2">Quand, ouvrant l’avenir, la Jeune République,</l>
					<l n="31" num="8.3">Des rivages d’Europe à la terre biblique,</l>
					<l n="32" num="8.4">Attirait les regards des peuples éblouis ?</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">« Comme l’astre émergeant des abîmes de l’onde ;</l>
					<l n="34" num="9.2">Elle jetait au loin d’immortelles clartés.</l>
					<l n="35" num="9.3">Les nuages fuyaient de nos cieux écartés,</l>
					<l n="36" num="9.4">Et sa lumière était l’espérance du monde.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">« Les despises émus tombaient à nos genoux.</l>
					<l n="38" num="10.2">Nous étions élevés tous à la même école :</l>
					<l n="39" num="10.3">Hoche, Marceau, Desaix, et le vainqueurs d’Arcole ;</l>
					<l n="40" num="10.4">Qui depuis… Mais alors il n’était qu’un de nous.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">« Nos fronts se couronnaient de gloire plébéienne,</l>
					<l n="42" num="11.2">Fils du peuple, sans pain, revêtus de haillons,</l>
					<l n="43" num="11.3">Nos soldats repoussaient les nombreux bataillons</l>
					<l n="44" num="11.4">Que déchaînaient les rois de Berlin et de Vienne.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">« La France aux nations disait : Nous sommes sœurs ;</l>
					<l n="46" num="12.2">Sa foudre proclamait les droits sacrés de l’homme.</l>
					<l n="47" num="12.3">Porté des bords du Rhin jusqu’aux échos de Rome,</l>
					<l n="48" num="12.4">Son hymne de combat troublait les oppresseurs.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">« Et maintenant, Français, qu’est-ce qu’on vous demande ?</l>
					<l n="50" num="13.2">Mon Alsace adorée… Ah ! répondez-leur : Non.</l>
					<l n="51" num="13.3">Répondez par le glaive et la voix du canon,</l>
					<l n="52" num="13.4">Et brisez sous vos pieds cette audace Allemande.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">« Des vainqueurs de Fleurus et d’heliopolis</l>
					<l n="54" num="14.2">Enfants, vous châtierez, tant de sanglants scandales ;</l>
					<l n="55" num="14.3">Si les envahisseurs sont de cruels vandales,</l>
					<l n="56" num="14.4">Vous, vous ne serez pas de Romains amollis.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">« La sainte Liberté pour vous se lève encore,</l>
					<l n="58" num="15.2">Et sa voix vous rappelle aux antiques vertus ;</l>
					<l n="59" num="15.3">Son souffle a ranimé tous les cœurs abattus ;</l>
					<l n="60" num="15.4">Il rendra la victoire au drapeau tricolore.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">« O ma chère Patrie, ô France, ô Niobé !</l>
					<l n="62" num="16.2">Tu ceindras de nouveau ton brillant diadème,</l>
					<l n="63" num="16.3">En vain tes insultes te lancent l’anathème ;</l>
					<l n="64" num="16.4">Le sceptre de tes mains n’est pas encore tombé.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">« Et toi, sur des débris assise, ô cité veuve,</l>
					<l n="66" num="17.2">O ma mère, Strasbourg, tes malheurs finiront.</l>
					<l n="67" num="17.3">Tu te relèveras, gardienne du grand fleuve,</l>
					<l n="68" num="17.4">Et le sang des germains lavera ton affront.»</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">De cet héroïque prophète</l>
					<l n="70" num="18.2">La parole aux cieux se perdit ;</l>
					<l n="71" num="18.3">Mais plus d’un soldat l’entendit :</l>
					<l n="72" num="18.4">Mon humble lyre la répète.</l>
				</lg>
			</div></body></text></TEI>