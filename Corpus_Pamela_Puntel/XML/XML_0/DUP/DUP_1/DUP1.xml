<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">SEDAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DUP">
					<name>
						<forename>Gustave</forename>
						<surname>DUPIN</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>132 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DUP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>SEDAN</title>
						<author>GUSTAVE DUPIN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5829622x.r=DUPIN%20SEDAN?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>SEDAN</title>
								<author>GUSTAVE DUPIN</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUP1">
				<head type="main">SEDAN</head>
				<lg n="1">
					<l n="1" num="1.1">Le sang à ce nom seul, se fige dans les veines,</l>
					<l n="2" num="1.2">Par un rêve hideux l'on se croit tourmenté,</l>
					<l n="3" num="1.3">Il passe sur le front de fétides haleines,</l>
					<l n="4" num="1.4">Comme devant l'abîme on est épouvanté.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Puis, quand la stupeur cesse, et que l'on se réveille,</l>
					<l n="6" num="2.2">Croyant avoir chassé la sombre vision,</l>
					<l n="7" num="2.3">Le spectre est encor là qui bourdonne à l'oreille</l>
					<l n="8" num="2.4">Le mot, l'horrible mot capitulation !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Et quand de ce marché, quand de ce pacte infâme</l>
					<l n="10" num="3.2">Vous pénétrez enfin le secret ténébreux,</l>
					<l n="11" num="3.3">Un cri soudain alors s'échappe de votre âme,</l>
					<l n="12" num="3.4">Cri de haine et de rage, effrayant, douloureux.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Car Sedan, c'est bien moins le revers que la honte ;</l>
					<l n="14" num="4.2">C'est la boue et le sang mêlés dans un seul lot ;</l>
					<l n="15" num="4.3">C'est l'odieux calcul c'est le traître qui compte</l>
					<l n="16" num="4.4">Ce que doit lui valoir son atroce complot.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Sedan, au dernier jour, est une cuve immense</l>
					<l n="18" num="5.2">Où soldats et chevaux, armes, caissons, plomb, fer,</l>
					<l n="19" num="5.3">Mêlés, broyés, fondus par un bras en démence,</l>
					<l n="20" num="5.4">Se tordent convulsifs comme dans un enfer.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">C'est la tombe où la France, éperdue et trahie,</l>
					<l n="22" num="6.2">Le front ceint de cyprès à défaut de lauriers,</l>
					<l n="23" num="6.3">Victime lamentable, avant d'être envahie,</l>
					<l n="24" num="6.4">S'ensevelit vivante avec tous ses guerriers.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Ah ! de quels désespoirs et de combien de larmes</l>
					<l n="26" num="7.2">Ils l'ont accompagné leur holocauste affreux,</l>
					<l n="27" num="7.3">Ces fiers soldats contraints de déposer leurs armes</l>
					<l n="28" num="7.4">Sans combattre, et combien ils furent malheureux !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Je le sais, et devant cet amer sacrifice,</l>
					<l n="30" num="8.2">Qu'une rigide loi les forçait de subir,</l>
					<l n="31" num="8.3">Je ne recherche point, accusateur d'office,</l>
					<l n="32" num="8.4">S'ils pouvaient justement refuser d'obéir.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Tous n'acceptèrent pas l'aveugle suicide ;</l>
					<l n="34" num="9.2">Plus d'an sut résister : honneur à qui l'osa !</l>
					<l n="35" num="9.3">Mais j'en veux au cruel, au lâche parricide</l>
					<l n="36" num="9.4">Qui signa le traité de honte et l'imposa.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Il pouvait se donner d'illustres funérailles ;</l>
					<l n="38" num="10.2">Il pouvait, par un mâle et glorieux effort,</l>
					<l n="39" num="10.3">S'élancer à travers ces épaisses murailles '</l>
					<l n="40" num="10.4">D'ennemis l'enserrant dans un cercle de mort.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Ce glaive encor souillé du meurtre de Décembre,</l>
					<l n="42" num="11.2">Ce glaive humide encor d'un si généreux sang,</l>
					<l n="43" num="11.3">Par le sang du Teuton, par celui du Sicambre,</l>
					<l n="44" num="11.4">Purifié, l'aurait rendu presque innocent.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Et s'il ne brisait point la formidable enceinte,</l>
					<l n="46" num="12.2">S'il tombait fracassé par le terrible écueil,</l>
					<l n="47" num="12.3">Il tombait revêtu de la majesté sainte</l>
					<l n="48" num="12.4">D'un empire croulant sous la patrie en deuil.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Mais quand autour de lui, prêts à vomir la foudre,</l>
					<l n="50" num="13.2">Il vit tant de canons braqués de toutes parts,</l>
					<l n="51" num="13.3">Il oublia qu'il lui restait assez de poudre</l>
					<l n="52" num="13.4">Pour se faire un tombeau de ses frêles remparts ;</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Mais il ne se souvint qu'il portait une épée</l>
					<l n="54" num="14.2">Que pour la retirer humblement du fourreau</l>
					<l n="55" num="14.3">Et la rendre, cette arme aux combats mal trempée,</l>
					<l n="56" num="14.4">Et qui ne fut jamais que l'arme d'un bourreau.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Ah ! je reconnais bien l'éternelle justice !</l>
					<l n="58" num="15.2">Il était dit là-haut que, par un beau trépas,</l>
					<l n="59" num="15.3">L'homme des coups d'État, ce héros tout factice,</l>
					<l n="60" num="15.4">De son forfait ancien ne se laverait pas !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Pour sauver sa couronne, il a sauvé sa vie !</l>
					<l n="62" num="16.2">Régner quand même fut son espoir suborneur ;</l>
					<l n="63" num="16.3">Il a parodié du vaincu de Pavie</l>
					<l n="64" num="16.4">Le mot sublime : « Tout est perdu, fors l'honneur ! »</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Sedan, ce pilori, cette immortelle injure,</l>
					<l n="66" num="17.2">Dont ne sera jamais le pays racheté,</l>
					<l n="67" num="17.3">C'est l'expiation d'un crime, le parjure,</l>
					<l n="68" num="17.4">Par un crime plus grand, l'insigne lâcheté !</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Mais, hélas ! c'est aussi le deuil inconsolable,</l>
					<l n="70" num="18.2">La ruine, ô mon Dieu ! que cet effondrement !</l>
					<l n="71" num="18.3">Et la France, à tes yeux, fut-elle si coupable</l>
					<l n="72" num="18.4">Qu'elle dût recevoir un pareil châtiment ?</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Dans ce gouffre sans fond, mystérieux abîme</l>
					<l n="74" num="19.2">Où s'engloutit un trône, une armée à la fois,</l>
					<l n="75" num="19.3">N'était-ce pas assez d'une double victime,</l>
					<l n="76" num="19.4">Et fallait-il encor qu'il en dévorât trois ?</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Car Sedan, c'est la France aux barbares livrée,</l>
					<l n="78" num="20.2">C'est le viol de Metz, c'est, logique du sort,</l>
					<l n="79" num="20.3">Paris capitulant, la patrie éventrée,</l>
					<l n="80" num="20.4">C'est l'affreuse rançon, la ruine et la mort !</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">A côté de Sedan, ce désastre suprême,</l>
					<l n="82" num="21.2">Qu'est-ce que Baylen, qu'est-ce que Pultawa ?</l>
					<l n="83" num="21.3">Sedan, il fait pâlir Warterloo lui-même !</l>
					<l n="84" num="21.4">Et peut-on bien encor parler de Sadowa ?</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Que sont près de Sedan les fatales journées,</l>
					<l n="86" num="22.2">Les douleurs de Crécy, de Poitiers, d'Azincourt,</l>
					<l n="87" num="22.3">Que la France aguerrie aux luttes obstinées.</l>
					<l n="88" num="22.4">Bientôt venge en brisant un joug honteux et lourd ?</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Sedan, c'est l'inconnu, le monstrueux, l'horrible,</l>
					<l n="90" num="23.2">Un prodige d'opprobre et de calamité,</l>
					<l n="91" num="23.3">Dans les fastes humains exception terrible,</l>
					<l n="92" num="23.4">Et qui sera l'effroi de la postérité.</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">O cité de malheur ! je frémis, ma main tremble,</l>
					<l n="94" num="24.2">Quand je trace, Sedan, tes cinq lettres de feu !</l>
					<l n="95" num="24.3">Car je vois le passé, l'avenir tout ensemble,</l>
					<l n="96" num="24.4">Atteints par ce grand coup marqué du doigt de Dieu !</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Je vois ton spectre pâle éclipsant de son ombre</l>
					<l n="98" num="25.2">Les radieux soleils d'Iéna, d'Austerlitz,</l>
					<l n="99" num="25.3">Et dans l'obscur lointain couvrant d'un voile sombre</l>
					<l n="100" num="25.4">Les plus fameux exploits de l'étendard des lis.</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">La France, qui comptait presque autant de victoires</l>
					<l n="102" num="26.2">Que de combats, déchue aujourd'hui de son rang,</l>
					<l n="103" num="26.3">La France ne peut plus se parer de ses gloires,</l>
					<l n="104" num="26.4">Que raille et que rançonne un brutal conquérant.</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1">Comme au temps d'Attila, pour chasser le barbare,</l>
					<l n="106" num="27.2">Elle n'a pas trouvé de patrice romain ;</l>
					<l n="107" num="27.3">Le grand Aétius brisa le flot tartare,</l>
					<l n="108" num="27.4">Et la France n'a pu briser le flot germain.</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1">A celle qui fut l'âme et le flambeau du monde,</l>
					<l n="110" num="28.2">La Prusse, à peine admise au cénacle des rois,</l>
					<l n="111" num="28.3">Parmi les nations pas même la seconde,</l>
					<l n="112" num="28.4">La Prusse, cet intrus, dicte aujourd'hui des lois !</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1">Ah ! pour faire rougir la Fortune insolente,</l>
					<l n="114" num="29.2">De si cruels dédains, de si rudes malheurs,</l>
					<l n="115" num="29.3">Le courroux indigné, la haine virulente,</l>
					<l n="116" num="29.4">Le mépris, ne sont pas assez, il faut des pleurs !</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1">Laissez-moi donc pleurer au pied de ce Calvaire,</l>
					<l n="118" num="30.2">Où la France, élevant ses bras meurtris au ciel,</l>
					<l n="119" num="30.3">Sous les coups du destin de plus en plus sévère,</l>
					<l n="120" num="30.4">Dut boire jusqu'au fond le calice de fiel !</l>
				</lg>
				<lg n="31">
					<l n="121" num="31.1">Oui, laissez-moi pleurer ; car pleurer c'est comprendre</l>
					<l n="122" num="31.2">Toute l'immensité d'un revers inouï !</l>
					<l n="123" num="31.3">Car pleurer ce n'est point pardonner, mais attendre</l>
					<l n="124" num="31.4">Que l'infaillible jour de la revanche ait lui !</l>
				</lg>
				<lg n="32">
					<l n="125" num="32.1">Car après tant d'affronts, après tant d'avanies,</l>
					<l n="126" num="32.2">Pleurer n'empêche point l'anathème vengeur ;</l>
					<l n="127" num="32.3">Car on peut, en pleurant, vouer aux gémonies</l>
					<l n="128" num="32.4">Le lâche qui fut traître et le Roi ravageur.</l>
				</lg>
				<lg n="33">
					<l n="129" num="33.1">Car Sedan c'est Satan, nom maudit, fatidique,</l>
					<l n="130" num="33.2">Dont le sinistre éclat flamboiera désormais,</l>
					<l n="131" num="33.3">Au fronton de ce livre, implacable, authentique,</l>
					<l n="132" num="33.4">Qui s'appelle l'Histoire et qui ne ment jamais !</l>
				</lg>
				<closer>
					<signed>◗ GUSTAVE DUPIN</signed>
					<dateline>
						<date when="1871">Octobre 1871</date>
					</dateline>
				</closer>
			</div></body></text></TEI>