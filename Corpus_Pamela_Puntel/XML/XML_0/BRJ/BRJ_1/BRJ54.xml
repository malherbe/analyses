<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ54">
					<head type="number">LIV</head>
					<head type="main">LA COUR DES MIRACLES</head>
					<lg n="1">
						<l n="1" num="1.1">Eh bien ! vous ne le croiriez pas ?</l>
						<l n="2" num="1.2">Les choses sont pourtant certaines</l>
						<l n="3" num="1.3">Ce ne sont pas tant les soldats</l>
						<l n="4" num="1.4">Qui volent que les capitaines !</l>
						<l n="5" num="1.5">Oui, tous ces brillants officiers,</l>
						<l n="6" num="1.6">Ces hobereaux, ces gentillâtres,</l>
						<l n="7" num="1.7">Si hautains qu'ils en sont grossiers</l>
						<l n="8" num="1.8">Dans leurs salons et leurs théâtres ;</l>
						<l n="9" num="1.9">Tous ces nobles à parchemin,</l>
						<l n="10" num="1.10">Qui passent dans le paysage,</l>
						<l n="11" num="1.11">Le nez en l'air, et de la main</l>
						<l n="12" num="1.12">Frappent leurs soldats au visage ;</l>
						<l n="13" num="1.13">Tous ces magnifiques seigneurs,</l>
						<l n="14" num="1.14">Exempts de peur et de reproches,</l>
						<l n="15" num="1.15">Couverts de grades et d'honneurs,</l>
						<l n="16" num="1.16">Garnissent volontiers leurs poches !</l>
						<l n="17" num="1.17">Ils prennent, d'un soin diligent,</l>
						<l n="18" num="1.18">(C'est la consigne universelle),</l>
						<l n="19" num="1.19">Les montres, les bijoux, l'argent…</l>
						<l n="20" num="1.20">Ils daignent casser la vaisselle.</l>
						<l n="21" num="1.21">Les propriétaires jaloux</l>
						<l n="22" num="1.22">Peuvent se plaindre ; on les assomme</l>
						<l n="23" num="1.23">Bref, une guerre de filous !…</l>
						<l n="24" num="1.24">N'est-ce pas que c'est gentilhomme ?</l>
						<l n="25" num="1.25">Transformer en gage d'amour</l>
						<l n="26" num="1.26">Un cachemire qu'on dérobe ;</l>
						<l n="27" num="1.27">Pour les tendresses du retour,</l>
						<l n="28" num="1.28">Emballer notre garde-robe !</l>
						<l n="29" num="1.29">Et, plus tard, quels étonnements,</l>
						<l n="30" num="1.30">Quand nos belles patriciennes</l>
						<l n="31" num="1.31">Reconnaîtront leurs diamants</l>
						<l n="32" num="1.32">Au cou des baronnes prussiennes !…</l>
						<l n="33" num="1.33">Cela sert d'exemple au vilain,</l>
						<l n="34" num="1.34">Pour qui les grands sont des oracles. –</l>
						<l n="35" num="1.35">On disait : la cour de Berlin' ;</l>
						<l n="36" num="1.36">On dira : la cour des miracles !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>