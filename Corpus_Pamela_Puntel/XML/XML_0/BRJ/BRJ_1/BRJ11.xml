<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ11">
					<head type="number">XI</head>
					<head type="main">LES BULLETINS DE VICTOIRE</head>
					<div type="section" n="1">
						<lg n="1">
							<l n="1" num="1.1">Non, Bertrand et Robert Macaire</l>
							<l n="2" num="1.2">Ne furent pas plus impudents ;</l>
							<l n="3" num="1.3">Ils mentent, ces foudres de guerre,</l>
							<l n="4" num="1.4">Gomme des arracheurs de dents :</l>
							<l n="5" num="1.5">Ces pick-pocket de la victoire,</l>
							<l n="6" num="1.6">Escamoteurs de bulletins,</l>
							<l n="7" num="1.7">Vous brusquent sans façon la gloire.</l>
							<l n="8" num="1.8">Comme la pire des catins.</l>
							<l n="9" num="1.9">Brave fille, à coups de cravache</l>
							<l n="10" num="1.10">Elle corrige ces valets ;</l>
							<l n="11" num="1.11">C'est avec des airs de bravache</l>
							<l n="12" num="1.12">Qu'ils en reçoivent les soufflets.</l>
							<l n="13" num="1.13">Ils vous ont des transports de joie,</l>
							<l n="14" num="1.14">Ces pantins à casques pointus,</l>
							<l n="15" num="1.15">Triomphants quand on les foudroie,</l>
							<l n="16" num="1.16">Et contents quand ils sont battus !</l>
							<l n="17" num="1.17">Que le mensonge soit leur règle,</l>
							<l n="18" num="1.18">Faut-il nous en plaindre ?… Fi donc !</l>
							<l n="19" num="1.19">L'Europe sait trop que leur aigle</l>
							<l n="20" num="1.20">A des allures de dindon.</l>
							<l n="21" num="1.21">Au bruit fêlé de son tonnerre,</l>
							<l n="22" num="1.22">On reconduira ce vilain,</l>
							<l n="23" num="1.23">Qui se prétend sorti d'une aire,</l>
							<l n="24" num="1.24">Dans son poulailler de Berlin !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1870">22 août 1870</date>
							</dateline>
						</closer>
					</div>
					<div type="section" n="2">
						<lg n="1">
							<l n="25" num="1.1">Hélas ! la trahison, les crimes,</l>
							<l n="26" num="1.2">Arrêtant la France en chemin,</l>
							<l n="27" num="1.3">Ont fait mentir ces pauvres rimes ;</l>
							<l n="28" num="1.4">Soit !… elles diront vrai demain !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1870">8 novembre 1 870.</date>
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>