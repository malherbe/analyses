<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ17">
					<head type="number">XVII</head>
					<head type="main">STRASBOURG</head>
					<lg n="1">
						<l n="1" num="1.1">Non ! ce n'est pas la guerre ! — On a vu des armées,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"/>La baïonnette rouge aux mains,</l>
						<l n="3" num="1.3">Par le carnage même au carnage animées,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"/>Entrechoquer leurs flots humains.</l>
						<l n="5" num="1.5">Soldats contre soldats, dans une aveugle rage,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"/>Renversés, mourants, à genoux,</l>
						<l n="7" num="1.7">Combattaient ; — des lions ils avaient le courage.</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"/>Mais non la cruauté des loups !</l>
						<l n="9" num="1.9">Jusque dans leurs fureurs les vertus étaient sauves ;</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"/>L'honneur se fût retrouvé là !…—</l>
						<l n="11" num="1.11">Ce qu'on n'avait pas vu, depuis les bêtes fauves</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"/>De Genséric et d'Attila.</l>
						<l n="13" num="1.13">Ce qui dépassera même les plus infâmes,</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"/>C'est ce raffinement vainqueur</l>
						<l n="15" num="1.15">De tuer lâchement des enfants et des femmes</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"/>Pour frapper les hommes au cœur ;</l>
						<l n="17" num="1.17">De calculer le sang, la terreur et les larmes</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"/>Dont il faut remplir la cité</l>
						<l n="19" num="1.19">Pour vaincre des soldats impuissants sous leurs armes.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"/>Et pour briser leur volonté !</l>
						<l n="21" num="1.21">Voilà ce qu'ils ont fait, voilà de quelle gloire</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"/>Se couvrent ces bourreaux pieux !</l>
						<l n="23" num="1.23">Les Peaux rouges, les Thugs, d'exécrable mémoire,</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"/>O Bismark, n'auraient pas fait mieux !</l>
						<l n="25" num="1.25">Non, ce n'est pas la guerre !… Et le cœur se soulève,</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"/>La colère gonfle le sein,</l>
						<l n="27" num="1.27">Quand on voit le soldat abandonner son glaive</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"/>Pour le couteau de l'assassin !</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1">Promène maintenait ta grandeur souveraine</l>
						<l n="30" num="2.2"><space unit="char" quantity="8"/>Parmi ces peuples envahis,</l>
						<l n="31" num="2.3">O roi Guillaume ! prends l'Alsace et la Lorraine,</l>
						<l n="32" num="2.4"><space unit="char" quantity="8"/>Pour les souder à ton pays.</l>
						<l n="33" num="2.5">Répands sur leurs malheurs une larme hypocrite,</l>
						<l n="34" num="2.6"><space unit="char" quantity="8"/>Et de ton cœur compatissant</l>
						<l n="35" num="2.7">Que la douceur chrétienne y soit partout écrite</l>
						<l n="36" num="2.8"><space unit="char" quantity="8"/>En traits de flamme, en traits de sang !…</l>
						<l n="37" num="2.9">Ah ! chez nos paysans de Lorraine et d'Alsace</l>
						<l n="38" num="2.10"><space unit="char" quantity="8"/>Tu te croyais un autre accès ?</l>
						<l n="39" num="2.11">Tu pensais des Germains y retrouver la trace ?</l>
						<l n="40" num="2.12"><space unit="char" quantity="8"/>Non, Guillaume ! ils étaient Français !</l>
						<l n="41" num="2.13">Frappe-les ! frappe-les ! De cette hydre fertile</l>
						<l n="42" num="2.14"><space unit="char" quantity="8"/>Suscite les rébellions !</l>
						<l n="43" num="2.15">Frappe !… Pour un qui tombe il en surgira mille.</l>
						<l n="44" num="2.16"><space unit="char" quantity="8"/>Et pour mille des millions !</l>
						<l n="45" num="2.17">Oui, jusqu'à submerger ton trône et ton royaume,</l>
						<l n="46" num="2.18"><space unit="char" quantity="8"/>Et tes étendards triomphants,</l>
						<l n="47" num="2.19">Et Sadowa vengé par nous, ô roi Guillaume,</l>
						<l n="48" num="2.20"><space unit="char" quantity="8"/>Et les enfants de tes enfants !</l>
						<l n="49" num="2.21">Pour nous débaptiser tes cruautés sont vaines,</l>
						<l n="50" num="2.22"><space unit="char" quantity="8"/>Et tes bienfaits sont superflus ;</l>
						<l n="51" num="2.23">Quand le sang de la France a passé dans nos veines.</l>
						<l n="52" num="2.24"><space unit="char" quantity="8"/>Ce sang-là ne se refait plus !</l>
					</lg>
					<lg n="3">
						<l n="53" num="3.1">Strasbourg, noble cité, tes murs et tes victimes</l>
						<l n="54" num="3.2"><space unit="char" quantity="8"/>Seront à la postérité</l>
						<l n="55" num="3.3">Les bulletins vengeurs, les témoins de leurs crimes</l>
						<l n="56" num="3.4"><space unit="char" quantity="8"/>Et de ton courage indompté !</l>
						<l n="57" num="3.5">L'histoire les attend et leur fera justice,</l>
						<l n="58" num="3.6"><space unit="char" quantity="8"/>Éclairés de ce même feu</l>
						<l n="59" num="3.7">Qui brûlait la pensée, et le temple, et l'hospice,</l>
						<l n="60" num="3.8"><space unit="char" quantity="8"/>La pitié, l'âme humaine, et Dieu ! —</l>
						<l n="61" num="3.9">C'est bien ! plus de pitié ! Que des armes plus sûres</l>
						<l n="62" num="3.10"><space unit="char" quantity="8"/>Servent des bras, des cœurs plus forts !</l>
						<l n="63" num="3.11">Nous songerons peut-être à panser les blessures</l>
						<l n="64" num="3.12"><space unit="char" quantity="8"/>Quand nous aurons vengé nos morts !</l>
						<l n="65" num="3.13">Et ne nous bornons pas à maudire les maîtres !</l>
						<l n="66" num="3.14"><space unit="char" quantity="8"/>Pour servir leurs plans belliqueux,</l>
						<l n="67" num="3.15">Ces peuples de bourreaux, d'espions et de traîtres</l>
						<l n="68" num="3.16"><space unit="char" quantity="8"/>Étaient tout entiers avec eux !</l>
						<l n="69" num="3.17">Un vieux levain jaloux fermentait dans leur âme ;</l>
						<l n="70" num="3.18"><space unit="char" quantity="8"/>Leur orgueil s'était offensé</l>
						<l n="71" num="3.19">De voir trop de lauriers couronner l'oriflamme</l>
						<l n="72" num="3.20"><space unit="char" quantity="8"/>Qui racontait notre passé ;</l>
						<l n="73" num="3.21">Et, du jour où Bismark, devenu populaire</l>
						<l n="74" num="3.22"><space unit="char" quantity="8"/>Par l'appât d'un premier succès,</l>
						<l n="75" num="3.23">Réveilla dans les cœurs cet espoir séculaire</l>
						<l n="76" num="3.24"><space unit="char" quantity="8"/>D'anéantir le nom Français,</l>
						<l n="77" num="3.25">De ce jour-là, les champs, les châteaux et les villes</l>
						<l n="78" num="3.26"><space unit="char" quantity="8"/>Se turent par enchantement ;</l>
						<l n="79" num="3.27">L'Allemagne fit trève aux discordes civiles,</l>
						<l n="80" num="3.28"><space unit="char" quantity="8"/>Et guetta l'heure et le moment.</l>
						<l n="81" num="3.29">Elle s'arma dans l'ombre, et savoura la joie,</l>
						<l n="82" num="3.30"><space unit="char" quantity="8"/>L'ivresse de nous outrager,</l>
						<l n="83" num="3.31">Attendit le signal, et bondit sur sa proie !…</l>
						<l n="84" num="3.32"><space unit="char" quantity="8"/>Aux armes !… voici l'étranger !…</l>
						<l n="85" num="3.33">L'étranger, Dieu puissant ! l'étranger sur la terre</l>
						<l n="86" num="3.34"><space unit="char" quantity="8"/>De Vaucouleurs, de Domrémy !…</l>
						<l n="87" num="3.35">O Jeanne ! lègue-nous ta haine héréditaire ;</l>
						<l n="88" num="3.36"><space unit="char" quantity="8"/>Marche avec nous à l'ennemi !</l>
						<l n="89" num="3.37">L'étranger sur le sol de la mère-patrie !</l>
						<l n="90" num="3.38"><space unit="char" quantity="8"/>O honte ! ô larmes ! ô douleurs !</l>
						<l n="91" num="3.39">Un uhlan se vautrant sur la France meurtrie !…</l>
						<l n="92" num="3.40"><space unit="char" quantity="8"/>Non, non ! du sang ! et pas de pleurs !</l>
					</lg>
					<lg n="4">
						<l n="93" num="4.1">Qu'ils soient maudits ! que Dieu condamne cette engeance</l>
						<l n="94" num="4.2"><space unit="char" quantity="8"/>D'hypocrites et de bandits !</l>
						<l n="95" num="4.3">— Ah ! que la haine est douce, et douce la vengeance ! —</l>
						<l n="96" num="4.4"><space unit="char" quantity="8"/>Qu'ils soient maudits ! qu'ils soient maudits !</l>
						<l n="97" num="4.5">Que dans leurs flots sanglants le Rhin et la Moselle</l>
						<l n="98" num="4.6"><space unit="char" quantity="8"/>Vers l'Océan roulent leurs corps ;</l>
						<l n="99" num="4.7">Que pas un chez les siens n'en porte la nouvelle,</l>
						<l n="100" num="4.8"><space unit="char" quantity="8"/>Avec le dernier cri des morts ;</l>
						<l n="101" num="4.9">Que leur lâche attentat au monde les signale,</l>
						<l n="102" num="4.10"><space unit="char" quantity="8"/>Les voue aux malédictions,</l>
						<l n="103" num="4.11">Et que la Prusse, avec cette marque fatale,</l>
						<l n="104" num="4.12"><space unit="char" quantity="8"/>Soit mise au ban des nations !</l>
					</lg>
					<lg n="5">
						<l n="105" num="5.1">Toi Werder, toi bourreau, toi le hideux ministre</l>
						<l n="106" num="5.2"><space unit="char" quantity="8"/>D'une hideuse atrocité,</l>
						<l n="107" num="5.3">Sois cloué, tout vivant, comme un hibou sinistre.</l>
						<l n="108" num="5.4"><space unit="char" quantity="8"/>Au carcan de l'humanité !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">2 septembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>