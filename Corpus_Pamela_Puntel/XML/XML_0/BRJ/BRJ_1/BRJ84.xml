<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ84">
					<head type="number">LXXXIV</head>
					<head type="main">TERESITA</head>
					<lg n="1">
						<l n="1" num="1.1">La fille attendait son père,</l>
						<l n="2" num="1.2">Et la femme son époux ;</l>
						<l n="3" num="1.3">Faut-il que Dieu désespère</l>
						<l n="4" num="1.4">Ceux qui combattent pour nous ?</l>
						<l n="5" num="1.5">L'ange de l'humble demeure</l>
						<l n="6" num="1.6">Ne les y recevra pas !…</l>
						<l n="7" num="1.7"><space unit="char" quantity="4"/>Garibaldi pleure !</l>
						<l n="8" num="1.8"><space unit="char" quantity="10"/>Hélas !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Tandis que pour l'Italie</l>
						<l n="10" num="2.2">Le père vient payer seul</l>
						<l n="11" num="2.3">Une dette qu'elle oublie,</l>
						<l n="12" num="2.4">L'enfant dort dans son linceul.</l>
						<l n="13" num="2.5">L adieu de la dernière heure</l>
						<l n="14" num="2.6">N'adoucit point son trépas !…</l>
						<l n="15" num="2.7"><space unit="char" quantity="4"/>Garibaldi pleure !</l>
						<l n="16" num="2.8"><space unit="char" quantity="10"/>Hélas !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1">« Dieu condamne ce qu'il aime !… »</l>
						<l n="18" num="3.2">Crîra sur ce pauvre corps</l>
						<l n="19" num="3.3">Un dévôt dont l'anathême</l>
						<l n="20" num="3.4">Est la prière des morts !</l>
						<l n="21" num="3.5">Notre prière est meilleure ;</l>
						<l n="22" num="3.6">Nos sanglots sonnent le glas !…</l>
						<l n="23" num="3.7"><space unit="char" quantity="4"/>Garibaldi pleure !</l>
						<l n="24" num="3.8"><space unit="char" quantity="10"/>Hélas !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">Malgré l'âge et la souffrance,</l>
						<l n="26" num="4.2">Soldat de l'humanité,</l>
						<l n="27" num="4.3">Il vengeait déjà la France</l>
						<l n="28" num="4.4">D'un malheur immérité.</l>
						<l n="29" num="4.5">« Il n'importe que je meure ;</l>
						<l n="30" num="4.6">Disait-il, « suivez mes pas !… »</l>
						<l n="31" num="4.7"><space unit="char" quantity="4"/>Garibaldi pleure !</l>
						<l n="32" num="4.8"><space unit="char" quantity="10"/>Hélas !</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">Je voulais chanter ses armes</l>
						<l n="34" num="5.2">Quand ma muse s'arrêta</l>
						<l n="35" num="5.3">En voyant couler ses larmes</l>
						<l n="36" num="5.4">Au nom de Teresita.</l>
						<l n="37" num="5.5">Que mon vers pieux effleure</l>
						<l n="38" num="5.6">Ce nom sacré !… parlons bas !</l>
						<l n="39" num="5.7"><space unit="char" quantity="4"/>Garibaldi pleure !</l>
						<l n="40" num="5.8"><space unit="char" quantity="10"/>Hélas !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1871">Janvier 1871</date>
						</dateline>
					</closer>
				</div></body></text></TEI>