<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ60">
					<head type="number">LX</head>
					<head type="main">DEUXIÈME A AUGUSTA</head>
					<lg n="1">
						<l n="1" num="1.1">Chère Augusta, ce même Dieu</l>
						<l n="2" num="1.2">Que nous avons comblé d'hommage</l>
						<l n="3" num="1.3">De l'ennemi prenant le jeu,</l>
						<l n="4" num="1.4">Me cause les plus grands dommages</l>
						<l n="5" num="1.5">Il me fait rosser aujourd'hui</l>
						<l n="6" num="1.6">Par ces légions de Pygmées !</l>
						<l n="7" num="1.7">Vieux brigand de Dieu des armées !…</l>
						<l n="8" num="1.8">Après ce que j'ai fait pour lui !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">De bonne foi, pouvais-je croire,</l>
						<l n="10" num="2.2">Au lendemain de nos succès,</l>
						<l n="11" num="2.3">Que ces enragés de Français</l>
						<l n="12" num="2.4">Allaient me battre sur la Loire ?…</l>
						<l n="13" num="2.5">Oui, ma chère, de vrais soldats,</l>
						<l n="14" num="2.6">Cavalerie, infanterie,</l>
						<l n="15" num="2.7">Et même de l'artillerie,</l>
						<l n="16" num="2.8">Qui n'entendent pas raillerie,</l>
						<l n="17" num="2.9">Et me livrent de vrais combats !</l>
						<l n="18" num="2.10">Maintenant la chose est certaine :</l>
						<l n="19" num="2.11">Ils n'existent pas que de nom…</l>
						<l n="20" num="2.12">Ah ! si j'avais eu là Bazaine,</l>
						<l n="21" num="2.13">Ou seulement Napoléon !</l>
					</lg>
					<lg n="3">
						<l n="22" num="3.1">Ce n'est pas tout ; Paris lui-même</l>
						<l n="23" num="3.2">M'en fait voir de toutes couleurs !</l>
						<l n="24" num="3.3">Chaque jour, nouvelles douleurs ;</l>
						<l n="25" num="3.4">C'est au point que j'en deviens blême !</l>
						<l n="26" num="3.5">J'ai vu l'instant où ce Paris,</l>
						<l n="27" num="3.6">Dans son audace sacrilège,</l>
						<l n="28" num="3.7">Me prenait, au lieu d'être pris !…</l>
						<l n="29" num="3.8">Que diable ! ce n'est plus un siège !</l>
						<l n="30" num="3.9">Encor si les Parisiens</l>
						<l n="31" num="3.10">Ne se permettaient pas de rire !…</l>
						<l n="32" num="3.11">Jamais je n'oserai te dire</l>
						<l n="33" num="3.12">Ce qu'ils appellent leurs prussiens !</l>
					</lg>
					<lg n="4">
						<l n="34" num="4.1">Certes, Bismark est un grand homme ;</l>
						<l n="35" num="4.2">Mais peut-être as-tu remarqué</l>
						<l n="36" num="4.3">Qu'il se trompe souvent ?… en somme.</l>
						<l n="37" num="4.4">Je crois qu'il est un peu toqué.</l>
						<l n="38" num="4.5">Il m'annonce une populace</l>
						<l n="39" num="4.6">Qui se rendra sans coup férir,</l>
						<l n="40" num="4.7">Et voilà que je suis en face</l>
						<l n="41" num="4.8">De citoyens prêts à mourir !</l>
					</lg>
					<lg n="5">
						<l n="42" num="5.1">Bref, aujourd'hui je me demande</l>
						<l n="43" num="5.2">Comment finira tout ceci ;</l>
						<l n="44" num="5.3">D'autant que ma suite allemande</l>
						<l n="45" num="5.4">Ajoute encor à mon souci.</l>
						<l n="46" num="5.5">Oh ! ces Bavarois !… quelle engeance !</l>
						<l n="47" num="5.6">Ils déclarent qu'ils sont à bout,</l>
						<l n="48" num="5.7">Et lâchent pied !… C'est d'eux surtout</l>
						<l n="49" num="5.8">Que je prétends tirer vengeance.</l>
						<l n="50" num="5.9">Louis n'a qu'à se bien tenir ;</l>
						<l n="51" num="5.10">Je lui prépare une musique</l>
						<l n="52" num="5.11">Qui fera pâlir, je m'en pique,</l>
						<l n="53" num="5.12">Toutes celles de l'avenir !</l>
					</lg>
					<lg n="6">
						<l n="54" num="6.1">Et le czar ?… Tu connais sa note ?…</l>
						<l n="55" num="6.2">En conscience, est-il permis</l>
						<l n="56" num="6.3">De jouer ainsi ses amis ?</l>
						<l n="57" num="6.4">Je ne sais pas ce qu'il tripotte.</l>
						<l n="58" num="6.5">Mais il me jette sur les bras</l>
						<l n="59" num="6.6">Florence, Vienne et l'Angleterre !</l>
						<l n="60" num="6.7">Que lui coûtait-il de se taire ?</l>
						<l n="61" num="6.8">Me voilà dans de jolis draps !</l>
					</lg>
					<lg n="7">
						<l n="62" num="7.1">Pourtant, ne jetons pas le manche</l>
						<l n="63" num="7.2">Après la cognée ; il faut voir</l>
						<l n="64" num="7.3">Si Moltke prendra sa revanche :</l>
						<l n="65" num="7.4">Et de fait il prétend l'avoir.</l>
						<l n="66" num="7.5">Surtout que les feuilles publiques</l>
						<l n="67" num="7.6">Expliquent bien aux mécréants</l>
						<l n="68" num="7.7">Notre retraite d'Orléans</l>
						<l n="69" num="7.8">Par des mouvements stratégiques ;</l>
						<l n="70" num="7.9">Comme aussi cette vérité.</l>
						<l n="71" num="7.10">Que, si Paris n'est pas encore</l>
						<l n="72" num="7.11">Anéanti comme Gomorrhe,</l>
						<l n="73" num="7.12">C'est par raison d'humanité. —</l>
						<l n="74" num="7.13">Oh ! mitrailler ce peuple en masse !</l>
						<l n="75" num="7.14">Bombarder Paris ! le faucher !…</l>
						<l n="76" num="7.15">Mais, pour bombarder une place,</l>
						<l n="77" num="7.16">Il faut pouvoir s'en approcher.</l>
						<l n="78" num="7.17">Que ferai-je, en cas de défaite,</l>
						<l n="79" num="7.18">Du vaste amas de lampions</l>
						<l n="80" num="7.19">Dont à Berlin nous espérions</l>
						<l n="81" num="7.20">Illuminer ce jour de fête ?</l>
						<l n="82" num="7.21">Non, je ne peux m'accoutumer</l>
						<l n="83" num="7.22">A ces angoisses lamentables…</l>
						<l n="84" num="7.23">C'est que ces gens-là sont capables</l>
						<l n="85" num="7.24">De venir nous les allumer !</l>
					</lg>
					<lg n="8">
						<l n="86" num="8.1">Tout cela, vois-tu, c'est la faute</l>
						<l n="87" num="8.2">De ce Gambetta… Quel coquin !</l>
						<l n="88" num="8.3">Ce manant, ce républicain</l>
						<l n="89" num="8.4">Gouverne, parle, agit, court, saute,</l>
						<l n="90" num="8.5">Monte en ballon, travaille, écrit.</l>
						<l n="91" num="8.6">Décrète. à donner le vertige !…</l>
						<l n="92" num="8.7">Ce n'est pas un homme, te dis-je.</l>
						<l n="93" num="8.8">Assurément c'est l'antéchrist</l>
						<l n="94" num="8.9">Ah ! le mieux a fait place au pire !</l>
						<l n="95" num="8.10">Tout allait si bien sous l'empire !</l>
						<l n="96" num="8.11">Je n'y songe qu'en enrageant !…</l>
						<l n="97" num="8.12">A propos, un journal rapporte</l>
						<l n="98" num="8.13">Qu'on fait voir, derrière une porte,</l>
						<l n="99" num="8.14">Napoléon pour de l'argent.</l>
						<l n="100" num="8.15">C'est une idée !… Il faut connaître,</l>
						<l n="101" num="8.16">Par les résultats obtenus,</l>
						<l n="102" num="8.17">Ce qu'elle vaut. Cela peut être</l>
						<l n="103" num="8.18">Une source de revenus. —</l>
					</lg>
					<lg n="9">
						<l n="104" num="9.1">Bon ! encor le canon qui tonne !…</l>
						<l n="105" num="9.2">Ah ! Ton ne mène pas ici</l>
						<l n="106" num="9.3">Une existence monotone !…</l>
						<l n="107" num="9.4">Diable ! que veut dire ceci ?</l>
						<l n="108" num="9.5">Une fusillade !… Serait-ce ? .</l>
						<l n="109" num="9.6">Mon carosse est prêt, Dieu merci !</l>
						<l n="110" num="9.7">J’abrège ; le courrier nu presse ; —</l>
						<l n="111" num="9.8">Et les baïonnettes aussi !</l>
					</lg>
					<lg n="10">
						<l n="112" num="10.1">Ingrat Sabaoth !… Quand j'y pense,</l>
						<l n="113" num="10.2">Mettre ainsi ma gloire a quia !…</l>
						<l n="114" num="10.3">Tu m'entends bien ; je te dispense</l>
						<l n="115" num="10.4">De lui chanter Alleluia ! —</l>
						<l n="116" num="10.5">Veux-tu que je te dise ?… un chaume,</l>
						<l n="117" num="10.6">Entre Fritz et mon Augusta,</l>
						<l n="118" num="10.7">Loin du monde… et de Gambetta…</l>
						<l n="119" num="10.8">Et des francs-tireurs !… — Ton Guillaume.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>