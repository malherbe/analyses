<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ36">
					<head type="number">XXXVI</head>
					<head type="main">ENFANTS ET VIEILLARDS</head>
					<lg n="1">
						<l n="1" num="1.1">Des enfants de quinze ans ! des vieillards de soixante !</l>
						<l n="2" num="1.2"><space unit="char" quantity="12"/>Un troupeau voyageur</l>
						<l n="3" num="1.3">Marchant à l'abattoir, victime obéissante !…</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"/>Est-ce-vrai, Dieu vengeur ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Puis les femmes, suivant le fils, l'époux, le père,</l>
						<l n="6" num="2.2"><space unit="char" quantity="12"/>Sur un sol étranger !</l>
						<l n="7" num="2.3">— Hélas ! l'homme parti, la femme désespère !</l>
						<l n="8" num="2.4"><space unit="char" quantity="12"/>L'enfant voudrait manger !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Voilà, voilà par qui vos hordes sont accrues.</l>
						<l n="10" num="3.2"><space unit="char" quantity="12"/>O bourreaux sans remords !</l>
						<l n="11" num="3.3">Princes et rois, voilà les nouvelles recrues</l>
						<l n="12" num="3.4"><space unit="char" quantity="12"/>Qui remplacent vos morts !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Ah ! je n'y puis durer ! l'horreur est dans mon âme ;</l>
						<l n="14" num="4.2"><space unit="char" quantity="12"/>Ma force me trahit,</l>
						<l n="15" num="4.3">Et malgré leur fureur, leur mitraille et leur flamme,</l>
						<l n="16" num="4.4"><space unit="char" quantity="12"/>La pitié m'envahit !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Mais, lorsque nos Français vous verront sous vos armes</l>
						<l n="18" num="5.2"><space unit="char" quantity="12"/>Si jeunes ou si vieux,</l>
						<l n="19" num="5.3">Les fusils tomberont de leurs mains, et les larmes</l>
						<l n="20" num="5.4"><space unit="char" quantity="12"/>Jailliront de leurs yeux !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Que voulez-vous ? ils ont ces scrupules infâmes,</l>
						<l n="22" num="6.2"><space unit="char" quantity="12"/>Vaincus ou triomphants,</l>
						<l n="23" num="6.3">D'épargner les vieillards, de protéger les femmes,</l>
						<l n="24" num="6.4"><space unit="char" quantity="12"/>De sauver les enfants !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Ainsi, sans autre but et sans autre espérance,</l>
						<l n="26" num="7.2"><space unit="char" quantity="12"/>Vos maîtres obéis.</l>
						<l n="27" num="7.3">Pour l'unique plaisir d'assassiner la France,</l>
						<l n="28" num="7.4"><space unit="char" quantity="12"/>Dépeuplent leur pays !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Ils veulent que la proie, à leurs pieds abattue,</l>
						<l n="30" num="8.2"><space unit="char" quantity="12"/>Expire en frissonnant !…</l>
						<l n="31" num="8.3">Ah ! vous le voyez bien qu'il faudra qu'on vous tue,</l>
						<l n="32" num="8.4"><space unit="char" quantity="12"/>Même en se détournant !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Il le faudra !… — Mais vous qui faites cette guerre,</l>
						<l n="34" num="9.2"><space unit="char" quantity="12"/>Pour en payer le prix,</l>
						<l n="35" num="9.3">Rites, est-il assez de haine sur la terre,</l>
						<l n="36" num="9.4"><space unit="char" quantity="12"/>De haine et de mépris ?</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">O Guillaume, ô Bismark, pour conter votre gloire</l>
						<l n="38" num="10.2"><space unit="char" quantity="12"/>Aux générations,</l>
						<l n="39" num="10.3">Est-il assez d'opprobre at» livre de l'histoire,</l>
						<l n="40" num="10.4"><space unit="char" quantity="12"/>Et d'imprécations ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">De palais consumés en vos propres royaumes</l>
						<l n="42" num="11.2"><space unit="char" quantity="12"/>Pour nos hameaux détruits ;</l>
						<l n="43" num="11.3">De remords pour vos jours, et de pâles fantômes</l>
						<l n="44" num="11.4"><space unit="char" quantity="12"/>Pour l'ombre de vos nuits ?</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Assez de Némésis, assez de gémonies</l>
						<l n="46" num="12.2"><space unit="char" quantity="12"/>Pour votre cruauté ?</l>
						<l n="47" num="12.3">Et, pour votre âme, au sein des douleurs infinies,</l>
						<l n="48" num="12.4"><space unit="char" quantity="12"/>Assez d'éternité ?…</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Octobre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>