<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ59">
					<head type="number">LIX</head>
					<head type="main">LE 2 DÉCEMBRE</head>
					<lg n="1">
						<l n="1" num="1.1">Voici le jour où sur nos pères</l>
						<l n="2" num="1.2">Brilla le soleil d'Austerlitz ;</l>
						<l n="3" num="1.3">Où, plus tard, des cieux moins prospères,</l>
						<l n="4" num="1.4">L'ombre descendit sur leurs fils !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Dans sa superbe indifférence.</l>
						<l n="6" num="2.2">Le canon, sujet à l'erreur,</l>
						<l n="7" num="2.3">Annonçait gaîment à la France</l>
						<l n="8" num="2.4">Sa sottise et son empereur !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Et voyez, — étrange ironie ! —</l>
						<l n="10" num="3.2">A quoi le sort m'a condamné :</l>
						<l n="11" num="3.3">Pour moi cette date est bénie :</l>
						<l n="12" num="3.4">C'est le jour où l'enfant est né !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Oui, ma tille venait de naître</l>
						<l n="14" num="4.2">Au bruit de ce fatal salve !…</l>
						<l n="15" num="4.3">Et c'est le seul malheur peut-être</l>
						<l n="16" num="4.4">Qui lui soit jamais arrivé.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Sur le berceau que son sourire</l>
						<l n="18" num="5.2">Illuminait, je lis ce vœu :</l>
						<l n="19" num="5.3">Qu'elle survécût à l'Empire ;</l>
						<l n="20" num="5.4">Et je fus entendu de Dieu !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Elle survit ; il fait naufrage ;</l>
						<l n="22" num="6.2">Et désormais ce jour fêté</l>
						<l n="23" num="6.3">Brille à mes yeux, sans qu'un nuage</l>
						<l n="24" num="6.4">En trouble la sérénité !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Elle a même, à ce qu'il faut croire,</l>
						<l n="26" num="7.2">Toutes les grâces du destin ;</l>
						<l n="27" num="7.3">Car ce sont des bruits de victoire</l>
						<l n="28" num="7.4">Qui nous arrivent ce matin !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Notre Loire, après deux batailles,</l>
						<l n="30" num="8.2">Voit s'enfuir les envahisseurs ;</l>
						<l n="31" num="8.3">Paris, vainqueur sous ses murailles.</l>
						<l n="32" num="8.4">Attend ses nouveaux défenseurs !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Dieu cesse enfin d'être sévère</l>
						<l n="34" num="9.2">A ce pays qu'il déchira ;</l>
						<l n="35" num="9.3">Ce que l'Empire n'a pu faire,</l>
						<l n="36" num="9.4">La République le fera !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Ces mêmes Rois qui se querellent</l>
						<l n="38" num="10.2">S'uniront pour en triompher,</l>
						<l n="39" num="10.3">Ces bons frères, comme ils s'appellent,</l>
						<l n="40" num="10.4">S'embrasseront pour l'étouffer ;</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Qu'importe ?… mes frayeurs me quittent,</l>
						<l n="42" num="11.2">Quand mon regard mesure et suit</l>
						<l n="43" num="11.3">Ces petits princes qui s'agitent</l>
						<l n="44" num="11.4">Aux mains de Dieu qui les conduit !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Que Bonaparte aide Guillaume</l>
						<l n="46" num="12.2">En ses-complots impertinents :</l>
						<l n="47" num="12.3">L'Empire n'est plus qu'un fantôme :</l>
						<l n="48" num="12.4">Je ne crois pas aux revenants !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">O jour du deux Décembre, monte</l>
						<l n="50" num="13.2">Sur l'horizon, pur et vermeil ;</l>
						<l n="51" num="13.3">Te voilà lavé de ta honte,</l>
						<l n="52" num="13.4">Et tu retrouves ton soleil !…</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Mais quoi ! l'hiver te fait cortège ;</l>
						<l n="54" num="14.2">Ton soleil est trop faible, hélas !</l>
						<l n="55" num="14.3">Pour fondre ce tapis de neige</l>
						<l n="56" num="14.4">Que la terre étend sous nos pas ! —</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Pardonne, enfant, si je n'apporte,</l>
						<l n="58" num="15.2">Pour te fêter, que cette fleur ;</l>
						<l n="59" num="15.3">Je l'ai trouvée à moitié morte,</l>
						<l n="60" num="15.4">Étiolée et sans couleur !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Mais, avec cette fleur flétrie.</l>
						<l n="62" num="16.2">J'offre un peuple ressuscité,</l>
						<l n="63" num="16.3">Les victoires de la patrie,</l>
						<l n="64" num="16.4">Et celles de la liberté !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>