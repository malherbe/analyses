<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ52">
					<head type="number">LII</head>
					<head type="main">JUSTICE POUR TOUS</head>
					<lg n="1">
						<l n="1" num="1.1">Peut-être qu'en ces vers animés par la fièvre</l>
						<l n="2" num="1.2">Un mot injuste a pu s'échapper de ma lèvre ;</l>
						<l n="3" num="1.3">Mesurons la colère où notre âme s'aigrit,</l>
						<l n="4" num="1.4">Et ne condamnons pas le bras comme l'esprit.</l>
						<l n="5" num="1.5">Avant qu'un châtiment sur eux s'appesantisse,</l>
						<l n="6" num="1.6">Même a ses ennemis il faut rendre justice.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Qui sont-ils ? — Séparons les troupeaux des bergers.</l>
						<l n="8" num="2.2">N'en est-il pas, parmi ces peuples étrangers,</l>
						<l n="9" num="2.3">Que la Prusse égoïste enlève à coup de crosse,</l>
						<l n="10" num="2.4">Pour servir d'instruments à sa haine féroce ;</l>
						<l n="11" num="2.5">Que la force brutale arrache à leurs foyers,</l>
						<l n="12" num="2.6">Et dont la peur aveugle a fait des meurtriers ?…</l>
						<l n="13" num="2.7">La Prusse, cependant, impassible statue,</l>
						<l n="14" num="2.8">Pour voler leur dépouille attendra qu'on les tue !</l>
					</lg>
					<lg n="3">
						<l n="15" num="3.1">On sait comment ce Roi toujours béni de Dieu,</l>
						<l n="16" num="3.2">Guillaume, fait marcher ses alliés au feu !</l>
						<l n="17" num="3.3">Il leur dépeint la France ainsi qu'une furie</l>
						<l n="18" num="3.4">Dont le vaincu devra subir la barbarie…</l>
						<l n="19" num="3.5">Nos prisonniers tremblants s'attendent à mourir ;</l>
						<l n="20" num="3.6">Ils doutent de la main qui les vient secourir,</l>
						<l n="21" num="3.7">Et, quand leur cœur, enfin, renaît à l'espérance,</l>
						<l n="22" num="3.8">Ils pleurent en jetant un cri : Vive la France ! —</l>
						<l n="23" num="3.9">Leur crime, (et par ses fers l'esclave est châtié)</l>
						<l n="24" num="3.10">Est de souffrir le joug de maîtres sans pitié.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">O pasteurs d'hommes, vous dont la main sanguinaire</l>
						<l n="26" num="4.2">Du maître souverain s'arroge le tonnerre,</l>
						<l n="27" num="4.3">Quel compte rendrez-vous de vos ambitions</l>
						<l n="28" num="4.4">Au Dieu dont vous volez les bénédictions ?…</l>
						<l n="29" num="4.5">Car cela semble un droit sacré du diadême,</l>
						<l n="30" num="4.6">De pouvoir tout yoler, jusques à Dieu lui-même !…</l>
						<l n="31" num="4.7">Et nous verrons encor de stupides effrois</l>
						<l n="32" num="4.8">Honnir la République et célébrer les rois !…</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">Que vous êtes plus grands, vous qui, vaincus naguère</l>
						<l n="34" num="5.2">Aux champs de Magenta, maudissez cette guerre,</l>
						<l n="35" num="5.3">Et qui, sans réveiller des souvenirs jaloux,</l>
						<l n="36" num="5.4">Quand nous sommes vaincus, tendez les mains vers nous !</l>
						<l n="37" num="5.5">O nos frères d'Autriche, honneur de l'Allemagne,</l>
						<l n="38" num="5.6">Vous êtes comme nous les fils de Charlemagne,</l>
						<l n="39" num="5.7">Et ce cri de vos cœurs jusqu'à nous apporté</l>
						<l n="40" num="5.8">Consacre pour toujours cette fraternité !</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1">Oui, justice pour tous !… Et gravons dans nos âmes.</l>
						<l n="42" num="6.2">Le nom des généreux et celui des infâmes !</l>
						<l n="43" num="6.3">Celui des faibles peut rester enseveli</l>
						<l n="44" num="6.4">Dans un pardon mêlé de dédain et d'oubli ;</l>
						<l n="45" num="6.5">Vous, peuples opprimés, peuples qu'on expatrie,</l>
						<l n="46" num="6.6">Que l'on traîne au combat comme à la boucherie,</l>
						<l n="47" num="6.7">Vous êtes les premiers que la Prusse accabla…</l>
						<l n="48" num="6.8">Resserrons notre haine aux Prussiens !… Mais ceux-là !…</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1">La France, de leur morgue accusant leur sottise,</l>
						<l n="50" num="7.2">Sous le vernis menteur dont on les civilise,</l>
						<l n="51" num="7.3">Aveugle, n'avait pas reconnu jusqu'ici</l>
						<l n="52" num="7.4">Le barbare du Nord à peine dégrossi.</l>
						<l n="53" num="7.5">Elle voit désormais ! — Cette guerre sauvage,</l>
						<l n="54" num="7.6">En arrachant le masque, a montré le visage !</l>
						<l n="55" num="7.7">La haine sauvera les Prussiens du mépris !</l>
						<l n="56" num="7.8">Le serpent, à nos yeux effrayés et surpris,</l>
						<l n="57" num="7.9">A jeté son venin en rampant dans la vase !…</l>
						<l n="58" num="7.10">On ne méprise pas la vipère, — on l'écrase !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>