<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ21">
					<head type="number">XXI</head>
					<head type="main">PARIS</head>
					<lg n="1">
						<l n="1" num="1.1">Le soleil brille, tout est joie !</l>
						<l n="2" num="1.2">La gaîté rit dans tous les yeux !</l>
						<l n="3" num="1.3">Il semble qu'un rayon flamboie</l>
						<l n="4" num="1.4">Dans les âmes et dans les cieux !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Le peuple, en habits de dimanche,</l>
						<l n="6" num="2.2">Se répand sur les boulevards ;</l>
						<l n="7" num="2.3">Sur la chaussée et sur la branche,</l>
						<l n="8" num="2.4">Oiseaux et gamins sont bavards !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">On entend, comme aux jours de foire,</l>
						<l n="10" num="3.2">Les clochettes et les tambours ;</l>
						<l n="11" num="3.3">Le grelot vous convie à boire ;</l>
						<l n="12" num="3.4">La ville se mêle aux faubourgs.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Plus de garde qui s'effarouche</l>
						<l n="14" num="4.2">D'un refrain qu'on entonne en chœur ;</l>
						<l n="15" num="4.3">Les chants volent de bouche en bouche</l>
						<l n="16" num="4.4">L'espoir vole de cœur en cœur !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Aux remparts, les hommes s'appellent</l>
						<l n="18" num="5.2">« Amis ! » sans connaître leurs noms ;</l>
						<l n="19" num="5.3">Les enfants aux canons se mêlent,</l>
						<l n="20" num="5.4">La mère sourit aux canons !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">On improvise une muraille,</l>
						<l n="22" num="6.2">On bouleverse un grand chemin,</l>
						<l n="23" num="6.3">On cause, on plaisante, on travaille</l>
						<l n="24" num="6.4">Avec des serrements de main.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Un étranger surpris s'arrête,</l>
						<l n="26" num="7.2">Et, voyant tant d'émotion,</l>
						<l n="27" num="7.3">Demande : « Quelle est cette fête ?… »</l>
						<l n="28" num="7.4">— C'est une révolution !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">C'est un empire qui s'écroule !</l>
						<l n="30" num="8.2">C'est toi, liberté, qui reviens !</l>
						<l n="31" num="8.3">C'est la vengeance de la foule !</l>
						<l n="32" num="8.4">C'est Paris— devant les Prussiens !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Septembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>