<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ3">
					<head type="number">II</head>
					<head type="main">TOAST<ref target="1" type="noteAnchor">1</ref></head>
					<lg n="1">
						<l n="1" num="1.1">Tandis que, souriant aux bouteilles nouvelles,</l>
						<l n="2" num="1.2">Un peu de sens encor demeure en vos cervelles.</l>
						<l n="3" num="1.3">Et que les noirs flacons, dans la glace endormis.</l>
						<l n="4" num="1.4">Gardent en paix l'ivresse, un mot, ô mes amis !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Et toi, Muse indolente, amoureuse des fêtes,</l>
						<l n="6" num="2.2">Qui te plais à tresser des roses pour nos têtes,</l>
						<l n="7" num="2.3">Et, guidant au plaisir tes pâles nourrissons,</l>
						<l n="8" num="2.4">Les allaites de vin et de molles chansons,</l>
						<l n="9" num="2.5">Toi qui n'a pas au cœur une mâle pensée,</l>
						<l n="10" num="2.6">Et de la voix du peuple as l'oreille blessée,</l>
						<l n="11" num="2.7">N'arrête pas ici tes regards incertains ;</l>
						<l n="12" num="2.8">Baisse ton voile et passe, ô Muse des festins !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">La muse que j'appelle et la Muse que j'aime,</l>
						<l n="14" num="3.2">C'est celle à qui la France a donné le baptême ;</l>
						<l n="15" num="3.3">C'est celle qui s'émeut des cris de ses enfants,</l>
						<l n="16" num="3.4">Qui nous porte, orgueilleuse, en ses bras triomphants,</l>
						<l n="17" num="3.5">Et nous fait écouter au loin, par intervalles,</l>
						<l n="18" num="3.6">Les sonores chansons de la poudre et des balles.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Mes amis, on a dit que nos cœurs abattus</l>
						<l n="20" num="4.2">Étaient déshérités de toutes les vertus,</l>
						<l n="21" num="4.3">Que nous avions perdu le souvenir antique</l>
						<l n="22" num="4.4">Des austères devoirs du foyer domestique,</l>
						<l n="23" num="4.5">Et que, se renfermant dans son ciel oublié,</l>
						<l n="24" num="4.6">Dieu retirait à lui l'amour et l'amitié.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Et moi, qui sens encore aux belles destinées</l>
						<l n="26" num="5.2">Se diriger l'ardeur de mes jeunes années,</l>
						<l n="27" num="5.3">Moi, qui suis riche encor d'espérance et de foi,</l>
						<l n="28" num="5.4">Amis, qui crois en vous, amour, qui crois en toi,</l>
						<l n="29" num="5.5">J'ai jeté vers les cieux mes hymnes plus ferventes,</l>
						<l n="30" num="5.6">Et j'ai dit : Non, Seigneur ! nos âmes sont vivantes,</l>
						<l n="31" num="5.7">Ta paternelle main n'a pas déshérité</l>
						<l n="32" num="5.8">Ceux où réside encor l'ardente charité ;</l>
						<l n="33" num="5.9">Et tu ne frapperas, au jour de tes colères,</l>
						<l n="34" num="5.10">Que ceux qui te chantaient en maudissant leurs frères ;</l>
						<l n="35" num="5.11">L'avenir est à nous, et nos pas sont vainqueurs ;</l>
						<l n="36" num="5.12">Ton culte est dans leur bouche, il sera dans nos cœurs !</l>
					</lg>
					<lg n="6">
						<l n="37" num="6.1">Oui, nous serons encor dignes fils de nos pères,</l>
						<l n="38" num="6.2">Mais non pas de ceux-là dont les vaines prières</l>
						<l n="39" num="6.3">Résonnaient tout le jour sans fruit pour les humains,</l>
						<l n="40" num="6.4">Non, Seigneur, mais de ceux qui priaient de leurs mains,</l>
						<l n="41" num="6.5">Des géants qui passaient les mers et les montagnes,</l>
						<l n="42" num="6.6">Et, comme un large fleuve aux arides campagnes</l>
						<l n="43" num="6.7">Laisse après lui les fleurs et la fécondité,</l>
						<l n="44" num="6.8">Laissaient chez. les vaincus la jeûne liberté !</l>
						<l n="45" num="6.9">Nous saurons entonner avec idolâtrie</l>
						<l n="46" num="6.10">Le chant sublime : « Allons, enfants de la patrie !… »</l>
						<l n="47" num="6.11">Partir, les yeux au ciel et le sein haletant,</l>
						<l n="48" num="6.12">Vaincre avec une fourche, et mourir en chantant !</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1">Et j'ai voulu, ce soir, entrechoquant nos verres,</l>
						<l n="50" num="7.2">Boire un vin généreux aux mânes de nos pères,</l>
						<l n="51" num="7.3">Et, mêlant une larme au plaisir insensé,</l>
						<l n="52" num="7.4">Que le jeune avenir saluât le passé !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1846">Juin 1846.</date>
						</dateline>
						<note id="1" type="footnote">
							Ces vers datent de mes vingt ans ; Peut-être leur accent détonne ;<lb/>
							C'est un rayon de mon printemps Dans les brumes de mon automne.
						</note>
					</closer>
				</div></body></text></TEI>