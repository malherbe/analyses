<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ5">
					<head type="number">IV</head>
					<head type="main">QUATRE-VINGT-NEUF</head>
					<opener>
						<salute>A MON PÈRE</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Buvons à l'an quatre— vingt-neuf,</l>
						<l n="2" num="1.2">Qui mit la Bastille par terre,</l>
						<l n="3" num="1.3">Qui nous fit un monde tout neuf,</l>
						<l n="4" num="1.4">Et qui te vit naître, cher père ! !</l>
						<l n="5" num="1.5">Hélas ! les voilà déjà vieux,</l>
						<l n="6" num="1.6">Ces hommes dont la destinée</l>
						<l n="7" num="1.7">Marqua les. fronts audacieux</l>
						<l n="8" num="1.8">Au coin de cette grande année ;</l>
						<l n="9" num="1.9">Mais, si les injures du temps</l>
						<l n="10" num="1.10">Ont courbé leur tête blanchie,</l>
						<l n="11" num="1.11">Leur âme, à jamais affranchie,</l>
						<l n="12" num="1.12">Reste jeune et n'a que vingt ans ;</l>
						<l n="13" num="1.13">L'étincelle de Prométhée</l>
						<l n="14" num="1.14">Rayonne encor dans leur cerveau</l>
						<l n="15" num="1.15">Pour fêter ce monde nouveau,</l>
						<l n="16" num="1.16">O Marseillaise, ils t'ont chantée !</l>
					</lg>
					<lg n="2">
						<l n="17" num="2.1">Tels apparaissent à mes yeux</l>
						<l n="18" num="2.2">Ces derniers débris d'une race</l>
						<l n="19" num="2.3">Qui sans peur, comme dit Horace,</l>
						<l n="20" num="2.4">Attendrait la chûte des cieux !</l>
						<l n="21" num="2.5">Tel je te contemple, ô mon père ;</l>
						<l n="22" num="2.6">Et ce feu toujours renaissant,</l>
						<l n="23" num="2.7">Tu me l'as transmis, je l'espère,</l>
						<l n="24" num="2.8">Dans une goutte de ton sang !</l>
					</lg>
					<lg n="3">
						<l n="25" num="3.1">Qu'importe que l'hypocrisie</l>
						<l n="26" num="3.2">Ramène ses noirs bataillons,</l>
						<l n="27" num="3.3">Qu'une royale fantaisie</l>
						<l n="28" num="3.4">Couse la pourpre à des haillons,</l>
						<l n="29" num="3.5">Qu'un soldat français gagne à Rome</l>
						<l n="30" num="3.6">Ses grades par des oremus,</l>
						<l n="31" num="3.7">Qu'on immole au non possumus</l>
						<l n="32" num="3.8">L'humanité devant un homme,</l>
						<l n="33" num="3.9">Que nos jeunes gens tout contrits</l>
						<l n="34" num="3.10">Auprès de Saint Vincent de Paule</l>
						<l n="35" num="3.11">Aillent chercher un coup d'épaule</l>
						<l n="36" num="3.12">Et mettre leurs vertus à prix,</l>
						<l n="37" num="3.13">Que, sous prétexte de famille,</l>
						<l n="38" num="3.14">Et d'ordre, et de propriété,</l>
						<l n="39" num="3.15">On nous administre, on nous pille,</l>
						<l n="40" num="3.16">On nous vole la liberté ?</l>
						<l n="41" num="3.17">Qu'importe qu'un peuple hasarde</l>
						<l n="42" num="3.18">Ce legs glorieux du vainqueur ?</l>
						<l n="43" num="3.19">Il suffit que je te regarde,</l>
						<l n="44" num="3.20">Et la foi renaît dans mon cœur !</l>
						<l n="45" num="3.21">Je vois rayonner sur la France</l>
						<l n="46" num="3.22">Cet astre un moment éclipsé,</l>
						<l n="47" num="3.23">Et dans nos regrets l'espérance,</l>
						<l n="48" num="3.24">Et l'avenir dans le passé !</l>
						<l n="49" num="3.25">Je vois s'épanouir la terre</l>
						<l n="50" num="3.26">Sous les clartés du vieux Voltaire,</l>
						<l n="51" num="3.27">Et les Patouillets effarés</l>
						<l n="52" num="3.28">Regagner leurs antres sacrés !</l>
						<l n="53" num="3.29">Le cœur s'échauffe, l'âme vibre ;</l>
						<l n="54" num="3.30">Le coq gaulois, joyeux et libre,</l>
						<l n="55" num="3.31">Écrase l'aigle dans son œuf !</l>
						<l n="56" num="3.32">Je te regarde, et vois éclore,</l>
						<l n="57" num="3.33">Dans ta vieillesse, une autre aurore</l>
						<l n="58" num="3.34">En buvant à quatre-vingt-neuf !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1862">18 octobre 1862.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>